package io.vov.vitamio.widget.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.view.play.PlayActivity;
import com.dongao.mainclient.model.bean.play.CourseDetail;
import com.dongao.mainclient.model.bean.play.CourseWare;


/**
 * Created by fengzongwei on 2016/1/4.
 */
public class MediaControllerEpListAdapter extends BaseExpandableListAdapter {

    private CourseDetail courseDetail;
    private PlayActivity context;
    private CourseWare cw;
    private final int[] childeType = {0,1};
    private int playingGroupPosition,playingChildPosition;

    public MediaControllerEpListAdapter(PlayActivity context, CourseDetail courseDetail, CourseWare cw){
        this.context = context;
        this.courseDetail = courseDetail;
        this.cw=cw;
    }

    @Override
    public int getGroupCount() {
//        return courseDetail.getMobileSectionList().size();
        return 0;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
//            return courseDetail.getMobileSectionList().get(groupPosition).getMobileCourseWareList().size();
            return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return null;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        ViewHolderGroup viewHolderGroup = null;
        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.media_group,parent,false);
            viewHolderGroup = new ViewHolderGroup();
            viewHolderGroup.textView_groupTitle = (TextView)convertView.findViewById(R.id.play_courselist_group_tv);
            viewHolderGroup.imageView_arrow = (ImageView)convertView.findViewById(R.id.play_courselist_group_img);
            viewHolderGroup.linearLayout_course_body = (LinearLayout)convertView.findViewById(R.id.play_courselist_group_course_body);
            convertView.setTag(viewHolderGroup);
        }else{
            viewHolderGroup = (ViewHolderGroup)convertView.getTag();
        }

//        viewHolderGroup.textView_groupTitle.setText(courseDetail.getMobileSectionList().get(groupPosition).getSectionName());
//        if(!isExpanded){
//            viewHolderGroup.imageView_arrow.setImageResource(R.drawable.play_courselist_group_dowm);
//        }else{
//            viewHolderGroup.imageView_arrow.setImageResource(R.drawable.play_courselist_group_up);
//        }
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ViewHolderChildCourse viewHolderChildCourse = null;
        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.media_child,parent,false);
            viewHolderChildCourse = new ViewHolderChildCourse();
            viewHolderChildCourse.textView_title = (TextView)convertView.findViewById(R.id.play_courselist_child_tv);
            convertView.setTag(viewHolderChildCourse);
        }else{
            viewHolderChildCourse = (ViewHolderChildCourse)convertView.getTag();
        }

//        viewHolderChildCourse.textView_title.setText(courseDetail.getMobileSectionList().get(groupPosition).getMobileCourseWareList()
//                .get(childPosition).getChapterNo()+" " +courseDetail.getMobileSectionList().get(groupPosition).getMobileCourseWareList()
//                .get(childPosition).getCwName());
//
//        if(courseDetail.getMobileSectionList().get(groupPosition).getMobileCourseWareList().get(childPosition).getCwName().equals(cw.getCwName())){
//            viewHolderChildCourse.textView_title.setTextColor(context.getResources().getColor(R.color.exam_list_error));
//        }else{
//            viewHolderChildCourse.textView_title.setTextColor(Color.WHITE);
//        }

        return convertView;
    }

    public void setPlayingPosition(int playingGroupPosition,int playingChildPosition){
        this.playingGroupPosition = playingGroupPosition;
        this.playingChildPosition = playingChildPosition;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    static class ViewHolderGroup{
        TextView textView_groupTitle;
        ImageView imageView_arrow;
        LinearLayout linearLayout_course_body;
    }

    static class ViewHolderChildCourse{
        TextView textView_title;
        ImageView imageView_download_status;
        TextView textView_download_status, textView_progress, textView_courseName;
        LinearLayout ll_download;
    }
}
