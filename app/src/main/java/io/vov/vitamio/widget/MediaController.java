/*
 * Copyright (C) 2006 The Android Open Source Project
 * Copyright (C) 2013 YIXIA.COM
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.vov.vitamio.widget;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioManager;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.AppConfig;
import com.bjjy.mainclient.phone.download.db.DownloadDB;
import com.bjjy.mainclient.phone.download.db.OperaDB;
import com.bjjy.mainclient.phone.utils.MenuRightAnimations;
import com.bjjy.mainclient.phone.utils.NetWorkUtils;
import com.bjjy.mainclient.phone.utils.NetworkUtil;
import com.bjjy.mainclient.phone.view.play.PlayActivity;
import com.bjjy.mainclient.phone.view.play.adapter.WrapperExpandableListAdapter;
import com.bjjy.mainclient.phone.view.play.domain.CwStudyLogDB;
import com.bjjy.mainclient.phone.view.play.utils.CommonGestures;
import com.bjjy.mainclient.phone.view.play.widget.FloatingGroupExpandableListView;
import com.bjjy.mainclient.phone.widget.DialogManager;
import com.dongao.libs.dongaoumengpushlib.constants.PushConstants;
import com.dongao.mainclient.core.util.FileUtil;
import com.dongao.mainclient.model.bean.play.CourseDetail;
import com.dongao.mainclient.model.bean.play.CourseWare;
import com.dongao.mainclient.model.bean.user.MyCollection;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.MyCollectionDB;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.umeng.analytics.MobclickAgent;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import io.vov.vitamio.MediaPlayer;
import io.vov.vitamio.utils.Log;
import io.vov.vitamio.utils.StringUtils;
import io.vov.vitamio.widget.adapter.MediaControllerEpListAdapter;
import io.vov.vitamio.widget.adapter.NoChapterAdapter;

//import com.dongao.app.dongaoacc.R;

/**
 * A view containing controls for a MediaPlayer. Typically contains the buttons
 * like "Play/Pause" and a progress slider. It takes care of synchronizing the
 * controls with the state of the MediaPlayer.
 * <p/>
 * The way to use this class is to a) instantiate it programatically or b)
 * create it in your xml layout.
 * <p/>
 * a) The MediaController will create a default set of controls and put them in
 * a window floating above your application. Specifically, the controls will
 * float above the view specified with setAnchorView(). By default, the window
 * will disappear if left idle for three seconds and reappear when the user
 * touches the anchor view. To customize the MediaController's style, layout and
 * controls you should extend MediaController and override the {#link
 * {@link #makeControllerView()} method.
 * <p/>
 * b) The MediaController is a FrameLayout, you can put it in your layout xml
 * and get it through {@link #findViewById(int)}.
 * <p/>
 * NOTES: In each way, if you want customize the MediaController, the SeekBar's
 * id must be mediacontroller_progress, the Play/Pause's must be
 * mediacontroller_pause, current time's must be mediacontroller_time_current,
 * total time's must be mediacontroller_time_total, file name's must be
 * mediacontroller_file_name. And your resources must have a pause_button
 * drawable and a play_button drawable.
 * <p/>
 * Functions like show() and hide() have no effect when MediaController is
 * created in an xml layout.
 */
public class MediaController extends FrameLayout implements
		View.OnClickListener, OnItemClickListener, CompoundButton.OnCheckedChangeListener, ExpandableListView.OnChildClickListener {
	private static final int sDefaultTimeout = 5000;
	private static final int TIME_STUDY_LOG = 1000 * 10; // 10s同步一次
	private static final int TIME_STUDY_LOG_SYNC = 1000 * 60 * 5;//先设为5分钟
	private static final int DIALOG_OPEN = 1000;//1S
	private static final int FADE_OUT = 1;
	private static final int SHOW_PROGRESS = 2;
	private static final int PLAY_ISTART = 131313;
	private static final int MSG_STUDY_LOG = 8;
	private static final int EXAM_DOWNLOAD = 13;    //下载随堂练习
	private static final int POST_STUDYLOG = 14;  //上传数据
	private MediaPlayerControl mPlayer;
	private Activity mContext;
	public PopupWindow mWindow;
	private int mAnimStyle;
	private View mAnchor;
	private View mRoot;
	private SeekBar mProgress;
	private TextView mEndTime, mCurrentTime;
	private TextView mFileName;
	private OutlineTextView mInfoView;
	private String mTitle;
	private long mDuration;
	private boolean mShowing;
	private boolean mDragging;
	private boolean mInstantSeeking = false;
	private boolean mFromXml = false;

	@Override
	public boolean showContextMenu() {
		return super.showContextMenu();
	}

	private ImageButton mPauseButton;
	private AudioManager mAM;
	private OnShownListener mShownListener;
	private OnHiddenListener mHiddenListener;

	private View mOperationVolLum; // 音量和亮度调节view
	private ImageView mVolLumNum;// 音量调节图片
	private ImageView mVolLumBg; // 亮度调节图片
	private RelativeLayout controller;
	private LinearLayout chapter, lecture;
	private LinearLayout rightlist;
	private CommonGestures mGestureDetector;
	private ImageView back;
	private ImageButton mini;
	private LinearLayout right;
	private FloatingGroupExpandableListView listview_expand;
	private ListView listview;
	private List<CourseWare> list = new ArrayList<CourseWare>();
	private CourseWare courseWare;
	/**
	 * 同步 wyc
	 **/
	private PlayActivity playActivity;
	private VideoView videoView;
	private long endTime = 0;

	private View bottom, top;
	private Handler playerHander;

	private static final int DATA_POST = 20001;//同步数据库
	private static final int DATA_POST_STARTTIME = 30001;//同步开始学习时间
	private CwStudyLogDB studyDB;
	@SuppressLint("HandlerLeak")
	public Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			long pos;
			switch (msg.what) {
				case FADE_OUT:
					hide();
					break;
				case SHOW_PROGRESS:
					pos = setProgress();
					if (!mDragging && mShowing) {
						msg = obtainMessage(SHOW_PROGRESS);
						sendMessageDelayed(msg, 1000 - (pos % 1000));
						updatePausePlay();
					}
					break;
				/**同步 wyc**/
				case MSG_STUDY_LOG:
//                    System.out.println("msg_study_log 方法每隔10s走一次");
					Message dataPost = Message.obtain();
					dataPost.what = DATA_POST;
					dataPost.obj = mPlayer.getCurrentPosition();
					playerHander.sendMessage(dataPost);
					mHandler.sendEmptyMessageDelayed(MSG_STUDY_LOG, TIME_STUDY_LOG);
					break;
				case POST_STUDYLOG:
					Message postlog = Message.obtain();
					postlog.what = POST_STUDYLOG;
					playerHander.sendMessage(postlog);
					mHandler.sendEmptyMessageDelayed(POST_STUDYLOG, TIME_STUDY_LOG_SYNC);//每5分钟上传一次数据
					break;
			}
		}
	};
	private OnClickListener mPauseListener = new OnClickListener() {
		public void onClick(View v) {
			doPauseResume();
			show(sDefaultTimeout);
//            playActivity.notifyPlayStatus();
		}
	};
	private OnSeekBarChangeListener mSeekListener = new OnSeekBarChangeListener() {
		public void onStartTrackingTouch(SeekBar bar) {
			/**同步 wyc**/
			endTime = mPlayer.getCurrentPosition();
			mDragging = true;
			show(3600000);
			mHandler.removeMessages(SHOW_PROGRESS);
			if (mInstantSeeking)
				mAM.setStreamMute(AudioManager.STREAM_MUSIC, true);
			if (mInfoView != null) {
				mInfoView.setText("");
				mInfoView.setVisibility(View.VISIBLE);
			}
			playerHander.sendEmptyMessage(PLAY_ISTART);//按Home键播放暂停 控制isStart逻辑
		}

		public void onProgressChanged(SeekBar bar, int progress,
									  boolean fromuser) {
			if (!fromuser)
				return;

			long newposition = (mDuration * progress) / 1000;
			String time = StringUtils.generateTime(newposition);
			if (mInstantSeeking)
				mPlayer.seekTo(newposition);
			if (mInfoView != null)
				mInfoView.setText(time);
			if (mCurrentTime != null)
				mCurrentTime.setText(time);
			progress_play.setText(time + "/");
		}

		public void onStopTrackingTouch(SeekBar bar) {
			if (!mInstantSeeking) {
				mPlayer.seekTo((mDuration * bar.getProgress()) / 1000);
			}

			if (mInfoView != null) {
				mInfoView.setText("");
				mInfoView.setVisibility(View.GONE);
			}
			show(sDefaultTimeout);
			mHandler.removeMessages(SHOW_PROGRESS);
			mAM.setStreamMute(AudioManager.STREAM_MUSIC, false);
			mDragging = false;
			mHandler.sendEmptyMessageDelayed(SHOW_PROGRESS, 1000);

			/****wyc***/
			Message dataPost = Message.obtain();
			dataPost.what = DATA_POST;
			dataPost.obj = endTime;
			playerHander.sendMessage(dataPost);

			mWebJSObject.jumpLecture();
			playActivity.setCourseHandOutTime(mPlayer.getCurrentPosition() / 1000);
		}
	};

	public MediaController(Context context, AttributeSet attrs) {
		super(context, attrs);
		mRoot = this;
		mFromXml = true;
		initController(context);
	}

	/**
	 * 同步 wyc
	 **/
	public MediaController(Context context, VideoView mVideoView, Handler handler) {
		super(context);
		videoView = mVideoView;
		playerHander = handler;
		if (!mFromXml && initController(context))
			initFloatingWindow();
		initAnimation();
	}

	private Animation mAnimSlideInRight;
	private Animation mAnimSlideInTop;
	private Animation mAnimSlideInBottom;
	private Animation mAnimSlideOutTop;
	private Animation mAnimSlideOutBottom;
	private Animation mAnimSlideOutRight;

	private void initAnimation() {
		mAnimSlideInTop = AnimationUtils.loadAnimation(mContext,
				R.anim.slide_in_top);
		mAnimSlideOutTop = AnimationUtils.loadAnimation(mContext,
				R.anim.slide_out_top);

		mAnimSlideOutRight = AnimationUtils.loadAnimation(mContext,
				R.anim.slide_out_right);
		mAnimSlideInRight = AnimationUtils.loadAnimation(mContext,
				R.anim.slide_in_right);

		mAnimSlideInBottom = AnimationUtils.loadAnimation(mContext,
				R.anim.slide_in_bottom);
		mAnimSlideInBottom.setAnimationListener(new Animation.AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				if (lock != null) {
					if (mContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
						lock.setVisibility(View.VISIBLE);
					}
				}
			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}
		});
		mAnimSlideOutBottom = AnimationUtils.loadAnimation(mContext,
				R.anim.slide_out_bottom);
		mAnimSlideOutBottom.setAnimationListener(new Animation.AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				controller.setVisibility(View.GONE);
//				mHandler.removeMessages(MSG_HIDE_SYSTEM_UI);
				if (lock != null) {
					if (mContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
						lock.setVisibility(View.GONE);
					}
				}
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}
		});
	}

	private boolean initController(Context context) {
		mContext = (Activity) context;
		mAM = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
		return true;
	}

	@Override
	public void onFinishInflate() {
		if (mRoot != null)
			initControllerView(mRoot);
		super.onFinishInflate();
	}

	private void initFloatingWindow() {
		mWindow = new PopupWindow(mContext);
		mWindow.setFocusable(true);
		mWindow.setBackgroundDrawable(new BitmapDrawable());
		mWindow.setOutsideTouchable(true);
		mAnimStyle = android.R.style.Animation;
	}

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	public void setWindowLayoutType() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			try {
//				mAnchor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
				Method setWindowLayoutType = PopupWindow.class.getMethod(
						"setWindowLayoutType", new Class[]{int.class});
				setWindowLayoutType
						.invoke(mWindow,
								WindowManager.LayoutParams.TYPE_APPLICATION_ATTACHED_DIALOG);
			} catch (Exception e) {
				Log.e("setWindowLayoutType", e);
			}
		}
	}

	/**
	 * Set the view that acts as the anchor for the control view. This can for
	 * example be a VideoView, or your Activity's main view.
	 *
	 * @param view The view to which to anchor the controller when it is visible.
	 */
	public void setAnchorView(View view) {
		mAnchor = view;
		if (!mFromXml) {
			removeAllViews();
			mRoot = makeControllerView();
			mWindow.setContentView(mRoot);
			mWindow.setWidth(LayoutParams.MATCH_PARENT);
			mWindow.setHeight(LayoutParams.WRAP_CONTENT);
		}
		initControllerView(mRoot);
	}

	/**
	 * Create the view that holds the widgets that control playback. Derived
	 * classes can override this to create their own.
	 *
	 * @return The controller view.
	 */
	protected View makeControllerView() {
		return ((LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
				R.layout.mediacontroller, this);
	}

	private NoChapterAdapter adapter;
	private WebView mWebView;
	private LinearLayout ll_webView;

	private GestureDetector.SimpleOnGestureListener mWebViewGDlistenter;
	private GestureDetector mWebViewGD;
	public WebJSObject mWebJSObject = new WebJSObject();
	private RelativeLayout layer, mediacontroller_top_right;
	private CheckBox collect;
	private ImageButton bt_offline, bt_chapter, bt_lectrue;
	private ImageView lecture_web;
	private LinearLayout mediacontroller_offline;
	private TextView lectureName;

	private LinearLayout bottom_three;
	private TextView speed1, speed2, speed3, speed4;
	private RelativeLayout speed;
	private TextView chooseSpeed;
	private TextView tv_lecture;

	private MyCollectionDB collectionDB;
	private MyCollection collection;
	private TextView excise;
	public ImageView lock;
	private TextView progress_play, progressAll;
	private LinearLayout ll_progress;

	@SuppressWarnings("deprecation")
	private void initControllerView(View v) {
		studyDB = new CwStudyLogDB(mContext);//初始化学习记录表
		db = new DownloadDB(mContext);
		operaDB = new OperaDB(mContext);
		collectionDB = new MyCollectionDB(mContext);

		progress_play = (TextView) v.findViewById(R.id.tv_progress);
		progressAll = (TextView) v.findViewById(R.id.tv_progressall);
		ll_progress = (LinearLayout) v.findViewById(R.id.ll_progress);

		lock = (ImageView) v.findViewById(R.id.img_lock);
		lock.setOnClickListener(this);

		tv_lecture = (TextView) v.findViewById(R.id.mediacontroller_lecture);
		speed = (RelativeLayout) v.findViewById(R.id.rl_speed);
		speed1 = (TextView) v.findViewById(R.id.media_speed1);
		speed2 = (TextView) v.findViewById(R.id.media_speed2);
		speed3 = (TextView) v.findViewById(R.id.media_speed3);
		speed4 = (TextView) v.findViewById(R.id.media_speed4);
		speed1.setOnClickListener(this);
		speed2.setOnClickListener(this);
		speed3.setOnClickListener(this);
		speed4.setOnClickListener(this);
		tv_lecture.setOnClickListener(this);

		excise = (TextView) v.findViewById(R.id.tv_excise);
		excise.setOnClickListener(this);

		chooseSpeed = (TextView) v.findViewById(R.id.mediacontroller_speed);
		chooseSpeed.setOnClickListener(this);

		controller = (RelativeLayout) v.findViewById(R.id.layout_control);
		controller.setOnClickListener(this);
		layer = (RelativeLayout) v.findViewById(R.id.layer);
		right = (LinearLayout) v.findViewById(R.id.mediacontroller_right);
		mediacontroller_top_right = (RelativeLayout) v.findViewById(R.id.mediacontroller_top_right);
		collect = (CheckBox) v.findViewById(R.id.cb_collect);
		collect.setOnCheckedChangeListener(this);
		bottom_three = (LinearLayout) v.findViewById(R.id.ll_bottom_three);

		mediacontroller_offline = (LinearLayout) v.findViewById(R.id.mediacontroller_offline);
		bt_offline = (ImageButton) v.findViewById(R.id.img_download);
		bt_chapter = (ImageButton) v.findViewById(R.id.img_chapter);
		bt_lectrue = (ImageButton) v.findViewById(R.id.img_lecture);
		bt_offline.setOnClickListener(this);
		bt_chapter.setOnClickListener(this);
		bt_lectrue.setOnClickListener(this);
		mediacontroller_offline.setOnClickListener(this);
		mediacontroller_offline.setVisibility(INVISIBLE);
		//// TODO: 2017/11/7  
		setNoUsedGone();
		// TODO: 2017/11/7  
		
		lectureName = (TextView) v.findViewById(R.id.media_lecture_name);

		bottom = v.findViewById(R.id.rl_bottom);
		top = v.findViewById(R.id.linear_top);
		ll_webView = (LinearLayout) v.findViewById(R.id.ll_webview);


		chapter = (LinearLayout) v.findViewById(R.id.mediacontroller_post);
		chapter.setOnClickListener(this);
		rightlist = (LinearLayout) v.findViewById(R.id.media_work_list);
		listview_expand = (FloatingGroupExpandableListView) v.findViewById(R.id.listview_expand);
		listview_expand.setOnChildClickListener(this);
		listview = (ListView) v.findViewById(R.id.listview);
		listview.setOnItemClickListener(this);

		lecture = (LinearLayout) v.findViewById(R.id.mediacontroller_lession);
		lecture.setOnClickListener(this);


		mGestureDetector = new CommonGestures(mContext);
		mGestureDetector.setTouchListener(mTouchListener, true);
		controller.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				onTouchEvent(event);
				return mGestureDetector.onTouchEvent(event);
			}
		});


		this.mWebViewGDlistenter = new GestureDetector.SimpleOnGestureListener() {
			@Override
			public boolean onDoubleTap(MotionEvent e) {
				toggleMediaControlsVisiblity();
				return super.onDoubleTap(e);
			}

			@Override
			public void onLongPress(MotionEvent e) {
				// TODO Auto-generated method stub
				return;
				// super.onLongPress(e);
			}

		};

		this.mWebViewGD = new GestureDetector(mContext, mWebViewGDlistenter);

		lecture_web = (ImageView) v.findViewById(R.id.lecture_web);
		lecture_web.setOnClickListener(this);
		mWebView = (WebView) v.findViewById(R.id.webview);
		mWebView.getSettings().setAppCacheEnabled(false);
		mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
//		String ur=Environment.getExternalStorageDirectory()+"/lecture.htm";
		if (!TextUtils.isEmpty(webUrl)) {
			mWebView.loadUrl(webUrl);
		} else {
			mWebView.loadDataWithBaseURL("", "讲义加载失败", "text/html", "UTF-8", "");
		}
		//"file:///android_asset/lecture.html"
		mWebView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				MediaController.this.mWebViewGD.onTouchEvent(event);
				mWebView.onTouchEvent(event);
				return true;
			}

		});
		mWebView.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				// TODO Auto-generated method stub
				return true;
			}
		});

		mWebView.getSettings().setDefaultTextEncodingName("UTF-8");
		// 开启JavaScript支持
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.setWebChromeClient(wc);
		mWebView.setWebViewClient(mWebClient);
		mWebView.addJavascriptInterface(mWebJSObject, "myObject");


		mVolLumBg = (ImageView) v.findViewById(R.id.operation_bg);
		mVolLumNum = (ImageView) v.findViewById(R.id.operation_percent);
		mOperationVolLum = v.findViewById(R.id.operation_volume_brightness);
		mMaxVolume = mAM.getStreamMaxVolume(AudioManager.STREAM_MUSIC);

		back = (ImageView) v.findViewById(R.id.img_back);
		back.setOnClickListener(this);
		mini = (ImageButton) v.findViewById(R.id.mini_screen);
		mini.setOnClickListener(this);

		mPauseButton = (ImageButton) v.findViewById(getResources()
				.getIdentifier("mediacontroller_play_pause", "id",
						mContext.getPackageName()));
		if (mPauseButton != null) {
			mPauseButton.requestFocus();
			mPauseButton.setOnClickListener(mPauseListener);
		}

		mProgress = (SeekBar) v.findViewById(getResources().getIdentifier(
				"mediacontroller_seekbar", "id", mContext.getPackageName()));
		if (mProgress != null) {
			if (mProgress instanceof SeekBar) {
				SeekBar seeker = (SeekBar) mProgress;
				seeker.setOnSeekBarChangeListener(mSeekListener);
			}
			mProgress.setMax(1000);
		}

		mEndTime = (TextView) v.findViewById(getResources().getIdentifier(
				"mediacontroller_time_total", "id", mContext.getPackageName()));
		mCurrentTime = (TextView) v.findViewById(getResources()
				.getIdentifier("mediacontroller_time_current", "id",
						mContext.getPackageName()));
		mFileName = (TextView) v.findViewById(getResources().getIdentifier(
				"mediacontroller_file_name", "id", mContext.getPackageName()));
		mFileName.setOnClickListener(this);

		if (mContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
			setGone();
			setHight(SharedPrefHelper.getInstance(mContext).getMiniScreenHight());
		} else {
			setVisible();
			setHight_land();
			MenuRightAnimations.initOffset(mContext);
		}

		if (mFileName != null)
			mFileName.setText(mTitle);

		playActivity = (PlayActivity) mContext;
		//判断课程有无章节
		if (playActivity.isHaveChapter()) {
			listview.setVisibility(View.GONE);
			listview_expand.setVisibility(View.VISIBLE);
			courseDetail = playActivity.courseDetail;
			initList(courseDetail);
		} else {
			listview.setVisibility(View.VISIBLE);
			listview_expand.setVisibility(View.GONE);
			list = playActivity.courseWareList;
			adapter = new NoChapterAdapter(list, courseWare, mContext);
			listview.setAdapter(adapter);
		}

		collection = collectionDB.findCollection(SharedPrefHelper.getInstance(mContext).getUserId(), courseWare.getClassId() + "_" + courseWare.getCwId(), "3");
		if (collection != null) {
			isCollect = true;
			isFirst = true;
			collect.setChecked(true);
			bt_lectrue.setImageResource(R.drawable.collect_star_deep);
		} else {
			isFirst = false;
			isCollect = false;
			collect.setChecked(false);
			bt_lectrue.setImageResource(R.drawable.collect_star_ligt);
		}

		/**同步 wyc**/
		loadData();
	}

	private void setNoUsedGone() {
		bt_lectrue.setVisibility(GONE);
		excise.setVisibility(GONE);
		collect.setVisibility(GONE);
	}

	public CourseDetail courseDetail;
	private MediaControllerEpListAdapter courseListAdapter;

	private void initList(CourseDetail courseDetail) {
		courseListAdapter = new MediaControllerEpListAdapter(playActivity, courseDetail, courseWare);
		final WrapperExpandableListAdapter wrapperAdapter = new WrapperExpandableListAdapter(courseListAdapter);
		listview_expand.setAdapter(wrapperAdapter);
//		for (int i = 0; i < courseDetail.getMobileSectionList().size(); i++) {
//			listview_expand.expandGroup(i);
//		}
	}

	private boolean isFirst;

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (mContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
			return;
		}
		if (isChecked) {
			collectionDB.insertNotUpdate(myCollection);
			bt_lectrue.setImageResource(R.drawable.collect_star_deep);
			if (!isFirst) {
				Toast toast = Toast.makeText(mContext, "收藏成功", Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.TOP, 0, SharedPrefHelper.getInstance(mContext).getMiniScreenHight() / 2);
				toast.show();
				isFirst = false;
			}
			isCollect = true;
		} else {
			isCollect = false;
			if (collection == null) {
				collection = collectionDB.findCollection(SharedPrefHelper.getInstance(mContext).getUserId(), courseWare.getClassId() + "_" + courseWare.getCwId(), "3");
			}
			collectionDB.delete(collection);
			bt_lectrue.setImageResource(R.drawable.collect_star_ligt);
			Toast toast = Toast.makeText(mContext, "取消收藏", Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.TOP, 0, SharedPrefHelper.getInstance(mContext).getMiniScreenHight() / 2);
			toast.show();
		}
	}

	private Handler mWebHanlder = new Handler();

	public class WebJSObject {

		public void jumpLecture() {
			mWebHanlder.post(new Runnable() {
				public void run() {
					mWebView.loadUrl("javascript:jumpLecture("
							+ mPlayer.getCurrentPosition() / 1000 + ")");
				}
			});

		}

		@JavascriptInterface
		public void jumpTime(final int time) {
			mWebHanlder.post(new Runnable() {
				public void run() {

					long endTime = mPlayer.getCurrentPosition();
					System.out.println("jumpTime方法  endTime==" + endTime);
					/****wyc***/
					Message dataPost = Message.obtain();
					dataPost.what = DATA_POST;
					dataPost.obj = endTime;
					playerHander.sendMessage(dataPost);
					mPlayer.seekTo(time * 1000);
					Message dataPost_StartTime = Message.obtain();
					dataPost_StartTime.what = DATA_POST_STARTTIME;
					dataPost_StartTime.obj = mPlayer.getCurrentPosition();
					playerHander.sendMessage(dataPost_StartTime);


				}
			});
		}

	}

	private WebViewClient mWebClient = new WebViewClient() {

		@Override
		public void onPageFinished(WebView view, String url) {
			// TODO Auto-generated method stub
			// mWebJSObject.getAllSection();
			super.onPageFinished(view, url);
		}

		@Override
		public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
			super.onReceivedError(view, errorCode, description, failingUrl);

		}
	};

	private WebChromeClient wc = new WebChromeClient() {

		@Override
		public boolean onJsAlert(WebView view, String url, String message,
								 JsResult result) {
			System.out.println("result==message==" + result + "==" + message);
			return super.onJsAlert(view, url, message, result);
		}

	};

	private void toggleMediaControlsVisiblity() {
		if (controller.isShown()) {
			hide();
		} else {
			show();
		}
	}

	public void setList(List list) {
//        this.list = list;
	}


	private String webUrl;
	private MyCollection myCollection;

	/**
	 * @param flag 是否为本地课程
	 */
	public void setCourseWare(CourseWare cw, boolean flag) {
		this.courseWare = cw;
		myCollection = new MyCollection();
		myCollection.setTitle(cw.getCwName());
		myCollection.setTime(System.currentTimeMillis() + "");
		myCollection.setYear(cw.getYear());
		myCollection.setUserId(SharedPrefHelper.getInstance(mContext).getUserId());
		myCollection.setSubjectId(cw.getSubjectId());
		myCollection.setClasssId(cw.getClassId());
		myCollection.setCwId(cw.getCwId());
		myCollection.setType("3");
		myCollection.setCollectionId(cw.getClassId() + "_" + cw.getCwId());
		if (NetworkUtil.isNetworkAvailable(mContext)) {
			webUrl = cw.getMobileLectureUrl();
		} else {
			File lectrueFile = new File(FileUtil.getDownloadPath(mContext) + SharedPrefHelper.getInstance(mContext).getUserId() + "_" + courseWare.getExamId() + "_" + courseWare.getSubjectId() + "_" + courseWare.getClassId() + "_" + courseWare.getSectionId() + "_" + courseWare.getCwId() + "/lecture/lecture.htm");
			if (lectrueFile.exists()) {
//                webUrl = "http://localhost:" + Constants.SERVER_PORT + "/" + SharedPrefHelper.getInstance(mContext).getUserId() + "_" + courseWare.getClassId() + "_" + courseWare.getCwId() + "_" + courseWare.getYear() + "/lecture/lecture.html";
				webUrl = getLocalLecUrl(courseWare);
			} else {
				webUrl = "";
			}
		}
		mTitle = cw.getCwName();
	}

	private String getLocalLecUrl(CourseWare coursew) {
		String localPath = "http://localhost:" + Constants.SERVER_PORT + "/" + SharedPrefHelper.getInstance(mContext).getUserId() + "_" + coursew.getExamId() + "_" + coursew.getSubjectId() + "_" + coursew.getClassId() + "_" + coursew.getSectionId() + "_" + coursew.getCwId() + "/lecture/lecture.htm";
		return localPath;
	}

	private int mMaxVolume; // 最大音量
	private float mBrightness = 0.01f; // 亮度
	private int mVolume = 0; // 当前音量
	private CommonGestures.TouchListener mTouchListener = new CommonGestures.TouchListener() {

		@Override
		public void onGestureBegin() {
			mBrightness = mContext.getWindow().getAttributes().screenBrightness;
			mVolume = mAM.getStreamVolume(AudioManager.STREAM_MUSIC);
			if (mBrightness <= 0.00f)
				mBrightness = 0.50f;
			if (mBrightness < 0.01f)
				mBrightness = 0.01f;
			if (mVolume < 0)
				mVolume = 0;
			show(10000);
		}

		@Override
		public void onGestureEnd() {
			mOperationVolLum.setVisibility(View.GONE);
		}

		@Override
		public void onLeftSlide(float percent) {
			if (!ll_webView.isShown()) {
				setBrightness(mBrightness + percent);
				setBrightnessScale(mContext.getWindow().getAttributes().screenBrightness);
			}
		}

		@Override
		public void onRightSlide(float percent) {
			int v = (int) (percent * mMaxVolume) + mVolume;
			setVolume(v);
		}

		@Override
		public void onSingleTap() {
			// TODO Auto-generated method stub

		}

		@Override
		public void onDoubleTap() {
			// TODO Auto-generated method stub

		}

		@Override
		public void onScale(float scaleFactor, int state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onLongPress() {
			// TODO Auto-generated method stub

		}

		@Override
		public void forward(float percent) {
			// TODO Auto-generated method stub
		}

		@Override
		public void backward(float percent) {
			// TODO Auto-generated method stub
		}
	};

	/**
	 * 设置亮度图片
	 *
	 * @param scale
	 */
	private void setBrightnessScale(float scale) {
		setGraphicOperationProgress(R.drawable.video_brightness_bg, scale);
	}

	/**
	 * 调节亮度
	 *
	 * @param f
	 */
	private void setBrightness(float f) {
		WindowManager.LayoutParams lp = mContext.getWindow().getAttributes();
		lp.screenBrightness = f;
		if (lp.screenBrightness > 1.0f)
			lp.screenBrightness = 1.0f;
		else if (lp.screenBrightness < 0.01f)
			lp.screenBrightness = 0.01f;
		mContext.getWindow().setAttributes(lp);
	}

	/**
	 * 音量和亮度进度条控制
	 *
	 * @param bgID
	 * @param scale
	 */

	private void setGraphicOperationProgress(int bgID, float scale) {
		mVolLumBg.setImageResource(bgID);
		mOperationVolLum.setVisibility(View.VISIBLE);
		ViewGroup.LayoutParams lp = mVolLumNum.getLayoutParams();
		lp.width = (int) (findViewById(R.id.operation_full).getLayoutParams().width * scale);
		mVolLumNum.setLayoutParams(lp);
	}

	/**
	 * 设置音量
	 *
	 * @param v
	 */
	private void setVolume(int v) {
		if (v > mMaxVolume)
			v = mMaxVolume;
		else if (v < 0)
			v = 0;
		mAM.setStreamVolume(AudioManager.STREAM_MUSIC, v, 0);
		setVolumeScale((float) v / mMaxVolume);
	}

	/**
	 * 设置音量图片
	 *
	 * @param scale
	 */
	private void setVolumeScale(float scale) {
		setGraphicOperationProgress(R.drawable.video_volumn_bg, scale);
	}

	public void setMediaPlayer(MediaPlayerControl player) {
		mPlayer = player;
		updatePausePlay();
	}

	/**
	 * Control the action when the seekbar dragged by user
	 *
	 * @param seekWhenDragging True the media will seek periodically
	 */
	public void setInstantSeeking(boolean seekWhenDragging) {
		mInstantSeeking = seekWhenDragging;
	}

	public void show() {
		show(sDefaultTimeout);
	}

	/**
	 * Set the content of the file_name TextView
	 *
	 * @param name
	 */
//    public void setFileName(String name) {
//        mTitle = name;
//        if (mFileName != null)
//            mFileName.setText(mTitle);
//    }

	/**
	 * Set the View to hold some information when interact with the
	 * MediaController
	 *
	 * @param v
	 */
	public void setInfoView(OutlineTextView v) {
		mInfoView = v;
	}

	/**
	 * <p>
	 * Change the animation style resource for this controller.
	 * </p>
	 * <p/>
	 * <p>
	 * If the controller is showing, calling this method will take effect only
	 * the next time the controller is shown.
	 * </p>
	 *
	 * @param animationStyle animation style to use when the controller appears and
	 *                       disappears. Set to -1 for the default animation, 0 for no
	 *                       animation, or a resource identifier for an explicit animation.
	 */
	public void setAnimationStyle(int animationStyle) {
		mAnimStyle = animationStyle;
	}

	/**
	 * Show the controller on screen. It will go away automatically after
	 * 'timeout' milliseconds of inactivity.
	 *
	 * @param timeout The timeout in milliseconds. Use 0 to show the controller
	 *                until hide() is called.
	 */
	public void show(int timeout) {
		if (mPauseButton != null)
			mPauseButton.requestFocus();
		if (!mShowing) {
			if (!isLock) {
				controller.setVisibility(View.VISIBLE);
				bottom.startAnimation(mAnimSlideInTop);
				top.startAnimation(mAnimSlideInBottom);
			}

			int[] location = new int[2];
			mAnchor.getLocationOnScreen(location);
			Rect anchorRect = new Rect(location[0], location[1], location[0]
					+ mAnchor.getWidth(), location[1] + mAnchor.getHeight());
			mWindow.setAnimationStyle(mAnimStyle);
			setWindowLayoutType();
			mWindow.showAtLocation(mAnchor, Gravity.TOP, 0, 0);// anchorRect.left,anchorRect.bottom
		}

		mShowing = true;
		if (mShownListener != null)
			mShownListener.onShown();

		updatePausePlay();
		mHandler.sendEmptyMessage(SHOW_PROGRESS);

		if (timeout != 0) {
			mHandler.removeMessages(FADE_OUT);
			mHandler.sendMessageDelayed(mHandler.obtainMessage(FADE_OUT),
					timeout);
		}
	}

	public boolean isShowing() {
		return mShowing;
	}

	public void hide() {
		if (mShowing) {
			try {
//				mHandler.removeMessages(SHOW_PROGRESS);
				top.startAnimation(mAnimSlideOutBottom);
				bottom.startAnimation(mAnimSlideOutTop);
//                mHandler.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        mWindow.dismiss();
//                    }
//                },500);
				if (rightlist.isShown()) {
					rightlist.startAnimation(mAnimSlideOutRight);
					mHandler.postDelayed(new Runnable() {
						@Override
						public void run() {
							rightlist.setVisibility(View.GONE);
						}
					}, 500);
				}

				if (animationSpeed) {
					MenuRightAnimations.startAnimationsOut(speed, 300);
					animationSpeed = false;
				}
				if (mContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
					mWindow.dismiss();
//                    mHandler.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            mWindow.dismiss();//若果miss掉，讲义也会miss
//                        }
//                    }, 500);
				}

			} catch (IllegalArgumentException ex) {
				Log.d("MediaController already removed");
			}
			mShowing = false;
			if (mHiddenListener != null)
				mHiddenListener.onHidden();
		}
	}

	public void setOnShownListener(OnShownListener l) {
		mShownListener = l;
	}

	public void setOnHiddenListener(OnHiddenListener l) {
		mHiddenListener = l;
	}

	private long setProgress() {
		if (mPlayer == null || mDragging)
			return 0;
		long position = mPlayer.getCurrentPosition();
		long duration = mPlayer.getDuration();
		if (mProgress != null) {
			if (duration > 0) {
				long pos = 1000L * position / duration;
				mProgress.setProgress((int) pos);
			}
			int percent = mPlayer.getBufferPercentage();
			mProgress.setSecondaryProgress(percent * 10);
		}

		mDuration = duration;

		if (mEndTime != null)
			mEndTime.setText(StringUtils.generateTime(mDuration));
		progressAll.setText(StringUtils.generateTime(mDuration));
		if (mCurrentTime != null)
			mCurrentTime.setText(StringUtils.generateTime(position));
		progress_play.setText(StringUtils.generateTime(position) + "/");

		return position;
	}

	private float mOldX, mOldY;
	private float first;

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (lock != null) {
			if (mContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
				lock.setVisibility(View.VISIBLE);
			}
		}
		if (!isLock) {
			show(sDefaultTimeout);
		}
		if (ll_webView.isShown()) {
			return true;
		}
		switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				mOldX = event.getRawX();
				mOldY = event.getRawY();
				first = event.getRawX();
				break;
			case MotionEvent.ACTION_MOVE:
				float deX = event.getRawX() - mOldX;
				float dey = event.getRawY() - mOldY;
				float defirst = event.getRawX() - first;
				if (Math.abs(deX) < Math.abs(dey) || Math.abs(defirst) < 20 || isLock) {
					break;
				}
				ll_progress.setVisibility(View.VISIBLE);
				if (mOldX >= event.getRawX()) {
					long time = (mDuration * (mProgress.getProgress() - 5)) / 1000;
					mPlayer.seekTo(time);
				} else {
					long time = (mDuration * (mProgress.getProgress() + 5)) / 1000;
					mPlayer.seekTo(time);
				}
				mOldX = event.getRawX();
				mOldY = event.getRawY();
				break;
			case MotionEvent.ACTION_UP:
				ll_progress.setVisibility(View.GONE);
				break;
		}
		return true;
	}

	@Override
	public boolean onTrackballEvent(MotionEvent ev) {
		show(sDefaultTimeout);
		return false;
	}

	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		int keyCode = event.getKeyCode();
		if (event.getRepeatCount() == 0
				&& (keyCode == KeyEvent.KEYCODE_HEADSETHOOK
				|| keyCode == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE || keyCode == KeyEvent.KEYCODE_SPACE)) {
			doPauseResume();
			show(sDefaultTimeout);
			if (mPauseButton != null)
				mPauseButton.requestFocus();
			return true;
		} else if (keyCode == KeyEvent.KEYCODE_MEDIA_STOP) {
			if (mPlayer.isPlaying()) {
				mPlayer.pause();
				updatePausePlay();
			}
			return true;
		} else if (keyCode == KeyEvent.KEYCODE_BACK
				|| keyCode == KeyEvent.KEYCODE_MENU) {
			hide();
			return true;
		} else {
			show(sDefaultTimeout);
		}
		return super.dispatchKeyEvent(event);
	}

	private void updatePausePlay() {
		if (mRoot == null || mPauseButton == null)
			return;

		if (mPlayer.isPlaying())
			mPauseButton.setImageResource(getResources().getIdentifier(
					"mediacontroller_pause", "drawable",
					mContext.getPackageName()));
		else
			mPauseButton.setImageResource(getResources().getIdentifier(
					"mediacontroller_play", "drawable",
					mContext.getPackageName()));
	}

	private void doPauseResume() {
		if (mPlayer.isPlaying())
			mPlayer.pause();
		else
			mPlayer.start();
		playerHander.sendEmptyMessage(PLAY_ISTART);
		updatePausePlay();
	}

	@Override
	public void setEnabled(boolean enabled) {
		if (mPauseButton != null)
			mPauseButton.setEnabled(enabled);
		if (mProgress != null)
			mProgress.setEnabled(enabled);
		super.setEnabled(enabled);
	}

	public interface OnShownListener {
		public void onShown();
	}

	public interface OnHiddenListener {
		public void onHidden();
	}

	public interface MediaPlayerControl {
		void start();

		void pause();

		long getDuration();

		long getCurrentPosition();

		void seekTo(long pos);

		boolean isPlaying();

		int getBufferPercentage();

		void playSpeed(float playSpeed);
	}

	DownloadDB db;
	private boolean isCollect;
	private static final int SCREEN_PORTRAIT = 1111;
	private static final int SCREEN_LANDSCAPE = 7777;

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.layout_control:
				hide();
				break;
			case R.id.img_chapter:
			case R.id.mediacontroller_post:
				show();
				if (rightlist.isShown()) {
					rightlist.startAnimation(mAnimSlideOutRight);
					mHandler.postDelayed(new Runnable() {
						@Override
						public void run() {
							rightlist.setVisibility(View.GONE);
						}
					}, 500);
				} else {
					rightlist.setVisibility(View.VISIBLE);
					rightlist.startAnimation(mAnimSlideInRight);
				}
				break;
			case R.id.img_back:
				if (isLock) {
					return;
				}
				if (mContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
					playActivity.finish();
				} else {
					playerHander.sendEmptyMessage(SCREEN_PORTRAIT);
//                    mContext.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
				}
				break;
			case R.id.mini_screen:
				if (mContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
//                    mContext.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
					playerHander.sendEmptyMessage(SCREEN_LANDSCAPE);
				} else {
					mContext.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
				}
				break;
			case R.id.img_lecture:
			case R.id.mediacontroller_lession:
				MobclickAgent.onEvent(playActivity, PushConstants.PLAY_COLLECT);
				if (isCollect) {
					isCollect = false;
					collect.setChecked(false);
					if (collection == null) {
						collection = collectionDB.findCollection(SharedPrefHelper.getInstance(mContext).getUserId(), courseWare.getClassId() + "_" + courseWare.getCwId(), "3");
					}
					collectionDB.delete(collection);
					bt_lectrue.setImageResource(R.drawable.collect_star_ligt);
					Toast.makeText(mContext, "取消收藏", Toast.LENGTH_SHORT).show();
				} else {
					isCollect = true;
					collect.setChecked(true);
					collectionDB.insertNotUpdate(myCollection);
					bt_lectrue.setImageResource(R.drawable.collect_star_deep);
					Toast.makeText(mContext, "收藏成功", Toast.LENGTH_SHORT).show();
				}
				break;
			case R.id.img_download:
			case R.id.mediacontroller_offline:
				boolean flag = db.CheckIsDownloaded(SharedPrefHelper.getInstance(mContext).getUserId(), courseWare.getClassId(), courseWare.getCwId());
				if (flag) {
					Toast.makeText(mContext, "该视频已下载", Toast.LENGTH_SHORT).show();
				} else {
//                    playActivity.checkMachine(new DownloadView() {
//                        @Override
//                        public void isCheckMachineOk(String msg, boolean isOk) {
//                            if (isOk) {
//                                CheckSettingNetDownload();
//                                Message dl = Message.obtain();
//                                dl.what = EXAM_DOWNLOAD;
//                                playerHander.sendMessage(dl);
//                            } else {
//                                Toast.makeText(playActivity, msg, Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                    });
					Message msgg = Message.obtain();
					msgg.obj = courseWare;
					msgg.what = 9898;
					playerHander.sendMessage(msgg);

				}
				break;
			case R.id.lecture_web:
				ll_webView.startAnimation(mAnimSlideOutRight);
				mHandler.postDelayed(new Runnable() {
					@Override
					public void run() {
						ll_webView.setVisibility(View.GONE);
					}
				}, 500);

				lectureName.setText("讲义");
				break;
			case R.id.mediacontroller_speed:
				if (animationSpeed) {
					MenuRightAnimations.startAnimationsOut(speed, 300);
					animationSpeed = false;
				} else {
					MenuRightAnimations.startAnimationsIn(speed, 300);
					animationSpeed = true;
				}
				break;
			case R.id.media_speed1:
				mPlayer.playSpeed(1.0f);
				chooseSpeed.setText("1.0X");
				MobclickAgent.onEvent(playActivity, PushConstants.PLAY_FASTPLAY1);
				break;
			case R.id.media_speed2:
				mPlayer.playSpeed(1.2f);
				chooseSpeed.setText("1.2X");
				MobclickAgent.onEvent(playActivity, PushConstants.PLAY_FASTPLAY12);
				break;
			case R.id.media_speed3:
				mPlayer.playSpeed(1.5f);
				chooseSpeed.setText("1.5X");
				MobclickAgent.onEvent(playActivity, PushConstants.PLAY_FASTPLAY15);
				break;
			case R.id.media_speed4:
				mPlayer.playSpeed(1.8f);
				chooseSpeed.setText("1.8X");
				MobclickAgent.onEvent(playActivity, PushConstants.PLAY_FASTPLAY18);
				break;
			case R.id.mediacontroller_lecture:
				MobclickAgent.onEvent(playActivity, PushConstants.PLAY_HANDOUT);
				if (ll_webView.isShown()) {
					ll_webView.startAnimation(mAnimSlideOutRight);
					mHandler.postDelayed(new Runnable() {
						@Override
						public void run() {
							ll_webView.setVisibility(View.GONE);
						}
					}, 500);
					lectureName.setText("讲义");
				} else {
					ll_webView.setVisibility(View.VISIBLE);
					ll_webView.startAnimation(mAnimSlideInRight);
					lectureName.setText("视频");
					hide();
				}
				break;
			case R.id.tv_excise:
				break;
			case R.id.img_lock:
				if (isLock) {
					controller.setVisibility(View.VISIBLE);
					bottom.startAnimation(mAnimSlideInTop);
					top.startAnimation(mAnimSlideInBottom);
					show();
					lock.setImageResource(R.drawable.media_btn_unlock);
					isLock = false;
				} else {
					bottom.startAnimation(mAnimSlideOutTop);
					top.startAnimation(mAnimSlideOutBottom);
					hide();
					lock.setImageResource(R.drawable.media_btn_lock);
					isLock = true;
				}
				playerHander.sendEmptyMessage(SCREEN_ISLOCK);
				break;
			default:
				break;
		}

	}

	public boolean isLock;
	private boolean animationSpeed;
	private static final int SCREEN_ISLOCK = 9879;

	/**
	 * 下载前检查网络
	 */
	private void CheckSettingNetDownload() {
		boolean isDownload = SharedPrefHelper.getInstance(mContext).getIsNoWifiPlayDownload();
		NetWorkUtils type = new NetWorkUtils(mContext);
		if (type.getNetType() == 0) {//无网络
			DialogManager.showMsgDialog(mContext, mContext.getResources().getString(R.string.dialog_message_vedio), mContext.getResources().getString(R.string.dialog_title_download), "确定");
		} else if (type.getNetType() == 2) { //流量
			if (!isDownload) {
				DialogManager.showNormalDialog(mContext, mContext.getResources().getString(R.string.dialog_warnning_vedio), "提示", "取消", "确定", new DialogManager.CustomDialogCloseListener() {
					@Override
					public void yesClick() {
						Toast.makeText(mContext, "已加入下载列表", Toast.LENGTH_SHORT).show();
						AppConfig.getAppConfig(mContext).download(courseWare);
					}

					@Override
					public void noClick() {
					}
				});
			} else {
				Toast.makeText(mContext, "已加入下载列表", Toast.LENGTH_SHORT).show();
				AppConfig.getAppConfig(mContext).download(courseWare);
			}
		} else if (type.getNetType() == 1) {//wifi
			Toast.makeText(mContext, "已加入下载列表", Toast.LENGTH_SHORT).show();
			AppConfig.getAppConfig(mContext).download(courseWare);
		}
	}

	private int scrollWidth;

	public void setHight(int hight) {
		DisplayMetrics dm = new DisplayMetrics();
		mContext.getWindowManager().getDefaultDisplay().getMetrics(dm);
		int ScreenW = dm.widthPixels;
		int ScreenH = dm.heightPixels;
		LayoutParams p = new LayoutParams(ScreenW, hight);
		layer.setLayoutParams(p);
		if (ScreenW > ScreenH) {
			scrollWidth = ScreenW;
		} else {
			scrollWidth = ScreenH;
		}
	}

	public void setHight_land() {
		DisplayMetrics dm = new DisplayMetrics();
		mContext.getWindowManager().getDefaultDisplay().getMetrics(dm);
		int ScreenW = dm.widthPixels;
		int ScreenH = dm.heightPixels;
		LayoutParams p = new LayoutParams(ScreenW, ScreenH);
		layer.setLayoutParams(p);
	}

	public void setGone() {
		lock.setVisibility(View.GONE);
		right.setVisibility(View.GONE);
		ll_webView.setVisibility(View.GONE);
		lectureName.setText("讲义");
		mediacontroller_top_right.setVisibility(View.GONE);

		collect.setVisibility(View.VISIBLE);
		bottom_three.setVisibility(View.GONE);
		mini.setVisibility(View.VISIBLE);
		speed.setVisibility(View.GONE);

		setNoUsedGone();
	}

	public void setVisible() {
		lock.setVisibility(View.VISIBLE);
		mediacontroller_top_right.setVisibility(View.VISIBLE);
		right.setVisibility(View.VISIBLE);

		collect.setVisibility(View.GONE);
		bottom_three.setVisibility(View.VISIBLE);
		mini.setVisibility(View.GONE);
		speed.setVisibility(View.VISIBLE);

		setNoUsedGone();
	}


	private static final int SWITCH_VIDEO = 1;//切换视频

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		show();
		playActivity.setCanLoadHandOut(false);
		playActivity.playVedio(list.get(position), -1, position);
		playActivity.notifyPlayStatus();
		adapter.notifyDataSetChanged();
	}


	@Override
	public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
//		show();
//		playActivity.setCanLoadHandOut(false);
//		playActivity.playVedio(courseDetail.getMobileSectionList().get(groupPosition).getMobileCourseWareList().get(childPosition), groupPosition, childPosition);
//		playActivity.notifyPlayStatus();
//		courseListAdapter.notifyDataSetChanged();
		return false;
	}

	public void setIsClickable(boolean flag) {
		mPauseButton.setClickable(flag);
	}

	public boolean toExcise;

	/**
	 * 同步 wyc
	 **/
//     注册播放完成的监听
	private void loadData() {

		// 播放完成后
		videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

			@Override
			public void onPrepared(MediaPlayer player) {
				// TODO Auto-generated method stub
				//以下只需要第一次的时候用就可以
				updatePausePlay();
//                videoView.setBackgroundColor(Color.TRANSPARENT);
				mHandler.sendEmptyMessageDelayed(MSG_STUDY_LOG, TIME_STUDY_LOG);
//                playActivity.notifyPlayStatus();
				mHandler.sendEmptyMessageDelayed(POST_STUDYLOG, TIME_STUDY_LOG_SYNC);//上传studyLog
			}
		});

		// 播放完成后
		videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer player) {
					boolean isFinished = studyDB.isFinished(SharedPrefHelper.getInstance(mContext).getUserId(), courseWare.getCwId(), courseWare.getClassId());

//					if (playActivity.isHaveChapter()) {
//						int gourp = playActivity.getPlayingGroupPosition();
//						int child = playActivity.getPlayingChildPosition();
//
//						courseDetail.getMobileSectionList().get(gourp).getMobileCourseWareList().get(child).setIsPlayFinished(isFinished);
//
//						if (child < courseDetail.getMobileSectionList().get(gourp).getMobileCourseWareList().size() - 1) {
//							if (!courseDetail.getMobileSectionList().get(gourp).getMobileCourseWareList().get(child + 1).getCwId().equals("kehouzuoye")) {
//								if (NetworkUtil.isNetworkAvailable(playActivity)) {
//									playActivity.playVedio(courseDetail.getMobileSectionList().get(gourp).getMobileCourseWareList().get(child + 1), gourp, (child + 1));
//								} else {
//									boolean isDownload = isDownloaded(courseDetail.getMobileSectionList().get(gourp).getMobileCourseWareList().get(child + 1));
//									if (isDownload) {
//										playActivity.playVedio(courseDetail.getMobileSectionList().get(gourp).getMobileCourseWareList().get(child + 1), gourp, (child + 1));
//									}
//								}
//							}
//						} else if (gourp < courseDetail.getMobileSectionList().size() - 1) {
//							if (NetworkUtil.isNetworkAvailable(playActivity)) {
//								playActivity.playVedio(courseDetail.getMobileSectionList().get(gourp + 1).getMobileCourseWareList().get(0), (gourp + 1), 0);
//							} else {
//								boolean isDownload = isDownloaded(courseDetail.getMobileSectionList().get(gourp + 1).getMobileCourseWareList().get(0));
//								if (isDownload) {
//									playActivity.playVedio(courseDetail.getMobileSectionList().get(gourp + 1).getMobileCourseWareList().get(0), (gourp + 1), 0);
//								}
//							}
//						}
//					} else {
//						int postion = playActivity.getPlayingChildPosition();
//						list.get(postion).setIsPlayFinished(isFinished);
//						if (postion < list.size() - 1) {
//							if (NetworkUtil.isNetworkAvailable(playActivity)) {
//								playActivity.playVedio(list.get(postion + 1), -1, (postion + 1));
//							} else {
//								boolean isDownload = isDownloaded(list.get(postion + 1));
//								if (isDownload) {
//									playActivity.playVedio(list.get(postion + 1), -1, (postion + 1));
//								}
//							}
//						}
//					}
//					playActivity.notifyPlayStatus();

			}
		});

		videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
			@Override
			public boolean onError(MediaPlayer mp, int what, int extra) {
				long errorTime = mPlayer.getCurrentPosition();
				times = playActivity.times;
				if (times != null && times.size() > 0) {
					int errorCount = getErrorCount(times, errorTime);
					playActivity.analysisDNS(courseWare);
//                    operaDB.add(courseWare, "playError", playActivity.urls.get(errorCount), System.currentTimeMillis() + "");
				} else {
					playActivity.analysisDNS(courseWare);
				}

				return false;
			}
		});
	}

	private boolean isDownloaded(CourseWare cw) {
		boolean flag;
		CourseWare localCw = db.getDownloadedModel(SharedPrefHelper.getInstance(playActivity).getUserId(), cw.getClassId(), cw.getCwId());
		if (localCw != null) {
			if (localCw.getState() == Constants.STATE_DownLoaded) {
				flag = true;
			} else {
				flag = false;
			}
		} else {
			flag = false;
		}
		return flag;
	}

	private int getErrorCount(List<Integer> times, long errorTime) {
		if (times != null && times.size() > 0) {
			for (int i = 0; i < times.size(); i++) {
				errortotal = errortotal + times.get(i);
				if (errortotal > errorTime) {
					return i;
				}
			}
		}
		return -1;
	}

	private List<Integer> times;
	private int errortotal;
	private OperaDB operaDB;
}
