package io.vov.vitamio.widget.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bjjy.mainclient.phone.R;
import com.dongao.mainclient.model.bean.play.CourseWare;

import java.util.List;

/**
 * Created by dell on 2016/6/7.
 */
public class NoChapterAdapter extends BaseAdapter {

    private CourseWare courseWare;
    private List<CourseWare> list;
    private Context context;

    public NoChapterAdapter(List<CourseWare> list, CourseWare courseWare, Context context) {
        this.courseWare = courseWare;
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView==null){
            holder=new ViewHolder();
            convertView=View.inflate(context, R.layout.mediacontroller_chaper_item, null);
            holder.name=(TextView)convertView.findViewById(R.id.tv_name);
            convertView.setTag(holder);
        }else{
            holder=(ViewHolder)convertView.getTag();
        }

//        holder.name.setText(list.get(position).getChapterNo()+" "+list.get(position).getCwName());
        holder.name.setText(list.get(position).getCwName());
        if (list.get(position).getCwId().equals(courseWare.getCwId())) {
            holder.name.setTextColor(context.getResources().getColor(R.color.exam_list_error));
        } else {
            holder.name.setTextColor(context.getResources().getColor(R.color.white));
        }
        return convertView;
    }

    class ViewHolder {
        TextView name;
    }
}
