package com.bjjy.mainclient.persenter;

import com.bjjy.mainclient.phone.utils.NetworkUtil;
import com.bjjy.mainclient.phone.view.user.ForgetPswView;
import com.dongao.mainclient.core.util.StringUtil;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ParamsUtils;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by fengzongwei on 2016/10/19 0019.
 */
public class ForgetPswPersenter extends BasePersenter<ForgetPswView>{

    public boolean isGetValidNow = false;//当前是否正在获取验证码
	private String yanzhengma="";

    private static final String PHONE = "^1[3|4|5|8|7][0-9]\\d{8}$";

    public final String format = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{4,20}$";//密码格式
    private final int MAX_COUNT = 20;//密码长度限制

    public boolean isLoadingData = false;

    @Override
    public void getData() {
        if(isLoadingData)
            return;
        if (StringUtil.isEmpty(getMvpView().phoneNumber())) {
            getMvpView().showError("手机号不能为空");
            return;
        }
        if (StringUtil.isEmpty(getMvpView().valideCode())) {
            getMvpView().showError("验证码不能为空");
            return;
        }
//        if (!getMvpView().valideCode().equals(yanzhengma)) {
//            getMvpView().showError("验证码错误");
//            return;
//        }
        if(getMvpView().newPsw().length()<4 || getMvpView().newPsw().length()>20){
            getMvpView().showError("请输入4-20位且由数字和字母组成的密码");
            return;
        }else{
            if(!getMvpView().newPsw().matches(format)){
                getMvpView().showError("请输入4-20位且由数字和字母组成的密码");
                return;
            }
        }
        isLoadingData = true;
        apiModel.getData(ApiClient.getClient().forgetPsw(
                ParamsUtils.modifyPsw(getMvpView().phoneNumber(), getMvpView().newPsw(), getMvpView().valideCode())));
    }

    @Override
    public void setData(String obj) {
        isLoadingData = false;
        try {
            JSONObject object = new JSONObject(obj);
            if(object.getString("code").equals(Constants.RESULT_CODE+"")){
                getMvpView().showError("密码修改成功");
                getMvpView().resetSuccess();
            }else{
                getMvpView().showError(object.getString("msg"));
            }
        }catch (Exception e){
            e.printStackTrace();
            getMvpView().showError("修改失败");
        }
    }

    @Override
    public void onError(Exception e) {
        isLoadingData = false;
        super.onError(e);
    }

    /**
     * 获取验证码
     */
    public void getMobileValid(){
        if(isGetValidNow)
            return;
        if(StringUtil.isEmpty(getMvpView().phoneNumber())){
            getMvpView().showError("手机号不能为空");
            return;
        }else if(!StringUtil.isEmpty(getMvpView().phoneNumber()) && !getMvpView().phoneNumber().matches(PHONE)){
            getMvpView().showError("请输入正确的手机号");
            return;
        }
        if(NetworkUtil.isNetworkAvailable(getMvpView().context())){
            isGetValidNow = true;
            getMvpView().showLoading();
            Call<String> call = ApiClient.getClient().mobilevalid(ParamsUtils.modifyPswGetCode(getMvpView().phoneNumber()));
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    getMvpView().hideLoading();
                    if (response.isSuccessful()) {
                        isGetValidNow = false;
                        String str = response.body();
                        try{
                            JSONObject jo = new JSONObject(str);
                            int code = jo.getInt("code");
                            if(code== Constants.RESULT_CODE){
								yanzhengma=new JSONObject(jo.getString("body")).getString("PIN");
                                getMvpView().showError("验证码已发送");
                                getMvpView().switchBtStatus();
                            }else{
                                getMvpView().showError(jo.getString("msg"));
                            }
                        }catch (Exception e){
                            getMvpView().showError("获取验证码失败");
                        }

                    } else {
                        isGetValidNow = false;
                        getMvpView().showError("获取验证码失败");
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    isGetValidNow = false;
                    getMvpView().showError("发送验证码失败");
                }
            });
        }else{
            isGetValidNow = false;
            getMvpView().showError("当前没有网络");
        }

    }

    @Override
    public void resetRequestStatus() {
        super.resetRequestStatus();
        isGetValidNow = false;
        isLoadingData = false;
    }
}
