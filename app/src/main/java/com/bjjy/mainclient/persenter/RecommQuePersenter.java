package com.bjjy.mainclient.persenter;

import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.bean.question.Question;
import com.dongao.mainclient.model.remote.ApiClient;
import com.bjjy.mainclient.phone.view.question.RecommQueView;
import com.bjjy.mainclient.phone.view.question.utils.QuestionParserUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

/**
 * Created by fengzognwei on 2016/5/3 0003.
 * (废弃)
 */
public class RecommQuePersenter extends BasePersenter<RecommQueView>{
    private String examId,questionId,subjectId;
    public RecommQuePersenter(String examId,String questionId,String subjectId){
        this.examId = examId;
        this.questionId = questionId;
        this.subjectId = subjectId;
    }

    @Override
    public void getData() {
        HashMap<String,String> params = new HashMap<>();
        params.put("app","com.dongao.app.exam");
        params.put("appName","da-exam-app");
        params.put("deviceType","1");
        params.put("uniqueId","865291026605922");
        params.put("version","1.5.1");
        params.put("sign","7cdb1915f1c659cfdad60dd328ef51b6");
        params.put("userId","21878075");
        params.put("examId","711");
        params.put("questionId","535747");
        params.put("subjectId","71639");
        apiModel.getData(ApiClient.getClient().recommQuestion(params));
    }

    @Override
    public void setData(String obj) {
//        getMvpView().showData(QuestionParserUtil.parseQueArray());
        try{
            JSONObject object1 = new JSONObject(obj);
            if(object1.getJSONObject("result").getInt("code") == 1){
                if(object1.has("body")) {
                    JSONObject object = object1.getJSONObject("body");
                    if(object.has("questionAnswerList")) {
                        JSONArray array = object.getJSONArray("questionAnswerList");
                        List<Question> lisMyQuestionObjs = QuestionParserUtil.parseQueArray(array, true);
                        getMvpView().showData(lisMyQuestionObjs);
                    }else{
                        getMvpView().showError("暂无内容");
                    }
                }else{
                    getMvpView().showError("暂无内容");
                }
            }else{
                getMvpView().showError("更新失败，点击重试");
            }
        }catch (Exception e){
            getMvpView().showError("更新失败，点击重试");
            e.printStackTrace();
        }

    }

    @Override
    public void onError(Exception e) {
        getMvpView().showError("网络异常,点击重试");
    }
}
