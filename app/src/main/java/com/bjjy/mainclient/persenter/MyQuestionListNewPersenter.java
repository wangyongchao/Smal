package com.bjjy.mainclient.persenter;

import com.alibaba.fastjson.JSON;
import com.dongao.mainclient.model.bean.question.QuestionRecomm;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ParamsUtils;
import com.bjjy.mainclient.phone.utils.NetworkUtil;
import com.bjjy.mainclient.phone.view.classroom.question.MyQuestionListNewView;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by fengzongwei on 2016/12/9 0009.
 */
public class MyQuestionListNewPersenter extends BasePersenter<MyQuestionListNewView> {

    @Override
    public void getData() {
//        apiModel.getData(ApiClient.getClient().questionMyList
//                (ParamsUtils.questionMyList(SharedPrefHelper.getInstance(getMvpView().context()).getSubjectId(),
//                        getMvpView().getPage()+"",getMvpView().pageSize()+"")));
        if(!NetworkUtil.isNetworkAvailable(getMvpView().context())){
            getMvpView().showNetError();
            return;
        }
        getMvpView().showLoading();
        apiModel.getData(ApiClient.getClient().questionMyList
                (ParamsUtils.questionMyList(SharedPrefHelper.getInstance(getMvpView().context()).getSubjectId(),
                        getMvpView().getPage() + "", getMvpView().pageSize() + "")));
    }

    @Override
    public void setData(String obj) {
        try {
            JSONObject object = new JSONObject(obj);
            if(object.getString("code").equals("1000")){
                List<QuestionRecomm> questionRecomms = JSON.parseArray(object.getJSONObject("body").getString("qaVoList"), QuestionRecomm.class);
                if(getMvpView().getPage() == 0) {
                    if(questionRecomms.size()>0)
                        getMvpView().showMyQuesList(questionRecomms);
                    else
                        getMvpView().showNoData();
                }else
                    getMvpView().showMoreList(questionRecomms);
            }else if(object.getString("code").equals("1016") || object.getString("code").equals("1015")){
                getMvpView().showNoData();
            }else{
                if(getMvpView().getPage() == 0)
                    getMvpView().showError("请求错误");
                else
                    getMvpView().loadMoreFail();
            }
        }catch (Exception e){
            e.printStackTrace();
            if(getMvpView().getPage() == 0)
                getMvpView().showError("数据解析异常");
            else
                getMvpView().loadMoreFail();
        }
    }

    @Override
    public void onError(Exception e) {
        if(getMvpView().getPage() == 0)
            getMvpView().showError("连接服务器失败");
        else
            getMvpView().loadMoreFail();
    }

}
