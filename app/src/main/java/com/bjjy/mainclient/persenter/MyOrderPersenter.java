package com.bjjy.mainclient.persenter;

import com.alibaba.fastjson.JSON;
import com.bjjy.mainclient.phone.view.myorder.MyOrderView;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.bean.myorder.MyOrderItem;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.remote.ApiClient;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by fengzongwei on 2016/4/6.
 */
public class MyOrderPersenter extends BasePersenter<MyOrderView> {


    @Override
    public void getData() {
//        getMvpView().showLoading();
        HashMap<String,String> params = new HashMap<>();
        params.put("version","1.0");
        params.put("time",""+System.currentTimeMillis());
        params.put("random", "263198958da6fcc76509565ed6aa067f");
        params.put("reqSource","m");
        params.put("userId", SharedPrefHelper.getInstance(getMvpView().context()).getUserId()+"");
        params.put("qType",getMvpView().type());
        //TODO 23401045
//        params.put("params", "{" + "\"userId\"" + ":\""+SharedPrefHelper.getInstance(getMvpView().context()).getUserId()+"\"" + ",\"qType\"" + ":\"" + getMvpView().type() + "\"}");
        apiModel.getData(ApiClient.getClient().orderList(params));
    }

    @Override
    public void onError(Exception e) {
        super.onError(e);
//        getMvpView().hideLoading();
        getMvpView().showError("网络异常，点击重试");
    }

    @Override
    public void setData(String obj) {
        getMvpView().hideLoading();
        try{
            JSONObject object = new JSONObject(obj);
            if(object.getString("status")!=null && object.getString("status").equals("1000")){
                List<MyOrderItem> myOrderItems = JSON.parseArray(object.getString("result"), MyOrderItem.class);
                getMvpView().loadSuccess(myOrderItems);
            }else{
                getMvpView().showError("网络异常，点击重试");
            }
        }catch (Exception e){
            e.printStackTrace();
            getMvpView().showError("网络异常，点击重试");
        }
    }

    public void checkToken(){
        getMvpView().showLoading();
        HashMap<String,String> params = new HashMap<>();
        params.put("userId", SharedPrefHelper.getInstance(getMvpView().context()).getUserId()+"");
        params.put("accessToken", SharedPrefHelper.getInstance(getMvpView().context()).getAccessToken());
        Call<String> call = ApiClient.getClient().checkToken(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                getMvpView().hideLoading();
                if (response.isSuccessful()) {
                    String str = response.body();
                    try {
                        JSONObject jo = new JSONObject(str);
                        JSONObject object = jo.getJSONObject("verify_result");
                        int code = object.getInt("code");
                        if(code == 1){
                            getMvpView().deviceTokenSuccess();
                            return;
                        }else{
//                            getMvpView().deviceTokenFailed();
                            getMvpView().deviceTokenSuccess();
                        }
                    } catch (Exception e) {
                    }
                } else {
//                    getMvpView().deviceTokenFailed();
                    getMvpView().deviceTokenSuccess();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                getMvpView().hideLoading();
//                getMvpView().deviceTokenFailed();
                getMvpView().deviceTokenSuccess();
            }
        });
    }

}
