package com.bjjy.mainclient.persenter;

import android.os.Handler;
import android.os.Message;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.alibaba.fastjson.JSON;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.bean.daytest.DayExercise;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.DayTestDB;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ParamsUtils;
import com.bjjy.mainclient.phone.utils.NetworkUtil;
import com.bjjy.mainclient.phone.view.exam.bean.Option;
import com.bjjy.mainclient.phone.view.exam.bean.Question;
import com.bjjy.mainclient.phone.view.exam.bean.RelevantPoint;
import com.bjjy.mainclient.phone.view.exam.dict.ExamTypeEnum;
import com.bjjy.mainclient.phone.view.exam.fragment.QuestionFragmentView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by fengzongwei on 2016/5/27 0027.
 */
public class DayTestFragmentPersenter extends BasePersenter<QuestionFragmentView>{
    public Map<Integer, Integer> heightList=new HashMap<>();
    public int index;
    public Question question;
    private int exam_tag;
    private boolean isShowWebView=false;
    public String[] optionJudge = {"对", "错"};
    public int[] optionAnswer = {1, 2};
    public String[] optionChoice = {"A", "B", "C", "D", "E", "F", "G"};
    private int lvHeight=0;
    public static ArrayList<Question> questions = new ArrayList<>();
    private StringBuffer sb;
    private DayTestDB dayTestDB;
    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
        }
    };
    public DayExercise dayExercise;
    @Override
    public void getData() {
//                HashMap<String,String> params = new HashMap<>();
//                params.put("subjectId",);
////                params.put("currentDate",getMvpView().currentDate());
//                params.put("currentDate","2015-04-27");
        getMvpView().showLoading();
        if(NetworkUtil.isNetworkAvailable(getMvpView().context()))
            apiModel.getData(ApiClient.getClient().daytestQuestion(ParamsUtils.dayTestQuestion(getMvpView().currentSubjectId(),getMvpView().currentDate())));
    }


    public void checkDb(){
        dayExercise = null;
        dayExercise = dayTestDB.getTodayDayExercise(question.getExamId(),question.getSubjectId(),getMvpView().currentDate());
    }

    private void initData() {//初始化参数
        checkDb();
        if(dayExercise!=null){
            question.setUserAnswer(dayExercise.getAnswerLocal());
        }
        if (question.getPointList()==null||question.getPointList().size()==0){//相关知识点
            question.setPointList(new ArrayList<RelevantPoint>());
        }
        if (question.getChoiceType() == ExamTypeEnum.EXAM_TYPE_PANDUAN.getId()){//判断题
            if (question.getOptionList().size() <= 1){
                List<Option> opList = new ArrayList<>();
                for (int i = 0; i < 2; i++) {
                    Option option = new Option();
                    option.setOptionContent(optionJudge[i]);
                    option.setName(optionAnswer[i] + "");
                    opList.add(option);
                }
                question.setOptionList(opList);
            }
        }
        //判断当前的题是哪一种类型
        exam_tag = Constants.EXAM_TAG_ABILITY;//试卷类型  使用EXAM_TAG_ABILITY
        judgeHasSolutions();//是否有解题思路

        //答案选项判断，是否为webview
        if (question.getOptionList() != null && question.getOptionList().size() != 0) {
            for (int i = 0; i < question.getOptionList().size(); i++) {
                if (question.getOptionList().get(i).getShowWebView() != null && question.getOptionList().get(i).getShowWebView().equals("1")) {
                    isShowWebView = true;
                }
            }
        }
        getMvpView().setNSListviewIsWebview(isShowWebView);

        //判断当前是否是判断题
        if (question.getChoiceType() == ExamTypeEnum.EXAM_TYPE_PANDUAN.getId()) {
            getMvpView().setRealAnswer(true, true, optionJudge[Integer.valueOf(question.getRealAnswer().toString().trim()) - 1] + "");
            if (question.getUserAnswer() != null && !"".equals(question.getUserAnswer())) {
                String answer = optionJudge[Integer.parseInt(question.getUserAnswer().toString().trim()) - 1];
                getMvpView().setLocalAnswer(answer);
            }
        } else {
            //判断是否是单选多选不定项
            boolean isNormalChoiceType = (question.getChoiceType() == ExamTypeEnum.EXAM_TYPE_PANDUAN.getId() || question.getChoiceType() == ExamTypeEnum.EXAM_TYPE_DANXUAN.getId()
                    || question.getChoiceType() == ExamTypeEnum.EXAM_TYPE_DUOXUAN.getId() || question.getChoiceType() == ExamTypeEnum.EXAM_TYPE_BUDINGXIANG.getId());
            boolean isShowHtmlTextview = !(question.getRealAnswer().contains("</td>") || question.getRealAnswer().contains("<img"));
            getMvpView().setRealAnswer(isNormalChoiceType, isShowHtmlTextview, question.getRealAnswer());
            if (question.getUserAnswer() != null && !"".equals(question.getUserAnswer())) {
                getMvpView().setLocalAnswer(question.getUserAnswer() + "");
            }
        }
        //判断是否有试题详解
        boolean hasAnalyze=!(question.getQuizAnalyze()==null ||"".equals(question.getQuizAnalyze()));
//        getMvpView().showAnalyze(hasAnalyze);
        if (hasAnalyze){//如果有试题详解再显示
            boolean analyzeIsWebview = question.getQuizAnalyze().contains("</td>") || question.getQuizAnalyze().contains("<img");
            getMvpView().setAnalyzeIsWebview(analyzeIsWebview, question.getQuizAnalyze());
        }
        //相关知识点
        boolean showRelevant = !(question.getPointList() == null || question.getPointList().size() == 0);
        getMvpView().showRlevantPoint(showRelevant);
        judgeTypeForQuestion();
        if(dayExercise!=null){
            getMvpView().showAnalyze(true);
        }else{
            getMvpView().showAnalyze(false);
        }
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                getMvpView().hideLoading();
//            }
//        }, 500);

    }

    /**
     * 判断是否有解题思路
     */
    private void judgeHasSolutions() {
        if (question.getSolutions()==null || question.getSolutions().isEmpty()){
            getMvpView().showSolutions(false, "");
        }else{
            getMvpView().showSolutions(true, question.getSolutions());
        }
    }

    //判断试卷的类型
    private void judgeTypeForQuestion() {
        boolean titleIsWebview=(question.getTitle().contains("</td>")||question.getTitle().contains("<img"));
        hideAnalyze(); //是否隐藏答案及解析内容
        getMvpView().setQuestionTitleName(titleIsWebview,question.getTitle());
        getMvpView().setQuestionTypeName(titleIsWebview,ExamTypeEnum.getValue(question.getChoiceType()));
    }

    /**
     * 是否显示答案
     */
    public void hideAnalyze() {
        if (exam_tag == Constants.EXAM_TAG_REPORT) {
            getMvpView().setOptionListViewIsEnable(false);
//            getMvpView().setShowAllAnalyze(true);
            getMvpView().setNotNormalTipShow(false);

        } else if (exam_tag == Constants.EXAM_TAG_COLLECTION || exam_tag == Constants.EXAM_TAG_FALT) {
            getMvpView().setOptionListViewIsEnable(true);
            if (questions.contains(question)) {
//                getMvpView().setShowAllAnalyze(true);
                getMvpView().setNotNormalTipShow(false);
            } else {
//                getMvpView().setShowAllAnalyze(false);
                //判断当前的题是否是综合题还是普通题
                if (question.getChoiceType() == ExamTypeEnum.EXAM_TYPE_PANDUAN.getId() ||question.getChoiceType() == ExamTypeEnum.EXAM_TYPE_DANXUAN.getId()
                        ||question.getChoiceType() == ExamTypeEnum.EXAM_TYPE_DUOXUAN.getId() ||question.getChoiceType() == ExamTypeEnum.EXAM_TYPE_BUDINGXIANG.getId()){
                    getMvpView().setNotNormalTipShow(false);
                }else{
                    getMvpView().setNotNormalTipShow(true);
                }
            }
        } else if (exam_tag == Constants.EXAM_ORIGINAL_QUESTION) {//获取原题
            getMvpView().setOptionListViewIsEnable(true);
//            getMvpView().setShowAllAnalyze(true);
            getMvpView().setNotNormalTipShow(false);
            getMvpView().setRelavantAnswers(false);
        } else {
            getMvpView().setOptionListViewIsEnable(true);
        }
    }

    @Override
    public void setData(String obj) {
        try{
            JSONObject object = new JSONObject(obj);
            if(object.getJSONObject("result").getInt("code") == 1000){
                String questionStr = object.optString("body");
                if(questionStr!=null && !questionStr.equals("") && !questionStr.equals("{}")){
                    question = JSON.parseObject(object.getJSONObject("body").getString("question"),Question.class);
                    if(question!=null && question.getQuestionId()!=null && !question.getQuestionId().equals("")){
                        question.setExamId(getMvpView().currentExamid());
                        question.setSubjectId(getMvpView().currentSubjectId());
                        initData();
                        getMvpView().hideLoading();
                        getMvpView().showData();
                    }else{
                        getMvpView().showNoData();
                    }
                }else{
                    getMvpView().showNoData();
                }
            }else{
                getMvpView().showError("加载试题失败");
            }
        }catch (Exception e){
            e.printStackTrace();
            getMvpView().showError("加载试题失败");
        }
    }

    @Override
    public void attachView(QuestionFragmentView mvpView) {
        super.attachView(mvpView);
        dayTestDB = new DayTestDB(getMvpView().context());
    }

    /**
     * 计算listview的高度
     */
    public void setListViewHeightBasedOnChildren(ListView listView) {
        // 获取ListView对应的Adapter
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }
        int totalHeight = 0;
        int h = 0;
        for (Integer i : heightList.keySet()) {
            h+=heightList.get(i);
        }
        if (h> totalHeight){
            totalHeight=h;
        }
        totalHeight=totalHeight+140;
        lvHeight=totalHeight;
        //判断当前的题是否已经存了lv的高度,若计算过就直接设置
        if (question.getLvHeight()!=0){
            if (lvHeight==question.getLvHeight()){
            }else if(lvHeight<question.getLvHeight()){

                lvHeight=question.getLvHeight();
                ViewGroup.LayoutParams params = listView.getLayoutParams();
                params.height = question.getLvHeight()+ (listView.getDividerHeight() * (listAdapter.getCount() - 1))+50;
                listView.setLayoutParams(params);

            }else{
                question.setLvHeight(lvHeight);
                ViewGroup.LayoutParams params = listView.getLayoutParams();
                params.height = question.getLvHeight()+ (listView.getDividerHeight() * (listAdapter.getCount() - 1))+50;
                listView.setLayoutParams(params);
            }
        }else{
            if (heightList.size()!=4){
                return;
            }
            question.setLvHeight(totalHeight);
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalHeight+ (listView.getDividerHeight() * (listAdapter.getCount() - 1))+50;

            listView.setLayoutParams(params);
        }

    }

    public void setListViewItemClick(int position){
        if (question.getChoiceType() == ExamTypeEnum.EXAM_TYPE_DANXUAN.getId()) {//单选
            long[] ids = getMvpView().getOptionListView().getCheckedItemIds();
            sb = new StringBuffer();
            for (int i = 0; i < ids.length; i++) {
                long id1 = ids[i];
                sb.append(optionChoice[(int) id1]).append("");
            }
            question.setUserAnswer(sb.toString() + "");
            getMvpView().refreshOptionAdapter();
        }else if (question.getChoiceType() == ExamTypeEnum.EXAM_TYPE_DUOXUAN.getId() || question.getChoiceType() == ExamTypeEnum.EXAM_TYPE_BUDINGXIANG.getId()) {//多选
            sb = new StringBuffer();
            long[] ids = getMvpView().getOptionListView().getCheckedItemIds();
            for (int i = 0; i < ids.length; i++) {
                long id1 = ids[i];
                sb.append(optionChoice[(int) id1]).append(", ");
            }
            if (sb.toString().equals("") || sb.toString().length() == 1 || sb.toString().length() == 0) {
                question.setUserAnswer("");
            } else {

                question.setUserAnswer(sb.toString().substring(0, sb.toString().length() - 2) + "");
            }
            getMvpView().refreshOptionAdapter();
        }else if (question.getChoiceType() == ExamTypeEnum.EXAM_TYPE_PANDUAN.getId()) {//判断
            sb = new StringBuffer();
            long[] ids = getMvpView().getOptionListView().getCheckedItemIds();
            for (int i = 0; i < ids.length; i++) {
                long id1 = ids[i];
                sb.append(optionAnswer[(int) id1]).append("");
            }
            question.setUserAnswer(sb.toString() + "");
            getMvpView().refreshOptionAdapter();
        }
    }

    /**
     * 保存每日一练做题记录
     * @param dayExercise
     */
    public void insertDayTest(DayExercise dayExercise){
        dayTestDB.insert(dayExercise);
    }

    /**
     * 更新保存
     * @param dayExercise
     */
    public void updateDayTest(DayExercise dayExercise){
        dayTestDB.update(dayExercise);
    }

}
