package com.bjjy.mainclient.persenter;


import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.bean.courselect.Exams;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ParamsUtils;
import com.dongao.mainclient.model.remote.bean.BaseBean;
import com.bjjy.mainclient.phone.view.setting.views.FeedBackView;

import java.util.ArrayList;

/**
 * 登录UI
 */
public class FeedBackPersenter extends BasePersenter<FeedBackView> {
    private ArrayList<Exams> mList;

    @Override
    public void attachView(FeedBackView centerView) {
        super.attachView(centerView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    @Override
    public void getData() {
        getMvpView().showLoading();
        String content=getMvpView().getContent();
        apiModel.getData(ApiClient.getClient().feedBack(ParamsUtils.getInstance(getMvpView().context()).feedBack(content)));
    }

    @Override
    public void setData(String obj) {
        getMvpView().hideLoading();
        try {
            BaseBean baseBean = JSON.parseObject(obj, BaseBean.class);
            if (baseBean == null) {
                return;
            }
            if (baseBean.getCode() == 1000) {
                getMvpView().setData();
            } else {
                Toast.makeText(getMvpView().context(), "提交失败!", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
