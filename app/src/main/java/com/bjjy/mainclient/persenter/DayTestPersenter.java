package com.bjjy.mainclient.persenter;

import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.bean.daytest.DayExSelectedSubject;
import com.dongao.mainclient.model.bean.daytest.DayExercise;
import com.dongao.mainclient.model.bean.daytest.Excercise;
import com.dongao.mainclient.model.bean.daytest.OldSubject;
import com.dongao.mainclient.model.local.DayTestDB;
import com.dongao.mainclient.model.local.DayTestSelectSubjectDB;
import com.dongao.mainclient.model.local.ExcerciseDao;
import com.dongao.mainclient.model.local.OldDayTestSubjectDao;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.bjjy.mainclient.phone.view.daytest.DayTestView;

import java.util.List;

/**
 * Created by fengzongwei on 2016/5/24 0024.
 */
public class DayTestPersenter extends BasePersenter<DayTestView>{

    private DayTestSelectSubjectDB dayTestSelectSubjectDB;
    private List<DayExSelectedSubject> selectedSubjects;
    @Override
    public void attachView(DayTestView mvpView) {
        super.attachView(mvpView);
        dayTestSelectSubjectDB = new DayTestSelectSubjectDB(getMvpView().context());
    }

    @Override
    public void getData() {
        List<DayExSelectedSubject> subjects;
        if(SharedPrefHelper.getInstance(getMvpView().context()).isLogin())
            subjects = dayTestSelectSubjectDB.findAllSelectSubject(SharedPrefHelper.getInstance(getMvpView().context()).getUserId());
        else
            subjects = dayTestSelectSubjectDB.findAllSelectSubject("0");
        if(selectedSubjects == null){
            selectedSubjects = subjects;
            setData("");
        }else{
            /**
             * 判断打开课程选择页面之后的选课是否发生了改变，如果发生了改变则进行刷新
             */
            if(selectedSubjects.size()!=subjects.size()){
                selectedSubjects = subjects;
                setData("");
            }else{
                boolean isChanged = false;
                for(int i=0;i<selectedSubjects.size();i++){
                    boolean isHave = false;
                    for(int j=0;j<subjects.size();j++){
                        if(selectedSubjects.get(i).getDbId( ) == subjects.get(j).getDbId()){
                            isHave = true;
                            break;
                        }
                    }
                    if(!isHave){
                        isChanged = true;
                    }
                    if(isChanged){
                        break;
                    }
                }
                if(selectedSubjects!=null && selectedSubjects.size()==0){
                    isChanged = true;
                }
                if(isChanged){
                    selectedSubjects = subjects;
                    setData("");
                }
            }
        }

    }

    @Override
    public void setData(String obj) {
        getMvpView().showData(selectedSubjects);
    }

    /**
     *迁移每日一练数据
     */
    public void transFormDayTestData(){
        if(!SharedPrefHelper.getInstance(getMvpView().context()).getIsTransformUser()
                && SharedPrefHelper.getInstance(getMvpView().context()).isLogin()){
            //转换每日一练用户做题数据
            ExcerciseDao excerciseDao = new ExcerciseDao(getMvpView().context());
            DayTestDB dayTestDB = new DayTestDB(getMvpView().context());
            List<Excercise> excerciseList = excerciseDao.getAllExcercises();
            if(excerciseList!=null && excerciseList.size()>0){
                for(int i=0;i<excerciseList.size();i++){
                    if(excerciseList.get(i).getAnswerloacal()==null || excerciseList.get(i).getAnswerloacal().equals(""))
                        continue;
                    DayExercise dayExercise = new DayExercise();
                    dayExercise.setChoiceType(excerciseList.get(i).getChoiceType() + "");
                    dayExercise.setUserId(SharedPrefHelper.getInstance(getMvpView().context()).getUserId() + "");
                    String myAnswer = "";
                    String[] locals ;
                    if(excerciseList.get(i).getAnswerloacal().contains(",")){
                    //老版本库中多选时，本地答案在每个选项后面都加了“,”如（A,B,C,），
                    // 而正确答案则在最后一个选项后面没有“,”如（A,B,C）  所以要去掉一个“,”
                        locals = excerciseList.get(i).getAnswerloacal().split(",");
                        for(int j=0;j<locals.length;j++){
                            myAnswer = myAnswer + locals[j];
                            if(j!=(locals.length-1))
                                myAnswer = myAnswer+",";
                        }
                    }else{
                        myAnswer = excerciseList.get(i).getAnswerloacal();
                    }
                    if(excerciseList.get(i).getRealAnswer().equals(myAnswer)){
                        dayExercise.setAnswerRight("1");
                    }else{
                        dayExercise.setAnswerRight("2");
                    }
                    dayExercise.setDataTime(excerciseList.get(i).getDate());
//                    dayExercise.setExamId("666");老版本库中没有存exmaid
                    dayExercise.setSubjectId(excerciseList.get(i).getSubjectid() + "");
                    dayExercise.setQuestionId(excerciseList.get(i).getExcerciseid() + "");
                    dayExercise.setAnswerLocal(excerciseList.get(i).getAnswerloacal());
                    dayTestDB.insert(dayExercise);
                }
            }
            //转换每日一练科目定制数据
            OldDayTestSubjectDao oldDayTestSubjectDao = new OldDayTestSubjectDao(getMvpView().context());
            List<OldSubject> oldSubjectList = oldDayTestSubjectDao.getSubjects(getMvpView().context());//老版本保存的时候userId为写死的0
            if(oldSubjectList.size()>0){
                DayTestSelectSubjectDB dayTestSelectSubjectDB = new DayTestSelectSubjectDB(getMvpView().context());
                for(int i=0;i<oldSubjectList.size();i++){
                    DayExSelectedSubject dayExSelectedSubject = new DayExSelectedSubject();
                    dayExSelectedSubject.setExamId(oldSubjectList.get(i).getExamId()+"");
                    dayExSelectedSubject.setUserId(SharedPrefHelper.getInstance(getMvpView().context()).getUserId() + "");
                    dayExSelectedSubject.setSubjectId(oldSubjectList.get(i).getUid() + "");
                    dayExSelectedSubject.setSubjectName(oldSubjectList.get(i).getName());
                    dayTestSelectSubjectDB.insert(dayExSelectedSubject);
                }
            }
            SharedPrefHelper.getInstance(getMvpView().context()).setIsTransformUser(true);
        }
    }

}
