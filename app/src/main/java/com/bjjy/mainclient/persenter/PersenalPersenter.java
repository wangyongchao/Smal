package com.bjjy.mainclient.persenter;

import com.alibaba.fastjson.JSON;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.bean.user.UserInfo;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ParamsUtils;
import com.bjjy.mainclient.phone.utils.NetworkUtil;
import com.bjjy.mainclient.phone.view.persenal.PersenalView;

import org.json.JSONObject;

/**
 * Created by fengzongwei on 2016/6/20 0020.
 */
public class PersenalPersenter extends BasePersenter<PersenalView> {

    @Override
    public void getData() {
        getMvpView().show(null);
//        if(NetworkUtil.isNetworkAvailable(getMvpView().context())){
//            apiModel.getData(ApiClient.getClient().myUserInfo(ParamsUtils.persenal()));
//        }else {
//            getMvpView().showError("网络无连接");
//        }

    }

    @Override
    public void setData(String obj) {
        try{
            JSONObject object = new JSONObject(obj);
            if(object.getInt("code") == 1000){
                UserInfo userInfo = JSON.parseObject(object.getJSONObject("body").getString("user"),UserInfo.class);
                SharedPrefHelper.getInstance(getMvpView().context()).setUserInfo(object.getJSONObject("body").getString("user"));
                getMvpView().show(userInfo);
            }else {
                getMvpView().showError("请求错误");
            }
        }catch (Exception e){
            e.printStackTrace();
            getMvpView().showError("加载信息失败");
        }
    }



}
