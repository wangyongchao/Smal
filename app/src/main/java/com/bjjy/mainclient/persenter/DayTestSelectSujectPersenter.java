package com.bjjy.mainclient.persenter;

import com.alibaba.fastjson.JSON;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.bean.daytest.DayExSelectedSubject;
import com.dongao.mainclient.model.bean.daytest.DayTestExam;
import com.dongao.mainclient.model.local.DayTestSelectSubjectDB;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ParamsUtils;
import com.bjjy.mainclient.phone.utils.NetworkUtil;
import com.bjjy.mainclient.phone.view.daytest.SelectSubjectView;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by fengzongwei on 2016/5/20 0020.
 */
public class DayTestSelectSujectPersenter extends BasePersenter<SelectSubjectView>{

    private DayTestSelectSubjectDB dayTestSelectSubjectDB;

    private List<DayTestExam> dayTestExamList;

    private boolean isLogin = false;

    @Override
    public void attachView(SelectSubjectView mvpView) {
        super.attachView(mvpView);
        dayTestSelectSubjectDB = new DayTestSelectSubjectDB(getMvpView().context());
        isLogin = SharedPrefHelper.getInstance(getMvpView().context()).isLogin();
    }

    @Override
    public void getData() {
        getMvpView().showLoading();
        if(NetworkUtil.isNetworkAvailable(getMvpView().context())){
            apiModel.getData(ApiClient.getClient().daytestsubjectlist(ParamsUtils.commonSignPramas()));
        }else{
            String json = SharedPrefHelper.getInstance(getMvpView().context()).getDayTestSubjectListJson();
            if(json!=null && !json.equals("")){
                dayTestExamList = JSON.parseArray
                        (json,DayTestExam.class);
                preData();
                getMvpView().showData(dayTestExamList);
            }else{
                getMvpView().showError("");
            }
        }
    }

    @Override
    public void setData(String obj) {
        try{
            JSONObject object = new JSONObject(obj);
            int code = object.getJSONObject("result").getInt("code");
            if(code == 1000){
                dayTestExamList = JSON.parseArray(object.getJSONObject("body").getString("examList"),DayTestExam.class);
                preData();
                SharedPrefHelper.getInstance(getMvpView().context()).setDayTestSubjectListJson
                        (object.getJSONObject("body").getString("examList"));
                getMvpView().showData(dayTestExamList);
                getMvpView().hideLoading();
            }else{
                getMvpView().showError("加载数据失败");
            }
        }catch (Exception e){
            e.printStackTrace();
            getMvpView().showError("加载数据失败");
        }

    }

    /**
     * 数据显示前的准备工作
     */
    private void preData(){
        for(int i=0;i<dayTestExamList.size();i++){
            for(int j=0;j<dayTestExamList.get(i).getSubjectList().size();j++){
                if(isLogin)
                dayTestExamList.get(i).getSubjectList().get(j).
                        setUserId(SharedPrefHelper.getInstance(getMvpView().context()).getUserId() + "");
                else
                    dayTestExamList.get(i).getSubjectList().get(j).setUserId("0");
                dayTestExamList.get(i).getSubjectList().get(j).setExamId(dayTestExamList.get(i).getExamId());
            }
        }
        for(int i=0;i<dayTestExamList.size();i++){
            for(int j=0;j<dayTestExamList.get(i).getSubjectList().size();j++){
                if(dayTestSelectSubjectDB.getSelectedSubject(dayTestExamList.get(i).getSubjectList().get(j))!=null){
                    dayTestExamList.get(i).getSubjectList().get(j).setSelectedTag(1);
                }
            }
        }
    }

    /**
     * 获取数据库中保存的已选科目
     * @return
     */
    public List<DayExSelectedSubject> getSelectableSubject(){
        return dayTestSelectSubjectDB.findAllSelectSubject(SharedPrefHelper.getInstance(getMvpView().context()).getUserId());
    }

    /**
     * 保存选择的课程
     */
    public void savaSelect(){
        dayTestSelectSubjectDB.deleteAll();
        for(int i=0;i<dayTestExamList.size();i++){
            for(int j=0;j<dayTestExamList.get(i).getSubjectList().size();j++){
                if(dayTestExamList.get(i).getSubjectList().get(j).getSelectedTag() == 1){
                    dayTestSelectSubjectDB.insert(dayTestExamList.get(i).getSubjectList().get(j));
                }
            }
        }
    }

}
