package com.bjjy.mainclient.persenter;

import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ParamsUtils;
import com.bjjy.mainclient.phone.view.question.QustionInputView;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by fengzongwei on 2016/12/5 0005.
 */
public class QuestionInputPersenter extends BasePersenter<QustionInputView> {

    @Override
    public void getData() {
        apiModel.getData(ApiClient.getClient().questionModify
                (ParamsUtils.questionModify(getMvpView().getQuestionId(), getMvpView().getQaInfoId(),
                        getMvpView().title(), getMvpView().content())));
    }

    @Override
    public void setData(String obj) {
        try{
            JSONObject object = new JSONObject(obj);
            if(object.getString("code").equals("1000")){
                getMvpView().modifySuccess();
            }else{
                getMvpView().showError(object.getString("msg"));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onError(Exception e) {
        super.onError(e);
        getMvpView().showError("连接服务器异常，请重试！");
    }

    public void zhuiwen(){
        Call<String> call = ApiClient.getClient().questionZhui(
                ParamsUtils.questionZhui(getMvpView().getQuestionId(), getMvpView().content()));
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try{
                    JSONObject object = new JSONObject(response.body());
                    if(object.getString("code").equals("1000")){
                        getMvpView().zhuiSuccess();
                    }else{
                        getMvpView().showError(object.getString("msg"));
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                getMvpView().showError("连接服务器异常，请重试！");
            }
        });
    }

}
