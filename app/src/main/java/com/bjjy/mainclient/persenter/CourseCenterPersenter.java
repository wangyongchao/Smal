package com.bjjy.mainclient.persenter;


import android.text.TextUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bjjy.mainclient.phone.view.goodsubject.views.CourseCenterView;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.bean.courselect.Exams;
import com.dongao.mainclient.model.remote.ApiClient;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * 登录UI
 */
public class CourseCenterPersenter extends BasePersenter<CourseCenterView> {
    private ArrayList<Exams> mList ;

    @Override
    public void attachView(CourseCenterView centerView) {
        super.attachView(centerView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    @Override
    public void getData(){
        getMvpView().showLoading();
//        String accessToken = SharedPrefHelper.getInstance(getMvpView().context()).getAccessToken();
        HashMap<String,String> params = new HashMap<>();
        params.put("time","");
        params.put("random","");
        params.put("reqSource","");
        apiModel.getData(ApiClient.getClient().getExamList(params));

    }

    @Override
    public void setData(String obj) {
        try {
//            BaseBean baseBean = JSON.parseObject(obj,BaseBean.class);
            if(TextUtils.isEmpty(obj)){
                getMvpView().showError("");
                return;
            }
            JSONObject json=JSON.parseObject(obj);
            String result=json.getString("result");
            mList = (ArrayList)JSON.parseArray(result,Exams.class);

        } catch (Exception e) {
            e.printStackTrace();
        }

        getMvpView().setAdapter(mList);
        //courseView.refresh();
    }
}
