package com.bjjy.mainclient.persenter;

import com.alibaba.fastjson.JSON;
import com.bjjy.mainclient.phone.view.question.QuestionDetailView;
import com.dongao.mainclient.model.bean.question.QuestionDetail;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ParamsUtils;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by fengzongwei on 2016/12/1 0001.
 */
public class QuestionDetailPersenter extends BasePersenter<QuestionDetailView> {

    public QuestionDetail questionDetail;

    @Override
    public void getData() {
        apiModel.getData(ApiClient.getClient().questionDetail(ParamsUtils.questionDetail(getMvpView().getQuestionId())));
//        apiModel.getData(ApiClient.getClient().questionDetail(ParamsUtils.questionDetail("")));
    }

    @Override
    public void setData(String obj) {
        try {
            JSONObject object = new JSONObject(obj);
            if(object.getInt("code") == 1000) {
                questionDetail = JSON.parseObject(object.getString("body"), QuestionDetail.class);
                getMvpView().showQuestionDetail(questionDetail);
            }else{
                questionDetail = null;
                getMvpView().showError("暂无内容");
            }
        }catch (Exception e){
            e.printStackTrace();
            questionDetail = null;
            getMvpView().showError("数据加载异常");
        }
    }

    @Override
    public void onError(Exception e) {
        super.onError(e);
        questionDetail = null;
        getMvpView().showError("服务器连接失败");
    }

    public void deleteQuestion(String questionId,boolean isHaveZhui){
        Call<String> call;
//        if(!isHaveZhui){
//            call = ApiClient.getClient().questionDeleteNone(ParamsUtils.questionDelete(questionId));
//        }else{
//            call = ApiClient.getClient().questionDelete(ParamsUtils.questionDelete(questionId));
//        }
        call = ApiClient.getClient().questionDeleteNew(ParamsUtils.questionDelete(questionId));
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    JSONObject object = new JSONObject(response.body());
                    if(object.getInt("code") == 1000){
                        getMvpView().deleteSuccess();
                    }else{
                        getMvpView().deleteFail();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    getMvpView().deleteFail();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                getMvpView().deleteFail();
            }
        });
    }


}
