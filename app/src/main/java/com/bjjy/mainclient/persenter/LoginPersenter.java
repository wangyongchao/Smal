package com.bjjy.mainclient.persenter;



import com.bjjy.mainclient.phone.view.user.LoginView;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.remote.ParamsUtils;
import com.dongao.mainclient.core.util.StringUtil;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.bean.user.User;
import com.dongao.mainclient.model.local.UserDB;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 登录UI
 */
public class LoginPersenter extends BasePersenter<LoginView> {

    @Override
    public void attachView(LoginView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    @Override
    public void getData() {
        // 判断空
        if (StringUtil.isEmpty(getMvpView().username())) {
            getMvpView().showError("用户名不能为空");
            return;
        }
        if (StringUtil.isEmpty(getMvpView().password())) {
            getMvpView().showError("密码不能为空");
            return;
        }
        getMvpView().showLoading();
        apiModel.getData(ApiClient.getClient().login(ParamsUtils.getInstance(getMvpView().context()).login(getMvpView().username(),getMvpView().password())));
    }

    @Override
    public void setData(String str){
        try {
            JSONObject jo = new JSONObject(str);
            int result = jo.getInt("resultCode");

            if (result == 0) {
                getMvpView().showError("用户名或者密码错误");
                return;
            }
            String uid = jo.getString("userId");
            String accessToken = jo.optString("accessToken");

            //报错到数据库
            User user = new User();
            user.setUserId(uid);
            user.setUsername(getMvpView().username());
            user.setPassword(getMvpView().password());
            user.setAccessToken(accessToken);
            UserDB userDB = new UserDB(getMvpView().context());
            userDB.insert(user);

            //报错到SharePref.
            SharedPrefHelper.getInstance(getMvpView().context())
                    .setAccessToken(accessToken);
            SharedPrefHelper.getInstance(getMvpView().context())
                    .setLoginUsername(getMvpView().username());
            SharedPrefHelper.getInstance(getMvpView().context())
                    .setLoginPassword(getMvpView().password());
            SharedPrefHelper.getInstance(getMvpView().context())
                    .setUserId(uid);
            getMvpView().intent();
        } catch (JSONException e) {
            // TODO Auto-generated catch block

        }
    }
    @Override
    public void onError(Exception e) {
        super.onError(e);
        getMvpView().showError("网络异常，登录失败");
    }
}
