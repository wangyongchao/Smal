package com.bjjy.mainclient.persenter;

import android.widget.Toast;

import com.bjjy.mainclient.phone.view.user.RegisteView;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.core.util.StringUtil;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ParamsUtils;
import com.dongao.mainclient.model.remote.SignUtils;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by fengzongwei on 2016/4/5.
 */
public class RegisterPersenter extends BasePersenter<RegisteView> {

    private static final String PHONE = "^1[3|4|5|8|7][0-9]\\d{8}$";
    private static final String RESEND = "重新发送";
    @Override
    public void attachView(RegisteView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    @Override
    public void getData() {
        if(getMvpView().phoneNumber()==null || !getMvpView().phoneNumber().matches(PHONE) || getMvpView().phoneNumber().equals("")) {
            getMvpView().showError("请输入正确的手机号");
            return;
        }
        if(getMvpView().checkNumber()==null || getMvpView().checkNumber().equals("")){
            getMvpView().showError("请输入验证码");
            return;
        }
        if(!getMvpView().isReadRuleAndAccess()){
            getMvpView().showError("请先阅读并接受条款");
            return;
        }
        if(getMvpView().psw()==null || getMvpView().psw().equals("")){
            getMvpView().showError("请输入密码");
            return;
        }
        getMvpView().showLoading();
        HashMap<String,String> params = new HashMap<>();
        params.put("userName", getMvpView().phoneNumber());
        params.put("password", getMvpView().psw());
        params.put("phoneNumber", getMvpView().phoneNumber());
        params.put("email", "");
        params.put("mobileCode", getMvpView().checkNumber());
        apiModel.getData(ApiClient.getClient().register(params));
    }

    @Override
    public void onError(Exception e) {
        super.onError(e);

    }

    @Override
    public void setData(String obj) {
        try {
            JSONObject jo = new JSONObject(obj);
            int result = jo.getInt("result");
            int reason = jo.getInt("reason");
            String notify = "";
            if(reason == 1){
                notify = "该手机号已经被注册";
                //Toast.makeText(RegisterActivity.this, R.string.userexist, Toast.LENGTH_LONG).show();
                //return;
            }else if(reason == 3){
                notify = "验证码已失效，请重新发送";
                //Toast.makeText(RegisterActivity.this, R.string.codetimeout, Toast.LENGTH_LONG).show();
                //return;
            }else if(reason == 4){//中文用户名不能出现敏感词汇
                notify = "用户名中有敏感词汇";
                //Toast.makeText(RegisterActivity.this, R.string.codetimeout, Toast.LENGTH_LONG).show();
                //return;
            }else if(result != 1 ){
                notify = "注册失败";
                //Toast.makeText(RegisterActivity.this, R.string.registerfail, Toast.LENGTH_LONG).show();
                //return;
            }
            if(!notify.equals("")){
                getMvpView().showError(notify);
                return;
            }else{
                Toast.makeText(getMvpView().context(),"注册成功",Toast.LENGTH_SHORT).show();
                getMvpView().intent();
            }
        } catch (JSONException e) {
            getMvpView().showError("发送验证码失败");
        }
    }

    /**
     * 获取验证码
     */
    public void getMobileValid(){
        if(StringUtil.isEmpty(getMvpView().phoneNumber())){
            getMvpView().showError("手机号不能为空");
            return;
        }else if(!StringUtil.isEmpty(getMvpView().phoneNumber()) && !getMvpView().phoneNumber().matches(PHONE)){
            getMvpView().showError("请输入正确的手机号");
            return;
        }

        getMvpView().showLoading();
        HashMap<String,String> params = new HashMap<>();
        params.put("phoneNum",getMvpView().phoneNumber());
        params.put("userName",getMvpView().phoneNumber());
        params.put("sendType","1");
        params.put("contentType","10");
        params.putAll(ParamsUtils.commonURL());
        params.put("sign", SignUtils.sign(params));
        Call<String> call = ApiClient.getClient().mobilevalid(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    String str = response.body();
                    try{
                        JSONObject jo = new JSONObject(str);
                        int result = jo.getInt("result");
                        if(result==0){
                            getMvpView().showError("该手机号码已注册");
                            return;
                        }
                        getMvpView().showError("验证码已发送");
                        getMvpView().switchBtStatus();
                    }catch (Exception e){

                    }

                } else {
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                getMvpView().showError("发送验证码失败");
            }
        });
    }

}
