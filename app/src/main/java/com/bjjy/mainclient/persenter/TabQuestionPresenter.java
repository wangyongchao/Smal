package com.bjjy.mainclient.persenter;

import com.alibaba.fastjson.JSON;
import com.bjjy.mainclient.phone.view.question.TabQuestionView;
import com.dongao.mainclient.model.bean.question.QuestionKonwLedge;
import com.dongao.mainclient.model.bean.question.QuestionRecomm;
import com.dongao.mainclient.model.bean.question.QustionBook;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ParamsUtils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by fengzongwei on 2016/12/1 0001.
 */
public class TabQuestionPresenter extends BasePersenter<TabQuestionView> {

    private boolean isLoadKnowNow = false;

    @Override
    public void attachView(TabQuestionView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void getData() {
//        apiModel.getData(ApiClient.getClient().questionBookList
//                (ParamsUtils.questionBookList(SharedPrefHelper.getInstance(getMvpView().context()).getSubjectId(),"2")));
        apiModel.getData(ApiClient.getClient().questionBookList
                (ParamsUtils.questionBookList(SharedPrefHelper.getInstance(getMvpView().context()).getSubjectId(), "1,2")));
    }

    @Override
    public void setData(String obj) {
        try {
            JSONObject object = new JSONObject(obj);
            if(object.getString("code").equals("1000")){
                List<QustionBook> qustionBooks = JSON.parseArray(object.getString("body"), QustionBook.class);
                getMvpView().showBookList(qustionBooks);
            }else{
                getMvpView().bookListLoadError();
            }
        }catch (Exception e){
            e.printStackTrace();
            getMvpView().bookListLoadError();
        }
    }

    @Override
    public void onError(Exception e) {
        super.onError(e);
        getMvpView().bookListLoadError();
    }

    public void getCanChoiceKnowLedgePointList(String bookId,String pageNum){
        if(isLoadKnowNow)
            return;
        isLoadKnowNow = true;
        String subjectId = SharedPrefHelper.getInstance(getMvpView().context()).getSubjectId();
        Call<String> call = ApiClient.getClient().questionKnowLedgePointList
                (ParamsUtils.questionKnowLedgePointList(subjectId, bookId, pageNum));
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                isLoadKnowNow = false;
                String obj = response.body();
                try {
                    JSONObject object = new JSONObject(obj);
                    if (object.getString("code").equals("1000")) {
                        List<QuestionKonwLedge> questionKonwLedges = new ArrayList<QuestionKonwLedge>();
                        List<QuestionKonwLedge> questionKonwLedges_question = JSON.parseArray(object.getJSONObject("body").getString("questionRes"), QuestionKonwLedge.class);
                        for(int i=0;i<questionKonwLedges_question.size();i++){
                            questionKonwLedges_question.get(i).setType(1);
                        }
                        List<QuestionKonwLedge> questionKonwLedges_knowledge = JSON.parseArray(object.getJSONObject("body").getString("knowledgeRes"), QuestionKonwLedge.class);
                        for(int i=0;i<questionKonwLedges_knowledge.size();i++){
                            questionKonwLedges_knowledge.get(i).setType(0);
                        }
                        questionKonwLedges.addAll(questionKonwLedges_question);
                        questionKonwLedges.addAll(questionKonwLedges_knowledge);
                        getMvpView().showKnowLedgeList(questionKonwLedges);
                    } else {
                        getMvpView().knowledgepointLoadError();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    getMvpView().knowledgepointLoadError();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                isLoadKnowNow = false;
                getMvpView().knowledgepointLoadError();
            }
        });
    }

    public void getRecommQuesList(QuestionKonwLedge questionKonwLedge){
//        Call<String> call = ApiClient.getClient().questionRecommList
//                (ParamsUtils.questionRecommList("2","0",bookQaId,""));
        Call<String> call = ApiClient.getClient().questionRecommList
                (ParamsUtils.questionRecommList("2", questionKonwLedge.getType()+"", questionKonwLedge.getId(), ""));
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                String obj = response.body();
                try {
                    JSONObject object = new JSONObject(obj);
                    if(object.getString("code").equals("1000")){
                        List<QuestionRecomm> questionRecomms =
                                JSON.parseArray(object.getString("body"), QuestionRecomm.class);
                        getMvpView().showRecommQuesList(questionRecomms);
                    }else{
                        getMvpView().recommQuesListLoadError();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    getMvpView().recommQuesListLoadError();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                getMvpView().recommQuesListLoadError();
            }
        });
    }

}
