package com.bjjy.mainclient.persenter;

import android.os.Bundle;

import com.bjjy.mainclient.phone.view.question.AddActivityNewView;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ParamsUtils;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by dell on 2016/12/7 0007.
 */
public class AddQuestionNewPersenter extends BasePersenter<AddActivityNewView> {

    @Override
    public void getData() {
        if(isPostDataNow)
            return;
        isPostDataNow = true;
        if(getMvpView().type() == 0){//图书答疑
            Bundle bundle = getMvpView().getIntentBundle();
            apiModel.getData(ApiClient.getClient().questionAdd(ParamsUtils.questionAddBook
                    (getMvpView().title(), getMvpView().content(), bundle.getString("bookId"),
                            bundle.getString("bookName"), bundle.getString("pageNum"), bundle.getString("bookQaType"),
                            bundle.getString("bookQaId"), bundle.getString("chapterIds"), bundle.getString("questionNum"))));
        }
    }

    @Override
    public void setData(String obj) {
        try {
            JSONObject object = new JSONObject(obj);
            if(object.getInt("code") == 1000){
                getMvpView().addSuccess();
            }else{
                getMvpView().showError(object.getString("msg"));
            }
        }catch (Exception e){
            isPostDataNow = false;
            e.printStackTrace();
            getMvpView().showError("提交失败，请重试");
        }
    }

    @Override
    public void onError(Exception e) {
        isPostDataNow = false;
        getMvpView().showError("提交失败，请重试");
    }


    public void examQuestionAdd(){
        if(isPostDataNow)
            return;
        isPostDataNow = true;
        Bundle bundle = getMvpView().getIntentBundle();
        Call<String> call = ApiClient.getClient().examQuestionAdd(
                ParamsUtils.examQuestionAdd(bundle.getString("examinationId"),bundle.getString("examquestionId"),bundle.getString("paperName"),bundle.getString("largeSegmentName")
                ,bundle.getString("subsectionName"),getMvpView().title(),getMvpView().content()));
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    JSONObject object = new JSONObject(response.body());
                    if(object.getInt("code") == 1000){
                        getMvpView().addSuccess();
                    }else{
                        getMvpView().showError("提交失败");
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    getMvpView().showError("提交失败");
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                isPostDataNow = false;
                getMvpView().showError("提交失败");
            }
        });
    }

}
