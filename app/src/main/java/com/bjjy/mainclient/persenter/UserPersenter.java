package com.bjjy.mainclient.persenter;

import android.widget.Toast;

import com.dongao.mainclient.core.util.StringUtil;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ParamsUtils;
import com.bjjy.mainclient.phone.event.LoginSuccessEvent;
import com.bjjy.mainclient.phone.utils.NetworkUtil;
import com.bjjy.mainclient.phone.view.user.LoginNewActivity;
import com.bjjy.mainclient.phone.view.user.UserView;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by fengzongwei on 2016/5/6 0006.
 */
public class UserPersenter extends BasePersenter<UserView>{

    private static final String PHONE = "^1[3|4|5|8|7][0-9]\\d{8}$";
    public final String format = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{4,20}$";//密码格式
    private static final String RESEND = "重新发送";

    private boolean isLoadingData = false;
    private boolean isGetValidNow = false;//当前是否正在获取验证码
    //登录
    @Override
    public void getData() {
//        if(isLoadingData)
//            return;
        // 判断空
        if(!NetworkUtil.isNetworkAvailable(getMvpView().context())){
            getMvpView().showError("当前无网络连接");
            return;
        }
        if (StringUtil.isEmpty(getMvpView().username())) {
            getMvpView().showError("用户名不能为空");
            return;
        }
        if (StringUtil.isEmpty(getMvpView().password())) {
            getMvpView().showError("密码不能为空");
            return;
        }
        isLoadingData = true;
        getMvpView().showLoading();
        if(NetworkUtil.isNetworkAvailable(getMvpView().context()))
            apiModel.getData(ApiClient.getClient().login(ParamsUtils.login(getMvpView().username(),getMvpView().password())));
        else
            getMvpView().showError("当前没有网络");
    }



    @Override
    public void setData(String obj) {
        getMvpView().hideLoading();
        isLoadingData = false;
        try {
            JSONObject jo = new JSONObject(obj);
            int result = jo.getInt("code");
            if(result!=Constants.RESULT_CODE){
                getMvpView().showError(jo.getString("msg"));
                return;
            }
            String userId = jo.getJSONObject("body").optString("uid");
            String accessToken = jo.getJSONObject("body").optString("mobileAccessToken");
            String periodId = jo.getJSONObject("body").optString("periodId");
            String partnerId = jo.getJSONObject("body").optString("partnerId");
            String userCode=jo.getJSONObject("body").optString("userCode");
            String userImage=jo.getJSONObject("body").optString("headImage");
            String skillGrade=jo.getJSONObject("body").optString("skillGrade");
            String accountantNoDate="";
            if ( jo.getJSONObject("body").has("accountantNoDate")){
                accountantNoDate=jo.getJSONObject("body").optString("accountantNoDate");
            }
            
            SharedPrefHelper.getInstance(getMvpView().context())
                    .setAccessToken(accessToken);
            SharedPrefHelper.getInstance(getMvpView().context())
                    .setSkillGrade(skillGrade);
            SharedPrefHelper.getInstance(getMvpView().context())
                    .setAccountantNoDate(accountantNoDate);
            SharedPrefHelper.getInstance(getMvpView().context())
                    .setUserId(userId);
            SharedPrefHelper.getInstance(getMvpView().context())
                    .setUserImage(userImage);
            SharedPrefHelper.getInstance(getMvpView().context())
                    .setUserCode(userCode);
            SharedPrefHelper.getInstance(getMvpView().context())
                    .setLoginUsername(getMvpView().username());
            SharedPrefHelper.getInstance(getMvpView().context())
                    .setLoginPassword(getMvpView().password());
            SharedPrefHelper.getInstance(getMvpView().context()).setIsLogin(true);
            SharedPrefHelper.getInstance(getMvpView().context()).setPartnerId(partnerId);
            SharedPrefHelper.getInstance(getMvpView().context()).setPeriodId(periodId);
            
            LoginNewActivity loginNewActivity = (LoginNewActivity)getMvpView().context();
            EventBus.getDefault().post(new LoginSuccessEvent(loginNewActivity.getIntent().getStringExtra(Constants.LOGIN_TYPE_KEY)));
            getMvpView().loginSuccess();
        } catch (JSONException e) {
            e.printStackTrace();
            getMvpView().showError("登录失败");
        }
    }

    private void praseNew(JSONObject jo) throws JSONException {
        String userId = jo.getJSONObject("body").optString("uid");
    }

    @Override
    public void onError(Exception e) {
        isLoadingData = false;
        getMvpView().showError("登录失败");
    }

    public void regitse(){
        if(!NetworkUtil.isNetworkAvailable(getMvpView().context())){
            getMvpView().showError("当前无网络连接");
            return;
        }
        if(getMvpView().phoneNumber()==null || !getMvpView().phoneNumber().matches(PHONE) || getMvpView().phoneNumber().equals("")) {
            getMvpView().showError("请输入正确的手机号");
            return;
        }
        if(getMvpView().checkNumber()==null || getMvpView().checkNumber().equals("")){
            getMvpView().showError("请输入验证码");
            return;
        }
        if(getMvpView().pswRegiste()==null || getMvpView().pswRegiste().equals("") ||
                (getMvpView().pswRegiste()!=null && getMvpView().pswRegiste().length()<4)
                || (getMvpView().pswRegiste()!=null && getMvpView().pswRegiste().length()>20)){
            getMvpView().showError("请输入4-20位且由数字和字母组成的密码");
            return;
        }else{
            if(!getMvpView().pswRegiste().matches(format)){
                getMvpView().showError("请输入4-20位且由数字和字母组成的密码");
                return;
            }
        }
        if(NetworkUtil.isNetworkAvailable(getMvpView().context())){
            getMvpView().showLoading();
            Call<String> call = ApiClient.getClient().register(ParamsUtils.registe(getMvpView().pswRegiste(),getMvpView().phoneNumber(),getMvpView().checkNumber()));
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    getMvpView().hideLoading();
                    try {
                        String obj = response.body();
                        JSONObject jo = new JSONObject(obj);
                        int status = jo.getInt("code");
                        if(status == Constants.RESULT_CODE){
                            getMvpView().registeSuccess();
                            String accessToken = jo.getJSONObject("body").optString("accessToken");
                            String userId = jo.getJSONObject("body").getJSONObject("user").getString("id");
                            SharedPrefHelper.getInstance(getMvpView().context())
                                    .setAccessToken(accessToken);
                            SharedPrefHelper.getInstance(getMvpView().context())
                                    .setLoginUsername(getMvpView().phoneNumber());
                            SharedPrefHelper.getInstance(getMvpView().context())
                                    .setLoginPassword(getMvpView().pswRegiste());
                            SharedPrefHelper.getInstance(getMvpView().context())
                                    .setUserId(userId);
                            SharedPrefHelper.getInstance(getMvpView().context()).setIsLogin(true);
                            LoginNewActivity loginNewActivity = (LoginNewActivity)getMvpView().context();
                            EventBus.getDefault().post(new LoginSuccessEvent(loginNewActivity.getIntent().getStringExtra(Constants.LOGIN_TYPE_KEY)));
                            Toast.makeText(getMvpView().context(),"自动登录成功",Toast.LENGTH_SHORT).show();
                        }else{
                            getMvpView().showError(jo.getString("msg"));
                        }
                    } catch (JSONException e) {
                        getMvpView().showError("注册失败");
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    getMvpView().showError("注册失败");
                }
            });
        }else{
            getMvpView().showError("当前没有网络");
        }

    }

    /**
     * 获取验证码
     */
    public void getMobileValid(){
        if(!NetworkUtil.isNetworkAvailable(getMvpView().context())){
            getMvpView().showError("当前无网络连接");
            return;
        }
        if(isGetValidNow)
            return;
        if(StringUtil.isEmpty(getMvpView().phoneNumber())){
            getMvpView().showError("手机号不能为空");
            return;
        }else if(!StringUtil.isEmpty(getMvpView().phoneNumber()) && !getMvpView().phoneNumber().matches(PHONE)){
            getMvpView().showError("请输入正确的手机号");
            return;
        }
        if(NetworkUtil.isNetworkAvailable(getMvpView().context())){
            isGetValidNow = true;
            getMvpView().showLoading();
            Call<String> call = ApiClient.getClient().mobilevalid(ParamsUtils.getMobileValid(getMvpView().phoneNumber()));
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    getMvpView().hideLoading();
                    if (response.isSuccessful()) {
                        isGetValidNow = false;
                        String str = response.body();
                        try{
                            JSONObject jo = new JSONObject(str);
                            int code = jo.getInt("code");
                            if(code == Constants.RESULT_CODE){
                                getMvpView().showError("验证码已发送");
                                getMvpView().switchBtStatus();
                            }else{
                                getMvpView().showError(jo.getString("msg"));
                            }
                        }catch (Exception e){
                            getMvpView().showError("获取验证码失败");
                        }

                    } else {
                        isGetValidNow = false;
                        getMvpView().showError("获取验证码失败");
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    isGetValidNow = false;
                    getMvpView().showError("发送验证码失败");
                }
            });
        }else{
            getMvpView().showError("当前没有网络");
        }

    }

    @Override
    public void resetRequestStatus() {
        super.resetRequestStatus();
        isGetValidNow = false;
        isLoadingData = false;
    }
}
