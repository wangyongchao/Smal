package com.bjjy.mainclient.persenter;

import com.alibaba.fastjson.JSON;
import com.bjjy.mainclient.phone.view.question.ExamRecommView;
import com.dongao.mainclient.model.bean.question.QuestionRecomm;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ParamsUtils;
import com.bjjy.mainclient.phone.utils.NetworkUtil;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by dell on 2016/12/13 0013.
 */
public class QuestionExamRecommPersenter extends BasePersenter<ExamRecommView>{
    public List<QuestionRecomm> questionRecomms;

    @Override
    public void getData() {
        if(!NetworkUtil.isNetworkAvailable(getMvpView().context())){
            getMvpView().showNetError();
            return;
        }
        getMvpView().showLoading();
        apiModel.getData(ApiClient.getClient().examRecommQuestion(ParamsUtils.exmRecommQuestion(getMvpView().examQuestionId())));
    }

    @Override
    public void setData(String obj) {
        try {
            JSONObject object = new JSONObject(obj);
            if(object.getString("code").equals("1000")){
                questionRecomms = JSON.parseArray(object.getString("body"), QuestionRecomm.class);
                if(questionRecomms.size()>0)
                    getMvpView().showRecommQuesList(questionRecomms);
                else
                    getMvpView().showNoData();
            }else{
                getMvpView().showError("获取数据失败");
            }
        }catch (Exception e){
            e.printStackTrace();
            getMvpView().showError("数据异常");
        }
    }

    @Override
    public void onError(Exception e) {
        getMvpView().showError("连接服务器失败");
    }
}
