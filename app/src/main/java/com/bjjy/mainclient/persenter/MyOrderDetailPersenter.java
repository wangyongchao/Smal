package com.bjjy.mainclient.persenter;

import com.alibaba.fastjson.JSON;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.bean.myorder.MyOrderDetail;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.remote.ApiClient;
import com.bjjy.mainclient.phone.view.myorder.MyOrderDetailView;

import org.json.JSONObject;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by fengzognwei on 2016/4/7.
 */
public class MyOrderDetailPersenter extends BasePersenter<MyOrderDetailView> {

    @Override
    public void getData() {
        getMvpView().showLoading();
        HashMap<String,String> params = new HashMap<>();
        params.put("version","1.0");
        params.put("time",""+System.currentTimeMillis());
        params.put("random", "263198958da6fcc76509565ed6aa067f");
        params.put("reqSource","m");
        params.put("userId", SharedPrefHelper.getInstance(getMvpView().context()).getUserId()+"");
        params.put("orderId",getMvpView().orderId());
//        params.put("params", "{" + "\"userId\"" + ":\"23401045\"" + ",\"orderId\"" + ":\"" + getMvpView().orderId() + "\"}");
        apiModel.getData(ApiClient.getClient().orderDetail(params));
    }

    @Override
    public void setData(String obj) {
        getMvpView().hideLoading();
        try{
            JSONObject object = new JSONObject(obj);
            if(object.getString("status")!=null && object.getString("status").equals("1000")){
                MyOrderDetail myOrderDetail = JSON.parseObject(object.getString("result"), MyOrderDetail.class);
                getMvpView().showData(myOrderDetail);
            }else{
                getMvpView().showError(object.getString("msg"));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onError(Exception e) {
        super.onError(e);
        getMvpView().hideLoading();
        getMvpView().showError("网络异常，点击重试");
    }

    public void checkToken(){
        HashMap<String,String> params = new HashMap<>();
        params.put("userId", SharedPrefHelper.getInstance(getMvpView().context()).getUserId()+"");
        params.put("accessToken", SharedPrefHelper.getInstance(getMvpView().context()).getAccessToken());
        Call<String> call = ApiClient.getClient().checkToken(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    String str = response.body();
                    try {
                        JSONObject jo = new JSONObject(str);
                        JSONObject object = jo.getJSONObject("verify_result");
                        int code = object.getInt("code");
                        if(code == 1){
                            getMvpView().deviceTokenSuccess();
                            return;
                        }else{
//                            getMvpView().deviceTokenFailed();
                            getMvpView().deviceTokenSuccess();
                        }
                    } catch (Exception e) {
                        getMvpView().showError("网络异常，点击重试");
                    }
                } else {
//                    getMvpView().deviceTokenFailed();
                    getMvpView().deviceTokenSuccess();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
//                getMvpView().deviceTokenFailed();
                getMvpView().deviceTokenSuccess();
            }
        });
    }

}
