package com.bjjy.mainclient.persenter;

import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.bjjy.mainclient.phone.view.classroom.question.ClassQuestionFragment;
import com.bjjy.mainclient.phone.view.question.ExamRecommQuestionListActivity;
import com.bjjy.mainclient.phone.view.question.MyQuestionListActivity;
import com.bjjy.mainclient.phone.view.question.MyQuestionListView;
import com.dongao.mainclient.model.local.MyCollectionDB;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.bean.question.Question;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ParamsUtils;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by fengzongwei on 2016/5/5 0005.
 */
public class MyQuestionListPersenter extends BasePersenter<MyQuestionListView>{

    private boolean isLoadmore = false;

    private int totlePage;//数据总页数
    private MyCollectionDB myCollectionDB;

    private boolean isCheckNow = false;//是否正在校验是否可以提问

    @Override
    public void attachView(MyQuestionListView mvpView) {
        super.attachView(mvpView);
        myCollectionDB = new MyCollectionDB(getMvpView().context());
    }

    /**
     * 访问前设置此属性，用来判断当前是刷新还是加载操作
     * @param isLoadmore
     */
    public void setLoadmore(boolean isLoadmore){
        this.isLoadmore = isLoadmore;
    }

    @Override
    public void getData() {
        //   SharedPrefHelper.getInstance(getMvpView().context()).getSubjectId()
        if(getMvpView() instanceof MyQuestionListActivity){//推荐和我的答疑，共用一个persenter所以进行判断
            HashMap<String,String> params = ParamsUtils.myQuestion(SharedPrefHelper.getInstance(getMvpView().context()).getSubjectId(),getMvpView().getPage()+"",
                    com.dongao.mainclient.model.common.Constants.PAGE_SIZE);
            apiModel.getData(ApiClient.getClient().myQuestion(params));
        }else{
            HashMap<String,String> params = null;
            if(getMvpView() instanceof ClassQuestionFragment) {
                params = ParamsUtils.jinghuaQuestion(SharedPrefHelper.getInstance(getMvpView().context()).getSubjectId(), getMvpView().getPage() + "",
                        com.dongao.mainclient.model.common.Constants.PAGE_SIZE);
                apiModel.getData(ApiClient.getClient().recommQuestion(params));
            }
        }
//        }else{
//            HashMap<String,String> params = ParamsUtils.recommQuestion("71634", getMvpView().getQuestionId());
//            apiModel.getData(ApiClient.getClient().recommQuestion(params));
//        }

    }

    @Override
    public void setData(String obj) {
        try{
            JSONObject object = new JSONObject(obj);
            if(object.getJSONObject("result").getInt("code")==1000)
                if(object.has("body")) {
                    if(object.optString("body").equals("null") || object.getString("body").equals("") || object.getString("body")==null){
                        getMvpView().showNoData();
                        return;
                    }
                    JSONObject body = object.getJSONObject("body");
//                    List<Question> lisMyQuestionObjs = QuestionParserUtil.parseQueArray(array, false);
                    String quesList = null;
                    getMvpView().setTotalPage(body.getInt("totalPages"));
                    if(getMvpView() instanceof ClassQuestionFragment)
                        quesList = body.optString("recommenAskList");
                    else if(getMvpView() instanceof ExamRecommQuestionListActivity)
                        quesList = body.optString("recommenAskList");
                    else
                        quesList = body.optString("myAskList");
                    if(quesList!=null && !quesList.equals("null") && !quesList.equals("")){
                        List<Question> lisMyQuestionObjs = JSON.parseArray(quesList,Question.class);
                        if(!isLoadmore)
                            getMvpView().showData(lisMyQuestionObjs);
                        else{
                            getMvpView().showMoreData(lisMyQuestionObjs);
                        }
                    }else{
                        if(isLoadmore){
                            getMvpView().showLoadMoreError("无更多推荐");
                        }else{
                            getMvpView().showNoData();
                        }
                    }
                }else{
                    getMvpView().showError("当前无推荐答疑");
                }
            else{
                if(!isLoadmore)
                    getMvpView().showError("加载数据失败,点击重试");
                else
                    getMvpView().showLoadMoreError("加载失败");
            }
        }catch (Exception e){
            e.printStackTrace();
            if(!isLoadmore)
                getMvpView().showError("加载数据失败,点击重试");
            else
                getMvpView().showLoadMoreError("加载失败");
        }
    }

    @Override
    public void onError(Exception e) {
        super.onError(e);
        getMvpView().showError("服务器异常，请稍后重试");
    }

    /**
     * 精准答疑推送、或者拉取某个答疑
     */
    public void getQuestionDetail(String questionAnswerId){

        Call<String> call = ApiClient.getClient().questionDetail(ParamsUtils.questionDetail(questionAnswerId));
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                getMvpView().hideLoading();
                if (response.isSuccessful()) {
                    String str = response.body();
                    try {
                        JSONObject jo = new JSONObject(str);
                        int code = jo.getJSONObject("result").getInt("code");
                        if (code == 1000) {
                            String body = jo.getJSONObject("body").getString("qasDetailList");
                            List<Question> questionList = JSON.parseArray(body, Question.class);
                            getMvpView().showData(questionList);
                        } else {
                            getMvpView().showError("");
                        }
                    } catch (Exception e) {
                        getMvpView().showError("");
                    }
                } else {
                    getMvpView().showError("");
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                getMvpView().showError("");
            }
        });
    }

    /**
     * 获取科目下的可选章节列表
     */
    public void getSectionsBySubjectId(){
        Call<String> call = ApiClient.getClient().questionSubjectSection(ParamsUtils.getSectionsBySubjectId(SharedPrefHelper.getInstance(getMvpView().context()).getSubjectId()));
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                getMvpView().hideLoading();
                if (response.isSuccessful()) {
                    String str = response.body();
                    try{
                        JSONObject jo = new JSONObject(str);
                        int code = jo.getJSONObject("result").getInt("code");
                        if(code==1000){
                            String body = jo.getJSONObject("body").getString("bookList");
                            getMvpView().bookListJson(body);
                        }else{
                        }
                    }catch (Exception e){
                    }

                } else {
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }


    /**
     * 获取试题推荐答疑
     */
    public void getExamRecommQuestion(){
        Call<String> call = ApiClient.getClient().examRecommQuestion(ParamsUtils.examRecommQuestion(getMvpView().getQuestionId(), getMvpView().getPage() + "",
                com.dongao.mainclient.model.common.Constants.PAGE_SIZE));
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                getMvpView().hideLoading();
                if (response.isSuccessful()) {
                    String str = response.body();
                    try{
                        JSONObject jo = new JSONObject(str);
                        int code = jo.getJSONObject("result").getInt("code");
                        if(code==1000){
                            String body = jo.getJSONObject("body").getString("recommQasAboutExaminationQuestion");
                            List<Question> questionList = JSON.parseArray(body, Question.class);
                            getMvpView().setTotalPage(jo.getJSONObject("body").getInt("totalPages"));
                            getMvpView().showData(questionList);
                        }else{
                            getMvpView().showError("");
                        }
                    }catch (Exception e){
                        getMvpView().showError("");
                    }
                } else {
                    getMvpView().showError("");
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                getMvpView().showError("");
            }

        });
    }

    /**
     * 获取试题推荐答疑
     */
    public void isCanAsk(String examinationQuestionId){
        //"580259"
        if(isCheckNow){
            Toast.makeText(getMvpView().context(),"正在校验您是否可以进行提问，请稍后...",Toast.LENGTH_SHORT).show();
            return;
        }
        isCheckNow = true;
        Call<String> call = ApiClient.getClient().isCanAsk(ParamsUtils.isCanAsk(examinationQuestionId));
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                isCheckNow = false;
                getMvpView().hideLoading();
                if (response.isSuccessful()) {
                    String str = response.body();
                    try{
                        JSONObject jo = new JSONObject(str);
                        int code = jo.getJSONObject("result").getInt("code");
                        if(code==1000){
                            getMvpView().isCanAsk(jo.getJSONObject("body").getString("askFlag"));
                        }else{
                            getMvpView().checkIsCanAskFail();
                        }
                    }catch (Exception e){
                        getMvpView().checkIsCanAskFail();
                    }
                } else {
                    getMvpView().checkIsCanAskFail();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                isCheckNow = false;
                getMvpView().checkIsCanAskFail();
            }
        });
    }

    /**
     * 删除对答疑的收藏
     */
    public void deleteCollection(String askId){
        myCollectionDB.deleteQuestion(askId);
    }

}
