package com.bjjy.mainclient.persenter;


import android.text.TextUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bjjy.mainclient.phone.view.goodsubject.views.CourseSelectView;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.bean.courselect.Exams;
import com.dongao.mainclient.model.bean.courselect.GoodsKind;
import com.dongao.mainclient.model.remote.ApiClient;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * 登录UI
 */
public class CourseSelectPersenter extends BasePersenter<CourseSelectView> {
    private ArrayList<GoodsKind> mList ;

    @Override
    public void attachView(CourseSelectView centerView) {
        super.attachView(centerView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    @Override
    public void getData(){
        getMvpView().showLoading();
//        String accessToken = SharedPrefHelper.getInstance(getMvpView().context()).getAccessToken();
        HashMap<String,String> params = new HashMap<>();
        Exams exam=new Exams();
        exam.setExamId(Integer.parseInt("711"));
        exam.setIsComplex("0");
        exam.setYear("2016");
        String examids=JSON.toJSONString(exam);
        params.put("time", "");
        params.put("random", "");
        params.put("reqSource", "");
        params.put("params",examids);
        apiModel.getData(ApiClient.getClient().getGoodsList(params));

    }

    @Override
    public void setData(String obj) {
        String json="{\"result\":[{\"goodsList\":[{\"goodsId\":\"13271\",\"price\":\"400.0\",\"subjectName\":\"初级会计实务\",\"subjectId\":\"71901\",\"name\":\"东奥精品班\"},{\"goodsId\":\"13273\",\"price\":\"400.0\",\"subjectName\":\"经济法基础\",\"subjectId\":\"71903\",\"name\":\"东奥精品班\"}],\"clazzName\":\"东奥精品班\"},{\"goodsList\":[{\"goodsId\":\"13275\",\"price\":\"1000.0\",\"subjectName\":\"初级会计实务+经济法基础+两科东奥机考模拟系统\",\"subjectId\":\"0\",\"name\":\"冲关班\"}],\"clazzName\":\"冲关班\"},{\"goodsList\":[{\"goodsId\":\"13277\",\"price\":\"100.0\",\"subjectName\":\"初级会计实务\",\"subjectId\":\"71901\",\"name\":\"机考系统\"},{\"goodsId\":\"13279\",\"price\":\"100.0\",\"subjectName\":\"经济法基础\",\"subjectId\":\"71903\",\"name\":\"机考系统\"}],\"clazzName\":\"机考系统\"},{\"goodsList\":[{\"goodsId\":\"13267\",\"price\":\"260.0\",\"subjectName\":\"初级会计实务\",\"subjectId\":\"71901\",\"name\":\"超值经典班\"},{\"goodsId\":\"13269\",\"price\":\"260.0\",\"subjectName\":\"经济法基础\",\"subjectId\":\"71903\",\"name\":\"超值经典班\"}],\"clazzName\":\"超值经典班\"}],\"status\":1000,\"msg\":\"ok\"}";
        obj=json;
        try {
            if(TextUtils.isEmpty(obj)){
                getMvpView().showError("");
                return;
            }
            JSONObject data=JSON.parseObject(obj);
            if(data.getIntValue("status")!=1000){
                return;
            }
            String result=data.getString("result");
            mList = (ArrayList)JSON.parseArray(result,GoodsKind.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        getMvpView().setAdapter(mList);
        //courseView.refresh();
    }
}
