package com.bjjy.mainclient.persenter;

import com.bjjy.mainclient.phone.view.question.MyQuestionModifyActivity;
import com.bjjy.mainclient.phone.view.question.MyQuestionModifyView;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ParamsUtils;

import org.json.JSONObject;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by fengzongwei on 2016/5/4 0004.
 */
public class ModifyQuePersenter extends BasePersenter<MyQuestionModifyView>{

    private int mode;

    public ModifyQuePersenter(int mode){
        this.mode = mode;
    }

    @Override
    public void getData() {
        if(getMvpView().getQuesContent().length()<10){
            getMvpView().showError("问题的描述不能少于10个字");
            return;
        }
        getMvpView().showAnimProgress();
        HashMap<String,String> params = null;
        if(mode == MyQuestionModifyActivity.CONTINUE_ASK){//追问
            params = ParamsUtils.continueAsk(getMvpView().getQuesTitle(),getMvpView().getQuesContent(),getMvpView().getParamsFromIntent("qaFatherId"));
            apiModel.getData(ApiClient.getClient().continueAskQuestion(params));
        }else if(mode == 2){//修改
            params = ParamsUtils.modifyQuestion(getMvpView().getQuesTitle(),getMvpView().getQuesContent(),getMvpView().getParamsFromIntent("questionId"));
            apiModel.getData(ApiClient.getClient().modifyQuestion(params));
        }else{//新增提问 SharedPrefHelper.getInstance(getMvpView().context()).getSubjectId()
            params = ParamsUtils.addQuestionNoraml(SharedPrefHelper.getInstance(getMvpView().context()).getExaminationId(),
                    getMvpView().getQuesTitle(),
                    getMvpView().getQuesContent(),
                    getMvpView().getParamsFromIntent("examinationQuestionId"));
            apiModel.getData(ApiClient.getClient().addQuestionNormal(params));
        }

    }

    @Override
    public void setData(String obj) {
        getMvpView().dissmissAnimProgress();
        try{
            JSONObject object = new JSONObject(obj).getJSONObject("result");
            if(object.getInt("code") == 1000)
                getMvpView().resultAction();
            else
                getMvpView().showError(object.getString("msg"));
        }catch (Exception e){
            e.printStackTrace();
            getMvpView().showError("提交失败");
        }
    }


    /**
     * 追问答疑
     */
    public void continueAskQuestion(){
        HashMap<String,String> params = new HashMap<>();
        params.put("qaFatherId",getMvpView().getParamsFromIntent("qaFatherId"));
        params.put("userId",SharedPrefHelper.getInstance(getMvpView().context()).getUserId());
        params.put("title",getMvpView().getQuesTitle());
        params.put("content",getMvpView().getQuesContent());
        Call<String> call = ApiClient.getClient().continueAskQuestion(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    String str = response.body();
                    try{
                        JSONObject jo = new JSONObject(str);
                        int code = jo.getJSONObject("result").getInt("code");
                        if(code==1000){
                            getMvpView().resultAction();
                        }else{
                        }
                    }catch (Exception e){
                    }
                } else {
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }



}
