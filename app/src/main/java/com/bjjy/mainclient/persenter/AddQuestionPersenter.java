package com.bjjy.mainclient.persenter;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.view.View;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.view.question.AddQuestionActivity;
import com.bjjy.mainclient.phone.view.question.AddQuestionView;
import com.dongao.mainclient.core.camera.bean.BookType;
import com.dongao.mainclient.core.camera.bean.ChapterModel;
import com.dongao.mainclient.core.camera.bean.PhotoModel;
import com.dongao.mainclient.core.camera.bean.Question;
import com.dongao.mainclient.core.camera.bean.QuestionDbBean;
import com.dongao.mainclient.core.camera.bean.UrlsModel;
import com.dongao.mainclient.core.camera.constants.Constants;
import com.dongao.mainclient.core.camera.utils.CameraUtils;
import com.dongao.mainclient.core.camera.utils.CommonUtils;
import com.dongao.mainclient.core.camera.utils.ImageUtil;
import com.dongao.mainclient.model.local.QuestionDB;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ParamsUtils;
import com.dongao.mainclient.model.remote.bean.BaseBean;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by fengzongwei on 2016/5/7 0007.
 */
public class AddQuestionPersenter extends BasePersenter<AddQuestionView>{

    /**
     * 需要的相关数据
     */
    public String userId,examId,subjectId;
    public List<BookType> bookList;
    public com.dongao.mainclient.model.bean.question.Question myQuestion;
    public QuestionDB questionDB;
    public QuestionDbBean questionDbBean;
    public ChapterModel chapterModel;
    public Question question;//当前生成的问题对象
    public ArrayList<ChapterModel> chapterList;
    public int bookIdNumber;
    public List<PhotoModel> selected;

    public int queueAboutCommit = 0;//上传数据的顺从
    public List<PhotoModel> commitSuccessList;//上传成功的图片集合
    public List<PhotoModel> commitFailList;//上传失败的图片集合
    public List<PhotoModel> commitList;//上传的图片集合
    public ArrayList<String> reImageList;

    private String[] comeInType = new String[]{"提问", "修改"};

    private ArrayList<String> urlList;
    public String str_choosed_img = "";

    public AddQuestionPersenter(){
        commitList = new ArrayList<>();
        reImageList = new ArrayList<String>();
        commitFailList = new ArrayList<>();
        commitSuccessList = new ArrayList<>();
    }

    public void initDB(){
        Intent intent = getMvpView().catchIntent();
//        userId = String.valueOf(SharedPrefHelper.getInstance(getMvpView().context()).getUserId());
        userId = SharedPrefHelper.getInstance(getMvpView().context()).getUserId();
        examId = SharedPrefHelper.getInstance(getMvpView().context()).getExamId();
        subjectId = SharedPrefHelper.getInstance(getMvpView().context()).getSubjectId();
        //questionId=intent.getStringExtra("questionId");
        String jsonBookList = intent.getStringExtra("bookList");
        bookList = (ArrayList<BookType>) JSON.parseArray(jsonBookList, BookType.class);
        if(bookList!=null && bookList.size()>0 && bookList.get(0).getBookSectionList()!=null && bookList.get(0).getBookSectionList().size()>0)
            getMvpView().setBookName(bookList.get(0).getBookName());
        else
            getMvpView().goneListView();
        //分辨是提问进入还是修改进入 修改进入则不为null
        myQuestion = (com.dongao.mainclient.model.bean.question.Question) intent.getSerializableExtra("modifyQuestion");
        questionDB = new QuestionDB(getMvpView().context());

        if (myQuestion == null) {//新增问题，而不是修改问题
            questionDbBean = questionDB.findBySubjectId(userId, subjectId);
            if (questionDbBean == null) {
                question = new Question();
            } else {
                question = JSON.parseObject(questionDbBean.getQuestion(), Question.class);
                if (question == null) {
                    question = new Question();
                }
            }
        } else {//修改问题
            getMvpView().setBookName(myQuestion.getBookName());
            questionDbBean = questionDB.findByQuestionID(userId, myQuestion.getAskId());
            if (questionDbBean == null) {
                question = new Question();
            } else {
                question = JSON.parseObject(questionDbBean.getQuestion(), Question.class);
                if (question == null) {
                    question = new Question();
                }
            }
        }
        //判断当前哪个章节被选中
        chapterModel = new ChapterModel();
        if (myQuestion != null) {
            if (question.getSectionId() != null) {
                for (int i = 0; i < bookList.size(); i++) {
                    for (int j = 0; j < bookList.get(i).getSectionList().size(); j++) {
                        if (bookList.get(i).getSectionList().get(j).getSectionId().equals(question.getSectionId())) {
                            bookList.get(i).getSectionList().get(j).setIsChecked(true);
                            chapterModel = bookList.get(i).getSectionList().get(j);
                            break;
                        }
                    }
                }
            } else {
                for (int i = 0; i < bookList.size(); i++) {
                    for (int j = 0; j < bookList.get(i).getSectionList().size(); j++) {
                        if (bookList.get(i).getSectionList().get(j).getSectionId().equals(myQuestion.getSectionId())) {
                            bookList.get(i).getSectionList().get(j).setIsChecked(true);
                            chapterModel = bookList.get(i).getSectionList().get(j);
                            break;
                        }
                    }
                }
            }
        } else {
            if (question.getSectionId() != null) {
                for (int i = 0; i < bookList.size(); i++) {
                    for (int j = 0; j < bookList.get(i).getSectionList().size(); j++) {
                        if (bookList.get(i).getSectionList().get(j).getSectionId().equals(question.getSectionId())) {
                            bookList.get(i).getSectionList().get(j).setIsChecked(true);
                            chapterModel = bookList.get(i).getSectionList().get(j);
                            break;
                        }
                    }
                }
            }
        }
        chapterList = new ArrayList<>();
        if(bookList!=null && bookList.size()>0 && bookList.get(0).getBookSectionList()!=null && bookList.get(0).getBookSectionList().size()>0)
            chapterList.addAll(bookList.get(0).getSectionList());
    }

    public void initData(){
        //判断是否显示添加图片的标示
        if (myQuestion == null) {
            ((AddQuestionActivity)getMvpView()).mSuggest.setText(question.getContent());
            selected = question.getPhotos();
            if (selected == null) {
                selected = new ArrayList<>();
            }
            getMvpView().resetAdapter(selected);
            getMvpView().setGVOnItemClickListener();
            if (question.getPage() != null && !question.getPage().equals("")) {
                ((AddQuestionActivity)getMvpView()).book_number_et.setText(question.getPage());
            }
            if (question.getSectionName() != null) {
                ((AddQuestionActivity)getMvpView()).choose_chapter_tv.setText(question.getSectionName());
            }
            getMvpView().setCameraViewVisibility(View.VISIBLE);
        } else {
            if (selected == null) {
                selected = new ArrayList<>();
            }

//            ((AddQuestionActivity)getMvpView()).choose_chapter_tv.setEnabled(false);
            ((AddQuestionActivity)getMvpView()).linearLayout_chooseChapter.setEnabled(false);
            ((AddQuestionActivity)getMvpView()).book_number_et.setEnabled(false);
            ((AddQuestionActivity)getMvpView()).book_number_et.setTextColor(getMvpView().context().getResources().getColor(R.color.line));
            //判断当前数据库是否有草稿
            if (question.getSectionName() != null ||
                    question.getPage() != null || question.getContent() != null
                    ) {
                if (question.getContent() != null && !question.getContent().equals("")) {
                    ((AddQuestionActivity)getMvpView()).mSuggest.setText(question.getContent());
                }
                if (question.getPage() != null && !question.getPage().equals("")) {
                    ((AddQuestionActivity)getMvpView()).book_number_et.setText(question.getPage());
                }
                if (question.getSectionName() != null && !question.getSectionName().equals("")) {
                    ((AddQuestionActivity)getMvpView()).choose_chapter_tv.setText(question.getSectionName());
                }
            } else {
                ((AddQuestionActivity)getMvpView()).book_number_et.setText(myQuestion.getPageNum());
                ((AddQuestionActivity)getMvpView()).mSuggest.setText(myQuestion.getContent());
                ((AddQuestionActivity)getMvpView()).choose_chapter_tv.setText(myQuestion.getSectionName());
            }
            getMvpView().setCameraViewVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void getData(){
        //第一次上传赋值给上传数据
        if ((commitFailList.size() + commitSuccessList.size()) == 0) {
            commitList.clear();
            commitList.addAll(selected);
        }
        //判断当前上传的图片是否完全上传成功
        if (selected.size() != 0 && (commitFailList.size() + commitSuccessList.size()) < selected.size()) {//有的还未进行上传动作
            commitImage(queueAboutCommit);
        } else if ((commitFailList.size() + commitSuccessList.size()) == selected.size()) {//所有的图片 都进行了上传动作
            if (selected.size() == 0 || commitSuccessList.size() == selected.size()) {//全部上传成功或者没有选择图片
                //判断当前是修改问题还是新交提问
                if (myQuestion == null) {//新增
                    if (question.getQuestionId() == null || question.getQuestionId().equals("")) {
                        commitQuestion();
                    } else {
                        commitModifyQuestion();
                    }
                } else {
                    commitModifyQuestion();
                }
            } else {//存在上传失败的情况
                if(commitSuccessList.size()==0){
//                    getMvpView().showError("图片全部上传失败");
                }else{
//                    getMvpView().showError("图片部分上传失败");
                }
            }
        }

    }

    /**
     * 上传提问的信息
     */
    private void commitQuestion(){
        getMvpView().showLoading();
        String content = getMvpView().getEditString();
        String imageUrl = "";
        StringBuffer sb = new StringBuffer();
        if (reImageList != null && reImageList.size() > 0) {
            for (int i = 0; i < reImageList.size(); i++) {
                if (i == reImageList.size() - 1) {
                    sb.append(reImageList.get(i));
                } else {
                    sb.append(reImageList.get(i)).append(",");
                }
            }
        }
        if (sb != null) {
            imageUrl = sb.toString();
        }
        if(bookList!=null && bookList.size()>0 && bookList.get(0).getBookSectionList()!=null && bookList.get(0).getBookSectionList().size()>0)
            apiModel.getData(ApiClient.getClient().addQuestion(ParamsUtils.addBookQuestion(bookList.get(0).getBookId(), chapterModel.getSectionId(),
                getMvpView().getPageNum(), bookList.get(0).getBookSectionList().get(bookIdNumber).getSubjectId(), CommonUtils.getTittleFromContent(getMvpView().getEditString()),
                content, imageUrl)));
        else
            apiModel.getData(ApiClient.getClient().addQuestion(ParamsUtils.addBookQuestion(bookList.get(0).getBookId(),"",
                    "","", CommonUtils.getTittleFromContent(getMvpView().getEditString()),
                    content, imageUrl)));
    }

    /**
     * 上传更改提问的信息
     */
    private void commitModifyQuestion() {
//        HashMap<String, String> params = new HashMap<String, String>();
//        params.put("questionId", myQuestion.getAskId());
//        params.put("title", CommonUtils.getTittleFromContent(getMvpView().getEditString()));
//        params.put("content", getMvpView().getEditString());
//        params.put("userId", "");
//        StringBuffer sb = new StringBuffer();
//        if (myQuestion.getImageUrlList() != null) {
//            for (int i = 0; i < myQuestion.getImageUrlList().size(); i++) {
//                sb.append(myQuestion.getImageUrlList().get(i)).append(",");
//            }
//            sb.substring(0, sb.lastIndexOf(","));
//        }
//        String url = sb.toString();
//        params.put("imageurl", url);
        getMvpView().showLoading();
        apiModel.getData(ApiClient.getClient().modifyQuestion(ParamsUtils.modifyQuestion
                (CommonUtils.getTittleFromContent(getMvpView().getEditString()), getMvpView().getEditString(), myQuestion.getAskId())));
    }


    @Override
    public void setData(String obj) {
        getResponseForQuestion(obj);
    }

    @Override
    public void onError(Exception e) {
        getMvpView().showError("问题提交失败");
    }

    public void commitImg(Bitmap bitmapOriginal,String path){
        getMvpView().showLoading();
        byte[] imageData = CommonUtils.Bitmap2Bytes(bitmapOriginal);
        Map<String,RequestBody> photos = new HashMap<>();
        String filename = path.substring(path.lastIndexOf("/")+1);
        photos.put("file\"; filename=\"" + filename,RequestBody.create(MediaType.parse("multipart/form-data"),imageData));Call<String> call = ApiClient.getClient().uploadImage(photos);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    BaseBean baseBean = JSON.parseObject(response.body(), BaseBean.class);
                    if (baseBean == null) {
                        commitFailList.add(commitList.get(queueAboutCommit));
                        queueAboutCommit++;
                        getData();
                        return;
                    } else {
                        int code = baseBean.getResult().getCode();
                        if (code != 1000) {
                            commitFailList.add(commitList.get(queueAboutCommit));
                            queueAboutCommit++;
                            getData();
                            if(code == 1018){
                                Toast.makeText(getMvpView().context(),"图片格式不支持", Toast.LENGTH_SHORT)
                                        .show();
                            }
                            return;
                        }
                        String body = baseBean.getBody().toString();
                        UrlsModel urlsModel = JSON.parseObject(body, UrlsModel.class);
                        urlList = new ArrayList<>();
                        urlList.add(urlsModel.getImageUrl());
                        //保存数据进入数据库
                        //reImageList.add(body.replace("[", "").replace("]", ""));
                        reImageList.addAll(urlList);
                        //上传成功
                        commitSuccessList.add(commitList.get(queueAboutCommit));
                        queueAboutCommit++;
                        getData();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    getMvpView().showPostImgError();
                    Toast.makeText(getMvpView().context(),
                            getMvpView().context().getString(R.string.add_question_update_error), Toast.LENGTH_SHORT)
                            .show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                getMvpView().showPostImgError();
            }
        });
    }

    /**
     * 上传图片
     *
     * @param queue
     */
    private void commitImage(int queue) {
        if (queue <= commitList.size() - 1) {
            Bitmap bitmapOriginal = ImageUtil.getSmallBitmap(commitList.get(queue).getOriginalPath(), 300, 300);
            if (bitmapOriginal == null) {//没有找到相应的图片
                return;
            }
            if (commitList.get(queue).getOrientation()!=0){
                bitmapOriginal=ImageUtil.rotaingImageView(commitList.get(queue).getOrientation(),bitmapOriginal);
            }
            commitImg(bitmapOriginal,commitList.get(queue).getOriginalPath());
        }
    }

    /**
     * 获取上传问题返回的参数
     */
    private void getResponseForQuestion(String str) {
        getMvpView().hideLoading();
        try {
            BaseBean baseBean = JSON.parseObject(str, BaseBean.class);
            if (baseBean == null) {
                if (myQuestion != null) {
                    Toast.makeText(getMvpView().context(),
                            getMvpView().context().getResources().getString(R.string.add_question_update_error), Toast.LENGTH_SHORT)
                            .show();
                } else {
                    if (question.getQuestionId() == null || question.getQuestionId().equals("")) {

                        Toast.makeText(getMvpView().context(),
                                getMvpView().context().getResources().getString(R.string.add_question_error), Toast.LENGTH_SHORT)
                                .show();
                    } else {
                        Toast.makeText(getMvpView().context(),
                                getMvpView().context().getResources().getString(R.string.add_question_update_error), Toast.LENGTH_SHORT)
                                .show();
                    }
                }
            } else {
                int code = baseBean.getResult().getCode();
                if (code != 1000) {
                    if (myQuestion != null) {
                        Toast.makeText(getMvpView().context(),
                                getMvpView().context().getResources().getString(R.string.add_question_update_error), Toast.LENGTH_SHORT)
                                .show();
                    } else {
                        if (question.getQuestionId() == null || question.getQuestionId().equals("")) {

                            Toast.makeText(getMvpView().context(),
                                    getMvpView().context().getResources().getString(R.string.add_question_error), Toast.LENGTH_SHORT)
                                    .show();
                        } else {
                            Toast.makeText(getMvpView().context(),
                                    getMvpView().context().getResources().getString(R.string.add_question_update_error), Toast.LENGTH_SHORT)
                                    .show();
                        }
                    }
                    return;
                }
                //判断当前的草稿箱是否存在
                if (myQuestion == null) {
                } else {
                    ((AddQuestionActivity)(getMvpView().context())).setResult(Constants.ACTIVITY_REQUEST_CODE_CHANGE_QUESTION);
                }
                if (questionDbBean != null) {
                    questionDB.deleteByQuestion(questionDbBean);
                }
                Toast.makeText(getMvpView().context(),"提问成功",Toast.LENGTH_SHORT).show();
                ((AddQuestionActivity) (getMvpView().context())).finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getMvpView().context(),"提问上传失败", Toast.LENGTH_SHORT)
                    .show();
        }
    }

    /**
     * 失败图片重新上传
     */
    public void commitFailImage() throws UnsupportedEncodingException {
        commitList.clear();
        commitList.addAll(commitFailList);
        commitFailList.clear();
        queueAboutCommit = 0;
        getData();
    }

    public void resultMain(Intent data){
            selected.clear();
            List<PhotoModel> photos = (ArrayList<PhotoModel>) data
                    .getExtras().getSerializable("selectphotos");
            selected.addAll(photos);
            getMvpView().notifyAdapter();
    }

    public void selectImgCode(Intent data){
        clearFailDate();
        selected.clear();
        List<PhotoModel> photos = (List<PhotoModel>) data.getExtras()
                .getSerializable("photos");
        getMvpView().notifyAdapter();
        selected.addAll(photos);
        getMvpView().resetAdapter(selected);
        getMvpView().notifyAdapter();
    }

    public void cameraCode(){
        clearFailDate();
        if (CameraUtils.cameraFile != null
                && CameraUtils.cameraFile.exists()) {
            str_choosed_img = CameraUtils.cameraFile.getAbsolutePath();
            int angle = ImageUtil.readPictureDegree(str_choosed_img);

            if (angle != 0) {
                ImageUtil.changePicByPathForSAMSUNG(str_choosed_img, angle);
            }
            PhotoModel cameraPhotoModel = new PhotoModel();
            cameraPhotoModel.setChecked(true);
            //cameraPhotoModel.setOrientation(angle);
            cameraPhotoModel.setOriginalPath(str_choosed_img);
            selected.add(cameraPhotoModel);
            getMvpView().notifyAdapter();
            MediaScannerConnection.scanFile(getMvpView().context(),
                    new String[]{str_choosed_img}, null, null);
        } else {
            CameraUtils.showToast(getMvpView().context(), "获取照片失败，请重试");
        }
    }

    /**
     * 清空上传失败保留的数据
     */
    public void clearFailDate() {
        reImageList.clear();
        commitFailList.clear();
        commitSuccessList.clear();
        commitList.clear();
    }

    /**
     * 点击了返回按钮
     */
    public void onBackPress(){
        if (myQuestion != null) {
            if (question.getQuestionId() == null || question.getQuestionId().equals("")) {

                if (!getMvpView().getEditString().equals(myQuestion.getContent())) {
                    getMvpView().showSaveDialog();
                } else {
                    ((AddQuestionActivity)getMvpView().context()).finish();
                }
            } else {
                if (!getMvpView().getEditString().equals(question.getContent())) {
                    getMvpView().showSaveDialog();
                } else {
                    ((AddQuestionActivity)getMvpView().context()).finish();
                }
            }
        } else {
            if (!getMvpView().getEditString().equals("") || selected.size() != 0 || !getMvpView().getPageNum().equals("") || !getMvpView().getChooseSectioName().toString().equals("")) {
                getMvpView().showSaveDialog();
            } else {
                ((AddQuestionActivity)getMvpView().context()).finish();
            }
        }
    }

    public void saveData(){
        Question questionSave = new Question();
        questionSave.setPage(getMvpView().getPageNum());
        questionSave.setContent(getMvpView().getEditString());
        questionSave.setPhotos(selected);
        questionSave.setBookId(bookList.get(0).getBookId());
        questionSave.setUserId(userId);
        questionSave.setSubjectId(subjectId);
        questionSave.setExamId(examId);
        if (chapterModel != null) {
            questionSave.setSectionId(chapterModel.getSectionId());
            questionSave.setSectionName(chapterModel.getSectionName());
        }

        //判断是那种方式进入的
        QuestionDbBean questionOld = null;
        /**
         * 判断当前的数据库中是否存在
         */

        if (myQuestion != null) {

            questionSave.setQuestionId(myQuestion.getAskId());
            questionOld = questionDB.findByQuestionID(userId, myQuestion.getAskId());
        } else {
            if (question.getQuestionId() == null || question.getQuestionId().equals("")) {
                questionOld = questionDB.findBySubjectId(userId, subjectId);

            } else {
                questionSave.setQuestionId(question.getQuestionId());
                questionOld = questionDB.findByQuestionID(userId, myQuestion.getAskId());
            }
        }
        if (questionOld != null) {
            questionDB.deleteByQuestion(questionOld);
        }
        QuestionDbBean questionDbBean = new QuestionDbBean();
        questionDbBean.setUserId(userId);
        questionDbBean.setSubjectId(subjectId);
        if (myQuestion != null) {
            questionDbBean.setComeInType(comeInType[1]);
            questionDbBean.setQuestionId(myQuestion.getAskId());
        } else {
            questionDbBean.setComeInType(comeInType[0]);
        }
        String answers = JSON.toJSONString(questionSave, SerializerFeature.DisableCircularReferenceDetect);
        questionDbBean.setQuestion(answers);
        questionDB.insert(questionDbBean);
    }

    //删除保存的数据
    public void deletaSava(){
        if(questionDbBean!=null)
            questionDB.deleteByQuestion(questionDbBean);
    }

}
