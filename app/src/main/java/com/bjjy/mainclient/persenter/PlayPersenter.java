package com.bjjy.mainclient.persenter;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Base64;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.bjjy.mainclient.phone.download.db.OperaDB;
import com.bjjy.mainclient.phone.download.db.PlayParamsDB;
import com.bjjy.mainclient.phone.event.PlayUploadRecord;
import com.bjjy.mainclient.phone.utils.NetworkUtil;
import com.bjjy.mainclient.phone.view.download.local.bean.YearInfo;
import com.bjjy.mainclient.phone.view.play.DownloadView;
import com.bjjy.mainclient.phone.view.play.PlayActivity;
import com.bjjy.mainclient.phone.view.play.PlayView;
import com.bjjy.mainclient.phone.view.play.domain.CwStudyLogDB;
import com.bjjy.mainclient.phone.view.play.fragment.uploadBean.PlayUrlBean;
import com.bjjy.mainclient.phone.view.play.utils.AESHelper;
import com.bjjy.mainclient.phone.view.play.utils.Constans;
import com.bjjy.mainclient.phone.view.play.utils.SignUtils;
import com.bjjy.mainclient.phone.widget.DialogManager;
import com.dongao.mainclient.core.util.FileUtil;
import com.dongao.mainclient.model.bean.course.Opera;
import com.dongao.mainclient.model.bean.play.CourseDetail;
import com.dongao.mainclient.model.bean.play.CourseWare;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.CourseDetailDB;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ParamsUtils;
import com.dongao.mainclient.model.remote.bean.BaseBean;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by fengzongwei on 2016/5/30 0030.
 */
public class PlayPersenter extends BasePersenter<PlayView> {


	public CourseDetail courseDetail;//接口返回的数据
	private CourseDetail courseDetailInDB;//从数据库中查询到的数据
	private List<CourseWare> courseWareLists;//没有章节时显示的数据
	private CourseDetailDB courseDetailDB;
	private CwStudyLogDB cwStudyLogDB;

	private String classId;//上级页面传递过来的课程id(班次id 一定有)
	private String courseBean;//CoursePlay的json串（一定有）
	private String subjectId;//上级页面传递过来的科目id(不一定有)
	private String playCwId;//打开详情时计划要播放的视频id(不一定有)
	private OperaDB operaDB;
	private String userId;
	private String lastVideoIdInDB;
	private String version, isFromLocal;
	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what){
				case 1000:
					getPreData(cw, "");
					break;
				case 2000:
					getMvpView().hideLoading();
					getMvpView().playError("key数据错误");
					break;
				case 3000:
					EventBus.getDefault().post(new PlayUploadRecord());
					sendUpload();
					break;
			}
		}
	};
	private YearInfo yearInfo;
	
	public void sendUpload(){
		handler.sendEmptyMessageDelayed(3000,240000);
	}

	@Override
	public void attachView(PlayView mvpView) {
		super.attachView(mvpView);
		playParamsDB = new PlayParamsDB(getMvpView().context());
		subjectId = getMvpView().getPLayIntent().getStringExtra("subjectId");
		classId = getMvpView().getPLayIntent().getStringExtra("classId");
		courseBean = getMvpView().getPLayIntent().getStringExtra("courseBean");
		playCwId = getMvpView().getPLayIntent().getStringExtra("playCwId");
		version = getMvpView().getPLayIntent().getStringExtra("version");
		isFromLocal = getMvpView().getPLayIntent().getStringExtra("isFromLocal");
		courseDetailDB = new CourseDetailDB(getMvpView().context());
		cwStudyLogDB = new CwStudyLogDB(getMvpView().context());
		operaDB = new OperaDB(getMvpView().context());
		userId = SharedPrefHelper.getInstance(getMvpView().context()).getUserId();

		if (playCwId == null)
			lastVideoIdInDB = cwStudyLogDB.getLastCoursePlay(SharedPrefHelper.getInstance(getMvpView().context()).getUserId(), classId);
		sendUpload();
	}

	@Override
	public void getData() {
		if (yearInfo == null) {
			String currentYear = SharedPrefHelper.getInstance(getMvpView().context()).getCurYear();
			yearInfo = JSON.parseObject(currentYear, YearInfo.class);
		}
		getMvpView().showLoading();
		getMvpView().isOldPlay(false);
		if (subjectId == null || subjectId.equals(""))
			courseDetailInDB = courseDetailDB.find(SharedPrefHelper.getInstance(getMvpView().context()).getUserId(),
					SharedPrefHelper.getInstance(getMvpView().context()).getSubjectId(), classId);
		else
			courseDetailInDB = courseDetailDB.find(SharedPrefHelper.getInstance(getMvpView().context()).getUserId(),
					subjectId, classId);
		if (NetworkUtil.isNetworkAvailable(getMvpView().context()))
			apiModel.getData(ApiClient.getClientTimeout(30).playDetail(ParamsUtils.playDetail(yearInfo.getYearName(), classId)));
		else {
			if (courseDetailInDB == null) {
				if (!getMvpView().isPlayLocal())
					getMvpView().showNetError();
				else
					DialogManager.showMsgWithCloseAction(getMvpView().context(), "请先用网络打开，然后再离线观看", "提示", new DialogManager.CustomDialogCloseListener() {
						@Override
						public void yesClick() {
							((PlayActivity) getMvpView().context()).finish();
						}

						@Override
						public void noClick() {

						}
					});
			} else {
				courseDetail = JSON.parseObject(courseDetailInDB.getContentJson(), CourseDetail.class);
				preData();
				getMvpView().hideLoading();
			}
		}
	}

	@Override
	public void onError(Exception e) {
		getMvpView().loadError();
	}

	@Override
	public void setData(String obj) {
		try {
			BaseBean baseBean = JSON.parseObject(obj, BaseBean.class);
			int result = baseBean.getCode();
			if (result == 0 && baseBean.getResult().getCode() == Constants.RESULT_CODE) {
				result = Constants.RESULT_CODE;
			}
			if (result == Constants.RESULT_CODE) {
				courseDetail = JSON.parseObject(baseBean.getBody(), CourseDetail.class);
				courseDetail.setContentJson(JSON.toJSONString(courseDetail));
				courseDetail.setUserId(SharedPrefHelper.getInstance(getMvpView().context()).getUserId());
				courseDetail.setClassId(classId);
				int cwCount = 0;
				cwCount = courseDetail.getVideoDtos().size();
				courseDetail.setCwCount(cwCount);
				if (subjectId == null)
					courseDetail.setSubjectId(SharedPrefHelper.getInstance(getMvpView().context()).getSubjectId());
				else
					courseDetail.setSubjectId(subjectId);
				if (courseDetailInDB == null) {//数据库中不存在  插入数据库
					courseDetailDB.insert(courseDetail);
				} else {//存在 更新数据库
					courseDetail.setDbId(courseDetailInDB.getDbId());
					courseDetailDB.update(courseDetail);
				}
				preData();
				getMvpView().hideLoading();

			} else if (result == 9) {
				getMvpView().showOtherLogin();
			} else {
				getMvpView().loadError();
			}
		} catch (Exception e) {
			e.printStackTrace();
			getMvpView().loadError();
		}

	}

	private void preData() {
		for (int j = 0; j < courseDetail.getVideoDtos().size(); j++) {
			//调教设置课程介绍的url
			courseDetail.getVideoDtos().get(j).setCwDesc(courseDetail.getCwDesc());
			//TODO  给每个课设置具体的班次id以及当前是否已经播放完成
			if (courseDetail.getVideoDtos().get(j).getCwId().equals(courseDetail.getLastListenCourseWareId()))
				courseDetail.getVideoDtos().get(j).setBeginSec(courseDetail.getLastListenCWSecond());
			if (subjectId != null)
				courseDetail.getVideoDtos().get(j).setSubjectId(subjectId);
			else
				courseDetail.getVideoDtos().get(j).setSubjectId(
						SharedPrefHelper.getInstance(getMvpView().context()).getSubjectId());
			courseDetail.getVideoDtos().get(j).setClassId(classId);
			courseDetail.getVideoDtos().get(j).setCourseBean(courseBean);
			courseDetail.getVideoDtos().get(j).setCwBean
					(JSON.toJSONString(courseDetail.getVideoDtos().get(j)));
			courseDetail.getVideoDtos().get(j).setIsPlayFinished(
					cwStudyLogDB.isFinished(SharedPrefHelper.getInstance(getMvpView().context()).getUserId(),
							courseDetail.getVideoDtos().get(j).getCwId(), classId));
		}
		courseWareLists = new ArrayList<>();
		courseWareLists.addAll(courseDetail.getVideoDtos());

		List<CourseWare> courseWareList11 = new ArrayList<>();
		if (courseWareList11.size() > 1)
			courseWareLists.addAll(courseWareList11);
		if (playCwId == null)
			if (!TextUtils.isEmpty(courseDetail.getLastListenCourseWareId()))
				getMvpView().showCwData(courseWareLists, courseDetail.getLastListenCourseWareId());
			else
				getMvpView().showCwData(courseWareLists, lastVideoIdInDB);
		else
			getMvpView().showCwData(courseWareLists, playCwId);
	}


	/**
	 * 通过数据库查询数据
	 */
	public void findDataInDB() {
//        courseDetail = courseDetailDb.find(SharedPrefHelper.getInstance(getMvpView().context()).getUserId(),
//                getMvpView().myCourse().getCwCode());
//        if(courseDetail!=null){
//            courseDetail = JSON.parseObject(courseDetail.getCourseDetailJson(),CourseDetail.class);
//            for(int i=0;i<courseDetail.getChapterDtos().size();i++){
//                for(CourseWare courseWare : courseDetail.getChapterDtos().get(i).getVideoDtos()){
//                    courseWare.setCourseId(courseDetail.getCwCode());
//                    courseWare.setUserId(SharedPrefHelper.getInstance().getUserId());
//                    courseWare.setYears(SharedPrefHelper.getInstance().getCurYear());
//                    courseWare.setChapterId(courseDetail.getChapterDtos().get(i).getSmallListID());
//                }
//            }
//            handler.postDelayed(new Runnable() {
//                @Override
//                public void run() {
////                    adapter.setCourseListData(arrayListCourseVedio,courseDetail.getCwCode(),currentYear.getYearName());
////                    adapter.setCourseInfoUrl(courseDetail.getCwDesc());
//                    getMvpView().showData(courseDetail);
//                }
//            },500);
//        }else {
//            handler.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    getMvpView().showError("");
//                }
//            }, 500);
//            DialogManager.showMsgWithCloseAction(getMvpView().context(), "网络错误，加载数据失败", "", new DialogManager.CustomDialogCloseListener() {
//                @Override
//                public void yesClick() {
//                    ((PlayActivityNew)getMvpView().context()).finish();
//                }
//
//                @Override
//                public void noClick() {
//
//                }
//            });
//        }
	}

	public void checkMachineSign(final DownloadView downloadView) {
		Call<String> call = ApiClient.getClient().checkMachine(ParamsUtils.getInstance(getMvpView().context()).checkMachine());
		call.enqueue(new Callback<String>() {
			@Override
			public void onResponse(Call<String> call, Response<String> response) {
				if (response.isSuccessful()) {
					String str = response.body();
					try {
						com.alibaba.fastjson.JSONObject json = JSON.parseObject(str);
						com.alibaba.fastjson.JSONObject check_result = json.getJSONObject("result");
						if (check_result != null) {
							String message = check_result.getString("msg");
							int code = check_result.getInteger("code");
							if (code == Constants.RESULT_CODE) {
								downloadView.isCheckMachineOk(message, true);
							} else {
								downloadView.isCheckMachineOk(message, false);
							}
						}

					} catch (Exception e) {
						Toast.makeText(getMvpView().context(), "数据异常", Toast.LENGTH_SHORT).show();
					}
				} else {
					Toast.makeText(getMvpView().context(), "数据异常", Toast.LENGTH_SHORT).show();
				}
			}

			@Override
			public void onFailure(Call<String> call, Throwable t) {
				getMvpView().hideLoading();
				Toast.makeText(getMvpView().context(), "请检查网络是否连接", Toast.LENGTH_SHORT).show();
			}
		});
	}

	public void postOperat() {
		final List<Opera> operas = operaDB.findOperas(userId);
		if (operas == null || operas.size() == 0) {
			return;
		}

		Call<String> call = ApiClient.getClient().postOperates(ParamsUtils.getInstance(getMvpView().context()).postDatas(operas));
		call.enqueue(new Callback<String>() {
			@Override
			public void onResponse(Call<String> call, Response<String> response) {
				if (response.isSuccessful()) {
					String obj = response.body();
					BaseBean baseBean = JSON.parseObject(obj, BaseBean.class);
					if (baseBean != null) {
						if (baseBean.getResult().getCode() == 1) {
							operaDB.deleteAll();
						}
					}
				}
			}

			@Override
			public void onFailure(Call<String> call, Throwable t) {
				getMvpView().hideLoading();
			}
		});
	}

	/**
	 * 是否是播放首页点击过来的视频
	 */
	public boolean isPlayMainVideo() {
		return playCwId != null && !playCwId.equals("");
	}

	public void setPlayCwId() {
		playCwId = null;
	}


	private String[] ips;//解析的ip集合
	String domainame;
	String playUrl;

	public void analysisIp(final CourseWare courseWare) {
		try {
			playUrl = courseWare.getMobileVideoUrl();
			domainame = getTopDomainWithoutSubdomain(playUrl);
		} catch (Exception e) {

		}
		if (TextUtils.isEmpty(domainame)) {
			return;
		}
		String s = encrypt(domainame);
		String ip = "http://119.29.29.29/d?id=245&dn=" + s;
		Call<String> call = ApiClient.getClient().analysisDNS(ip);

		call.enqueue(new Callback<String>() {
			@Override
			public void onResponse(Call<String> call, Response<String> response) {
				if (response.isSuccessful()) {
					String t = response.body();
					t = t.replaceAll("(\r\n|\r|\n|\n\r)", "");
					t = decrypt(t);
					if (!TextUtils.isEmpty(t)) {
						ips = t.split(";");
						if (!TextUtils.isEmpty(ips[0])) {
//							operaDB.add(courseWare, "playError", ips[0], System.currentTimeMillis() + "");// TODO: 2017/11/6  
						}
					}
				} else {
//					operaDB.add(courseWare, "playError", "", System.currentTimeMillis() + "");// TODO: 2017/11/6  
				}
			}

			@Override
			public void onFailure(Call<String> call, Throwable t) {
				getMvpView().hideLoading();
				operaDB.add(courseWare, "playError", "", System.currentTimeMillis() + "");
			}
		});
	}

	/**
	 * 域名加密
	 *
	 * @param domain
	 * @return
	 */
	public String encrypt(String domain) {
		String crypt = "";
		try {
			//初始化密钥
			SecretKeySpec keySpec = new SecretKeySpec("i_PqWF_o".getBytes("utf-8"), "DES");
			//选择使用 DES 算法，ECB 方式，填充方式为 PKCS5Padding
			Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
			//初始化
			cipher.init(Cipher.ENCRYPT_MODE, keySpec);
			//获取加密后的字符串
			byte[] encryptedString = cipher.doFinal(domain.getBytes("utf-8"));
			crypt = bytesToHexString(encryptedString);
		} catch (Exception e) {
			return crypt;
		}
		return crypt;
	}

	/**
	 * 域名解密
	 *
	 * @param data
	 * @return
	 */
	public String decrypt(String data) {
		String decrypt = "";
		try {
			SecretKeySpec keySpec = new SecretKeySpec("i_PqWF_o".getBytes("utf-8"), "DES");
			Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, keySpec);
			byte[] decrypts = cipher.doFinal(hexStringToByte(data));
			decrypt = new String(decrypts, "utf-8");
		} catch (Exception e) {
			return decrypt;
		}
		return decrypt;
	}

	/**
	 * 2进制byte[]转成16进制字符串
	 *
	 * @param src
	 * @return
	 */
	public static String bytesToHexString(byte[] src) {
		StringBuilder stringBuilder = new StringBuilder();
		if (src == null || src.length <= 0) {
			return null;
		}
		for (int i = 0; i < src.length; i++) {
			int v = src[i] & 0xFF;
			String hv = Integer.toHexString(v);
			if (hv.length() < 2) {
				stringBuilder.append(0);
			}
			stringBuilder.append(hv);
		}
		return stringBuilder.toString();
	}

	/**
	 * 16进制字符串转成byte[]
	 *
	 * @param hex
	 * @return
	 */
	public static byte[] hexStringToByte(String hex) {
		int len = (hex.length() / 2);
		byte[] result = new byte[len];
		char[] achar = hex.toCharArray();
		for (int i = 0; i < len; i++) {
			int pos = i * 2;
			result[i] = (byte) (toByte(achar[pos]) << 4 | toByte(achar[pos + 1]));
		}
		return result;
	}

	private static byte toByte(char c) {
		byte b = (byte) "0123456789abcdef".indexOf(c);
		return b;
	}

	public static String getTopDomainWithoutSubdomain(String url)
			throws MalformedURLException {

		String host = new URL(url).getHost().toLowerCase();// 此处获取值转换为小写
		Pattern pattern = Pattern
				.compile("[^//]*?\\.(com|cn|net|org|biz|info|cc|tv)");
		Matcher matcher = pattern.matcher(host);
		while (matcher.find()) {
			return matcher.group();
		}
		return null;
	}

	private PlayParamsDB playParamsDB;

	/**
	 * 字符串转换成为16进制(无需Unicode编码)
	 *
	 * @param str
	 * @return
	 */
	public static String str2HexStr(String str) {
		char[] chars = "0123456789ABCDEF".toCharArray();
		StringBuilder sb = new StringBuilder("");
		byte[] bs = str.getBytes();
		int bit;
		for (int i = 0; i < bs.length; i++) {
			bit = (bs[i] & 0x0f0) >> 4;
			sb.append(chars[bit]);
			bit = bs[i] & 0x0f;
			sb.append(chars[bit]);
			// sb.append(' ');
		}
		return sb.toString().trim();
	}


	public void downLoad(final String path, final CourseWare courseWare) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					String m3u8path = getPath(courseWare);
					File des = new File(m3u8path);
					if (!des.exists()) {
						des.mkdirs();
					}
					File videoFile = new File(m3u8path + "keyText.txt");
					FileOutputStream fos = new FileOutputStream(videoFile);
					URL url = new URL(path);
					System.out.println("下载路径:" + path);
					HttpURLConnection conn = (HttpURLConnection) url.openConnection();
					conn.setRequestMethod("GET");
					conn.setConnectTimeout(5000);
					InputStream inputStream = conn.getInputStream();

					byte[] buffer = new byte[1024];
					int len = 0;
					int total = 0;
					while ((len = inputStream.read(buffer)) != -1) {
						fos.write(buffer, 0, len);
					}
					fos.flush();
					fos.close();
					cw = courseWare;
					handler.sendEmptyMessage(1000);
				} catch (Exception e) {
					//上传错误至服务器
					e.printStackTrace();
					handler.sendEmptyMessage(2000);
					
				}

			}
		}).start();

	}

	public void getPlayKey(final CourseWare courseWare) {
		Call<String> call =ApiClient.getClient().downloadM3U8(courseWare.getVideoUrl());
		call.enqueue(new Callback<String>() {
			@Override
			public void onResponse(Call<String> call, Response<String> response) {
				if (response.isSuccessful()) {
					String str = response.body();
					try {
						if(str.contains("URI=")){
							String[] strs=str.split("URI=");
							String str1=strs[1].split("#EXTINF")[0];
							String url=str1.substring(1,str1.lastIndexOf("key"));
							url=url+"key";
							downLoad(url, courseWare);
						}else{
							cw = courseWare;
							handler.sendEmptyMessage(1000);
						}
						
					} catch (Exception e) {
						getMvpView().playError("analysis failed");
					}
				} else {
					getMvpView().playError("数据错误");
				}
			}

			@Override
			public void onFailure(Call<String> call, Throwable t) {
				getMvpView().hideLoading();
			}
		});

	}

	private void getPreData(CourseWare courseWare, String key) {// TODO: 2017/11/8  
		cw = courseWare;
		downloadM3U8(courseWare, courseWare.getVideoUrl(), key);
	}


	//获取播放地址
	public void getPlayUrl(final CourseWare courseWare) {
		Call<String> call = ApiClient.getClient().getPlayUrl(ParamsUtils.getInstance(getMvpView().context()).getPlayPath(courseWare.getCwId()));
		call.enqueue(new Callback<String>() {
			@Override
			public void onResponse(Call<String> call, Response<String> response) {
				if (response.isSuccessful()) {
					String str = response.body();
					try {
						JSONObject total = new JSONObject(str);
						String code = total.optString("code");
						if (!code.equals("" + Constants.RESULT_CODE)) {
							return;
						}
						PlayUrlBean playUrlBean = new PlayUrlBean();

						PlayUrlBean.VideoBean videoBean = new PlayUrlBean.VideoBean();
						PlayUrlBean.VideoBean.SdBean sdModel = new PlayUrlBean.VideoBean.SdBean();

						JSONObject body = total.getJSONObject("body");
						JSONObject video = body.getJSONObject("video");

						String key = body.optString("cipherkeyDeal");
						playUrlBean.setCipherkeyDeal(key);

						JSONObject sd = video.getJSONObject("sd");

						JSONArray three = sd.optJSONArray("1.5");
						JSONArray two = sd.optJSONArray("1.2");
						JSONArray one = sd.optJSONArray("1.0");

						/**
						 *设置sd（标清）不同倍速url
						 */
						List<String> sdthree = new ArrayList();
						for (int i = 0; i < three.length(); i++) {
							String rrr = (String) three.get(i);
							sdthree.add(rrr);
						}
						sdModel.setThree(sdthree);

						List<String> sdtwo = new ArrayList();
						for (int i = 0; i < two.length(); i++) {
							String rrr = (String) two.get(i);
							sdtwo.add(rrr);
						}
						sdModel.setTwo(sdtwo);

						List<String> sdone = new ArrayList();
						for (int i = 0; i < one.length(); i++) {
							String rrr = (String) one.get(i);
							sdone.add(rrr);
						}
						sdModel.setOne(sdone);
						videoBean.setSd(sdModel);
						playUrlBean.setVideo(videoBean);

						JSONObject cipherkeyVo = body.getJSONObject("cipherkeyVo");

						String app = cipherkeyVo.optString("app");
						String type = cipherkeyVo.optString("type");
						String vid = cipherkeyVo.optString("vid");
						String vkey = cipherkeyVo.optString("key");
						String vcode = cipherkeyVo.optString("code");
						String message = cipherkeyVo.optString("message");
						String lecture = body.optString("handOut");
						playParamsDB.add(userId, courseWare.getCwId(), app, type, vid, vkey, vcode, message);
						courseWare.setMobileLectureUrl(lecture);
						courseWare.setMobileVideoUrl(playUrlBean.getVideo().getSd().getOne().get(0));
						cw = courseWare;
						String jm = new String(AESHelper.decrypt(Base64.decode(vkey, Base64.DEFAULT),
								SignUtils.getKey(app, vid, Integer.parseInt(type)).getBytes(),
								SignUtils.getIV(vid).getBytes()));
						downloadM3U8(courseWare, playUrlBean.getVideo().getSd().getOne().get(0), jm);
					} catch (Exception e) {
						getMvpView().playError("analysis failed");
					}
				} else {
					getMvpView().playError("数据错误");
				}
			}

			@Override
			public void onFailure(Call<String> call, Throwable t) {
				getMvpView().hideLoading();
			}
		});
	}

	FileWriter wf;
	CourseWare cw;

	public void downloadM3U8(final CourseWare courseWare, final String path, final String key) {

		Call<String> call = ApiClient.getClient().downloadM3U8(path);
		call.enqueue(new Callback<String>() {
			@Override
			public void onResponse(Call<String> call, Response<String> response) {
				if (response.isSuccessful()) {
					String str = response.body();
					try {
						String m3u8path = getPath(courseWare);
						File des = new File(m3u8path);
						if (!des.exists()) {
							des.mkdirs();
						}
						File videoFile = new File(m3u8path + "online.m3u8");
						wf = new FileWriter(videoFile);
						wf.write(str);
					} catch (Exception e) {
						getMvpView().playError("analysis failed");
					} finally {
						if (wf != null) {
							try {
								wf.close();
								int index = path.lastIndexOf("/") + 1;
								String rootUrl = path.substring(0, index);
								getKeyTxt(rootUrl, getPath(courseWare) + "online.m3u8", key);
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					}
				} else {
					getMvpView().playError("m3u8数据错误");
				}
			}

			@Override
			public void onFailure(Call<String> call, Throwable t) {
				getMvpView().playError("网络错误");
			}
		});
	}

	/**
	 * 获取并保存Key到文件中
	 *
	 * @param urlHead
	 */
	public void getKeyTxt(final String urlHead, final String m3u8Path, final String keyText) {
		try {

			String keyUrl = null;
			FileOutputStream fos = null;
			File m3u8File = new File(m3u8Path);
			FileReader fileReader = new FileReader(m3u8File);
			BufferedReader reader11 = new BufferedReader(fileReader);
			String line1 = null;
			StringBuffer stringBuffer1 = new StringBuffer();
			String line_copy = null;
			boolean isNeedKey = false;//是否需要进行加解密
			while ((line1 = reader11.readLine()) != null) {
				line_copy = line1;
				if (line1.contains("EXT-X-KEY"))
					isNeedKey = true;
				if (line1.contains("EXT-X-KEY") && !line1.contains(Constans.M3U8_KEY_SUB)) {
					//如果当前视频需要进行解密，则修改m3u8文件中EXT-X-KEY对应的值指向本地key文件，来标示需要进行解密
					String[] keyPart = line1.split(",");
					String keyUrll = keyPart[1].split("URI=")[1].trim();
					keyUrl = keyUrll.substring(1, keyUrll.length() - 1);
//					line_copy = keyPart[0] + Constans.M3U8_KEY_SUB + getKeyPath(cw) + "keyText.txt\"," + keyPart[2];// TODO: 2017/11/5  
//					line_copy = keyPart[0] + Constans.M3U8_KEY_SUB + getKeyPath(cw) + "keyText.txt\""+",IV=0x"+IVKey;
//					line_copy = keyPart[0] + Constans.M3U8_KEY_SUB +  "video.key\"";
//					line_copy = keyPart[0] + Constans.M3U8_KEY_SUB + getKeyPath(cw) + "keyText.txt\"";// TODO: 2017/11/10  
					line_copy = keyPart[0] + Constans.M3U8_KEY_SUB + getKeyPath(cw) + "keyText.txt\"";
				} else if (line1.contains(".ts") && !line1.contains("http:") && isNeedKey) {
					line_copy = urlHead + line_copy;//补全ts的网络路径
				}else if (line1.contains(".ts") && !line1.contains("http:") && !isNeedKey) {
					line_copy = urlHead + line_copy;//补全ts的网络路径
				}
				stringBuffer1.append(line_copy);
				stringBuffer1.append("\r\n");
			}
			reader11.close();
			FileWriter writer = new FileWriter(m3u8File, false);
			BufferedWriter writer2 = new BufferedWriter(writer);
			writer2.write(stringBuffer1.toString());
			writer2.flush();
			writer2.close();
			getMvpView().playNew(cw);
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    getMvpView().playNew(cw);
//                }
//            },2000);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private String getPath(CourseWare courseWare) {
		String userId = SharedPrefHelper.getInstance(getMvpView().context()).getUserId();
		String desPath = FileUtil.getDownloadPath(getMvpView().context());

		return desPath + userId + "_" + courseWare.getExamId() + "_" + courseWare.getSubjectId() + "_" + courseWare.getClassId() + "_" + courseWare.getSectionId() + "_" + courseWare.getCwId() + "/" + "playOnline/";

	}

	private String getKeyPath(CourseWare courseWare) {
		String userId = SharedPrefHelper.getInstance(getMvpView().context()).getUserId();

		return userId + "_" + courseWare.getExamId() + "_" + courseWare.getSubjectId() + "_" + courseWare.getClassId() + "_" + courseWare.getSectionId() + "_" + courseWare.getCwId() + "/" + "playOnline/";

	}
}
