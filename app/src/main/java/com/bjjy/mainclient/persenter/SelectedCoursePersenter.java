package com.bjjy.mainclient.persenter;


import android.text.TextUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bjjy.mainclient.phone.view.goodsubject.views.SelectedView;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.bean.courselect.DiscountCode;
import com.dongao.mainclient.model.bean.courselect.Goods;
import com.dongao.mainclient.model.bean.courselect.GoodsInfos;
import com.dongao.mainclient.model.bean.courselect.GoodsParams;
import com.dongao.mainclient.model.bean.courselect.GoodsResult;
import com.dongao.mainclient.model.remote.ApiClient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 登录UI
 */
public class SelectedCoursePersenter extends BasePersenter<SelectedView> {
    private List<GoodsInfos> mList;
    private List<DiscountCode> usedCards;

    @Override
    public void attachView(SelectedView centerView) {
        super.attachView(centerView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    @Override
    public void getData() {

        getMvpView().showLoading();
//        String accessToken = SharedPrefHelper.getInstance(getMvpView().context()).getAccessToken();
        HashMap<String, String> params = new HashMap<>();
        GoodsParams goods = new GoodsParams();
        goods.setUserId("16923559");
        goods.setCampaignId("");
        goods.setGoodsInfos(initData(getMvpView().listData()));
        goods.setDiscountCodes(initCodes(getMvpView().listCards()));
        String examids = JSON.toJSONString(goods);
        params.put("time", "14021272241");
        params.put("version", "1.0");
        params.put("random", "263198958da6fcc76509565ed6aa067f");
        params.put("reqSource", "m");
        params.put("params", examids);
        apiModel.getData(ApiClient.getClient().getGoodsDiscount(params));

    }

    private List<GoodsInfos> initData(List<Goods> goods) {
        List<GoodsInfos> list = new ArrayList();
        for (int i = 0; i < goods.size(); i++) {
            GoodsInfos goodsInfos = new GoodsInfos();
            goodsInfos.setId(goods.get(i).getGoodsId());
            goodsInfos.setNum(1 + "");
            goodsInfos.setUpgradeGoods("");
            list.add(goodsInfos);
        }
        return list;
    }

    private List<DiscountCode> initCodes(List<String> cards) {
        List<DiscountCode> list = new ArrayList();
        if (cards != null && cards.size() > 0) {
            for (int i = 0; i < cards.size(); i++) {
                DiscountCode code = new DiscountCode();
                code.setDiscountCode(cards.get(i));
                if (getMvpView().listUsedCards() != null && getMvpView().listUsedCards().size() > 0) {
                    if (getMvpView().listUsedCards().contains(cards.get(i))) {
                        code.setIsUsed("true");
                    } else {
                        code.setIsUsed("true");
                    }
                } else {
                    code.setIsUsed("true");
                }
                list.add(code);
            }

        }
        return list;
    }

    @Override
    public void setData(String obj) {
        GoodsResult result = null;
        try {
            if (TextUtils.isEmpty(obj)) {
                getMvpView().showError("");
                return;
            }
            JSONObject data = JSON.parseObject(obj);
            if (data.getIntValue("status") != 1000) {
                return;
            }
            String resultcode = data.getString("result");
            result = JSON.parseObject(resultcode, GoodsResult.class);
            mList = result.getGoodsVos();
            usedCards = result.getDiscountCodes();
        } catch (Exception e) {
            e.printStackTrace();
        }

        getMvpView().setAdapter(mList);
        getMvpView().setResult(result);
        getMvpView().setUsedCards(usedCards);
        //courseView.refresh();
    }
}
