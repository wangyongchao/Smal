package com.bjjy.mainclient.phone.view.question.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dongao.mainclient.model.bean.question.QuestionKonwLedge;
import com.bjjy.mainclient.phone.R;

import java.util.List;

/**
 * Created by fengzongwei on 2016/11/29 0029.
 */
public class QuestionSelectExmAdapter extends BaseAdapter{

    private Context context;
    private List<QuestionKonwLedge> questionKonwLedges;
    public QuestionSelectExmAdapter(Context context,List<QuestionKonwLedge> questionKonwLedges){
        this.context = context;
        this.questionKonwLedges = questionKonwLedges;
    }

    @Override
    public int getCount() {
        return questionKonwLedges.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.question_select_exm_lv_item,parent,false);
            viewHolder = new ViewHolder();
            viewHolder.textView = (TextView)convertView.findViewById(R.id.question_select_exm_lv_item_tv);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder)convertView.getTag();
        }
        if(!TextUtils.isEmpty(questionKonwLedges.get(position).getQuestionNum()) && !questionKonwLedges.get(position).getQuestionNum().equals("null"))
            viewHolder.textView.setText(questionKonwLedges.get(position).getName() + " 第" + questionKonwLedges.get(position).getQuestionNum() + "题");
        else
            viewHolder.textView.setText(questionKonwLedges.get(position).getName());
        return convertView;
    }
    class ViewHolder{
        TextView textView;
    }
}
