package com.bjjy.mainclient.phone.view.myorder.adpter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bjjy.mainclient.phone.view.payment.PayWayActivity;
import com.dongao.mainclient.model.bean.myorder.MyOrderItem;
import com.dongao.mainclient.model.bean.myorder.MyOrderProduct;
import com.bjjy.mainclient.phone.R;

import java.util.List;

/**
 * Created by fengzongwei on 2016/4/6.
 */
public class MyOrderFragmentAdapter extends BaseAdapter{

    private Context context;

    private List<MyOrderItem> list;
    private boolean isPay = false;
    public MyOrderFragmentAdapter(Context context,List<MyOrderItem> list,boolean isPay){
        this.context = context;
        this.list = list;
        this.isPay = isPay;
    }
    @Override
    public int getCount() {
        return list.size();
//        return 1;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.my_order_item,parent,false);
            viewHolder = new ViewHolder();
            viewHolder.textView_createTime = (TextView)convertView.findViewById(R.id.my_oreder_item_createTime_tv);
            viewHolder.textView_orderId = (TextView)convertView.findViewById(R.id.my_oreder_item_orderId_tv);
            viewHolder.textView_payMoney = (TextView)convertView.findViewById(R.id.my_oreder_item_payMoney_tv);
            viewHolder.textView_payStatus = (TextView)convertView.findViewById(R.id.my_oreder_item_payStatus_tv);
            viewHolder.linearLayout_pay = (LinearLayout)convertView.findViewById(R.id.my_order_item_payBody_ll);
            viewHolder.linearLayout_product = (LinearLayout)convertView.findViewById(R.id.my_order_item_productBody_ll);
            viewHolder.button_pay = (TextView)convertView.findViewById(R.id.my_oreder_item_pay_bt);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder)convertView.getTag();
        }

        if(list.get(position).getPayStatus().equals("3")) {
            viewHolder.linearLayout_pay.setVisibility(View.GONE);
            viewHolder.textView_payStatus.setTextColor(Color.parseColor("#44db5e"));
            viewHolder.textView_payStatus.setText("已付款");
        }else{
            viewHolder.textView_payStatus.setTextColor(Color.parseColor("#d54636"));
            viewHolder.textView_payStatus.setText("待付款");
        }

        viewHolder.button_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context,PayWayActivity.class);
                intent.putExtra("orderId",list.get(position).getId());
                intent.putExtra("totalPrice",list.get(position).getActualOrderAmt());
                context.startActivity(intent);
            }
        });

        viewHolder.textView_orderId.setText(list.get(position).getId());
        viewHolder.textView_payMoney.setText("¥"+list.get(position).getActualOrderAmt());
        viewHolder.textView_createTime.setText(list.get(position).getOrderDate());
        addProduct(list.get(position).getOrderItemVos(),viewHolder.linearLayout_product);
        return convertView;
    }

    private void addProduct(List<MyOrderProduct> myOrderProduct,LinearLayout linearLayout){
        linearLayout.removeAllViews();
        MyOrderProduct myOrderDetailProduct = null;
        TextView  textView = null;
        for(int i=0;i<myOrderProduct.size();i++){
            myOrderDetailProduct = myOrderProduct.get(i);
            textView =  new TextView(context);
            textView.setSingleLine(true);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams
                    (LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
            if(i!=0){
                params.setMargins(0,15,0,0);
            }
            textView.setLayoutParams(params);
            textView.setText(myOrderDetailProduct.getGoodsName());
            linearLayout.addView(textView);
        }
    }

    static class ViewHolder{
        TextView textView_orderId,textView_payStatus,textView_payMoney,textView_createTime;
        LinearLayout linearLayout_pay,linearLayout_product;
        TextView button_pay;
    }

}
