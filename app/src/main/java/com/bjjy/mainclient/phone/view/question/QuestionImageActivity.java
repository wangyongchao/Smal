package com.bjjy.mainclient.phone.view.question;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;

import com.bjjy.mainclient.phone.app.BaseActivity;
import com.bjjy.mainclient.phone.view.question.widget.ZoomImageView;
import com.bjjy.mainclient.phone.R;

import java.io.IOException;
import java.io.InputStream;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by dell on 2016/7/26 0026.
 */
public class QuestionImageActivity extends BaseActivity {
    @Bind(R.id.question_img_iv)
	ZoomImageView zoomImageView;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.question_img);
        zoomImageView = (ZoomImageView) findViewById(R.id.question_img_iv);
        try {
            InputStream is = getAssets().open(getIntent().getStringExtra("imagePath"));
            Bitmap bmp = BitmapFactory.decodeStream(is);
            zoomImageView.setImageBitmap(bmp);
        } catch (IOException e) {
            e.printStackTrace();
        }
        zoomImageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(QuestionImageActivity.this, MyQuestionListActivity.class);
                startActivity(i);
                finish();
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent i = new Intent(this, MyQuestionListActivity.class);
            startActivity(i);
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @OnClick(R.id.question_img_iv) void close(){
        finish();
    }

    @Override
    public void initView() {

    }

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View v) {

    }
}
