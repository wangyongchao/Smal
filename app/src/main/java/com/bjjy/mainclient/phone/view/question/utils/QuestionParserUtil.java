package com.bjjy.mainclient.phone.view.question.utils;



import com.dongao.mainclient.model.bean.question.Question;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 获取分组后的答疑集合工具类
 * @author fengzongwei
 *
 */
public class QuestionParserUtil{

	/**
	 * 获取分组后的答疑集合
	 * @param questionList 传入借口返回的questionAnswerList
	 * @param isRecomm  是否是推荐答疑
	 * @return
	 */
	public static List<Question> parseQueArray(JSONArray questionList,boolean isRecomm){
		ArrayList<Question> arrayListBegin = new ArrayList<Question>();
		if(questionList.length()>0){
			JSONObject object = null;
			Question question = null;
			try {
				for(int i=0;i<questionList.length();i++){
					object = questionList.getJSONObject(i);
					question = new Question();
					question.setAskId(object.optString("id"));
					question.setEssence(object.optString("essence"));
					question.setEssenceTitle(object.optString("essenceTitle"));
					question.setTitle(object.optString("title"));
					question.setContent(object.optString("content"));
//					question.setAnswerStatus(object.optString("answerStatus"));
					question.setAnswer(object.optString("answer"));
					question.setExamQuestionId(object.optString("examQuestionId"));
					question.setHits(object.optString("hits"));
					question.setSubjectName(object.optString("subjectName"));
					question.setParentId(object.optString("parentId"));
					question.setCreateTime(object.optString("createTime"));
					question.setAnswerTime(object.optString("answerTime"));
					question.setInterval(object.optString("interval"));
					question.setUserId(object.optString("userId"));
					arrayListBegin.add(question);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		if(!isRecomm)
			return getFinalList(arrayListBegin);
		else
			return arrayListBegin;
	}
	/**
	 * 根据未分组解析后的集合进行分组操作
	 * @param arrayList 未分组时的集合，即解析后生成的原始集合
	 * @return 分组后的集合  父问题跟追问在集合中的层级相同，排列为父问题在前，追问在后边紧随排列   A-a-a-B-b-b-C-c-c
	 */
	private static ArrayList<Question> getFinalList(ArrayList<Question> arrayList){
		if(arrayList.size()>0){
			ArrayList<Question> finalList = new ArrayList<Question>();
			String bigId ;
			ArrayList<Question> arQuestions;
			for(int i=0;i<arrayList.size();i++){
				if(isParent(arrayList.get(i).getParentId())){
					bigId = arrayList.get(i).getAskId();
					arQuestions = new ArrayList<Question>();
					for(int j = 0;j<arrayList.size();j++){
						if(arrayList.get(j).getParentId()!=null && arrayList.get(j).getParentId().equals(bigId)){
							arQuestions.add(arrayList.get(j));
						}
					}
					arrayList.get(i).setChildQuestions(arQuestions);
					arrayList.get(i).setChildQuestionCount(arQuestions.size());
//					arrayList.get(i).setCanAsk(isCanAsk(arQuestions,bigId));
					finalList.add(arrayList.get(i));
				}
			}
			ArrayList<Question> sortList = new ArrayList<Question>();
			List<Question> neiList;
			for(int i=0;i<finalList.size();i++){
				sortList.add(finalList.get(i));
				neiList = finalList.get(i).getChildQuestions();
				if(neiList!=null){
					for(int j=0;j<neiList.size();j++){
						sortList.add(neiList.get(j));
					}
				}
			}
			return sortList;
		}else{
			return arrayList;
		}
	}

	/**
	 * 判断当前该提问是否可以进行追问
	 * @param parentId
	 * @return
	 */
	private static boolean isCanAsk(ArrayList<Question> questionList,String parentId){
		boolean isCan = true;
		for(int i=0;i<questionList.size();i++){
			if(questionList.get(i).getParentId()!=null && questionList.get(i).getParentId().equals(parentId)){
				if(questionList.get(i).getReadStatus().equals("0"))
					isCan = false;
			}
		}
		return isCan;
	}

	/**
	 * 判断当前的问题对象是否是基础问题，即用户对此题的首次提问问题
	 * @param parentId 判断的依据，为null或者为""是则表示该题为基础问题  否则为追问
	 * @return
	 */
	public static boolean isParent(String parentId){
		return parentId == null || parentId.equals("");
	}
	
}
