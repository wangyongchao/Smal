package com.bjjy.mainclient.phone.wxapi;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.bjjy.mainclient.phone.event.PaySuccessEvent;
import com.dongao.mainclient.core.payment.payutils.Constants;
import com.bjjy.mainclient.phone.R;
import com.tencent.mm.sdk.constants.ConstantsAPI;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import org.greenrobot.eventbus.EventBus;

import butterknife.ButterKnife;

public class WXEntryActivity extends Activity implements IWXAPIEventHandler {


	private static final String TAG = "MicroMsg.SDKSample.WXPayEntryActivity";

	private IWXAPI api;
//	@Bind(R.id.top_title_left)
//	ImageView top_title_left;
//	@Bind(R.id.top_title_text)
//	TextView top_title_text;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pay_result);
		ButterKnife.bind(this);
		api = WXAPIFactory.createWXAPI(this, Constants.APP_ID);
		api.handleIntent(getIntent(), this);
		EventBus.getDefault().post(new PaySuccessEvent());
//		top_title_left.setVisibility(View.VISIBLE);
//		top_title_text.setText("支付成功");
	}
//	@OnClick(R.id.top_title_left) void onBack() {
//		onBackPressed();
//	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
		api.handleIntent(intent, this);
	}

	@Override
	public void onReq(BaseReq req) {
	}

	@Override
	public void onResp(BaseResp resp) {
		Toast.makeText(this,"支付成功",Toast.LENGTH_SHORT).show();
		if (resp.getType() == ConstantsAPI.COMMAND_PAY_BY_WX) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("提示");
			builder.setMessage(getString(R.string.cancel, String.valueOf(resp.errCode)));
			builder.show();
		}
	}
}