package com.bjjy.mainclient.phone.view.setting.update;


/**
 * Created by wyc on 14-5-8.
 */
public class UpdateInfo {

   // public static final String VERSION_URL = "http://img.dongao.cn/appupdate/appupdate/version_android_phone_mc.xml";
    private String versionCode; //版本号
    private String versionName; //版本名称
    private String appUrl; //下载URL
    private String changeLog; //版本更新描述
    private String isForce; //是否强制升级
    
    private String id;
    
    public String getCurrentVersion() {
        return versionCode;
    }

    public void setCurrentVersion(String currentVersion) {
        this.versionCode = currentVersion;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public String getDownloadUrl() {
        return appUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.appUrl = downloadUrl;
    }

    public String getDescrip() {
        return changeLog;
    }

    public void setDescrip(String descrip) {
        this.changeLog = descrip;
    }

    public String getIsUpdate() {
        return isForce;
    }

    public void setIsUpdate(String isUpdate) {
        this.isForce = isUpdate;
    }

    public String getAppUrl() {
        return appUrl;
    }

    public void setAppUrl(String appUrl) {
        this.appUrl = appUrl;
    }

    public String getChangeLog() {
        return changeLog;
    }

    public void setChangeLog(String changeLog) {
        this.changeLog = changeLog;
    }

    public String getIsForce() {
        return isForce;
    }

    public void setIsForce(String isForce) {
        this.isForce = isForce;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(String versionCode) {
        this.versionCode = versionCode;
    }
}
