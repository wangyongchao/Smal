package com.bjjy.mainclient.phone.download;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.bjjy.mainclient.phone.app.AppContext;
import com.bjjy.mainclient.phone.download.db.DownloadDB;
import com.bjjy.mainclient.phone.download.db.OperaDB;
import com.bjjy.mainclient.phone.utils.NetworkUtil;
import com.bjjy.mainclient.phone.utils.StringUtil;
import com.bjjy.mainclient.phone.view.play.utils.PrePlayVideoCenter;
import com.dongao.mainclient.core.util.FileUtil;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;

import org.apache.http.client.methods.HttpGet;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;


public class DownloadExcutor {

	FileWriter wf;
	List<String> list = new ArrayList<String>();
	List<String> finish = new ArrayList<String>();
	String requestpath = "";
	private Context context;
	private boolean downloadflag;
	private static DownloadExcutor excutor;
	private Boolean isError = false;
	private int errorCode;
	public DownloadTask task;
	private int times = 0;

	// private ChangeDB db_item;
	private DownloadDB db_main;
	private OperaDB operaDB;
	AppContext application;
	private PrePlayVideoCenter prePlayVideoCenter;
	private boolean isCrypt;//根据m3u8判断是否为新视频（无key为老，有key为新）(默认为false)

	private DownloadExcutor(Context context) {
		this.context = context;
		// db_item=new ChangeDB(context);
		db_main = new DownloadDB(context);
		operaDB = new OperaDB(context);
		application = AppContext.getInstance();
		prePlayVideoCenter = new PrePlayVideoCenter(AppContext.getInstance(), new PrePlayVideoCenter.PrePlayListener() {
			@Override
			public void preSuccess(String vedioPath) {

			}

			@Override
			public void preFailed() {

			}
		});
	}

	public static DownloadExcutor getInstance(Context context) {
		if (excutor == null) {
			excutor = new DownloadExcutor(context);
		}
		return excutor;
	}

	public void download(DownloadTask task) {
		this.task = task;
		isCrypt = false;
		downloadflag = true;
		list.clear();
		finish.clear();
		if (db_main.getState(SharedPrefHelper.getInstance(context).getUserId(), task.getCourseId(), task.getCourseWareId()) == Constants.STATE_DownLoaded) {
			return;
		}
		SharedPrefHelper.getInstance(context).setDownloadTaskId(task.getCourseWareId());


		resolveLecture();
		resolveIntroduce();// TODO: 2017/11/17  
//        analysisDns();
		analysis();
		checkStorage();
//		resolveCaption();
		if (downloadflag) {
			downloadList();
		} else {
			db_main.updateState(SharedPrefHelper.getInstance(context).getUserId(), task.getCourseId(), task.getCourseWareId(),
					Constants.STATE_Error);

			Handler handler = application.getHandler();
			if (handler != null) {
				Message msg = Message.obtain();
				msg.obj = 50;
				handler.sendMessage(msg);
			}
			if (AppContext.getInstance().getHandlerDetail() != null) {
				AppContext.getInstance().getHandlerDetail()
						.sendEmptyMessage(0);
			}
		}
	}

//    private void analysisDns() {
//        requestpath = task.getVideo_url();
//        requestpath = setHostIp(requestpath);
//        if (TextUtils.isEmpty(requestpath)) {
//            requestpath = task.getVideo_url();
//        }
//    }

//    private String domainame;
//    private String[] ips;//解析的ip集合
//    private List<String> ipList;

//    private String setHostIp(String url) {
//        String replace = "";
//        try {
//            domainame = getTopDomainWithoutSubdomain(url);
//            if (TextUtils.isEmpty(domainame)) {
//                return "";
//            }
//            String s = encrypt(domainame);
//            String ip = "http://119.29.29.29/d?id=245&dn=" + s;
//            URL videourl = new URL(ip);
//            HttpURLConnection conn = (HttpURLConnection) videourl
//                    .openConnection();
//            conn.setRequestMethod("GET");
//            conn.setConnectTimeout(5000);
//            conn.setReadTimeout(10000);
//            int code = conn.getResponseCode();
//            if (code == 200) {
//                InputStream inputStream = conn.getInputStream();
//                String t = StringUtil.convertStreamToString(inputStream);
//                t = t.replaceAll("(\r\n|\r|\n|\n\r)", "");
//                t = decrypt(t);
//                if (!TextUtils.isEmpty(t)) {
//                    ips = t.split(";");
//                    if (!TextUtils.isEmpty(ips[0])) {
//                        replace = url.replace(domainame, ips[0]);
//                    }
//                }
//
//                ipList = new ArrayList<>();
//                for (int i = 0; i < ips.length; i++) {
//                    ipList.add(ips[i]);
//                }
//                if (application.getDomainNameList() != null && application.getDomainNameList().size() > 0) {
//                    ipList.addAll(application.getDomainNameList());
//                }
//            }
////			InetAddress inetAddress = InetAddress.getByName(domainame);
////			String hostAddress = inetAddress.getHostAddress();
////          replace = url.replace(domainame, hostAddress);
//        } catch (Exception e1) {
//            return replace;
//        }
//        return replace;
//    }

	public static String getTopDomainWithoutSubdomain(String url)
			throws MalformedURLException {

		String host = new URL(url).getHost().toLowerCase();// 此处获取值转换为小写
		Pattern pattern = Pattern
				.compile("[^//]*?\\.(com|cn|net|org|biz|info|cc|tv)");
		Matcher matcher = pattern.matcher(host);
		while (matcher.find()) {
			return matcher.group();
		}
		return null;
	}

	public void analysis() {
//        boolean isDomainame=true;
		try {
			if (db_main.getState(SharedPrefHelper.getInstance(context).getUserId(), task.getCourseId(), task.getCourseWareId()) == Constants.STATE_Pause) {
				return;
			}

//            if (times > 0) {
//                for (int i = 0; i < ipList.size() - 1; i++) {
//                    if (requestpath.contains(ipList.get(i))) {
//                        requestpath = requestpath.replace(ipList.get(i), ipList.get(i + 1));
//                        if (application.getDomainNameList() != null && application.getDomainNameList().size() > 0 && application.getDomainNameList().contains(ipList.get(i+1))) {
//                            isDomainame=false;
//                        }else{
//                            isDomainame=true;
//                        }
//                        break;
//                    }
//                }
//            }
			requestpath = task.getVideo_url();
			URL videourl = new URL(requestpath);
			HttpURLConnection conn = (HttpURLConnection) videourl
					.openConnection();
			conn.setRequestProperty("Referer", "android_phone_kq://bjsteach.com");
//            if(isDomainame){
//                if (!TextUtils.isEmpty(domainame)) {
//                    conn.setRequestProperty("Host", domainame);
//                }
//            }
			conn.setRequestMethod("GET");
			conn.setConnectTimeout(5000);
			conn.setReadTimeout(10000);
			int code = conn.getResponseCode();
			if (code == 200) {
				times = 0;
				InputStream inputStream = conn.getInputStream();
				String t = StringUtil.convertStreamToString(inputStream);

				String m3u8path = getPath(requestpath);
				File des = new File(m3u8path);
				if (!des.exists()) {
					des.mkdirs();
				}
				File videoFile = new File(m3u8path + "video.m3u8");
				wf = new FileWriter(videoFile);
				wf.write(t);
				String[] as = t.split("\n");
				// 暂时修改as.length 10
				for (int i = 0; i < as.length; i++) {
					if (!as[i].startsWith("#EXT")) {
						int index = requestpath.lastIndexOf("/") + 1;
						final String fileName = as[i];
						if (fileName.contains(".ts")) {
							String rootUrl = requestpath.substring(0, index);
							String url = rootUrl + fileName;
							list.add(url);
						}
					} else {
						if (!isCrypt) {
							if (as[i].contains("EXT-X-KEY")) {
								isCrypt = true;
								db_main.updateIsOld(SharedPrefHelper.getInstance(context).getUserId(), task.getCourseId(), task.getCourseWareId(), 0);
							}
						}
					}
				}
				prePlayVideoCenter.getKeyTxt("", m3u8path);
			} else if (code == 404) { // 提示下载失败，并更新数据库
				db_main.updateState(SharedPrefHelper.getInstance(context).getUserId(), task.getCourseId(), task.getCourseWareId(), Constants.STATE_Error);
				downloadflag = false;
				return;
			} else {
				operaDB.add(task.getCw(), errorCode + "", requestpath, System.currentTimeMillis() + "");
				if (times < 3) {
					times++;
					analysis();
				} else {
					times = 0;
					db_main.updateState(SharedPrefHelper.getInstance(context).getUserId(), task.getCourseId(), task.getCourseWareId(), Constants.STATE_Error);
					downloadflag = false;
					return;
				}
			}

			if (conn != null) {
				conn.disconnect();
			}
		} catch (Exception e) {
			db_main.updateState(SharedPrefHelper.getInstance(context).getUserId(), task.getCourseId(), task.getCourseWareId(), Constants.STATE_Error);
			downloadflag = false;
			return;
		} finally {
			if (wf != null) {
				try {
					wf.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
		}
	}


	/**
	 * 解析字幕文件加入下载列表
	 */
	private void resolveCaption() {
		String zmpath = task.getCaptionUrl();//字幕url
		HttpGet get = new HttpGet(zmpath);
		//get.setHeader("refer","android_phone_kq://bjsteach.com");
		FileWriter wf = null;
		try {
//			if (db_main.getState(task.getCourseWareId()) == Constants.STATE_Pause) {
//				return;
//			}

			URL url = new URL(zmpath);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setConnectTimeout(5000);
			int code = conn.getResponseCode();
			if (code == 200) {
				times = 0;
				InputStream inputStream = conn.getInputStream();
				String htmlContent = StringUtil.convertStreamToString(inputStream);

				String file = zmpath.substring(zmpath.lastIndexOf("/") + 1);
				// 得到下载路径 视频和讲义分别放入不同目录
				File path = new File(getPath(zmpath));
				// 创建下载目录
				if (!path.exists()) {
					path.mkdirs();
				}

				// 写入文件
				File videoFile = new File(path.getPath() + "/" + file);
				wf = new FileWriter(videoFile);
				wf.write(htmlContent);
			} else if (code == 404) {
//				db_main.updateErrorInfo(requestpath, "captionError","字幕下载失败404");
				System.out.println("caption下载失败");
			} else if (code == 503) {
				if (times < 3) {
					times++;
					resolveCaption();
				} else {
					times = 0;
//					db_main.updateErrorInfo(requestpath, "captionError","字幕下载失败503");
				}
			}
			if (conn != null) {
				conn.disconnect();
			}
		} catch (Exception e) {
//			db_main.updateErrorInfo(requestpath, "captionError","字幕下载异常");
		} finally {
			if (wf != null) {
				try {
					wf.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}

	}

	/**
	 * 检查存储空间
	 */
	private void checkStorage() {
		if (list == null || list.isEmpty()) {
			db_main.updateState(SharedPrefHelper.getInstance(context).getUserId(), task.getCourseId(), task.getCourseWareId(), Constants.STATE_Error);
			// M3U8文件为空
			downloadflag = false;
			return;
		}
		if (db_main.getState(SharedPrefHelper.getInstance(context).getUserId(), task.getCourseId(), task.getCourseWareId()) == Constants.STATE_Pause) {
			return;
		}
		long length = 0;
		String tsUrl = list.get(10);

		try {
			URL url = new URL(tsUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setConnectTimeout(5000);
			conn.setReadTimeout(10000);
			int code = conn.getResponseCode();
			if (code == 200) {
				length = conn.getContentLength();
				length = (length * list.size() / (1024 * 1024));

				long avilableMB = (FileUtil.getSizeByPath(FileUtil.getDownloadPath(context)) / (1024 * 1024));
				if (length > avilableMB) {
					db_main.updateState(SharedPrefHelper.getInstance(context).getUserId() + "", task.getCourseId(), task.getCourseWareId(), Constants.STATE_Pause);
					downloadflag = false;
					return;
				}
			}
			if (conn != null) {
				conn.disconnect();
			}
		} catch (Exception e) {
			return;
		}

	}
	
	public String INTRO_TITTLE="introdecewcy";

	/**
	 * 解析课程介绍
	 */
	private void resolveIntroduce() {
		String cwUrl = task.getCwDesc();
		FileWriter wf = null;
		try {
			if (db_main.getState(SharedPrefHelper.getInstance(context).getUserId(), task.getCourseId(), task.getCourseWareId()) == Constants.STATE_Pause) {
				return;
			}
			if (!db_main.find(SharedPrefHelper.getInstance(context).getUserId(), task.getCourseId(), task.getCourseWareId())) {
				return;
			}
			URL url = new URL(cwUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setConnectTimeout(5000);
			conn.setReadTimeout(10000);
			int code = conn.getResponseCode();
			if (code == 200) {
				times = 0;
				InputStream inputStream = conn.getInputStream();
				String htmlContent = StringUtil.convertStreamToString(inputStream);

				int index = cwUrl.lastIndexOf("/");
				String rootUrl = cwUrl.substring(0, index + 1);
				List<String> listjy = new ArrayList<String>();
				htmlContent = parseHtmlDownloadFile(htmlContent, rootUrl, listjy);
//				for (int i = 0; i < listjy.size(); i++) {
//					list.add(INTRO_TITTLE+listjy.get(i));
//				}
				list.addAll(listjy);
				// 得到URL
				String lectureUrl = cwUrl;
				// 得到文件
//				String file = lectureUrl.substring(lectureUrl.lastIndexOf("/") + 1);
				String file = "intro.htm";
				// 得到下载路径 视频和讲义分别放入不同目录
				File path = new File(this.getPathIntro(lectureUrl));
				// 创建下载目录
				if (!path.exists()) {
					path.mkdirs();
				}
				// 写入文件
				File videoFile = new File(path.getPath() + "/" + file);
				wf = new FileWriter(videoFile);
				wf.write(htmlContent);
			} else if (code == 404) {
//				db_main.updateErrorInfo(requestpath,"errorInfo","讲义添加失败404");
				System.out.println("下载失败");
			} else if (code == 503) {
				if (times < 3) {
					times++;
					resolveIntroduce();
				} else {
					times = 0;
//					db_main.updateErrorInfo(requestpath, "errorInfo","讲义添加失败503");
				}
			}
			//暂时注释掉 2.5.3重新打开
			if (conn != null) {
				conn.disconnect();
			}

		} catch (Exception e) {
//			db_main.updateErrorInfo(requestpath,"errorInfo","讲义异常");
		} finally {
			if (wf != null) {
				try {
					wf.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
	}

	/**
	 * 解析讲义文件加入下载列表
	 */
	private void resolveLecture() {
		String cwUrl = task.getLectureUrl();
		FileWriter wf = null;
		try {
			if (db_main.getState(SharedPrefHelper.getInstance(context).getUserId(), task.getCourseId(), task.getCourseWareId()) == Constants.STATE_Pause) {
				return;
			}
			if (!db_main.find(SharedPrefHelper.getInstance(context).getUserId(), task.getCourseId(), task.getCourseWareId())) {
				return;
			}
			URL url = new URL(cwUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setConnectTimeout(5000);
			conn.setReadTimeout(10000);
			int code = conn.getResponseCode();
			if (code == 200) {
				times = 0;
				InputStream inputStream = conn.getInputStream();
				String htmlContent = StringUtil.convertStreamToString(inputStream);

				int index = cwUrl.lastIndexOf("/");
				String rootUrl = cwUrl.substring(0, index + 1);
				List<String> listjy = new ArrayList<String>();
				htmlContent = parseHtmlDownloadFile(htmlContent, rootUrl, listjy);

				list.addAll(listjy);
				// 得到URL
				String lectureUrl = cwUrl;
				// 得到文件
//				String file = lectureUrl.substring(lectureUrl.lastIndexOf("/") + 1);
				String file = "lecture.htm";
				// 得到下载路径 视频和讲义分别放入不同目录
				File path = new File(this.getPath(lectureUrl));
				// 创建下载目录
				if (!path.exists()) {
					path.mkdirs();
				}

				// 写入文件
				File videoFile = new File(path.getPath() + "/" + file);
				wf = new FileWriter(videoFile);
				wf.write(htmlContent);
			} else if (code == 404) {
//				db_main.updateErrorInfo(requestpath,"errorInfo","讲义添加失败404");
				System.out.println("下载失败");
			} else if (code == 503) {
				if (times < 3) {
					times++;
					resolveLecture();
				} else {
					times = 0;
//					db_main.updateErrorInfo(requestpath, "errorInfo","讲义添加失败503");
				}
			}
			//暂时注释掉 2.5.3重新打开
			if (conn != null) {
				conn.disconnect();
			}

		} catch (Exception e) {
//			db_main.updateErrorInfo(requestpath,"errorInfo","讲义异常");
		} finally {
			if (wf != null) {
				try {
					wf.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
	}


	/**
	 * 解析HTML文件 返回需要下载的文件
	 *
	 * @param htmlContent
	 * @return
	 */
	private String parseHtmlDownloadFile(String htmlContent, String rootUrl, List<String> list) {

		Document doc = Jsoup.parse(htmlContent);
		Elements csss = doc.select("link");
		Elements jss = doc.select("script");
		Elements imgs = doc.select("img");
		for (Element element : csss) {
			String result = element.attr("href");
			String path = toAbsolutePath(rootUrl, result);
			if (!list.contains(path))
				list.add(toAbsolutePath(rootUrl, result));
			// 替换文件为相对路径
			element.attr("href", result.substring(result.lastIndexOf("/") + 1));
		}
		for (Element element : jss) {
			String result = element.attr("src");
			String path = toAbsolutePath(rootUrl, result);
			if (!list.contains(path))
				list.add(path);
			// 替换文件为相对路径
			element.attr("src", result.substring(result.lastIndexOf("/") + 1));
		}
		for (Element element : imgs) {
			String result = element.attr("src");
			String path = toAbsolutePath(rootUrl, result);
			if (!list.contains(path))
				list.add(path);
			// 替换文件为相对路径
			element.attr("src", result.substring(result.lastIndexOf("/") + 1));
		}
		// 得到新的html文本
		String newHtmlContent = doc.html();
		return newHtmlContent;
	}

	/**
	 * 得到存储路径
	 *
	 * @param url
	 * @return
	 */
	private String getPathIntro(String url) {
		if (url == null || url.equals(""))
			return "";

		return task.getDesPath() + task.getUserId() + "_" + task.getExamId() + "_" + task.getSubjectId() + "_" + task.getCourseId() + "_" + task.getSectionId() + "_" + task.getCourseWareId() + "/" + FileUtil.INTRO;
	}

	/**
	 * 得到存储路径
	 *
	 * @param url
	 * @return
	 */
	private String getPath(String url) {
		if (url == null || url.equals(""))
			return "";

		int index = url.lastIndexOf(".");
		String prefix = url.substring(index + 1);
		if (prefix.equals("m3u8") || prefix.equals("ts")) {
			return task.getDesPath() + task.getUserId() + "_" + task.getExamId() + "_" + task.getSubjectId() + "_" + task.getCourseId() + "_" + task.getSectionId() + "_" + task.getCourseWareId() + "/" + FileUtil.VIDEOPATH;
		}
//        else if(prefix.equals("intro")){
//            return task.getDesPath() + task.getUserId() + "_" + task.getExamId() + "_" + task.getSubjectId() + "_" + task.getCourseId() + "_" + task.getSectionId() + "_" + task.getCourseWareId() + "/" + FileUtil.INTRO;
//        }
		else {
			return task.getDesPath() + task.getUserId() + "_" + task.getExamId() + "_" + task.getSubjectId() + "_" + task.getCourseId() + "_" + task.getSectionId() + "_" + task.getCourseWareId() + "/" + FileUtil.LECTURE;
		}

	}

	/**
	 * 如果url为相对路径 转换为绝对路径
	 *
	 * @param rootUrl
	 * @param url
	 * @return
	 */
	private String toAbsolutePath(String rootUrl, String url) {
		String result = "";
		if (url.startsWith("http://") || url.startsWith("https://")) {
			result = url;
		} else {
			result = rootUrl + url;
		}
		return result;
	}

	//	public void disk1() {
//		// download.setEnabled(false);
//		finish.clear();
//		downloadflag = true;
//		for (int i = 0; i < list.size(); i++) {
//			// new DownLoadTask(list.get(i), desPath + namelist.get(i));
//			download(list.get(i), desPath + namelist.get(i) + ".temp");
//		}
//	}
//	private int exceptionTimes;
	private boolean exception;
	private boolean isEXsit;

	public void downloadList() {
//		exceptionTimes=0;
		if (list.size() <= 0) {//|| namelist.size()<=0
			db_main.updateState(SharedPrefHelper.getInstance(context).getUserId(), task.getCourseId(), task.getCourseWareId(), Constants.STATE_Error);
			return;
		}
		if (db_main.getState(SharedPrefHelper.getInstance(context).getUserId(), task.getCourseId(), task.getCourseWareId()) == Constants.STATE_Pause) {
			return;
		}
		downloadflag = true;
		for (int i = 0; i < list.size(); i++) {
			if (!downloadflag) {
				return;
			}
			isError = false;
			exception = false;
			isEXsit = false;
			errorCode = 0;
			String path = list.get(i);
			String name = path.substring(path.lastIndexOf("/") + 1, path.length());
			String desth = getPath(path) + name + ".temp";
			String alreadyDesth = getPath(path) + name;
			File file = null;
			File temp = null;
			int startpoint = 0;

			try {
				// 设置当前现在下载的开始位置.
				file = new File(desth.substring(0, desth.length() - 5));
				temp = new File(desth);
				if (file.exists() && file.length() > 0) {
					isEXsit = true;
					continue;
				}

				if (temp.exists() && temp.length() > 0) {
					long length = temp.length();
					startpoint = (int) length;
				}
				URL url = new URL(path);
				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();
				conn.setRequestProperty("Referer", "android_phone_kq://bjsteach.com");
//                if (!TextUtils.isEmpty(domainame) && path.contains(".ts")) {
//                    conn.setRequestProperty("Host", domainame);
//                }
				conn.setRequestMethod("GET");
				conn.setConnectTimeout(5000);
				conn.setReadTimeout(10000);
				conn.setRequestProperty("Range", "bytes=" + startpoint + "-");
				int code = conn.getResponseCode();// 下载指定位置的文件 206
				if (code == 206 || code == 200) {
					times = 0;
					InputStream is = conn.getInputStream();
					RandomAccessFile raf = new RandomAccessFile(desth, "rw");
					// 指定保存数据的开始位置
					raf.seek(startpoint);
					byte[] buffer = new byte[1024 * 1024];
					int len = 0;
					while ((len = is.read(buffer)) != -1 && downloadflag) {
						raf.write(buffer, 0, len);
					}
					is.close();
					raf.close();
				} else if (code == 404) {// 提示下载失败  更新数据库中哪个ts下载失败
					isError = true;
					errorCode = 404;
					System.out.println("donwload_code:" + path);
				} else {
					errorCode = code;
					reConnect(path, desth);
				}
				if (conn != null) {
					conn.disconnect();
				}
			} catch (Exception e) {    //删除异常文件 移除异常path 然后添加进来再次下载
				if (temp.exists()) {
					System.out.println("temp:::::::delete" + temp.delete());
				}
				exception = true;
				isError = true;
//				if(exceptionTimes<3){
//					i=i-1;
//					exceptionTimes++;
//				}
				System.out.println("donwload_except:" + e.toString() + "*****" + path + "::" + startpoint);
			} finally {
				synchronized (DownloadExcutor.class) {
					if (isError && !exception) {
						operaDB.add(task.getCw(), errorCode + "", path, System.currentTimeMillis() + "");
					}

					if (!downloadflag
							|| !NetworkUtil.isNetworkAvailable(context)) {//如果暂停或没有网络停止下载
						return;
					}

					if (isError && i != list.size() - 1) {//如果下载出错   继续下一个ts下载
						continue;
					}

					if (!isError) {
						finish.add(path);
					}
					if (isEXsit && i != list.size() - 1) {
						continue;
					}
					// 更新该视频片段已下载完毕
					System.out.println("percent::::::::::"
							+ ((double) finish.size()
							/ (double) list.size() * 100.0));
					if (!file.exists() && !isError) {
						if (temp.exists() && !isError) {
							boolean flag = temp.renameTo(file);
							if (flag) {
								if (!isCrypt && path.contains(".ts")) {
//									try{
//										byte[] data = getTsBytes(alreadyDesth);
//										data = FileJNILib.videoCrypt(data);
//										getFile(data, getPath(path), name);
//									}catch (Exception e){
//										String sr=e.getMessage();
//									}
									
								}
							} else {
								finish.remove(path);
							}
						}
						double perc = ((double) finish.size() / (double) list.size() * 100.0);
						int localPerc = db_main.getPercent(SharedPrefHelper.getInstance(context).getUserId(), task.getCourseId(), task.getCourseWareId());
						if (perc < localPerc && localPerc <= 100 && i != list.size() - 1) {
							continue;
						}
						if (perc > 100) {
							db_main.updateState(SharedPrefHelper.getInstance(context).getUserId(), task.getCourseId(), task.getCourseWareId(),
									Constants.STATE_Error);
							downloadflag = false;
						} else if (perc == 100 && i != list.size() - 1) {
							db_main.updateState(SharedPrefHelper.getInstance(context).getUserId(), task.getCourseId(), task.getCourseWareId(),
									Constants.STATE_Error);
							downloadflag = false;
						} else {
							if (downloadflag) {
								int updateItem = db_main.updatePercent(SharedPrefHelper.getInstance(context).getUserId(), task.getCourseId(), task.getCourseWareId(), (int) perc);
								if (updateItem == 0) {
									downloadflag = false;
								}
							}
						}

						Handler handler = application.getHandler();
						if (handler != null) {
							handler.sendEmptyMessage(0);
						}
						if (AppContext.getInstance().getHandlerDetail() != null) {
							AppContext.getInstance().getHandlerDetail()
									.sendEmptyMessage(0);
						}
					}

					if (i == list.size() - 1) {
						// 更新该视频已下载完毕
						Message msg = Message.obtain();
						System.out.println("over");
						if (finish.size() != list.size()) {
							msg.obj = 99;
//							db_main.updateState(SharedPrefHelper.getInstance(context).getUserId(),task.getCourseId(),task.getCourseWareId(),
//									Constants.STATE_Error);
							traverseDownload();
						} else if (finish.size() == list.size() && list.size() > 0) {
							msg.obj = 100;
							db_main.updateState(SharedPrefHelper.getInstance(context).getUserId(), task.getCourseId(), task.getCourseWareId(),
									Constants.STATE_DownLoaded);
						}

						int errorPerc = db_main.getPercent(SharedPrefHelper.getInstance(context).getUserId(), task.getCourseId(), task.getCourseWareId());
						int state = db_main.getState(SharedPrefHelper.getInstance(context).getUserId(), task.getCourseId(), task.getCourseWareId());
						if (errorPerc == 100 && state == Constants.STATE_Error) {
							db_main.updatePercentState(SharedPrefHelper.getInstance(context).getUserId(), task.getCourseId(), task.getCourseWareId(), 99);
						}
						Handler handler = application.getHandler();
						if (handler != null) {
							handler.sendMessage(msg);
						}
						if (AppContext.getInstance().getHandlerDetail() != null) {
							AppContext.getInstance().getHandlerDetail()
									.sendEmptyMessage(0);
						}
					}
				}

			}

		}
	}

	/**
	 * 获得指定文件的byte数组
	 */
	private byte[] getTsBytes(String filePath) {
		byte[] buffer = null;
		try {
			File file = new File(filePath);
			FileInputStream fis = new FileInputStream(file);
			ByteArrayOutputStream bos = new ByteArrayOutputStream(1000);
			byte[] b = new byte[1000];
			int n;
			while ((n = fis.read(b)) != -1) {
				bos.write(b, 0, n);
			}
			fis.close();
			bos.close();
			buffer = bos.toByteArray();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return buffer;
	}

	/**
	 * 根据byte数组，生成文件
	 */
	public static void getFile(byte[] bfile, String filePath, String fileName) {
		BufferedOutputStream bos = null;
		FileOutputStream fos = null;
		File file = null;
		try {
			File dir = new File(filePath);
			if (!dir.exists() && dir.isDirectory()) {//判断文件目录是否存在
				dir.mkdirs();
			}
			file = new File(filePath + fileName);
			fos = new FileOutputStream(file);
			bos = new BufferedOutputStream(fos);
			bos.write(bfile);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (bos != null) {
				try {
					bos.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	private void traverseDownload() {
		finish.clear();
		for (int i = 0; i < list.size(); i++) {
			isError = false;
			exception = false;
			isEXsit = false;
			errorCode = 0;
			String path = list.get(i);
			String name = path.substring(path.lastIndexOf("/") + 1, path.length());
			String desth = getPath(path) + name + ".temp";
			String alreadyDesth = getPath(path) + name;
			File file = null;
			File temp = null;
			int startpoint = 0;

			try {
				// 设置当前现在下载的开始位置.
				file = new File(desth.substring(0, desth.length() - 5));
				temp = new File(desth);
				if (file.exists() && file.length() > 0) {
					isEXsit = true;
					continue;
				}

				if (temp.exists() && temp.length() > 0) {
					long length = temp.length();
					startpoint = (int) length;
				}
				URL url = new URL(path);
				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();
				conn.setRequestProperty("Referer", "android_phone_kq://bjsteach.com");
//                if (!TextUtils.isEmpty(domainame) && path.contains(".ts")) {
//                    conn.setRequestProperty("Host", domainame);
//                }
				conn.setRequestMethod("GET");
				conn.setConnectTimeout(5000);
				conn.setReadTimeout(10000);
				conn.setRequestProperty("Range", "bytes=" + startpoint + "-");
				int code = conn.getResponseCode();// 下载指定位置的文件 206
				if (code == 206 || code == 200) {
					times = 0;
					InputStream is = conn.getInputStream();
					RandomAccessFile raf = new RandomAccessFile(desth, "rw");
					// 指定保存数据的开始位置
					raf.seek(startpoint);
					byte[] buffer = new byte[1024 * 1024];
					int len = 0;
					while ((len = is.read(buffer)) != -1 && downloadflag) {
						raf.write(buffer, 0, len);
					}
					is.close();
					raf.close();
				} else if (code == 404) {// 提示下载失败  更新数据库中哪个ts下载失败
					isError = true;
					errorCode = 404;
					System.out.println("donwload_code:" + path);
				} else {
					errorCode = code;
				}
				if (conn != null) {
					conn.disconnect();
				}
			} catch (Exception e) {    //删除异常文件 移除异常path 然后添加进来再次下载
				if (temp.exists()) {
					System.out.println("temp:::::::delete" + temp.delete());
				}
				exception = true;
				isError = true;
				System.out.println("donwload_except:" + e.toString() + "*****" + path + "::" + startpoint);
			} finally {
				synchronized (DownloadExcutor.class) {
					if (isError && !exception) {
						operaDB.add(task.getCw(), errorCode + "", path, System.currentTimeMillis() + "");
					}

					if (!downloadflag
							|| !NetworkUtil.isNetworkAvailable(context)) {//如果暂停或没有网络停止下载
						return;
					}

					if (isError && i != list.size() - 1) {//如果下载出错   继续下一个ts下载
						continue;
					}

					if (!isError) {
						finish.add(path);
					}
					if (isEXsit && i != list.size() - 1) {
						continue;
					}
					// 更新该视频片段已下载完毕
					System.out.println("percent::::::::::"
							+ ((double) finish.size()
							/ (double) list.size() * 100.0));
					if (!file.exists() && !isError) {
						if (temp.exists() && !isError) {
							boolean flag = temp.renameTo(file);
							if (flag) {
//								if (!isCrypt && path.contains(".ts")) {
//									byte[] data = getTsBytes(alreadyDesth);
//									data = FileJNILib.videoCrypt(data);
//									getFile(data, getPath(path), name);
//								}
							} else {
								finish.remove(path);
							}
						}
						double perc = ((double) finish.size() / (double) list.size() * 100.0);
						int localPerc = db_main.getPercent(SharedPrefHelper.getInstance(context).getUserId(), task.getCourseId(), task.getCourseWareId());
						if (perc < localPerc && localPerc <= 100 && i != list.size() - 1) {
							continue;
						}
						if (perc > 100) {
							db_main.updateState(SharedPrefHelper.getInstance(context).getUserId(), task.getCourseId(), task.getCourseWareId(),
									Constants.STATE_Error);
							downloadflag = false;
						} else if (perc == 100 && i != list.size() - 1) {
							db_main.updateState(SharedPrefHelper.getInstance(context).getUserId(), task.getCourseId(), task.getCourseWareId(),
									Constants.STATE_Error);
							downloadflag = false;
						} else {
							int updateItem = db_main.updatePercent(SharedPrefHelper.getInstance(context).getUserId(), task.getCourseId(), task.getCourseWareId(), (int) perc);
							if (updateItem == 0) {
								downloadflag = false;
							}
						}

						Handler handler = application.getHandler();
						if (handler != null) {
							handler.sendEmptyMessage(0);
						}
						if (AppContext.getInstance().getHandlerDetail() != null) {
							AppContext.getInstance().getHandlerDetail()
									.sendEmptyMessage(0);
						}
					}

					if (i == list.size() - 1) {
						// 更新该视频已下载完毕
						Message msg = Message.obtain();
						System.out.println("over");
						if (finish.size() != list.size()) {
							msg.obj = 99;
							int localPerc = db_main.getPercent(SharedPrefHelper.getInstance(context).getUserId(), task.getCourseId(), task.getCourseWareId());
							if (localPerc >= 100) {
								db_main.updatePercentState(SharedPrefHelper.getInstance(context).getUserId(), task.getCourseId(), task.getCourseWareId(), 99);
							} else {
								db_main.updateState(SharedPrefHelper.getInstance(context).getUserId(), task.getCourseId(), task.getCourseWareId(),
										Constants.STATE_Error);
							}
						} else {
							msg.obj = 100;
							db_main.updateState(SharedPrefHelper.getInstance(context).getUserId(), task.getCourseId(), task.getCourseWareId(),
									Constants.STATE_DownLoaded);
						}

						int errorPerc = db_main.getPercent(SharedPrefHelper.getInstance(context).getUserId(), task.getCourseId(), task.getCourseWareId());
						int state = db_main.getState(SharedPrefHelper.getInstance(context).getUserId(), task.getCourseId(), task.getCourseWareId());
						if (errorPerc == 100 && state == Constants.STATE_Error) {
							db_main.updatePercentState(SharedPrefHelper.getInstance(context).getUserId(), task.getCourseId(), task.getCourseWareId(), 99);
						}
						Handler handler = application.getHandler();
						if (handler != null) {
							handler.sendMessage(msg);
						}
						if (AppContext.getInstance().getHandlerDetail() != null) {
							AppContext.getInstance().getHandlerDetail()
									.sendEmptyMessage(0);
						}
					}
				}

			}

		}
	}

	private void reConnect(String path, String desth) {
//        boolean isDomainame=true;
//        for (int i = 0; i < ipList.size() - 1; i++) {
//            if (path.contains(ipList.get(i))) {
//                path = path.replace(ipList.get(i), ipList.get(i + 1));
//                if (application.getDomainNameList() != null && application.getDomainNameList().size() > 0 && application.getDomainNameList().contains(ipList.get(i+1))) {
//                    isDomainame=false;
//                }else{
//                    isDomainame=true;
//                }
//                break;
//            }
//        }
		isError = false;
		File file = null;
		File temp = null;
		int startpoint = 0;

		try {
			// 设置当前现在下载的开始位置.
			file = new File(desth.substring(0, desth.length() - 5));
			temp = new File(desth);
			if (file.exists() && file.length() > 0) {
				return;
			}

			if (temp.exists() && temp.length() > 0) {
				long length = temp.length();
				startpoint = (int) length;
			}

			URL url = new URL(path);
			HttpURLConnection conn = (HttpURLConnection) url
					.openConnection();
			conn.setRequestMethod("GET");
			conn.setConnectTimeout(5000);
			conn.setReadTimeout(10000);
			conn.setRequestProperty("Range", "bytes=" + startpoint + "-");
//            if(isDomainame){
//                if (!TextUtils.isEmpty(domainame) && path.contains(".ts")) {
//                    conn.setRequestProperty("Host", domainame);
//                }
//            }
			int code = conn.getResponseCode();// 下载指定位置的文件 206
			if (code == 206 || code == 200) {
				times = 0;
				InputStream is = conn.getInputStream();
				RandomAccessFile raf = new RandomAccessFile(desth, "rw");
				// 指定保存数据的开始位置
				raf.seek(startpoint);
				byte[] buffer = new byte[1024 * 1024];
				int len = 0;
				// 当前线程下载的大小
				// int total = 0;
				while ((len = is.read(buffer)) != -1 && downloadflag) {
					raf.write(buffer, 0, len);
					// total += len;
				}
				is.close();
				raf.close();
			} else if (code == 404) {// 提示下载失败  更新数据库中哪个ts下载失败
				isError = true;
				System.out.println("donwload_code503:" + code);
			} else {
				if (times < 3) {
					times++;
					reConnect(path, desth);
				} else {
					times = 0;
					isError = true;
					System.out.println("donwload_code503:" + code);
				}
			}
			if (conn != null) {
				conn.disconnect();
			}
		} catch (Exception e) {
			System.out.println("donwload_exception503:" + e.toString());
			isError = true;
		}


	}

	public void setFlag(boolean b) {
		this.downloadflag = b;
	}

	/**
	 * 域名加密
	 *
	 * @param domain
	 * @return
	 */
	public String encrypt(String domain) {
		String crypt = "";
		try {
			//初始化密钥
			SecretKeySpec keySpec = new SecretKeySpec("i_PqWF_o".getBytes("utf-8"), "DES");
			//选择使用 DES 算法，ECB 方式，填充方式为 PKCS5Padding
			Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
			//初始化
			cipher.init(Cipher.ENCRYPT_MODE, keySpec);
			//获取加密后的字符串
			byte[] encryptedString = cipher.doFinal(domain.getBytes("utf-8"));
			crypt = bytesToHexString(encryptedString);
		} catch (Exception e) {
			return crypt;
		}
		return crypt;
	}

	/**
	 * 域名解密
	 *
	 * @param data
	 * @return
	 */
	public String decrypt(String data) {
		String decrypt = "";
		try {
			SecretKeySpec keySpec = new SecretKeySpec("i_PqWF_o".getBytes("utf-8"), "DES");
			Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, keySpec);
			byte[] decrypts = cipher.doFinal(hexStringToByte(data));
			decrypt = new String(decrypts, "utf-8");
		} catch (Exception e) {
			return decrypt;
		}
		return decrypt;
	}

	/**
	 * 2进制byte[]转成16进制字符串
	 *
	 * @param src
	 * @return
	 */
	public static String bytesToHexString(byte[] src) {
		StringBuilder stringBuilder = new StringBuilder();
		if (src == null || src.length <= 0) {
			return null;
		}
		for (int i = 0; i < src.length; i++) {
			int v = src[i] & 0xFF;
			String hv = Integer.toHexString(v);
			if (hv.length() < 2) {
				stringBuilder.append(0);
			}
			stringBuilder.append(hv);
		}
		return stringBuilder.toString();
	}

	/**
	 * 16进制字符串转成byte[]
	 *
	 * @param hex
	 * @return
	 */
	public static byte[] hexStringToByte(String hex) {
		int len = (hex.length() / 2);
		byte[] result = new byte[len];
		char[] achar = hex.toCharArray();
		for (int i = 0; i < len; i++) {
			int pos = i * 2;
			result[i] = (byte) (toByte(achar[pos]) << 4 | toByte(achar[pos + 1]));
		}
		return result;
	}

	private static byte toByte(char c) {
		byte b = (byte) "0123456789abcdef".indexOf(c);
		return b;
	}
}
