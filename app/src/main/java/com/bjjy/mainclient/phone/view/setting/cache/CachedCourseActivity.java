package com.bjjy.mainclient.phone.view.setting.cache;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dongao.mainclient.model.bean.play.CourseWare;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseActivity;
import com.bjjy.mainclient.phone.view.play.PlayActivity;
import com.bjjy.mainclient.phone.view.setting.cache.adapter.CachedCourseAdapter;
import com.bjjy.mainclient.phone.download.db.DownloadDB;
import com.bjjy.mainclient.phone.widget.DialogManager;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by dell on 2016/5/17.
 */
public class CachedCourseActivity extends BaseActivity implements AdapterView.OnItemClickListener{

    @Bind(R.id.cachedcourse_ls)
    ListView cachedcourseLs;
    @Bind(R.id.top_title_left)
    ImageView topTitleLeft;
    @Bind(R.id.top_title_right)
    ImageView topTitleRight;
    @Bind(R.id.top_title_text)
    TextView topTitleText;
    @Bind(R.id.top_title_bar_layout)
    RelativeLayout topTitleBarLayout;

    private CachedCourseAdapter adapter;
    private DownloadDB db;
    private List<CourseWare> list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cachedcourse);
        ButterKnife.bind(this);
        initView();
        initData();
    }

    @Override
    public void initView() {
        String title=getIntent().getStringExtra("title");
        topTitleLeft.setImageResource(R.drawable.nav_left);
        topTitleRight.setImageResource(R.drawable.local_delete_light);
        topTitleRight.setVisibility(View.VISIBLE);
        topTitleLeft.setVisibility(View.VISIBLE);
        topTitleLeft.setOnClickListener(this);
        topTitleRight.setOnClickListener(this);
        topTitleText.setText(title);

        adapter = new CachedCourseAdapter(CachedCourseActivity.this);
        cachedcourseLs.setOnItemClickListener(this);

        db=new DownloadDB(this);
    }

    @Override
    public void initData() {
        String courseId=getIntent().getStringExtra("courseId");
        list= db.findCourseWares(courseId, SharedPrefHelper.getInstance(this).getUserId());
        adapter.setList(list);
        cachedcourseLs.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.top_title_left:
                finish();
                break;
            case R.id.top_title_right:
                DialogManager.showNormalDialog(this, "确定要全部删除吗", "提示", "取消", "确定",
                        new DialogManager.CustomDialogCloseListener() {
                            @Override
                            public void yesClick() {
                                db.deleteCourseWares(SharedPrefHelper.getInstance(CachedCourseActivity.this).getUserId(), list);
                                finish();
                            }

                            @Override
                            public void noClick() {
                            }
                        });
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent =new Intent(CachedCourseActivity.this, PlayActivity.class);
        intent.putExtra("classId",list.get(position).getClassId());
        intent.putExtra("playCwId",list.get(position).getCwId());
        intent.putExtra("courseBean", list.get(position).getCourseBean());
        intent.putExtra("subjectId", list.get(position).getSubjectId());
        intent.putExtra("examId", list.get(position).getExamId());
        intent.putExtra("sectionId", list.get(position).getSectionId());
        intent.putExtra("version", list.get(position).getCwVersion());
        intent.putExtra("isFromLocal", "yes");
        startActivity(intent);
    }
}
