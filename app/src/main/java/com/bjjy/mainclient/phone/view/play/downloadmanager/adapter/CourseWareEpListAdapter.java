package com.bjjy.mainclient.phone.view.play.downloadmanager.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.dongao.mainclient.model.bean.play.CourseChapter;
import com.dongao.mainclient.model.bean.play.CourseWare;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.view.play.downloadmanager.AbsCoursListFragment;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.dongao.mainclient.model.common.Constants.STATE_DownLoaded;
import static com.dongao.mainclient.model.common.Constants.STATE_DownLoading;
import static com.dongao.mainclient.model.common.Constants.STATE_Error;
import static com.dongao.mainclient.model.common.Constants.STATE_Pause;
import static com.dongao.mainclient.model.common.Constants.STATE_Waiting;

/**
 * Created by yunfei on 2016/11/30.
 */

public class CourseWareEpListAdapter extends BaseExpandableListAdapter {

    private final LayoutInflater inflater;
    private final AbsCoursListFragment.OnItemSelectNumChangedListener onItemSelectNumChangedListener;

    private List<CourseChapter> data = Collections.emptyList();
    private HashSet<CourseWare> courseWaresChecked;

    public CourseWareEpListAdapter(Context context, AbsCoursListFragment.OnItemSelectNumChangedListener onItemSelectNumChangedListener, HashSet<CourseWare> courseWaresChecked) {
        inflater = LayoutInflater.from(context);
        this.courseWaresChecked = courseWaresChecked;
        this.onItemSelectNumChangedListener = onItemSelectNumChangedListener;
    }

    public void setData(List<CourseChapter> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public int getGroupCount() {
        return data.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if (data.isEmpty() || data.get(groupPosition).getMobileCourseWareList() == null || data.get(groupPosition).getMobileCourseWareList().isEmpty())
            return 0;
        return data.get(groupPosition).getMobileCourseWareList().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return data.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return data.get(groupPosition).getMobileCourseWareList().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        GroupViewHolder groupViewHolder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.pay_download_course_ware_group_item, null);
            groupViewHolder = new GroupViewHolder(convertView);
            convertView.setTag(groupViewHolder);
        } else {
            groupViewHolder = (GroupViewHolder) convertView.getTag();
        }
        CourseChapter courseChapter = data.get(groupPosition);
        groupViewHolder.tv_title.setText(courseChapter.getSectionName());
        if (isExpanded)
            groupViewHolder.iv_indicator.setImageResource(R.drawable.play_download_manager_indicator_open);
        else
            groupViewHolder.iv_indicator.setImageResource(R.drawable.play_download_manager_indicator_close);

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final ChildViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.pay_download_course_ware_item, null);
            viewHolder = new ChildViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ChildViewHolder) convertView.getTag();
        }
        final CourseWare item = data.get(groupPosition).getMobileCourseWareList().get(childPosition);
        viewHolder.tv_title.setText(item.getChapterNo() + " " + item.getCwName());
        int state = item.getState();
        if (state == STATE_Pause
                || state == STATE_DownLoading
                || state == STATE_Error
                || state == STATE_Waiting) { //不可下载
            viewHolder.checkbox.setVisibility(View.INVISIBLE);
            viewHolder.iv_state.setVisibility(View.VISIBLE);
            viewHolder.iv_state.setImageResource(R.drawable.downloadmanager_loading);
            viewHolder.tv_title.setTextColor(viewHolder.tv_title.getResources().getColor(R.color.text_color_primary_hint));
        }else if(state == STATE_DownLoaded){
            viewHolder.iv_state.setVisibility(View.VISIBLE);
            viewHolder.iv_state.setImageResource(R.drawable.downloadmanager_bendi);
        } else { //可下载
            viewHolder.iv_state.setVisibility(View.GONE);
            viewHolder.checkbox.setVisibility(View.INVISIBLE);
            viewHolder.tv_title.setTextColor(viewHolder.tv_title.getResources().getColor(R.color.text_color_primary_dark));
        }

        if (courseWaresChecked.contains(item)) {
            viewHolder.checkbox.setChecked(true);
        } else {
            viewHolder.checkbox.setChecked(false);
        }
        viewHolder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    courseWaresChecked.add(item);
                } else {
                    courseWaresChecked.remove(item);
                }
                onItemSelectNumChangedListener.onItemSelectNumChanged();
            }
        });

//        convertView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                viewHolder.checkbox.setChecked(!viewHolder.checkbox.isChecked());
//            }
//        });

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    static class ChildViewHolder {
        @Bind(R.id.checkbox)
        CheckBox checkbox;
        @Bind(R.id.tv_title)
        TextView tv_title;
        @Bind(R.id.iv_state)
        ImageView iv_state;

        ChildViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    static class GroupViewHolder {
        @Bind(R.id.tv_title)
        TextView tv_title;
        @Bind(R.id.iv_indicator)
        ImageView iv_indicator;

        GroupViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
