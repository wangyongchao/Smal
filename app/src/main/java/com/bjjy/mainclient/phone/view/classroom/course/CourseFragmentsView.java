package com.bjjy.mainclient.phone.view.classroom.course;


import com.dongao.mainclient.model.mvp.MvpView;
import com.dongao.mainclient.model.bean.course.CourseAnswer;

/**
 * UI 定义UI 看此UI中有何事件
 */
public interface CourseFragmentsView extends MvpView {
  void setView(CourseAnswer courseJson,boolean isModifySp);
}
