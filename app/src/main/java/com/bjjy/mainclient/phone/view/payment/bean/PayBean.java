package com.bjjy.mainclient.phone.view.payment.bean;

/**
 * Created by dell on 2016/12/6.
 */
public class PayBean {
    private String result_code;
    private String result_msg;
    private String sign;
    private String result_val;

    public String getResult_val() {
        return result_val;
    }

    public void setResult_val(String result_val) {
        this.result_val = result_val;
    }

    public String getResult_code() {
        return result_code;
    }

    public void setResult_code(String result_code) {
        this.result_code = result_code;
    }

    public String getResult_msg() {
        return result_msg;
    }

    public void setResult_msg(String result_msg) {
        this.result_msg = result_msg;
    }


    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}
