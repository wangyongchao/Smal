package com.bjjy.mainclient.phone.view.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.AppConfig;
import com.bjjy.mainclient.phone.app.BaseFragmentActivity;
import com.bjjy.mainclient.phone.download.db.DownloadDB;
import com.bjjy.mainclient.phone.event.CancelLoginEvent;
import com.bjjy.mainclient.phone.event.LoginSuccessEvent;
import com.bjjy.mainclient.phone.event.MainTabChoosed;
import com.bjjy.mainclient.phone.event.NewMsgNotification;
import com.bjjy.mainclient.phone.event.PlayUploadRecord;
import com.bjjy.mainclient.phone.event.PositionedEvent;
import com.bjjy.mainclient.phone.event.PushMsgNotification;
import com.bjjy.mainclient.phone.event.UpdateEvent;
import com.bjjy.mainclient.phone.utils.NetWorkUtils;
import com.bjjy.mainclient.phone.utils.NetworkUtil;
import com.bjjy.mainclient.phone.view.course.CourseFragment;
import com.bjjy.mainclient.phone.view.exam.utils.CommenUtils;
import com.bjjy.mainclient.phone.view.home.HomeFragments;
import com.bjjy.mainclient.phone.view.persenal.PersenalFragment;
import com.dongao.mainclient.model.bean.play.CwStudyLog;
import com.bjjy.mainclient.phone.view.play.domain.CwStudyLogDB;
import com.bjjy.mainclient.phone.view.studybar.Constant;
import com.bjjy.mainclient.phone.view.user.LoginNewActivity;
import com.bjjy.mainclient.phone.view.zb.LiveFragment;
import com.bjjy.mainclient.phone.widget.CustomViewPager;
import com.bjjy.mainclient.phone.widget.DialogManager;
import com.dongao.libs.dongaoumengpushlib.constants.PushConstants;
import com.dongao.mainclient.core.app.AppManager;
import com.dongao.mainclient.core.util.SystemUtils;
import com.dongao.mainclient.model.bean.play.CourseWare;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.umeng.analytics.MobclickAgent;
import com.umeng.message.IUmengRegisterCallback;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.ButterKnife;

/**
 * @author wyc
 */
public class MainActivity extends BaseFragmentActivity implements MainView,AMapLocationListener {

    private FragmentTabHost mTabHost;
    private CustomViewPager mViewPager;
    private List<Fragment> mFragmentList;
    private RelativeLayout main_guide,linearLayout_classroom_guide;
    private ImageView  imageView_guide_4, imageView_guide_5;
    private int guideNo = 1;//当前引导显示了几个图
    //private Class mClass[] = {HomeFragments.class, CourseFragments.class, StudyBarFragment.class, PersenalFragment.class};
    //private Fragment mFragment[] = {};
    private String mTitles[] = {"首页", "课堂", "直播", "我的"};
    private int mImages[] = {
            R.drawable.tab_home,
            R.drawable.tab_classroom,
//            R.drawable.tab_learn,
            R.drawable.tab_learn,
            R.drawable.tab_my
    };

    public AMapLocationClient mLocationClient = null;

    public AMapLocationClientOption mLocationOption = null;

    private CwStudyLogDB cwStudyLogDB;

    private MainActivityPersenter persenter;


    private Handler handler = new Handler();
    private DownloadDB db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        EventBus.getDefault().register(this);
        ButterKnife.bind(this);
        persenter = new MainActivityPersenter();
        persenter.attachView(this);
        Activity activity= AppManager.getAppManager().currentActivity();
        String name=activity.getLocalClassName();
        initView();
//        initLocation();
        initData();

        checkDownloadStatus();
        setTranslucentStatus();
        upLoadPositon();
    }

    @Override
    public void initView() {
        //初始化友盟推送
        youmeng();
        cwStudyLogDB = new CwStudyLogDB(this);
        db=new DownloadDB(this);

        mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        mViewPager = (CustomViewPager) findViewById(R.id.view_pager);
        mViewPager.setPagingEnabled(false);
        mFragmentList = new ArrayList<Fragment>();
        mFragmentList.add(new HomeFragments());
//        mFragmentList.add(CourseFragments.newInstance(1));
        mFragmentList.add(new CourseFragment());
        mFragmentList.add(new LiveFragment());
//        mFragmentList.add(new CacheFragment());
        mFragmentList.add(new PersenalFragment());

        linearLayout_classroom_guide = (RelativeLayout) findViewById(R.id.main_classroom_guide);
        linearLayout_classroom_guide.setOnClickListener(this);
        main_guide = (RelativeLayout) findViewById(R.id.main_guide);
        if (SharedPrefHelper.getInstance(this).isFirstIn()) {
            main_guide.setVisibility(View.GONE);
        }
        //imageView_guide_2 = (ImageView) findViewById(R.id.home_guide2);
        //imageView_guide_3 = (ImageView) findViewById(R.id.home_guide3);
        imageView_guide_4 = (ImageView) findViewById(R.id.home_guide4);
        imageView_guide_5 = (ImageView) findViewById(R.id.home_guide5);
        main_guide.setOnClickListener(this);

        //StatusBarUtil.setTranslucentDiff(this);
        // StatusBarUtil.setTranslucent(this, StatusBarUtil.DEFAULT_STATUS_BAR_ALPHA);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int ScreenW = dm.widthPixels;
        SharedPrefHelper.getInstance(this).setFullScreenHight(ScreenW);
        String time = SystemUtils.secToTime(2654);
        String time1 = SystemUtils.secToTime(2654);

        initTabs();
    }

    private void initTabs(){
        mTabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);
        mTabHost.getTabWidget().setDividerDrawable(null);

        for (int i = 0; i < mFragmentList.size(); i++) {
            TabHost.TabSpec tabSpec = mTabHost.newTabSpec(mTitles[i]).setIndicator(getTabView(i));
            mTabHost.addTab(tabSpec, mFragmentList.get(i).getClass(), null);
            mTabHost.getTabWidget().getChildAt(i).setBackgroundColor(Color.WHITE);
        }

        mViewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return mFragmentList.get(position);
            }

            @Override
            public int getCount() {
                return mFragmentList.size();
            }
        });

        mViewPager.setOffscreenPageLimit(4);

    }


    private void youmeng() {
        mpAgent.enable();
        mpAgent.enable(new MyCallBack());
        String device_token = mpAgent.getRegistrationId();
        if (device_token!=null&&!device_token.isEmpty()){
            SharedPrefHelper.getInstance(MainActivity.this).setUmDevicetoken(device_token);
        }
    }

    /**
     * 重写友盟的callback方法
     */
    private class MyCallBack implements IUmengRegisterCallback {

        @Override
        public void onRegistered(String arg0) {
            // TODO Auto-generated method stub
            String device_token = mpAgent.getRegistrationId();
            boolean isRegist = mpAgent.isRegistered();
            if (device_token!=null&&!device_token.isEmpty()){
                SharedPrefHelper.getInstance(MainActivity.this).setUmDevicetoken(device_token);
            }
        }
    }

    /**
     * 数据移植
     */
    @Override
    public void initData() {
        if (NetworkUtil.isNetworkAvailable(this)) {
            String listenStr = getStudyLogs();
            if(!TextUtils.isEmpty(listenStr)){
                persenter.upLoadVideos(listenStr);
            }
        }
    }


    @Override
    protected void onResume() {
        initEvent();
        super.onResume();
        SharedPreferences sp = getSharedPreferences("user", Context.MODE_PRIVATE);
        if (!sp.getBoolean("isTransFormUser", false)) {
            SharedPrefHelper.getInstance(this).setIsLogin(false);
            sp.edit().putBoolean("isTransFormUser", true).commit();
            final String username = sp.getString("username", "");
            final String password = sp.getString("password", "");
            if (username != null && password != null && !username.equals("") && !password.equals("")) {
                //TODO 设置到3.0首页的登录窗口进行登录，登录完成后将isTransFormUser设置为true
                Intent intent = new Intent(this,LoginNewActivity.class);
                intent.putExtra("username",username);
                intent.putExtra("psw",password);
                startActivity(intent);
            }
        }

        /**
         * 处理跳转到登录再返回来选中状态和页面不一致的情况
         */
        if(mTabHost != null){
            if(mTabHost.getCurrentTab() != mViewPager.getCurrentItem()){
                mTabHost.setCurrentTab(mViewPager.getCurrentItem());
            }
        }

    }

    private boolean isLogin() {
        return SharedPrefHelper.getInstance(this).isLogin();
    }

    @Override
    protected void loginSuccessed() {
        Toast.makeText(this, "登录成功", Toast.LENGTH_SHORT).show();
        SharedPreferences sp = getSharedPreferences("user", Context.MODE_PRIVATE);
        sp.edit().putBoolean("isTransFormUser", true);
    }

    private void initEvent() {

        mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                if (mTitles[1].equals(tabId) || mTitles[2].equals(tabId)) {
                    if (!isLogin()) {
                        Intent intent = new Intent(MainActivity.this, LoginNewActivity.class);
                        startActivity(intent);
                    } else {
                        if (mTabHost.getCurrentTab() == 1) {
//                            if (StringUtil.isEmpty(SharedPrefHelper.getInstance(MainActivity.this).getSubjectName())) {
//                                Intent intent = new Intent(MainActivity.this, SubjectActivity.class);
//                                startActivity(intent);
//                            }else {
                                showView(mTabHost.getCurrentTab());
//                            }
                        }else {
                            showView(mTabHost.getCurrentTab());
                        }
                    }
                } else {
                    showView(mTabHost.getCurrentTab());
                }
                if("首页".equals(tabId)){
                    MobclickAgent.onEvent(MainActivity.this,PushConstants.TAB_MAIN);
                }else if("课堂".equals(tabId)){
                    MobclickAgent.onEvent(MainActivity.this,PushConstants.TAB_CLASSROOM);
                }else if("学吧".equals(tabId)){
                    MobclickAgent.onEvent(MainActivity.this,PushConstants.TAB_STUDYBAR);
                }
            }

        });

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 1) {
//                    showClassGuide();
                    if (SharedPrefHelper.getInstance(MainActivity.this).getIsfirrstinClassroom()) {
                        showClassGuide();
                    }
                }
               // showView(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void showView(int position) {
        mViewPager.setCurrentItem(position);
//        if (position == 2) {
//            mTabHost.getTabWidget().getChildAt(2).findViewById(R.id.main_push_notify_iv).setVisibility(View.INVISIBLE);
//            SharedPrefHelper.getInstance(this).setShowStudybarNotify(false);
//        }
    }


    private View getTabView(final int index) {
        View view = LayoutInflater.from(this).inflate(R.layout.main_tab_item, null);

        ImageView image = (ImageView) view.findViewById(R.id.image);
        TextView title = (TextView) view.findViewById(R.id.title);
        ImageView main_push_notify_iv = (ImageView) view.findViewById(R.id.main_push_notify_iv);
        if (index == 3) {
            if (SharedPrefHelper.getInstance(this).isShowStudybarNotify()) {
//                main_push_notify_iv.setVisibility(View.VISIBLE);
                main_push_notify_iv.setVisibility(View.INVISIBLE);
            } else {
                main_push_notify_iv.setVisibility(View.INVISIBLE);
            }
        } else {
            main_push_notify_iv.setVisibility(View.INVISIBLE);
        }
        image.setImageResource(mImages[index]);
        title.setText(mTitles[index]);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this,"点击"+mTitles[index],Toast.LENGTH_SHORT).show();
                return;
            }
        });

        return view;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.main_guide:
//                if (guideNo == 0) {
//                    Animation animation = new AlphaAnimation(0, 1);
//                    animation.setDuration(500);
//                    animation.setFillAfter(true);
//                    imageView_guide_2.startAnimation(animation);
//                    imageView_guide_2.setVisibility(View.VISIBLE);
//                    imageView_guide_3.startAnimation(animation);
//                    imageView_guide_3.setVisibility(View.VISIBLE);
//                    guideNo++;
//                } else

                    if (guideNo == 1) {
                    Animation animation = new AlphaAnimation(0, 1);
                    animation.setDuration(500);
                    animation.setFillAfter(true);
                    imageView_guide_4.startAnimation(animation);
                    imageView_guide_4.setVisibility(View.VISIBLE);
                    imageView_guide_5.startAnimation(animation);
                    imageView_guide_5.setVisibility(View.VISIBLE);
                    guideNo++;
                } else {
//                    Intent intent = new Intent(MainActivity.this, LoginNewActivity.class);
//                    startActivity(intent);
                   // userViewManager.showUserCenterWindow();
                    main_guide.setVisibility(View.GONE);
                    SharedPrefHelper.getInstance(MainActivity.this).setFirstIn(false);
                }
                break;
            case R.id.main_classroom_guide:
                linearLayout_classroom_guide.setVisibility(View.GONE);
                SharedPrefHelper.getInstance(MainActivity.this).setIsfirrstinClassroom(false);
                break;
        }
    }

    private List<CwStudyLog> studyLogs;

    private String getStudyLogs() {
        studyLogs = cwStudyLogDB.queryByUserId(SharedPrefHelper.getInstance(this).getUserId());
        String str= CommenUtils.getStudyLogs(studyLogs,SharedPrefHelper.getInstance(this).getUserCode());
        return str;
    }

    private void upLoadPositon() {
        long previours = SharedPrefHelper.getInstance(this).getPreviousEnterTime();
        long now = System.currentTimeMillis();
        if (previours == -1) {
            SharedPrefHelper.getInstance(this).setPreviousEnterTime(now);
            initLocation();
            mLocationClient.startLocation();
        } else {
            Date datePre = new Date(previours);
            Date dateNow = new Date(now);
            int day = SystemUtils.getBetweenDay(datePre, dateNow);
            if (day >= 1) {
                SharedPrefHelper.getInstance(this).setPreviousEnterTime(now);
                initLocation();
                mLocationClient.startLocation();
            }
        }
    }

    public void initLocation() {

        mLocationClient = new AMapLocationClient(getApplicationContext());
        mLocationClient.setLocationListener(this);

        //初始化AMapLocationClientOption对象
        mLocationOption = new AMapLocationClientOption();

        mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);

        mLocationOption.setOnceLocation(false);//true表示启动单次定位，false表示使用默认的连续定位策略。
        mLocationOption.setOnceLocationLatest(true);
        //设置定位间隔,单位毫秒,默认为2000ms，最低1000ms。
        mLocationOption.setInterval(1000);

        //单位是毫秒，默认30000毫秒，建议超时时间不要低于8000毫秒。
        mLocationOption.setHttpTimeOut(20000);

        mLocationClient.setLocationOption(mLocationOption);
    }

    /**
     * 实现实位回调监听
     */
    @Override
    public void onLocationChanged(AMapLocation amapLocation) {
        if (amapLocation != null) {
            if (amapLocation.getErrorCode() == 0) {

                latitude=amapLocation.getLatitude();//获取纬度
                lontitude=amapLocation.getLongitude();//获取经度
                city=amapLocation.getCity();//城市信息
                district=amapLocation.getDistrict();//城区信息
                province=amapLocation.getProvince();//省信息
                mLocationClient.onDestroy();
                persenter.getData();

                if(!TextUtils.isEmpty(province) && !TextUtils.isEmpty(city) && !TextUtils.isEmpty(district)){
                    if(province.equals(city)){
                        SharedPrefHelper.getInstance(MainActivity.this).setUserPosition(province + district);
                    }else{
                        SharedPrefHelper.getInstance(MainActivity.this).setUserPosition(province + city + district);
                    }
                }else if(!TextUtils.isEmpty(province) && !TextUtils.isEmpty(city) && TextUtils.isEmpty(district)){
                    if(province.equals(city)){
                        SharedPrefHelper.getInstance(MainActivity.this).setUserPosition(province);
                    }else{
                        SharedPrefHelper.getInstance(MainActivity.this).setUserPosition(province + city);
                    }
                }else if(!TextUtils.isEmpty(province) && TextUtils.isEmpty(city) && TextUtils.isEmpty(district)){
                    SharedPrefHelper.getInstance(MainActivity.this).setUserPosition(province);
                }
                EventBus.getDefault().post(new PositionedEvent());
            }
        }
    }

    private double latitude;//纬度
    private double lontitude;//经度
    private String addr;
    private String city, province, district;

    public double getLatitude() {
        return latitude;
    }

    public double getLontitude() {
        return lontitude;
    }

    public String getAddr() {
        return addr;
    }

    private void changTab(boolean isRead) {
        if (isRead){
            mTabHost.getTabWidget().getChildAt(3).findViewById(R.id.main_push_notify_iv).setVisibility(View.INVISIBLE);
        }else{
            mTabHost.getTabWidget().getChildAt(3).findViewById(R.id.main_push_notify_iv).setVisibility(View.VISIBLE);
        }
        SharedPrefHelper.getInstance(this).setShowStudybarNotify(!isRead);
    }

    /**
     * 选择科目
     * @param event
     */
    @Subscribe
    public void onEventAsync(UpdateEvent event)
    {
        showView(mTabHost.getCurrentTab());
    }

    @Subscribe
    public void onEventAsync(LoginSuccessEvent event) {
        if (mTabHost.getCurrentTab() == 1) {
//            if (StringUtil.isEmpty(SharedPrefHelper.getInstance(MainActivity.this).getSubjectName())) {
//                Intent intent = new Intent(MainActivity.this, SubjectActivity.class);
//                startActivity(intent);
//            }else {
                showView(mTabHost.getCurrentTab());
//            }
        }else{
            showView(mTabHost.getCurrentTab());
        }
    }


    @Subscribe
    public void onEventAsync(CancelLoginEvent event) {
       /* if(mTabHost != null){
            mTabHost.setCurrentTab(0);
            //showView(mViewPager.getCurrentItem());
        }*/
    }

    @Subscribe
    public void onEventAsync(MainTabChoosed event) {
       mTabHost.setCurrentTab(event.position);
        showView(event.position);
    }

    @Subscribe
    public void onEventMainThread(PushMsgNotification pushMsgNotification) {
        changTab(pushMsgNotification.isRead);
    }

    @Subscribe
    public void onEventMainThread(NewMsgNotification newMsgNotification) {
        changTab(newMsgNotification.isRead);
    }
    @Subscribe
    public void onEventMainThread(PlayUploadRecord newMsgNotification) {
        initData();
    }

    /**
     * 首次进入切换到课堂fragment时打开课堂的引导图
     */
    public void showClassGuide() {
        linearLayout_classroom_guide.setVisibility(View.GONE);
    }


    @Override
    public void setResult() {

    }

    @Override
    public void setCodet(int code) {
        if (code == Constants.RESULT_CODE) {
            for (CwStudyLog cwStudyLog : studyLogs) {
                cwStudyLog.setWatchedAt(0);
                if (CommenUtils.isPlayOver(cwStudyLog)){
                    cwStudyLog.setStatus(Constant.PLAY_STATUS_COMPLETE);
                }
                cwStudyLogDB.update(cwStudyLog);
            }
        }
    }

    private List<CourseWare> donwloadList;

    /**
     * 检查未完成的下载
     */
    private void checkDownloadStatus() {
        if (!SharedPrefHelper.getInstance(this).isLogin()) {
            return;
        }
        donwloadList = new ArrayList<>();
        boolean isDownload = SharedPrefHelper.getInstance(this).getIsNoWifiPlayDownload();
        CourseWare courseWare = db.findStatusDownloading(SharedPrefHelper.getInstance(this).getUserId());
        if (courseWare != null) {
            donwloadList.add(courseWare);
        }
        List<CourseWare> list = db.findStatusWating(SharedPrefHelper.getInstance(this).getUserId());
        if (list != null) {
            donwloadList.addAll(list);
        }
        if (donwloadList != null && donwloadList.size() > 0) {
            NetWorkUtils type = new NetWorkUtils(this);
            if (type.getNetType() == 1) {//wifi
                DialogManager.showNormalDialog(this, "您当前连接的wifi，有未完成的下载，要下载吗？", "提示", "取消", "确定", new DialogManager.CustomDialogCloseListener() {
                    @Override
                    public void yesClick() {

                        for (int i = 0; i < donwloadList.size(); i++) {
                            AppConfig.getAppConfig(MainActivity.this).download(donwloadList.get(i));
                        }
                    }

                    @Override
                    public void noClick() {
                        db.updateState(Constants.STATE_Pause);
                    }
                });
            } else if (type.getNetType() == 2) { //流量
                if(isDownload){
                    DialogManager.showNormalDialog(this, "您当前连接的3G/4G，有未完成的下载，确定下载吗？", "提示", "取消", "确定", new DialogManager.CustomDialogCloseListener() {
                        @Override
                        public void yesClick() {
                            for (int i = 0; i < donwloadList.size(); i++) {
                                AppConfig.getAppConfig(MainActivity.this).download(donwloadList.get(i));
                            }
                        }

                        @Override
                        public void noClick() {
                            db.updateState(Constants.STATE_Pause);
                        }
                    });
                }else{
                    db.updateState(Constants.STATE_Pause);
                }
            } else if (type.getNetType() == 0) {//无网络
                db.updateState(Constants.STATE_Pause);
            }
        }

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public Context context() {
        return this;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SharedPrefHelper.getInstance(this).setIsPlayOnet(false);
        //暂停所有下载
        AppConfig.getAppConfig(this).stopDownload();
        ButterKnife.unbind(this);
        EventBus.getDefault().unregister(this);
    }
}
