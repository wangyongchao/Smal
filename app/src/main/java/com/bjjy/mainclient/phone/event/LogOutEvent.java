package com.bjjy.mainclient.phone.event;

/**
 * Created by fengzongwei on 2016/6/28 0028.
 * 登出
 */
public class LogOutEvent {
    public int type;
    private static final String TAG = "LogOutEvent";
    public LogOutEvent(int type)
    {
        this.type = type;
    }
}
