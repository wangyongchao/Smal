package com.bjjy.mainclient.phone.view.studybar.fragment;


import android.content.Intent;

import com.alibaba.fastjson.JSON;
import com.bjjy.mainclient.phone.event.NewMsgNotification;
import com.bjjy.mainclient.phone.view.studybar.Constant;
import com.dongao.libs.dongaoumengpushlib.constants.PushConstants;
import com.dongao.mainclient.core.util.MD5Util;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.core.util.NetUtils;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ParamsUtils;
import com.dongao.mainclient.model.remote.bean.BaseBean;
import com.bjjy.mainclient.phone.view.setting.WebViewActivity;
import com.bjjy.mainclient.phone.view.studybar.fragment.bean.StudyBar;
import com.bjjy.mainclient.phone.view.studybar.fragment.bean.StudyBarData;
import com.bjjy.mainclient.phone.view.studybar.fragment.db.StudyBarDB;
import com.bjjy.mainclient.phone.view.studybar.privateteacher.PrivateTeacherActivity;
import com.bjjy.mainclient.phone.view.studybar.questionsultion.QuestionSolutionActivity;
import com.umeng.analytics.MobclickAgent;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author wyc
 */
public class StudyBarFragmentPercenter extends BasePersenter<StudyBarFragmentView> {
    public List<StudyBar> studyBarList = new ArrayList<>();
    private String userId;
    private StudyBarDB studyBarDB;
    private String examId;
    private String subjectId;

    @Override
    public void getData() {
        getMvpView().showLoading();
        userId = SharedPrefHelper.getInstance(getMvpView().context()).getUserId() + "";
        studyBarDB = new StudyBarDB(getMvpView().context());

        userId = SharedPrefHelper.getInstance(getMvpView().context()).getUserId();
        ;
        examId = SharedPrefHelper.getInstance(getMvpView().context()).getExamId();
        subjectId = SharedPrefHelper.getInstance(getMvpView().context()).getSubjectId();

//        test();
        if (NetUtils.checkNet(getMvpView().context()).isAvailable()) {
            getInterData();
        } else {
            getLocalData();
        }
    }

    private void getLocalData() {
        studyBarList = studyBarDB.findAll(userId);
        if (studyBarList == null || studyBarList.size() == 0) {//无本地数据
            studyBarList = new ArrayList<>();
            getMvpView().showCurrentView(Constant.VIEW_TYPE_1);
        } else {
            getMvpView().showCurrentView(Constant.VIEW_TYPE_0);
            getMvpView().refreshAdapter();
            judgeShowPrompt();
        }
    }

    private void getInterData() {
        HashMap<String, String> params = new HashMap<>();
        apiModel.getData(ApiClient.getClient().getStudyBarFragment(ParamsUtils.getInstance(getMvpView().context()).getStudyFragment()));
    }

    @Override
    public void setData(String obj) {
        try {
            studyBarList.clear();
            getMvpView().hideLoading();
            BaseBean baseBean = JSON.parseObject(obj, BaseBean.class);
            if (baseBean == null) {
                getMvpView().showCurrentView(Constant.VIEW_TYPE_1);
                return;
            } else {
                int result = baseBean.getResult().getCode();
                if (result != 1000) {
                    getMvpView().showCurrentView(Constant.VIEW_TYPE_3);
                    return;
                }
            }
            String body = baseBean.getBody();
            //设置返回的数据
            StudyBarData studyBarData = JSON.parseObject(body, StudyBarData.class);
            studyBarList.addAll(studyBarData.getStudyBarList());
//            addListTo();
//            studyBarList=studyBarData.getStudyBarList();
            if (studyBarList.size() == 0) {
                getMvpView().showCurrentView(Constant.VIEW_TYPE_2);
                return;
            }
            getOldData();
            saveDB();
            judgeShowPrompt();
            getMvpView().refreshAdapter();
            getMvpView().showCurrentView(Constant.VIEW_TYPE_0);
        } catch (Exception e) {
            getMvpView().showCurrentView(Constant.VIEW_TYPE_3);
        }
    }

    public void addListTo() {
        int size = 5 - studyBarList.size();
        for (int i = 0; i < size; i++) {
            StudyBar studyBar = new StudyBar();
            studyBar.setTitle("测试");
            studyBar.setMessageTitle("测试");
            studyBar.setClassId(i + "");
            studyBar.setType(i);
            studyBarList.add(studyBar);
        }
    }

    public void judgeShowPrompt() {
        boolean isRead = true;
        for (int m = 0; m < studyBarList.size(); m++) {
            if (!studyBarList.get(m).isRead()) {
                isRead = false;
                break;
            }
        }
        SharedPrefHelper.getInstance(getMvpView().context()).setShowStudybarNotify(!isRead);
        EventBus.getDefault().post(new NewMsgNotification(isRead));
    }

    /**
     * 获取老数据，并比较当前的数据是否已读
     */
    private void getOldData() {
        userId = SharedPrefHelper.getInstance(getMvpView().context()).getUserId();
        List<StudyBar> oldList = studyBarDB.findAll(userId);
        if (oldList == null || oldList.size() == 0) {
            for (int i = 0; i < studyBarList.size(); i++) {
                studyBarList.get(i).setIsRead(false);
            }
        } else {
            for (int m = 0; m < studyBarList.size(); m++) {
                studyBarList.get(m).setIsRead(false);
            }
            for (int i = 0; i < studyBarList.size(); i++) {
                for (int j = 0; j < oldList.size(); j++) {
                    if (studyBarList.get(i).getType() == (oldList.get(j).getType())) {
                        if (studyBarList.get(i).getClassId().equals(oldList.get(j).getClassId())) {
                            if (studyBarList.get(i).getMessageCount() < oldList.get(j).getMessageCount()) {
                                studyBarList.get(i).setIsRead(true);
                            } else if (studyBarList.get(i).getMessageCount() == oldList.get(j).getMessageCount() && oldList.get(j).isRead()) {
                                studyBarList.get(i).setIsRead(true);
                            }
                        } 
                    }

                }
            }
        }
    }

    private void saveDB() {
        String userId = SharedPrefHelper.getInstance(getMvpView().context()).getUserId();
        for (int i = 0; i < studyBarList.size(); i++) {
            studyBarList.get(i).setUserId(userId);
            StudyBar studyBar = studyBarDB.findByClassId(userId, studyBarList.get(i).getClassId(), studyBarList.get(i).getType());
            if (studyBar != null) {
                studyBarDB.deleteByExamination(studyBar);
            }
            studyBarDB.insert(studyBarList.get(i));
        }
    }

    public void setOnItemClickListener(int position) {
        int type = studyBarList.get(position).getType();
        studyBarList.get(position).setIsRead(true);
//        if (true){
//            Intent intent1=new Intent(getMvpView().context(),OrderGoodsActivity.class);
//            getMvpView().context().startActivity(intent1);
//            return;
//        }
        if (type == Constants.STUDYBAR_TYPE_PRIVATE) {
            Intent intent = new Intent(getMvpView().context(), PrivateTeacherActivity.class);
            intent.putExtra("typeId", studyBarList.get(position).getNewsType());
            intent.putExtra("classId", studyBarList.get(position).getClassId());
            intent.putExtra(Constants.APP_WEBVIEW_TITLE, studyBarList.get(position).getTitle());
            getMvpView().context().startActivity(intent);
            MobclickAgent.onEvent(getMvpView().context(), PushConstants.STUDYBAR_TO_PRIVATETEACH);
        } else if (type == Constants.STUDYBAR_TYPE_MESSAGE) {
            Intent intent = new Intent(getMvpView().context(), WebViewActivity.class);
            String userId1 = SharedPrefHelper.getInstance(getMvpView().context()).getUserId();
            String userId = SharedPrefHelper.getInstance(getMvpView().context()).getUserId() + Constants.USER_MD5_YAN;
            String signstr = MD5Util.encrypt(userId);
            String url = studyBarList.get(position).getUrl() + "?userId=" + userId1 + "&signstr=" + signstr;
//            url="http://p.m.test.com/appInfo/appLetter.html?userId="+userId;
            intent.putExtra(Constants.APP_WEBVIEW_TITLE, studyBarList.get(position).getTitle());
            intent.putExtra(Constants.APP_WEBVIEW_URL, url);
            getMvpView().context().startActivity(intent);
            MobclickAgent.onEvent(getMvpView().context(), PushConstants.STUDYBAR_TO_MSGCENTER);
        } else if (type == Constants.STUDYBAR_TYPE_QUESTION) {
            Intent intent = new Intent(getMvpView().context(), QuestionSolutionActivity.class);
            intent.putExtra("type", studyBarList.get(position).getType());
            intent.putExtra("classId", studyBarList.get(position).getClassId());
            intent.putExtra(Constants.APP_WEBVIEW_TITLE, studyBarList.get(position).getTitle());
            getMvpView().context().startActivity(intent);
            MobclickAgent.onEvent(getMvpView().context(), PushConstants.STUDYBAR_TO_MYQUESTION);
        }
        saveDB();
        judgeShowPrompt();

    }

    @Override
    public void onError(Exception e) {
        super.onError(e);
        getMvpView().showCurrentView(Constant.VIEW_TYPE_1);
    }
}
