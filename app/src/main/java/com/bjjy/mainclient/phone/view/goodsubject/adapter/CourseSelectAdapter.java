package com.bjjy.mainclient.phone.view.goodsubject.adapter;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.dongao.mainclient.model.bean.courselect.Goods;
import com.dongao.mainclient.model.bean.courselect.GoodsKind;
import com.bjjy.mainclient.phone.R;

import java.util.ArrayList;

/**
 * Created by dell on 2016/4/6.
 */
public class CourseSelectAdapter extends BaseExpandableListAdapter {

    private Context mcontext;
    private ArrayList<GoodsKind> mlist;
    private Handler handler;

    public CourseSelectAdapter(Context context, Handler handler) {
        super();
        this.mcontext = context;
        this.handler = handler;
    }

    public void setList(ArrayList<GoodsKind> mlist) {
        this.mlist = mlist;
    }

    @Override
    public Object getChild(int arg0, int arg1) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getChildId(int arg0, int arg1) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView,
                             ViewGroup arg4) {
        ChildHolder holder;
        if (convertView == null) {
            holder = new ChildHolder();
            convertView = View.inflate(mcontext, R.layout.courselect_item_child, null);
            holder.box = (CheckBox) convertView.findViewById(R.id.course_select_cb);
            holder.title = (TextView) convertView.findViewById(R.id.tv_cousetitle);
            convertView.setTag(holder);
        } else {
            holder = (ChildHolder) convertView.getTag();
        }
        final Goods good = mlist.get(groupPosition).getGoodsList().get(childPosition);
        if(good.isSlected()){
            holder.box.setChecked(true);
        }else{
            holder.box.setChecked(false);
        }
        holder.box.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Message msg = Message.obtain();
                    msg.what = 1;
                    msg.obj = good;
                    handler.sendMessage(msg);
                } else {
                    Message msg = Message.obtain();
                    msg.what = 0;
                    msg.obj = good;
                    handler.sendMessage(msg);
                }
            }
        });
        holder.title.setText(good.getSubjectName());
        return convertView;
    }

    @Override
    public int getChildrenCount(int position) {
        // TODO Auto-generated method stub
        return mlist.get(position).getGoodsList().size();
    }

    @Override
    public Object getGroup(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int getGroupCount() {
        // TODO Auto-generated method stub
        return mlist.size();
    }

    @Override
    public long getGroupId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getGroupView(int position, boolean arg1, View arg2,
                             ViewGroup arg3) {
        View view = View.inflate(mcontext, R.layout.courselect_item_group, null);
        TextView tv = (TextView) view.findViewById(R.id.tv_group);
        tv.setText(mlist.get(position).getClazzName());
        return view;
    }

    @Override
    public boolean hasStableIds() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isChildSelectable(int arg0, int arg1) {
        // TODO Auto-generated method stub
        return false;
    }

    class ChildHolder {
        CheckBox box;
        TextView title;
    }

}
