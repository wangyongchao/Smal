package com.bjjy.mainclient.phone.view.book;


import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ParamsUtils;
import com.dongao.mainclient.model.remote.bean.BaseBean;


/**
 * 随书赠卡Persenter
 */
public class BookActivatePersenter extends BasePersenter<BookActivateView> {
    public static final int TYPE_BOOK = 0;
    public static final int TYPE_MATLL = 1;

    @Override
    public void attachView(BookActivateView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    @Override
    public void getData() {
        //一些业务逻辑的判断 有网没网 是请求网络还是请求数据库
        getMvpView().showLoading();
        apiModel.getData(ApiClient.getClient().bookActivate(
                ParamsUtils.getInstance(getMvpView().context()).bookActivate(getMvpView().cardNum())));
    }

    @Override
    public void setData(String obj) {

        //设置可用
        //usernameEdit.setEnabled(true);
        try {
            BaseBean baseBean = JSON.parseObject(obj, BaseBean.class);
            if (baseBean == null) {
                getMvpView().showAppMsg("服务器异常");
                return;
            }
            if (baseBean.getCode() == 1000) {
                SharedPrefHelper.getInstance(getMvpView().context()).setIsRefreshMainBookList(true);
                getMvpView().showAppMsg("激活成功");
                Toast.makeText(getMvpView().context(), "激活成功", Toast.LENGTH_LONG).show();
                getMvpView().activitySuccess();
            } else {
                getMvpView().showAppMsg(baseBean.getMsg() + "");
            }
        } catch (Exception e) {
            getMvpView().showAppMsg("服务器异常");
            e.printStackTrace();
        }
    }

    public void getData(int mType) {
        //一些业务逻辑的判断 有网没网 是请求网络还是请求数据库
        getMvpView().showLoading();
        if (mType == TYPE_BOOK){
            apiModel.getData(ApiClient.getClient().bookActivate(
                    ParamsUtils.getInstance(getMvpView().context()).bookActivate(getMvpView().cardNum())));
        }else {
            apiModel.getData(ApiClient.getClient().tmallActivate(
                    ParamsUtils.getInstance(getMvpView().context()).tmallActivate(getMvpView().cardNum())));
        }

    }
}
