package com.bjjy.mainclient.phone.view.play.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.AppContext;
import com.bjjy.mainclient.phone.app.BaseFragment;
import com.bjjy.mainclient.phone.download.db.DownloadDB;
import com.bjjy.mainclient.phone.utils.NetworkUtil;
import com.bjjy.mainclient.phone.view.play.PlayActivity;
import com.bjjy.mainclient.phone.view.play.adapter.CourseWareEpListAdapter;
import com.bjjy.mainclient.phone.view.play.adapter.CwListAdapter;
import com.bjjy.mainclient.phone.view.play.downloadmanager.DownLoadManagerActivity;
import com.bjjy.mainclient.phone.widget.DialogManager;
import com.dongao.mainclient.model.bean.play.CourseDetail;
import com.dongao.mainclient.model.bean.play.CourseWare;
import com.dongao.mainclient.model.local.SharedPrefHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by fengzongwei on 2016/1/4.
 */
public class CourseListFragment extends BaseFragment {
    @Bind(R.id.manager_tv)
    TextView managerTv;
    private View view;
    private ListView listView;//不存在章节区分时显示此控件
    private CourseWareEpListAdapter courseWareListAdapter_courseEp;
    private CwListAdapter courseListAdapter;
    private LinearLayout linearLayout_loading, linearLayout_error;
    private RelativeLayout relativeLayout_loadingBody;

    private List<Object> list_nosection = new ArrayList<>();

    private PlayActivity playActivity;

    private CourseDetail courseDetail;
    private List<CourseWare> courseWareList;
    private DownloadDB db;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 1 && msg.obj != null && msg.obj instanceof CourseWare) {
                onLongClickCheckDownload((CourseWare) msg.obj);
                return;
            }
            if (msg.what == 2 && msg.obj != null && msg.obj instanceof CourseWare) {//有章节切换课程
//                if (courseDetail.getMobileSectionList().get(msg.arg1).getMobileCourseWareList().get(msg.arg2).getCwId().equals("kehouzuoye")) {
//                    SharedPrefHelper.getInstance(getActivity()).setExamTag(Constants.EXAM_TAG_ABILITY);
//                    Intent intent = new Intent(getActivity(), ExamActivity.class);
//                    getActivity().startActivity(intent);
//                    return;
//                }
//                CourseWare courseWare = courseDetail.getMobileSectionList().get(msg.arg1).getMobileCourseWareList().get(msg.arg2);
//                playActivity.playVedio(courseWare, msg.arg1, msg.arg2);
//                courseWareListAdapter_courseEp.notifyDataSetChanged();
            }
            if (msg.what == 3) {//没有章节时切换或暂停视频
                CourseWare courseWare = courseWareList.get(msg.arg1);
                playActivity.playVedio(courseWare, -1, msg.arg1);
                courseListAdapter.notifyDataSetChanged();
            }
            if (courseWareListAdapter_courseEp != null) {
                courseWareListAdapter_courseEp.notifyDataSetChanged();
            }
            if (courseListAdapter != null) {
                courseListAdapter.notifyDataSetChanged();
            }

        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        playActivity = (PlayActivity) getActivity();
        view = inflater.inflate(R.layout.play_courselist_fragment, container, false);
        ButterKnife.bind(this, view);
        db = new DownloadDB(getActivity());
        initView();
        return view;
    }


    @Override
    public void initData() {

    }

    @Override
    public void initView() {
        managerTv.setOnClickListener(this);
        AppContext.getInstance().setHandlerDetail(handler);
        relativeLayout_loadingBody = (RelativeLayout) view.findViewById(R.id.courselist_fragment_loading_body_rl);
        linearLayout_loading = (LinearLayout) view.findViewById(R.id.courselist_fragment_loading_body_ll);
        linearLayout_error = (LinearLayout) view.findViewById(R.id.courselist_fragment_error_body_rl);
        listView = (ListView) view.findViewById(R.id.play_courselist_fragment_course_lv);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                onLongClickCheckDownload(courseWareList.get(position));
                return false;
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.manager_tv:
                if (courseWareList != null)
                    DownLoadManagerActivity.startActivityByCourseList(getActivity(), (ArrayList<CourseWare>) courseWareList);
                if (courseDetail != null)
//                    DownLoadManagerActivity.startActivityByCourseChapterList(getActivity(), (ArrayList<CourseChapter>) courseDetail.getMobileSectionList());
                break;
        }
    }


    public void showCwData(List<CourseWare> courseWareList) {
        if (relativeLayout_loadingBody != null)
            relativeLayout_loadingBody.setVisibility(View.GONE);
        this.courseWareList = courseWareList;
        listView.setVisibility(View.VISIBLE);
        courseListAdapter = new CwListAdapter(playActivity, courseWareList, handler);
        listView.setAdapter(courseListAdapter);
        for (int i = 0; i < list_nosection.size(); i++) {
            if (list_nosection.get(i) instanceof CourseWare) {
                if (((CourseWare) list_nosection.get(i)).getCwId().equals(playActivity.getPlayingCWId())) {
                    listView.setSelection(i);
                }
            }
        }
    }

    /**
     * 设置课程目录数据加载失败显示
     */
    public void setLoadDataError() {
        linearLayout_loading.setVisibility(View.GONE);
        linearLayout_error.setVisibility(View.VISIBLE);
    }

    public void notifyAdapter() {
        if (courseListAdapter != null)
            courseListAdapter.notifyDataSetChanged();
        if (courseWareListAdapter_courseEp != null)
            courseWareListAdapter_courseEp.notifyDataSetChanged();
    }

    /**
     * 条目长按时进行课件下载状态的判断，如果可以离线观看则提示
     *
     * @param cw
     */
    private void onLongClickCheckDownload(final CourseWare cw) {
        int percent = db.getPercent(SharedPrefHelper.getInstance(getActivity()).getUserId(), cw.getClassId(), cw.getCwId());
        if (percent >= 70 && percent < 100) {
            playActivity.pauseVideo();
            DialogManager.showNormalDialog(playActivity, "该视频可离线观看，是否进行离线观看？", "", "在线观看", "离线观看", new DialogManager.CustomDialogCloseListener() {
                @Override
                public void yesClick() {
                    //TODO  选择离线观看
                    if (playActivity.getPlayingCWId().equals(cw.getCwId())) {
                        if (playActivity.isPlaying())
                            playActivity.startVideo();
                        else {
                            playActivity.setIsPlayFromLocal(true);
                            playActivity.startPlayVideo(cw);
                        }
                    } else {
                        playActivity.setIsPlayFromLocal(true);
                        playActivity.startPlayVideo(cw);
                    }
                    courseListAdapter.notifyDataSetChanged();
                    Toast.makeText(playActivity, "离线观看", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void noClick() {
                    if (NetworkUtil.isNetworkAvailable(playActivity)) {
                        if (playActivity.getPlayingCWId().equals(cw.getCwId())) {
                            if (!playActivity.isPlaying())
                                playActivity.startVideo();
                            else
                                playActivity.startPlayVideo(cw);
                        } else
                            playActivity.startPlayVideo(cw);
                    } else {
                        DialogManager.showMsgDialog(getActivity(),
                                getResources().getString(R.string.dialog_message_vedio),
                                getResources().getString(R.string.dialog_title_vedio), "确定");
                    }
                }
            });
        }
//        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
