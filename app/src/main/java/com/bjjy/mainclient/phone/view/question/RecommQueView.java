package com.bjjy.mainclient.phone.view.question;

import com.dongao.mainclient.model.mvp.MvpView;
import com.dongao.mainclient.model.bean.question.Question;

import java.util.List;

/**
 * Created by fengzognwei on 2016/5/3 0003.
 */
public interface RecommQueView extends MvpView{
    void showData(List<Question> lisMyQuestionObjs);//显示推荐答疑内容
    String getQuestionId();
}
