package com.bjjy.mainclient.phone.view.exam.activity;

import android.content.Intent;
import android.os.Bundle;

import com.bjjy.mainclient.phone.view.exam.ExamActivity;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.bjjy.mainclient.phone.view.exam.utils.CommenUtils;

/**
 * Created by wyc on 2016/5/3.
 */
public class ExamReportPersenter extends BasePersenter<ExamReportView> {
    private int report_false_num;
    private int report_true_num;
    private String time;
    private String score;
    private String typeId;
    private String examId;
    private String examinationId;
    private String subjectId;

    @Override
    public void getData() {
       // exam_tag = SharedPrefHelper.getInstance(getMvpView().context()).getExamTag();
        int exam_tag= Constants.EXAM_TAG_ABILITY;
        Intent intent=getMvpView().getWayIntent();
        Bundle bundle=intent.getExtras();
        report_false_num=bundle.getInt("error_num", 0);
        report_true_num=bundle.getInt("right_num", 0);
        //time=bundle.getString("time", "");
        time=bundle.getString("time");
        score=bundle.getString("score");
        float sc=Float.valueOf(score);
        String pingjia=CommenUtils.getPingJia(sc,getMvpView().getAppContext().getAllList());
        getMvpView().showPingJia(pingjia);
        if (null==time){
            time="";
        }
        if (null==score){
            score="";
        }
        getIntentData(bundle);
        getMvpView().setErrorNumber(report_false_num);
        getMvpView().setRightNumber(report_true_num);
        getMvpView().setScore(score);
        getMvpView().setUseTime(time);
        getMvpView().setVisible(exam_tag);

    }

    @Override
    public void setData(String obj) {

    }

    @Override
    public void attachView(ExamReportView mvpView) {
        super.attachView(mvpView);
    }

    public void analyzeWrong(){
        getMvpView().getAppContext().setQuestionlist(getMvpView().getAppContext().getErrorList());
        Intent intent_wrong = new Intent(getMvpView().context(),ExamActivity.class);
        //把数据传递给答题报告
        intent_wrong.putExtra("typeId", typeId);
        intent_wrong.putExtra("examId", examId);
        intent_wrong.putExtra("examinationId", examinationId);
        intent_wrong.putExtra("subjectId", subjectId);
        SharedPrefHelper.getInstance(getMvpView().context()).setExamTag(Constants.EXAM_TAG_REPORT);
        getMvpView().context().startActivity(intent_wrong);
    }

    public void analyzeAll(){
        getMvpView().getAppContext().setQuestionlist(getMvpView().getAppContext().getAllList());
        Intent intent_all = new Intent(getMvpView().context(),ExamActivity.class);
        intent_all.putExtra("typeId", typeId);
        intent_all.putExtra("examId", examId);
        intent_all.putExtra("examinationId", examinationId);
        intent_all.putExtra("subjectId", subjectId);
        SharedPrefHelper.getInstance(getMvpView().context()).setExamTag(Constants.EXAM_TAG_REPORT);
        getMvpView().context().startActivity(intent_all);
    }

    /**获取intent传递过来的数据
     * @param bundle*/
    private void getIntentData(Bundle bundle) {
        typeId=bundle.getString("typeId");
        if (typeId==null||typeId.isEmpty()){
            typeId=SharedPrefHelper.getInstance(getMvpView().context()).getMainTypeId();
        }
        examId=bundle.getString("examId");
        if (examId==null||examId.isEmpty()){
            examId=SharedPrefHelper.getInstance(getMvpView().context()).getExamId();
        }
        examinationId=bundle.getString("examinationId");
        if (examinationId ==null||examinationId.isEmpty()){
            examinationId=SharedPrefHelper.getInstance(getMvpView().context()).getExaminationId();
        }
        subjectId=bundle.getString("subjectId");
        if (subjectId==null||subjectId.isEmpty()){
            subjectId=SharedPrefHelper.getInstance(getMvpView().context()).getSubjectId();
        }
    }
}
