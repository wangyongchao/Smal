package com.bjjy.mainclient.phone.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.bjjy.mainclient.phone.event.PaySuccessEvent;
import com.bjjy.mainclient.phone.view.exam.view.CustomDialog;
import com.bjjy.mainclient.phone.view.payment.utis.IPUtils;
import com.bjjy.mainclient.phone.view.setting.WebViewActivity;
import com.bjjy.mainclient.phone.view.studybar.Constant;
import com.dongao.mainclient.core.payment.payutils.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ApiService;
import com.dongao.mainclient.model.remote.ParamsUtils;
import com.dongao.mainclient.model.remote.bean.BaseBean;
import com.bjjy.mainclient.phone.R;
import com.tencent.mm.sdk.constants.ConstantsAPI;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WXPayEntryActivity extends Activity implements IWXAPIEventHandler, View.OnClickListener {
	private static final String TAG = "MicroMsg.SDKSample.WXPayEntryActivity";
    private IWXAPI api;
//	@Bind(R.id.pay_result_ll)
//	LinearLayout pay_result_ll;
//	@Bind(R.id.top_title_left)
//	ImageView top_title_left;
//	@Bind(R.id.top_title_text)
//	TextView top_title_text;
	private CustomDialog dialog_complete_unsubmit;
	private TextView tv_exam_contiue;
	private TextView tv_exam_giveup;
	private TextView tv_exam_nextdo;
	private TextView dialog_title;
	private TextView dialog_subtitle;
	private int resultStatus;
	private String payWay;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.pay_result);
		ButterKnife.bind(this);
		initView();
    	api = WXAPIFactory.createWXAPI(this, Constants.APP_ID);
        api.handleIntent(getIntent(), this);
//		top_title_left.setVisibility(View.VISIBLE);
//		top_title_text.setText("支付成功");
    }
//	@OnClick(R.id.top_title_left) void onBack() {
//		onBackPressed();
//	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
        api.handleIntent(intent, this);
	}

	private void initView() {
		payWay= SharedPrefHelper.getInstance(WXPayEntryActivity.this).getPayFromWay();
//		pay_result_ll.getBackground().setAlpha(0);
		dialog_complete_unsubmit = new CustomDialog(this, R.layout.dialog_complete_unsubmit, R.style.Theme_dialog);
		dialog_complete_unsubmit.setCanceledOnTouchOutside(false);
		tv_exam_contiue = (TextView) dialog_complete_unsubmit.findViewById(R.id.tv_exam_contiue);
		tv_exam_contiue.setOnClickListener(this);
		tv_exam_giveup = (TextView) dialog_complete_unsubmit.findViewById(R.id.tv_exam_giveup);
		tv_exam_giveup.setOnClickListener(this);
		tv_exam_nextdo = (TextView) dialog_complete_unsubmit.findViewById(R.id.tv_exam_nextdo);
		tv_exam_nextdo.setOnClickListener(this);
		dialog_title = (TextView) dialog_complete_unsubmit.findViewById(R.id.dialog_title);
		dialog_subtitle = (TextView) dialog_complete_unsubmit.findViewById(R.id.dialog_subtitle);
		if (payWay.equals(Constant.PAY_WAY_FROM_SHOUYE)){
			tv_exam_contiue.setText(getString(R.string.pay_result_back));
		}else{
			tv_exam_contiue.setText(getString(R.string.pay_result_back_dingdan));
		}
		
	}

	private void initData() {
		String ip= IPUtils.getIP(WXPayEntryActivity.this);
		payOpen(ParamsUtils.getInstance(WXPayEntryActivity.this).payOpen(ip));
	}

	private void payOpen(HashMap<String, String> params ) {
		Call<String> call = ApiClient.getClient().paySuccessOpen(params);
		call.enqueue(new Callback<String>() {
			@Override
			public void onResponse(Call<String> call, Response<String> response) {
				if (response.isSuccessful()) {
					try {
						String str = response.body();
						BaseBean baseBean = JSON.parseObject(str, BaseBean.class);
						if (baseBean == null) {
							return;
						} else {
							int result = baseBean.getCode();
							if (result != 1000) {
								return;
							}
						}
					} catch (Exception e) {
					}

				} else {
				}
			}

			@Override
			public void onFailure(Call<String> call, Throwable t) {
			}
		});
	}


	@Override
	public void onReq(BaseReq req) {
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.tv_exam_contiue:
				EventBus.getDefault().post(new PaySuccessEvent());
				dialog_complete_unsubmit.dismiss();
				this.finish();
				break;
			case R.id.tv_exam_nextdo:

				if (resultStatus==0) {//支付成功
					Intent intent = new Intent(this, WebViewActivity.class);
					intent.putExtra(com.dongao.mainclient.model.common.Constants.APP_WEBVIEW_TITLE, "选课购买");
					intent.putExtra(com.dongao.mainclient.model.common.Constants.APP_WEBVIEW_URL, ApiService.SELECT_COURSE_URL);
					startActivity(intent);
				}
				dialog_complete_unsubmit.dismiss();
				this.finish();
				break;
		}

	}

	@Override
	public void onResp(BaseResp resp) {
		if (resp.getType() == ConstantsAPI.COMMAND_PAY_BY_WX) {
			resultStatus=resp.errCode;
			if (resp.errCode==0) {//成功
				dialog_title.setText(getString(R.string.pay_result_success));
				dialog_subtitle.setText(getString(R.string.pay_result_success_title));
				tv_exam_nextdo.setText(getString(R.string.pay_result_continue));
				initData();
			} else {
				dialog_title.setText(getString(R.string.pay_result_error));
				dialog_subtitle.setVisibility(View.GONE);
				tv_exam_nextdo.setText(getString(R.string.pay_result_restart_buy));
			}

			dialog_complete_unsubmit.show();
//			if (resp.errCode==0){
//				EventBus.getDefault().post(new PaySuccessEvent());
//				pay_result_ll.getBackground().setAlpha(255);
//
//			}else{
//				pay_result_ll.getBackground().setAlpha(0);
//				Toast.makeText(this, getResources().getString(R.string.pay_result_error), Toast.LENGTH_SHORT).show();
//				this.finish();
//			}
		}else{
//			pay_result_ll.getBackground().setAlpha(0);
			Toast.makeText(this, getResources().getString(R.string.pay_result_error), Toast.LENGTH_SHORT).show();
			this.finish();
		}
	}
}