package com.bjjy.mainclient.phone.view.myorder;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import com.bjjy.mainclient.phone.event.PaySuccessEvent;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseFragmentActivity;
import com.bjjy.mainclient.phone.view.myorder.adpter.MyOrderAdapter;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by fengzongwei on 2016/4/6.
 */
public class MyOrderActivity extends BaseFragmentActivity {
    @Bind(R.id.my_order_vp)
    ViewPager mPager;
    @Bind(R.id.my_order_viewpagertab)
    SmartTabLayout mViewPagerTab;
    @Bind(R.id.top_title_text)
    TextView textView_title;
    @Bind(R.id.top_title_left)
    ImageView imageView_back;
    public static final int RELOGIN = 11;
    private MyOrderAdapter myOrderAdapter;
    private MaterialDialog materialDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_order);
        EventBus.getDefault().register(this);
        ButterKnife.bind(this);
        initView();
    }

    @Override
    public void initView() {
        textView_title.setText("我的订单");
        imageView_back.setVisibility(View.VISIBLE);
        imageView_back.setImageResource(R.drawable.back);

        mViewPagerTab = (SmartTabLayout) findViewById(R.id.my_order_viewpagertab);
        mPager = (ViewPager) findViewById(R.id.my_order_vp);
        myOrderAdapter = new MyOrderAdapter(getSupportFragmentManager());
        mPager.setAdapter(myOrderAdapter);
        mViewPagerTab.setViewPager(mPager);
        materialDialog = new MaterialDialog(this);
        materialDialog.setTitle("提示");
        materialDialog.setMessage("当前账号已被踢出，请重新登录");
        materialDialog.setPositiveButton("确定", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(MyOrderActivity.this, LoginNewActivity.class);
//                startActivityForResult(intent, MyOrderActivity.RELOGIN);
            }
        });
    }


    @OnClick(R.id.top_title_left) void back(){
        this.finish();
    }

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View v) {

    }

    public void showLoginDialog(){
        materialDialog.show();
    }

    @Subscribe
    public void onEventMainThread(PaySuccessEvent event) {
        //TODO 支付成功后进行刷新
        myOrderAdapter.loginCallBack();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == RESULT_OK ){
            myOrderAdapter.loginCallBack();
        }
    }


}
