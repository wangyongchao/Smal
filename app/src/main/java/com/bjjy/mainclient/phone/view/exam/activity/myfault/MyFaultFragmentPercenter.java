package com.bjjy.mainclient.phone.view.exam.activity.myfault;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;

import com.bjjy.mainclient.phone.view.exam.ExamActivity;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.bjjy.mainclient.phone.view.exam.activity.myfault.bean.ExaminationClass;
import com.bjjy.mainclient.phone.view.exam.db.FaltQuestionDB;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wyc on 2016/5/18.
 */
public class MyFaultFragmentPercenter extends BasePersenter<MyFaultFragmentView> {
    private String userId;
    private String examId;
    private String subjectId;
    private String typeId;
    public List<ExaminationClass> faultList;
    public boolean isVisible=true;
    public boolean mHasLoadedOnce;
    public boolean isLogin;
    /** 标志位，标志已经初始化完成 */
    public boolean isPrepared;

    @Override
    public void getData() {
       
        userId= SharedPrefHelper.getInstance(getMvpView().context()).getUserId()+"";
        examId=SharedPrefHelper.getInstance(getMvpView().context()).getExamId();
        subjectId=SharedPrefHelper.getInstance(getMvpView().context()).getSubjectId();
        typeId=SharedPrefHelper.getInstance(getMvpView().context()).getFaultTypeId();

        FaltQuestionDB faltQuestionDB=new FaltQuestionDB(getMvpView().context());
        faultList=faltQuestionDB.findAllByName(userId, examId, subjectId,typeId);
        if (faultList==null){
            faultList=new ArrayList<>();
        }
        getMvpView().initAdapter();
        mHasLoadedOnce = true; //设置是否
        if (faultList.size()==0){
            getMvpView().getEmptyView().showEmpty();
        }else{
            getMvpView().getEmptyView().showContentView();
        }
        getMvpView().setIsRefresh(false);
    }

    @Override
    public void setData(String obj) {

    }
    
    public void setUserHint(boolean isTrue){
        if(isTrue) {
            isVisible = true;
            boolean isLogins = SharedPrefHelper.getInstance(getMvpView().context()).isLogin();
            String examid = SharedPrefHelper.getInstance(getMvpView().context()).getExamId();
            if(isLogins != isLogin || examid .equals( examId)){
                mHasLoadedOnce = false;
            }
            getMvpView().setIsRefresh(true);
            getData();
        } else {
            isVisible = false;
        }
    }

    public void setOnItemClick(AdapterView<?> parent, View view, int position, long id){
        SharedPrefHelper.getInstance(getMvpView().context()).setExamTag(Constants.EXAM_TAG_FALT);
        Intent intent=new Intent(getMvpView().context(), ExamActivity.class);
        intent.putExtra("examinationId",faultList.get(position).getExaminationId());
        getMvpView().context().startActivity(intent);
    }
    
}
