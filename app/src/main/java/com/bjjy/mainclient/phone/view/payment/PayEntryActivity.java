package com.bjjy.mainclient.phone.view.payment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.bjjy.mainclient.phone.view.studybar.Constant;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ApiService;
import com.dongao.mainclient.model.remote.ParamsUtils;
import com.dongao.mainclient.model.remote.bean.BaseBean;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.event.PaySuccessEvent;
import com.bjjy.mainclient.phone.view.exam.view.CustomDialog;
import com.bjjy.mainclient.phone.view.payment.utis.IPUtils;
import com.bjjy.mainclient.phone.view.setting.WebViewActivity;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PayEntryActivity extends Activity implements View.OnClickListener {
    private String resultStatus;
    private TextView tv_exam_contiue;
    private TextView tv_exam_giveup;
    private TextView tv_exam_nextdo;
    private TextView dialog_title;
    private TextView dialog_subtitle;
    private CustomDialog dialog_complete_unsubmit;
    @Bind(R.id.pay_result_ll)
    LinearLayout pay_result_ll;
    private String payWay;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.pay_result);
        ButterKnife.bind(this);
        initView();
    }

    private void initView() {
        payWay= SharedPrefHelper.getInstance(PayEntryActivity.this).getPayFromWay();
       
        dialog_complete_unsubmit = new CustomDialog(this, R.layout.dialog_complete_unsubmit, R.style.Theme_dialog);
        dialog_complete_unsubmit.setCanceledOnTouchOutside(false);
        tv_exam_contiue = (TextView) dialog_complete_unsubmit.findViewById(R.id.tv_exam_contiue);
        tv_exam_contiue.setOnClickListener(this);
        tv_exam_giveup = (TextView) dialog_complete_unsubmit.findViewById(R.id.tv_exam_giveup);
        tv_exam_giveup.setOnClickListener(this);
        tv_exam_nextdo = (TextView) dialog_complete_unsubmit.findViewById(R.id.tv_exam_nextdo);
        tv_exam_nextdo.setOnClickListener(this);
        dialog_title = (TextView) dialog_complete_unsubmit.findViewById(R.id.dialog_title);
        dialog_subtitle = (TextView) dialog_complete_unsubmit.findViewById(R.id.dialog_subtitle);
        if (payWay.equals(Constant.PAY_WAY_FROM_SHOUYE)){
            tv_exam_contiue.setText(getString(R.string.pay_result_back));
        }else{
            tv_exam_contiue.setText(getString(R.string.pay_result_back_dingdan));
        }
        Intent intent = getIntent();
        resultStatus = intent.getStringExtra("resultStatus");
        if (TextUtils.equals(resultStatus, "9000")) {
            dialog_title.setText(getString(R.string.pay_result_success));
            dialog_subtitle.setText(getString(R.string.pay_result_success_title));
            tv_exam_nextdo.setText(getString(R.string.pay_result_continue));
            initData();
        } else {
            // “8000”代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
            if (TextUtils.equals(resultStatus, "8000")) {
                Toast.makeText(PayEntryActivity.this, "支付结果确认中",
                        Toast.LENGTH_SHORT).show();

            } else {
                // 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
//				Toast.makeText(context, "支付失败",
//						Toast.LENGTH_SHORT).show();
            }
            dialog_title.setText(getString(R.string.pay_result_error));
            dialog_subtitle.setVisibility(View.GONE);
            tv_exam_nextdo.setText(getString(R.string.pay_result_restart_buy));
        }

        dialog_complete_unsubmit.show();
    }

    private void initData() {
        String ip= IPUtils.getIP(PayEntryActivity.this);
        payOpen(ParamsUtils.getInstance(PayEntryActivity.this).payOpen(ip));
    }

    private void payOpen(HashMap<String, String> params ) {
        Call<String> call = ApiClient.getClient().paySuccessOpen(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    try {
                        String str = response.body();
                        BaseBean baseBean = JSON.parseObject(str, BaseBean.class);
                        if (baseBean == null) {
                            return;
                        } else {
                            int result = baseBean.getCode();
                            if (result != 1000) {
                                return;
                            }
                        }
                    } catch (Exception e) {
                    }

                } else {
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_exam_contiue:
                EventBus.getDefault().post(new PaySuccessEvent());
                dialog_complete_unsubmit.dismiss();
                PayEntryActivity.this.finish();
                break;
            case R.id.tv_exam_nextdo:
                
                if (TextUtils.equals(resultStatus, "9000")) {//支付成功
                    Intent intent = new Intent(PayEntryActivity.this, WebViewActivity.class);
                    intent.putExtra(Constants.APP_WEBVIEW_TITLE, "选课购买");
                    intent.putExtra(Constants.APP_WEBVIEW_URL, ApiService.SELECT_COURSE_URL);
                    startActivity(intent);
                } 
                dialog_complete_unsubmit.dismiss();
                PayEntryActivity.this.finish();
                break;
        }

    }
}