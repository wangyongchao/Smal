package com.bjjy.mainclient.phone.view.book;


import com.dongao.mainclient.model.mvp.MvpView;

/**
 * UI 定义UI 看此UI中有何事件
 */
public interface BookActivateView extends MvpView {
  String cardNum();
  void showAppMsg(String data);
  void activitySuccess();
}
