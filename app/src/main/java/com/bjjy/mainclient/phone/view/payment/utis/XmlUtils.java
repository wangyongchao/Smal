package com.bjjy.mainclient.phone.view.payment.utis;

import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;

import java.io.StringReader;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wyc on 2016/12/6.
 */
public class XmlUtils {
    public static List<Object> parseXml(String xmlStr, Class<?> clazz) {
        String itemElement="xml";
        List<String> elements = getElements();
        List<String> fields = getElements();
       return parse(xmlStr, clazz, fields,  elements,  itemElement );
    }


    /**
     * 解析XML转换成对象
     *
     * @param clazz       对象Class
     * @param fields      字段集合一一对应节点集合
     * @param elements    节点集合一一对应字段集合
     * @param itemElement 每一项的节点标签
     * @return
     */
    public static List<Object> parse(String xmlStr, Class<?> clazz,
                                     List<String> fields, List<String> elements, String itemElement) {
        List<Object> list = new ArrayList<Object>();
        try {
            XmlPullParser xmlPullParser = Xml.newPullParser();
//            xmlPullParser.setInput(is, "UTF-8");
            xmlPullParser.setInput(new StringReader(xmlStr));
            int event = xmlPullParser.getEventType();

            Object obj = null;
            obj = clazz.newInstance();
            while (event != XmlPullParser.END_DOCUMENT) {
               
                switch (event) {
                    case XmlPullParser.START_TAG:
//                        obj = clazz.newInstance();
//                        if (itemElement.equals(xmlPullParser.getName())) {
//                        }
                        if (obj != null
                                && elements.contains(xmlPullParser.getName())) {
                            setFieldValue(obj, fields.get(elements
                                            .indexOf(xmlPullParser.getName())),
                                    xmlPullParser.nextText());
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        if (itemElement.equals(xmlPullParser.getName())) {
                            list.add(obj);
                            obj = null;
                        }
                        break;
                }
                event = xmlPullParser.next();
            }
        } catch (Exception e) {
            throw new RuntimeException("解析XML异常：" + e.getMessage());
        }
        return list;
    }

    private static List<String> getElements() {
        List<String> list = new ArrayList<>();
        list.add("result_code");
        list.add("result_msg");
        list.add("result_val");
        list.add("sign");
        return list;
    }

    /**
     * 设置字段值
     *
     * @param propertyName 字段名
     * @param obj          实例对象
     * @param value        新的字段值
     * @return
     */
    public static void setFieldValue(Object obj, String propertyName,
                                     Object value) {
        try {
            Field field = obj.getClass().getDeclaredField(propertyName);
            field.setAccessible(true);
            field.set(obj, value);
        } catch (Exception ex) {
            throw new RuntimeException();
        }
    }

}
