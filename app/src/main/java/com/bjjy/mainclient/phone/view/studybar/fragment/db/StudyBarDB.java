package com.bjjy.mainclient.phone.view.studybar.fragment.db;

import android.content.Context;

import com.bjjy.mainclient.phone.view.studybar.fragment.bean.StudyBar;
import com.dongao.mainclient.model.local.KaoQianDBHelper;
import com.yunqing.core.db.DBExecutor;
import com.yunqing.core.db.sql.Sql;
import com.yunqing.core.db.sql.SqlFactory;

import java.util.List;


/**
 * Created by wyc on 16/6/3.
 * 此类DAO只用于查询和特殊操作
 */
public class StudyBarDB {
    Context mContext;
    DBExecutor dbExecutor = null;
    Sql sql = null;

    public StudyBarDB(Context mContext){
        this.mContext = mContext;
        dbExecutor =  DBExecutor.getInstance(KaoQianDBHelper.getInstance(mContext));
    }

    public void insert(StudyBar collection){
        dbExecutor.insert(collection);
    }

    public void update(StudyBar collection){
        dbExecutor.updateById(collection);
    }

    public StudyBar find(String id){
        return dbExecutor.findById(StudyBar.class,id);
    }


    /**
     * 搜索全部记录count
     * @return
     */
    public long findAllCount(){
       return dbExecutor.count(StudyBar.class);
    }


    /**
     * 查询全部的学习中心的列表
     * @return
     */
    public List<StudyBar> findAll(String userId){
        sql = SqlFactory.find(StudyBar.class).where("userId=?", new Object[]{userId}).orderBy("dbId",true);
        return dbExecutor.executeQuery(sql);
    }


    /**
     * 查询一个学吧
     */
    public StudyBar findByClassId(String userId,String  classId,int type){
        sql = SqlFactory.find(StudyBar.class).where("userId=? and classId=? and type=?", new Object[]{userId,classId,type}).orderBy("dbId",true);
        return dbExecutor.executeQueryGetFirstEntry(sql);
    }

    /**
     * 删除答题记录
     */

    public void deleteByExamination(StudyBar StudyBar) {
            dbExecutor.deleteById(StudyBar.class, StudyBar.getDbId());//   .delete(Collection.class, collection);
    }
}
