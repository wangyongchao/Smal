package com.bjjy.mainclient.phone.view.question;

import com.dongao.mainclient.model.mvp.MvpView;
import com.dongao.mainclient.model.bean.question.Question;

import java.util.List;

/**
 * Created by fengzongwei on 2016/5/5 0005.
 */
public interface MyQuestionListView extends MvpView{
    void showData(List<Question> questionList);//显示下拉刷新获取的数据
    int getPage();//要加载第几页数据
    void showLoadMoreError(String message);//加载更多数据失败处理动作
    void showMoreData(List<Question> questionList);//显示加载更多获取的数据
    void bookListJson(String json);//获取到的科目下的可选章节数据json
    void showNoData();//显示没有数据界面
    void isCanAsk(String flag);//用户可以提问，进行跳转
    void checkIsCanAskFail();//检查是否可以提问失败
    String getQuestionId();//获取题的id(推荐答疑使用)
    void setTotalPage(int totalPage);//设置数据总页数
}
