package com.bjjy.mainclient.phone.view.studybar.down;

import android.annotation.TargetApi;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bjjy.mainclient.phone.app.BaseFragment;
import com.bjjy.mainclient.phone.download.db.DownloadDB;
import com.bjjy.mainclient.phone.event.DeleteEvent;
import com.bjjy.mainclient.phone.view.exam.activity.exampaperlist.bean.Course;
import com.bjjy.mainclient.phone.view.setting.cache.adapter.CacheCourseAdapter;
import com.bjjy.mainclient.phone.widget.DialogManager;
import com.bjjy.mainclient.phone.widget.statusbar.Utils;
import com.dongao.mainclient.core.util.DensityUtil;
import com.dongao.mainclient.model.bean.play.CourseWare;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.AppContext;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author wyc
 */
public class CacheFragment extends BaseFragment implements ViewPager.OnPageChangeListener {

    @Bind(R.id.top_title_left)
    ImageView topTitleLeft;
    @Bind(R.id.top_title_right)
    ImageView topTitleRight;
    @Bind(R.id.top_title_text)
    TextView topTitleText;
    @Bind(R.id.top_title_bar_layout)
    RelativeLayout topTitleBarLayout;

    @Bind(R.id.download_menu_tv)
    TextView downloadMenuTv;
    @Bind(R.id.course_line)
    View courseLine;
    @Bind(R.id.course_menu_tv)
    TextView courseMenuTv;
    @Bind(R.id.download_line)
    View downloadLine;
    @Bind(R.id.cache_vp)
    ViewPager cacheVp;
    @Bind(R.id.tv_pointnum)
    TextView tvPointnum;
    @Bind(R.id.ll_point)
    RelativeLayout llPoint;
    @Bind(R.id.status_bar_fix)
    View status_bar_fix;
    

    private CacheCourseAdapter adapter;
    private View view_now_showLine;
    private List<CourseWare> list;
    private DownloadDB db;
    private List<Course> courses;
    private int postion;
    private boolean mHasLoadedOnce;
    private View mRootView;
    private int gao;
//    private GestureImageView status_bar_fix;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if(mHasLoadedOnce){
                cacheVp.setCurrentItem(0);
                EventBus.getDefault().post(new DeleteEvent());
            }
            if (isVisibleToUser && !mHasLoadedOnce) {
                // async http request here
                mHasLoadedOnce = true;
                initData();
            }
        }
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.cache_activity, container, false);
        }
        ButterKnife.bind(this, mRootView);
        ViewGroup parent = (ViewGroup) mRootView.getParent();
        if (parent != null) {
            parent.removeView(mRootView);
        }
        initView();
        initData();
        return mRootView;
    }

    @Override
    public void initView() {
        setTranslucentStatus();
//        getHeight();
        db = new DownloadDB(getActivity());
        topTitleLeft.setImageResource(R.drawable.nav_left);
        topTitleRight.setImageResource(R.drawable.my_question_title_add);
        topTitleRight.setVisibility(View.VISIBLE);
        topTitleRight.setImageResource(R.drawable.local_delete_light);
        topTitleLeft.setVisibility(View.INVISIBLE);
        topTitleLeft.setOnClickListener(this);
        topTitleRight.setOnClickListener(this);
        topTitleText.setText("我的下载");

        downloadMenuTv.setOnClickListener(this);
        courseMenuTv.setOnClickListener(this);

        view_now_showLine = courseLine;
        adapter = new CacheCourseAdapter(getChildFragmentManager());
        cacheVp.setAdapter(adapter);
        cacheVp.addOnPageChangeListener(this);

    }

    //计算当前的listview的高度，如果不满屏就补充，满屏后就不补充

    private void getHeight() {
        DisplayMetrics wm = getActivity().getResources().getDisplayMetrics();
        int screenHeight = wm.heightPixels;
        int top = DensityUtil.dip2px(getActivity(), 50);
        int top2 = DensityUtil.dip2px(getActivity(), 50);
        int top1 = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            //当状态栏透明后，内容布局会上移，这里使用一个和状态栏高度相同的view来修正内容区域
            top1 = Utils.getStatusHeight(getActivity());
        }
        int topTitle = top + top1 + top2;
        gao = screenHeight - topTitle;
    }

    @TargetApi(19)
    public void setTranslucentStatus() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            //当状态栏透明后，内容布局会上移，这里使用一个和状态栏高度相同的view来修正内容区域
            status_bar_fix.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Utils.getStatusHeight(getActivity())));
            status_bar_fix.setAlpha(1);
        }
    }

    /**
     * 错误监听
     */
    private View.OnClickListener mErrorClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        }
    };
    /**
     * 无数据监听
     */
    private View.OnClickListener emptyButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
        }
    };

    @Override
    public void initData() {
        list = db.findDownloadList(SharedPrefHelper.getInstance(getActivity()).getUserId());
        if (list != null && list.size() > 0) {
            llPoint.setVisibility(View.VISIBLE);
            tvPointnum.setText(list.size()+"");
        }else{
            llPoint.setVisibility(View.GONE);
        }

        courses = db.findCourses(SharedPrefHelper.getInstance(getActivity()).getUserId());
        if (postion == 0) {
            if (courses.size() > 0) {
                topTitleRight.setVisibility(View.VISIBLE);
            } else {
                topTitleRight.setVisibility(View.INVISIBLE);
            }
        } else {
            if (list.size() > 0) {
                topTitleRight.setVisibility(View.VISIBLE);
            } else {
                topTitleRight.setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        initData();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.download_menu_tv:
                setAnimation(downloadLine);
                courseMenuTv.setTextColor(Color.parseColor("#000000"));
                downloadMenuTv.setTextColor(getResources().getColor(R.color.color_primary));
                cacheVp.setCurrentItem(1);
                break;
            case R.id.course_menu_tv:
                setAnimation(courseLine);
                downloadMenuTv.setTextColor(Color.parseColor("#000000"));
                courseMenuTv.setTextColor(getResources().getColor(R.color.color_primary));
                cacheVp.setCurrentItem(0);
                break;
            case R.id.top_title_left:
                break;
            case R.id.top_title_right:
                //删除全部课程
                if (postion == 0) {
                    DialogManager.showNormalDialog(getActivity(), "确定要全部删除吗", "提示", "取消", "确定",
                            new DialogManager.CustomDialogCloseListener() {
                                @Override
                                public void yesClick() {
                                    db.deleteCourses(SharedPrefHelper.getInstance(getActivity()).getUserId(), courses);
                                    initData();
                                    EventBus.getDefault().post(new DeleteEvent());
                                }

                                @Override
                                public void noClick() {
                                }
                            });
                } else {
                    DialogManager.showNormalDialog(getActivity(), "确定要全部删除吗", "提示", "取消", "确定",
                            new DialogManager.CustomDialogCloseListener() {
                                @Override
                                public void yesClick() {
//                                db.deletes(SharedPrefHelper.getInstance(getActivity()).getUserId());
                                    db.deleteCourseWaresDownloading(SharedPrefHelper.getInstance(getActivity()).getUserId(), list);
                                    initData();
                                    EventBus.getDefault().post(new DeleteEvent());
                                }

                                @Override
                                public void noClick() {
                                }
                            });
                }
                break;
        }
    }

    private void setAnimation(View lineView) {
        if (view_now_showLine != lineView) {
            Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.play_menu_line_out);
            view_now_showLine.startAnimation(animation);
            view_now_showLine.setVisibility(View.INVISIBLE);
            view_now_showLine = lineView;
            view_now_showLine.setVisibility(View.VISIBLE);
            Animation animation1 = AnimationUtils.loadAnimation(getActivity(), R.anim.play_menu_line_in);
            view_now_showLine.startAnimation(animation1);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        adapter.notifyDataSetChanged();
        this.postion=position;
        if (position == 0) {
            setAnimation(courseLine);
            downloadMenuTv.setTextColor(Color.parseColor("#000000"));
            courseMenuTv.setTextColor(getResources().getColor(R.color.color_primary));
            if(courses!=null && courses.size()>0){
                topTitleRight.setVisibility(View.VISIBLE);
            }else{
                topTitleRight.setVisibility(View.INVISIBLE);
            }
        } else if (position == 1) {
            setAnimation(downloadLine);
            courseMenuTv.setTextColor(Color.parseColor("#000000"));
            downloadMenuTv.setTextColor(getResources().getColor(R.color.color_primary));
            if (list != null && list.size() > 0) {
                topTitleRight.setVisibility(View.VISIBLE);
            } else {
                topTitleRight.setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        AppContext.getInstance().setHandler(null);
    }
}
