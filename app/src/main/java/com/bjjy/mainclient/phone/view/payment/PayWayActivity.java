package com.bjjy.mainclient.phone.view.payment;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BasesActivity;
import com.bjjy.mainclient.phone.event.PaySuccessEvent;
import com.dongao.mainclient.core.payment.payutils.Constants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.drakeet.materialdialog.MaterialDialog;


public class PayWayActivity extends BasesActivity implements PayWayView {

    @Bind(R.id.top_title_text)
    TextView topTitleTv;

    @Bind(R.id.top_title_left)
    ImageView top_title_left;

    @Bind(R.id.pay_weixin_ll)
    LinearLayout pay_weixin_ll;

    @Bind(R.id.pay_weixin_iv)
    ImageView pay_weixin_iv;

    @Bind(R.id.pay_zhifubao_ll)
    LinearLayout pay_zhifubao_ll;

    @Bind(R.id.pay_zhifubao_iv)
    ImageView pay_zhifubao_iv;

    @Bind(R.id.pay_sure_tv)
    TextView pay_sure_tv;

    @Bind(R.id.pay_total_price_tv)
    TextView pay_total_price_tv;


    private PayWayPersenter payWayPresenter;
    private String payWay = Constants.wx_pay;
    private MaterialDialog mMaterialDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pay_way_activity);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        payWayPresenter = new PayWayPersenter();
        payWayPresenter.attachView(this);
        initView();
    }

    public void initView() {
        top_title_left.setVisibility(View.VISIBLE);
        topTitleTv.setText(getResources().getString(R.string.pay_order_info));
        payWayPresenter.initData();
//        payWayPresenter.getTotalPrice();
    }

    @OnClick(R.id.top_title_left)
    void onBack() {
        onBackPressed();
    }

    @OnClick(R.id.pay_sure_tv)
    void submit() {
        payWayPresenter.getData();
    }

    @OnClick(R.id.pay_weixin_ll)
    void chooseWX() {
        pay_weixin_iv.setBackgroundResource(R.drawable.btn_pay_choose);
        pay_zhifubao_iv.setBackgroundResource(R.drawable.btn_pay_uncheck);
        payWay = Constants.wx_pay;
    }

    @OnClick(R.id.pay_zhifubao_ll)
    void chooseZFB() {
        pay_weixin_iv.setBackgroundResource(R.drawable.btn_pay_uncheck);
        pay_zhifubao_iv.setBackgroundResource(R.drawable.btn_pay_choose);
        payWay = Constants.zfb_pay;
    }

    @Override
    public void showLoading() {
        //进度条
        //loginBtn.setProgress(50);
        showProgressDialog(getResources().getString(R.string.pay_loading));
        pay_sure_tv.setEnabled(false);
    }

    @Override
    public void hideLoading() {
        //loginBtn.setProgress(100);
        dismissProgressDialog();
        pay_sure_tv.setEnabled(true);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {
        Toast.makeText(PayWayActivity.this,message,Toast.LENGTH_SHORT).show();
        hideLoading();
    }

    @Override
    public Context context() {
        return PayWayActivity.this;
    }


    @Override
    public String payWay() {
        return payWay;
    }

    @Override
    public void intent() {
       /* Intent intent = new Intent(this, CourseActivity.class);
        startActivity(intent);
        this.finish();*/
    }

    @Override
    public void out() {
        onBackPressed();
    }

    @Override
    public String orderNumber() {
        Intent intent = this.getIntent();
        String orderNumber = intent.getStringExtra("orderNumber");
        if (orderNumber == null || orderNumber.isEmpty()) {
            orderNumber = "123456";
        }
        return orderNumber;
    }

    @Override
    public Intent getTheIntent() {
        return this.getIntent();
    }

    @Override
    public void totalPrice(String price) {
        pay_total_price_tv.setText("￥" + price);
    }

    @Override
    public void finishActivity() {
        onBackPressed();
    }

    @Subscribe
    public void onEventMainThread(PaySuccessEvent event) {
        
        this.finish();
    }

    /**
     * 显示进度条
     */
    public void showProgressDialog(String loadingtv){
        if(mMaterialDialog==null){
            mMaterialDialog = new MaterialDialog(this);
            mMaterialDialog.setCanceledOnTouchOutside(false);
            View view = LayoutInflater.from(this).inflate(R.layout.app_view_loading,null);
            TextView tv = (TextView) view.findViewById(R.id.app_loading_tv);
            ImageView imageView = (ImageView)view.findViewById(R.id.empty_layout_loading_img);
            AnimationDrawable animationDrawable = (AnimationDrawable)imageView.getBackground();
            animationDrawable.start();
            tv.setText(loadingtv);
            mMaterialDialog.setContentView(view);
        }
        mMaterialDialog.show();
    }

    public void dismissProgressDialog(){
        if(mMaterialDialog!=null)
            mMaterialDialog.dismiss();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        hideLoading();
    }
}
