package com.bjjy.mainclient.phone.view.main;

import android.content.Context;
import android.view.ViewGroup;

import com.bjjy.mainclient.phone.widget.recyclerview.adapter.BaseViewHolder;
import com.bjjy.mainclient.phone.widget.recyclerview.adapter.RecyclerArrayAdapter;


public class TimeLineAdapter extends RecyclerArrayAdapter<MsgValue> {

    public TimeLineAdapter(Context context) {
        super(context);
    }
    @Override
    public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
        return new TimeLineViewHolder(parent,viewType);
    }


   /* private List<MsgValue> mFeedList;
    private Context context;

    public TimeLineAdapter(Context context,List<MsgValue> feedList) {
        mFeedList = feedList;
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {
        return TimelineView.getTimeLineViewType(position,getItemCount());
    }

     @Override
    public TimeLineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(parent.getContext(), R.layout.home_frgment_item, null);
        return new TimeLineViewHolder(view, viewType);
    }


    @Override
    public void onBindViewHolder(TimeLineViewHolder holder, int position) {

        final MsgValue homeModel = mFeedList.get(position);

        switch (homeModel.getContentType()){
            case 1:
                MainTypeEnum.MAIN_TYPE_DAY.getName();
                holder.action.setVisibility(View.INVISIBLE);
                if(homeModel.getStatus() == 0){
                    holder.mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_lianxi));
                    holder.title.setText("每日一练");
                }else{
                    holder.mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_lianxi));
                    holder.title.setText("完成每日一练");
                }
                holder.subTitle.setVisibility(View.GONE);
                holder.subTitle2.setVisibility(View.GONE);
                break;
            case 2:
                MainTypeEnum.MAIN_TYPE_SELECT_COURSE.getName();
                holder.mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_tike_h));
                holder.action.setVisibility(View.INVISIBLE);
                holder.subTitle.setVisibility(View.VISIBLE);
                holder.subTitle2.setVisibility(View.GONE);
                break;
            case 3:
                MainTypeEnum.MAIN_TYPE_BOOK.getName();
                holder.mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_tike_h));
                holder.action.setVisibility(View.INVISIBLE);
                holder.subTitle.setVisibility(View.VISIBLE);
                holder.subTitle2.setVisibility(View.GONE);
                break;
            case 4:
                MainTypeEnum.MAIN_TYPE_WAIT.getName();
                holder.mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_tike_h));
                holder.action.setVisibility(View.INVISIBLE);
                holder.subTitle.setVisibility(View.VISIBLE);
                holder.subTitle2.setVisibility(View.GONE);
                break;
            case 5:
                MainTypeEnum.MAIN_TYPE_COUTSE.getName();
                if(homeModel.getStatus() == 0){
                    holder.mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_tike_n));
                    holder.action.setVisibility(View.VISIBLE);
                }else{
                    holder.mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_tike_h));
                    holder.action.setVisibility(View.INVISIBLE);
                }
                holder.subTitle.setVisibility(View.VISIBLE);
                holder.subTitle2.setVisibility(View.VISIBLE);
                break;
            case 6:
                MainTypeEnum.MAIN_TYPE_KEHOUZUOYE.getName();
                if(homeModel.getStatus() == 0){
                    holder.mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_exercises_n));
                    holder.action.setVisibility(View.INVISIBLE);
                }else{
                    holder.mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_exercises_h));
                    holder.action.setVisibility(View.INVISIBLE);
                }
                holder.subTitle.setVisibility(View.VISIBLE);
                holder.subTitle2.setVisibility(View.VISIBLE);
                break;
            case 7:
                MainTypeEnum.MAIN_TYPE_MONISHIJUAN.getName();
                if(homeModel.getStatus() == 0){
                    holder.mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_exercises_n));
                    holder.action.setVisibility(View.INVISIBLE);
                }else{
                    holder.mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_exercises_h));
                    holder.action.setVisibility(View.INVISIBLE);
                }
                holder.subTitle.setVisibility(View.VISIBLE);
                holder.subTitle2.setVisibility(View.VISIBLE);
                break;
            case 8:
                MainTypeEnum.MAIN_TYPE_LINIANZHENTI.getName();
                if(homeModel.getStatus() == 0){
                    holder.mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_exercises_n));
                    holder.action.setVisibility(View.INVISIBLE);
                }else{
                    holder.mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_exercises_h));
                    holder.action.setVisibility(View.INVISIBLE);
                }
                holder.subTitle.setVisibility(View.VISIBLE);
                holder.subTitle2.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }

        holder.home_fragment_item_course_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (homeModel.getContentType()) {
                    case 1:
                        MainTypeEnum.MAIN_TYPE_DAY.getName();
                        Intent intent = new Intent(context, SelectSubjectActivity.class);
                        context.startActivity(intent);
                        break;
                    case 2:
                        MainTypeEnum.MAIN_TYPE_SELECT_COURSE.getName();
                        Intent intent_select_course = new Intent(context, SelectSubjectActivity.class);
                        context.startActivity(intent_select_course);
                        break;
                    case 3:
                        MainTypeEnum.MAIN_TYPE_BOOK.getName();
                        Intent intent_book = new Intent(context, BookListActivity.class);
                        context.startActivity(intent_book);
                        break;
                    case 4:
                        MainTypeEnum.MAIN_TYPE_WAIT.getName();
                        context.startActivity(intent_wait);
                        break;
                    case 5:
                        MainTypeEnum.MAIN_TYPE_COUTSE.getName();
                        Intent intent_course = new Intent(context, PlayActivity.class);
                        context.startActivity(intent_course);
                        break;
                    case 6:
                        MainTypeEnum.MAIN_TYPE_KEHOUZUOYE.getName();
                        Intent intent_kehouzuoye = new Intent(context, ExamActivity.class);
                        context.startActivity(intent_kehouzuoye);
                        break;
                    case 7:
                        MainTypeEnum.MAIN_TYPE_MONISHIJUAN.getName();
                        Intent intent_monishijuan = new Intent(context, ExamActivity.class);
                        context.startActivity(intent_monishijuan);
                        break;
                    case 8:
                        MainTypeEnum.MAIN_TYPE_LINIANZHENTI.getName();
                        Intent intent_linianzhenti = new Intent(context, ExamActivity.class);
                        context.startActivity(intent_linianzhenti);
                        break;
                    default:
                        break;
                }
            }
        });

        holder.action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PlayActivity.class);
                context.startActivity(intent);

            }
        });

        holder.title.setText(homeModel.getTitle1());
        holder.subTitle.setText(homeModel.getTitle2());
        holder.subTitle2.setText(homeModel.getTitle3());
        holder.date.setText(homeModel.getTime());

    }

    @Override
    public int getItemCount() {
        return (mFeedList!=null? mFeedList.size():0);
    }
*/
}
