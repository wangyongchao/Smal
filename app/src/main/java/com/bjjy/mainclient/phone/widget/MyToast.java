package com.bjjy.mainclient.phone.widget;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.bjjy.mainclient.phone.R;

/**
 * Created by fengzongwei on 2016/11/22 0022.
 */
public class MyToast {

    private static View contentView;
    private static TextView textView;

    private static  Toast toast;

    private static void initView(Context context,String msg){
        if(contentView == null){
            contentView  = LayoutInflater.from(context).inflate(R.layout.wight_mytoast,null);
            textView = (TextView)contentView.findViewById(R.id.my_toast_tv);
        }
        textView.setText(msg);
    }

    public static void makeText(Context context,String msg,int time){
        initView(context,msg);
        if(toast == null){
            toast = new Toast(context);
            toast.setGravity(Gravity.CENTER, 0,0);
            toast.setDuration(time);
        }
        toast.setView(contentView);
        toast.show();
    }

}
