package com.bjjy.mainclient.phone.view.exam.activity;


import android.content.Intent;
import com.dongao.mainclient.model.mvp.MvpView;
import com.bjjy.mainclient.phone.app.AppContext;

/**
 * @author wyc
 * 答题报告UI 
 */
public interface ExamReportView extends MvpView {
  void intent();
  void out();
  Intent getWayIntent();
  void setRightNumber(int number);
  void setErrorNumber(int number);
  void setUseTime(String time);
  void setScore(String score);
  void setVisible(int examTag);
  AppContext getAppContext();
  void showPingJia(String str);
  
}
