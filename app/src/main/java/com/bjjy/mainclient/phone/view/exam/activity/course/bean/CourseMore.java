package com.bjjy.mainclient.phone.view.exam.activity.course.bean;

/**
 * Created by wyc on 2015/12/30.
 */
public class CourseMore {
    private String id;//课程ID
    private String name;// 课程名称
    private String progressSuggested;//课件进度提示
    private String daPersonName;//老师名字
    private String daPersonImg;//老师图片路径	
    private String updateTime;//更新时间

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProgressSuggested() {
        return progressSuggested;
    }

    public void setProgressSuggested(String progressSuggested) {
        this.progressSuggested = progressSuggested;
    }

    public String getDaPersonName() {
        return daPersonName;
    }

    public void setDaPersonName(String daPersonName) {
        this.daPersonName = daPersonName;
    }

    public String getDaPersonImg() {
        return daPersonImg;
    }

    public void setDaPersonImg(String daPersonImg) {
        this.daPersonImg = daPersonImg;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
}
