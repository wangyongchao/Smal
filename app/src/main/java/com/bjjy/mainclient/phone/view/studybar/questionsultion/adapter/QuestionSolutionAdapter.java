package com.bjjy.mainclient.phone.view.studybar.questionsultion.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bjjy.mainclient.phone.view.studybar.privateteacher.adapter.FootViewHolder;
import com.bjjy.mainclient.phone.view.studybar.questionsultion.bean.QuestionSolution;
import com.dongao.mainclient.core.util.DateUtil;
import com.bjjy.mainclient.phone.R;

import java.util.List;

public class QuestionSolutionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_ITEM = 0;
    private static final int TYPE_FOOTER = 1;
    private List<QuestionSolution> mFeedList;
    private Context context;
    private ReclerViewItemClick reclerViewItemClick;
    private boolean isShow;

    public QuestionSolutionAdapter(Context context, List<QuestionSolution> feedList) {
        mFeedList = feedList;
        this.context = context;
    }

    public void setNoDataShow(boolean isShow){
        this.isShow=isShow;
    }
    
    public void setOnItemClick(ReclerViewItemClick reclerViewItemClick){
        this.reclerViewItemClick=reclerViewItemClick;
    }

    @Override
    public int getItemViewType(int position) {
        if (position + 1 == getItemCount()) {
            return TYPE_FOOTER;
        } else {
            return TYPE_ITEM;
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       
        if (viewType == TYPE_ITEM) {
            View view = View.inflate(parent.getContext(), R.layout.studybar_questionsolution_activity_item, null);
            return new QuestionSolutionViewHolder(view);
        } else if (viewType == TYPE_FOOTER) {
            View view = LayoutInflater.from(context).inflate(R.layout.swiperefresh_item_foot, parent,
                    false);
            return new FootViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof QuestionSolutionViewHolder) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (reclerViewItemClick!=null){
                        reclerViewItemClick.onItemClickListenerPosition(position);
                    }
                }
            });
            QuestionSolution homeModel = mFeedList.get(position);

            ((QuestionSolutionViewHolder)holder).name_tv.setText(homeModel.getTitle());
            if(homeModel.getCreateTime()!=null&&!homeModel.getCreateTime().isEmpty()){
                ((QuestionSolutionViewHolder)holder).time_tv.setText(DateUtil.twoDateDistanceForStudyBarList(homeModel.getCreateTime()));
            }else{
                ((QuestionSolutionViewHolder)holder).time_tv.setText("");
            }
        }else if (holder instanceof FootViewHolder) {
            if (getItemCount()<7){
                isShow=true;
            }
            if (isShow){
                ((FootViewHolder)holder).no_data.setVisibility(View.VISIBLE);
            }else{
                ((FootViewHolder)holder).no_data.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return (mFeedList!=null? (mFeedList.size()+1):0);
    }
   public interface ReclerViewItemClick{
        void onItemClickListenerPosition(int position);
    }
}
