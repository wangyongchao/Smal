package com.bjjy.mainclient.phone.view.book;


import com.alibaba.fastjson.JSON;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ParamsUtils;
import com.dongao.mainclient.model.remote.bean.BaseBean;

import java.util.List;


/**
 * 随书赠卡Persenter
 */
public class BookListPersenter extends BasePersenter<BookListView> {
    private List<Book> mList;

    @Override
    public void attachView(BookListView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    @Override
    public void getData(){
        //一些业务逻辑的判断 有网没网 是请求网络还是请求数据库
        getMvpView().showLoading();
        apiModel.getData(ApiClient.getClient().bookActivateList(
                ParamsUtils.getInstance(getMvpView().context()).bookActivateList()));
    }

    /**
     *字段	描述
     1000	成功
     1009	失败
     1014	用户未激活随书赠卡
     */
    @Override
    public void setData(String obj) {
        try {
            BaseBean baseBean = JSON.parseObject(obj, BaseBean.class);
            if(baseBean == null){
                getMvpView().showError("");
                return;
            }
            if(baseBean.getCode()==1000 ){
                mList = JSON.parseArray(JSON.parseObject(baseBean.getBody()).getJSONArray("bookList").toString(),Book.class);
            }else if (baseBean.getCode() == 1014){ //用户未激活随书赠卡
                getMvpView().setAdapter(null);
            } else {
                getMvpView().showError("");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        getMvpView().setAdapter(mList);
    }
}
