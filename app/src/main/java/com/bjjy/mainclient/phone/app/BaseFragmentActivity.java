package com.bjjy.mainclient.phone.app;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.bjjy.mainclient.phone.view.studybar.Constant;
import com.bjjy.mainclient.phone.view.user.utils.UserViewManager;
import com.bjjy.mainclient.phone.widget.statusbar.SystemBarTintManager;
import com.dongao.mainclient.core.app.AppManager;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.bjjy.mainclient.phone.R;
import com.umeng.analytics.MobclickAgent;
import com.umeng.message.PushAgent;

public abstract class BaseFragmentActivity extends FragmentActivity implements View.OnClickListener {

    protected AppContext appContext;
    protected Dialog progressDialog;

    protected UserViewManager userViewManager;
    public PushAgent mpAgent;

    protected SystemBarTintManager tintManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
        userViewManager = new UserViewManager(this, new UserViewManager.LoginLinstener() {
            @Override
            public void loginSuccess() {
                loginSuccessed();
            }
        });
    }

    @TargetApi(19)
    public void setTranslucentStatus() {

        tintManager = new SystemBarTintManager(this);
        tintManager.setStatusBarTintEnabled(true);
        tintManager.setStatusBarTintResource(android.R.color.transparent);  //设置上方状态栏透明
        Window win = getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        final int bits = WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                winParams.flags |= bits;
        }else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    /**
     * 登陆成功后的操作
     */
    protected void loginSuccessed(){
        String userId= SharedPrefHelper.getInstance(this).getUserId()+"";
        if (userId.isEmpty()){
            userId="";
        }
        new AddAliasTask(userId, Constant.UMENG_BIECHENG_TYPE).execute();
//        mpAgent.setAlias(userId, Constant.UMENG_BIECHENG_TYPE);
    }

    /**
     * 显示登陆view
     */
    protected void showLoginView(){
        String userId=SharedPrefHelper.getInstance(this).getUserId()+"";
        try {
            if(!userId.isEmpty()){
                mpAgent.removeAlias(userId, Constant.UMENG_BIECHENG_TYPE);
            }
        }catch (Exception e){
        }
        userViewManager.showUserCenterWindow();
    }

    // 初始化
    private void init() {
        // 添加Activity到堆栈
        AppManager.getAppManager().addActivity(this);
        appContext = (AppContext) AppContext.getApp().getApplicationContext();
        //EventBus.getDefault().register(this);
        //初始化推送
        mpAgent= PushAgent.getInstance(this);
        mpAgent.onAppStart();
    }

    public abstract void initView();

    public abstract void initData();

    //
    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        // 结束Activity&从堆栈中移除
        AppManager.getAppManager().finishActivity(this);
       // EventBus.getDefault().unregister(this);
        super.onDestroy();

    }

    /**
     * 进度条对话框
     */
    public void showProgressDialog(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.loading_layout,
                null);
        if(progressDialog != null && progressDialog.isShowing()){
            progressDialog.dismiss();
            progressDialog = null;
        }
        progressDialog = new Dialog(context, R.style.customDialog);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setContentView(view);
        progressDialog.show();
    }

    /**
     * 取消进度条对话框
     */
    public void dismissProgressDialog(){
        if(progressDialog != null && progressDialog.isShowing()){
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    /**
     * 添加友盟推送的别称
     */
    class AddAliasTask extends AsyncTask<Void, Void, Boolean> {

        String alias;
        String aliasType;

        public AddAliasTask(String aliasString,String aliasTypeString) {
            // TODO Auto-generated constructor stub
            this.alias = aliasString;
            this.aliasType = aliasTypeString;
        }

        protected Boolean doInBackground(Void... params) {
            try {
                return mpAgent.addAlias(alias, aliasType);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
        }

    }

}
