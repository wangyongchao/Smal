package com.bjjy.mainclient.phone.view.zb.bean;

import com.yunqing.core.db.annotations.Id;
import com.yunqing.core.db.annotations.Table;

import java.io.Serializable;

/**
 * 答题记录
 * wyc
 */

@Table(name="t_live_play_data_log")
public class LivePlayDataBean implements Serializable {
    @Id
    private int dbId;

    private String id; 
    private String teachingPlanId;
    private String channelId;
    private String title;
    private String subheading;
    private String course;
    private String coverPicture;
    private String year;
    private String teacherName;
    private String isPay;
    private String aooointmentDate;
    private String appointmentTime;
    private String isValid;
    private String createdDate;
    private String updateDate;
    private String createname;
    private String compere;
    private String updateName;
    private String prompt;
    private String description;
    private int status;
    private String cost;
    private String userId;
    private String channelStartTimes;
    private String channelEndTimes;
    private String channelName;
    private String imageUrl;
    private PageBean pageParameter;

    public int getDbId() {
        return dbId;
    }

    public void setDbId(int dbId) {
        this.dbId = dbId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTeachingPlanId() {
        return teachingPlanId;
    }

    public void setTeachingPlanId(String teachingPlanId) {
        this.teachingPlanId = teachingPlanId;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubheading() {
        return subheading;
    }

    public void setSubheading(String subheading) {
        this.subheading = subheading;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getCoverPicture() {
        return coverPicture;
    }

    public void setCoverPicture(String coverPicture) {
        this.coverPicture = coverPicture;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getIsPay() {
        return isPay;
    }

    public void setIsPay(String isPay) {
        this.isPay = isPay;
    }

    public String getAooointmentDate() {
        return aooointmentDate;
    }

    public void setAooointmentDate(String aooointmentDate) {
        this.aooointmentDate = aooointmentDate;
    }

    public String getAppointmentTime() {
        return appointmentTime;
    }

    public void setAppointmentTime(String appointmentTime) {
        this.appointmentTime = appointmentTime;
    }

    public String getIsValid() {
        return isValid;
    }

    public void setIsValid(String isValid) {
        this.isValid = isValid;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getCreatename() {
        return createname;
    }

    public void setCreatename(String createname) {
        this.createname = createname;
    }

    public String getCompere() {
        return compere;
    }

    public void setCompere(String compere) {
        this.compere = compere;
    }

    public String getUpdateName() {
        return updateName;
    }

    public void setUpdateName(String updateName) {
        this.updateName = updateName;
    }

    public String getPrompt() {
        return prompt;
    }

    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getChannelStartTimes() {
        return channelStartTimes;
    }

    public void setChannelStartTimes(String channelStartTimes) {
        this.channelStartTimes = channelStartTimes;
    }

    public String getChannelEndTimes() {
        return channelEndTimes;
    }

    public void setChannelEndTimes(String channelEndTimes) {
        this.channelEndTimes = channelEndTimes;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public PageBean getPageParameter() {
        return pageParameter;
    }

    public void setPageParameter(PageBean pageParameter) {
        this.pageParameter = pageParameter;
    }
}
