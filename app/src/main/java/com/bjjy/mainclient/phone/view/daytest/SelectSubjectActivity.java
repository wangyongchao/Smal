package com.bjjy.mainclient.phone.view.daytest;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.dongao.mainclient.model.bean.daytest.DayExSelectedSubject;
import com.dongao.mainclient.model.bean.daytest.DayTestExam;
import com.bjjy.mainclient.persenter.DayTestSelectSujectPersenter;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseActivity;
import com.bjjy.mainclient.phone.view.daytest.adapter.DayTestSelectSubjectAdapter;
import com.bjjy.mainclient.phone.widget.EmptyViewLayout;
import com.bjjy.mainclient.phone.widget.MyGridView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by wyc on 2016/5/20 0020.
 */
public class SelectSubjectActivity extends BaseActivity implements SelectSubjectView{
    @Bind(R.id.scroll_view)
    ScrollView scrollView;
    @Bind(R.id.top_title_text)
    TextView textView_title;
    @Bind(R.id.top_title_right)
    TextView textView_commit;
    @Bind(R.id.day_test_selectsubject_body)
    LinearLayout linearLayout_body;//显示主体内容的container

    private EmptyViewLayout emptyViewLayout;

    private DayTestSelectSujectPersenter dayTestSelectSujectPersenter;

    private List<DayTestExam> selectData;//用户可选课程

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.day_test_selectsubject);
        ButterKnife.bind(this);
        dayTestSelectSujectPersenter = new DayTestSelectSujectPersenter();
        dayTestSelectSujectPersenter.attachView(this);
        initView();
        initData();
    }

    @Override
    public void initView() {
        emptyViewLayout = new EmptyViewLayout(this,scrollView);
        emptyViewLayout.setErrorButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initData();
            }
        });
        textView_title.setText("选择科目");
        textView_commit.setVisibility(View.VISIBLE);
        textView_commit.setText("完成");
        textView_commit.setOnClickListener(this);
    }

    @Override
    public void initData() {
        dayTestSelectSujectPersenter.getData();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.top_title_right:
                if(selectData==null)
                    return;
                dayTestSelectSujectPersenter.savaSelect();
                finish();
                break;
        }
    }

    @OnClick(R.id.top_title_left) void back(){
        finish();
    }

    @Override
    public void showData(List<DayTestExam> selectSubjectData) {
        this.selectData = selectSubjectData;
        addSubjectItem();
    }

    /**
     * 逐条显示需要展示的数据
     */
    private void addSubjectItem(){
        for(int i=0;i<selectData.size();i++){
            DayTestExam dayTestExam= selectData.get(i);
                View view = LayoutInflater.from(this).inflate(R.layout.day_test_selelctsubject_item,null);
                MyGridView gridView = (MyGridView)view.findViewById(R.id.day_test_selectsubject_item_gv);
                TextView textView_name = (TextView)view.findViewById(R.id.day_test_selectsubject_item_name);
                textView_name.setText(dayTestExam.getExamName());
                final List<DayExSelectedSubject> subjectList = dayTestExam.getSubjectList();
                if(subjectList.size()%2!=0){
                    subjectList.add(new DayExSelectedSubject());
                }
                final DayTestSelectSubjectAdapter dayTestSelectSubjectAdapter = new DayTestSelectSubjectAdapter(gridView,this,subjectList);
                gridView.setAdapter(dayTestSelectSubjectAdapter);
                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if(subjectList.get(position).getSubjectId()==null || subjectList.get(position).getSubjectId().equals(""))
                            return;
                        if(subjectList.get(position).getSelectedTag() == 0)
                            subjectList.get(position).setSelectedTag(1);
                        else
                            subjectList.get(position).setSelectedTag(0);
                        dayTestSelectSubjectAdapter.notifyDataSetChanged();
                    }
                });
                linearLayout_body.addView(view);
        }
    }

    @Override
    public void showLoading() {
        emptyViewLayout.showLoading();
    }

    @Override
    public void hideLoading() {
        emptyViewLayout.showContentView();
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {
        emptyViewLayout.showError();
    }

    @Override
    public Context context() {
        return this;
    }
}
