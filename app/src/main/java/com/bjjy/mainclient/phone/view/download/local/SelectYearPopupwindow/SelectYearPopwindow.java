package com.bjjy.mainclient.phone.view.download.local.SelectYearPopupwindow;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;

import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.view.download.local.bean.YearInfo;
import com.bjjy.mainclient.phone.view.exam.activity.myexam.adapter.PopUpWindowAdapter;
import com.dongao.mainclient.core.util.DensityUtil;

import java.util.List;

/**
 * Created by fengzongwei on 2015/12/30.
 * 用于生成选择年份的popupwindow
 */
public class SelectYearPopwindow {

    private RecyclerView rv_queryadd_pop;
    private PopupWindow popupWindow;
    private ImageView iv_arroww;
    private int xoff;
    private View view_clickBody;
    private Context context;
    private PopUpWindowAdapter.MainTypeItemClick mainTypeItemClick;
    private PopUpWindowAdapter questionSelectBookAdapter;

    /**
     *
     * @param context  上下文对象
     * @param view_clickBody 点击能弹出popupwindow的控件
     * @param iv_arrow  图片（显示上下箭头）
     * @param mainTypeItemClick popupwindow中的listview点击Item时的监听
     */
    public SelectYearPopwindow(Context context, View view_clickBody,
							   final ImageView iv_arrow,PopUpWindowAdapter.MainTypeItemClick mainTypeItemClick){
        View contentView = LayoutInflater.from(context).inflate(R.layout.queryadd_select_examination_pop,null);
        rv_queryadd_pop = (RecyclerView) contentView.findViewById(R.id.rv_queryadd_pop);
        rv_queryadd_pop.setNestedScrollingEnabled(false);
        rv_queryadd_pop.setLayoutManager(new LinearLayoutManager(rv_queryadd_pop.getContext(),LinearLayoutManager.VERTICAL, false));
        iv_arroww = iv_arrow;
        this.view_clickBody = view_clickBody;
        this.context = context;
        this.mainTypeItemClick = mainTypeItemClick;
        popupWindow = new PopupWindow(contentView, ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        // 需要设置一下此参数，点击外边可消失
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        // 设置点击窗口外边窗口消失
        popupWindow.setOutsideTouchable(true);
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {

            @Override
            public void onDismiss() {
                if(iv_arroww!=null)
                    iv_arroww.setImageResource(R.drawable.mylesson_top_arrow_down_normal);
            }
        });
        // 设置此参数获得焦点，否则无法点击
        popupWindow.setFocusable(true);
        xoff = (int) ((DensityUtil.getWidthInPx(context) - DensityUtil
                .dip2px(context, 186)));
    }

    /**
     * 当前是否正在显示popupwindow
     * @return
     */
    public boolean isShowPopNow(){
        return popupWindow!=null && popupWindow.isShowing();
    }

    /**
     *
     * @param yearInfoList  年份集合
     * @param currentYearInfo  当前页面显示年份
     */
    public void showPop(List<YearInfo> yearInfoList, YearInfo currentYearInfo){
        questionSelectBookAdapter = new PopUpWindowAdapter(context, yearInfoList);
        rv_queryadd_pop.setAdapter(questionSelectBookAdapter);
        int position= 0;
        for(int i=0;i<yearInfoList.size();i++){
            if(yearInfoList.get(i).getYearName().equals(currentYearInfo.getYearName()))
                position = i;
        }
        if (currentYearInfo != null) {
            questionSelectBookAdapter.checkedPosition(position+"");
        }
        questionSelectBookAdapter.setRvItemClick(mainTypeItemClick);
        rv_queryadd_pop.setAdapter(questionSelectBookAdapter);
//        lv_years.setOnItemClickListener(listener);
        popupWindow.showAsDropDown(view_clickBody, 0, 0);
        if(iv_arroww!=null)
            iv_arroww.setImageResource(R.drawable.mylesson_top_arrow_up_normal);
    }

    /**
     * 隐藏popupwindow
     */
    public void dissmissPop(){
        if (popupWindow!=null && popupWindow.isShowing()) {
            popupWindow.dismiss();
            if(iv_arroww!=null)
                iv_arroww.setImageResource(R.drawable.mylesson_top_arrow_down_normal);
        }
    }

}
