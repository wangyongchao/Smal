package com.bjjy.mainclient.phone.view.classroom.course;


import com.alibaba.fastjson.JSON;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.bean.course.CourseJson;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ParamsUtils;
import com.dongao.mainclient.model.remote.bean.BaseBean;


/**
 * 课堂首页Persenter
 */
public class CourseFragmentPersenter extends BasePersenter<CourseFragmentView> {
    private CourseJson courseJson ;

    @Override
    public void attachView(CourseFragmentView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    @Override
    public void getData(){
        //一些业务逻辑的判断 有网没网 是请求网络还是请求数据库
        getMvpView().showLoading();
        apiModel.getData(ApiClient.getClient().courseList(
                ParamsUtils.getInstance(getMvpView().context()).courseHome()));
    }

    @Override
    public void setData(String obj) {
        try {
            BaseBean baseBean = JSON.parseObject(obj,BaseBean.class);
            if(baseBean == null){
                getMvpView().showError("");
                return;
            }

            if(baseBean.getResult().getCode()==1){
                courseJson = JSON.parseObject(baseBean.getBody(),CourseJson.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        getMvpView().setView(courseJson);
    }
}
