package com.bjjy.mainclient.phone.view.exam.event;

/**
 * Created by wyc on 3/5/16.
 */
public class ShowComprehensiveAnalyzeEvent {
    public boolean flag;
    private static final String TAG = "ShowComprehensiveAnalyzeEvent";
    public ShowComprehensiveAnalyzeEvent(boolean flag)
    {
        this.flag=flag;
    }
}
