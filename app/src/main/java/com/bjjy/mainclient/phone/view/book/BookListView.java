package com.bjjy.mainclient.phone.view.book;


import com.dongao.mainclient.model.mvp.MvpView;

import java.util.List;

/**
 * UI 定义UI 看此UI中有何事件
 */
public interface BookListView extends MvpView {
  void setAdapter(List<Book> books);
}
