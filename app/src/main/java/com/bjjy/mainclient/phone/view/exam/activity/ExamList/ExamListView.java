package com.bjjy.mainclient.phone.view.exam.activity.ExamList;


import com.bjjy.mainclient.phone.widget.xlistview.XListView;
import com.dongao.mainclient.model.mvp.MvpView;
import com.bjjy.mainclient.phone.widget.EmptyViewLayout;

/**
 * Created by dell on 2016/5/11.
 */
public interface ExamListView extends MvpView {
    EmptyViewLayout getEmptyLayout();
    void initAdapter();
    void refreshAdapter();
    void loadOver();
    XListView getListView();
    void setFootWords(String str);
}
