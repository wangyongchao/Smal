package com.bjjy.mainclient.phone.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by fengzongwei on 2015/8/21.
 */
public class PushPicToPhoto {

    /**
     * 将图片添加到手机相册，并通知相册更新
     * @param context
     * @param bitmap  所要存放的图片
     */
    public static void pushPic(Context context,Bitmap bitmap){
        File appDir = new File(Environment.getExternalStorageDirectory(), "dongaoPic");
        if (!appDir.exists()) {
            appDir.mkdir();
        }
        String fileName = "dongao_erweima.jpg";
        File file = new File(appDir, fileName);
        if(!file.exists()){
            try {
                FileOutputStream fos = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                fos.flush();
                fos.close();
                // 其次把文件插入到系统图库
                MediaStore.Images.Media.insertImage(context.getContentResolver(),
                        file.getAbsolutePath(), fileName, null);
                // 最后通知图库更新
                context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + file.getAbsolutePath())));
                Toast.makeText(context, "成功保存到手机相册", Toast.LENGTH_SHORT).show();
                if(!bitmap.isRecycled()){
                    bitmap.recycle();
                    System.gc();
                }
            }catch (Exception e) {
                Toast.makeText(context, "保存到相册失败", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }else
            Toast.makeText(context,"已保存到手机相册", Toast.LENGTH_SHORT).show();
    }

}
