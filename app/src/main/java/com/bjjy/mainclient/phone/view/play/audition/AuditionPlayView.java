package com.bjjy.mainclient.phone.view.play.audition;

import android.content.Intent;

import com.dongao.mainclient.model.bean.play.CourseDetail;
import com.dongao.mainclient.model.bean.play.CourseWare;
import com.dongao.mainclient.model.mvp.MvpView;

import java.util.List;

/**
 * Created by fengzongwei on 2016/5/30 0030.
 */
public interface AuditionPlayView extends MvpView{
    void playError(String msg);
    CourseWare getCw();
    void startPlay(CourseWare cw);
}
