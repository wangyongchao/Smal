package com.bjjy.mainclient.phone.view.classroom.subject;


import com.alibaba.fastjson.JSON;
import com.dongao.mainclient.model.bean.course.Subject;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.local.SubjectDB;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.bean.course.Exam;
import com.dongao.mainclient.model.bean.course.ExamJson;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ParamsUtils;
import com.dongao.mainclient.model.remote.bean.BaseBean;

import java.util.List;

/**
 * 登录UI
 */
public class SubjectFragmentPersenter extends BasePersenter<SubjectFragmentView> {
    private List<Exam> mList ;
    private String userId;
    private SubjectDB subjectDB;

    @Override
    public void attachView(SubjectFragmentView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    @Override
    public void getData(){
        //一些业务逻辑的判断 有网没网 是请求网络还是请求数据库
        //net
        getMvpView().showLoading();
        apiModel.getData(ApiClient.getClient().subjectList(ParamsUtils.subjectList()));
        userId = SharedPrefHelper.getInstance(getMvpView().context()).getUserId();
        subjectDB = new SubjectDB(getMvpView().context());
    }

    @Override
    public void setData(String obj) {
        try {
            BaseBean baseBean = JSON.parseObject(obj,BaseBean.class);
            if(baseBean == null){
                getMvpView().showError("");
                return;
            }
            if(baseBean.getCode()==1000 || baseBean.getCode()==1) {
                ExamJson examJson = JSON.parseObject(baseBean.getBody(), ExamJson.class);
                mList = examJson.getMobileExamList();
                getMvpView().setAdapter(mList);

                for(Exam exam : mList){
                    for(Subject subject : exam.getSubjectInfoList()){
                        subject.setUserId(userId);
                        subjectDB.insert(subject);
                    }
                }
                /*List<Subject> subjects = subjectDB.findAll(userId);
                for(Subject subject : subjects){
                    LogUtils.d("subjectName="+subject.getSubjectName());
                }*/
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
