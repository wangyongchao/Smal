package com.bjjy.mainclient.phone.view.download;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.bjjy.mainclient.phone.widget.progress.NumberProgressBar;
import com.bjjy.mainclient.phone.R;
import com.dongao.mainclient.model.bean.course.CourseWare;
import com.dongao.mainclient.model.local.CourseDB;

import java.util.List;

/**
 * 我的课程年份的adapter
 * 
 * @author wyc
 * 
 */
public class DownloadAdapter extends BaseAdapter {
	private Context context;
	private LayoutInflater mInflater;
	private List<DownloadTask> dts;
	private DownloadTask curDownloadTask;//正在下载的位置
    private CourseDB courseDB;

	public DownloadAdapter(Context context) {
		this.context = context;
		mInflater = LayoutInflater.from(context);
        courseDB = new CourseDB(context);
	}

	public void setCurDownloadTask(DownloadTask curDownloadTask) {
		this.curDownloadTask = curDownloadTask;
	}

	public void setList(List<DownloadTask> dts) {
		this.dts = dts;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return dts == null ? 0 : dts.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder = null;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.download_item, null);
			viewHolder = new ViewHolder();
            viewHolder.checkBox = (CheckBox) convertView.findViewById(R.id.checkbox);
            viewHolder.title = (TextView) convertView.findViewById(R.id.title);
			viewHolder.speed = (TextView) convertView.findViewById(R.id.tv_speed);
			viewHolder.status = (TextView) convertView.findViewById(R.id.tv_status);
            viewHolder.numberProgressBar = (NumberProgressBar) convertView.findViewById(R.id.numberbar);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

        DownloadTask dt = dts.get(position);
		if (dt != null) {
            CourseWare cw = courseDB.findById(dt.getCwId());
            viewHolder.title.setText(cw.getChapterNo()+"  "+cw.getName());
            viewHolder.status.setText(DownloadTask.getStatusInfo(dt));
            viewHolder.numberProgressBar.setProgress(dt.getPercent() == 0 ? 0 : (dt.getPercent()));
            viewHolder.speed.setText(dt.getDownloadSpeed() == null ? "0kb/s" : dt.getDownloadSpeed());
            viewHolder.speed.setVisibility(View.INVISIBLE);
		}

		return convertView;
	}

	class ViewHolder {
        CheckBox checkBox;
        TextView title;
		TextView speed;//
		TextView status;// 已学完/播放中
        NumberProgressBar numberProgressBar;
	}
}
