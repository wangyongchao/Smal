package com.bjjy.mainclient.phone.view.download.local.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.view.exam.activity.exampaperlist.bean.Course;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;

/**
 * Created by dell on 2016/1/4.
 */
public class LocalCourseAdapter extends BaseQuickAdapter<Course, BaseViewHolder> {
    private CourseItemClickListener courseItemClickListener;
    private boolean isShowCheck;
    private Context context;

    public LocalCourseAdapter() {
        super(R.layout.localcourse_list_item);
    }

    public LocalCourseAdapter(Context context, ArrayList<Course> models, boolean isShowCheck) {
        super(R.layout.localcourse_list_item,models);
        this.isShowCheck=isShowCheck;
        this.context=context;
    }
    public void setCourseClickListener(CourseItemClickListener courseClickListener){
        this.courseItemClickListener=courseItemClickListener;
    }

    /**
     * 设置当前是否显示选择框
     */
    public void setShowCheckOrNot(boolean isShowCheck){
        this.isShowCheck=isShowCheck;
        notifyDataSetChanged();
    }

    private void updateCheckImage(int position, ImageView check_iv) {

    }

    @Override
    protected void convert(final BaseViewHolder helper, Course item) {
        ImageLoader.getInstance().loadImage(item.getCwName(), null, null, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String s, View view) {
                helper.setImageDrawable(R.id.picture_iv,context.getResources().getDrawable(R.drawable.ic_picture_loadfailed));
            }

            @Override
            public void onLoadingFailed(String s, View view, FailReason failReason) {
                helper.setImageDrawable(R.id.picture_iv,context.getResources().getDrawable(R.drawable.ic_picture_loadfailed));
            }

            @Override
            public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                helper.setImageBitmap(R.id.picture_iv,bitmap);

            }

            @Override
            public void onLoadingCancelled(String s, View view) {

            }
        });
        if (item.getCwName()==null){
            item.setCwName("");
        }
        if ( item.getCourseImg()==null){
            item.setCourseImg("");
        }
        helper.setText(R.id.course_name_tv,item.getCwName());

        if (item.getCourseCount()==(item.getDownloadCount())) {
            helper.setText(R.id.course_state_tv,"缓存完成");
        }else{
            helper.setText(R.id.course_state_tv,"已缓存" + item.getDownloadCount() + "节");
        }
        helper.setText(R.id.total_course_tv,"共" + item.getCourseCount() + "节");
        if (item.getCourseImg() ==null){
            item.setCourseImg("");
        }
        ImageLoader.getInstance().displayImage(item.getCourseImg(), (ImageView) helper.getView(R.id.picture_iv));

        if (item.getDownloadCount()==1){
            helper.setBackgroundRes(R.id.play_iv,R.drawable.button_top_right);
        }else{
            helper.setBackgroundRes(R.id.play_iv,R.drawable.button_top_right);
        }
        if (isShowCheck){
            helper.setVisible(R.id.check_iv,true);
            helper.setVisible(R.id.play_iv,false);
        }else {
            helper.setVisible(R.id.play_iv,true);
            helper.setVisible(R.id.check_iv,false);
        }
        if (item.isCheck()) {
            helper.setBackgroundRes(R.id.check_iv,R.drawable.localcourse_delete_btn_choose);
        } else {
            helper.setBackgroundRes(R.id.check_iv,R.drawable.myclass_play_select);

        }
        /* holder.play_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (courseItemClickListener!=null){
                    courseItemClickListener.courseClick(position);
                }
            }
        });*/
    }

    public  interface CourseItemClickListener{
       public void  courseClick(int position, long l);
    }

    static class ViewHolder{
        TextView course_name_tv,total_course_tv,course_state_tv,download_course_size_tv,all_course_size_tv;
        ImageView check_iv,picture_iv,play_iv;
    }

}
