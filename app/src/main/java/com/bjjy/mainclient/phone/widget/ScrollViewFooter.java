package com.bjjy.mainclient.phone.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bjjy.mainclient.phone.R;

/**
 * Created by dell on 2016/6/14 0014.
 */
public class ScrollViewFooter extends RelativeLayout {

    public final static int STATE_NORMAL = 0;//普通情况
    public final static int STATE_LOADING = 1;//正在刷新
    public final static int STATE_NOMORE = 2;//无更多数据

    private TextView textView;
    private ProgressBar progressBar;

    private int currentState = -1;

    private RefreshScrollView.OnRefreshScrollViewListener onRefreshScrollViewListener;

    public ScrollViewFooter(Context context) {
        super(context);
        if(!isInEditMode())
            initView(context);
    }

    public ScrollViewFooter(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
        if(!isInEditMode())
            initView(context);
    }

    public ScrollViewFooter(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        // TODO Auto-generated constructor stub
        if(!isInEditMode())
            initView(context);
    }

    private void initView(Context context){
        View view = LayoutInflater.from(context).inflate(R.layout.scrollview_footer,this, true);
        textView = (TextView)view.findViewById(R.id.scrollview_footer_tv);
        textView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentState == STATE_LOADING || currentState == STATE_NOMORE) {
                    return;
                } else {
                    setState(STATE_LOADING);
                    if(onRefreshScrollViewListener!=null)
                        onRefreshScrollViewListener.onLoadMore();
                }
            }
        });
        progressBar = (ProgressBar)view.findViewById(R.id.scrollview_footer_pb);
    }

    public void setOnRefreshScrollViewListener(RefreshScrollView.OnRefreshScrollViewListener onRefreshScrollViewListener){
        this.onRefreshScrollViewListener = onRefreshScrollViewListener;
    }

    public void setState(int currentState){
        if(this.currentState == currentState) {
            return ;
        }
        switch (currentState) {
            case STATE_NORMAL:
                textView.setVisibility(VISIBLE);
                progressBar.setVisibility(GONE);
                textView.setText("点击加载更多");
                break;
            case STATE_LOADING:
                textView.setVisibility(GONE);
                progressBar.setVisibility(VISIBLE);
                break;
            case STATE_NOMORE:
                textView.setVisibility(VISIBLE);
                progressBar.setVisibility(GONE);
                textView.setText("无更多数据");
                break;
        }
        this.currentState = currentState;
    }



}
