package com.bjjy.mainclient.phone.view.play.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.alibaba.fastjson.JSON;
import com.bjjy.mainclient.phone.view.play.PlayActivity;
import com.dongao.mainclient.core.util.FileUtil;
import com.dongao.mainclient.core.util.StringUtil;
import com.dongao.mainclient.model.bean.play.CourseWare;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.remote.bean.BaseBean;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.event.FreshHandOut;
import com.bjjy.mainclient.phone.utils.NetworkUtil;

import org.greenrobot.eventbus.EventBus;

import java.io.File;

/**
 * 我的课程中的 课程讲义
 */
public class CourseHandOutFragment extends Fragment {
    protected WebView webView;
    protected ProgressBar pb_progress;
    private static final String mimeType = "text/html";
    private static final String encoding = "utf-8";
    private String urlString;
    private PlayActivity playActivity;
    private WebJSObject webJSObject = new WebJSObject();
    private CourseWare courseWare;
    private boolean isCanChangeVideoPosition;//当前是否可以通过拖动讲义改变播放进度

    public CourseHandOutFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        playActivity = (PlayActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.play_course_info_fragment, container, false);
        webView = (WebView) view.findViewById(R.id.couseinfo_webView);
        pb_progress = (ProgressBar) view.findViewById(R.id.couseinfo_pb_progress);
        EventBus.getDefault().register(this);
        initWebView();
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    void initWebView() {
        webView.setWebViewClient(new WebViewClient());//不打开新的应用

        //进度条
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                // TODO Auto-generated method stub
                super.onProgressChanged(view, newProgress);
                if (newProgress == 100) {
                    pb_progress.setVisibility(View.GONE);
                }
            }
        });
        webView.getSettings().setDefaultTextEncodingName("UTF-8");
        // 开启JavaScript支持
        webView.getSettings().setJavaScriptEnabled(true);
        webView.addJavascriptInterface(webJSObject, "myObject");
    }

    public void setCourseHandOutUrl(String courseInfoUrl, boolean isCanChangeProgress) {
        if(pb_progress!=null){
            pb_progress.setVisibility(View.VISIBLE);
        }
        urlString = courseInfoUrl;
        isCanChangeVideoPosition = isCanChangeProgress;
        courseWare = playActivity.getPlayingCw();
        loadUrl();
    }

    @org.greenrobot.eventbus.Subscribe
    public void onEventAsync(FreshHandOut event) {
        playActivity.setCanLoadHandOut(true);
        loadUrl();
    }

    private void loadUrl() {
        if(!playActivity.isCanLoadHandOut()){
            return;
        }
        if(courseWare == null)
            return;
        if (webView != null) {
            if (StringUtil.isEmpty(urlString)) {
                File lectrueFile = new File(FileUtil.getDownloadPath(getActivity()) + SharedPrefHelper.getInstance(getActivity()).getUserId() + "_" + courseWare.getExamId() + "_" + courseWare.getSubjectId() + "_" + courseWare.getClassId() + "_" + courseWare.getSectionId() + "_" + courseWare.getCwId() + "/lecture/lecture.htm");
                if (lectrueFile.exists()) {
                    urlString = "http://localhost:" + Constants.SERVER_PORT + "/" + SharedPrefHelper.getInstance(getActivity()).getUserId() + "_" + courseWare.getExamId() + "_" + courseWare.getSubjectId() + "_" + courseWare.getClassId() + "_" + courseWare.getSectionId() + "_" + courseWare.getCwId() + "/lecture/lecture.htm";
                    webView.loadUrl(urlString);
                } else {
                    if (NetworkUtil.isNetworkAvailable(getActivity())) {
                        webView.loadDataWithBaseURL("", "暂无课程简介", mimeType, encoding, "");
                    } else {
                        webView.loadDataWithBaseURL("", "网络不可用 请检查网络连接", mimeType, encoding, "");
                    }
                }
            } else if (urlString.contains("http")) {
                //判断有没有网络
                if (NetworkUtil.isNetworkAvailable(getActivity())) {
                    webView.loadUrl(urlString);
                } else {
                    File lectrueFile = new File(FileUtil.getDownloadPath(getActivity()) + SharedPrefHelper.getInstance(getActivity()).getUserId() + "_" + courseWare.getExamId() + "_" + courseWare.getSubjectId() + "_" + courseWare.getClassId() + "_" + courseWare.getSectionId() + "_" + courseWare.getCwId() + "/lecture/lecture.htm");
                    if (lectrueFile.exists()) {
                        urlString = "http://localhost:" + Constants.SERVER_PORT + "/" + SharedPrefHelper.getInstance(getActivity()).getUserId() + "_" + courseWare.getExamId() + "_" + courseWare.getSubjectId() + "_" + courseWare.getClassId() + "_" + courseWare.getSectionId() + "_" + courseWare.getCwId() + "/lecture/lecture.htm";
                        webView.loadUrl(urlString);
                    } else {
                        webView.loadDataWithBaseURL("", "网络不可用 请检查网络连接", mimeType, encoding, "");
                    }
                }
            } else {
                try {
                    BaseBean baseBean = JSON.parseObject(urlString, BaseBean.class);
                    if (baseBean.getResult().getCode() == 9) {
//						playActivity.pauseVideo();
//						playActivity.showLoginOtherPlace();
                    } else {
                        webView.loadDataWithBaseURL("", "讲义加载失败", mimeType, encoding, "");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    webView.loadDataWithBaseURL("", "讲义加载失败", mimeType, encoding, "");
                }
            }
        }
    }


    public class WebJSObject {

        public void jumpLecture(long time) {
            if(webView!=null){
                webView.loadUrl("javascript:jumpLecture("
                        + time + ")");
            }
        }

        @JavascriptInterface
        public void jumpTime(final int time) {
            if (isCanChangeVideoPosition) {
                playActivity.webSeekTo(time * 1000);
            }
        }
    }

    /**
     * 设置当前讲义的进度，与播放器同步
     *
     * @param position 当前播放器播放到的时间
     */
    public void setWebViewProgress(long position) {
        webJSObject.jumpLecture(position);
    }

}
