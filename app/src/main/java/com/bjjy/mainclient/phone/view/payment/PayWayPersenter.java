package com.bjjy.mainclient.phone.view.payment;


import android.util.Xml;
import android.widget.Toast;

import com.bjjy.mainclient.phone.view.payment.bean.PayBean;
import com.bjjy.mainclient.phone.view.payment.utis.SignUtils;
import com.dongao.mainclient.core.payment.payutils.Constants;
import com.dongao.mainclient.core.payment.payutils.WXpay;
import com.dongao.mainclient.core.payment.payutils.ZFBPay;
import com.dongao.mainclient.core.util.StringUtil;
import com.dongao.mainclient.model.bean.myorder.OrderItem;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.remote.ApiClient;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.utils.NetworkUtil;
import com.bjjy.mainclient.phone.view.payment.utis.XmlUtils;
import com.tencent.mm.sdk.constants.Build;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;

import java.io.StringReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 登录UI
 */
public class PayWayPersenter extends BasePersenter<PayWayView> {

    private OrderItem orderItem;
    private String price;

    @Override
    public void attachView(PayWayView payWayView) {
        super.attachView(payWayView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }
    
//    public void getTotalPrice(){
//       String totalPrice=  getMvpView().getTheIntent().getStringExtra("totalPrice");
//        getMvpView().totalPrice(totalPrice);
//    }
    public void initData(){
        String fromType = getMvpView().getTheIntent().getStringExtra("fromType");
        SharedPrefHelper.getInstance(getMvpView().context()).setPayFromWay(fromType);
        price = getMvpView().getTheIntent().getStringExtra("price");
        String orderId = getMvpView().getTheIntent().getStringExtra("orderId");
        orderItem = new OrderItem();
        orderItem.setFromType(fromType);
        orderItem.setPrice(price);
        orderItem.setOrderId(orderId);
        SharedPrefHelper.getInstance(getMvpView().context()).setOrderId(orderId);
        /*String orderInfo=getMvpView().getTheIntent().getStringExtra("orderjson");
        if (StringUtil.isEmpty(orderInfo)){
            getMvpView().finishActivity();
            return;
        }
        orderItem= JSON.parseObject(orderInfo,OrderItem.class);*/
        getMvpView().totalPrice(orderItem.getPrice());
        
    }
    @Override
    public void getData() {
        // 判断空
        if (StringUtil.isEmpty(getMvpView().payWay())) {
            getMvpView().showError("请选择支付方式");
            return;
        }
       
        getMvpView().showLoading();
        String orderId=orderItem.getOrderId();
        HashMap<String, String> params = new HashMap();
        params.put("orderId", orderId);
        params.put("userId", SharedPrefHelper.getInstance(getMvpView().context()).getUserId() + "");
        if (!NetworkUtil.isNetworkAvailable(getMvpView().context())){
            getMvpView().showError(getMvpView().context().getResources().getString(R.string.check_net));
            getMvpView().hideLoading();
            return;
        }
        if (getMvpView().payWay().equals(Constants.wx_pay)) {
            IWXAPI api = WXAPIFactory.createWXAPI(getMvpView().context(), Constants.APP_ID, false);
            boolean isPaySupported = api.getWXAppSupportAPI() >= Build.PAY_SUPPORTED_SDK_INT;
            if (!isPaySupported){
//                getMvpView().showError(getMvpView().context().getResources().getString(R.string.pay_install_wx));
                Toast.makeText(getMvpView().context(),getMvpView().context().getResources().getString(R.string.pay_install_wx),Toast.LENGTH_SHORT).show();
                getMvpView().hideLoading();
                return;
            }
            HashMap<String,String> map=new HashMap();
            map.put("p_id","44");
            map.put("m_id","34");
            map.put("order_no",orderId);
            map.put("goods_info","东奥手机支付");
            map.put("total_fee",price);
//            map.put("total_fee","0.01");
            String sign= SignUtils.createWxSign(map);
            map.put("sign", sign);
            apiModel.getData(ApiClient.getClient().getCommodityOrderInfoByWX(map));
        } else if (getMvpView().payWay().equals(Constants.zfb_pay)) {
            HashMap<String,String> map=new HashMap();
            map.put("p_id","24");
            map.put("m_id","34");
            map.put("order_no",orderId);
            map.put("goods_info","东奥手机支付");
            map.put("total_fee",price);
//            map.put("total_fee","0.01");
            String sign= SignUtils.createWxSign(map);
            map.put("sign", sign);
            payByZFB(map);
        }
    }

    public static String getRandomString(int length) { //length表示生成字符串的长度  
        String base = "abcdefghijklmnopqrstuvwxyz0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        return sb.toString();
    }

    private void payByZFB(HashMap<String, String> params ) {
        Call<String> call = ApiClient.getClient().getCommodityOrderInfoByWX(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    try {
                        getMvpView().hideLoading();
                        String str = response.body();
                        List list1 = XmlUtils.parseXml(str, PayBean.class);
                        if (list1==null||list1.size()==0){
                            getMvpView().showError(getMvpView().context().getResources().getString(R.string.pay_result_error));
                            return;
                        }
                        PayBean payBean=(PayBean)list1.get(0);
                        if (!payBean.getResult_code().equals("1")){
                            getMvpView().showError(getMvpView().context().getResources().getString(R.string.pay_result_error));
                            return;
                        }
                      
                        JSONObject object=new JSONObject(payBean.getResult_val());
                        String zfbVal = "";
                        if (object.has("signData")){
                            zfbVal=object.getString("signData");
                        }else{
                            getMvpView().showError(getMvpView().context().getResources().getString(R.string.pay_result_error));
                            return;
                        }
                        
                        Pay pay = new Pay();
                        pay.payByZfb(getMvpView().context(), zfbVal);
//                        String str = response.body();
//                        BaseBean baseBean = JSON.parseObject(str, BaseBean.class);
//                        if (baseBean==null) {
//                            getMvpView().showError(getMvpView().context().getResources().getString(R.string.pay_result_error));
//                            return;
//                        }
//                        int result =baseBean.getResult().getCode();
//                        if (result !=1000) {
//                            getMvpView().showError(getMvpView().context().getResources().getString(R.string.pay_result_error));
//                            return;
//                        }
//                        String payData=baseBean.getBody();
//                        JSONObject jo = new JSONObject(payData);
//                        String data=jo.getString("payParams");
//                        Pay pay = new Pay();
//                        pay.payByZfb(getMvpView().context(), data);
                    } catch (Exception e) {
                        getMvpView().showError(getMvpView().context().getResources().getString(R.string.pay_result_error));
                    }

                } else {
                    getMvpView().showError(getMvpView().context().getResources().getString(R.string.pay_result_error));
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                getMvpView().showError(getMvpView().context().getResources().getString(R.string.pay_result_error));
            }
        });
    }

    @Override
    public void onSuccess(String json) {
        setData(json);
    }

    @Override
    public void setData(String str) {
        try {
            getMvpView().hideLoading();
            List list1=XmlUtils.parseXml(str, PayBean.class);
            if (list1==null||list1.size()==0){
                getMvpView().showError(getMvpView().context().getResources().getString(R.string.pay_result_error));
                return;
            }
            PayBean payBean=(PayBean)list1.get(0);
            if (!payBean.getResult_code().equals("1")){
                getMvpView().showError(getMvpView().context().getResources().getString(R.string.pay_result_error));
                return;
            }
//            Pay pay = new Pay();
//            pay.payByZfb(getMvpView().context(), payBean.getResult_val());
//            BaseBean baseBean = JSON.parseObject(str, BaseBean.class);
//            if (baseBean==null) {
//                getMvpView().showError(getMvpView().context().getResources().getString(R.string.pay_result_error));
//                return;
//            }
//            int result =baseBean.getResult().getCode();
//            if (result !=1000) {
//                getMvpView().showError(getMvpView().context().getResources().getString(R.string.pay_result_error));
//                return;
//            }
//            String payData=baseBean.getBody();
//            JSONObject jo = new JSONObject(payData);
//            String data=jo.getString("payParams");
            JSONObject allData=new JSONObject(payBean.getResult_val());
                Map<String, String> orderInfo = new HashMap();
                orderInfo = judgeNull(allData);
                Pay pay = new Pay();
                if (getMvpView().payWay().equals(Constants.wx_pay)) {
                    pay.payByWx(getMvpView().context(), orderInfo);
            }

        } catch (Exception e) {
            getMvpView().showError(getMvpView().context().getResources().getString(R.string.pay_result_error));
            return;
        }
    }
    
    private void transXml(String xmlStr){
        XmlPullParser parser = Xml.newPullParser();
        try {
            parser.setInput(new StringReader(xmlStr));
            int event = parser.getEventType();
            while (event != XmlPullParser.END_DOCUMENT) {
                switch (event) {
                    case XmlPullParser.START_DOCUMENT:
                        break;
                    case XmlPullParser.START_TAG:
                        if ("UpdateResult".equals(parser.getName())) {
                            int count = parser.getAttributeCount();
                            for (int i = 0; i < count; i++) {
                                String key = parser.getAttributeName(i);
                                if ("NeedUpdate".equals(key)) {
                              //      isNeedUpdate= "true".equals(parser.getAttributeValue(i));
                                }
                            }
                        } else if ("FileUrl".equals(parser.getName())) {
                            int count = parser.getAttributeCount();
                            for (int i = 0; i < count; i++) {
                                String key = parser.getAttributeName(i);
                                if ("value".equals(key)) {
                                //    FileUrl = parser.getAttributeValue(i);
                                }
                            }
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        break;
                }
                event = parser.next();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
       
    }

    private void test() {
        if (getMvpView().payWay().equals(Constants.wx_pay)) {
//                    getMvpView().payUseWx(orderInfo);
            WXpay wXpay = new WXpay(getMvpView().context());
            wXpay.wxpay(getMvpView().getTheIntent().getStringExtra("orderId"));
        } else if (getMvpView().payWay().equals(Constants.zfb_pay)) {
//                    getMvpView().payUseZfb(orderInfo);
            ZFBPay zfbPay = new ZFBPay(getMvpView().context());
            zfbPay.pay();
        }
        return;
    }

    /**
     * 判空返回值是否为null
     *
     * @param jo
     * @return
     */
    private Map<String, String> judgeNull(JSONObject jo) {
        Map<String, String> orderInfo = new HashMap();
        try {
            if (jo.has("appid")){
                String appId = changeNull(jo.getString("appid"));
                orderInfo.put("appId", appId);
            }
            if (jo.has("partnerid")){
                String partnerId = changeNull(jo.getString("partnerid"));
                orderInfo.put("partnerId", partnerId);
            }
            if (jo.has("prepayid")){
                String prepayId = changeNull(jo.getString("prepayid"));
                orderInfo.put("prepayId", prepayId);
            }
            if (jo.has("package")){
                String packageValue = changeNull(jo.getString("package"));
                orderInfo.put("packageValue", packageValue);
            }
            if (jo.has("timeStamp")){
                String timeStamp = changeNull(jo.getString("timeStamp"));
                orderInfo.put("timeStamp", timeStamp);
            }
            if (jo.has("nonceStr")){
                String nonceStr = changeNull(jo.getString("nonceStr"));
                orderInfo.put("nonceStr", nonceStr);
            }
            if (jo.has("sign")){
                String sign = changeNull(jo.getString("sign"));
                orderInfo.put("sign", sign);
            }
           
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return orderInfo;
    }

    /**
     * 改变当前为null的返回值
     * @param appId
     */
    private String changeNull(Object appId) {
        if (appId == null) {
            appId = "";
        }
        return (String) appId;
    }

    @Override
    public void onError(Exception e) {
        super.onError(e);
        getMvpView().showError(getMvpView().context().getResources().getString(R.string.pay_internet_error));
        getMvpView().hideLoading();
    }
}
