package com.bjjy.mainclient.phone.view.classroom.question;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.bjjy.mainclient.phone.event.DeleteQuestionCollection;
import com.dongao.libs.dongaoumengpushlib.constants.PushConstants;
import com.dongao.mainclient.core.util.DateUtil;
import com.dongao.mainclient.model.bean.question.Question;
import com.dongao.mainclient.model.bean.user.MyCollection;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.MyCollectionDB;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.bjjy.mainclient.persenter.MyQuestionListPersenter;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.event.CourseTabTypeEvent;
import com.bjjy.mainclient.phone.event.UpdateEvent;
import com.bjjy.mainclient.phone.utils.NetworkUtil;
import com.bjjy.mainclient.phone.view.classroom.course.HeaderViewPagerFragment;
import com.bjjy.mainclient.phone.view.exam.ExamActivity;
import com.bjjy.mainclient.phone.view.question.AddQuestionActivity;
import com.bjjy.mainclient.phone.view.question.MyQuestionListActivity;
import com.bjjy.mainclient.phone.view.question.MyQuestionListView;
import com.bjjy.mainclient.phone.view.question.MyQuestionModifyActivity;
import com.bjjy.mainclient.phone.view.question.utils.JavaScriptInterface;
import com.bjjy.mainclient.phone.view.question.utils.QuestionParserUtil;
import com.bjjy.mainclient.phone.widget.EmptyViewLayout;
import com.bjjy.mainclient.phone.widget.NoScrollRefreshScrollView;
import com.bjjy.mainclient.phone.widget.RefreshScrollView;
import com.bjjy.mainclient.phone.widget.ScrollViewFooter;
import com.umeng.analytics.MobclickAgent;

import org.greenrobot.eventbus.Subscribe;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import org.greenrobot.eventbus.EventBus;

/**
 * Created by fengzongwei on 2016/6/2 0002.
 */
public class ClassQuestionFragment extends HeaderViewPagerFragment implements MyQuestionListView,RefreshScrollView.OnRefreshScrollViewListener{
    @Bind(R.id.myquestion_scroll)
    NoScrollRefreshScrollView refreshScrollView;
    @Bind(R.id.my_questiont_hintImg)
    ImageView imageView_hint;
    @Bind(R.id.my_ques_list_lodingBody)
    RelativeLayout relativeLayout_hint_body;
    @Bind(R.id.my_ques_list_pb)
    ProgressBar progressBar;
    @Bind(R.id.my_ques_list_picBody)
    LinearLayout linearLayout_pic_body;
    @Bind(R.id.my_ques_list_pic_tvHint)
    TextView textView_hint;
    //当前执行动画的控件
    private LinearLayout linearLayout_btBody,linearLayout_zhuiwenContainer;

    private final int maxHeight = 360;
    private static final String encoding = "utf-8";
    private static final String mimeType = "text/html";

    private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private String pushExamId,questionAnswerId,examId,subjectId,userId;

    // 一次点击是否结束
    private boolean isClickOver =true;
    //点击动画是否完成
    private boolean isAnimationOver = false;
    //所点击按钮的标示
    private String compareId = null;
    //进行展开还是收起动作的标示
    private int tag;
    private int modify_ques_position;
    private int childParentWidght;
    private int zhuiwenBtBodyWidth,copyzhuiwenBtBodyWidth;
    private LinearLayout.LayoutParams layoutParams_content;
    private LinearLayout.LayoutParams layoutParams_contentAll = new LinearLayout.LayoutParams
            (LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

    private ArrayList<View> viewArrayList = new java.util.ArrayList<>();
    private ArrayList<LinearLayout> zhuiwenContainerList = new ArrayList<>();
    private ArrayList<LinearLayout> zhuiwenByBodyList = new ArrayList<>();

    private ArrayList<ImageView> imageViewArrayList = new ArrayList<>();//收藏图片
    private ArrayList<TextView> textViewArrayList = new ArrayList<>();//收藏文字

    private List<Question> lisMyQuestionObjs;//存放当前页面所有数据的集合

    private MyQuestionListPersenter myQuestionListPersenter;

    private boolean isLoadmore = false;//是否是加载更多操作

    private String bookJson;//可选书数据json

    private MyCollectionDB myCollectionDB;

    private boolean isShowAnimProgress = false;

    private Handler handlerChangeUi = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            if(msg.what == 1){
                imageViewArrayList.get(msg.arg1).setImageResource(R.drawable.collect_star_black);
                textViewArrayList.get(msg.arg1).setText("收藏");
                textViewArrayList.get(msg.arg1).setTextColor(Color.parseColor("#666666"));
            }
        }
    };

    private Handler handler = new Handler(){
        public void handleMessage(Message msg) {
            if(msg.what == 1){//展示的追问的根布局现在为隐藏状态
                if(linearLayout_btBody == null){
                    for(int i=0;i<zhuiwenByBodyList.size();i++){
                        String id = (String)zhuiwenByBodyList.get(i).getTag();
                        if(id.equals(compareId)){
                            linearLayout_btBody = zhuiwenByBodyList.get(i);
                        }
                    }
                    for(int i=0;i<zhuiwenContainerList.size();i++){
                        String id = (String)zhuiwenContainerList.get(i).getTag();
                        if(id.equals(compareId)){
                            linearLayout_zhuiwenContainer = zhuiwenContainerList.get(i);
                        }
                    }
                }
                if (copyzhuiwenBtBodyWidth == 0) {
                    copyzhuiwenBtBodyWidth = zhuiwenBtBodyWidth;
                }
                if (copyzhuiwenBtBodyWidth < childParentWidght && linearLayout_btBody != null) {
                    copyzhuiwenBtBodyWidth = copyzhuiwenBtBodyWidth + childParentWidght / 100;
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(copyzhuiwenBtBodyWidth, RelativeLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                    linearLayout_btBody.setLayoutParams(layoutParams);
                } else {
                    if (linearLayout_zhuiwenContainer != null)
                        linearLayout_zhuiwenContainer.setVisibility(View.VISIBLE);
                    if(linearLayout_zhuiwenContainer!=null && linearLayout_zhuiwenContainer.getChildCount()==0){
                        List<Question> list = null;
                        for(int i=0;i<lisMyQuestionObjs.size();i++){
                            if(lisMyQuestionObjs.get(i).getAskId().equals(compareId))
                                list = lisMyQuestionObjs.get(i).getChildQuestions();
                        }
                        addZhuiWenItem(list,linearLayout_zhuiwenContainer);
                    }
                    copyzhuiwenBtBodyWidth = 0;
                    isClickOver = true;
                    isAnimationOver = true;
                    linearLayout_btBody = null;
                    linearLayout_zhuiwenContainer = null;
                    compareId = null;
                }
            }else if(msg.what == 2){//展示的追问的根布局现在为显示状态
                if(linearLayout_btBody == null){
                    for(int i=0;i<zhuiwenByBodyList.size();i++){
                        String id = (String)zhuiwenByBodyList.get(i).getTag();
                        if(id.equals(compareId)){
                            linearLayout_btBody = zhuiwenByBodyList.get(i);
                        }
                    }
//					linearLayout_zhuiwenContainer = findLinearById(compareId);
                    for(int i=0;i<zhuiwenContainerList.size();i++){
                        String id = (String)zhuiwenContainerList.get(i).getTag();
                        if(id.equals(compareId)){
                            linearLayout_zhuiwenContainer = zhuiwenContainerList.get(i);
                        }
                    }
                }
                if (copyzhuiwenBtBodyWidth == 0) {
                    copyzhuiwenBtBodyWidth = childParentWidght;
                }
                if (copyzhuiwenBtBodyWidth >= (zhuiwenBtBodyWidth+10) && linearLayout_btBody != null) {
                    copyzhuiwenBtBodyWidth = copyzhuiwenBtBodyWidth - childParentWidght / 100;
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(copyzhuiwenBtBodyWidth, RelativeLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                    linearLayout_btBody.setLayoutParams(layoutParams);
                } else {
                    if (linearLayout_zhuiwenContainer != null)
                        linearLayout_zhuiwenContainer.setVisibility(View.GONE);
                    copyzhuiwenBtBodyWidth = 0;
                    isClickOver = true;
                    isAnimationOver = true;
                    linearLayout_btBody = null;
                    linearLayout_zhuiwenContainer = null;
                    compareId = null;
                }
            }
//            else if (msg.what == TaskType.TS_ISCONNECT_BACKGROUND){
//                String list_str = (String) msg.obj;
//                initListView(list_str);
//            }
        };
    };

    private View view;

    private int page = 1;//加载数据的页数
    private int totalPage;
    private EmptyViewLayout emptyViewLayout;


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
       // setUserVisibleHint(true);
        super.onActivityCreated(savedInstanceState);
    }

    private boolean mHasLoadedOnce = false;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if (isVisibleToUser) {
                if(!mHasLoadedOnce && lisMyQuestionObjs == null){
                    // async http request here
                    mHasLoadedOnce = true;
                    initData();
                }else if(mHasLoadedOnce){
                    if(!userId.equals(SharedPrefHelper.getInstance(getActivity()).getUserId())){
                        initData();
                    }else{
                        if(lisMyQuestionObjs != null && !lisMyQuestionObjs.isEmpty())
//                            freshContentView(lisMyQuestionObjs);

                        if (refreshScrollView != null) {
                            refreshScrollView.smoothScrollTo(0, 0);
                        }
                    }
                }
            }
        }
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.ketang_question,container,false);
        ButterKnife.bind(this,view);
        EventBus.getDefault().register(this);
        myQuestionListPersenter = new MyQuestionListPersenter();
        myQuestionListPersenter.attachView(this);
        myCollectionDB = new MyCollectionDB(getActivity());
        ViewGroup parent = (ViewGroup) view.getParent();
        if (parent != null){
            parent.removeView(view);
        }

        initView();
        return view;
    }

    @Override
    public void initView() {
        emptyViewLayout = new EmptyViewLayout(getActivity(),refreshScrollView);
//        ViewGroup viewGroup= (ViewGroup) LayoutInflater.from(getActivity()).inflate(R.layout.app_view_error_dongao,null);
//        emptyViewLayout.setNetErrorView(viewGroup);
        emptyViewLayout.setErrorButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emptyViewLayout.showLoading();
                getData();
            }
        });
        refreshScrollView.setOnRefreshScrollViewListener(this);
        refreshScrollView.setEnableRefresh(false);
        refreshScrollView.setHeaderGone();
    }

    /**
     * 加载数据总方法
     */
    private void getData(){
        SimpleDateFormat sm=new SimpleDateFormat("MM-dd HH:mm:ss");
//        SharedPrefHelper.getInstance(getActivity()).setMyQuestionListTime(userId, sm.format(new Date()));
//        refreshScrollView.setRefreshTime(SharedPrefHelper.getInstance().getMyQuestionListTime(userId));
        if(NetworkUtil.isNetworkAvailable(appContext)) {
            if(lisMyQuestionObjs == null || lisMyQuestionObjs.size()==0) {
//                relativeLayout_hint_body.setVisibility(View.VISIBLE);
//                progressBar.setVisibility(View.VISIBLE);
//                linearLayout_pic_body.setVisibility(View.GONE);
                if(emptyViewLayout!=null)
                    emptyViewLayout.showLoading();
            }
            if (questionAnswerId==null || questionAnswerId.isEmpty()){
                myQuestionListPersenter.getData();
            }else{
            }
        }else{
            if(emptyViewLayout!=null)
                emptyViewLayout.showNetErrorView();
//            showError("网路异常，点击重试");
        }
    }

    @OnClick(R.id.ketang_question_add) void addQuestion(){
        if(bookJson!=null && !bookJson.equals("[]")){
            Intent intent = new Intent(getActivity(),AddQuestionActivity.class);
            intent.putExtra("examId",SharedPrefHelper.getInstance(getActivity()).getExamId());
            intent.putExtra("subjectId",SharedPrefHelper.getInstance(getActivity()).getSubjectId());
            intent.putExtra("bookList",bookJson);
            getActivity().startActivity(intent);
            MobclickAgent.onEvent(getActivity(), PushConstants.CLASSROOM_QUESTION_TO_ASKQUESTION);
        }else if(bookJson!=null && bookJson.equals("[]")){
            Toast.makeText(getActivity(),"无教材信息",Toast.LENGTH_SHORT).show();
        }else if(bookJson == null){
            isShowAnimProgress = true;
            showAnimProgress("数据加载中...");
            myQuestionListPersenter.getSectionsBySubjectId();
        }

//        Intent intent = new Intent(getActivity(),ExamRecommQuestionListActivity.class);
//        startActivity(intent);
    }

    @OnClick(R.id.ketang_question_my) void myQuestion(){
        Intent intent = new Intent(getActivity(),MyQuestionListActivity.class);
        getActivity().startActivity(intent);
    }

    @OnClick(R.id.my_ques_list_picBody) void retry(){
        getData();
    }

    /**
     * 刷新scrollView的内容
     */
    private void freshContentView(final List<Question> questionList){
        emptyViewLayout.showContentView();
        if(!isLoadmore){
            viewArrayList.clear();
            zhuiwenContainerList.clear();
            zhuiwenByBodyList.clear();
            imageViewArrayList.clear();
            textViewArrayList.clear();
        }else{
            lisMyQuestionObjs.addAll(questionList);
        }
        for(int i=0;i<questionList.size();i++){
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.my_question_item,null);
            final TextView textView_bookName,textView_queTime,textView_allContent,textView_anwTime,
                    textView_subjectName,textView_continue,textView_modify,textView_zhuiwenText,textView_go,textView_collect;
            final LinearLayout linearLayout_answerBody,linearLayout_answerContainer11,linearLayout_zhuiwenBtBody,linearLayout_go,linearLayout_continue,
                    linearLayout_modify,linearLayout_allContent,linearLayout_img_body,linearLayout_recomm_goQuestion,linearLayout_recomm_collect;
            final ImageView imageView_continue,imageView_modify,imageView_allContent,imageView_look_continue_ask,imageView_zhuiwen
                    ,imageView_from,imageView_go,imageView_collect;
            final HtmlTextView htmlTextView_anw,htmlTextView_que;
            final WebView webView_anw,webView_que;
            final RelativeLayout relativeLayout_zhuiwenBody;
            final LinearLayout linearLayout_function_body;
            imageView_collect = (ImageView)view.findViewById(R.id.recomm_question_item_continueAsk_img);
            imageView_collect.setTag(questionList.get(i).getAskId());
            imageViewArrayList.add(imageView_collect);
            textView_collect = (TextView)view.findViewById(R.id.recomm_question_item_continueAsk_text);
            textView_collect.setTag(questionList.get(i).getAskId());
            textViewArrayList.add(textView_collect);
            imageView_from = (ImageView)view.findViewById(R.id.my_question_item_from_img);
            imageView_from.setVisibility(View.GONE);
            textView_go = (TextView)view.findViewById(R.id.recomm_question_item_go_tv);
            imageView_go = (ImageView)view.findViewById(R.id.recomm_question_item_go_img);
            linearLayout_recomm_goQuestion = (LinearLayout)view.findViewById(R.id.recomm_question_item_goQuestion);
            linearLayout_recomm_goQuestion.setTag(i);
            linearLayout_recomm_collect = (LinearLayout)view.findViewById(R.id.recomm_question_item_continueAsk);
            linearLayout_recomm_collect.setTag(questionList.get(i));
            linearLayout_function_body = (LinearLayout)view.findViewById(R.id.my_question_item_function_body);
            linearLayout_function_body.setVisibility(View.GONE);
            linearLayout_img_body = (LinearLayout)view.findViewById(R.id.my_question_item_img_body);
            linearLayout_img_body.setVisibility(View.GONE);
            textView_subjectName = (TextView)view.findViewById(R.id.my_question_item_subjectName);
            htmlTextView_que = (HtmlTextView)view.findViewById(R.id.my_question_item_queContentHtml);
            webView_que = (WebView)view.findViewById(R.id.my_question_item_queContentWeb);
            textView_queTime = (TextView)view.findViewById(R.id.my_question_item_queTime);
            textView_bookName = (TextView)view.findViewById(R.id.my_question_item_bookName);
            linearLayout_answerBody = (LinearLayout)view.findViewById(R.id.my_question_item_answerBody);
            htmlTextView_anw = (HtmlTextView)view.findViewById(R.id.my_question_item_anwContentHtml);
            webView_anw = (WebView)view.findViewById(R.id.my_question_item_anwContentWeb);
            webView_anw.getSettings().setJavaScriptEnabled(true);
            webView_anw.addJavascriptInterface(new JavaScriptInterface(getActivity()),
                    "JSInterface");
            textView_anwTime = (TextView)view.findViewById(R.id.my_question_item_answerTime);
            linearLayout_allContent = (LinearLayout)view.findViewById(R.id.my_question_item_allContent);
            linearLayout_allContent.setTag(i);
            textView_allContent = (TextView)view.findViewById(R.id.my_question_item_allContent_text);
            imageView_allContent = (ImageView)view.findViewById(R.id.my_question_item_allContent_img);
            relativeLayout_zhuiwenBody = (RelativeLayout)view.findViewById(R.id.my_question_item_zhuiwenBody);
            linearLayout_zhuiwenBtBody = (LinearLayout)view.findViewById(R.id.my_question_item_zhuiwenBtBody);
            linearLayout_zhuiwenBtBody.setTag(lisMyQuestionObjs.get(i).getAskId());
            imageView_zhuiwen = (ImageView)view.findViewById(R.id.my_question_item_zhuiwenBt);
            textView_zhuiwenText = (TextView)view.findViewById(R.id.my_question_item_zhuiwenBt_text);
            zhuiwenByBodyList.add(linearLayout_zhuiwenBtBody);
            imageView_look_continue_ask = (ImageView)view.findViewById(R.id.my_question_item_zhuiwenBt);
            imageView_look_continue_ask.setTag(lisMyQuestionObjs.get(i).getAskId());
            linearLayout_answerContainer11 = (LinearLayout)view.findViewById(R.id.my_question_item_zhuiwenContainer);
            linearLayout_answerContainer11.setTag(lisMyQuestionObjs.get(i).getAskId());
            linearLayout_go = (LinearLayout)view.findViewById(R.id.my_question_item_goQuestion);
            linearLayout_go.setTag(i);
            linearLayout_continue = (LinearLayout)view.findViewById(R.id.my_question_item_continueAsk);
            textView_continue = (TextView)view.findViewById(R.id.my_question_item_continueAsk_text);
            imageView_continue = (ImageView)view.findViewById(R.id.my_question_item_continueAsk_img);
            linearLayout_modify = (LinearLayout)view.findViewById(R.id.my_question_item_modify);
            textView_modify = (TextView)view.findViewById(R.id.my_question_item_modify_text);
            imageView_modify = (ImageView)view.findViewById(R.id.my_question_item_modify_img);
            /**
             * 只添加父问题
             */
                if(QuestionParserUtil.isParent(questionList.get(i).getParentId())){

                    if(myCollectionDB.findCollection(SharedPrefHelper.getInstance(getActivity()).getUserId(), questionList.get(i).getAskId(),
                            MyCollection.TYPE_QUESTION + "") != null){
                        imageView_collect.setImageResource(R.drawable.icon_collected);
                        textView_collect.setText("已收藏");
                        textView_collect.setTextColor(Color.parseColor("#ff7666"));
                    }

                    if(questionList.get(i).getQasTypeFlag().equals("2")){
                        textView_go.setTextColor(Color.parseColor("#DBDBDB"));
                        imageView_go.setImageResource(R.drawable.icon_subject_gray);
                    }
                    try{
                        textView_queTime.setText(DateUtil.twoDateDistance(new Date(format.parse(questionList.get(i).getCreateTime()).getTime()), new Date()));
                    }catch (Exception e){
                        e.printStackTrace();
                        textView_queTime.setText(questionList.get(i).getCreateTime());
                    }
//                    textView_queTime.setText(questionList.get(i).getCreateTime());
                    textView_subjectName.setText(questionList.get(i).getSubjectName());
                    if(questionList.get(i).getContent().contains("</td>")){
                        webView_que.setVisibility(View.VISIBLE);
                        htmlTextView_que.setVisibility(View.GONE);
                        webView_que.loadDataWithBaseURL("", "<font color='#808080' style='font-size:15px;'>" + (questionList.get(i).getContent()) + "</font>", mimeType, encoding, "");
                    }else{
                        htmlTextView_que.setHtmlFromString("<font color='#808080' style='font-size:15px;'>" + (questionList.get(i).getContent()) + "</font>", new HtmlTextView.RemoteImageGetter());
                    }
                    if(!questionList.get(i).getReadStatus().equals("0")){
                        try{
                            textView_anwTime.setText(DateUtil.twoDateDistance(new Date(format.parse(questionList.get(i).getAnswerTime()).getTime()), new Date()));
                        }catch (Exception e){
                            e.printStackTrace();
                            textView_anwTime.setText(questionList.get(i).getAnswerTime());
                        }
//                        textView_anwTime.setText(questionList.get(i).getAnswerTime());
                        if(questionList.get(i).getAnswer().contains("</td>") || questionList.get(i).getAnswer().contains("<img")){
                            webView_anw.setVisibility(View.VISIBLE);
                            layoutParams_content = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,maxHeight);
                            layoutParams_content.setMargins(webView_anw.getLeft(),webView_anw.getTop(),webView_anw.getRight(),0);
                            layoutParams_contentAll.setMargins(webView_anw.getLeft(),webView_anw.getTop(),webView_anw.getRight(),0);
                            webView_anw.setLayoutParams(layoutParams_content);
                            questionList.get(i).setShowAllAns(1);
                            htmlTextView_anw.setVisibility(View.GONE);
                            webView_anw.loadDataWithBaseURL("",
                                    webContentHandle(questionList.get(i).getAnswer()),
                                    mimeType,
                                    encoding, "");
                        }else{
                            htmlTextView_anw.setTag(i);
                            htmlTextView_anw.setHtmlFromString("<font color='#808080' style='font-size:15px;'>" + (questionList.get(i).getAnswer()) + "</font>", new HtmlTextView.RemoteImageGetter());
                            if(layoutParams_content == null) {
                                layoutParams_content = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,maxHeight);
                                layoutParams_contentAll = (LinearLayout.LayoutParams) htmlTextView_anw.getLayoutParams();
                                layoutParams_content.setMargins(layoutParams_contentAll.leftMargin,layoutParams_contentAll.topMargin,layoutParams_contentAll.rightMargin, 0);
                            }
                            ViewTreeObserver viewTreeObserver = htmlTextView_anw.getViewTreeObserver();
                            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                                @Override
                                public void onGlobalLayout() {
                                    if (htmlTextView_anw.getVisibility() == View.VISIBLE && questionList.get((int) htmlTextView_anw.getTag()).isShowAllAns() == -1) {
                                        if (htmlTextView_anw.getHeight() >= maxHeight) {
                                            questionList.get((int) htmlTextView_anw.getTag()).setShowAllAns(1);
                                            htmlTextView_anw.setLayoutParams(layoutParams_content);
                                        } else {
                                            questionList.get((int) htmlTextView_anw.getTag()).setShowAllAns(0);
                                            linearLayout_allContent.setVisibility(View.GONE);
                                        }
                                    }
                                    htmlTextView_anw.getViewTreeObserver().removeGlobalOnLayoutListener(this);//没有此句话，此监听listerner会一直监听，从而影响之后的setLayoutParams
                                }
                            });
                        }
                        if(questionList.get(i).getChildAskList() == null || questionList.get(i).getChildQuestionCount() == 0) {
                            relativeLayout_zhuiwenBody.setVisibility(View.GONE);
                        }else{
                            zhuiwenContainerList.add(linearLayout_answerContainer11);
                        }
//                        if(questionList.get(i).isCanAsk()){
//    //                        textView_isAnswer.setText("已回复");
//    //                        textView_isAnswer.setTextColor(Color.parseColor("#0ac7c4"));
//                            linearLayout_continue.setTag(i);
//                            textView_modify.setTextColor(Color.parseColor("#C2C2C2"));
//                            imageView_modify.setImageResource(R.drawable.icon_modify_disabled);
//                        }else{
//                            linearLayout_modify.setTag(i);
//                            textView_continue.setTextColor(Color.parseColor("#C2C2C2"));
//                            imageView_continue.setImageResource(R.drawable.icon_ask_disabled);
//    //                        textView_isAnswer.setText("未回复");
//    //                        textView_isAnswer.setTextColor(Color.parseColor("#ff661a"));
//                        }
                    }else{
                        textView_continue.setTextColor(Color.parseColor("#C2C2C2"));
                        imageView_continue.setImageResource(R.drawable.icon_ask_disabled);
                        linearLayout_modify.setTag(i);
                        linearLayout_answerBody.setVisibility(View.GONE);
    //                    textView_isAnswer.setText("未回复");
    //                    textView_isAnswer.setTextColor(Color.parseColor("#ff661a"));
                    }

                    linearLayout_allContent.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(htmlTextView_anw.getVisibility() == View.VISIBLE){
                                if(questionList.get((int)linearLayout_allContent.getTag()).isShowAllAns() == 1){
                                    questionList.get((int) linearLayout_allContent.getTag()).setShowAllAns(2);
                                    htmlTextView_anw.setLayoutParams(layoutParams_contentAll);
                                    textView_allContent.setText("收起全文");
                                    imageView_allContent.setImageResource(R.drawable.icon_arrow_up);
                                }else{
                                    questionList.get((int) linearLayout_allContent.getTag()).setShowAllAns(1);
                                    htmlTextView_anw.setLayoutParams(layoutParams_content);
                                    textView_allContent.setText("查看全文");
                                    imageView_allContent.setImageResource(R.drawable.icon_arrow_down);
                                }
                            }else{
                                if(questionList.get((int)linearLayout_allContent.getTag()).isShowAllAns() == 1){
                                    questionList.get((int) linearLayout_allContent.getTag()).setShowAllAns(2);
                                    webView_anw.setLayoutParams(layoutParams_contentAll);
                                    textView_allContent.setText("收起全文");
                                    imageView_allContent.setImageResource(R.drawable.icon_arrow_up);
                                }else{
                                    questionList.get((int) linearLayout_allContent.getTag()).setShowAllAns(1);
                                    webView_anw.setLayoutParams(layoutParams_content);
                                    textView_allContent.setText("查看全文");
                                    imageView_allContent.setImageResource(R.drawable.icon_arrow_down);
                                }
                            }
                        }
                    });

                    imageView_look_continue_ask.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(childParentWidght == 0){
                                childParentWidght = relativeLayout_zhuiwenBody.getWidth();
                                zhuiwenBtBodyWidth = linearLayout_zhuiwenBtBody.getWidth();
                            }
                            if(isClickOver){
                                isClickOver = false;
                                isAnimationOver = false;
                                Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.my_question_item_rotate);
                                imageView_zhuiwen.startAnimation(animation);
                                if(linearLayout_answerContainer11.getVisibility() == View.GONE){
                                    tag = 1;
                                    textView_zhuiwenText.setText("收起追问");
                                }else {
                                    tag = 2;
                                    textView_zhuiwenText.setText("查看追问");
                                }
                                if(compareId == null)
                                    compareId = (String)imageView_look_continue_ask.getTag();
                                new Thread(){
                                    @Override
                                    public void run() {
                                        while(!isAnimationOver) {
                                            try{
                                                Thread.sleep(2);
                                            }catch (Exception e){
                                                e.printStackTrace();
                                            }
                                            handler.sendEmptyMessage(tag);
                                        }
                                    }
                                }.start();
                            }
                        }
                    });
                    linearLayout_recomm_collect.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(myCollectionDB.findCollection(SharedPrefHelper.getInstance(getActivity()).getUserId(),((Question) linearLayout_recomm_collect.getTag()).getAskId(),
                                    MyCollection.TYPE_QUESTION + "") != null){
                                myCollectionDB.delete(myCollectionDB.findCollection(SharedPrefHelper.getInstance(getActivity()).getUserId(),
                                        ((Question) linearLayout_recomm_collect.getTag()).getAskId(),
                                        MyCollection.TYPE_QUESTION + ""));
                                imageView_collect.setImageResource(R.drawable.collect_star_black);
                                textView_collect.setText("收藏");
                                textView_collect.setTextColor(Color.parseColor("#666666"));
                            }else{
                                MyCollection myCollection = new MyCollection();
                                myCollection.setContent(JSON.toJSONString(myCollection));
                                myCollection.setCollectionId(((Question) linearLayout_recomm_collect.getTag()).getAskId());
                                myCollection.setType(MyCollection.TYPE_QUESTION + "");
                                if(TextUtils.isEmpty(((Question) linearLayout_recomm_collect.getTag()).getFinalTitle()))
                                    myCollection.setTitle(((Question) linearLayout_recomm_collect.getTag()).getTitle());
                                else
                                    myCollection.setTitle(((Question) linearLayout_recomm_collect.getTag()).getFinalTitle());
                                myCollection.setTime(System.currentTimeMillis() + "");
                                myCollection.setUserId(SharedPrefHelper.getInstance(getActivity()).getUserId());
                                myCollectionDB.insert(myCollection);
                                imageView_collect.setImageResource(R.drawable.icon_collected);
                                textView_collect.setText("已收藏");
                                textView_collect.setTextColor(Color.parseColor("#ff7666"));
                            }
                            MobclickAgent.onEvent(getActivity(),PushConstants.QUESTION_COLLECT);
                        }
                    });
                    linearLayout_recomm_goQuestion.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(questionList.get((int) linearLayout_go.getTag()).getQasTypeFlag().equals("2"))
                                return;
                            Intent intent = new Intent(getActivity(), ExamActivity.class);
                            intent.putExtra("questionId", questionList.get((int) linearLayout_go.getTag()).getExaminationQuestionId());
                            SharedPrefHelper.getInstance(getActivity()).setExamTag(Constants.EXAM_ORIGINAL_QUESTION);
                            getActivity().startActivity(intent);
                        }
                    });
                    linearLayout_go.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
    //                        Intent intent = new Intent(MyQuestionListActivity.this, ExamActivity.class);
    //                        intent.putExtra("questionId", lisMyQuestionObjs.get((int) linearLayout_go.getTag()).getExamQuestionId());
    //                        SharedPrefHelper.getInstance(MyQuestionListActivity.this).setExamTag(com.dongao.app.exam.common.Constants.EXAM_ORIGINAL_QUESTION);
    //                        MyQuestionListActivity.this.startActivity(intent);
                        }
                    });

                    linearLayout_continue.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(questionList.get(((int)linearLayout_continue.getTag())).getQasTypeFlag().equals("2")){//试题答疑修改
                                Intent intent = new Intent(getActivity(), MyQuestionModifyActivity.class);
                                intent.putExtra("mode", 1);
                                if (QuestionParserUtil.isParent(questionList.get((int) linearLayout_continue.getTag()).getParentId()))
                                    intent.putExtra("qaFatherId", questionList.get((int) linearLayout_continue.getTag()).getAskId());
                                else
                                    intent.putExtra("qaFatherId", questionList.get((int) linearLayout_continue.getTag()).getParentId());
                                getActivity().startActivityForResult(intent, 0);
                            }else{//教材答疑修改
                                Intent intent = new Intent(getActivity(), AddQuestionActivity.class);
                                intent.putExtra("modifyQuestion", questionList.get(((int) linearLayout_continue.getTag())));
                                getActivity().startActivityForResult(intent, 0);
                            }
                        }
                    });

                    linearLayout_modify.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(linearLayout_modify.getTag()!=null){
                                modify_ques_position = (int) linearLayout_modify.getTag();
                                Intent intent = new Intent(getActivity(), MyQuestionModifyActivity.class);
                                intent.putExtra("mode", 2);
                                /**
                                 * 此题有没有追问
                                 */
                                String questionId = null,neirong =null;
                                if(questionList.get(modify_ques_position).getChildQuestionCount()>0){
                                    int childCount = questionList.get(modify_ques_position).getChildQuestionCount();
                                    questionId = questionList.get(modify_ques_position+childCount).getAskId();
                                    neirong = questionList.get(modify_ques_position+childCount).getContent();
                                }else{
                                    questionId = questionList.get(modify_ques_position).getAskId();
                                    neirong = questionList.get(modify_ques_position).getContent();
                                }
                                intent.putExtra("questionId", questionId);
                                intent.putExtra("neirong",neirong);
                                getActivity().startActivityForResult(intent, 0);
                            }
                        }
                    });
                    viewArrayList.add(view);

                }

        }
        refreshScrollView.refreshContainer(viewArrayList);
        for(int i=0;i<zhuiwenContainerList.size();i++){
            zhuiwenContainerList.get(i).setVisibility(View.GONE);
        }
    }

    /**
     * 将追问的条目逐条加入到追问显示主体中
     * @param listQuestion  某个父问题所对应的追问集合
     * @param linearLayout_body 显示追问条目的主体
     */
    private void addZhuiWenItem(final List<Question> listQuestion,final LinearLayout linearLayout_body) {
        for (int i = 0; i < listQuestion.size(); i++) {
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.my_question_item_zhuiwen_item, null);
            final HtmlTextView htmlTextView_que, htmlTextView_anw;
            final TextView textView_createTime, textView_answTime, textView_isAnswer, textView_allContent;
            final WebView webView_que, webView_ans;
            final ImageView imageView_allContent;
            final LinearLayout linearLayout_anwsBody, linearLayout_allContent;
            htmlTextView_que = (HtmlTextView) view.findViewById(R.id.my_question_zhuiwen_item_queContentHtml);
            webView_que = (WebView) view.findViewById(R.id.my_question_zhuiwen_item_queContentWeb);
            textView_isAnswer = (TextView) view.findViewById(R.id.my_question_zhuiwen_item_isAnswer);
            textView_createTime = (TextView) view.findViewById(R.id.my_question_zhuiwen_item_queTime);
            linearLayout_anwsBody = (LinearLayout) view.findViewById(R.id.my_question_zhuiwen_item_answBody);
            htmlTextView_anw = (HtmlTextView) view.findViewById(R.id.my_question_zhuiwen_item_anwContentHtml);
            webView_ans = (WebView) view.findViewById(R.id.my_question_zhuiwen_item_anwContentWeb);
            linearLayout_allContent = (LinearLayout) view.findViewById(R.id.my_question_zhuiwen_item_allContent);
            textView_allContent = (TextView) view.findViewById(R.id.my_question_zhuiwen_item_allContent_text);
            imageView_allContent = (ImageView) view.findViewById(R.id.my_question_zhuiwen_item_allContent_img);
            textView_answTime = (TextView) view.findViewById(R.id.my_question_zhuiwen_item_answerTime);

            textView_createTime.setText(listQuestion.get(i).getCreateTime());

            if (listQuestion.get(i).getContent().contains("</td>")) {
                webView_que.setVisibility(View.VISIBLE);
                htmlTextView_que.setVisibility(View.GONE);
                webView_que.loadDataWithBaseURL("", "<font color='#808080' style='font-size:15px;'>" + (listQuestion.get(i).getContent()) + "</font>", mimeType, encoding, "");
            } else {
                htmlTextView_que.setHtmlFromString("<font color='#808080' style='font-size:15px;'>" + (listQuestion.get(i).getContent()) + "</font>", new HtmlTextView.RemoteImageGetter());
            }

            if (lisMyQuestionObjs.get(i).getReadStatus().equals("0")) {
                linearLayout_anwsBody.setVisibility(View.GONE);
                textView_isAnswer.setText("未回复");
                textView_isAnswer.setTextColor(Color.parseColor("#ff661a"));
            } else {
                textView_isAnswer.setText("已回复");
                textView_isAnswer.setTextColor(Color.parseColor("#0ac7c4"));
                textView_answTime.setText(listQuestion.get(i).getAnswerTime());
                if (listQuestion.get(i).getAnswer().contains("</td>")) {
                    webView_ans.setVisibility(View.VISIBLE);
                    layoutParams_content = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, maxHeight);
                    layoutParams_content.setMargins(webView_ans.getLeft(), webView_ans.getTop(), webView_ans.getRight(), 0);
                    layoutParams_contentAll.setMargins(webView_ans.getLeft(), webView_ans.getTop(), webView_ans.getRight(), 0);
                    webView_ans.setLayoutParams(layoutParams_content);
                    htmlTextView_anw.setVisibility(View.GONE);
                    webView_ans.loadDataWithBaseURL("", "<font color='#808080' style='font-size:15px;'>" + (listQuestion.get(i).getAnswer()) + "</font>", mimeType, encoding, "");
                } else {
                    htmlTextView_anw.setTag(listQuestion.get(i).getAskId());
                    htmlTextView_anw.setHtmlFromString("<font color='#808080' style='font-size:15px;'>" + (listQuestion.get(i).getAnswer()) + "</font>", new HtmlTextView.RemoteImageGetter());
                    if (layoutParams_content == null) {
                        layoutParams_content = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, maxHeight);
                        layoutParams_contentAll = (LinearLayout.LayoutParams) htmlTextView_anw.getLayoutParams();
                        layoutParams_content.setMargins(layoutParams_contentAll.leftMargin, layoutParams_contentAll.topMargin, layoutParams_contentAll.rightMargin, 0);
                    }
                    ViewTreeObserver viewTreeObserver = htmlTextView_anw.getViewTreeObserver();
                    viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                        @Override
                        public void onGlobalLayout() {
                            if (htmlTextView_anw.getVisibility() == View.VISIBLE && getQuestionIsShowAllAns((String) htmlTextView_anw.getTag()) == -1) {
                                if (htmlTextView_anw.getHeight() >= maxHeight) {
                                    freshShowAllAnsSta((String) htmlTextView_anw.getTag(), 1);
                                    htmlTextView_anw.setLayoutParams(layoutParams_content);
                                } else {
                                    freshShowAllAnsSta((String) htmlTextView_anw.getTag(), 0);
                                    linearLayout_allContent.setVisibility(View.GONE);
                                }
                            }
                            htmlTextView_anw.getViewTreeObserver().removeGlobalOnLayoutListener(this);//没有此句话，此监听listerner会一直监听，从而影响之后的setLayoutParams
                        }
                    });
                }
            }
            linearLayout_allContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (htmlTextView_anw.getVisibility() == View.VISIBLE) {
                        if (getQuestionIsShowAllAns((String) htmlTextView_anw.getTag()) == 1) {
                            freshShowAllAnsSta((String) htmlTextView_anw.getTag(), 2);
                            htmlTextView_anw.setLayoutParams(layoutParams_contentAll);
                            textView_allContent.setText("收起全文");
                            imageView_allContent.setImageResource(R.drawable.icon_arrow_up);
                        } else {
                            freshShowAllAnsSta((String) htmlTextView_anw.getTag(), 1);
                            htmlTextView_anw.setLayoutParams(layoutParams_content);
                            textView_allContent.setText("查看全文");
                            imageView_allContent.setImageResource(R.drawable.icon_arrow_down);
                        }
                    } else {
                        if (getQuestionIsShowAllAns((String) webView_ans.getTag()) == 1) {
                            freshShowAllAnsSta((String) webView_ans.getTag(), 2);
                            webView_ans.setLayoutParams(layoutParams_contentAll);
                            textView_allContent.setText("收起全文");
                            imageView_allContent.setImageResource(R.drawable.icon_arrow_up);
                        } else {
                            freshShowAllAnsSta((String) webView_ans.getTag(), 1);
                            webView_ans.setLayoutParams(layoutParams_content);
                            textView_allContent.setText("查看全文");
                            imageView_allContent.setImageResource(R.drawable.icon_arrow_down);
                        }
                    }
                }
            });
            linearLayout_body.addView(view);
            if (i != (listQuestion.size() - 1)) {//添加条目间的横隔线
                View view1 = new View(getActivity());
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 2);
                layoutParams.setMargins(0, 15, 0, 0);
                view1.setLayoutParams(layoutParams);
                view1.setBackgroundColor(Color.parseColor("#ececec"));
                linearLayout_body.addView(view1);
            }
        }
    }

    /**
     * 获取当前所要控制的问题 是否显示了全部内容
     * @param id
     * @return
     */
    private int getQuestionIsShowAllAns(String id){
        int isShowAllAns = -2;
        for(int i=0;i<lisMyQuestionObjs.size();i++){
            if(lisMyQuestionObjs.get(i).getAskId().equals(id))
                isShowAllAns = lisMyQuestionObjs.get(i).isShowAllAns();
        }
        return isShowAllAns;
    }

    /**
     * 点击全文或者收起时设置对应题目的显示情况
     * @param id
     * @param status
     */
    private void freshShowAllAnsSta(String id,int status){
        for(int i=0;i<lisMyQuestionObjs.size();i++){
            if(lisMyQuestionObjs.get(i).getAskId().equals(id))
                lisMyQuestionObjs.get(i).setShowAllAns(status);
        }
    }

    /**
     * 显示没有数据的提示
     */
    public void showNoDataHint(){
//        relativeLayout_hint_body.setVisibility(View.VISIBLE);
//        progressBar.setVisibility(View.GONE);
//        linearLayout_pic_body.setVisibility(View.VISIBLE);
//        imageView_hint.setImageResource(R.drawable.my_question_nodata);
//        textView_hint.setVisibility(View.GONE);
        emptyViewLayout.showEmpty();
    }

    @Override
    public void initData() {
        userId = SharedPrefHelper.getInstance(getActivity()).getUserId();
        myQuestionListPersenter.getSectionsBySubjectId();
        getData();
    }

    public void freshData(){
        page = 1;
        isLoadmore = false;
        myQuestionListPersenter.setLoadmore(false);
        getData();
    }

    @Subscribe
    public void onEventAsync(UpdateEvent event) {
        initData();
    }

    @Subscribe
    public void onEventAsync(DeleteQuestionCollection event) {
        updateUI(event.getAskId());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void showData(List<Question> questionList) {
        if(totalPage == page)
            refreshScrollView.setFooterState(ScrollViewFooter.STATE_NOMORE);
        if(questionList.size()>=Constants.PAGESIZE){
            refreshScrollView.setFooterState(ScrollViewFooter.STATE_NORMAL);
        }else{
            refreshScrollView.setFooterState(ScrollViewFooter.STATE_NOMORE);
        }
        this.lisMyQuestionObjs = questionList;
        relativeLayout_hint_body.setVisibility(View.GONE);
        refreshScrollView.stopRefresh();
        if(lisMyQuestionObjs.size()>0)
            freshContentView(lisMyQuestionObjs);
        else
            showNoDataHint();

        EventBus.getDefault().post(new CourseTabTypeEvent(Constants.COURSE_TAB_TYPE_QUESTION));
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showLoadMoreError(String message){
        refreshScrollView.setFooterState(ScrollViewFooter.STATE_NORMAL);
        Toast.makeText(getActivity(),message,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showError(String message) {
        if(emptyViewLayout!=null)
            emptyViewLayout.showError();
//        refreshScrollView.stopRefresh();
//        relativeLayout_hint_body.setVisibility(View.VISIBLE);
//        progressBar.setVisibility(View.GONE);
//        linearLayout_pic_body.setVisibility(View.VISIBLE);
//        imageView_hint.setImageResource(R.drawable.error_pic);
//        textView_hint.setText(message);
    }

    @Override
    public Context context() {
        return getActivity();
    }

    @Override
    public void onRefresh() {
        isLoadmore = false;
        page = 1;
        myQuestionListPersenter.setLoadmore(false);
        getData();
    }

    @Override
    public void onLoadMore() {
        isLoadmore = true;
        page++;
        myQuestionListPersenter.setLoadmore(true);
        getData();
    }

    @Override
    public int getPage() {
        return page;
    }

    @Override
    public void showMoreData(List<Question> questionList) {
        if(totalPage == page)
            refreshScrollView.setFooterState(ScrollViewFooter.STATE_NOMORE);
        if(questionList.size()>=10){
            refreshScrollView.setFooterState(ScrollViewFooter.STATE_NORMAL);
        }else{
            refreshScrollView.setFooterState(ScrollViewFooter.STATE_NOMORE);
        }
        if(questionList.size()>0)
            freshContentView(questionList);
    }


    @Override
    public void bookListJson(String json) {
        bookJson = json;
        if(isShowAnimProgress){
            isShowAnimProgress = false;
            Intent intent = new Intent(getActivity(),AddQuestionActivity.class);
            intent.putExtra("bookList",bookJson);
            getActivity().startActivity(intent);
            dismissAnimProgress();
        }
    }

    @Override
    public View getScrollableView() {
        return view;
    }

    @Override
    public void showNoData() {
        if(emptyViewLayout!=null)
            emptyViewLayout.showEmpty();
    }

    @Override
    public void isCanAsk(String flag) {

    }

    @Override
    public void checkIsCanAskFail() {

    }

    @Override
    public String getQuestionId() {
        return null;
    }

    @Override
    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    private String webContentHandle(String webContent) {
        if(webContent.contains("<img")){
            String resultWebContent = "";
            // 在body中添加显示图片的div容器，原body中的<img />格式图片将会被添加点击事件，点击之后原图片统一会被放入该容器进行全屏显示
            String divContainer = "<div id=\"imageScaler\" style=\"position: fixed; width: 100%; height: 100%; left: 0%; top: 100%; background-color: white; background-image: url(../loading.gif); background-repeat: no-repeat; background-position: center; background-size: inherit;\" onclick=\"javascript: window.backAction();\"></div>";
            resultWebContent = webContent.replace("</body>", divContainer
                    + "\n</body>");
            // 在head中添加图片操作的javascript语句；首先引入一个javascript工具Jquery，用起来方便；接下引入一个javascript变量expanded，用来标记页面是否处于图片全屏显示状态；再接下来引入一个javascript函数backAction，该函数用来做回退操作，退出图片全屏状态或者退出程序（javascript调用java函数）；最后引入一个javascript函数click用来响应全屏或者非全屏状态下图片上的点击事件，若是全屏状态则退出全屏状态，若是非全屏状态则进入全屏状态。
            String js = "<script type=\"text/javascript\" charset=\"utf-8\" src=\"../jquery-1.7.1.min.js\"></script>"
                    + "<script type=\"text/javascript\">"
                    + "var expanded=true;"
                    + "function backAction() { var $imageScaler=$('#imageScaler'); if (expanded!=true) { $imageScaler.animate({ top: '100%' }, 600, function() { expanded=true; $('body').css('overflow', 'auto'); $imageScaler.css('background-size', 'inherit').css('background-image', 'url(../loading.gif)'); }); } else { window.JSInterface.closeApp(); } }"
                    + "function click(obj) { var $imageScaler=$('#imageScaler'); if (expanded==true) { var $obj=$(obj); $imageScaler.animate({ top: '0%' }, 300, function() { expanded=false; $('body').css('overflow', 'hidden'); setTimeout(function() { $imageScaler.css('background-size', 'contain').css('background-image', 'url(' + $obj.attr('src') + ')'); }, 300); }); } else { backAction(); } }"
                    + "</script>";
            resultWebContent = resultWebContent
                    .replace("</head>", js + "\n</head>");
            // 搜索body中的<img />标签，给第一个添加点击事件调用外部java方法showImage()，给第二个添加点击事件调用当前页面的javascript方法click()，
            String prePart = resultWebContent.substring(0, resultWebContent.lastIndexOf("<img"));
            String postPart = resultWebContent.substring(resultWebContent.lastIndexOf("<img"));
            String onClick1 = "onclick=\"javascript: window.JSInterface.showImage(''+"
                    + "$(this).attr('src')" + ");\"";
            prePart = prePart.replace("<img", "<img " + onClick1);
            String finalStr = prePart + postPart;
            return finalStr;
        }else{
            return  webContent;
        }

    }



    /**
     * 当收藏的答疑被删除时，更新收藏按钮状态
     * @param askId
     */
    private void updateUI(final String askId){
        new Thread(){
            @Override
            public void run() {
                for(int i=0;i<imageViewArrayList.size();i++){
                    if(((String)imageViewArrayList.get(i).getTag())!=null && ((String)imageViewArrayList.get(i).getTag()).equals(askId)){
                        Message message = handlerChangeUi.obtainMessage();
                        message.what = 1;
                        message.arg1 = i;
                        handlerChangeUi.sendMessage(message);
                        break;
                    }
                }
            }
        }.start();
    }

}
