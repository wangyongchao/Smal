package com.bjjy.mainclient.phone.view.course;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.alibaba.fastjson.JSON;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseFragment;
import com.bjjy.mainclient.phone.event.DeleteEvent;
import com.bjjy.mainclient.phone.view.course.bean.MCourseBean;
import com.bjjy.mainclient.phone.view.course.bean.MCourseDetailBean;
import com.bjjy.mainclient.phone.view.download.local.bean.YearInfo;
import com.bjjy.mainclient.phone.view.exam.activity.myexam.ExamMineActivity;
import com.bjjy.mainclient.phone.view.main.MainActivity;
import com.bjjy.mainclient.phone.view.play.PlayActivity;
import com.bjjy.mainclient.phone.view.play.adapter.WrapperExpandableListAdapter;
import com.dongao.mainclient.model.local.SharedPrefHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by dell on 2016/5/16.
 */
public class CourseClassFragment extends BaseFragment implements ExpandableListView.OnChildClickListener, CourseWodeExpandAdapter.ExpableListListener {

    @Bind(R.id.cached_ls)
    ExpandableListView cached_ls;
    @Bind(R.id.v_empty)
    View v_empty;

    private View view;
    private MainActivity activity;
    private CourseWodeExpandAdapter expandAdapter;
    private List<MCourseBean> mCourseBeanList=new ArrayList<>();
    private WrapperExpandableListAdapter wrapperAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        view = inflater.inflate(R.layout.cachecourse_fragment, null);
        ButterKnife.bind(this, view);
        activity= (MainActivity) getActivity();
        initView();
        initData();
        return view;
    }

    @Override
    public void initView() {
        expandAdapter=new CourseWodeExpandAdapter(activity,imageLoader);
        expandAdapter.setList(mCourseBeanList);
        wrapperAdapter = new WrapperExpandableListAdapter(expandAdapter);
        cached_ls.setOnChildClickListener(this);
        expandAdapter.setParentClick(this);
        cached_ls.setAdapter(wrapperAdapter);
        openAll();

//        mEmptyLayout = new EmptyViewLayout(getActivity(), ll_content);
//        mEmptyLayout.setErrorButtonClickListener(mErrorClickListener);
//        mEmptyLayout.setEmptyButtonClickListener(emptyButtonClickListener);
    }

    /**
     * 错误监听
     */
    private View.OnClickListener mErrorClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        }
    };
    /**
     * 无数据监听
     */
    private View.OnClickListener emptyButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //showAppMsg("显示kong");
        }
    };


    private void openAll() {
        for (int i = 0; i < expandAdapter.getGroupCount(); i++) {
            cached_ls.expandGroup(i);
        }
    }

    @Override
    public void initData() {
    }
    
    public void setData(List<MCourseBean> list){
        mCourseBeanList.clear();
        mCourseBeanList.addAll(list);
        expandAdapter.notifyDataSetChanged();
        wrapperAdapter.notifyDataSetChanged();
        openAll();
        if (mCourseBeanList.size()==0){
            v_empty.setVisibility(View.VISIBLE);
        }else{
            v_empty.setVisibility(View.GONE);
        }
    }

    private boolean first;
    @Override
    public void onResume() {
        super.onResume();
        if(first){
            initData();
        }
        first=true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.local_notask:
//                downlaod();
                break;
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }


    //onEventAsync
    @Subscribe
    public void onEventMainThread(DeleteEvent event) {
        initData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        try{
            MCourseDetailBean mCourseDetailBean=mCourseBeanList.get(groupPosition).getCourseDetailInfo().get(childPosition);
            String year= SharedPrefHelper.getInstance(getActivity()).getCurYear();
            YearInfo yearInfo= JSON.parseObject(year,YearInfo.class);//无缘无故下标越界
            Intent intent = new Intent(getActivity(), PlayActivity.class);
            String cwCode=mCourseDetailBean.getCwCode();
            intent.putExtra("classId",cwCode);
            if (yearInfo!=null){
                intent.putExtra("subjectId",yearInfo.getYearName());
            }
            intent.putExtra("courseBean", JSON.toJSONString(mCourseDetailBean));
            startActivity(intent);
        }catch (Exception e){
            initData();
        }
        return false;
    }

    @Override
    public void onParentChick(int position) {
        Intent intent = new Intent(getActivity(), ExamMineActivity.class);
        intent.putExtra("ruleId",mCourseBeanList.get(position).getRuleId());
        intent.putExtra("accountId",mCourseBeanList.get(position).getAccountId());
        startActivity(intent);
    }
}
