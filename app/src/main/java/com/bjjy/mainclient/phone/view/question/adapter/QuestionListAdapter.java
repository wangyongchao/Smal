package com.bjjy.mainclient.phone.view.question.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dongao.mainclient.model.bean.question.QuestionRecomm;
import com.bjjy.mainclient.phone.R;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.List;

/**
 * Created by dell on 2016/11/28 0028.
 */
public class QuestionListAdapter extends BaseAdapter{
    private static final String encoding = "utf-8";
    private static final String mimeType = "text/html";
    private Context context;
    private List<QuestionRecomm> questionRecomms;
    public QuestionListAdapter(Context context,List<QuestionRecomm> questionRecomms){
        this.context = context;
        this.questionRecomms = questionRecomms;
    }

    @Override
    public int getCount() {
        return questionRecomms.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.tab_question_fragmrn_lv_item,parent,false);
            viewHolder = new ViewHolder();
            viewHolder.imageView_unread = (ImageView)convertView.findViewById(R.id.tab_question_fragment_lv_item_unread_img);
            viewHolder.textView_title = (TextView)convertView.findViewById(R.id.tab_question_fragment_lv_item_title_tv);
            viewHolder.textView_time = (TextView)convertView.findViewById(R.id.tab_question_fragment_lv_item_time_tv);
            viewHolder.textView_chapter = (TextView)convertView.findViewById(R.id.tab_question_fragment_lv_item_chapter_tv);
            viewHolder.textView_zhui = (TextView)convertView.findViewById(R.id.tab_question_fragment_lv_item_zhui_tv);
            viewHolder.line_two = convertView.findViewById(R.id.tab_question_fragment_lv_item_linetwo);
            viewHolder.textView_content = (HtmlTextView) convertView.findViewById(R.id.tab_question_fragment_lv_item_content_htv);
            viewHolder.webView_content = (WebView) convertView.findViewById(R.id.tab_question_fragment_lv_item_content_wv);
//            viewHolder.textView_content11 = (TextView) convertView.findViewById(R.id.tab_question_fragment_lv_item_content_tv);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder)convertView.getTag();
        }

        if(questionRecomms.get(position).getReadStatus().equals("3") || questionRecomms.get(position).getReadStatus().equals("0")){
            viewHolder.imageView_unread.setVisibility(View.INVISIBLE);
        }else{
            viewHolder.imageView_unread.setVisibility(View.VISIBLE);
        }
        viewHolder.textView_title.setText(questionRecomms.get(position).getTitle());
//        viewHolder.textView_content11.setText(questionRecomms.get(position).getContent());
        if(questionRecomms.get(position).getContent().contains("</td>") || questionRecomms.get(position).getContent().contains("</p>")){
            viewHolder.textView_content.setVisibility(View.VISIBLE);
            viewHolder.webView_content.setVisibility(View.GONE);
            viewHolder.textView_content.setHtmlFromString
                    ("<font color='#808080' style='font-size:15px;'>" + ("请点击查看详情") + "</font>",new HtmlTextView.RemoteImageGetter());
        }else{
            viewHolder.textView_content.setVisibility(View.VISIBLE);
            viewHolder.webView_content.setVisibility(View.GONE);
            viewHolder.textView_content.setHtmlFromString
                    ( "<font color='#808080' style='font-size:15px;'>" + (questionRecomms.get(position).getContent()) + "</font>", new HtmlTextView.RemoteImageGetter());
        }
        if(questionRecomms.get(position).getChildrenNum()>0){
            viewHolder.textView_time.setText(questionRecomms.get(position).getCreateDate() + " | " + questionRecomms.get(position).getKnowledgeNames()
            + " | " +"追问("+questionRecomms.get(position).getChildrenNum()+")");
//            viewHolder.line_two.setVisibility(View.VISIBLE);
//            viewHolder.textView_zhui.setVisibility(View.VISIBLE);
//            viewHolder.textView_zhui.setText("追问("+questionRecomms.get(position).getChildrenNum()+")");
        }else{
            viewHolder.textView_time.setText(questionRecomms.get(position).getCreateDate() + " | " +
                    questionRecomms.get(position).getKnowledgeNames());
//            viewHolder.line_two.setVisibility(View.INVISIBLE);
//            viewHolder.textView_zhui.setVisibility(View.INVISIBLE);
        }

        viewHolder.textView_content.setClickable(false);
        viewHolder.textView_content.setFocusable(false);
        viewHolder.webView_content.setClickable(false);
        viewHolder.webView_content.setFocusable(false);

//        viewHolder.textView_time.setText(questionRecomms.get(position).getCreateDate());
//        viewHolder.textView_chapter.setText(questionRecomms.get(position).getKnowledgeNames());
        return convertView;
    }

    class ViewHolder{
        ImageView imageView_unread;
        TextView textView_title,textView_time,textView_chapter,textView_zhui;
//        TextView textView_content11;
        View line_two;
        HtmlTextView textView_content;
        WebView webView_content;
    }

}
