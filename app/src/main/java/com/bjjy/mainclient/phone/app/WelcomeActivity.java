package com.bjjy.mainclient.phone.app;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.view.main.MainActivity;
import com.bjjy.mainclient.phone.view.setting.update.UpdateManager;
import com.bjjy.mainclient.phone.view.user.LoginNewActivity;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.remote.ApiService;
import com.dongao.mainclient.model.remote.ParamsUtils;

import butterknife.Bind;
import butterknife.ButterKnife;

//import TransDataLoadingActivity;

public class WelcomeActivity extends BaseActivity {

	@Bind(R.id.img_welcome)
	ImageView img_welcome;
	@Bind(R.id.tv_time)
	TextView tvTime;
	@Bind(R.id.welcome)
	LinearLayout welcome;

	private final int MSG_START = 1111;//开启下一页
	private final int MSG_NEW_VERSION = 2222;
	private boolean isCanOpenMain = true;
	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
				case MSG_START:
					gotoMain();
					break;
				case MSG_NEW_VERSION:
					isCanOpenMain = false;
					break;
			}
		}

		;
	};

	private SharedPreferences preferences_update;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.welcome_activity);
		ButterKnife.bind(this);
		preferences_update = getSharedPreferences("Updater",
				Context.MODE_PRIVATE);
		initView();
		initData();
	}

	@Override
	public void initData() {
		ParamsUtils.getInstance(this);
		UpdateManager updateManager = new UpdateManager.Builder(this)
				.checkUrl(ApiService.VERSION_URL).isAutoInstall(true).putMessageTransfer(mHandler).build();
		updateManager.check();

	}

	/**
	 * 初始化
	 */
	@Override
	public void initView() {
		img_welcome.setVisibility(View.VISIBLE);
		tvTime.setText("");
		welcome.setOnClickListener(this);
	}

	private void gotoMain() {
		if (SharedPrefHelper.getInstance(WelcomeActivity.this).isFirstIn()) {
			startActivity(new Intent(WelcomeActivity.this, GuideActivity.class));
		} else {
			if (SharedPrefHelper.getInstance(WelcomeActivity.this).isLogin() &&
					SharedPrefHelper.getInstance(WelcomeActivity.this).isFirstInVersion3()) {
				SharedPrefHelper.getInstance(WelcomeActivity.this).setIsLogin(false);
				SharedPrefHelper.getInstance(WelcomeActivity.this).setUserId("0");
				SharedPrefHelper.getInstance(WelcomeActivity.this).setFirstInVersion3(false);
				Intent intent = new Intent(WelcomeActivity.this, LoginNewActivity.class);
				intent.putExtra(Constants.LOGIN_TYPE_KEY, Constants.LOGIN_TYPE_MAIN);
				startActivity(intent);
			}else{
				SharedPrefHelper.getInstance(WelcomeActivity.this).setFirstInVersion3(false);
				startActivity(new Intent(WelcomeActivity.this, MainActivity.class));
			}
		}
		finish();
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.welcome:
				Intent intent = new Intent(WelcomeActivity.this, MainActivity.class);
				startActivity(intent);
				finish();
				break;
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onRestart() {
		super.onRestart();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}


}
