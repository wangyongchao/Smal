package com.bjjy.mainclient.phone.app;

import android.app.ProgressDialog;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bjjy.mainclient.phone.view.user.utils.UserViewManager;
import com.bjjy.mainclient.phone.R;
import com.nostra13.universalimageloader.core.ImageLoader;

import me.drakeet.materialdialog.MaterialDialog;


public abstract class BaseFragment extends Fragment  implements View.OnClickListener,FragmentUserVisibleController.UserVisibleCallback {


    protected AppContext appContext;
    protected ProgressDialog progress;

    protected UserViewManager userViewManager;
    protected ImageLoader imageLoader = ImageLoader.getInstance();

    protected MaterialDialog materialDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
        initProgress();
        userViewManager = new UserViewManager(getActivity(), new UserViewManager.LoginLinstener() {
            @Override
            public void loginSuccess() {
                loginSuccessed();
            }
        });
    }

    /**
     * 登陆成功后的操作
     */
    protected void loginSuccessed(){

    }

    private void initProgress(){
        progress=new ProgressDialog(getActivity());
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setMessage("正在加载…………");

    }

    /**
     * 显示动画的加载中
     * @param loadingtv
     */
    protected void showAnimProgress(String loadingtv){
        materialDialog = new MaterialDialog(getActivity());
        materialDialog.setCanceledOnTouchOutside(false);
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.app_view_loading,null);
        TextView tv = (TextView) view.findViewById(R.id.app_loading_tv);
        ImageView imageView = (ImageView)view.findViewById(R.id.empty_layout_loading_img);
        AnimationDrawable animationDrawable = (AnimationDrawable)imageView.getBackground();
        animationDrawable.start();
        tv.setText(loadingtv);
        materialDialog.setContentView(view);
    }

    protected void dismissAnimProgress(){
        if(materialDialog!=null)
            materialDialog.dismiss();
    }

    // 初始化
    private void init() {
        // 添加Activity到堆栈
       // AppManager.getAppManager().addActivity(getActivity());
        appContext = (AppContext) AppContext.getApp().getApplicationContext();
    }


    public abstract void initView();

    public abstract void initData();

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        // 结束Activity&从堆栈中移除
//        AppManager.getAppManager().finishActivity(getActivity());
        super.onDestroy();
    }

    private FragmentUserVisibleController userVisibleController;

    public BaseFragment() {
        userVisibleController = new FragmentUserVisibleController(this, this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        userVisibleController.activityCreated();
    }

    @Override
    public void onResume() {
        super.onResume();
        userVisibleController.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        userVisibleController.pause();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        userVisibleController.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    public void setWaitingShowToUser(boolean waitingShowToUser) {
        userVisibleController.setWaitingShowToUser(waitingShowToUser);
    }

    @Override
    public boolean isWaitingShowToUser() {
        return userVisibleController.isWaitingShowToUser();
    }

    @Override
    public boolean isVisibleToUser() {
        return userVisibleController.isVisibleToUser();
    }

    @Override
    public void callSuperSetUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    public void onVisibleToUserChanged(boolean isVisibleToUser, boolean invokeInResumeOrPause) {

    }


}
