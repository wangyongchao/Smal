package com.bjjy.mainclient.phone.view.main;

import java.io.Serializable;
import java.util.List;

public class UserRecommenJson implements Serializable{

   private List<MessageValue> list;

    public List<MessageValue> getList() {
        return list;
    }

    public void setList(List<MessageValue> list) {
        this.list = list;
    }
}
