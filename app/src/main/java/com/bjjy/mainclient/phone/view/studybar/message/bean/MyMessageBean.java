package com.bjjy.mainclient.phone.view.studybar.message.bean;

/**
 * Created by wyc on 2016/6/6.
 */
public class MyMessageBean {
    private String id;//公告ID
    private String content; //内容
    private String createDate; //公告时间
    private String title;//公告标题

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
}
