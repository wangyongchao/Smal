package com.bjjy.mainclient.phone.view.exam.activity.course;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bjjy.mainclient.phone.app.BaseFragmentActivity;
import com.bjjy.mainclient.phone.view.exam.activity.course.adapter.CourseMoreAdapter;
import com.bjjy.mainclient.phone.widget.EmptyViewLayout;
import com.bjjy.mainclient.phone.widget.pulltorefresh.PullToRefreshBase;
import com.dongao.mainclient.core.camera.bean.ImageLoaderConfig;
import com.dongao.mainclient.core.camera.constants.Constants;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.widget.pulltorefresh.PullToRefreshExpandableListView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by wyc on 2016/5/11.
 */
public class CourseMoreActivity extends BaseFragmentActivity implements PullToRefreshBase.OnRefreshListener<ExpandableListView>, ExpandableListView.OnChildClickListener, CourseMoreView{
    @Bind(R.id.top_title_left)
    ImageView top_title_left;
    @Bind(R.id.examprictice_expanble_lv)
    PullToRefreshExpandableListView examprictice_expanble_lv;
    @Bind(R.id.top_title_right)
    ImageView top_title_right;
    @Bind(R.id.top_title_text)
    TextView top_title_text;
    private EmptyViewLayout mEmptyLayout;
    private CourseMorePercenter courseMorePercenter;
    private CourseMoreAdapter courseMoreAdapter;

    @OnClick(R.id.top_title_left) void onBack(){
        onBackPressed();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.course_more_activity);
        ButterKnife.bind(this);
        ImageLoaderConfig.initImageLoader(this, Constants.BASE_IMAGE_CACHE);
        courseMorePercenter=new CourseMorePercenter();
        courseMorePercenter.attachView(this);
        initView();
        initData();
    }

    @Override
    public void initView() {
        top_title_left.setVisibility(View.VISIBLE);
        top_title_text.setText(getResources().getString(R.string.exam_prictice));
        examprictice_expanble_lv.setScrollingWhileRefreshingEnabled(true);
        examprictice_expanble_lv.getRefreshableView().setGroupIndicator(null);
        examprictice_expanble_lv.getRefreshableView().setOnChildClickListener(this);
        mEmptyLayout = new EmptyViewLayout(this, examprictice_expanble_lv);
//        ViewGroup viewGroup= (ViewGroup) LayoutInflater.from(this).inflate(R.layout.app_view_error_dongao,null);
//        ViewGroup dataError= (ViewGroup) LayoutInflater.from(this).inflate(R.layout.app_view_data_empty_dongao,null);
//        mEmptyLayout.setNetErrorView(viewGroup);
//        mEmptyLayout.setDataEmptyView(dataError);
        mEmptyLayout.setErrorButtonClickListener(mErrorClickListener);
        mEmptyLayout.setEmptyButtonClickListener(emptyButtonClickListener);
        mEmptyLayout.setEmptyMessage("");
        mEmptyLayout.setShowEmptyButton(false);
        initContrl();
    }
    

    @Override
    public void initData() {
        courseMoreAdapter = new CourseMoreAdapter(this,  courseMorePercenter.courseList, true);
        examprictice_expanble_lv.getRefreshableView().setAdapter(courseMoreAdapter);
        courseMorePercenter.getData();
        setAllCollapsed();
    }

    @Override
    public void onClick(View v) {

    }

    /**
     * 默认打开全部
     */
    public void setAllCollapsed(){
        for(int i = 0; i < courseMoreAdapter.getGroupCount(); i++){
            examprictice_expanble_lv.getRefreshableView().expandGroup(i);
        }
        examprictice_expanble_lv.getRefreshableView().setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                return true;
            }
        });
    }

    @Override
    public EmptyViewLayout getEmptyLayout() {
        return mEmptyLayout;
    }

    @Override
    public PullToRefreshExpandableListView getPTREListView() {
        return examprictice_expanble_lv;
    }

    @Override
    public void initAdapter() {
        courseMoreAdapter = new CourseMoreAdapter(this,  courseMorePercenter.courseList, true);
        examprictice_expanble_lv.getRefreshableView().setAdapter(courseMoreAdapter);
 
    }

    /**
     * 控制expandListview的刷新监听
     */
    public void initContrl() {
        // TODO Auto-generated method stub
        examprictice_expanble_lv.getRefreshableView().setOnChildClickListener(this);
        examprictice_expanble_lv.setOnRefreshListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        return false;
    }

    @Override
    public void onRefresh(PullToRefreshBase<ExpandableListView> refreshView) {
        courseMorePercenter.getData();
    }

    @Override
    public void showLoading() {
        
    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public Context context() {
        return CourseMoreActivity.this;
    }

    /**
     * 错误监听
     */
    private View.OnClickListener mErrorClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        }
    };
    /**
     * 无数据监听
     */
    private View.OnClickListener emptyButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //showAppMsg("显示kong");
        }
    };


    @Override
    public void refreshAdapter() {
        courseMoreAdapter.notifyDataSetChanged();
    }
    
}
