package com.bjjy.mainclient.phone.view.play.fragment;

import com.dongao.mainclient.model.mvp.MvpView;

import java.util.List;

/**
 * Created by fengzongwei on 2016/5/30 0030.
 */
public interface FragmentPlayView extends MvpView{
    void showData(List<Integer> times,List<String> urls);
    String getPath();
    void setResult(int code);
    void playError(String msg);
}
