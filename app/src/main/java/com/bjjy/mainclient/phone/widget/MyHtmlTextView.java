package com.bjjy.mainclient.phone.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import org.sufficientlysecure.htmltextview.HtmlTextView;

/**
 * Created by dell on 2016/12/22 0022.
 */

public class MyHtmlTextView extends HtmlTextView{

    public MyHtmlTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public MyHtmlTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyHtmlTextView(Context context) {
        super(context);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }
}
