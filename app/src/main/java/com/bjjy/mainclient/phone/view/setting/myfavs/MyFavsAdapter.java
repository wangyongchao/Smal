package com.bjjy.mainclient.phone.view.setting.myfavs;

import android.content.Context;
import android.text.Html;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dongao.mainclient.core.util.SystemUtils;
import com.dongao.mainclient.model.bean.user.MyCollection;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.bjjy.mainclient.phone.R;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by dell on 2016/5/16.
 */
public class MyFavsAdapter extends BaseAdapter {

    private Context mContext;
    private List<MyCollection> mList;

    public MyFavsAdapter(Context context) {
        super();
        this.mContext = context;
    }

    public void setList(List<MyCollection> list) {
        this.mList = list;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = View.inflate(mContext, R.layout.myfavs_item, null);
            holder.title = (TextView) convertView.findViewById(R.id.myfavs_item_title_tv);
            holder.time = (TextView) convertView.findViewById(R.id.myfavs_item_time);
            holder.imgTop = (ImageView) convertView.findViewById(R.id.myfavs_item_top_img);
            holder.imgBottom = (ImageView) convertView.findViewById(R.id.myfavs_item_bottom_img);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        String type = mList.get(position).getType();
        if (type.equals("1")) {
            holder.imgTop.setImageResource(R.drawable.favs_writ);
        } else if (type.equals("2")) {
            holder.imgTop.setImageResource(R.drawable.favs_quest);
        } else if (type.equals("3")) {
            holder.imgTop.setImageResource(R.drawable.favs_play);
        } else if (type.equals("4")) {
            holder.imgTop.setImageResource(R.drawable.favs_read);
        }
        String title=mList.get(position).getTitle();
//        holder.title.setHtmlFromString("<font color='#808080' style='font-size:18px;'>" + (title) + "</font>",new HtmlTextView.RemoteImageGetter());

        holder.title.setText(Html.fromHtml(title));
//        holder.title.setText(title);
//        holder.title.loadDataWithBaseURL("", "<font color='#808080' style='font-size:18px;'>" + (title) + "</font>", "text/html", "UTF-8", "");

        long now = System.currentTimeMillis();
        long time = Long.parseLong(mList.get(position).getTime());

        Date datePre = new Date(time);
        Date dateNow = new Date(now);
        int day = SystemUtils.getBetweenDay(datePre, dateNow);
        if (day == 0) {
            holder.time.setText("今天");
        } else if (day == 1) {
            holder.time.setText("昨天");
        } else if (day >= 2 && day < 8) {
            holder.time.setText(day + "天前");
        } else if (day >= 8) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
            holder.time.setText(sdf.format(datePre));
        }
        return convertView;
    }

    class ViewHolder {
        TextView title;
        TextView time;
        ImageView imgTop;
        ImageView imgBottom;
    }
}
