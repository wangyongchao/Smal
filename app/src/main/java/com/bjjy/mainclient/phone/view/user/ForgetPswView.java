package com.bjjy.mainclient.phone.view.user;

import com.dongao.mainclient.model.mvp.MvpView;

/**
 * Created by fengzongwei on 2016/10/19 0019.
 */
public interface ForgetPswView extends MvpView{
    void switchBtStatus();//切换获取验证码按钮的状态
    void resetSuccess();//密码修改成功
    String phoneNumber();//手机号
    String valideCode();//验证码
    String newPsw();//新密码
}
