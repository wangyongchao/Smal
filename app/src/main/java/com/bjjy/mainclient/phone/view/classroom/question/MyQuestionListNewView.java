package com.bjjy.mainclient.phone.view.classroom.question;

import com.dongao.mainclient.model.bean.question.QuestionRecomm;
import com.dongao.mainclient.model.mvp.MvpView;

import java.util.List;

/**
 * Created by dell on 2016/12/1 0001.
 */
public interface MyQuestionListNewView extends MvpView {
    void showMyQuesList(List<QuestionRecomm> questionRecomms);
    void showMoreList(List<QuestionRecomm> questionRecomms);
    int getPage();
    int pageSize();
    void loadMoreFail();
    void showNoData();
    void showNetError();
}
