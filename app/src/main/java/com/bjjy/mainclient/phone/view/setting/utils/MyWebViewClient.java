package com.bjjy.mainclient.phone.view.setting.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.dongao.mainclient.core.webview.WVJBWebViewClient;
import com.bjjy.mainclient.phone.view.main.UserBean;
import com.bjjy.mainclient.phone.widget.EmptyViewLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by fengzongwei on 2015/8/21.
 */
public class MyWebViewClient extends WVJBWebViewClient {

    private ProgressBar progressBar;
    private Context context;
    ArrayList<String> loadHistoryUrls;
    private EmptyViewLayout mEmptyViewLayout;

    public MyWebViewClient(WebView webView, final Context context, EmptyViewLayout mEmptyViewLayout, ArrayList<String> loadHistoryUrls) {
        // support js send
        // support js send
        super(webView, new WVJBWebViewClient.WVJBHandler() {
            @Override
            public void request(Object data, WVJBResponseCallback callback) {
                //Toast.makeText(getActivity(), "Android Received message from JS:" + data, Toast.LENGTH_LONG).show();
                callback.callback("Responsessss for message from Android!");
            }
        });

          /*  // not support js send
            registerHandler("testAndroidCallback", new WVJBWebViewClient.WVJBHandler() {
                @Override
                public void request(Object data, WVJBResponseCallback callback) {
                    Toast.makeText(getActivity(), "testAndroidCallback called:" + data, Toast.LENGTH_LONG).show();
                    callback.callback("Response from testAndroidCallback!");
                }
            });*/

        // not support js send
        registerHandler("returnCommonParams", new WVJBWebViewClient.WVJBHandler() {
            @Override
            public void request(Object data, WVJBResponseCallback callback) {
                UserBean userBean = new UserBean();
                userBean.setType("app");
                userBean.setUserId("21878075");
                String json = JSON.toJSONString(userBean);
                Toast.makeText(context, "Android got response! :" + json, Toast.LENGTH_LONG).show();
                callback.callback(json);
            }
        });

        // not support js send
        registerHandler("payTableViewController", new WVJBWebViewClient.WVJBHandler() {
            @Override
            public void request(Object data, WVJBResponseCallback callback) {
                Toast.makeText(context, "Android got response! :" + data, Toast.LENGTH_LONG).show();
                callback.callback("response payTableViewController");
            }
        });

        try {
            callHandler("getJSRegisterInfoHandler",
                    new JSONObject("{\"userId\":\"21878075\",\"type\":\"app\" }"),
                    new WVJBResponseCallback() {
                        @Override
                        public void callback(Object data) {
                            Toast.makeText(context, "Android call testJavascriptHandler got response! :" + data, Toast.LENGTH_LONG).show();
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }

        this.mEmptyViewLayout = mEmptyViewLayout;
        this.context = context;
        this.loadHistoryUrls = loadHistoryUrls;
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (url.startsWith("tel:")) {
            Intent intent = new Intent(Intent.ACTION_DIAL,
                    Uri.parse(url));
            context.startActivity(intent);
        }else if(url.startsWith("http:") || url.startsWith("https:")) {
            view.loadUrl(url);
        }
        loadHistoryUrls.add(url);
        return true;
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
       // progressBar.setVisibility(View.VISIBLE);
        mEmptyViewLayout.showLoading();
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
       // progressBar.setVisibility(View.GONE);
        mEmptyViewLayout.showContentView();
    }

}
