package com.bjjy.mainclient.phone.view.classroom.course;


import com.alibaba.fastjson.JSON;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.bean.course.CourseAnswer;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ParamsUtils;
import com.dongao.mainclient.model.remote.bean.BaseBean;


/**
 * 课堂首页Persenter
 */
public class CourseFragmentsPersenter extends BasePersenter<CourseFragmentsView> {
    private CourseAnswer courseJson ;

    @Override
    public void attachView(CourseFragmentsView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    @Override
    public void getData(){
        //一些业务逻辑的判断 有网没网 是请求网络还是请求数据库
        apiModel.getData(ApiClient.getClient().courseAnswerNum(
                ParamsUtils.getInstance(getMvpView().context()).courseAnswerNum()));
    }

    @Override
    public void setData(String obj) {
        try {
            BaseBean baseBean = JSON.parseObject(obj,BaseBean.class);
            if(baseBean == null){
                getMvpView().showError("");
                return;
            }
            if(baseBean.getCode()==1000){
                courseJson = JSON.parseObject(baseBean.getBody(),CourseAnswer.class);
                getMvpView().setView(courseJson,true);
            }else{
                getMvpView().showError("");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
