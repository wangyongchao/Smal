package com.bjjy.mainclient.phone.view.play.fragment.uploadBean;

import java.io.Serializable;

/**
 * Created by dell on 2017/11/18.
 */

public class CourseVideoBean implements Serializable{
	private String videoID;
	private long listenTime;
	private long nowPlayingTime;
	private String createdTime;
	private String lastUpdateTime;

	public String getVideoID() {
		return videoID;
	}

	public void setVideoID(String videoID) {
		this.videoID = videoID;
	}

	public long getListenTime() {
		return listenTime;
	}

	public void setListenTime(long listenTime) {
		this.listenTime = listenTime;
	}

	public long getNowPlayingTime() {
		return nowPlayingTime;
	}

	public void setNowPlayingTime(long nowPlayingTime) {
		this.nowPlayingTime = nowPlayingTime;
	}

	public String getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}

	public String getLastUpdateTime() {
		return lastUpdateTime;
	}

	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	
	
}
