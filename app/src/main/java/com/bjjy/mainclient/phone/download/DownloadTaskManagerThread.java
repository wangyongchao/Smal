package com.bjjy.mainclient.phone.download;

import android.content.Context;

import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.bjjy.mainclient.phone.app.AppConfig;
import com.bjjy.mainclient.phone.app.AppContext;
import com.bjjy.mainclient.phone.download.db.DownloadDB;
import com.bjjy.mainclient.phone.utils.NetworkUtil;

public class DownloadTaskManagerThread implements Runnable {

	private DownloadTaskManager downloadTaskManager;
	DownloadDB db;
	DownloadExcutor excutor;
	// 轮询时间
	private final int SLEEP_TIME = 1000;
	// 是否停止
	private boolean isStop = false;
	private Context context;
	private DownloadTask downloadTask;

	public DownloadTaskManagerThread(Context context) {
		this.context = context;
		db = new DownloadDB(context);
		AppContext.getInstance().isStart = 1;
		downloadTaskManager = DownloadTaskManager.getInstance();
		excutor = DownloadExcutor.getInstance(context);
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		while (!isStop) {
			try {
				if(AppContext.getInstance().isStart==1){
					if (NetworkUtil.isNetworkAvailable(context)) { //判断有网之后，再判断是否为3g，如果是，提示用户时候继续下载
						downloadTask = downloadTaskManager.getDownloadTask();
						if (downloadTask != null) {
							System.out.println("开始下载");
							int state = db.getState(SharedPrefHelper.getInstance(context).getUserId(),downloadTask.getCourseId(),downloadTask.getCourseWareId());
							if (state == Constants.STATE_Waiting) {
								int effect= db.updateState(SharedPrefHelper.getInstance(context).getUserId(),downloadTask.getCourseId(),downloadTask.getCourseWareId(),
										Constants.STATE_DownLoading);// 此处更新adapter
								if(effect!=0){
									if (AppContext.getInstance().getHandler() != null) {
										AppContext.getInstance().getHandler()
												.sendEmptyMessage(0);
									}
									if (AppContext.getInstance().getHandlerDetail() != null) {
										AppContext.getInstance().getHandlerDetail()
												.sendEmptyMessage(0);
									}
									excutor.download(downloadTask);
								}else{
									db.updateState(SharedPrefHelper.getInstance(context).getUserId(),downloadTask.getCourseId(),downloadTask.getCourseWareId(),
											Constants.STATE_Pause);
									if (AppContext.getInstance().getHandler() != null) {
										AppContext.getInstance().getHandler()
												.sendEmptyMessage(0);
									}
									if (AppContext.getInstance().getHandlerDetail() != null) {
										AppContext.getInstance().getHandlerDetail()
												.sendEmptyMessage(0);
									}
								}
							}
						}
//					else { // 如果当前未有downloadTask在任务队列中
//						try {
//							// 查询任务完成失败的,重新加载任务队列
//							// 轮询,
//							Thread.sleep(SLEEP_TIME);
//						} catch (InterruptedException e) {
//							// TODO Auto-generated catch block
//							e.printStackTrace();
//						}
//						System.out.println("没有下载内容");
//					}
					} else {
						// 这里要添加网络判断**************（如果没有网络，所有下载状态更新成暂停，除了下载完成的和下载出错,下载线程停止）
						db.updateState(Constants.STATE_Pause);
						if (AppContext.getInstance().getHandler() != null) {
							AppContext.getInstance().getHandler()
									.sendEmptyMessage(0);
						}
						if (AppContext.getInstance().getHandlerDetail() != null) {
							AppContext.getInstance().getHandlerDetail()
									.sendEmptyMessage(0);
						}
//						isStop=true;
						AppContext.getInstance().isStart=0;
						excutor.setFlag(false);
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				System.out.println("thread——error*******************");
				e.printStackTrace();
			}
		}
		
		if (isStop) {
			excutor.setFlag(false);
		}
		AppConfig.getAppConfig(context).downloadTaskManagerThread=null;
		System.out.println("stop*******************");

	}

	/**
	 * @param isStop
	 *            the isStop to set
	 */
	public void setStop(boolean isStop) {
		this.isStop = isStop;
	}
	
	//抛异常是否正确下载
	//404 503处理
	//无网络处理
	//请求失败  重复5次
	//Constants.isStart,isStop合并
}
