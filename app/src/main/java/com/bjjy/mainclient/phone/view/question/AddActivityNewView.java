package com.bjjy.mainclient.phone.view.question;

import android.os.Bundle;

import com.dongao.mainclient.model.mvp.MvpView;

/**
 * Created by fengzongwei on 2016/12/7 0007.
 */
public interface AddActivityNewView extends MvpView {
    void addSuccess();//新增问题成功
    Bundle getIntentBundle();//获取前一个页面传递过来的bundle对象
    int type();//新增问题类型 （0图书，1试题）
    String title();
    String content();
}
