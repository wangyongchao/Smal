package com.bjjy.mainclient.phone.view.zb.utils;

import android.content.Context;
import android.widget.ImageView;

import com.bjjy.mainclient.phone.R;
import com.bumptech.glide.Glide;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by dell
 * on 2017/12/26.
 */

public class GlideUtils {

	public static void loadImage(Context context, ImageView imageView, String url) {
		Glide.with(context)
				.load(url)
//				.centerCrop()
				.crossFade()
				.placeholder(R.drawable.default_img)
				.into(imageView);
	}
	public static void loadUserImage(Context context, ImageView imageView, String url) {
		Glide.with(context)
				.load(url)
				.bitmapTransform(new CropCircleTransformation(context))
				.centerCrop()
				.crossFade()
				.placeholder(R.drawable.persenal_logined)
				.into(imageView)
				;
	}
}
