package com.bjjy.mainclient.phone.intent;

/**
 * 所有静态参数Action
 */
public class Constants {

    public final static String intent_rule_1 =  "intent:#Intent;component=com.dongao.mainclient.phone/.view.TestActivity;end";
    public final static String intent_rule_2 =  "intent:#Intent;component=com.dongao.mainclient.phone/.view.TestActivity;B.flag=true;S.name=测试跳转;i.code=13214;end";
    public final static String intent_rule_3 =  "http://www.bjsteach.com";

}
