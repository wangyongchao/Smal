package com.bjjy.mainclient.phone.view.play.utils;

import android.os.Handler;

import com.dongao.mainclient.core.util.FileUtil;
import com.bjjy.mainclient.phone.app.AppContext;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by fengzongwei on 2016/5/9 0009.
 * 播放视频前加解密准备中心
 */
public class PrePlayVideoCenter {
    private AppContext appContext;
    private PrePlayListener prePlayListener;
    private DownLoadUtils downLoadUtils;
    private String videoId;
    private String m3u8Url;
    private static byte[] intaa;

    private DownLoadUtils.DownLoadCallBack callBack = new DownLoadUtils.DownLoadCallBack() {
        @Override
        public void onDownLoadFinished() {
            getKeyTxt(Constans.M3U8_URL_HEAD,FileUtil.getDownloadPath(appContext)+"/"+videoId+".m3u8");
        }

        @Override
        public void onDownLoadError() {
            prePlayListener.preFailed();
        }
    };

    private Handler handler = new Handler(){
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case 1:
                    if(prePlayListener!=null)
                        prePlayListener.preSuccess(DownLoadUtils.getDownLoadedM3u8FilePath(videoId));
                    break;
                case 2:
                    prePlayListener.preSuccess(m3u8Url);
                    break;
                case 3:
                    prePlayListener.preFailed();
                    break;
            }
        };
    };

    /**
     *
     * @param appContext
     */
    public PrePlayVideoCenter(AppContext appContext,PrePlayListener prePlayListener){
        this.appContext = appContext;
        this.prePlayListener = prePlayListener;
    }

    /**
     * 进行播放前的准备工作
     */
    public void prePlay(String videoId,String m3u8Url){
        this.videoId = videoId;
        this.m3u8Url = m3u8Url;
        downLoadUtils = new DownLoadUtils(appContext, callBack);
        downLoadUtils.downM3u8ToFile(videoId, m3u8Url);
//        downLoadUtils.downM3u8ToFile(videoId, Constans.M3U8_URL_OFF);
    }

    /**
     * 获取并保存Key到文件中
     * @param urlHead
     */
    public  void getKeyTxt(final String urlHead,final String m3u8Path){// TODO: 2017/11/7 22.36 
        new Thread(){
            @Override
            public void run() {
                try {
                    String keyUrl = null;
                    FileOutputStream fos = null;
                    File m3u8File = new File(m3u8Path);
                    FileReader fileReader = new FileReader(m3u8File);
                    BufferedReader reader11 = new BufferedReader(fileReader);
                    String line1 = null;
                    StringBuffer stringBuffer1 = new StringBuffer();
                    String line_copy = null;
                    boolean isNeedKey = false;//是否需要进行加解密
                    while((line1 = reader11.readLine())!=null){
                        line_copy = line1;
                        if(line1.contains("EXT-X-KEY"))
                            isNeedKey = true;
                        if(line1.contains("EXT-X-KEY") && !line1.contains(Constans.M3U8_KEY_SUB)){
                            //如果当前视频需要进行解密，则修改m3u8文件中EXT-X-KEY对应的值指向本地key文件，来标示需要进行解密
                            String[] keyPart = line1.split(",");
                            String keyUrll = keyPart[1].split("URI=")[1].trim();
                            keyUrl = keyUrll.substring(1,keyUrll.length()-1);
//                            line_copy = keyPart[0] + Constans.M3U8_KEY_SUB + videoId + "_keyText.txt\"," + keyPart[2];// TODO: 2017/11/10  
                            line_copy = keyPart[0] + Constans.M3U8_KEY_SUB + videoId + "_keyText.txt\"";

                        }else if(line1.contains(".ts") && !line1.contains("http:") && isNeedKey){
                            line_copy = urlHead + line_copy;//补全ts的网络路径
                        }
                        stringBuffer1.append(line_copy);
                        stringBuffer1.append("\r\n");
                    }
                    reader11.close();
                    FileWriter writer = new FileWriter(m3u8File,false);
                    BufferedWriter writer2 = new BufferedWriter(writer);
                    writer2.write(stringBuffer1.toString());
                    writer2.flush();
                    writer2.close();
                    //"http://172.16.200.244/interface/drm.php?vid=video05"
                    if(isNeedKey){
                        URL url = new URL(keyUrl);
                        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                        connection.setRequestMethod("GET");
                        connection.setDoInput(true);
                        connection.setUseCaches(false);
                        String useragent = SignUtils.getUserAgent(SharedPrefHelper.getInstance(appContext).getUserId());
                        connection.addRequestProperty("user-agent",useragent);
                        connection.setConnectTimeout(10000);
                        connection.connect();
                        int responseCode = connection.getResponseCode();
                        if(responseCode == 200){
                            InputStream inputStream = connection.getInputStream();
                            intaa = input2byte(inputStream);
                            File file = new File(FileUtil.getDownloadPath(appContext) + videoId + "_keyText.txt");
                            if(!file.exists())
                                file.createNewFile();
                            fos = new FileOutputStream(file);
                            appContext.setEntryData(intaa);
                            fos.write("dongao".getBytes());
                            fos.close();
                        }
                    }
                    if(isNeedKey)
                        handler.sendEmptyMessage(1);
                    else
                        handler.sendEmptyMessage(2);
                } catch (Exception e) {
                    e.printStackTrace();
                    handler.sendEmptyMessage(3);
                }
            }
        }.start();
    }

    private final byte[] input2byte(InputStream inStream)
            throws IOException {
        ByteArrayOutputStream swapStream = new ByteArrayOutputStream();
        byte[] buff = new byte[100];
        int rc = 0;
        while ((rc = inStream.read(buff, 0, 100)) > 0) {
            swapStream.write(buff, 0, rc);
        }
        byte[] in2b = swapStream.toByteArray();
        return in2b;
    }

    public interface  PrePlayListener{
        /**
         * 准备成功，可以进行播放
         * @param vedioPath 可以直接进行播放的视频地址
         */
        void preSuccess(String vedioPath);

        /**
         * 准备失败，无法进行后续的播放
         */
        void preFailed();
    }

}
