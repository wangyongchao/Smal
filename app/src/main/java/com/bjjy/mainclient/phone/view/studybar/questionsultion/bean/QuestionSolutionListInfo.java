package com.bjjy.mainclient.phone.view.studybar.questionsultion.bean;

import java.util.List;

/**
 * Created by wyc on 2016/6/6.
 */
public class QuestionSolutionListInfo {
    private List<QuestionSolution> qasList;//公告标题
    private int totalSize;//总页数

    public List<QuestionSolution> getQasList() {
        return qasList;
    }

    public void setQasList(List<QuestionSolution> qasList) {
        this.qasList = qasList;
    }

    public int getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(int totalSize) {
        this.totalSize = totalSize;
    }
}
