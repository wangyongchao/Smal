package com.bjjy.mainclient.phone.view.payment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;

import com.alipay.sdk.app.PayTask;
import com.dongao.mainclient.core.payment.payutils.zfbutils.PayResult;
import com.tencent.mm.sdk.modelpay.PayReq;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

import java.util.Map;

public class Pay {
	private static final int SDK_PAY_FLAG = 1;
	private Context context;

	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
				case SDK_PAY_FLAG: {
					PayResult payResult = new PayResult((String) msg.obj);

					// 支付宝返回此次支付结果及加签，建议对支付宝签名信息拿签约时支付宝提供的公钥做验签
					String resultInfo = payResult.getResult();

					String resultStatus = payResult.getResultStatus();
					Intent intent=new Intent(context, PayEntryActivity.class);
					intent.putExtra("resultStatus",resultStatus);
					context.startActivity(intent);
					// 判断resultStatus 为“9000”则代表支付成功，具体状态码代表含义可参考接口文档
//					if (TextUtils.equals(resultStatus, "9000")) {
//						Toast.makeText(context, "支付成功",
//								Toast.LENGTH_SHORT).show();
//						Intent intent=new Intent(context, PayEntryActivity.class);
//						context.startActivity(intent);
//					} else {
//						// 判断resultStatus 为非“9000”则代表可能支付失败
//						// “8000”代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
//						if (TextUtils.equals(resultStatus, "8000")) {
//							Toast.makeText(context, "支付结果确认中",
//									Toast.LENGTH_SHORT).show();
//
//						} else {
//							// 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
//							Toast.makeText(context, "支付失败",
//									Toast.LENGTH_SHORT).show();
//							
//						}
//						((PayWayActivity)context).hideLoading();
//					}
					break;
				}
				default:
					break;
			}
		};
	};

	/**
	 * 微信支付
	 * @param context
	 * @param orderInfo
	 */
	public void payByWx(Context context,Map<String,String> orderInfo){
		this.context=context;
		IWXAPI msgApi = WXAPIFactory.createWXAPI(context, null);
		PayReq req = new PayReq();
		req=getWxBean(orderInfo);
//		msgApi.registerApp(Constants.APP_ID);
		msgApi.registerApp(orderInfo.get("appId"));
		msgApi.sendReq(req);
	}
	
	

	public void payByZfb(final Context context, final String mapInfo){
		this.context=context;
		// 订单
//		String orderInfo = getOrderInfo("测试的商品", "该测试商品的详细描述", "0.01",mapInfo,orderNumber);


		// 完整的符合支付宝参数规范的订单信息
		/*final String payInfo = orderInfo + "&sign=\"" + mapInfo.get("sign") + "\"&"
				+ getSignType();*/

		Runnable payRunnable = new Runnable() {

			@Override
			public void run() {
				// 构造PayTask 对象
				PayTask alipay = new PayTask((Activity) context);
				// 调用支付接口，获取支付结果
				String result = alipay.pay(mapInfo,true);
//				String apInfo="{\"payParams\":\"_input_charset=utf-8&body=dongao&notify_url=http:\\/\\/\n" +
//						"114.255.153.236\\/ma\\/disposeAppAlipayNotify.html&out_trade_no=5245039&partner=2088201768879583&payment_type=1&seller_id=alipay@bjsteach.com&service=mobile.securitypay.pay&sign=WbmWDSSEneLS9HHUKWr0mOcJx6JUOBy%2Frpe5NUhh5sPw%2BmIiAb8h6cG75cK89UX2%2BfLPfSMAKaudGlPsQXBESK0XPFqLaZ%2Bgb6NJbWXon0tcZ8StdOKtjaoP%2BhB9cjTWfYhUFCSfIxito1CAHietZoreWsKy8VhHl0qJSLeOtmU%3D&sign_type=\"RSA\"&subject=dongao&total_fee=180.00\"";
				Message msg = new Message();
				msg.what = SDK_PAY_FLAG;
				msg.obj = result;
				mHandler.sendMessage(msg);
			}
		};

		// 必须异步调用
		Thread payThread = new Thread(payRunnable);
		payThread.start();
	}


	private PayReq getWxBean(Map<String,String> orderInfo){
		PayReq req = new PayReq();
		req.nonceStr=orderInfo.get("nonceStr");
		req.timeStamp=orderInfo.get("timeStamp");
		req.appId=orderInfo.get("appId");
		req.packageValue=orderInfo.get("packageValue");
		req.sign=orderInfo.get("sign");
		req.prepayId=orderInfo.get("prepayId");
		req.partnerId=orderInfo.get("partnerId");
		return req;
	}

	/**
	 * create the order info. 创建订单信息
	 *
	 */
	public String getOrderInfo(String subject, String body, String price,Map<String,String> mapInfo,String orderNumber) {

		// 签约合作者身份ID
		String orderInfo = "partner=" + "\"" + mapInfo.get("partnerid") + "\"";

		// 签约卖家支付宝账号
		orderInfo += "&seller_id=" + "\"" + mapInfo.get("appid") + "\"";

		// 商户网站唯一订单号
		orderInfo += "&out_trade_no=" + "\"" +orderNumber + "\"";

		// 商品名称
		orderInfo += "&subject=" + "\"" + subject + "\"";

		// 商品详情
		orderInfo += "&body=" + "\"" + body + "\"";

		// 商品金额
		orderInfo += "&total_fee=" + "\"" + price + "\"";

		// 服务器异步通知页面路径
		orderInfo += "&notify_url=" + "\"" + "http://notify.msp.hk/notify.htm"
				+ "\"";

		// 服务接口名称， 固定值
		orderInfo += "&service=\"mobile.securitypay.pay\"";

		// 支付类型， 固定值
		orderInfo += "&payment_type=\"1\"";

		// 参数编码， 固定值
		orderInfo += "&_input_charset=\"utf-8\"";

		// 设置未付款交易的超时时间
		// 默认30分钟，一旦超时，该笔交易就会自动被关闭。
		// 取值范围：1m～15d。
		// m-分钟，h-小时，d-天，1c-当天（无论交易何时创建，都在0点关闭）。
		// 该参数数值不接受小数点，如1.5h，可转换为90m。
		orderInfo += "&it_b_pay=\"30m\"";

		// extern_token为经过快登授权获取到的alipay_open_id,带上此参数用户将使用授权的账户进行支付
		// orderInfo += "&extern_token=" + "\"" + extern_token + "\"";

		// 支付宝处理完请求后，当前页面跳转到商户指定页面的路径，可空
		orderInfo += "&return_url=\"m.alipay.com\"";

		// 调用银行卡支付，需配置此参数，参与签名， 固定值 （需要签约《无线银行卡快捷支付》才能使用）
		// orderInfo += "&paymethod=\"expressGateway\"";

		return orderInfo;
	}

	/**
	 * get the sign type we use. 获取签名方式
	 *
	 */
	public String getSignType() {
		return "sign_type=\"RSA\"";
	}

}
