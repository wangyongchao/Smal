package com.bjjy.mainclient.phone.view.course;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;

import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.view.course.bean.MCourseBean;
import com.bjjy.mainclient.phone.view.course.bean.MCourseDetailBean;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.List;

/**
 * Created by wyc 2017.10.30
 */
public class KechengAdapter extends BaseQuickAdapter<MCourseBean, BaseViewHolder> {
    private MCourseBeanItemClickListener mCourseBeanClickListener;
    private Context context;

    public KechengAdapter() {
        super(R.layout.course_kecheng_item);
    }

    public KechengAdapter(Context context, List<MCourseBean> models) {
        super(R.layout.course_kecheng_item,models);
        this.context=context;
    }
    public void setMCourseBeanClickListener(MCourseBeanItemClickListener mCourseBeanClickListener){
        this.mCourseBeanClickListener=mCourseBeanClickListener;
    }

    /**
     * 设置当前是否显示选择框
     */
    public void setShowCheckOrNot(boolean isShowCheck){
        notifyDataSetChanged();
    }

    private void updateCheckImage(int position, ImageView check_iv) {

    }

    @Override
    protected void convert(final BaseViewHolder helper, MCourseBean item) {
        if (item==null||item.getCourseDetailInfo()==null||item.getCourseDetailInfo().size()==0)return;
        MCourseDetailBean mCourseDetailBean=item.getCourseDetailInfo().get(0);
        ImageLoader.getInstance().loadImage(item.getImageUrl(), null, null, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String s, View view) {
                helper.setImageDrawable(R.id.iv_corse_img,context.getResources().getDrawable(R.drawable.default_img));
            }

            @Override
            public void onLoadingFailed(String s, View view, FailReason failReason) {
                helper.setImageDrawable(R.id.iv_corse_img,context.getResources().getDrawable(R.drawable.default_img));
            }

            @Override
            public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                helper.setImageBitmap(R.id.iv_corse_img,bitmap);

            }

            @Override
            public void onLoadingCancelled(String s, View view) {

            }
        });
        if (helper.getLayoutPosition()==0){
            helper.setVisible(R.id.v_top,false);
        }else{
            helper.setVisible(R.id.v_top,true);
        }
        helper.setText(R.id.tv_name,mCourseDetailBean.getCourseName());
        helper.setText(R.id.tv_teacher_name,"讲师："+mCourseDetailBean.getTeacherName());
        helper.setText(R.id.tv_time,mCourseDetailBean.getCredit());
        helper.getView(R.id.tv_in_exam).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCourseBeanClickListener!=null){
                    mCourseBeanClickListener.mCourseBeanClick(0,helper.getLayoutPosition());
                }
            }
        });
        helper.getView(R.id.ll_kecheng).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCourseBeanClickListener!=null){
                    mCourseBeanClickListener.mCourseBeanClick(1,helper.getLayoutPosition());
                }
            }
        });
    }

    public  interface MCourseBeanItemClickListener{
       public void  mCourseBeanClick(int type,int position);
    }
}
