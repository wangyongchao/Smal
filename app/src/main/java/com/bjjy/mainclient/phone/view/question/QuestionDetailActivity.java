package com.bjjy.mainclient.phone.view.question;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.dongao.mainclient.model.bean.question.QuestionAnswer;
import com.dongao.mainclient.model.bean.question.QuestionDetail;
import com.bjjy.mainclient.persenter.QuestionDetailPersenter;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseActivity;
import com.bjjy.mainclient.phone.event.ReloadQuestion;
import com.bjjy.mainclient.phone.event.UpdateEvent;
import com.bjjy.mainclient.phone.utils.NetworkUtil;
import com.bjjy.mainclient.phone.widget.RefreshScrollView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by fengzongwei on 2016/12/1 0001.
 */
public class QuestionDetailActivity extends BaseActivity implements QuestionDetailView,RefreshScrollView.OnRefreshScrollViewListener{

    @Bind(R.id.myquestion_scroll)
    ScrollView refreshScrollView;
    @Bind(R.id.my_questiont_hintImg)
    ImageView imageView_hint;
    @Bind(R.id.my_ques_list_pic_tvHint)
    TextView textView_hint;
    @Bind(R.id.my_ques_list_lodingBody)
    RelativeLayout relativeLayout_hint_body;
    @Bind(R.id.animation_progress_img)
    ImageView imageView_loading;
    @Bind(R.id.my_ques_list_picBody)
    LinearLayout linearLayout_hint_img_body;

    /**
     * 显示夫问题的控件
     */
    @Bind(R.id.my_question_item_queContentWeb)
    WebView fa_ques_queContentWeb;
    @Bind(R.id.my_question_item_queContentHtml)
    HtmlTextView fa_ques_queContentHtml;
    @Bind(R.id.my_question_item_queTime)
    TextView fa_ques_queTime;
    @Bind(R.id.my_question_item_bookName)
    TextView fa_ques_queKnowName;
    @Bind(R.id.my_question_item_zhuicount)
    TextView  fa_ques_zhuiCount;
    @Bind(R.id.my_question_item_linetwo)
    TextView fa_ques_lineTwo;
    @Bind(R.id.my_question_item_answerBody)
    LinearLayout fa_ques_linearLayout_answerBody;
    @Bind(R.id.my_question_item_anwContentWeb)
    WebView fa_ques_ansContentWeb;
    @Bind(R.id.my_question_item_anwContentHtml)
    HtmlTextView fa_ques_ansContenHtml;
    @Bind(R.id.my_question_item_answerTime)
    TextView fa_ques_ansTime;
    @Bind(R.id.my_question_item_zhuiwenBody)
    RelativeLayout fa_ques_zhuiBody;
    @Bind(R.id.my_question_item_zhuiwenBtBody)
    LinearLayout linearLayout_zhuiwenBody;
    @Bind(R.id.myquestion_title_tv)
    TextView textView_title;
    @Bind(R.id.myquestion_type_tv)
    TextView textView_type;

    private ImageView imageView_right;

    private boolean isMyQuestion = false;
    public String questionAnswerId;

    private MyQuestionDetailPop myQuestionDetailPop;

    private QuestionDetail questionDetail;
    private List<QuestionAnswer> questionAnswers;

    private static final String encoding = "utf-8";
    private static final String mimeType = "text/html";

    public QuestionDetailPersenter questionDetailPersenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question_detail);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        isMyQuestion = getIntent().getBooleanExtra("isMyQuestion",false);
        initView();
        initData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onEventMainThread(ReloadQuestion reloadQuestion){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getData();
            }
        },300);
    }

    @Override
    public void initView() {
        myQuestionDetailPop = new MyQuestionDetailPop(this);
        relativeLayout_hint_body.setVisibility(View.VISIBLE);
        ((ImageView)findViewById(R.id.top_title_left)).setImageResource(R.drawable.back);
        findViewById(R.id.top_title_left).setVisibility(View.VISIBLE);
        ((TextView)findViewById(R.id.top_title_text)).setText("答疑详情");
        imageView_right = ((ImageView)findViewById(R.id.top_title_right));
        if(isMyQuestion) {
            ((ImageView) findViewById(R.id.top_title_right)).setImageResource(R.drawable.my_question_detail_title_right);
            findViewById(R.id.top_title_right).setVisibility(View.VISIBLE);
            findViewById(R.id.top_title_right).setOnClickListener(this);
        }
        linearLayout_hint_img_body.setOnClickListener(this);
        findViewById(R.id.top_title_left).setOnClickListener(this);
    }

    @Override
    public void initData() {
        questionAnswerId = getIntent().getStringExtra("questionAnswerId");
        questionDetailPersenter = new QuestionDetailPersenter();
        questionDetailPersenter.attachView(this);
        getData();
    }

    private void getData(){
        if(NetworkUtil.isNetworkAvailable(this)){
            showLoading();
            questionDetailPersenter.getData();
        }else{
            showNetError();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.top_title_right:
                myQuestionDetailPop.showPop(imageView_right);
                break;
            case R.id.top_title_left:
                finish();
                break;
            case R.id.my_ques_list_picBody:
                getData();
                break;
        }
    }

    @Override
    public void onRefresh() {

    }

    @Override
    public void onLoadMore() {

    }

    @Override
    public String getQuestionId() {
        return questionAnswerId;
    }

    @Override
    public void showQuestionDetail(QuestionDetail questionDetail) {
        relativeLayout_hint_body.setVisibility(View.GONE);
        this.questionDetail = questionDetail;
        textView_title.setText(questionDetail.getTitle());
        questionAnswers = questionDetail.getQaInfoVoList();
        if(questionDetailPersenter.questionDetail.getQuestionSourceVo()!=null && !TextUtils.isEmpty(questionDetailPersenter.questionDetail.getQuestionSourceVo().getQuestionId())){
//            textView_type.setText(questionDetailPersenter.questionDetail.getQuestionSourceVo().getPaperName());
            fa_ques_queKnowName.setText(questionDetailPersenter.questionDetail.getQuestionSourceVo().getPaperName());
        }else if(questionDetailPersenter.questionDetail.getBookSourceVo()!=null && !TextUtils.isEmpty(questionDetailPersenter.questionDetail.getBookSourceVo().getBookId())){
//            textView_type.setText(questionDetailPersenter.questionDetail.getBookSourceVo().getBookName());
            fa_ques_queKnowName.setText(questionDetailPersenter.questionDetail.getBookSourceVo().getChapterName());
        }
        showQuestionDetailInView();
    }

    @Override
    public void showLoading() {
        relativeLayout_hint_body.setVisibility(View.VISIBLE);
        imageView_loading.setVisibility(View.VISIBLE);
        linearLayout_hint_img_body.setVisibility(View.GONE);
        AnimationDrawable animationDrawable = (AnimationDrawable)imageView_loading.getDrawable();
        animationDrawable.start();
    }

    @Override
    public void hideLoading() {
        relativeLayout_hint_body.setVisibility(View.GONE);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {
        showLoadError(message);
    }


    public void showLoadError(String msg){
        relativeLayout_hint_body.setVisibility(View.VISIBLE);
        imageView_loading.setVisibility(View.GONE);
        linearLayout_hint_img_body.setVisibility(View.VISIBLE);
        imageView_hint.setImageResource(R.drawable.error_pic);
        textView_hint.setVisibility(View.VISIBLE);
        textView_hint.setText(msg);
    }

    public void showNetError(){
        relativeLayout_hint_body.setVisibility(View.VISIBLE);
        imageView_loading.setVisibility(View.GONE);
        linearLayout_hint_img_body.setVisibility(View.VISIBLE);
        imageView_hint.setImageResource(R.drawable.pic_network2);
        textView_hint.setVisibility(View.GONE);
    }

    @Override
    public Context context() {
        return this;
    }

    public void showQuestionDetailInView(){
        if(questionAnswers.get(0).getContent().contains("</td>")){
            fa_ques_queContentWeb.setVisibility(View.VISIBLE);
            fa_ques_queContentHtml.setVisibility(View.GONE);
            fa_ques_queContentWeb.loadDataWithBaseURL
                    ("", "<font color='#808080' style='font-size:15px;'>" + (questionAnswers.get(0).getContent()) + "</font>", mimeType, encoding, "");
        }else{
            fa_ques_queContentHtml.setVisibility(View.VISIBLE);
            fa_ques_queContentWeb.setVisibility(View.GONE);
            fa_ques_queContentHtml.setHtmlFromString
                    ( "<font color='#808080' style='font-size:15px;'>" + (questionAnswers.get(0).getContent()) + "</font>", new HtmlTextView.RemoteImageGetter());
        }
        if(TextUtils.isEmpty(questionAnswers.get(0).getAnswer()))
            fa_ques_queTime.setText(questionAnswers.get(0).getCreateDate());
        else
            fa_ques_queTime.setText(questionAnswers.get(0).getCreateDate());
        if(questionAnswers.size()>1){
            fa_ques_zhuiCount.setText("追问(" + (questionAnswers.size()-1) + ")");
        }else{
            fa_ques_lineTwo.setVisibility(View.GONE);
            fa_ques_zhuiCount.setVisibility(View.GONE);
        }
        fa_ques_lineTwo.setVisibility(View.GONE);
        fa_ques_zhuiCount.setVisibility(View.GONE);
        if(TextUtils.isEmpty(questionAnswers.get(0).getAnswer())){
            fa_ques_linearLayout_answerBody.setVisibility(View.GONE);
        }else{
            if(questionAnswers.get(0).getAnswer().contains("</td>") || questionAnswers.get(0).getContent().contains("</p>")){
                fa_ques_ansContentWeb.setVisibility(View.VISIBLE);
                fa_ques_ansContenHtml.setVisibility(View.GONE);
                fa_ques_ansContentWeb.loadDataWithBaseURL
                        ("", "<font color='#808080' style='font-size:15px;'>" + (questionAnswers.get(0).getAnswer()) + "</font>", mimeType, encoding, "");
            }else{
                fa_ques_ansContenHtml.setVisibility(View.VISIBLE);
                fa_ques_ansContentWeb.setVisibility(View.GONE);
                fa_ques_ansContenHtml.setHtmlFromString
                        ( "<font color='#808080' style='font-size:15px;'>" + (questionAnswers.get(0).getAnswer()) + "</font>", new HtmlTextView.RemoteImageGetter());
            }
            fa_ques_ansTime.setText(questionAnswers.get(0).getAnswerDate());
        }
        if(questionAnswers.size()>1){
            addChildQuestion(linearLayout_zhuiwenBody);
        }else{
            fa_ques_zhuiBody.setVisibility(View.GONE);
        }
    }

    public void addChildQuestion(LinearLayout body){
        if(body==null){
            body = (LinearLayout) findViewById(R.id.my_question_item_zhuiwenBtBody);
        }
        while(body.getChildCount()>1){
            body.removeViewAt(1);
        }
//        for(int i=1;i<childCount;i++){
//            body.removeViewAt(i);
//        }
        for(int i=1;i<questionAnswers.size();i++){
            View view_child_question = LayoutInflater.from(this).inflate(R.layout.my_question_item_zhuiwen_item,null);
            WebView webView_que = (WebView)view_child_question.findViewById(R.id.my_question_zhuiwen_item_queContentWeb);
            HtmlTextView htmlTextView_que = (HtmlTextView)view_child_question.findViewById(R.id.my_question_zhuiwen_item_queContentHtml);
            TextView textView_que_time = (TextView)view_child_question.findViewById(R.id.my_question_zhuiwen_item_queTime);
            LinearLayout linearLayout_ans_body = (LinearLayout)view_child_question.findViewById(R.id.my_question_zhuiwen_item_answBody);
            WebView webView_ans = (WebView)view_child_question.findViewById(R.id.my_question_zhuiwen_item_anwContentWeb);
            HtmlTextView htmlTextView_ans = (HtmlTextView)view_child_question.findViewById(R.id.my_question_zhuiwen_item_anwContentHtml);
            TextView textView_ans_time = (TextView)view_child_question.findViewById(R.id.my_question_zhuiwen_item_answerTime);
            if(questionAnswers.get(i).getContent().contains("</td>") || questionAnswers.get(i).getContent().contains("</p>")){
                webView_que.setVisibility(View.VISIBLE);
                htmlTextView_que.setVisibility(View.GONE);
                webView_que.loadDataWithBaseURL
                        ("", "<font color='#808080' style='font-size:15px;'>" + (questionAnswers.get(i).getContent()) + "</font>", mimeType, encoding, "");
            }else{
                htmlTextView_que.setVisibility(View.VISIBLE);
                webView_que.setVisibility(View.GONE);
                htmlTextView_que.setHtmlFromString
                        ("<font color='#808080' style='font-size:15px;'>" + (questionAnswers.get(i).getContent()) + "</font>", new HtmlTextView.RemoteImageGetter());
            }
            textView_que_time.setText(questionAnswers.get(i).getCreateDate());
            if(TextUtils.isEmpty(questionAnswers.get(i).getAnswer())){
                linearLayout_ans_body.setVisibility(View.GONE);
            }else{
                if(questionAnswers.get(i).getAnswer().contains("</td>") || questionAnswers.get(i).getContent().contains("</p>")){
                    webView_ans.setVisibility(View.VISIBLE);
                    htmlTextView_ans.setVisibility(View.GONE);
                    webView_ans.loadDataWithBaseURL
                            ("", "<font color='#808080' style='font-size:15px;'>" + (questionAnswers.get(i).getAnswer()) + "</font>", mimeType, encoding, "");
                }else{
                    htmlTextView_ans.setVisibility(View.VISIBLE);
                    webView_ans.setVisibility(View.GONE);
                    htmlTextView_ans.setHtmlFromString
                            ("<font color='#808080' style='font-size:15px;'>" + (questionAnswers.get(i).getAnswer()) + "</font>", new HtmlTextView.RemoteImageGetter());
                }
                textView_ans_time.setText(questionAnswers.get(i).getAnswerDate());
            }
            body.addView(view_child_question);
        }
    }

    @Override
    public void deleteSuccess() {
        Toast.makeText(this,"删除成功",Toast.LENGTH_SHORT).show();
        finish();
        EventBus.getDefault().post(new UpdateEvent(false));
    }

    public void deleteQuestion(String questionId,boolean isHaveZhui){
        questionDetailPersenter.deleteQuestion(questionId, isHaveZhui);
    }

    @Override
    public void deleteFail() {
        Toast.makeText(this,"删除失败，请重试",Toast.LENGTH_SHORT).show();
    }

}
