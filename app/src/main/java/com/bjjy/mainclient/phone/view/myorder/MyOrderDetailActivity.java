package com.bjjy.mainclient.phone.view.myorder;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bjjy.mainclient.persenter.MyOrderDetailPersenter;
import com.bjjy.mainclient.phone.app.BaseActivity;
import com.bjjy.mainclient.phone.event.PaySuccessEvent;
import com.bjjy.mainclient.phone.view.payment.PayWayActivity;
import com.dongao.mainclient.core.util.NetUtils;
import com.dongao.mainclient.model.bean.myorder.MyOrderDetail;
import com.dongao.mainclient.model.bean.myorder.MyOrderDetailProduct;
import com.bjjy.mainclient.phone.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by fengzongwei on 2016/4/6.
 */
public class MyOrderDetailActivity extends BaseActivity implements MyOrderDetailView{
    @Bind(R.id.top_title_text)
    TextView textView_title;
    @Bind(R.id.top_title_left)
    ImageView imageView_back;
    @Bind(R.id.my_order_detail_pay_ll)
    LinearLayout linearLayout_pay;//底部黄色去付款布局
    @Bind(R.id.my_order_detail_warning_tv)
    TextView textView_hint;//顶部黄色提示文字
    @Bind(R.id.my_order_detail_payStatus_tv)
    TextView textView_payStatus;//支付状态
    @Bind(R.id.my_order_detail_count_tv)
    TextView textView_count;//商品件数
    @Bind(R.id.my_order_detail_totlePrice_tv)
    TextView textView_totle;//订单总额
    @Bind(R.id.my_order_detail_price)
    TextView textView_price;//商品清单下面红色价格
    @Bind(R.id.my_order_detail_discountPrice_tv)
    TextView textView_discount;//优惠金额
    @Bind(R.id.my_order_detail_points_tv)
    TextView textView_point;//获得积分
    @Bind(R.id.my_order_detail_actualPrice_tv)
    TextView textView_actual;//实付金额
    @Bind(R.id.my_order_detail_id_tv)
    TextView textView_id;//订单编号
    @Bind(R.id.my_order_detail_time_tv)
    TextView textView_time;//订单时间
    @Bind(R.id.my_order_detail_product_body_ll)
    LinearLayout linearLayout_productBody;//商品列表父布局
    @Bind(R.id.my_order_detail_content_body)
    LinearLayout linearLayout_body;
    @Bind(R.id.hint_body)
    RelativeLayout relativeLayout_hint;
    @Bind(R.id.hint_tv)
    TextView textView_error;
    private MyOrderDetail myOrderDetail;
    private MaterialDialog materialDialog;
    private boolean isPay = false;
    private String orderId = "121";
    private MyOrderDetailPersenter myOrderDetailPersenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_order_detail);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        isPay = getIntent().getBooleanExtra("isPay",false);
        orderId = getIntent().getStringExtra("orderId");
        myOrderDetailPersenter = new MyOrderDetailPersenter();
        myOrderDetailPersenter.attachView(this);
        initView();
        initData();
    }

    @Override
    public void initView() {
        textView_title.setText("订单详情");
        imageView_back.setImageResource(R.drawable.back);
        imageView_back.setVisibility(View.VISIBLE);

        materialDialog = new MaterialDialog(this);
        materialDialog.setTitle("提示");
        materialDialog.setMessage("当前账号已被踢出，请重新登录");
        materialDialog.setPositiveButton("确定", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(MyOrderDetailActivity.this, LoginNewActivity.class);
//                startActivityForResult(intent, RESULT_OK);
            }
        });

    }

    @OnClick(R.id.top_title_left) void back(){
        this.finish();
    }

    @OnClick(R.id.hint_click_body) void retry(){
        initData();
    }

    @OnClick(R.id.my_order_detail_pay_ll) void pay(){
        Intent intent=new Intent(MyOrderDetailActivity.this,PayWayActivity.class);
        intent.putExtra("orderId",myOrderDetail.getId());
        intent.putExtra("totalPrice",myOrderDetail.getActualOrderAmt());
        startActivity(intent);
    }

    @Override
    public void initData() {
        if(NetUtils.checkNet(this).available) {
            myOrderDetailPersenter.checkToken();
            relativeLayout_hint.setVisibility(View.GONE);
        }else{
            relativeLayout_hint.setVisibility(View.VISIBLE);
            showError("网络异常，点击重试");
        }
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public String orderId() {
        return orderId;
    }

    @Override
    public void showData(MyOrderDetail myOrderDetail) {
        this.myOrderDetail = myOrderDetail;
        if(myOrderDetail.getPayStatus() == MyOrderDetail.PAYED) {
            textView_hint.setVisibility(View.GONE);
            linearLayout_pay.setVisibility(View.GONE);
            textView_payStatus.setText("已付款");
            textView_payStatus.setTextColor(Color.parseColor("#44db5e"));
        }else {
            textView_payStatus.setText("待付款");
            textView_payStatus.setTextColor(Color.parseColor("#d54636"));
        }
        textView_count.setText("共"+myOrderDetail.getGoodsNum()+"件");
        textView_price.setText("¥"+myOrderDetail.getOrderAmt());
        textView_totle.setText("¥"+myOrderDetail.getOrderAmt());
        textView_discount.setText("- ¥"+myOrderDetail.getDiscountAmt());
        textView_point.setText(myOrderDetail.getIntegral());
        textView_actual.setText("¥"+myOrderDetail.getActualOrderAmt());
        textView_id.setText(myOrderDetail.getId());
        textView_time.setText(myOrderDetail.getOrderDate());
        addProduct(myOrderDetail);
        linearLayout_body.setVisibility(View.VISIBLE);
    }
    private void addProduct(MyOrderDetail myOrderDetail){
        MyOrderDetailProduct myOrderDetailProduct = null;
        TextView  textView = null;
        for(int i=0;i<myOrderDetail.getOrderItemVos().size();i++){
            myOrderDetailProduct = myOrderDetail.getOrderItemVos().get(i);
            textView =  new TextView(MyOrderDetailActivity.this);
            textView.setSingleLine(true);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams
                    (LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
            if(i!=0){
                params.setMargins(0,15,0,0);
            }
            textView.setLayoutParams(params);
            textView.setText(myOrderDetailProduct.getGoodsName());
            linearLayout_productBody.addView(textView);
        }
    }

    @Override
    public void showLoading() {
        progress.show();
    }

    @Override
    public void hideLoading() {
        progress.dismiss();
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {
        relativeLayout_hint.setVisibility(View.VISIBLE);
        textView_error.setText(message);
    }

    @Override
    public Context context() {
        return this;
    }

    @Override
    public void deviceTokenSuccess() {
        myOrderDetailPersenter.getData();
    }

    @Override
    public void deviceTokenFailed() {
        materialDialog.show();
    }

    @Subscribe
    public void onEventMainThread(PaySuccessEvent event) {
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            initData();
        }
    }
}
