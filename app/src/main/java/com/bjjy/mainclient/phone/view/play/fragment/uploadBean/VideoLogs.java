package com.bjjy.mainclient.phone.view.play.fragment.uploadBean;


import java.io.Serializable;
import java.util.List;

/**
 * Created by fengzongwei on 2016/5/30 0030.
 * шо▓
 */
public class VideoLogs implements Serializable{

    private String cwCode;
    private String year;
    private List<CourseVideoBean> videoDtos;



    public List<CourseVideoBean> getVideoDtos() {
        return videoDtos;
    }

    public void setVideoDtos(List<CourseVideoBean> videoDtos) {
        this.videoDtos = videoDtos;
    }

    public String getCwCode() {
        return cwCode;
    }

    public void setCwCode(String cwCode) {
        this.cwCode = cwCode;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

}
