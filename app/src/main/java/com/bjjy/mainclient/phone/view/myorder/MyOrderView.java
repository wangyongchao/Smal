package com.bjjy.mainclient.phone.view.myorder;

import com.dongao.mainclient.model.mvp.MvpView;
import com.dongao.mainclient.model.bean.myorder.MyOrderItem;

import java.util.List;

/**
 * Created by fengzongwei on 2016/4/6.
 */
public interface MyOrderView  extends MvpView {
    void freshData();//刷新数据
    String type();//当前页面的类型（未付款、已付款）
    void loadSuccess(List<MyOrderItem> orderItemList);
    void deviceTokenSuccess();//token验证成功
    void deviceTokenFailed();//token验证失败
}
