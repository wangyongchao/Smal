package com.bjjy.mainclient.phone.view.download.local.SelectYearPopupwindow;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.view.download.local.bean.YearInfo;

import java.util.List;

/**
 * 我的课程年份的adapter
 * @author renyangyang
 *
 */
public class YearsAdapter extends BaseAdapter {
	private Context context;
	private List<YearInfo> list;
	private LayoutInflater mInflater;
	private int focusPosition;//设置默认选中的 年份
	
	public YearsAdapter(Context context){
		this.context =  context;
		mInflater = LayoutInflater.from(context);
	}
	
	public void setList(List<YearInfo> list) {
		this.list = list;
	}

	public void setFocusPosition(int position){
		this.focusPosition = position;
		this.notifyDataSetChanged();
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder = null;
		if(convertView == null){
			convertView = mInflater.inflate(R.layout.widget_selectyears_popup_item, null);
			viewHolder = new ViewHolder();
			viewHolder.tv_year = (TextView) convertView.findViewById(R.id.tv_year);
			convertView.setTag(viewHolder);
		}else{
			viewHolder = (ViewHolder) convertView.getTag();
		}
		
		if(list != null && list.size()> position){
			YearInfo yearInfo = list.get(position);
			if (yearInfo != null) {
				if(yearInfo.getShowYear()!=null && !yearInfo.getShowYear().equals(""))
					viewHolder.tv_year.setText(yearInfo.getShowYear());
				else
					viewHolder.tv_year.setText(yearInfo.getYearName()+"年");
				if(focusPosition == position){
					viewHolder.tv_year.setBackgroundResource(R.drawable.popup_item_focused);
				}else{
					viewHolder.tv_year.setBackgroundResource(R.drawable.list_selector);
				}
			}
		}
		
		return convertView;
	}

	
	class  ViewHolder{
		TextView tv_year;
	}
}
