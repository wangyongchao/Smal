package com.bjjy.mainclient.phone.view.play.domain;

import android.content.Context;

import com.bjjy.mainclient.phone.utils.StringUtil;
import com.bjjy.mainclient.phone.view.exam.activity.exampaperlist.bean.MyAllCourse;
import com.dongao.mainclient.core.util.LogUtils;
import com.dongao.mainclient.model.bean.play.CwStudyLog;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.yunqing.core.db.DBExecutor;
import com.yunqing.core.db.sql.Sql;
import com.yunqing.core.db.sql.SqlFactory;

import java.util.List;

/**
 * Created by wyc on 2015/1/12.
 */
public class CwStudyLogDB {
    Context mContext;
    DBExecutor dbExecutor = null;
    Sql sql = null;

    public CwStudyLogDB(Context mContext){
        this.mContext = mContext;
        dbExecutor = DBExecutor.getInstance(mContext);
    }

    public void insert(CwStudyLog cwStudyLog){
        dbExecutor.insert(cwStudyLog);
    }

    public void update(CwStudyLog cwStudyLog){
        dbExecutor.updateById(cwStudyLog);
    }

    public boolean delete(CwStudyLog cwStudyLog){
        return dbExecutor.deleteById(CwStudyLog.class, cwStudyLog.getDbId());
    }

    public CwStudyLog find(int id){
        return dbExecutor.findById(CwStudyLog.class, id);
    }


    public List<CwStudyLog> findAll(){
        sql = SqlFactory.find(MyAllCourse.class);
        return dbExecutor.executeQuery(sql);
    }

    /**
     * 查询方法
     * @return
     */
    public CwStudyLog query(String userid,String videoID,String cwCode){
        sql = SqlFactory.find(CwStudyLog.class).orderBy("lastUpdateTime", true).where("videoID=? and userCode=? and cwCode=?", new Object[]{videoID, userid, cwCode});
        return dbExecutor.executeQueryGetFirstEntry(sql);
    }

    /**
     * 查询方法
     * @return
     */
    public CwStudyLog queryLogs(String userid,String cwCode,String subjectId){
        sql = SqlFactory.find(CwStudyLog.class).orderBy("lastUpdateTime", true).where("userCode=? and cwCode=? and subjectId=?", new Object[]{userid, cwCode,subjectId});
        return dbExecutor.executeQueryGetFirstEntry(sql);
    }
    /**
     * 查询方法
     * @return
     */
    public List<CwStudyLog> query(String userid,String cwCode){
        SharedPrefHelper.getInstance(mContext).setIsAreadyStudyOver("");
        List<CwStudyLog> list;
        sql = SqlFactory.find(CwStudyLog.class).orderBy("lastUpdateTime", true).where("userCode=? and cwCode=?", new Object[]{userid, cwCode});
        list=dbExecutor.executeQuery(sql);
        for(int i=0;i<list.size();i++){
            if(list.get(i).getNativeWatcheAt()+15000<list.get(i).getTotalTime()){
                SharedPrefHelper.getInstance(mContext).setIsAreadyStudyOver("NO");
                return list;
            }
        }
        return list;
    }

    /**
     * 查询改课程最后播放的视频课件
     * @return
     */
    public String getLastCoursePlay(String userid,String cwCode){
        sql = SqlFactory.find(CwStudyLog.class).orderBy("lastUpdateTime", true).where("userCode=? and cwCode=?", new Object[]{userid, cwCode});
        CwStudyLog studyLog=dbExecutor.executeQueryGetFirstEntry(sql);
        if(studyLog!=null){
            return studyLog.getCwid();
        }
        return "";
    }

    /**
     * 查询方法
     * @return
     */
    public List<CwStudyLog> queryByYearAndCourseId(String userid,String curriculumId,String mYear){
        sql = SqlFactory.find(CwStudyLog.class).orderBy("lastUpdateTime", true).where("cwCode=? and userCode=? and mYear=? and watchedAt > 0", new Object[]{curriculumId, userid, mYear});
        return dbExecutor.executeQuery(sql);
    }

    /**
     * 查询方法
     * @return
     */
    public List<CwStudyLog> queryByUserId(String userid){
        sql = SqlFactory.find(CwStudyLog.class).orderBy("lastUpdateTime", true).where("userCode=? and watchedAt > 0", new Object[]{userid});
        return dbExecutor.executeQuery(sql);
    }

    /**
     * 查询方法
     * @return
     */
    public List<CwStudyLog> queryLast5LogByUserId(String userid){
        List<CwStudyLog> list;
        sql = SqlFactory.find(CwStudyLog.class).orderBy("lastUpdateTime", true).where("userCode=?", new Object[]{userid}).limit(5);
        list = dbExecutor.executeQuery(sql);
        if(list!=null && list.size()>5){
            list.subList(0,5);
        }
        return list;
    }

    /**
     * 查询方法所有听过的课节数
     * @return
     */
    public int getAllListened(String userid,String subjectId){
        sql = SqlFactory.find(CwStudyLog.class).orderBy("lastUpdateTime", true).where("userCode=? and subjectId=?", new Object[]{userid,subjectId});
        return dbExecutor.executeQuery(sql).size();
    }

    /**
     * 查询方法所有听过的课节数的总时长
     * @return
     */
    public long getListenedTimes(String userid,String subjectId){
        long time=0;
        sql = SqlFactory.find(CwStudyLog.class).orderBy("lastUpdateTime", true).where("userCode=? and subjectId=?", new Object[]{userid,subjectId});
        List<CwStudyLog> cwStudyLogs=dbExecutor.executeQuery(sql);
        for(CwStudyLog cwStudyLog:cwStudyLogs){
            time=time+cwStudyLog.getNativeWatcheAt();
        }
        return time;
    }

    /**
     * 查询方法
     * @return
     */
    public CwStudyLog queryByUserIdLastUpdateTime(String userid,String subjectId){
        sql = SqlFactory.find(CwStudyLog.class).orderBy("lastUpdateTime", true).where("userCode=? and subjectId=?", new Object[]{userid,subjectId});
        return dbExecutor.executeQueryGetFirstEntry(sql);
    }

    /**
     * 未同步 总时长
     * @param userid
     * @param year
     * @return
     */
    public long queryUnSynStudyTime(String userid,String year){
        String sqlStr = "select sum(watchedAt) from t_cw_study_log where userCode='"+userid
                +"' and mYear="+year;
        sql = SqlFactory.makeSql(CwStudyLog.class, sqlStr);
        String result = dbExecutor.executeQueryGetFirstEntry(sql).toString();
        long sum = 0;
        if (!StringUtil.isEmpty(result)) {
            sum = Long.parseLong(result);
            LogUtils.d("未同步总时长" + sum);
        }else {
            LogUtils.d("未同步总时长" + "null");
        }
        return sum;
    }

    public boolean deleteAll(){
        return dbExecutor.deleteAll(CwStudyLog.class);
    }

    /**
     * 查询方法
     * @return
     */
    public boolean isFinished(String userid,String videoID,String cwCode){
        sql = SqlFactory.find(CwStudyLog.class).where("videoID=? and userCode=? and cwCode=?", new Object[]{videoID, userid, cwCode});
        CwStudyLog cwStudyLog=dbExecutor.executeQueryGetFirstEntry(sql);
        if(cwStudyLog!=null && cwStudyLog.getNativeWatcheAt()+15000>=cwStudyLog.getTotalTime()){
            return true;
        }
        return false;
    }
}
