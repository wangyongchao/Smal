package com.bjjy.mainclient.phone.view.exam.activity.myexam;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseFragmentActivity;
import com.bjjy.mainclient.phone.view.exam.ExamActivity;
import com.bjjy.mainclient.phone.view.exam.activity.exampaperlist.adapter.ExamPaperListAdapter;
import com.bjjy.mainclient.phone.view.exam.activity.myexam.adapter.QueryAddPopAdapter;
import com.bjjy.mainclient.phone.view.exam.activity.myexam.bean.QueryAddBookBean;
import com.bjjy.mainclient.phone.view.studybar.Constant;
import com.bjjy.mainclient.phone.widget.EmptyViewLayout;
import com.dongao.mainclient.model.bean.exam.ExamRuleInfo;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by wyc on 2016/6/21.
 */
public class ExamMineActivity extends BaseFragmentActivity implements ExamMineView, ExamPaperListAdapter.ReclerViewItemClick, QueryAddPopAdapter.MainTypeItemClick {
	@Bind(R.id.top_title_left)
	ImageView top_title_left;
	@Bind(R.id.top_title_right)
	ImageView top_title_right;
	@Bind(R.id.tv_right)
	TextView tv_right;
	@Bind(R.id.ll_top_right)
	LinearLayout ll_top_right;
	@Bind(R.id.top_title_text)
	TextView top_title_text;
	@Bind(R.id.swipe_container)
	SwipeRefreshLayout swipe_container;
	@Bind(R.id.ll_content)
	LinearLayout ll_content;
	@Bind(R.id.tv_top_name)
	TextView tv_top_name;
	@Bind(R.id.tv_time)
	TextView tv_time;
	@Bind(R.id.tv_produce)
	TextView tv_produce;
	@Bind(R.id.tv_number)
	TextView tv_number;
	@Bind(R.id.tv_score)
	TextView tv_score;
	@Bind(R.id.tv_done_num)
	TextView tv_done_num;
	private PopupWindow popupWindow;
	private View contentView;
	private RecyclerView rv_queryadd_pop;
	private QueryAddPopAdapter questionSelectBookAdapter;
	private String checkedPosition;

	@OnClick(R.id.top_title_left)
	void onBackClick() {
		onBackPressed();
	}

	@OnClick(R.id.tv_go_exam)
	void goExam() {
		SharedPrefHelper.getInstance(ExamMineActivity.this).setExamTag(Constants.EXAM_TAG_ABILITY);
		Intent intent = new Intent(ExamMineActivity.this, ExamActivity.class);
		intent.putExtra("examRule", JSON.toJSONString(examPaperListPercenter.examRuleInfo));
		startActivity(intent);
	}

	private ExamMinePercenter examPaperListPercenter;
	private EmptyViewLayout mEmptyLayout;

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.exam_mine_activity);
		ButterKnife.bind(this);
		examPaperListPercenter = new ExamMinePercenter();
		examPaperListPercenter.attachView(this);
		initView();
		initData();
	}

	@Override
	public void initView() {
		top_title_left.setVisibility(View.VISIBLE);
		tv_right.setVisibility(View.VISIBLE);
		mEmptyLayout = new EmptyViewLayout(this, ll_content);
		mEmptyLayout.setErrorButtonClickListener(mErrorClickListener);
		mEmptyLayout.setEmptyButtonClickListener(emptyButtonClickListener);
		setListener();
//        initPop();
		mEmptyLayout.showLoading();
	}

	private void setListener() {
		swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				examPaperListPercenter.initData();
			}
		});
		ll_top_right.setOnClickListener(this);
	}

	/**
	 * 错误监听
	 */
	private View.OnClickListener mErrorClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			examPaperListPercenter.initData();
		}
	};
	/**
	 * 无数据监听
	 */
	private View.OnClickListener emptyButtonClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View view) {
			//showAppMsg("显示kong");
		}
	};

	@Override
	public void initData() {
		mEmptyLayout.showLoading();
		examPaperListPercenter.initData();
		mEmptyLayout.showContentView();
	}

	@Override
	public void showLoading() {
		if (swipe_container != null) {
			swipe_container.setRefreshing(true);
		}
	}

	@Override
	public void hideLoading() {
		if (swipe_container != null) {
			swipe_container.setRefreshing(false);
		}
	}

	@Override
	public void showRetry() {

	}

	@Override
	public void hideRetry() {

	}

	@Override
	public void showError(String message) {
		Toast.makeText(ExamMineActivity.this, message, Toast.LENGTH_SHORT).show();
	}

	@Override
	public Context context() {
		return ExamMineActivity.this;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.ll_top_right:
				break;
			default:
				break;
		}
	}


	@Override
	public void initAdapter() {
	}

	@Override
	public void showContentView(int type) {
		if (type == Constant.VIEW_TYPE_0) {
			mEmptyLayout.showContentView();
		} else if (type == Constant.VIEW_TYPE_1) {
			mEmptyLayout.showNetErrorView();
		} else if (type == Constant.VIEW_TYPE_2) {
			mEmptyLayout.showEmpty();
		} else {
			mEmptyLayout.showError();
		}
		hideLoading();
	}

	@Override
	public boolean isRefreshNow() {
		if (swipe_container == null) {
			return false;
		}
		return swipe_container.isRefreshing();
	}

	@Override
	public SwipeRefreshLayout getRefreshLayout() {
		return swipe_container;
	}

	@Override
	public Intent getTheIntent() {
		return getIntent();
	}

	@Override
	public void showTopTextTitle(String title) {
		top_title_text.setVisibility(View.VISIBLE);
		top_title_text.setText(title);
	}

	@Override
	public void setNoDataMoreShow(boolean isShow) {
	}

	@Override
	public void showExamInfo(ExamRuleInfo examRuleInfo) {
		tv_number.setText(examRuleInfo.getSimulationNum() + "题");
		tv_time.setText(examRuleInfo.getExamTime() + "分钟");
		tv_score.setText(examRuleInfo.getPassScore() + "分");
		tv_done_num.setText(examRuleInfo.getCount() + "次");
	}

	@Override
	public void onItemClickListenerPosition(int position) {
		examPaperListPercenter.setOnItemClick(position);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

	@Override
	protected void onResume() {
		super.onResume();
		initView();
		initData();
	}

	private void initPop() {
		if (popupWindow == null) {
			contentView = LayoutInflater.from(ExamMineActivity.this).inflate(R.layout.queryadd_select_examination_pop, null);
			popupWindow = new PopupWindow(contentView, ViewGroup.LayoutParams.WRAP_CONTENT,
					ViewGroup.LayoutParams.WRAP_CONTENT);
			popupWindow.setBackgroundDrawable(new BitmapDrawable());
			popupWindow.setOutsideTouchable(true);
			popupWindow.setFocusable(true);
			rv_queryadd_pop = (RecyclerView) contentView.findViewById(R.id.rv_queryadd_pop);
			rv_queryadd_pop.setNestedScrollingEnabled(false);
			rv_queryadd_pop.setLayoutManager(new LinearLayoutManager(rv_queryadd_pop.getContext(), LinearLayoutManager.VERTICAL, false));
		}

	}

	public void showPop(View archer, List<QueryAddBookBean> qustionBooks) {
		initPop();
		questionSelectBookAdapter = new QueryAddPopAdapter(ExamMineActivity.this, qustionBooks, this);
		questionSelectBookAdapter.checkedPosition(checkedPosition);
		rv_queryadd_pop.setAdapter(questionSelectBookAdapter);
		popupWindow.showAsDropDown(ll_top_right, 0, 0);
	}

	@Override
	public void itemClick(int mainPosition, int itemType, int type, int position) {

	}
}
