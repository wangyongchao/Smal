package com.bjjy.mainclient.phone.view.studybar.privateteacher;

import android.content.Intent;

import com.dongao.mainclient.model.mvp.MvpView;
import com.bjjy.mainclient.phone.view.studybar.view.RefreshLayout;

/**
 * Created by wyc on 2016/6/6.
 */
public interface PrivateTeacherView extends MvpView{
    void initAdapter();
    void showContentView(int type);//0:正常 1：网络错误 2：没有数据
    boolean isRefreshNow();//是否正在刷新
    RefreshLayout getRefreshLayout();
    Intent getTheIntent();
    void showTopTitle(String title);
    void setNoDataMoreShow(boolean isShow);
}
