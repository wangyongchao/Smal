package com.bjjy.mainclient.phone.view.course;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.view.course.bean.MCourseBean;
import com.bjjy.mainclient.phone.view.course.bean.MCourseDetailBean;
import com.bjjy.mainclient.phone.view.main.MainActivity;
import com.bjjy.mainclient.phone.view.zb.utils.GlideUtils;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * Created by dell on 2016/8/23.
 */
public class CourseWodeExpandAdapter extends BaseExpandableListAdapter {

    private List<MCourseBean> mList;
    private MainActivity mContext;
    private ExpableListListener expableListListener;

    private ImageLoader imageLoader;
    private String userId;

    public CourseWodeExpandAdapter(MainActivity context, ImageLoader imageLoader){
        this.mContext = context;
        this.imageLoader=imageLoader;
        userId= SharedPrefHelper.getInstance(context).getUserId();
    }

    public void setList(List list) {
        this.mList = list;
    }
    
    public void setParentClick(ExpableListListener expableListListener) {
        this.expableListListener = expableListListener;
    }

    @Override
    public int getGroupCount() {
        return mList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mList.get(groupPosition).getCourseDetailInfo().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return null;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        GroupViewHolder viewHolder;
        if(convertView==null){
            viewHolder=new GroupViewHolder();
            convertView=View.inflate(mContext, R.layout.course_banxing_group,null);
            viewHolder.subjectName=(TextView)convertView.findViewById(R.id.subject_name);
            viewHolder.tv_in_exam=(TextView) convertView.findViewById(R.id.tv_in_exam);
            convertView.setTag(viewHolder);
        }else{
            viewHolder=(GroupViewHolder)convertView.getTag();
        }
        viewHolder.subjectName.setText(mList.get(groupPosition).getShowName());
        viewHolder.tv_in_exam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (expableListListener!=null){
                    expableListListener.onParentChick(groupPosition);
                }
            }
        });
        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition,final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ChildViewHolder holder;
        if (convertView == null) {
            holder = new ChildViewHolder();
            convertView = View.inflate(mContext, R.layout.course_banxing_item, null);
            holder.tv_name = (TextView) convertView.findViewById(R.id.tv_name);
            holder.v_top = (View) convertView.findViewById(R.id.v_top);
            holder.tv_teacher_name = (TextView) convertView.findViewById(R.id.tv_teacher_name);
            holder.tv_time = (TextView) convertView.findViewById(R.id.tv_time);
            holder.iv_corse_img = (ImageView) convertView.findViewById(R.id.iv_corse_img);
            convertView.setTag(holder);
        } else {
            holder = (ChildViewHolder) convertView.getTag();
        }
        if (childPosition==0){
            holder.v_top.setVisibility(View.GONE);
        }else{
            holder.v_top.setVisibility(View.VISIBLE);
        }
        MCourseBean mCourseBean=mList.get(groupPosition);
        if(!TextUtils.isEmpty(mCourseBean.getImageUrl()) && mCourseBean.getImageUrl().startsWith("http")){
            GlideUtils.loadImage(mContext,holder.iv_corse_img,mCourseBean.getImageUrl());


        }
        final MCourseDetailBean course=mList.get(groupPosition).getCourseDetailInfo().get(childPosition);
        holder.tv_name.setText(course.getCourseName());
        holder.tv_teacher_name.setText("讲师："+course.getTeacherName());
        holder.tv_time.setText(course.getListenDuration());
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    class GroupViewHolder {
        TextView subjectName;
        TextView tv_in_exam;
    }

    class ChildViewHolder {
        TextView tv_name;
        TextView tv_teacher_name;
        TextView tv_time;
        ImageView iv_corse_img;
        View v_top;
    }
    
    public interface ExpableListListener{
        void onParentChick(int position);
    }
}
