package com.bjjy.mainclient.phone.view.exam.activity.course.bean;

import com.yunqing.core.db.annotations.Id;
import com.yunqing.core.db.annotations.Table;
import java.util.List;

/**
 * Created by wyc on 2015/1/7.
 */
@Table(name="d_course_more_allClassify")
public class CourseMoreAllClassify {
    @Id
    private int dbId;
    private String year;//当前年份
    private String subjectId;// 科目Id
    private List<CourseMoreClassify> courseList;// 课程类别列表
    
    //添加的json数据
    private String userId;//用户Id
    private String content;//json字符串

    public int getDbId() {
        return dbId;
    }

    public void setDbId(int dbId) {
        this.dbId = dbId;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public List<CourseMoreClassify> getCourseList() {
        return courseList;
    }

    public void setCourseList(List<CourseMoreClassify> courseList) {
        this.courseList = courseList;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
