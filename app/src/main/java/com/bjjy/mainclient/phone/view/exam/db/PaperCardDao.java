package com.bjjy.mainclient.phone.view.exam.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.dongao.mainclient.model.bean.user.OldCollection;
import com.dongao.mainclient.model.local.BaseDao;
import com.bjjy.mainclient.phone.view.exam.bean.OldAnswerLog;

/**
 * 答题记录的dao
 * */
public class PaperCardDao extends BaseDao {
	private static final String SELECT_SQL = "select * from question where userid=? and subjectid=? and examid=?";
	private static final String EXIST_SQL = "select * from paperhistory where userid=? and subjectid=? and examid=?";
	private static final String SELECT_INDEX_SQL = "select writenum,totaltime from paperhistory where userid=? and subjectid=? and examid=? ";
	private static final String SELECT_REPORTED_EXAMPAPER_SQL = "select * from exampaper";
	private static final String SELECT_BY_ID_SQL = "select * from exampaper where userid=? and subjectid=? and examid=?";
	
	public PaperCardDao(Context context) {
		super(context);
	}
	public boolean exist(OldAnswerLog paper){
		SQLiteDatabase db = getWritableDB();
		Cursor cursor = db.rawQuery(EXIST_SQL, new String[]{String.valueOf(paper.getUserId()),String.valueOf(paper.getSubjectId()),
				   String.valueOf(paper.getUid())});
		if(cursor.moveToNext()){
			return true;
		}
		cursor.close();
		return false;
		
	}
	public long insertDoPaperTime(long time){
		SQLiteDatabase db = getWritableDB();
		
		ContentValues values=new ContentValues();
//		values.put(key, value)
		db.insert("paperhistory", null, values);
		return 0;
		
	}
	/**
	 * 获取生成答题记录的试卷列表
	 * @return
	 */
	public List<OldAnswerLog> getReportedExamPaper(){
		SQLiteDatabase db = getReadableDB();
		List<OldAnswerLog> oldAnswerLogs=new ArrayList<OldAnswerLog>();
		Cursor cursor = db.rawQuery(SELECT_REPORTED_EXAMPAPER_SQL,null);
		int i=0;//设置只能取前五百条数据
		while(cursor.moveToNext()){
			if (i>500){
				break;
			}
			OldAnswerLog oldAnswerLog=new OldAnswerLog();
			oldAnswerLog.setUid(cursor.getInt(cursor.getColumnIndex("examid")));
			oldAnswerLog.setUserId(cursor.getInt(cursor.getColumnIndex("userid")));
			// examinationPaper.setExamId(cursor.getInt(cursor.getColumnIndex("examid")));
			oldAnswerLog.setSubjectId(cursor.getInt(cursor.getColumnIndex("subjectid")));
			oldAnswerLog.setExamType(cursor.getInt(cursor.getColumnIndex("examtype")));
			oldAnswerLog.setSubject(cursor.getString(cursor.getColumnIndex("subject")));
			oldAnswerLog.setDescription(cursor.getString(cursor.getColumnIndex("description")));
			oldAnswerLog.setExamTime(cursor.getLong(cursor.getColumnIndex("examtime")));
			oldAnswerLog.setLimitTime(cursor.getLong(cursor.getColumnIndex("limittime")));
			oldAnswerLog.setCheckStatue(cursor.getInt(cursor.getColumnIndex("checkstatue")));
			oldAnswerLog.setExaminationHistoryId(cursor.getInt(cursor.getColumnIndex("examinationhistoryid")));
			int saved = cursor.getInt(cursor.getColumnIndex("saved"));
			oldAnswerLog.setSaved(saved == 1);
			oldAnswerLog.setUpdateTime(cursor.getLong(cursor.getColumnIndex("updatetime")));
			oldAnswerLog.setReaded(cursor.getInt(cursor.getColumnIndex("readed")) == 1);
			oldAnswerLog.setDownloaded(cursor.getInt(cursor.getColumnIndex("downloaded")) == 1);
			oldAnswerLog.setSubmmited(cursor.getInt(cursor.getColumnIndex("submmited")) == 1);
			if(saved==1){
				oldAnswerLogs.add(oldAnswerLog);
				i++;
			}
		}
		cursor.close();
		return oldAnswerLogs ;
		
		
	}
	private String getColumnIndex(Cursor cursor,String str){
		if (cursor.getColumnIndex(str)<0){
			return "";
		}else{
			
			return cursor.getString(cursor.getColumnIndex(str));
		}
		
	}
	
	public long deleteAll(){
		SQLiteDatabase db = getWritableDB();
		return db.delete("paperhistory", null, null);
		
		
	}

	/**
	 * renyangyang
	 * @return
	 */
	public OldAnswerLog queryByID(OldAnswerLog paper) {
		SQLiteDatabase db = getReadableDB();
		Cursor cursor = db.rawQuery(SELECT_BY_ID_SQL,
				new String[] { String.valueOf(paper.getUserId()), String.valueOf(paper.getSubjectId()), String.valueOf(paper.getUid()) });
		OldAnswerLog examinationPaper = null;
		int index = 0;
		while (cursor.moveToNext()) {
			examinationPaper = new OldAnswerLog();
			examinationPaper.setUid(cursor.getInt(cursor.getColumnIndex("examid")));
			examinationPaper.setUserId(cursor.getInt(cursor.getColumnIndex("userid")));
			// examinationPaper.setExamId(cursor.getInt(cursor.getColumnIndex("examid")));
			examinationPaper.setSubjectId(cursor.getInt(cursor.getColumnIndex("subjectid")));
			examinationPaper.setExamType(cursor.getInt(cursor.getColumnIndex("examtype")));
			examinationPaper.setSubject(cursor.getString(cursor.getColumnIndex("subject")));
			examinationPaper.setDescription(cursor.getString(cursor.getColumnIndex("description")));
			examinationPaper.setExamTime(cursor.getLong(cursor.getColumnIndex("examtime")));
			examinationPaper.setLimitTime(cursor.getLong(cursor.getColumnIndex("limittime")));
			examinationPaper.setCheckStatue(cursor.getInt(cursor.getColumnIndex("checkstatue")));
			examinationPaper.setExaminationHistoryId(cursor.getInt(cursor.getColumnIndex("examinationhistoryid")));
			int saved = cursor.getInt(cursor.getColumnIndex("saved"));
			examinationPaper.setSaved(saved == 1);
			examinationPaper.setUpdateTime(cursor.getLong(cursor.getColumnIndex("updatetime")));
			examinationPaper.setReaded(cursor.getInt(cursor.getColumnIndex("readed")) == 1);
			examinationPaper.setDownloaded(cursor.getInt(cursor.getColumnIndex("downloaded")) == 1);
			examinationPaper.setSubmmited(cursor.getInt(cursor.getColumnIndex("submmited")) == 1);
		}
		cursor.close();
		return examinationPaper;
	}

	/**
	 * 获取成绩单所对应的试题记录
	 * @return
	 */
	public List<OldCollection> getQuestionsByPaper(String userId,String subjectId,String examinationId){
		SQLiteDatabase db = getReadableDB();
		List<OldCollection> questions=new ArrayList<OldCollection>();
		int index;
		Cursor cursor = db.rawQuery(SELECT_SQL, new String[]{userId,subjectId,examinationId});
		while(cursor.moveToNext()){

			index=0;
			OldCollection question=new OldCollection();
			question.setUserId(cursor.getInt(cursor.getColumnIndex("userid")));
			question.setSubjectId(cursor.getInt(cursor.getColumnIndex("subjectid")));
			question.setUid(cursor.getInt(cursor.getColumnIndex("questionid")));
			question.setExamId(cursor.getInt(cursor.getColumnIndex("examid")));
			question.setExamType(cursor.getInt(cursor.getColumnIndex("examtype")));
			question.setChoiceType(cursor.getInt(cursor.getColumnIndex("choicetype")));
			question.setRealAnswer(cursor.getString(cursor.getColumnIndex("realanswer")));
			question.setAnswerLocal(cursor.getString(cursor.getColumnIndex("answerlocal")));
			question.setBigSubject(cursor.getString(cursor.getColumnIndex("bigsubject")));
			question.setQuizAnalyze(cursor.getString(cursor.getColumnIndex("quizanalyze")));
			question.setTag(cursor.getString(cursor.getColumnIndex("tag")));
			question.setDescription(cursor.getString(cursor.getColumnIndex("description")));
			question.setAttachOptionNum(cursor.getInt(cursor.getColumnIndex("attachoptionnum")));
			question.setScore(cursor.getInt(cursor.getColumnIndex("score")));
			question.setSelfType(cursor.getInt(cursor.getColumnIndex("selftype")));
			question.setAnswerRight(cursor.getString(cursor.getColumnIndex("answerright")));
			question.setRight((cursor.getInt(cursor.getColumnIndex("isright"))));
			questions.add(question);
		}
		cursor.close();

		return questions;

	}

	public Map<String,String> getLastQUestionIndex(OldAnswerLog paper){
		SQLiteDatabase db = getReadableDB();
		Map<String,String>map=new HashMap<String, String>();
		int index=-1;
		Cursor cursor = db.rawQuery(SELECT_INDEX_SQL, new String[]{String.valueOf(paper.getUserId()),String.valueOf(paper.getSubjectId()),String.valueOf(paper.getUid())} );
		if(cursor.moveToNext()){
			map.put("writenum", String.valueOf(cursor.getInt(cursor.getColumnIndex("writenum"))));
			map.put("totaltime", String.valueOf(cursor.getLong(cursor.getColumnIndex("totaltime"))));
//			map.put("errornum", String.valueOf(cursor.getLong(cursor.getColumnIndex("errornum"))));
//			map.put("rightnum", String.valueOf(cursor.getLong(cursor.getColumnIndex("rightnum"))));
//			map.put("percent", String.valueOf(cursor.getLong(cursor.getColumnIndex("percent"))));
//			map.put("totalpoints", String.valueOf(cursor.getLong(cursor.getColumnIndex("totalpoints"))));
		}
		cursor.close();
		return map;

	}
}
