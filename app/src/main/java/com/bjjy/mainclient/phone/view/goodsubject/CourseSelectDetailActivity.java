package com.bjjy.mainclient.phone.view.goodsubject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bjjy.mainclient.persenter.CourseSelectPersenter;
import com.bjjy.mainclient.phone.app.BaseActivity;
import com.bjjy.mainclient.phone.view.goodsubject.adapter.CourseSelectAdapter;
import com.bjjy.mainclient.phone.view.goodsubject.views.CourseSelectView;
import com.bjjy.mainclient.phone.widget.pulltorefresh.PullToRefreshBase;
import com.bjjy.mainclient.phone.widget.pulltorefresh.PullToRefreshExpandableListView;
import com.dongao.mainclient.model.bean.courselect.Exams;
import com.dongao.mainclient.model.bean.courselect.Goods;
import com.dongao.mainclient.model.bean.courselect.GoodsKind;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.AppContext;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CourseSelectDetailActivity extends BaseActivity implements CourseSelectView, PullToRefreshBase.OnRefreshListener<ExpandableListView>, OnGroupClickListener, ExpandableListView.OnChildClickListener {

    @Bind(R.id.top_title_left)
    ImageView top_title_left;

    @Bind(R.id.top_title_text)
    TextView top_title_text;
    @Bind(R.id.tv_pay)
    TextView tvPay;
    @Bind(R.id.select_num_tv)
    TextView selectNumTv;

    private PullToRefreshExpandableListView listview;
    private CourseSelectAdapter adapter;
    private CourseSelectPersenter precenter;
    private int num;
    private List<Goods> list;
    private ArrayList<GoodsKind> goodsKinds;
    private boolean isFirst;

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    Goods good=(Goods)msg.obj;
                    for(int i=0;i<list.size();i++){
                        if(list.get(i).getGoodsId().equals(good.getGoodsId())){
                            list.remove(i);
                        }
                    }
                    AppContext.getInstance().setGoods(list);
                    num=num-1;
                    selectNumTv.setText("已选课程" + "（" + num + "）");
                    break;
                case 1:
                    Goods goods=(Goods)msg.obj;
                    list.add(goods);
                    AppContext.getInstance().setGoods(list);
                    num=num+1;
                    selectNumTv.setText("已选课程" + "（" + num + "）");
                    break;
            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.courselect_activity);
        ButterKnife.bind(this);
        precenter = new CourseSelectPersenter();
        precenter.attachView(this);
        if(AppContext.getInstance().getGoods()!=null && AppContext.getInstance().getGoods().size()>0){
            list=AppContext.getInstance().getGoods();
            num=list.size();
            selectNumTv.setText("已选课程" + "（" + num + "）");
        }else{
            list=new ArrayList<>();
        }
        initView();
        initData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(isFirst){
            list=AppContext.getInstance().getGoods();
            adapter.setList(reSetList(goodsKinds));
            adapter.notifyDataSetChanged();
            selectNumTv.setText("已选课程" + "（" + list.size() + "）");
        }
    }

    @Override
    public void initView() {

        Exams exam=(Exams)getIntent().getSerializableExtra("exam");

        top_title_left.setVisibility(View.VISIBLE);
        top_title_left.setImageResource(R.drawable.back);
        top_title_left.setOnClickListener(this);
        top_title_text.setText(exam.getExamName());
        tvPay.setOnClickListener(this);

        adapter = new CourseSelectAdapter(CourseSelectDetailActivity.this, handler);
        listview = (PullToRefreshExpandableListView) findViewById(R.id.expand);
        listview.setOnRefreshListener(this);

        listview.getRefreshableView().setOnGroupClickListener(this);
        listview.getRefreshableView().setOnChildClickListener(this);
    }

    @Override
    public void initData() {
        precenter.getData();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_pay:
                if(list.size()>0){
                    isFirst=true;
                    Intent intent = new Intent(CourseSelectDetailActivity.this, SelectedCourseActivity.class);
                    intent.putExtra("goodsList",(Serializable)list);
                    startActivity(intent);
                }else{
                    Toast.makeText(CourseSelectDetailActivity.this, "你还没有选商品", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    public void onRefresh(PullToRefreshBase<ExpandableListView> refreshView) {
        listNum=0;
        initData();
        listview.onRefreshComplete();
    }

    @Override
    public boolean onGroupClick(ExpandableListView arg0, View arg1, int arg2,
                                long arg3) {
        return true;
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

        return false;
    }

    @Override
    public void setAdapter(ArrayList<GoodsKind> examList) {
        goodsKinds=examList;
        if(list!=null && list.size()>0){
            ArrayList<GoodsKind> listss=reSetList(examList);
            adapter.setList(listss);
        }else{
            adapter.setList(examList);
        }

        listview.getRefreshableView().setAdapter(adapter);
        int count = listview.getRefreshableView().getCount();
        for (int i = 0; i < count; i++) {
            listview.getRefreshableView().expandGroup(i);
        }
    }

    private int listNum;
    private ArrayList<GoodsKind> reSetList(ArrayList<GoodsKind> examList){
        for(int i=0;i<examList.size();i++){
            if(listNum==list.size()){
                return examList;
            }
            List<Goods> goodsList= examList.get(i).getGoodsList();
            for(int j=0;j<goodsList.size();j++){
                if(listNum==list.size()){
                    return examList;
                }
                String goodsId= goodsList.get(j).getGoodsId();
                for(int k=0;k<list.size();k++){
                    String id=list.get(k).getGoodsId();
                    if(goodsId.equals(id)){
                        goodsList.get(j).setSlected(true);
                        listNum=listNum+1;
                        break;
                    }
                }
            }
        }
        return examList;
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public Context context() {
        return null;
    }

}
