package com.bjjy.mainclient.phone.view.goodsubject.views;


import com.dongao.mainclient.model.mvp.MvpView;
import com.dongao.mainclient.model.bean.courselect.Exams;

import java.util.ArrayList;


/**
 * 登录UI 定义UI 看此UI中有何事件
 */
public interface CourseCenterView extends MvpView {
  void setAdapter(ArrayList<Exams> examList);
//  void refresh();
}
