package com.bjjy.mainclient.phone.view.download.db;

import android.content.Context;

import com.dongao.mainclient.model.local.KaoQianDBHelper;
import com.bjjy.mainclient.phone.view.download.DownloadTask;
import com.yunqing.core.db.DBExecutor;
import com.yunqing.core.db.sql.Sql;
import com.yunqing.core.db.sql.SqlFactory;

import java.util.List;

/**
 * Created by wyc on 15/4/15.
 * 此类DAO只用于查询和特殊操作
 */
public class DownloadDB {
    DBExecutor dbExecutor = null;
    Sql sql = null;

    public DownloadDB(Context mContext){
        dbExecutor = DBExecutor.getInstance(KaoQianDBHelper.getInstance(mContext));
    }

    public void insert(DownloadTask task){
        dbExecutor.insert(task);
    }

    public void update(DownloadTask ts){
        dbExecutor.updateById(ts);
    }

    public DownloadTask find(int id){
        return dbExecutor.findById(DownloadTask.class,id);
    }

    public DownloadTask findByTaskId(int taskId){
        sql = SqlFactory.find(DownloadTask.class).where("taskId","=",taskId);
        return dbExecutor.executeQueryGetFirstEntry(sql);
    }

    public DownloadTask findByCwId(int cwId){
        sql = SqlFactory.find(DownloadTask.class).where("cwId","=",cwId);
        return dbExecutor.executeQueryGetFirstEntry(sql);
    }

    public List<DownloadTask> findAll(){
        sql = SqlFactory.find(DownloadTask.class).where("status","!=",DownloadTask.FINISHED);
        return dbExecutor.executeQuery(sql);
    }

    public List<DownloadTask> findAllByUserId(String userId){
        sql = SqlFactory.find(DownloadTask.class).where("userId","=",userId);
        return dbExecutor.executeQuery(sql);
    }

    public List<DownloadTask> findAll(String userId){
        sql = SqlFactory.find(DownloadTask.class).where("userId=? and status=?",new Object[]{userId,DownloadTask.WAITING}).orderBy("updateDate",true);
        return dbExecutor.executeQuery(sql);
    }
}
