package com.bjjy.mainclient.phone.view.daytest.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.dongao.mainclient.model.bean.daytest.DayExSelectedSubject;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.view.home.view.PMDTextView;

import java.util.List;

/**
 * Created by dell on 2016/5/20 0020.
 */
public class DayTestSelectSubjectAdapter extends BaseAdapter {

    private Context context;
    private List<DayExSelectedSubject> dayExSelectedSubjects;

    private GridView gv;

    public DayTestSelectSubjectAdapter(GridView gv,Context context,List<DayExSelectedSubject> dayExSelectedSubjects){
        this.gv = gv;
        this.context = context;
        this.dayExSelectedSubjects = dayExSelectedSubjects;
    }

    @Override
    public int getCount() {
        return dayExSelectedSubjects.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.day_test_selectsubject_exam_item,parent,false);
            viewHolder = new ViewHolder();
//            viewHolder.textView = (TextView)convertView.findViewById(R.id.day_test_selectsubject_exam_item_name_tv);
            viewHolder.textView = (PMDTextView)convertView.findViewById(R.id.day_test_selectsubject_exam_item_name_tv);
            viewHolder.relativeLayout_select_tag = (RelativeLayout)convertView.findViewById(R.id.day_test_selectsubject_exam_item_tag_rl);
            viewHolder.imageView_select_tag = (ImageView)convertView.findViewById(R.id.day_test_selectsubject_exam_item_tag_img);
//            viewHolder.line_top = convertView.findViewById(R.id.day_test_selectsubject_exam_item_line_top);
//            viewHolder.line_right = convertView.findViewById(R.id.day_test_selectsubject_exam_item_line_right);
            convertView.setTag(viewHolder);
            viewHolder.update();
        }else{
            viewHolder = (ViewHolder)convertView.getTag();
        }
        viewHolder.textView.setTag(convertView);
        viewHolder.imageView_select_tag.setTag(position);
//        if(position == (dayExSelectedSubjects.size()-1)){
//            viewHolder.line_right.setVisibility(View.GONE);
//        }else{
//            viewHolder.line_right.setVisibility(View.VISIBLE);
//        }

//        if(position== 0 || position == 1){
//            viewHolder.line_top.setVisibility(View.GONE);
//        }else{
//            viewHolder.line_top.setVisibility(View.VISIBLE);
//        }

        viewHolder.textView.setText(dayExSelectedSubjects.get(position).getSubjectName());
        if(dayExSelectedSubjects.get(position).getSelectedTag() == 1){
            viewHolder.relativeLayout_select_tag.setBackgroundColor(context.getResources().getColor(R.color.color_primary));
            viewHolder.imageView_select_tag.setVisibility(View.VISIBLE);
        }else{
            viewHolder.relativeLayout_select_tag.setBackgroundColor(Color.WHITE);
            viewHolder.imageView_select_tag.setVisibility(View.GONE);
        }

        return convertView;
    }

    public class ViewHolder{
//        TextView textView;
        PMDTextView textView;
        RelativeLayout relativeLayout_select_tag;
        ImageView imageView_select_tag;
        int maxHeight;
//        View line_right;
//        View line_top;
        public void update() {
        // 精确计算GridView的item高度
            textView.getViewTreeObserver().addOnGlobalLayoutListener(
            new ViewTreeObserver.OnGlobalLayoutListener() {
                public void onGlobalLayout() {
                    int position = (Integer) imageView_select_tag.getTag();
                    // 这里是保证同一行的item高度是相同的！！也就是同一行是齐整的 height相等
                    if (position > 0 && position % 2 == 1) {
                        View v = (View) textView.getTag();
                        int height = v.getHeight();

                        View view = gv.getChildAt(position - 1);
                        int lastheight = view.getHeight();
                        // 得到同一行的最后一个item和前一个item想比较，把谁的height大，就把两者中                                                                // height小的item的高度设定为height较大的item的高度一致，也就是保证同一                                                                 // 行高度相等即可
                        if (height > lastheight) {
                            if(maxHeight==0 || maxHeight<height){
                                maxHeight = height;
                                view.setLayoutParams(new GridView.LayoutParams(
                                        GridView.LayoutParams.FILL_PARENT,
                                        height));
                            }
                            else {
                                view.setLayoutParams(new GridView.LayoutParams(
                                        GridView.LayoutParams.FILL_PARENT,
                                        maxHeight));
                            }
                        } else if (height < lastheight) {
                            if(maxHeight==0 || maxHeight<lastheight){
                                maxHeight = lastheight;
                                view.setLayoutParams(new GridView.LayoutParams(
                                        GridView.LayoutParams.FILL_PARENT,
                                        lastheight));
                            }
                            else {
                                view.setLayoutParams(new GridView.LayoutParams(
                                        GridView.LayoutParams.FILL_PARENT,
                                        maxHeight));
                            }
                        }
                    }
                }
            });
        }

    }

}
