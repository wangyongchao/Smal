package com.bjjy.mainclient.phone.view.download.local;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseFragmentActivity;
import com.bjjy.mainclient.phone.event.PushMsgNotification;
import com.bjjy.mainclient.phone.view.download.local.adapter.OfflineSwipeAdapter;
import com.bjjy.mainclient.phone.view.studybar.Constant;
import com.bjjy.mainclient.phone.view.studybar.view.RefreshLayout;
import com.bjjy.mainclient.phone.widget.EmptyViewLayout;
import com.bjjy.mainclient.phone.widget.swipe.SwipeMenu;
import com.bjjy.mainclient.phone.widget.swipe.SwipeMenuCreator;
import com.bjjy.mainclient.phone.widget.swipe.SwipeMenuItem;
import com.bjjy.mainclient.phone.widget.swipe.SwipeMenuListView;
import com.dongao.mainclient.core.util.DensityUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by wyc on 2016/6/6.
 */
public class DownloadLocalActivity extends BaseFragmentActivity implements DownloadLocalView {
    @Bind(R.id.top_title_left)
    ImageView top_title_left;
    @Bind(R.id.top_title_right)
    ImageView top_title_right;
    @Bind(R.id.top_title_text)
    TextView top_title_text;
    @Bind(R.id.offline_list)
    SwipeMenuListView offline_list;
    private SwipeMenuCreator creator;
    private OfflineSwipeAdapter offlineSwipeAdapter;

    @OnClick(R.id.top_title_left) void onBackClick(){
        onBackPressed();
    }
    private DownloadLocalPercenter privateTeacherPercenter;
    private EmptyViewLayout mEmptyLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.localcourse_offline_activity);
        ButterKnife.bind(this);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        privateTeacherPercenter = new DownloadLocalPercenter();
        privateTeacherPercenter.attachView(this);
        initView();
        initData();
    }

    @Override
    public void initView() {
        mEmptyLayout = new EmptyViewLayout(this, offline_list);
        mEmptyLayout.setErrorButtonClickListener(mErrorClickListener);
        mEmptyLayout.setEmptyButtonClickListener(emptyButtonClickListener);
        createSwipe();

    }
    private void createSwipe() {
        creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {

                // create "open" item
                SwipeMenuItem openItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                openItem.setBackground(new ColorDrawable(Color.rgb(0xff, 0x00,
                        0x00)));
                // set item width
                openItem.setWidth(DensityUtil.dip2px(DownloadLocalActivity.this,75));
                // set item title
                openItem.setTitle(getResources().getString(R.string.delete));
                // set item title fontsize
                openItem.setTitleSize(18);
                // set item title font color
                openItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(openItem);
            }
        };
    }


    /**
     * 错误监听
     */
    private View.OnClickListener mErrorClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        }
    };
    /**
     * 无数据监听
     */
    private View.OnClickListener emptyButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //showAppMsg("显示kong");
        }
    };

    @Override
    public void initData() {
        mEmptyLayout.showLoading();
        privateTeacherPercenter.initData();
    }

    @Override
    public void showLoading() {
    }

    @Override
    public void hideLoading() {
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public Context context() {
        return DownloadLocalActivity.this;
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void initAdapter() {
        if (offlineSwipeAdapter==null){
            offlineSwipeAdapter=new OfflineSwipeAdapter(this,privateTeacherPercenter.courseList);
            offline_list.setAdapter(offlineSwipeAdapter);
            offline_list.setMenuCreator(creator);
            changSwipe();
        }else{
            offlineSwipeAdapter.notifyDataSetChanged();
        }
    }

    /**
     * 设置swipe的点击事件的响应
     */
    private void changSwipe() {
        offline_list.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        privateTeacherPercenter.deleteCourse(position);
                        break;
                    case 1:
                        privateTeacherPercenter.remove(position);
                        break;
                }
                return false;
            }
        });
        offline_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                privateTeacherPercenter.setOnItemClick(i);
                
            }
        });

        // set SwipeListener
        offline_list.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {

            @Override
            public void onSwipeStart(int position) {
            }

            @Override
            public void onSwipeEnd(int position) {
            }
        });

        // set MenuStateChangeListener
        offline_list.setOnMenuStateChangeListener(new SwipeMenuListView.OnMenuStateChangeListener() {
            @Override
            public void onMenuOpen(int position) {
            }

            @Override
            public void onMenuClose(int position) {
            }
        });
    }
    
    
    
    @Override
    public void showContentView(int type) {
        if (type== Constant.VIEW_TYPE_0){
            mEmptyLayout.showContentView();
        }else  if (type==Constant.VIEW_TYPE_1){
            mEmptyLayout.showNetErrorView();
        }else  if (type==Constant.VIEW_TYPE_2){
            mEmptyLayout.showEmpty();
        }else  if (type==Constant.VIEW_TYPE_3){
            mEmptyLayout.showError();
        }
        hideLoading();
    }

    @Override
    public boolean isRefreshNow() {
        return false;
    }

    @Override
    public RefreshLayout getRefreshLayout() {
        return null;
    }

    @Override
    public Intent getTheIntent() {
        return getIntent();
    }

    @Override
    public void finishActivity() {
        onBackPressed();
    }

    @Override
    public void showTopTitle(String title) {
        top_title_text.setText(title);
    }

    @Override
    public void setNoDataMoreShow(boolean isShow) {
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Subscribe
    public void onEventAsync(PushMsgNotification event) {
        initData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        initData();
    }
}
