package com.bjjy.mainclient.phone.view.studybar.push;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import com.alibaba.fastjson.JSON;
import com.bjjy.mainclient.phone.view.question.MyQuestionListActivity;
import com.bjjy.mainclient.phone.view.studybar.push.bean.PushMessage;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.bjjy.mainclient.phone.view.main.MainActivity;
import com.bjjy.mainclient.phone.view.setting.WebViewActivity;
import com.bjjy.mainclient.phone.view.studybar.push.bean.Push;
import com.bjjy.mainclient.phone.view.user.LoginNewActivity;

import java.util.List;

/**
 * Created by wyc on 2016/6/13.
 */
public class PushUtils {
    /**
     * 开启app
     * @param context
     */
    public static void openApp(Context context){
        try {
            if (isRunningForeground(context)){
                PackageManager packageManager =context.getPackageManager();
                Intent intent=new Intent();
                intent = packageManager.getLaunchIntentForPackage("com.dongao.app.exam");
                context.startActivity(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
           /* Intent viewIntent = new
                    Intent("android.intent.action.VIEW", Uri.parse("http://weixin.qq.com/"));
            context.startActivity(viewIntent);*/
        }
    }

    /**
     * 开启activity
     * @param context
     */
    public static void openActivity(Context context,Push pushMessage){
       if (isAppAlive(context,"com.dongao.mainclient.phone")){
           if (SharedPrefHelper.getInstance(context).isLogin()){
               if (pushMessage.getMessageTypeValue().equals(Constants.PUSH_MESSAGE_TYPE_QUESTION)){
                   Intent mainIntent = new Intent(context, MainActivity.class);
                   mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                   Intent detailIntent = new Intent(context, MyQuestionListActivity.class);
                   if (pushMessage.getQuestionId()!=null){
                       detailIntent.putExtra("questionAnswerId", pushMessage.getQuestionId());
                   }
                   Intent[] intents = {mainIntent, detailIntent};
                   context.startActivities(intents);
               } else if(pushMessage.getMessageTypeValue().equals(Constants.PUSH_MESSAGE_TYPE_MESSAGE)){
                   Intent mainIntent = new Intent(context, MainActivity.class);
                   mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                   Intent detailIntent = new Intent(context, WebViewActivity.class);
                   if(pushMessage.getPushTypeName()==null ||pushMessage.getPushTypeName().isEmpty()){
                       pushMessage.setPushTypeName("小奥");
                   }
                   if (pushMessage.getUrl()==null||pushMessage.getUrl().isEmpty()){
                       pushMessage.setUrl("http://www.bjsteach.com/");
                   }
                   if (pushMessage.getPushMessageId()!=null){
                       detailIntent.putExtra("pushMessageId", pushMessage.getPushMessageId());
                   }
                   String url= pushMessage.getUrl()+"?userId="+SharedPrefHelper.getInstance(context).getUserId();
                   detailIntent.putExtra(Constants.APP_WEBVIEW_TITLE,pushMessage.getPushTypeName());
                   detailIntent.putExtra(Constants.APP_WEBVIEW_URL, url);
                   Intent[] intents = {mainIntent, detailIntent};
                   context.startActivities(intents);
               }/*else if(pushMessage.getMessageTypeValue().equals(Constants.PUSH_MESSAGE_TYPE_PRIVATE)){
                   Intent mainIntent = new Intent(context, MainActivity.class);
                   mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                   Intent detailIntent = new Intent(context, WebViewActivity.class);
                   if(pushMessage.getPushTypeName()==null ||pushMessage.getPushTypeName().isEmpty()){
                       pushMessage.setPushTypeName("小奥");
                   }
                   if (pushMessage.getUrl()==null||pushMessage.getUrl().isEmpty()){
                       pushMessage.setUrl("http://www.bjsteach.com/");
                   }
                   if (pushMessage.getPushMessageId()!=null){
                       detailIntent.putExtra("pushMessageId", pushMessage.getPushMessageId());
                   }
                   String url= pushMessage.getUrl()+"?id="+ pushMessage.getQuestionId();
                   detailIntent.putExtra(Constants.APP_WEBVIEW_TITLE,pushMessage.getPushTypeName());
                   detailIntent.putExtra(Constants.APP_WEBVIEW_URL, url);
                   Intent[] intents = {mainIntent, detailIntent};
                   context.startActivities(intents);
               }*/else{
                   Intent mainIntent = new Intent(context, MainActivity.class);
                   mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                   Intent detailIntent = new Intent(context, WebViewActivity.class);
                   detailIntent.putExtra(Constants.APP_WEBVIEW_TITLE,pushMessage.getPushTypeName());
                   detailIntent.putExtra(Constants.APP_WEBVIEW_URL, pushMessage.getUrl());
                   Intent[] intents = {mainIntent, detailIntent};
                   context.startActivities(intents);
               }

           }else{
               Intent mainIntent = new Intent(context, MainActivity.class);
               mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
               Intent detailIntent = new Intent(context, LoginNewActivity.class);
               String content=JSON.toJSONString(pushMessage);
               detailIntent.putExtra("pushMessage", content);
               detailIntent.putExtra(Constants.LOGIN_TYPE_KEY,Constants.LOGIN_TYPE_PUSH);
               Intent[] intents = {mainIntent, detailIntent};
               context.startActivities(intents);
           }
       }else {
           Intent launchIntent = context.getPackageManager().
                   getLaunchIntentForPackage("com.dongao.mainclient.phone");
           launchIntent.setFlags(
                   Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
           Bundle args = new Bundle();
           launchIntent.putExtra(Constants.PUSH_EXTRA_BUNDLE_LAUNCH, args);
           context.startActivity(launchIntent);
       }
    }

    /**
     * 判断当前app在前台或后台
     * @param context
     */
    public static boolean isRunningForeground(Context context){
        ActivityManager activityManager = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager
                .getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.processName.equals(context.getPackageName())) {  
                if (appProcess.importance != ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    /**
     * 判断应用是否已经启动
     * @param context 一个context
     * @param packageName 要判断应用的包名
     * @return boolean
     */
    public static boolean isAppAlive(Context context, String packageName){
        ActivityManager activityManager =
                (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> processInfos
                = activityManager.getRunningAppProcesses();
        for(int i = 0; i < processInfos.size(); i++){
            if(processInfos.get(i).processName.equals(packageName)){
                return true;
            }
        }
        return false;
    }

    /**
     * 获取推送信息
     * @param context 一个context
     * @param info jason 包
     * @return boolean
     */
    public static void getAppInfo(Context context, String info){
        try {
            PushMessage pushMessage = JSON.parseObject(info, PushMessage.class);
            if(pushMessage == null){
                return;
            }
            openActivity(context,pushMessage.getParams());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
