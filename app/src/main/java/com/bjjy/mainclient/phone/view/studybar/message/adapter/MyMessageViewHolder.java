package com.bjjy.mainclient.phone.view.studybar.message.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bjjy.mainclient.phone.view.studybar.privateteacher.adapter.PrivateTeacherAdapter;
import com.bjjy.mainclient.phone.R;

public class MyMessageViewHolder extends RecyclerView.ViewHolder {

    public final TextView tv_content;
    public final TextView tv_title,tv_time;
    public final ImageView iv_read;
    private PrivateTeacherAdapter.ReclerViewItemClick reclerViewItemClick;

    public MyMessageViewHolder(View itemView) {
        super(itemView);
        tv_content = (TextView) itemView.findViewById(R.id.tv_content);
        tv_title = (TextView) itemView.findViewById(R.id.tv_title);
        iv_read= (ImageView) itemView.findViewById(R.id.iv_read);
        tv_time= (TextView) itemView.findViewById(R.id.tv_time);
    }

}
