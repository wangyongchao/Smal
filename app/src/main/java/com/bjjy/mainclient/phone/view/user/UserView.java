package com.bjjy.mainclient.phone.view.user;

import com.dongao.mainclient.model.mvp.MvpView;

/**
 * Created by fengzongwei on 2016/5/6 0006.
 */
public interface UserView extends MvpView{
    String username();//登录账号
    String password();//登录密码
    void loginSuccess();//登录成功后的动作
    String phoneNumber();//注册手机号
    String checkNumber();//手机验证码
    String pswRegiste();//注册的密码
    void registeSuccess();//注册成功后动作
    void switchBtStatus();//切换获取验证码按钮的状态
}
