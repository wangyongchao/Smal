package com.bjjy.mainclient.phone.view.classroom.liveplay;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.view.classroom.liveplay.bean.LivesPlayBean;
import com.bjjy.mainclient.phone.view.play.domain.CwStudyLogDB;
import com.dongao.mainclient.model.local.CourseDetailDB;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import java.util.List;

/**
 * Created by dell on 2016/5/16.
 */
public class LivePlayExpandAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private List<LivesPlayBean> mList;
    private ImageLoader imageLoader;
    private CwStudyLogDB cwStudyLogDB;
    private CourseDetailDB courseDetailDB;
    private String userId,subjectId;
    private DisplayImageOptions options;

    public LivePlayExpandAdapter(Context context, ImageLoader imageLoader, DisplayImageOptions options) {
        super();
        this.mContext = context;
        this.imageLoader=imageLoader;
        this.options=options;
        cwStudyLogDB=new CwStudyLogDB(context);
        courseDetailDB=new CourseDetailDB(context);
    }

    public void setList(List<LivesPlayBean> list) {
        this.mList = list;
    }

    @Override
    public int getGroupCount() {
        return mList.size();
    }
    
    //TODO
    //TODO
    //TODO
    //TODO
    //TODO
    //TODO
    @Override
    public int getChildrenCount(int groupPosition) {
//        return mList.get(groupPosition).getCourseItems().size();
        return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return null;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View view=View.inflate(mContext,R.layout.course_live_item_group,null);
        TextView name=(TextView)view.findViewById(R.id.tv_name);
//        name.setText(mList.get(groupPosition).getCourseTypeName());
        return view;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ViewHolder holder;
//        if(convertView==null){
//            holder = new ViewHolder();
//            convertView=View.inflate(mContext, R.layout.course_live_item, null);
//            holder.title = (TextView) convertView.findViewById(R.id.courseplay_title);
//            holder.count = (TextView) convertView.findViewById(R.id.courseplay_count);
//            holder.img = (ImageView) convertView.findViewById(R.id.courseplay_img);
//            holder.playPro = (ImageView) convertView.findViewById(R.id.img_play);
//            holder.bottom = (TextView) convertView.findViewById(R.id.tv_child_bottom);
//            convertView.setTag(holder);
//        }else{
//            holder = (ViewHolder) convertView.getTag();
//        }
//
//        holder.title.setText(mList.get(groupPosition).getCourseItems().get(childPosition).getName());
//        if(!TextUtils.isEmpty(mList.get(groupPosition).getCourseItems().get(childPosition).getProgressSuggested())){
//            holder.bottom.setText(mList.get(groupPosition).getCourseItems().get(childPosition).getProgressSuggested() + "  " + mList.get(groupPosition).getCourseItems().get(childPosition).getUpdateTime());
//        }else {
//            holder.bottom.setText("课程暂未更新");
//        }
//
//        subjectId=SharedPrefHelper.getInstance(mContext).getSubjectId();
//        userId= SharedPrefHelper.getInstance(mContext).getUserId();
//       // String computerFlag=mList.get(groupPosition).getCourseItems().get(childPosition).getComputerExamFlag();
//        List<CwStudyLog> cwStudyLogs = cwStudyLogDB.query(userId,mList.get(groupPosition).getCourseItems().get(childPosition).getId());
//        int countatal=courseDetailDB.getCourseCount(subjectId,mList.get(groupPosition).getCourseItems().get(childPosition).getId());
//        /*if(computerFlag.equals("1")){
//            holder.count.setTextColor(mContext.getResources().getColor(R.color.textColor));
//            holder.count.setText("请使用PC端学习");
//            holder.playPro.setImageResource(R.drawable.course_play);
//        }else{*/
//            if(cwStudyLogs!=null && cwStudyLogs.size()>0){
//                holder.count.setTextColor(mContext.getResources().getColor(R.color.pay_total_price));
//                if(countatal!=-1){
//                    if(SharedPrefHelper.getInstance(mContext).getIsAreadyStudyOver().equals("NO")){
//                        holder.count.setText("已学习"+cwStudyLogs.size()+"/"+countatal);
//                        holder.playPro.setImageResource(R.drawable.continue_course);
//                    }else{
//                        if(cwStudyLogs.size()==countatal){
//                            holder.count.setText("已学完");
//                            holder.playPro.setImageResource(R.drawable.complete_course);
//                        }else{
//                            holder.count.setText("已学习"+cwStudyLogs.size()+"/"+countatal);
//                            holder.playPro.setImageResource(R.drawable.continue_course);
//                        }
//                    }
//                }else{
//                    holder.count.setText("学习中");
//                    holder.playPro.setImageResource(R.drawable.continue_course);
//                }
//            }else{
//                holder.count.setTextColor(mContext.getResources().getColor(R.color.textColor));
//                String type=mList.get(groupPosition).getCourseItems().get(childPosition).getType();
//                if(TextUtils.isEmpty(type)){
//                    holder.count.setText("未学习");
//                }else{
//                    holder.count.setText("暂未开课");
//                }
////                if (mList.get(groupPosition).getCourseItems().get(childPosition).getType().equals("66")){
////                    holder.count.setText("暂未开课");
////                }else{
////                    holder.count.setText("未学习");
////                }
//                holder.playPro.setImageResource(R.drawable.course_play);
//                
//            }
//       // }
//        String imgPath=mList.get(groupPosition).getCourseItems().get(childPosition).getDaPersonImg();
//        if(!TextUtils.isEmpty(imgPath) && imgPath.startsWith("http")){
//            imageLoader.displayImage(mList.get(groupPosition).getCourseItems().get(childPosition).getDaPersonImg(),holder.img,options);
//        }
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    class ViewHolder {
        TextView title;
        TextView count;
        ImageView img;
        TextView bottom;
        ImageView playPro;
    }
}
