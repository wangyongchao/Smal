package com.bjjy.mainclient.phone.view.setting.cache.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.bjjy.mainclient.phone.view.setting.cache.fragment.CacheCourseFragment;
import com.bjjy.mainclient.phone.view.setting.cache.fragment.CachedownloadFragment;

/**
 * Created by dell on 2016/5/16.
 */
public class CacheCourseAdapter extends FragmentPagerAdapter {

    private Fragment fragment;
    public CacheCourseAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if(position==0){
            fragment= new CacheCourseFragment();
        }else if(position==1){
            fragment= new CachedownloadFragment();
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        if(position==0){
            fragment= (CacheCourseFragment) super.instantiateItem(container, position);
        }else if(position==1){
            fragment= (CachedownloadFragment) super.instantiateItem(container, position);
        }
        return fragment;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return super.getPageTitle(position);
    }
}
