package com.bjjy.mainclient.phone.view.persenal;

import com.dongao.mainclient.model.mvp.MvpView;
import com.dongao.mainclient.model.bean.user.UserInfo;

/**
 * Created by fengzongwei on 2016/6/20 0020.
 */
public interface PersenalView extends MvpView{
    void show(UserInfo userInfo);
}
