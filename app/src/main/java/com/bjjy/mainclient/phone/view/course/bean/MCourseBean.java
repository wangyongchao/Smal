package com.bjjy.mainclient.phone.view.course.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by dell on 2017/10/30.
 */

public class MCourseBean implements Serializable{
	private String id;
	private String accountId;
	private String showName;
	private String ruleId;
	private String year;
	private List<MCourseDetailBean> courseDetailInfo;
	private String commodityId;
	private int pdId;
	private String imageUrl;

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getShowName() {
		return showName;
	}

	public void setShowName(String showName) {
		this.showName = showName;
	}

	public String getRuleId() {
		return ruleId;
	}

	public void setRuleId(String ruleId) {
		this.ruleId = ruleId;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public List<MCourseDetailBean> getCourseDetailInfo() {
		return courseDetailInfo;
	}

	public void setCourseDetailInfo(List<MCourseDetailBean> courseDetailInfo) {
		this.courseDetailInfo = courseDetailInfo;
	}

	public String getCommodityId() {
		return commodityId;
	}

	public void setCommodityId(String commodityId) {
		this.commodityId = commodityId;
	}

	public int getPdId() {
		return pdId;
	}

	public void setPdId(int pdId) {
		this.pdId = pdId;
	}
}
