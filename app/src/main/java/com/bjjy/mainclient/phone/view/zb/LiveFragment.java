package com.bjjy.mainclient.phone.view.zb;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseFragment;
import com.bjjy.mainclient.phone.event.PushMsgNotification;
import com.bjjy.mainclient.phone.view.download.local.SelectYearPopupwindow.SelectYearPopwindow;
import com.bjjy.mainclient.phone.view.download.local.bean.YearInfo;
import com.bjjy.mainclient.phone.view.exam.activity.myexam.adapter.PopUpWindowAdapter;
import com.bjjy.mainclient.phone.view.polyv.activity.PolyvPPTPlayerActivity;
import com.bjjy.mainclient.phone.view.polyv.activity.PolyvPlayerActivity;
import com.bjjy.mainclient.phone.view.polyv.permission.PolyvPermission;
import com.bjjy.mainclient.phone.view.studybar.Constant;
import com.bjjy.mainclient.phone.view.studybar.view.RefreshLayout;
import com.bjjy.mainclient.phone.view.zb.adapter.LivePlayAdapter;
import com.bjjy.mainclient.phone.widget.EmptyViewLayout;
import com.bjjy.mainclient.phone.widget.statusbar.Utils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.easefun.polyvsdk.live.chat.ppt.api.PolyvLiveMessage;
import com.easefun.polyvsdk.live.chat.ppt.api.entity.PolyvLiveMessageEntity;
import com.easefun.polyvsdk.live.chat.ppt.api.listener.PolyvLiveMessageListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author wyc
 */
public class LiveFragment extends BaseFragment implements LivePlayView, LivePlayAdapter.LivePlayDataBeanItemClickListener, BaseQuickAdapter.RequestLoadMoreListener {

	@Bind(R.id.top_title_left)
	ImageView top_title_left;
	@Bind(R.id.top_title_text)
	TextView top_title_text;
	@Bind(R.id.content_ll)
	LinearLayout content_ll;
	@Bind(R.id.swipe_container)
	SwipeRefreshLayout swipe_container;
	@Bind(R.id.rv_course)
	RecyclerView rv_course;
	@Bind(R.id.tv_right)
	TextView tv_right;
	@Bind(R.id.ll_top_right)
	LinearLayout ll_top_right;
	@Bind(R.id.iv_right_pop)
	ImageView iv_right_pop;
	@Bind(R.id.status_bar_fix)
	View status_bar_fix;
	private SelectYearPopwindow selectYearPopwindow;
	private LivePlayAdapter mAdapter;
	private boolean mHasLoadedOnce;
	private View mRootView;
	private LivePlayPercenter mPresenter;
	private EmptyViewLayout mEmptyLayout;
	private PolyvPermission polyvPermission;
	private static final int SETTING = 1;

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		if (this.isVisible()) {
			// we check that the fragment is becoming visible
			if (mHasLoadedOnce) {
//                cacheVp.setCurrentItem(0);
//                EventBus.getDefault().post(new DeleteEvent());
			}
			if (isVisibleToUser && !mHasLoadedOnce) {
				// async http request here
				mHasLoadedOnce = true;
				initData();
			}
		}
		super.setUserVisibleHint(isVisibleToUser);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (!EventBus.getDefault().isRegistered(this)) {
			EventBus.getDefault().register(this);
		}
//        mRootView =  inflater.inflate(R.layout.live_play_activity,null);
		mRootView = inflater.inflate(R.layout.live_play_activity, null);
		ButterKnife.bind(this, mRootView);
		ViewGroup parent = (ViewGroup) mRootView.getParent();
		if (parent != null) {
			parent.removeView(mRootView);
		}
		mPresenter = new LivePlayPercenter();
		mPresenter.attachView(this);
		initView();
		initData();
		return mRootView;
	}

	@Override
	public void initView() {
		setTranslucentStatus();
		swipe_container.setColorSchemeResources(R.color.color_primary);
		ll_top_right.setOnClickListener(this);
		top_title_left.setVisibility(View.INVISIBLE);
		ll_top_right.setVisibility(View.VISIBLE);
		iv_right_pop.setVisibility(View.VISIBLE);
		top_title_text.setText(getResources().getText(R.string.live_play_list_top));
		mEmptyLayout = new EmptyViewLayout(getActivity(), content_ll);
		mEmptyLayout.setErrorButtonClickListener(mErrorClickListener);
		mEmptyLayout.setEmptyButtonClickListener(emptyButtonClickListener);
		rv_course.setLayoutManager(new LinearLayoutManager(rv_course.getContext(), LinearLayoutManager.VERTICAL, false));
		swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				mPresenter.initData();
			}
		});
		initLivePlay();
	}

	private void initLivePlay() {
		polyvPermission = new PolyvPermission();
		polyvPermission.setResponseCallback(new PolyvPermission.ResponseCallback() {
			@Override
			public void callback() {
				requestPermissionWriteSettings();
			}
		});
	}

	/**
	 * 错误监听
	 */
	private View.OnClickListener mErrorClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {initData();
		}
	};
	/**
	 * 无数据监听
	 */
	private View.OnClickListener emptyButtonClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View view) {initData();
		}
	};

	@Override
	public void initData() {
		mEmptyLayout.showLoading();
		selectYearPopwindow = new SelectYearPopwindow(getActivity(), ll_top_right, iv_right_pop, onItemClickListener);
		mPresenter.initData();
	}

	/**
	 * 年份的点击事件
	 */
	private PopUpWindowAdapter.MainTypeItemClick onItemClickListener = new PopUpWindowAdapter.MainTypeItemClick() {
		@Override
		public void itemClick(int mainPosition, int itemType, int type, int position) {
			mPresenter.setOnItemClick(position);
		}
	};

	@Override
	public void onResume() {
		super.onResume();
		initData();
	}

	@Override
	public void showLoading() {
		if (swipe_container != null) swipe_container.setRefreshing(true);
	}

	@Override
	public void hideLoading() {
		if (swipe_container != null) swipe_container.setRefreshing(false);
	}

	@Override
	public void showRetry() {

	}

	@Override
	public void hideRetry() {

	}

	@Override
	public void showError(String message) {

	}

	@Override
	public Context context() {
		return getActivity();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.tv_right:
			case R.id.ll_top_right:
				selectYearPopwindow.showPop(mPresenter.yearList, mPresenter.currYear);
				break;
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		ButterKnife.unbind(this);
	}

	@Override
	public void initAdapter() {
		if (mAdapter == null) {
			mAdapter = new LivePlayAdapter(getActivity(), mPresenter.courseList);
			mAdapter.setLivePlayDataBeanClickListener(this);
			mAdapter.setEnableLoadMore(true);
			mAdapter.setOnLoadMoreListener(this, rv_course);
			rv_course.setAdapter(mAdapter);
		} else {
			mAdapter.setNewData( mPresenter.courseList);
			mAdapter.notifyDataSetChanged();
		}
	}

	@Override
	public void loadMoreStatus(int status) {
		switch (status) {
			case 0:
				mAdapter.loadMoreComplete();
				break;
			case 1:
				mAdapter.loadMoreEnd();
				break;
			case 2:
				mAdapter.loadMoreFail();
				break;
		}
	}

	@Override
	public void showContentView(int type) {
		if (type == Constant.VIEW_TYPE_0) {
			mEmptyLayout.showContentView();
		} else if (type == Constant.VIEW_TYPE_1) {
			mEmptyLayout.showNetErrorView();
		} else if (type == Constant.VIEW_TYPE_2) {
			mEmptyLayout.showEmpty();
		} else if (type == Constant.VIEW_TYPE_3) {
			mEmptyLayout.showError();
		}
		hideLoading();
	}

	@Override
	public boolean isRefreshNow() {
		return false;
	}

	@Override
	public RefreshLayout getRefreshLayout() {
		return null;
	}

	@Override
	public Intent getTheIntent() {
		return getActivity().getIntent();
	}

	@Override
	public void finishActivity() {

	}

	@Override
	public void showCurrentYear(YearInfo currYear) {
		tv_right.setText(currYear.getYearName());
	}

	@Override
	public void hideYearPop(boolean isHide) {
		if (isHide && selectYearPopwindow != null) {
			selectYearPopwindow.dissmissPop();
		}
	}

	@Override
	public void showTopTitle(String title) {
		top_title_text.setText(title);
	}

	@Override
	public void setNoDataMoreShow(boolean isShow) {
	}


	@Subscribe
	public void onEventAsync(PushMsgNotification event) {
		initData();
	}

	@Override
	public void LivePlayDataBeanClick(int position, long l) {
		mPresenter.setLiveItemClick(position, l);
		polyvPermission.applyPermission(getActivity(), PolyvPermission.OperationType.play);
	}

	@Override
	public void onLoadMoreRequested() {
		if (mPresenter.courseList.size() < Constants.PAGE_NUMBER||mPresenter.totalPage<mPresenter.currentPage) {
			mAdapter.loadMoreEnd();
		} else {
			mPresenter.loadMore();
		}
	}


	private void gotoPlay() {
		if (mPresenter.currenLiveBean == null) return;
		final String channelId = mPresenter.currenLiveBean.getChannelId();
//		final String userId = SharedPrefHelper.getInstance(getActivity()).getUserId();
		// TODO: 2017/11/9  
//        final  String channelId="136913";
		final  String userId="0a128c702d";
//		final  String userId="5c6c26bc19";
//		final  String channelId="132652";
		new PolyvLiveMessage().getLiveType(channelId, new PolyvLiveMessageListener() {
			@Override
			public void success(boolean b, PolyvLiveMessageEntity polyvLiveMessageEntity) {
				SharedPrefHelper.getInstance(getActivity()).setIsFreeCourse(mPresenter.isFreeCourse());
				Intent playUrl = b ? new Intent(getActivity(), PolyvPPTPlayerActivity.class)
						: new Intent(getActivity(), PolyvPlayerActivity.class);
				playUrl.putExtra("uid", userId);
				playUrl.putExtra("cid", channelId);
				startActivity(playUrl);
			}

			@Override
			public void fail(final String s, final int i) {
				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Toast.makeText(getActivity(), "获取直播信息失败\n" + s + "-" + i, Toast.LENGTH_SHORT).show();
					}
				});
			}
		});
	}

	/**
	 * 请求写入设置的权限
	 */
	@SuppressLint("InlinedApi")
	private void requestPermissionWriteSettings() {
		if (!PolyvPermission.canMakeSmores()) {
			gotoPlay();
		} else if (Settings.System.canWrite(getActivity())) {
			gotoPlay();
		} else {
			Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:" + getActivity().getPackageName()));
			startActivityForResult(intent, SETTING);
		}
	}

	@Override
	@SuppressLint("InlinedApi")
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == SETTING) {
			if (PolyvPermission.canMakeSmores()) {
				if (Settings.System.canWrite(getActivity())) {
					gotoPlay();
				} else {
					new AlertDialog.Builder(getActivity())
							.setTitle("showPermissionInternet")
							.setMessage(Settings.ACTION_MANAGE_WRITE_SETTINGS + " not granted")
							.setPositiveButton(android.R.string.ok, null)
							.show();
				}
			}
		}
	}

	/**
	 * This is the method that is hit after the user accepts/declines the
	 * permission you requested. For the purpose of this example I am showing a "success" header
	 * when the user accepts the permission and a snackbar when the user declines it.  In your application
	 * you will want to handle the accept/decline in a way that makes sense.
	 *
	 * @param requestCode
	 * @param permissions
	 * @param grantResults
	 */
	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		if (polyvPermission.operationHasPermission(requestCode)) {
			requestPermissionWriteSettings();
		} else {
			polyvPermission.makePostRequestSnack();
		}
	}

	@TargetApi(19)
	public void setTranslucentStatus() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			//当状态栏透明后，内容布局会上移，这里使用一个和状态栏高度相同的view来修正内容区域
			status_bar_fix.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Utils.getStatusHeight(getActivity())));
			status_bar_fix.setAlpha(1);
		}
	}

}
