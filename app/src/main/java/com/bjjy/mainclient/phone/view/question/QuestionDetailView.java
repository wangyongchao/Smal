package com.bjjy.mainclient.phone.view.question;

import com.dongao.mainclient.model.bean.question.QuestionDetail;
import com.dongao.mainclient.model.mvp.MvpView;

/**
 * Created by fengzongwei on 2016/12/1 0001.
 */
public interface QuestionDetailView extends MvpView {
    String getQuestionId();//答疑id
    void showQuestionDetail(QuestionDetail questionDetail);//显示答疑详情
    void deleteSuccess();//删除成功
    void deleteFail();//删除失败
}
