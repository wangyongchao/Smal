package com.bjjy.mainclient.phone.view.main;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.dict.MainTypeEnum;
import com.bjjy.mainclient.phone.view.book.BookListActivity;
import com.bjjy.mainclient.phone.view.daytest.SelectSubjectActivity;
import com.bjjy.mainclient.phone.view.exam.ExamActivity;
import com.bjjy.mainclient.phone.view.play.PlayActivity;
import com.bjjy.mainclient.phone.widget.recyclerview.adapter.BaseViewHolder;
import com.bjjy.mainclient.phone.widget.timeline.TimeLineItemType;
import com.bjjy.mainclient.phone.widget.timeline.TimeLinesView;

public class TimeLineViewHolder extends BaseViewHolder<MsgValue> {
    public TextView title;
    public TextView subTitle;
    public TextView subTitle2;
    public TextView date;
    public TimeLinesView mTimelineView;
    public LinearLayout home_fragment_item_course_layout;
    public ImageView action;

    public TimeLineViewHolder(ViewGroup parent, int viewType){
        super(parent, R.layout.home_frgment_item);
        title = (TextView) itemView.findViewById(R.id.title);
        subTitle = (TextView) itemView.findViewById(R.id.subTitle);
        subTitle2 = (TextView) itemView.findViewById(R.id.subTitle2);
        date = (TextView) itemView.findViewById(R.id.date);
        mTimelineView = (TimeLinesView) itemView.findViewById(R.id.timeLineView);
        home_fragment_item_course_layout = (LinearLayout) itemView.findViewById(R.id.home_fragment_item_course_layout);

        action = (ImageView) itemView.findViewById(R.id.action_iv);
        if (viewType == TimeLineItemType.ATOM) {
            mTimelineView.setBeginLine(null);
            mTimelineView.setEndLine(null);
        } else if (viewType == TimeLineItemType.START) {
            mTimelineView.setBeginLine(null);
        } else if (viewType == TimeLineItemType.END) {
            mTimelineView.setEndLine(null);
        }
    }

    @Override
    public void setData(final MsgValue homeModel){
        final Context context = getContext();
        switch (homeModel.getContentType()){
            case 1:
                MainTypeEnum.MAIN_TYPE_DAY.getName();
                action.setVisibility(View.INVISIBLE);
                if(homeModel.getAccomplished() == 0){
                    mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_lianxi));
                    title.setText("每日一练");
                }else{
                    mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_lianxi));
                    title.setText("完成每日一练");
                }
                subTitle.setVisibility(View.GONE);
                subTitle2.setVisibility(View.GONE);
                break;
            case 2:
                MainTypeEnum.MAIN_TYPE_SELECT_COURSE.getName();
                mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_tike_h));
                action.setVisibility(View.INVISIBLE);
                subTitle.setVisibility(View.VISIBLE);
                subTitle2.setVisibility(View.GONE);
                break;
            case 3:
                MainTypeEnum.MAIN_TYPE_BOOK.getName();
                mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_tike_h));
                action.setVisibility(View.INVISIBLE);
                subTitle.setVisibility(View.VISIBLE);
                subTitle2.setVisibility(View.GONE);
                break;
            case 4:
                MainTypeEnum.MAIN_TYPE_WAIT.getName();
                mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_tike_h));
                action.setVisibility(View.INVISIBLE);
                subTitle.setVisibility(View.VISIBLE);
                subTitle2.setVisibility(View.GONE);
                break;
            case 5:
                MainTypeEnum.MAIN_TYPE_COUTSE.getName();
                if(homeModel.getAccomplished() == 0){
                    mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_tike_n));
                    action.setVisibility(View.VISIBLE);
                }else{
                    mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_tike_h));
                    action.setVisibility(View.INVISIBLE);
                }
                subTitle.setVisibility(View.VISIBLE);
                subTitle2.setVisibility(View.VISIBLE);
                break;
            case 6:
                MainTypeEnum.MAIN_TYPE_KEHOUZUOYE.getName();
                if(homeModel.getAccomplished() == 0){
                    mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_exercises_n));
                    action.setVisibility(View.INVISIBLE);
                }else{
                    mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_exercises_h));
                    action.setVisibility(View.INVISIBLE);
                }
                subTitle.setVisibility(View.VISIBLE);
                subTitle2.setVisibility(View.VISIBLE);
                break;
            case 7:
                MainTypeEnum.MAIN_TYPE_MONISHIJUAN.getName();
                if(homeModel.getAccomplished() == 0){
                    mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_exercises_n));
                    action.setVisibility(View.INVISIBLE);
                }else{
                    mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_exercises_h));
                    action.setVisibility(View.INVISIBLE);
                }
                 subTitle.setVisibility(View.VISIBLE);
                 subTitle2.setVisibility(View.VISIBLE);
                break;
            case 8:
                MainTypeEnum.MAIN_TYPE_LINIANZHENTI.getName();
                if(homeModel.getAccomplished() == 0){
                    mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_exercises_n));
                    action.setVisibility(View.INVISIBLE);
                }else{
                    mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_exercises_h));
                    action.setVisibility(View.INVISIBLE);
                }
                subTitle.setVisibility(View.VISIBLE);
                subTitle2.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }

        home_fragment_item_course_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (homeModel.getContentType()) {
                    case 1:
                        MainTypeEnum.MAIN_TYPE_DAY.getName();
                        Intent intent = new Intent(context, SelectSubjectActivity.class);
                        context.startActivity(intent);
                        break;
                    case 2:
                        MainTypeEnum.MAIN_TYPE_SELECT_COURSE.getName();
                        Intent intent_select_course = new Intent(context, SelectSubjectActivity.class);
                        context.startActivity(intent_select_course);
                        break;
                    case 3:
                        MainTypeEnum.MAIN_TYPE_BOOK.getName();
                        Intent intent_book = new Intent(context, BookListActivity.class);
                        context.startActivity(intent_book);
                        break;
                    case 4:
                        break;
                    case 5:
                        MainTypeEnum.MAIN_TYPE_COUTSE.getName();
                        Intent intent_course = new Intent(context, PlayActivity.class);
                        context.startActivity(intent_course);
                        break;
                    case 6:
                        MainTypeEnum.MAIN_TYPE_KEHOUZUOYE.getName();
                        Intent intent_kehouzuoye = new Intent(context, ExamActivity.class);
                        context.startActivity(intent_kehouzuoye);
                        break;
                    case 7:
                        MainTypeEnum.MAIN_TYPE_MONISHIJUAN.getName();
                        Intent intent_monishijuan = new Intent(context, ExamActivity.class);
                        context.startActivity(intent_monishijuan);
                        break;
                    case 8:
                        MainTypeEnum.MAIN_TYPE_LINIANZHENTI.getName();
                        Intent intent_linianzhenti = new Intent(context, ExamActivity.class);
                        context.startActivity(intent_linianzhenti);
                        break;
                    default:
                        break;
                }
            }
        });

        action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PlayActivity.class);
                context.startActivity(intent);

            }
        });

        title.setText(homeModel.getTitle1());
        subTitle.setText(homeModel.getTitle2());
        subTitle2.setText(homeModel.getTitle3());
        date.setText(homeModel.getCreateTimeStr());

      /*  holder.itemView.post(new Runnable() {
            @Override
            public void run() {

                int cellWidth = holder.itemView.getWidth();// this will give you cell width dynamically
                int cellHeight = holder.itemView.getHeight();// this will give you cell height dynamically

                dynamicHeight.HeightChange(position, cellHeight); //call your iterface hear
            }
        });*/
    }

    public interface DynamicHeight {
        void HeightChange (int position, int height);
    }



    public TimeLineViewHolder(View itemView, int viewType) {
        super(itemView);
        title = (TextView) itemView.findViewById(R.id.title);
        subTitle = (TextView) itemView.findViewById(R.id.subTitle);
        subTitle2 = (TextView) itemView.findViewById(R.id.subTitle2);
        date = (TextView) itemView.findViewById(R.id.date);
        mTimelineView = (TimeLinesView) itemView.findViewById(R.id.timeLineView);
        home_fragment_item_course_layout = (LinearLayout) itemView.findViewById(R.id.home_fragment_item_course_layout);

        action = (ImageView) itemView.findViewById(R.id.action_iv);
        if (viewType == TimeLineItemType.ATOM) {
            mTimelineView.setBeginLine(null);
            mTimelineView.setEndLine(null);
        } else if (viewType == TimeLineItemType.START) {
            mTimelineView.setBeginLine(null);
        } else if (viewType == TimeLineItemType.END) {
            mTimelineView.setEndLine(null);
        }
    }

}
