/*
 *
 *  * Copyright (C) 2015 by  wyc@qq.com
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  * http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *
 */

package com.bjjy.mainclient.phone.view.download.util;


import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.util.LongSparseArray;

import com.alibaba.fastjson.JSON;
import com.dongao.mainclient.core.util.LogUtils;
import com.bjjy.mainclient.phone.view.download.DownloadTask;
import com.dongao.mainclient.model.bean.user.User;
import com.dongao.mainclient.model.local.UserDB;
import com.dongao.mainclient.model.common.Constants;
import com.bjjy.mainclient.phone.view.download.db.DownloadDB;

import java.io.File;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 课程下载的 service
 * 
 * @author wyc
 *
 * 点击  下载按钮，不论以前状态如何，一律改为等待中 只有等待中这个状态才能进度下载的队列
 * 点击  全部开始  或者wifi状态下重新开始下载时，把所有的状态改为等待中
 * 
 */
public class CourseDownloadService extends Service {
	// 线程池
	private ExecutorService fixedThreadPool;
	// 缓存队列
	private LongSparseArray<DownloadTask> cacheQueue;
	// 是否保存Service服务
	private boolean keepService = true;
	private UserDB userDB;
	private User user;
	private DownloadDB downloadInfoDB;
    CwDLExecutor cwDLExecutor;

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		LogUtils.d("onCreate() executed");
        cwDLExecutor = new CwDLExecutor(this);
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		LogUtils.d("onStartCommand() executed ");
		String todo = (intent != null && intent.getExtras() != null) ? intent.getExtras().getString(Constants.TODO) : null;
		LogUtils.d(" 执行命令 : " + todo);
		if (todo != null) {
			if (cacheQueue == null) {
				cacheQueue = new LongSparseArray<DownloadTask>();
			}
			if (userDB == null) {
				userDB = new UserDB(this);
			}
			if (downloadInfoDB == null) {
				downloadInfoDB = new DownloadDB(this);
			}
			// 初始化同时执行下载的任务数 HY 父线程数设置
			if (fixedThreadPool == null) {
				// 同时下载的线程数, 需要把同步锁移除才生效
				fixedThreadPool = Executors.newFixedThreadPool(1);
				// fixedThreadPool.isTerminated() // 判断是否池子执行完毕
			}
			if (Constants.DL_INIT_DOWNLOAD.equalsIgnoreCase(todo)) {
				LogUtils.d("初始化Service");
				user = userDB.find();
				if (user != null) {
					// 创建时需要加载下载任务, 用于上次没有下载完成的任务
					startDownloadTask(user);
				}
			} else if (Constants.DL_TODO_ADD_FILE_DOWNLOAD.equals(todo) || Constants.DL_TODO_ALL_FILE_DOWNLOAD.equals(todo)) {
				// 添加下载任务, 或者 全部开始时触发 ( 偷懒模式, 不针对添加的课件作添加 , 使用所有 )
				user = userDB.find();
				if (user != null) {
					startDownloadTask(user);
				}
			}
		}
		
		return super.onStartCommand(intent, flags, startId);
	}
	
	private void startDownloadTask(User user) {
		List<DownloadTask> downloadTasks = downloadInfoDB.findAll(user.getUserId());
		//获得所有未完成 下载任务
		if (downloadTasks != null && !downloadTasks.isEmpty()) {
			for (DownloadTask downloadTask : downloadTasks) {
				
				// 没有在队列的才加入
				if (cacheQueue.get(downloadTask.getId()) == null) {
					fixedThreadPool.submit(new DownloadBackgroupRunnable(downloadTask));
					cacheQueue.put(downloadTask.getId(), downloadTask);
				} else {
					// 在队列了, 存在队列没移除的情况 ?? HY 待验证
				}
			}
		}
		LogUtils.d("当前下载队列 : "+cacheQueue.size());
		if (cacheQueue.size() > 0) {
			for (int i = 0; i < cacheQueue.size(); i++) {
				LogUtils.d("队列下载任务 >> "+ JSON.toJSONString(cacheQueue.get(cacheQueue.keyAt(i))));
			}
		}
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		if (fixedThreadPool != null) {
			List<Runnable> runnables = fixedThreadPool.shutdownNow();
			LogUtils.d("销毁时停止了"+runnables.size()+"个下载任务");
			keepService = false;
		}
		LogUtils.d("onDestroy() executed");
	}
	
	class DownloadBackgroupRunnable implements Runnable {

		/** 当前队列的下载对象 */
		private DownloadTask downloadTask;
		private File file;
		/** 子线程是否需要关闭 */
		boolean childThread = false;

		public DownloadBackgroupRunnable(DownloadTask downloadTask) {
			this.downloadTask = downloadTask;
		}
		
		@Override
		public void run() {
			for (;;) {
				synchronized (CourseDownloadService.this) {
					try {
						if (downloadTask == null) {
							childThread = true;
							break;
						}

						if (!keepService) {
							LogUtils.d("service 停止后, 线程结束");
							childThread = true;
							cacheQueue.remove(downloadTask.getId());
                            cwDLExecutor.cancel();
							break;
						}
						// 找最新状态
                        downloadTask = downloadInfoDB.find(downloadTask.getId());
						if (downloadTask == null) {
							childThread = true;
							//可能是删除了的，要让他停止下载。
                            cwDLExecutor.cancel();
							break;
						}

						if (downloadTask.getStatus() == DownloadTask.PAUSE) { //点击了暂停按钮
							childThread = true;
							cacheQueue.remove(downloadTask.getId());
                            cwDLExecutor.cancel();
							break;
						} else if (downloadTask.getStatus() == DownloadTask.WAITING){
                            downloadTask.setStatus(DownloadTask.DOWNLOADING);// 正在下载
							downloadInfoDB.update(downloadTask);
                            cwDLExecutor.start(downloadTask);
						}
						
						//判断是否下载完成，
						if (downloadTask.getStatus()==DownloadTask.FINISHED) {
							LogUtils.d("进行下一个任务");
							break;
						}
                        sendPercentBroadcast();
                       // EventBus.getDefault().post(new StatusEvent(true));
						// ============ 下载任务 == END ==========================
						Thread.sleep(2000); // 2秒心跳一次
					} catch (Exception e) {
						LogUtils.exception(e);
					}
				}
			}
		}


        private void sendPercentBroadcast(){
            Intent intent = new Intent(Constants.ACTION_DOWNLOADING);
            intent.putExtra("cwId", downloadTask.getCwId());
            sendBroadcast(intent);
        }

	}



}
