package com.bjjy.mainclient.phone.view.studybar.down;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bjjy.mainclient.phone.event.PushMsgNotification;
import com.bjjy.mainclient.phone.view.studybar.fragment.StudyBarFragmentPercenter;
import com.bjjy.mainclient.phone.view.studybar.fragment.StudyBarFragmentView;
import com.bjjy.mainclient.phone.view.studybar.fragment.adapter.StudyBarFragmentAdapter;
import com.bjjy.mainclient.phone.view.studybar.Constant;
import com.bjjy.mainclient.phone.widget.statusbar.SystemBarTintManager;
import com.bjjy.mainclient.phone.widget.statusbar.Utils;
import com.dongao.mainclient.core.util.DensityUtil;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseFragmentActivity;
import com.bjjy.mainclient.phone.event.LogOutEvent;
import com.bjjy.mainclient.phone.event.LoginSuccessEvent;
import com.bjjy.mainclient.phone.view.exam.view.NoScrollListviewForPT;
import com.bjjy.mainclient.phone.view.persenal.widget.studybar.PullScrollView;
import com.bjjy.mainclient.phone.widget.EmptyViewLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by dell on 2016/5/16.
 */
public class StudyBarActivity extends BaseFragmentActivity implements StudyBarFragmentView, PullScrollView.OnTurnListener {

    @Bind(R.id.studybar_pzlv)
    NoScrollListviewForPT studybar_pzlv;
    @Bind(R.id.scroll_view)
    PullScrollView scroll_view;
    @Bind(R.id.background_img)
    ImageView background_img;
    @Bind(R.id.bar_top_title)
    TextView bar_top_title;
    @Bind(R.id.bar_top_right)
    ProgressBar bar_top_right;
    @Bind(R.id.ll_neirong)
    LinearLayout ll_neirong;
    @Bind(R.id.status_bar_fix)
    View status_bar_fix;
    @Bind(R.id.ll_top_title)
    LinearLayout ll_top_title;
    @Bind(R.id.top_title_left)
    ImageView top_title_left;
    


    private StudyBarFragmentPercenter studyBarFragmentPercenter;
    private StudyBarFragmentAdapter studyBarFragmentAdapter;
    private EmptyViewLayout mEmptyLayout;

    private boolean mHasLoadedOnce = false;
    private int gao;
    private View footView;
    private int stateHeight;
    
    @OnClick(R.id.top_title_left) void leftBack(){
        this.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.study_bar_fragment);
        EventBus.getDefault().register(this);
        ButterKnife.bind(this);
        studyBarFragmentPercenter = new StudyBarFragmentPercenter();
        studyBarFragmentPercenter.attachView(this);
        initView();
        initData();
    }

    @Override
    public void initView() {
        setTranslucentStatus();
        top_title_left.setVisibility(View.VISIBLE);
//        getHeight();
        scroll_view.setHeader(background_img);
        scroll_view.setNormalMovingListener(new PullScrollView.onNormalMovingListener() {
            @Override
            public void onShow(boolean show) {
            }
        });
        scroll_view.setOnTurnListener(this);
        studybar_pzlv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                studyBarFragmentPercenter.setOnItemClickListener(position);
            }
        });

        studybar_pzlv.setFocusable(false);

        mEmptyLayout = new EmptyViewLayout(StudyBarActivity.this, studybar_pzlv);
        mEmptyLayout.setErrorButtonClickListener(mErrorClickListener);
        mEmptyLayout.setEmptyButtonClickListener(emptyButtonClickListener);
        footView = LayoutInflater.from(StudyBarActivity.this).inflate(R.layout.app_studybar_view_empty, null);
        studybar_pzlv.addFooterView(footView, null, false);
    }

    @Override
    public void initData() {
        mEmptyLayout.showLoading();
        studyBarFragmentPercenter.getData();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        initData();
    }

    private void judgeHeightLv() {
        if (stateHeight==0){
            stateHeight= Utils.getStatusHeight(StudyBarActivity.this);
        }
        if (footView == null) {
            View footView = LayoutInflater.from(StudyBarActivity.this).inflate(R.layout.app_studybar_view_empty, null);
            studybar_pzlv.addFooterView(footView, null, false);
        }
        getHeight();
        int lvHeight = getLvHeight(studybar_pzlv);//studybar_pzlv.getHeight();
        if (gao < 10 || lvHeight <= 10) {
            footView.setLayoutParams(new AbsListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1));
            return;
        }
        if (gao >= lvHeight) {
            footView.setLayoutParams(new AbsListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (gao - lvHeight + 1)));
        } else {
            footView.setLayoutParams(new AbsListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1));
        }
    }

    private void getHeight() {
        DisplayMetrics wm = getResources().getDisplayMetrics();
        int screenHeight = wm.heightPixels;
        int top = DensityUtil.dip2px(StudyBarActivity.this, 50);
        int top2 = DensityUtil.dip2px(StudyBarActivity.this, 50);
        int top1 = 0;
        
        
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            //当状态栏透明后，内容布局会上移，这里使用一个和状态栏高度相同的view来修正内容区域
            top1 = Utils.getStatusHeight(StudyBarActivity.this);
        }
//        int topTitle = top + top1 + top2;
        int topTitle = top + top1 ;
        gao = screenHeight - topTitle;
    }

    @TargetApi(19)
    public void setTranslucentStatus() {
        SystemBarTintManager tintManager = new SystemBarTintManager(this);
        tintManager.setStatusBarTintEnabled(true);
        tintManager.setStatusBarTintResource(android.R.color.transparent);  //设置上方状态栏透明
        Window win = getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        final int bits = WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            winParams.flags |= bits;
        }else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
        
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            //当状态栏透明后，内容布局会上移，这里使用一个和状态栏高度相同的view来修正内容区域
            status_bar_fix.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Utils.getStatusHeight(StudyBarActivity.this)));
            status_bar_fix.setAlpha(0);
        }
    }
    /**
     * 错误监听
     */
    private View.OnClickListener mErrorClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mEmptyLayout.showLoading();
            studyBarFragmentPercenter.getData();
        }
    };
    /**
     * 无数据监听
     */
    private View.OnClickListener emptyButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mEmptyLayout.showLoading();
            studyBarFragmentPercenter.getData();
        }
    };



    @Override
    public void showLoading() {
        bar_top_right.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        bar_top_right.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public Context context() {
        return StudyBarActivity.this;
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void initAdapter() {
        studyBarFragmentAdapter = new StudyBarFragmentAdapter(StudyBarActivity.this, studyBarFragmentPercenter.studyBarList);
        studybar_pzlv.setAdapter(studyBarFragmentAdapter);
    }

    @Override
    public void refreshAdapter() {
        if (studyBarFragmentAdapter != null) {
            studyBarFragmentAdapter.notifyDataSetChanged();
        } else {
            studyBarFragmentAdapter = new StudyBarFragmentAdapter(StudyBarActivity.this, studyBarFragmentPercenter.studyBarList);
            studybar_pzlv.setAdapter(studyBarFragmentAdapter);
//            setLvHeight(studybar_pzlv);
        }
        judgeHeightLv();
    }

    @Override
    public void showCurrentView(int type) {
        bar_top_right.setVisibility(View.INVISIBLE);
        if (type == Constant.VIEW_TYPE_0) {
            mEmptyLayout.showContentView();
        } else if (type == Constant.VIEW_TYPE_1) {
            mEmptyLayout.showNetErrorView();
        } else if (type == Constant.VIEW_TYPE_2) {
            mEmptyLayout.showEmpty();
        } else {
            mEmptyLayout.showError();
        }
    }

    @Override
    public void onTurn() {
        if (bar_top_right.getVisibility()!=View.VISIBLE){
            bar_top_right.setVisibility(View.VISIBLE);
            studyBarFragmentPercenter.getData();
        }
    }

    @Subscribe
    public void onEventMainThread(PushMsgNotification pushMsgNotification) {
        studyBarFragmentPercenter.getData();
    }

    /**
     * listView调节高度
     *
     * @param list
     */
    public static int getLvHeight(ListView list) {
        ListAdapter adapter = list.getAdapter();
        if (adapter == null) {
            return 0;
        }
        int totalHeight = 0;
        if (adapter.getCount()==0){
            return 0;
        }
        for (int i = 0; i < adapter.getCount(); i++) {
            View itemView = adapter.getView(i, null, list);
            itemView.measure(0, 0);
            totalHeight += itemView.getMeasuredHeight();
        }
        return totalHeight
                + (list.getDividerHeight() * (adapter.getCount() - 1));// 总行高+每行的间距
    }

    @Subscribe
    public void onEventAsync(LoginSuccessEvent event) {
        bar_top_right.setVisibility(View.VISIBLE);
        initData();
    }

    @Subscribe
    public void onEventAsync(LogOutEvent event) {
        bar_top_right.setVisibility(View.VISIBLE);
        initData();
    }

    @Subscribe
    public void onEventAsync(PushMsgNotification event) {
        bar_top_right.setVisibility(View.VISIBLE);
        initData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }


    @Override
    public void onResume() {
        super.onResume();
        if (studyBarFragmentAdapter!=null){
            studyBarFragmentAdapter.notifyDataSetChanged();
        }
    }
}
