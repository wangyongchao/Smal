package com.bjjy.mainclient.phone.view.question;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dongao.libs.dongaoumengpushlib.constants.PushConstants;
import com.dongao.mainclient.core.camera.PhotoPreviewActivity;
import com.dongao.mainclient.core.camera.PhotoSelectorActivity;
import com.dongao.mainclient.core.camera.adapter.ChapterAdapter;
import com.dongao.mainclient.core.camera.adapter.GdAdapter;
import com.dongao.mainclient.core.camera.bean.ChapterModel;
import com.dongao.mainclient.core.camera.bean.ImageLoaderConfig;
import com.dongao.mainclient.core.camera.bean.PhotoModel;
import com.dongao.mainclient.core.camera.constants.Constants;
import com.dongao.mainclient.core.camera.utils.CameraUtils;
import com.dongao.mainclient.core.camera.utils.CommonUtils;
import com.bjjy.mainclient.persenter.AddQuestionPersenter;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseActivity;
import com.bjjy.mainclient.phone.utils.NetworkUtil;
import com.umeng.analytics.MobclickAgent;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by fengzongwei on 2016/5/6 0006.
 */
public class AddQuestionActivity extends BaseActivity implements AddQuestionView{
//    @Bind(R.id.question_add_body)
//    LinearLayout parent;
    @Bind(R.id.choose_chapter_ll)
    public LinearLayout linearLayout_chooseChapter;
    @Bind(R.id.book_number_ll)
    public LinearLayout linearLayout_page_num;
    @Bind(R.id.choose_chapter_tv)
    public TextView choose_chapter_tv;
    @Bind(R.id.suggest)
    public EditText mSuggest;
    @Bind(R.id.book_number_et)
    public EditText book_number_et;
    @Bind(R.id.add_subjectname_tv)
    TextView textView_bookName;
    @Bind(R.id.add_question_no_section_hint)
    TextView textView_no_section_hint;
    @Bind(R.id.rest_count)
    TextView rest_count;
    @Bind(R.id.loading_rl)
    RelativeLayout loading_rl;
    @Bind(R.id.choice_photo_project_ll)
    LinearLayout choice_photo_project_ll;
    @Bind(R.id.count)
    TextView mTextView;
    @Bind(R.id.add_booktype_number_hsv)
    HorizontalScrollView add_booktype_number_hsv;
    @Bind(R.id.add_booktype_number_ll)
    LinearLayout add_booktype_number_ll;
    @Bind(R.id.comit_btn)
    TextView comit_btn;
    @Bind(R.id.gd)
    GridView gd;
    private Dialog dialog_choose_chapter;
    private ListView chapter_list;
    private Dialog dialog_choose_img_way;
    protected MaterialDialog mMaterialDialog;
    private AlertDialog.Builder dialog_save;

    private ArrayList<TextView> bookViewList;
    private GdAdapter adapter;
    private ChapterAdapter chapterAdapter;

    public String BASE_IMAGE_CACHE = Constants.BASE_IMAGE_CACHE;// 缓存图片路径
    private static int MAX_COUNT = Constants.MAX_COUNT;//输入最多字数
    private final int MAX_PHOTOS = Constants.MAX_PHOTOS;// 最大的选着照片的数量
    private final int SELECT_IMAGE_CODE = 1001;

    private int copy_count = 0;

    private AddQuestionPersenter addQuestionPersenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.question_add_newques);
        ButterKnife.bind(this);
        ImageLoaderConfig.initImageLoader(this, BASE_IMAGE_CACHE);
        addQuestionPersenter = new AddQuestionPersenter();
        addQuestionPersenter.attachView(this);
        addQuestionPersenter.initDB();
        initView();
        initData();
    }

    @Override
    public void initView() {
//        textView_bookName.setText(getIntent().getStringExtra("bookName"));
        mSuggest.setSingleLine(false);
        mSuggest.addTextChangedListener(mTextWatcher);
        mSuggest.setSelection(mSuggest.length());
        linearLayout_chooseChapter.setOnClickListener(listener);
        choose_chapter_tv.setOnClickListener(listener);
        findViewById(R.id.camera).setOnClickListener(listener);
        findViewById(R.id.back).setOnClickListener(listener);
        comit_btn.setOnClickListener(listener);
        linearLayout_chooseChapter.setOnClickListener(listener);
        mTextView.setText(MAX_COUNT + "字");
        mTextView.setVisibility(View.VISIBLE);
        rest_count.setVisibility(View.VISIBLE);
    }

    private void setLeftCount() {
        if(getInputCount()>0){
            mTextView.setText(String.valueOf((MAX_COUNT - getInputCount())) + "字");
            mTextView.setVisibility(View.VISIBLE);
            rest_count.setVisibility(View.VISIBLE);
        }else{
            mTextView.setText(String.valueOf((MAX_COUNT - getInputCount())) + "字");
            mTextView.setVisibility(View.INVISIBLE);
            rest_count.setVisibility(View.INVISIBLE);
        }
//        if (MAX_COUNT - getInputCount() <= 20) {
//            mTextView.setText(String.valueOf((MAX_COUNT - getInputCount())) + "字");
//            mTextView.setVisibility(View.VISIBLE);
//            rest_count.setVisibility(View.VISIBLE);
//        } else {
//            mTextView.setText(String.valueOf((MAX_COUNT - getInputCount())) + "字");
//            mTextView.setVisibility(View.INVISIBLE);
//            rest_count.setVisibility(View.INVISIBLE);
//        }
    }

    private long getInputCount() {
//        return calculateLength(mSuggest.getText().toString());
        return mSuggest.getText().toString().length();
    }
    private long calculateLength(CharSequence c) {
        double len = 0;
        for (int i = 0; i < c.length(); i++) {
            int tmp = (int) c.charAt(i);
            if (tmp > 0 && tmp < 127) {
                len += 0.5;
            } else {
                len++;
            }
        }
        return Math.round(len);
    }
    /**
     * 添加子布局
     */

    public void addChildView() {
        bookViewList = new ArrayList<>();
        // 新增子布局对象到父布局中
        if (addQuestionPersenter.bookList == null) {
            addQuestionPersenter.bookList = new ArrayList<>();
        }
        for (int i = 0; i < addQuestionPersenter.bookList.size(); i++) {
            final TextView textView = new TextView(this);
            textView.setText(addQuestionPersenter.bookList.get(i).getAlias());
            textView.setTextSize(16);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(10, 20, 10, 20);
            textView.setPadding(10, 10, 10, 10);
            textView.setLayoutParams(params);
            //判断当前显示的字符是否
            if (addQuestionPersenter.bookList.get(i).getAlias().length() > 4) {
            } else {
                textView.setWidth((this.getWindowManager().getDefaultDisplay().getWidth() - addQuestionPersenter.bookList
                        .size() * 10) / 4 + 40);
            }
            textView.setGravity(Gravity.CENTER);
            textView.setId(i);
            if (addQuestionPersenter.question.getBookId() == null || addQuestionPersenter.question.getBookId().equals("")) {
                //区分是修改进入还是新提问
                if (addQuestionPersenter.myQuestion == null) {
                    addQuestionPersenter.bookIdNumber = 0;
                    if (i == 0) {
                        textView.setBackgroundResource(R.drawable.question_book_pressed);
                        textView.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        textView.setBackgroundResource(R.drawable.question_book_normal);
                        textView.setTextColor(getResources().getColor(R.color.text_color_primary_hint));
                    }

                } else {
                    if (addQuestionPersenter.bookIdNumber == 0) {
                        for (int j = 0; j < addQuestionPersenter.bookList.size(); j++) {
                            if (addQuestionPersenter.myQuestion.getSectionId().equals(addQuestionPersenter.bookList.get(j).getBookId())) {
                                addQuestionPersenter.bookIdNumber = j;
                                addQuestionPersenter.chapterList = (ArrayList<ChapterModel>)
                                        addQuestionPersenter.bookList.get(addQuestionPersenter.bookIdNumber).getSectionList();
                                break;
                            }
                        }
                    }
                    if (addQuestionPersenter.bookIdNumber == i) {
                        textView.setBackgroundResource(R.drawable.question_book_pressed);
                        textView.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        textView.setBackgroundResource(R.drawable.question_book_normal);
                        textView.setTextColor(getResources().getColor(R.color.text_color_primary_hint));
                    }
                }
            } else {
                if (addQuestionPersenter.bookIdNumber == 0) {
                    for (int j = 0; j < addQuestionPersenter.bookList.size(); j++) {
                        if (addQuestionPersenter.question.getBookId().equals(addQuestionPersenter.bookList.get(j).getBookId())) {
                            addQuestionPersenter.bookIdNumber = j;
                            addQuestionPersenter.chapterList = (ArrayList<ChapterModel>)
                                    addQuestionPersenter.bookList.get(addQuestionPersenter.bookIdNumber).getSectionList();
                            break;
                        }
                    }
                }
                //bookIdNumber=Integer.valueOf(question.getBookId());
                if (addQuestionPersenter.bookIdNumber == i) {
                    textView.setBackgroundResource(R.drawable.question_book_pressed);
                    textView.setTextColor(getResources().getColor(R.color.white));
                } else {
                    textView.setBackgroundResource(R.drawable.question_book_normal);
                    textView.setTextColor(getResources().getColor(R.color.text_color_primary_hint));
                }
            }
            //判断当前的位置
            //if ()
            //add_booktype_number_hsv.scrollTo(,0);
            /**判断当前进入提问页的方式*/
            if (addQuestionPersenter.myQuestion != null) {
                textView.setEnabled(false);
            }
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addQuestionPersenter.bookIdNumber = textView.getId();
                    changeColor(textView.getId());
                    addQuestionPersenter.chapterList = (ArrayList<ChapterModel>)
                            addQuestionPersenter.bookList.get(addQuestionPersenter.bookIdNumber).getSectionList();
                    //判断当前数组中是否已有被选中的
                    choose_chapter_tv.setText("");
                    addQuestionPersenter.chapterModel = new ChapterModel();
                    for (int i = 0; i < addQuestionPersenter.bookList.get(addQuestionPersenter.bookIdNumber).getSectionList().size(); i++) {
                        if (addQuestionPersenter.bookList.get(addQuestionPersenter.bookIdNumber).getSectionList().get(i).isChecked()) {
                            choose_chapter_tv.setText(addQuestionPersenter.bookList.get(addQuestionPersenter.bookIdNumber).getSectionList().get(i).getSectionName());
                            addQuestionPersenter.chapterModel = addQuestionPersenter.bookList.get(addQuestionPersenter.bookIdNumber).getSectionList().get(i);
                            break;
                        }
                    }
                }
            });
            // textView.setTextSize(20);
            bookViewList.add(textView);
            choice_photo_project_ll.addView(textView);// 将childView添加到父布局
        }
    }

    /**
     * 把其他的都设置成白色被选中的设置为深颜色
     */
    public void changeColor(int position) {
        for (int i = 0; i < addQuestionPersenter.bookList.size(); i++) {
            if (position == i) {
                addQuestionPersenter.bookList.get(i).setIsChecked(true);
                bookViewList.get(i).setTextColor(getResources().getColor(R.color.white));
                bookViewList.get(i).setBackgroundResource(R.drawable.question_book_pressed);
            } else {
                addQuestionPersenter.bookList.get(i).setIsChecked(false);
                bookViewList.get(i).setTextColor(getResources().getColor(R.color.text_color_primary_hint));
                bookViewList.get(i).setBackgroundResource(R.drawable.question_book_normal);
            }
        }
    }

    /**
     * 普通的View.onclick点击事件
     */
    public View.OnClickListener listener = new View.OnClickListener() {

        @Override
        public void onClick(View arg0) {
            switch (arg0.getId()) {
                case R.id.choose_chapter_ll:
                case R.id.choose_chapter_tv:
                    showChapter();
//                    showChapterPop();
                    break;
                case R.id.back:
                    onBackPressed();
                    break;
                case R.id.camera:
                    showChooseWayDialog();
                    MobclickAgent.onEvent(AddQuestionActivity.this, PushConstants.QUESTION_ADDPICTURE);
                    break;
                case R.id.bt_choose_by_local:
                    enterChoosePhoto();
                    break;
                case R.id.bt_choose_by_camera:
                    dialog_choose_img_way.cancel();
                    if (addQuestionPersenter.selected.size() >= MAX_PHOTOS) {
                        Toast.makeText(AddQuestionActivity.this,
                                "最多上传" + MAX_PHOTOS + "张", Toast.LENGTH_SHORT)
                                .show();
                        return;
                    } else {
                        CameraUtils.selectPicFromCamera(AddQuestionActivity.this);
                    }
                    break;
                case R.id.bt_dialog_cancel:
                    dialog_choose_img_way.cancel();
                    break;
                case R.id.rest_view_ll:
//                    dialog_choose_chapter.cancel();
                    dialog_choose_chapter.dismiss();
                   /* if (dialog_choose_chapter!=null&&dialog_choose_chapter.isShowing()){

                        dialog_choose_chapter.cancel();
                    }*/
                    break;
//                case R.id.rest_view_v:
////                    dialog_choose_chapter.cancel();
//                    if (dialog_choose_chapter != null) {
//                        dialog_choose_chapter.dismiss();
//                    }
//                    break;
                case R.id.other_view:
                    dialog_choose_img_way.cancel();
                    break;
                case R.id.comit_btn:
                    if (judgeInfo(book_number_et.getText().toString().trim(), mSuggest.getText().toString().trim())) {
                        if (!NetworkUtil.isNetworkAvailable(getApplication())) {
                            Toast.makeText(AddQuestionActivity.this, getResources().getText(R.string.select_subject_checknet), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        try {
                            addQuestionPersenter.getData();
                        } catch (Exception e) {
                            e.printStackTrace();
                            showError("上传错误");
                        }
                    }
                    break;
                default:
                    break;
            }

        }
    };

    /**
     * editext的监听事件
     */
    private TextWatcher mTextWatcher = new TextWatcher() {

        private int editStart;

        private int editEnd;

        public void afterTextChanged(Editable s) {
//            editStart = mSuggest.getSelectionStart();
//            editEnd = mSuggest.getSelectionEnd();
//
//			/* 先去掉监听器，否则会出现栈溢出 */
//            mSuggest.removeTextChangedListener(mTextWatcher);
//
//			/*
//             * 注意这里只能每次都对整个EditText的内容求长度，不能对删除的单个字符求长度
//			 * 因为是中英文混合，单个字符而言，calculateLength函数都会返回1 当输入字符个数超过限制的大小时，进行截断操作
//			 */
//            while (calculateLength(s.toString()) > MAX_COUNT) {
//                s.delete(editStart - 1, editEnd);
//                editStart--;
//                editEnd--;
//            }
//
//			/* 将这行代码注释掉就不会出现后面所说的输入法在数字界面自动跳转回主界面的问题了 */
//            //mEditText.setText(s);
//            mSuggest.setSelection(editStart);
//
//			/* 恢复监听器 */
//            mSuggest.addTextChangedListener(mTextWatcher);
//
//            setLeftCount();
        }

        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            if (copy_count >= 0) {
                copy_count = MAX_COUNT - mSuggest.getText().length();
                if (copy_count < 0) {
                    copy_count = 0;
                    mSuggest.setText(s.subSequence(0, MAX_COUNT));
                    mSuggest.setSelection(MAX_COUNT);
                }
                mTextView.setText(copy_count + "字");
            }
        }

    };

    @Override
    public void initData() {
        addQuestionPersenter.initData();
    }

    /**
     * 显示章节具体信息
     */
    private void showChapter() {
        dialog_choose_chapter = new Dialog(this, R.style.MyDialogStyle);
//        dialog_choose_chapter = new MaterialDialog(this);
        View contentView = LayoutInflater.from(this).inflate(R.layout.question_add_question_show_chapter,null);
        dialog_choose_chapter.setContentView(contentView);
        dialog_choose_chapter.setCanceledOnTouchOutside(true);
        dialog_choose_chapter.setCancelable(true);
        chapter_list = (ListView) contentView.findViewById(R.id.chapter_list);
        contentView.findViewById(R.id.rest_view_ll).setOnClickListener(listener);
//        contentView.findViewById(R.id.rest_view_v).setOnClickListener(listener);
        //创建adapter
        chapterAdapter = new ChapterAdapter(this, addQuestionPersenter.chapterList);
        chapterAdapter.setList(addQuestionPersenter.chapterList);
        chapter_list.setAdapter(chapterAdapter);
        chapter_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long l) {
                ChapterModel current = (ChapterModel) parent.getItemAtPosition(position);
                for (int i = 0; i < parent.getCount(); i++) {
                    ChapterModel album = (ChapterModel) parent.getItemAtPosition(i);
                    if (i == position) {
                        album.setIsChecked(true);
                        addQuestionPersenter.chapterModel = album;
                    } else {
                        album.setIsChecked(false);
                    }
                }
                addQuestionPersenter.bookIdNumber = position;
                chapterAdapter.notifyDataSetChanged();
                choose_chapter_tv.setText(current.getSectionName());
                dialog_choose_chapter.dismiss();
            }
        });
        setDialog(dialog_choose_chapter, chapter_list);
        dialog_choose_chapter.show();
    }

    /**
     * 设置Dialog的大小和Dialog中ListView的自适应
     * @param dialog 对话框
     * @param list 对话框中的ListView
     */
    public void setDialog(Dialog dialog,ListView list) {
        if (dialog != null) {
            //得到当前dialog对应的窗体
            Window dialogWindow = dialog.getWindow();
            //管理器
            WindowManager m = getWindowManager();
            //屏幕分辨率，获取屏幕宽、高用
            Display d = m.getDefaultDisplay();
            //获取对话框当前的参数值
            WindowManager.LayoutParams p = dialogWindow.getAttributes();
            //宽度设置为屏幕的0.8
            p.width = (int) (d.getWidth() * 1);
            //获取ListView的高度和当前屏幕的0.6进行比较，如果高，就自适应改变
            if(getTotalHeightofListView(list) > d.getHeight()*0.7){
                //得到ListView的参数值
                ViewGroup.LayoutParams params = list.getLayoutParams();
                //设置ListView的高度是屏幕的一半
                params.height = (int) (d.getHeight()*0.7);
                //设置
                list.setLayoutParams(params);
            }else{
                ViewGroup.LayoutParams params = list.getLayoutParams();
                //设置ListView的高度是屏幕的一半
                params.height = getTotalHeightofListView(list);
                //设置
                list.setLayoutParams(params);
            }
            p.height = (int)(d.getHeight() * 0.95);
            //设置Dialog的高度
            dialogWindow.setAttributes(p);
        }
    }

    /**
     * 获取ListView的高度
     * @param list  listview内容列表
     * @return ListView的高度
     */
    public int getTotalHeightofListView(ListView list) {
        //ListView的适配器
        ListAdapter mAdapter = list.getAdapter();
        if (mAdapter == null) {
            return 0;
        }
        int totalHeight = 0;
        //循环适配器中的每一项
        for (int i = 0; i < mAdapter.getCount(); i++) {
            //得到每项的界面view
            View mView = mAdapter.getView(i, null, list);
            //得到一个view的大小
            mView.measure(
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));

            //总共ListView的高度
            totalHeight += mView.getMeasuredHeight();
        }
        return totalHeight;
    }

    /**
     * 选择图片上传的方式
     */
    private void showChooseWayDialog() {
        dialog_choose_img_way = new Dialog(this, R.style.MyDialogStyle);
        dialog_choose_img_way.setContentView(R.layout.question_dialog_choose_img);
        dialog_choose_img_way.setCanceledOnTouchOutside(true);
        dialog_choose_img_way.findViewById(R.id.other_view).setOnClickListener(
                listener);
        dialog_choose_img_way.findViewById(R.id.bt_dialog_cancel)
                .setOnClickListener(listener);
        // 拍照上传
        dialog_choose_img_way.findViewById(R.id.bt_choose_by_camera)
                .setOnClickListener(listener);
        // 本地上传
        dialog_choose_img_way.findViewById(R.id.bt_choose_by_local)
                .setOnClickListener(listener);
        dialog_choose_img_way.show();
    }

    /**
     * 进入图库选择图片
     */
    private void enterChoosePhoto() {
        // TODO Auto-generated method stub

        ArrayList<PhotoModel> choosed = new ArrayList<PhotoModel>();
        if (addQuestionPersenter.selected.size() > 0) {
            choosed.addAll(addQuestionPersenter.selected);
        }
        Intent intent = new Intent(AddQuestionActivity.this,
                PhotoSelectorActivity.class);
        intent.putExtra(PhotoSelectorActivity.KEY_MAX, MAX_PHOTOS);
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("selected",
                (ArrayList<? extends Parcelable>) choosed);
        intent.putExtras(bundle);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivityForResult(intent, SELECT_IMAGE_CODE);
    }

    /**
     * 判断当前的数据是否选择完成
     */
    private boolean judgeInfo(String book_number_et, String mSuggest) {
        if ("".equals(mSuggest) || mSuggest.length()<10) {
            CameraUtils.showToast(this, "输入至少10个字的问题描述才能进行提交哦");
            return false;
        } else if ("".equals(book_number_et) || book_number_et.length()>4) {
            CameraUtils.showToast(this, "请您先输入正确的页码数后，再进行问题的提交");
            return false;
        } else if (addQuestionPersenter.chapterModel.getSectionId() == null || addQuestionPersenter.chapterModel.getSectionId().equals("")) {
            CameraUtils.showToast(this, "请您先选择相应的章节后，再进行问题的提交");
            return false;
        } else {
            return true;
        }
    }

    /**
     * 显示进度条
     */
    public void showProgressDialog(String loadingtv){
        mMaterialDialog = new MaterialDialog(this);
        mMaterialDialog.setCanceledOnTouchOutside(false);
        View view = LayoutInflater.from(this).inflate(R.layout.app_view_loading,null);
        TextView tv = (TextView) view.findViewById(R.id.app_loading_tv);
        ImageView imageView = (ImageView)view.findViewById(R.id.empty_layout_loading_img);
        AnimationDrawable animationDrawable = (AnimationDrawable)imageView.getBackground();
        animationDrawable.start();
        tv.setText(loadingtv);
        mMaterialDialog.setContentView(view);
        mMaterialDialog.show();
    }

    /**
     * 弹出上传图片失败提示
     */
    private void showErrorDialog() {
        // loading_rl.setVisibility(View.GONE);
        closeProgressDialog();
        dialog_save = new AlertDialog.Builder(this);
        dialog_save.setMessage("是否重新上传").setTitle("上传图片失败").setNegativeButton("否", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //AddQuestionActivity.this.finish();
            }
        }).setPositiveButton("是", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                try {
                    addQuestionPersenter.commitFailImage();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });
        dialog_save.show();
    }

    /**
     * 隐藏进度条
     */
    public void closeProgressDialog(){
        mMaterialDialog.dismiss();
    }

    @Override
    public void onClick(View v) {
    }

    @Override
    public Intent catchIntent() {
        return getIntent();
    }

    @Override
    public void showLoading() {
        showProgressDialog(getResources().getString(R.string.add_question_ing));
    }

    @Override
    public void hideLoading() {
        closeProgressDialog();
    }

    @Override
    public void showRetry() {
    }

    @Override
    public String getEditString() {
        return mSuggest.getText().toString().trim();
    }

    @Override
    public String getPageNum() {
        return book_number_et.getText().toString().trim();
    }

    @Override
    public void hideRetry() {

    }
    @Override
    public void showError(String message) {
        hideLoading();
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }
    @Override
    public Context context() {
        return this;
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (dialog_choose_img_way != null && dialog_choose_img_way.isShowing()) {
            dialog_choose_img_way.dismiss();
        }
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case SELECT_IMAGE_CODE:
                    addQuestionPersenter.selectImgCode(data);
                    break;
                case CameraUtils.CAMERA_PHOTO:// 拍照上传
                    addQuestionPersenter.cameraCode();
                    break;
                default:
                    break;
            }
        } else if (resultCode == Constants.RESULT_MAIN) {
            addQuestionPersenter.clearFailDate();
            if (requestCode == Constants.REQUEST_MAIN) {
                addQuestionPersenter.resultMain(data);
            }
        }
    }

    @Override
    public void notifyAdapter() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void resetAdapter(List<PhotoModel> selected) {
        adapter = new GdAdapter(AddQuestionActivity.this,selected);
        gd.setAdapter(adapter);
    }

    @Override
    public void setCameraViewVisibility(int visibility) {
        findViewById(R.id.camera).setVisibility(visibility);
    }

    @Override
    public void setGVOnItemClickListener() {
        gd.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                List<PhotoModel> lists = new ArrayList<PhotoModel>();
                lists.addAll(addQuestionPersenter.selected);
                // lists.remove(lists.size() - 1);
                Bundle bundle = new Bundle();
                bundle.putInt("position", position);
                bundle.putSerializable("photos", (Serializable) lists);
                bundle.putInt("type", Constants.TYPE_RECHOICE);
                bundle.putInt("maximage", MAX_PHOTOS);
                // CommonUtils.launchActivity(this, PhotoPreviewActivity.class, bundle);
                CommonUtils.launchActivityForResultBundle(AddQuestionActivity.this,
                        PhotoPreviewActivity.class, Constants.REQUEST_MAIN, bundle);
            }
        });
    }

    @Override
    public void onBackPressed() {
        addQuestionPersenter.onBackPress();
    }

    @Override
    public void showPostImgError() {
        showErrorDialog();
    }

    @Override
    public void setBookName(String bookName) {
        textView_bookName.setText(bookName);
    }

    @Override
    public String getChooseSectioName() {
        return book_number_et.getText().toString();
    }

    @Override
    public void showSaveDialog() {
        materialDialog = new MaterialDialog(this);
        materialDialog.setTitle("提示");
        materialDialog.setMessage("是否保留当前的数据");
        materialDialog.setNegativeButton("取消", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addQuestionPersenter.deletaSava();
                finish();
            }
        });
        materialDialog.setPositiveButton("确定", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addQuestionPersenter.saveData();
                Toast.makeText(AddQuestionActivity.this,"保存成功",Toast.LENGTH_SHORT).show();
                finish();
            }
        });
        materialDialog.show();
    }

    @Override
    public void goneListView() {
        linearLayout_chooseChapter.setVisibility(View.GONE);
        linearLayout_page_num.setVisibility(View.GONE);
        textView_no_section_hint.setVisibility(View.VISIBLE);
    }
}
