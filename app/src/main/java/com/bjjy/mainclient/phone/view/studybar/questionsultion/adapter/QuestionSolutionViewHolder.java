package com.bjjy.mainclient.phone.view.studybar.questionsultion.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bjjy.mainclient.phone.view.studybar.privateteacher.adapter.PrivateTeacherAdapter;
import com.bjjy.mainclient.phone.R;

public class QuestionSolutionViewHolder extends RecyclerView.ViewHolder {

    public final TextView name_tv;
    public final TextView time_tv;
    public final ImageView iv_is_read;
    private PrivateTeacherAdapter.ReclerViewItemClick reclerViewItemClick;

    public QuestionSolutionViewHolder(View itemView) {
        super(itemView);
        name_tv = (TextView) itemView.findViewById(R.id.name_tv);
        time_tv = (TextView) itemView.findViewById(R.id.time_tv);
        iv_is_read= (ImageView) itemView.findViewById(R.id.iv_is_read);
    }

}
