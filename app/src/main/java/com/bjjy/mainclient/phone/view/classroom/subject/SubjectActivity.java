package com.bjjy.mainclient.phone.view.classroom.subject;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseFragmentActivity;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SubjectActivity extends BaseFragmentActivity {
	@Bind(R.id.top_title_left)
	ImageView top_title_left;

	@Bind(R.id.top_title_text)
	TextView top_title_text;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.course_subject_activity);
		ButterKnife.bind(this);
        initView();

	}

    @Override
	public void initView() {
		// TODO Auto-generated method stub

		top_title_left.setImageResource(R.drawable.back);
		top_title_left.setVisibility(View.VISIBLE);
		top_title_left.setOnClickListener(this);
		top_title_text.setText("科目选择");

        SubjectFragment subjectFragment = new SubjectFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.course_subject_frame, subjectFragment).commit();

	}


	@Override
	public void initData() {

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
            case R.id.top_title_left:
               	onBackPressed();
                break;
		default:
			break;
		}
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		//EventBus.getDefault().post(new UpdateEvent(true));
		this.finish();
	}
}
