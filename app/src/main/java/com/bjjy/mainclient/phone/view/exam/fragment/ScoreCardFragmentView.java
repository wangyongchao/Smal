package com.bjjy.mainclient.phone.view.exam.fragment;

import android.os.Bundle;
import android.view.View;
import com.dongao.mainclient.model.mvp.MvpView;


/**
 * Created by wyc on 2016/5/6.
 */
public interface ScoreCardFragmentView  extends MvpView {
   void setView(View view);
    void finishActivity();
    Bundle getArgumentData();
}
