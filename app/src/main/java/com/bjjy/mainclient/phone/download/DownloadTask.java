package com.bjjy.mainclient.phone.download;

import com.dongao.mainclient.model.bean.play.CourseWare;

public class DownloadTask {
	
	private String video_url;
	//添加课程介绍的 url // TODO: 2017/11/17  
	private String cwDesc;
	private String lectureUrl;
	private String captionUrl;
	private String desPath;
	private int state;
	private int percent;
	private String courseWareId;
	private String courseWareBean;
	private String userId;
	private String year;

	private String courseId;
	private String examId;
	private String subjectId;
	private String sectionId;
	private CourseWare cw;

	public CourseWare getCw() {
		return cw;
	}

	public void setCw(CourseWare cw) {
		this.cw = cw;
	}

	public String getExamId() {
		return examId;
	}

	public void setExamId(String examId) {
		this.examId = examId;
	}

	public String getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}

	public String getSectionId() {
		return sectionId;
	}

	public void setSectionId(String sectionId) {
		this.sectionId = sectionId;
	}

	public String getCourseWareBean() {
		return courseWareBean;
	}

	public void setCourseWareBean(String courseWareBean) {
		this.courseWareBean = courseWareBean;
	}

	public String getVideo_url() {
		return video_url;
	}

	public void setVideo_url(String video_url) {
		this.video_url = video_url;
	}

	public String getDesPath() {
		return desPath;
	}

	public void setDesPath(String desPath) {
		this.desPath = desPath;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public int getPercent() {
		return percent;
	}

	public void setPercent(int percent) {
		this.percent = percent;
	}

	public String getLectureUrl() {
		return lectureUrl;
	}

	public void setLectureUrl(String lectureUrl) {
		this.lectureUrl = lectureUrl;
	}

	public String getCaptionUrl() {
		return captionUrl;
	}

	public void setCaptionUrl(String captionUrl) {
		this.captionUrl = captionUrl;
	}

	public String getCourseWareId() {
		return courseWareId;
	}

	public void setCourseWareId(String courseWareId) {
		this.courseWareId = courseWareId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getCwDesc() {
		return cwDesc;
	}

	public void setCwDesc(String cwDesc) {
		this.cwDesc = cwDesc;
	}
}
