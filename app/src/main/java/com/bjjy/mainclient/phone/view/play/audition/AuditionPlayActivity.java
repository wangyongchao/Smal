package com.bjjy.mainclient.phone.view.play.audition;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.dongao.mainclient.model.bean.play.CourseWare;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.vov.vitamio.MediaPlayer;
import io.vov.vitamio.Vitamio;
import io.vov.vitamio.widget.auditionplay.MediaController;
import io.vov.vitamio.widget.auditionplay.VideoView;

/**
 * Created by dell on 2016/6/16.
 */
public class AuditionPlayActivity extends BaseActivity implements MediaPlayer.OnInfoListener,AuditionPlayView {

    @Bind(R.id.surface_view)
    VideoView videoView;
    @Bind(R.id.probar)
    ProgressBar pb;

    private MediaController mediaController;
    private String paths;
    private CourseWare cw;
    private Uri uri;
    private boolean seekto;
    private static final int SEEK_TO = 166;

    private AuditionPlayPersenter persenter;

    private Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            if (isStart) {
                videoView.seekTo(reStartTime);
            } else {
                videoView.seekTo(startTime);
            }
            seekto = true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.play_audition);
        ButterKnife.bind(this);
        Vitamio.isInitialized(this);
        persenter=new AuditionPlayPersenter();
        persenter.attachView(this);

        initView();
//        initData();
    }

    @Override
    public void initView() {
        cw=(CourseWare)getIntent().getSerializableExtra("cw");
        if(!TextUtils.isEmpty(cw.getBeginSec())){
            startTime=Long.parseLong(cw.getBeginSec())*1000;
        }
        persenter.getData();
    }

    //paths = "http://appdownload.bjsteach.com/2014zs/ck/zzf/jc/ydb/14zsjc_zzfjc_ck_061_qy_yd_2/upload/media/m3u8_10/video.m3u8";
    @Override
    public void initData() {


    }

    @Override
    public void startPlay(CourseWare courseware) {
        if(isNull){
            return;
        }
//        paths=courseware.getMobileVideoUrl();
        paths=getPlayUrl(courseware);
        if (paths == "") {
            Toast.makeText(this, "视频数据错误", Toast.LENGTH_LONG).show();
            return;
        } else {
            if (mediaController == null) {
                mediaController = new MediaController(this, videoView);
            }
            videoView.setMediaController(mediaController);
            videoView.setOnInfoListener(this);
            mediaController.setCourseWare(courseware);
            uri = Uri.parse(paths);
            videoView.setVideoURI(uri);
            videoView.requestFocus();
        }
    }

    private String getPlayUrl(CourseWare coursew) {
        String localPath = "http://localhost:" + Constants.SERVER_PORT + "/" + SharedPrefHelper.getInstance(this).getUserId()+"_sanning_" + coursew.getCwId() + "/online.m3u8";
        return localPath;
    }

    @Override
    public void onClick(View v) {

    }

    private boolean isStart;
    private long reStartTime;
    private long startTime;
    @Override
    protected void onPause() {
        super.onPause();
        isStart = true;
        seekto = false;
        reStartTime = videoView.getCurrentPosition();
        videoView.pause();
    }

    @Override
    public boolean onInfo(MediaPlayer mp, int what, int extra) {
        switch (what) {
            case MediaPlayer.MEDIA_INFO_BUFFERING_START:
                if (videoView.isPlaying()) {
                    videoView.pause();
                    mediaController.setIsClickable(false);
                    pb.setVisibility(View.VISIBLE);
                }
                break;
            case MediaPlayer.MEDIA_INFO_BUFFERING_END:
                mediaController.setIsClickable(true);
                if (!seekto) {
                    handler.sendEmptyMessage(SEEK_TO);
                }
                videoView.start();
                pb.setVisibility(View.GONE);
                break;
            case MediaPlayer.MEDIA_INFO_DOWNLOAD_RATE_CHANGED:
                break;
        }
        return true;
    }

    private boolean isNull;
    @Override
    public void onDestroy() {
        super.onDestroy();
        videoView.stopPlayback();
        isNull=true;
    }

    @Override
    public CourseWare getCw() {
        return cw;
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public Context context() {
        return this;
    }

    @Override
    public void playError(String msg) {
        Toast.makeText(AuditionPlayActivity.this,msg, Toast.LENGTH_SHORT).show();
    }
}
