package com.bjjy.mainclient.phone.view.exam.activity.course;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bjjy.mainclient.phone.view.play.utils.SharedPrefHelper;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.remote.ApiClient;
import com.bjjy.mainclient.phone.view.exam.activity.course.bean.CourseMore;
import com.bjjy.mainclient.phone.view.exam.activity.course.bean.CourseMoreAllClassify;
import com.bjjy.mainclient.phone.view.exam.activity.course.bean.CourseMoreClassify;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by wyc on 2016/5/10.
 */
public class CourseMorePercenter extends BasePersenter<CourseMoreView> {
    private CourseMoreAllClassify myAllCourse;
    public ArrayList<CourseMoreClassify> courseList = new ArrayList<>();
    private String userId;

    @Override
    public void getData() {
        userId = SharedPrefHelper.getInstance(getMvpView().context()).getUserId();
        courseList = new ArrayList<>();
        getMvpView().showLoading();
        HashMap<String, String> params = new HashMap<>();
        apiModel.getData(ApiClient.getClient().getExamPracticeList(params));
        testData();
    }

    private void testData() {
        CourseMoreAllClassify courseMoreAllClassify=new CourseMoreAllClassify();
        courseMoreAllClassify.setSubjectId("111");
        courseMoreAllClassify.setYear("1");
        List<CourseMoreClassify> classifyList=new ArrayList<>();
      
        for (int i = 0; i < 5; i++) {
            CourseMoreClassify courseMoreClassify=new CourseMoreClassify();
            courseMoreClassify.setCourseTypeName("你笨啊："+i);
            for (int j = 0; j < 5; j++) {
                CourseMore courseMore=new CourseMore();
                courseMore.setProgressSuggested(""+i);
                courseMore.setDaPersonName("5527");
                courseMore.setName("sss"+j);
                courseMore.setUpdateTime("wang");
            }
            classifyList.add(courseMoreClassify);
        }
        courseMoreAllClassify.setCourseList(classifyList);
    }

    @Override
    public void setData(String str) {
        try {
            getMvpView().hideLoading();
            JSONObject object = JSON.parseObject(str);
            String body = object.getString("body");
            getMvpView().getPTREListView().onRefreshComplete();
           /* BaseBean baseBean = JSON.parseObject(str, BaseBean.class);
            if (baseBean == null) {
                mEmptyLayout.showError();
                return;
            }*/

           /* int code = object.getString("result");
            if (code != 1) {
                if (code == 9) {
//                        showLoginOtherPlace();
                } else {
                    mEmptyLayout.showError();
                }
                return;
            }*/
            getMvpView().getEmptyLayout().showContentView();
            myAllCourse = JSON.parseObject(body, CourseMoreAllClassify.class);
            courseList.clear();
            courseList.addAll(myAllCourse.getCourseList());
            courseList.addAll(myAllCourse.getCourseList());
            CourseMoreAllClassify newMyAllCourse = new CourseMoreAllClassify();
            newMyAllCourse = myAllCourse;
            newMyAllCourse.setUserId(userId);
            newMyAllCourse.setContent(body);
            getMvpView().initAdapter();
            getMvpView().refreshAdapter();
            getMvpView().setAllCollapsed();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onError(Exception e) {
        getMvpView().showError("获取数据失败");
    }
}
