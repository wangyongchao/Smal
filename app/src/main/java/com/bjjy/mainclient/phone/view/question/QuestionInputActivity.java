package com.bjjy.mainclient.phone.view.question;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.Selection;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bjjy.mainclient.persenter.QuestionInputPersenter;
import com.bjjy.mainclient.phone.app.BaseActivity;
import com.bjjy.mainclient.phone.event.ReloadQuestion;
import com.bjjy.mainclient.phone.view.setting.WebViewActivity;
import com.dongao.mainclient.model.common.Constants;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.widget.MyToast;

import org.greenrobot.eventbus.EventBus;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by fengzongwei on 2016/11/30 0030.
 */
public class QuestionInputActivity extends BaseActivity implements QustionInputView{
    @Bind(R.id.login_dialog_body_ll)
    LinearLayout linearLayout_body;
    @Bind(R.id.top_title_left)
    ImageView imageView_back;
    @Bind(R.id.top_title_text)
    TextView textView_title;
    @Bind(R.id.tv_right)
    TextView textView_post;
    @Bind(R.id.question_input_title_et)
    EditText editText_title;
    @Bind(R.id.question_input_title_count_tv)
    TextView textView_title_count;
    @Bind(R.id.question_input_content_et)
    EditText editText_content;
    @Bind(R.id.question_input_conten_count_tv)
    TextView textView_content_count;
    @Bind(R.id.question_input_title_body_ll)
    LinearLayout linearLayout_title_body;
    @Bind(R.id.add_question_warning)
    TextView textView_warning;

    private final int TITLE_MAX_COUNT = 30,CONTENT_MAX_COUNT = 500;

    private boolean isModifyQues = false,isCanModifyTitle = false;

    private String questionId,qaInfoId;

    private QuestionInputPersenter questionInputPersenter;

    //输入表情前的光标位置
    private int cursorPos;
    //输入表情前EditText中的文本
    private String inputAfterText;
    //是否重置了EditText的内容
    private boolean resetText;

    private final String WARNING_HINT = "温馨提示：请您仔细阅读提问规范，对不符合条件的提问，我们将按照规范进行处理，感谢您的理解和配合。点击查看完整《提问规范》";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question_intput);
        ButterKnife.bind(this);
        isModifyQues = getIntent().getBooleanExtra("isModifyQues",false);
        isCanModifyTitle = getIntent().getBooleanExtra("isCanModifyTitle",false);
        questionInputPersenter = new QuestionInputPersenter();
        questionInputPersenter.attachView(this);
        initView();
        initData();
    }

    @Override
    public void initView() {
        SpannableStringBuilder spannable = new SpannableStringBuilder(WARNING_HINT);
        spannable.setSpan(new ForegroundColorSpan(Color.parseColor("#ff8d34")),48,WARNING_HINT.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView_warning.setText(spannable);
//        WindowManager m = getWindowManager();
//        Display d = m.getDefaultDisplay();  //为获取屏幕宽、高
//        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams
//                ((int) (d.getWidth() * 0.4), RelativeLayout.LayoutParams.WRAP_CONTENT);
//        linearLayout_body.setLayoutParams(params);
        imageView_back.setImageResource(R.drawable.back);
        imageView_back.setOnClickListener(this);
        textView_warning.setOnClickListener(this);
        imageView_back.setVisibility(View.VISIBLE);
        textView_post.setText("提交");
        textView_post.setVisibility(View.VISIBLE);
        textView_post.setOnClickListener(this);
        if (!isModifyQues){
            linearLayout_title_body.setVisibility(View.GONE);
            textView_title.setText("追问");
        }else{
            if(!isCanModifyTitle)
                linearLayout_title_body.setVisibility(View.GONE);
            textView_title.setText("修改");
            String title_original = getIntent().getStringExtra("title");
            String content_original = getIntent().getStringExtra("content");
            String title,content;
            if(title_original.length()>30){
                title = title_original.substring(0,30);
            }else{
                title = title_original;
            }
            if(content_original.length()>500){
                content = content_original.substring(0,500);
            }else{
                content = content_original;
            }
            editText_title.setText(title);
            editText_title.setSelection(title.length());
            editText_content.setText(content);
            editText_content.setSelection(content.length());
            textView_title_count.setText(title.length() + "/" + TITLE_MAX_COUNT);
            textView_content_count.setText(content.length() + "/" + CONTENT_MAX_COUNT);
        }

        editText_title.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (!resetText) {
                    cursorPos = editText_content.getSelectionEnd();
                    // 这里用s.toString()而不直接用s是因为如果用s，
                    // 那么，inputAfterText和s在内存中指向的是同一个地址，s改变了，
                    // inputAfterText也就改变了，那么表情过滤就失败了
                    inputAfterText= s.toString();
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!resetText) {
                    if (count >= 2) {//表情符号的字符长度最小为2
                        CharSequence input = s.subSequence(cursorPos, cursorPos + count);
                        if (containsEmoji(input.toString())) {
                            resetText = true;
                            showError("不支持输入Emoji表情符号");
                            //是表情符号就将文本还原为输入表情符号之前的内容
                            editText_content.setText(inputAfterText);
                            CharSequence text = editText_content.getText();
                            if (text instanceof Spannable) {
                                Spannable spanText = (Spannable) text;
                                Selection.setSelection(spanText, text.length());
                            }
                        }
                    }
                } else {
                    resetText = false;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                int count = editText_title.getText().toString().length();
                textView_title_count.setText(count + "/" + TITLE_MAX_COUNT);
            }
        });

        editText_content.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (!resetText) {
                    cursorPos = editText_content.getSelectionEnd();
                    // 这里用s.toString()而不直接用s是因为如果用s，
                    // 那么，inputAfterText和s在内存中指向的是同一个地址，s改变了，
                    // inputAfterText也就改变了，那么表情过滤就失败了
                    inputAfterText= s.toString();
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!resetText) {
                    if (count >= 2) {//表情符号的字符长度最小为2
                        CharSequence input = s.subSequence(cursorPos, cursorPos + count);
                        if (containsEmoji(input.toString())) {
                            resetText = true;
                            showError("不支持输入Emoji表情符号");
                            //是表情符号就将文本还原为输入表情符号之前的内容
                            editText_content.setText(inputAfterText);
                            CharSequence text = editText_content.getText();
                            if (text instanceof Spannable) {
                                Spannable spanText = (Spannable) text;
                                Selection.setSelection(spanText, text.length());
                            }
                        }
                    }
                } else {
                    resetText = false;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                int count = editText_content.getText().toString().length();
                textView_content_count.setText(count + "/" + CONTENT_MAX_COUNT);
            }
        });
        textView_post.setOnClickListener(this);
    }

    @Override
    public void initData() {
        questionId = getIntent().getStringExtra("questionId");
        qaInfoId = getIntent().getStringExtra("qaInfoId");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.add_question_warning:
                Intent intent_warning = new Intent(this, WebViewActivity.class);
                intent_warning.putExtra(Constants.APP_WEBVIEW_TITLE,"提问规范说明");
                intent_warning.putExtra(Constants.APP_WEBVIEW_URL,"http://m.bjsteach.com/app/question/rule.html");
                startActivity(intent_warning);
                break;
            case R.id.top_title_left:
                View view = getWindow().peekDecorView();
                if (view != null) {
                    InputMethodManager inputmanger = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputmanger.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                finish();
                break;
            case R.id.tv_right:
                if(isModifyQues){
                    if(TextUtils.isEmpty(editText_title.getText().toString())){
                        showError("请输入标题");
                        return;
                    }
                    if(TextUtils.isEmpty(editText_content.getText().toString())){
                        showError("请输入内容");
                        return;
                    }
                    if(editText_content.getText().toString().length()<10){
                        showError("提问内容不少于10个字");
                        return;
                    }
                    questionInputPersenter.getData();
                }else{
                    if(TextUtils.isEmpty(editText_content.getText().toString())){
                        showError("请输入内容");
                        return;
                    }
                    if(editText_content.getText().toString().length()<10){
                        showError("提问内容不少于10个字");
                        return;
                    }
                    questionInputPersenter.zhuiwen();
                }
                break;
        }
    }

    @Override
    public String getQuestionId() {
        return questionId;
    }

    @Override
    public String getQaInfoId() {
        return qaInfoId;
    }

    @Override
    public String title() {
        return editText_title.getText().toString();
    }

    @Override
    public String content() {
        return editText_content.getText().toString();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {
        MyToast.makeText(this,message, Toast.LENGTH_SHORT);
    }

    @Override
    public Context context() {
        return this;
    }

    @Override
    public void modifySuccess() {
        EventBus.getDefault().post(new ReloadQuestion());
        Toast.makeText(this,"修改成功",Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void zhuiSuccess() {
        EventBus.getDefault().post(new ReloadQuestion());
        Toast.makeText(this, "追问成功", Toast.LENGTH_SHORT).show();
        finish();
    }

    /**
     * 检测是否有emoji表情
     *
     * @param source
     * @return
     */
    public static boolean containsEmoji(String source) {
        int len = source.length();
        for (int i = 0; i < len; i++) {
            char codePoint = source.charAt(i);
            if (!isEmojiCharacter(codePoint)) { //如果不能匹配,则该字符是Emoji表情
                return true;
            }
        }
        return false;
    }

    /**
     * 判断是否是Emoji
     *
     * @param codePoint 比较的单个字符
     * @return
     */
    private static boolean isEmojiCharacter(char codePoint) {
        return (codePoint == 0x0) || (codePoint == 0x9) || (codePoint == 0xA) ||
                (codePoint == 0xD) || ((codePoint >= 0x20) && (codePoint <= 0xD7FF)) ||
                ((codePoint >= 0xE000) && (codePoint <= 0xFFFD)) || ((codePoint >= 0x10000)
                && (codePoint <= 0x10FFFF));
    }

}
