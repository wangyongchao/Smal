package com.bjjy.mainclient.phone.view.play;

import android.content.Intent;

import com.dongao.mainclient.model.mvp.MvpView;
import com.dongao.mainclient.model.bean.play.CourseWare;
import com.dongao.mainclient.model.bean.play.CourseDetail;

import java.util.List;

/**
 * Created by fengzongwei on 2016/5/30 0030.
 */
public interface PlayView extends MvpView{

    Intent getPLayIntent();
    void showData(CourseDetail courseDetail,String lastVideoId);//展示当前
    void showOtherLogin();//异地登录被踢出
    void showCwData(List<CourseWare> courseWareList,String lastVideoId);//显示不带章节的课节数据
    void loadError();//数据加载失败
    boolean isPlayLocal();//是否是播放的本地视频
    void playError(String msg);
    void playNew(CourseWare courseWare);
    void isOldPlay(boolean isOld);//是否是3.0的视频
    void showNetError();
    CourseWare getCurrentCourseWare();
    
}
