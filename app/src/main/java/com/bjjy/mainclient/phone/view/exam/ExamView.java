package com.bjjy.mainclient.phone.view.exam;


import android.content.Intent;
import android.support.v4.view.ViewPager;
import com.dongao.mainclient.model.mvp.MvpView;
import com.bjjy.mainclient.phone.app.AppContext;
import com.bjjy.mainclient.phone.widget.EmptyViewLayout;

public interface ExamView extends MvpView {
  void intent();
  void out();
  Intent getWayIntent();
  void showTotalNumber(String totalNumber);
  void showCurrentNumber(int currentNumber);
  void showExaminationTittle(String title);
  ViewPager getViewPager();
  void showBottomTittle(int type);
  void showBottomTittlePress(int type);
  void showTopTittle(String title);
  void updateQuestionItemAdapter();
  void showExamTime(String time);
  void finishActivity();
  void setVPIndex(int position);
  void vpNotifyData();
  void intentExamReportActivity();
  void showTopTitleRight(boolean isShow);
  void showExamPagerBottom(boolean isShow);
  void showExamPagerBottomReportLayou(boolean isShow);
  AppContext getAppContext();
  void showExamTimeOrNot(boolean isShow);
  void showExamBottomRight(int tag);
  void showBackPress();
  EmptyViewLayout getEmptyView();
  void progressStatus(int status);
  void showDuoxuanGuide(boolean isShow);
}
