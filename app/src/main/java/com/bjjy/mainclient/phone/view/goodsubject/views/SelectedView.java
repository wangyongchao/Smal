package com.bjjy.mainclient.phone.view.goodsubject.views;


import com.dongao.mainclient.model.mvp.MvpView;
import com.dongao.mainclient.model.bean.courselect.DiscountCode;
import com.dongao.mainclient.model.bean.courselect.Goods;
import com.dongao.mainclient.model.bean.courselect.GoodsInfos;
import com.dongao.mainclient.model.bean.courselect.GoodsResult;

import java.util.List;


/**
 * 登录UI 定义UI 看此UI中有何事件
 */
public interface SelectedView extends MvpView {
  void setAdapter(List<GoodsInfos> examList);
  void setResult(GoodsResult resultData);
  void setUsedCards(List<DiscountCode> usedCardsList);
  List<Goods> listData();
  List<String> listCards();
  List<String> listUsedCards();
//  void refresh();
}
