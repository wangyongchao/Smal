package com.bjjy.mainclient.phone.view.classroom.subject;


import com.dongao.mainclient.model.mvp.MvpView;
import com.dongao.mainclient.model.bean.course.Exam;

import java.util.List;

/**
 * 登录UI 定义UI 看此UI中有何事件
 */
public interface SubjectFragmentView extends MvpView {
  void setAdapter(List<Exam> examList);
}
