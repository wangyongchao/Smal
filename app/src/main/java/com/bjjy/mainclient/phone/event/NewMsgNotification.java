package com.bjjy.mainclient.phone.event;


/**
 * Created by wyc on 
 * @time 2016.06.13
 * 
 */
public class NewMsgNotification {
    private static final String TAG = "PushMsgNotification";
    public boolean isRead;
    public NewMsgNotification(boolean isRead){
        this.isRead=isRead;
    }
}
