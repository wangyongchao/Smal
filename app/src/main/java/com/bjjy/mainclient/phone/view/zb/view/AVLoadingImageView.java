package com.bjjy.mainclient.phone.view.zb.view;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.LinearInterpolator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fishzhang on 2017/9/29.
 */

public class AVLoadingImageView extends View {
    private static final int DEFULT_COLOR= Color.parseColor("#ffffff");
    private static final float SCALE=0.3f;
    private static final long ANIM_S=700;
    private int itemCount=3;
    private float itemWidth;
    private float itemDist;
    private int itemMaxHeight=0;
    private float[] scale=new float[]{SCALE,SCALE,SCALE,SCALE};
    private long[] delays=new long[]{ANIM_S/2*3/6,ANIM_S/2*2/6,0,ANIM_S/2*4/6};
    private List<ValueAnimator> animators=new ArrayList<>();
    private Paint mPaint;
    private Context context;
    private RectF area;

    public AVLoadingImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context=context;
        mPaint=new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setColor(DEFULT_COLOR);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
//        itemMaxHeight=h;
//        itemWidth=w/(itemCount*2-1);
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        itemWidth= TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,1.5f,displayMetrics);
        itemDist= TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,2f,displayMetrics);
        float left=(w-itemWidth*itemCount-itemDist*(itemCount-1))/2;
        float right=left+itemWidth*itemCount+itemDist*(itemCount-1);
        area=new RectF(left,0,right,h);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float left=area.left;
        for (int i = 0; i < itemCount; i++) {
            RectF rectF=new RectF(left,area.height()*(1-scale[i]),left+itemWidth,area.height());
            canvas.drawRoundRect(rectF,0,0,mPaint);
            left+=itemWidth+itemDist;
        }
    }

    public void startAnim(){
        if (animators.isEmpty()){
            for (int i = 0; i < itemCount; i++) {
                final int index=i;
                ValueAnimator va= ValueAnimator.ofFloat(SCALE,1f,SCALE);
                va.setStartDelay(delays[index]);
                va.setRepeatCount(ValueAnimator.INFINITE);
                va.setDuration(ANIM_S);
                va.setInterpolator(new LinearInterpolator());
                va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        scale[index]=(float)animation.getAnimatedValue();
                        postInvalidate();
                    }
                });
                va.start();
                animators.add(va);
            }
        }
    }

    public void stopAnim(){
        if (!animators.isEmpty()){
            for (ValueAnimator va : animators) {
                if (va!=null && va.isStarted()){
                    va.removeAllUpdateListeners();
                    va.cancel();
                }
            }
            animators.clear();
        }
    }

}
