package com.bjjy.mainclient.phone.download.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.alibaba.fastjson.JSON;
import com.dongao.mainclient.model.bean.course.CoursePlay;
import com.dongao.mainclient.model.bean.play.CourseWare;
import com.dongao.mainclient.model.bean.play.CwStudyLog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class BeforeDB {
    private DownloadBeforeDB db;
    private Context mContext;
    private HelperDB oldcourse;

    public BeforeDB(Context context) {
        mContext = context;
        db = new DownloadBeforeDB(context);
        oldcourse=new HelperDB(context);
    }

    /**
     * 移植2.0的下载数据
     * @param
     * @return
     */
    public List<CourseWare> beforeData() {
        List<CourseWare> list = new ArrayList();
        if (tabIsExist("download")) {
            SQLiteDatabase sql = db.getReadableDatabase();
            Cursor cursor = sql.query("download", null, null,null, null, null, null);
            while (cursor.moveToNext()) {
                CourseWare ware=getCw(cursor.getInt(4) + "", cursor.getInt(6) + "");
                if(ware!=null){
                    CourseWare course = new CourseWare();
                    course.setUserId(cursor.getInt(1)+"");
                    course.setClassId(cursor.getInt(4) + "");
                    course.setExamId(cursor.getInt(2) + "");
                    course.setCwId(cursor.getInt(6) + "");
                    course.setSubjectId(cursor.getInt(3) + "");
                    course.setSectionId(cursor.getInt(5) + "");
                    course.setState(cursor.getInt(8));
                    course.setMobileLectureUrl(ware.getMobileLectureUrl());
                    course.setMobileDownloadUrl(ware.getMobileDownloadUrl());
                    course.setCwName(ware.getCwName());
                    course.setCourseBean(JSON.toJSONString(find(cursor.getInt(4) + "")));
                    course.setCwBean(JSON.toJSONString(course));
                    list.add(course);
                }
            }
            cursor.close();
            sql.close();
        }
        return list;
    }

    public List<String> getUsers() {
        List<String> list = new ArrayList();
        if (tabIsExist("download")) {
            SQLiteDatabase sql = db.getReadableDatabase();
            Cursor cursor = sql.query("download", null, null,null, null, null, null);
            while (cursor.moveToNext()) {
                String userId=cursor.getInt(1)+"";
                if(!list.contains(userId)){
                    list.add(userId);
                }
            }
            cursor.close();
            sql.close();
        }
        return list;
    }

    public CoursePlay find(String courseId) {
        CoursePlay course=null;
        SQLiteDatabase sql = oldcourse.getReadableDatabase();
        Cursor cursor = sql.query("courseOld", null, "courseId=?",
                new String[]{courseId}, null, null, null);
        if (cursor.moveToNext()) {
            course = new CoursePlay();
            course.setDaPersonImg(cursor.getString(3));
            course.setDaPersonName(cursor.getString(5));
            course.setName(cursor.getString(4));
        }
        cursor.close();
        sql.close();
        return course;
    }

    public CourseWare getCw(String classId, String cwId) {
        CourseWare courseWare=null;
        SQLiteDatabase sql = db.getReadableDatabase();

        if (tabIsExist("t_course_ware")) {
            Cursor cursor = sql.query("t_course_ware", null, "courseId=? and uid=?",
                    new String[]{classId, cwId}, null, null, null);
            if (cursor.moveToNext()) {
                courseWare=new CourseWare();
                courseWare.setCwName(cursor.getString(2));
                courseWare.setMobileDownloadUrl(cursor.getString(11));
                courseWare.setMobileLectureUrl(cursor.getString(12));
            }
            cursor.close();
            sql.close();
        }
        return courseWare;
    }

    public List<CwStudyLog> getPreStudyLog(){
        List<CwStudyLog> list = new ArrayList();
        if (tabIsExist("studylog")) {
            SQLiteDatabase sql = db.getReadableDatabase();
            Cursor cursor = sql.query("studylog", null, null,null, null, null, null);
            while (cursor.moveToNext()) {
                CwStudyLog studyLog = new CwStudyLog();
                studyLog.setEndTime(cursor.getInt(7));
                studyLog.setUserId(cursor.getInt(1) + "");
                studyLog.setExamId(cursor.getString(2));
                if(getCw(cursor.getString(4),cursor.getString(6))==null){
                    studyLog.setCwName("其他");
                }else{
                    studyLog.setCwName(getCw(cursor.getString(4),cursor.getString(6)).getCwName());
                }
                studyLog.setSubjectId(cursor.getString(3));
                studyLog.setCourseId(cursor.getString(4));
                studyLog.setSectionId(cursor.getString(5));
                studyLog.setLastUpdateTime(initimes(cursor.getString(8)));
                studyLog.setCwid(cursor.getString(6));
                list.add(studyLog);
            }
            cursor.close();
            sql.close();
        }
        return list;
    }

    private String initimes(String time){
        long millionSeconds=0;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            millionSeconds = sdf.parse(time).getTime();//毫秒
        }catch (Exception e){

        }
        return millionSeconds+"";
    }


    public boolean tabIsExist(String tabName) {
        SQLiteDatabase database = db.getReadableDatabase();
        boolean result = false;
        if (tabName == null) {
            return false;
        }
        Cursor cursor = null;
        try {

            String sql = "select count(*) as c from sqlite_master where type ='table' and name ='" + tabName.trim() + "' ";
            cursor = database.rawQuery(sql, null);
            if (cursor.moveToNext()) {
                int count = cursor.getInt(0);
                if (count > 0) {
                    result = true;
                }
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
        cursor.close();
        return result;
    }
}
