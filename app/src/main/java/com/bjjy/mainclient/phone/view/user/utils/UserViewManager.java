package com.bjjy.mainclient.phone.view.user.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dongao.mainclient.core.util.NetUtils;
import com.dongao.mainclient.model.common.Constants;
import com.bjjy.mainclient.persenter.UserPersenter;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.view.setting.WebViewActivity;
import com.bjjy.mainclient.phone.view.user.UserView;

/**
 * Created by fengzongwei on 2016/5/9 0009.
 * 用户中心布局显示管理
 */
public class UserViewManager implements UserView,View.OnClickListener{

    private Activity activity;

    private  PopupWindow popupWindow;
    private  LayoutInflater layoutInflater;

    private  View userView;
    private  LinearLayout linearLayout_login;
    private  LinearLayout linearLayout_registe;
    private  RelativeLayout container;
    private  RelativeLayout contaier_parent;
    private  TextView textView_title;
    //登录相关view
    private  EditText usernameEdit;
    private  EditText passwordEdit;
    private  Button login_bt;
    private ImageView login_imageView_claer;
    private TextView textView_login_pass;
    private LinearLayout linearLayout_login_error;
    private TextView login_error_hint;

    //注册相关view
    private  EditText editText_phone;
    private  EditText editText_checknumber;
    private  EditText editText_psw;
    private  TextView bt_checkNumber;
    private  Button button_registe;
    private ImageView imageView_seePsw,registe_imageView_checkNum_clear,registe_imageView_psw_clear;
    private TextView textView_registe_pass;
    private LinearLayout linearLayout_registe_error;
    private TextView registe_error_hint;

    private UserPersenter userPersenter;
    private LoginLinstener loginLinstener;

    private RotateAnimationUtil rotateAnimationUtil;
    private int screenWidth;

    private static final String PHONE = "^1[3|4|5|8|7][0-9]\\d{8}$";

    private boolean isShowPswNow = false;//当前密码是否可见
    private boolean isLoginView = true;//当前是否登录布局在显示
    private boolean isCandismiss = true;//当前是否可以消失掉此页面

    private String resend = "倒计时";
    private boolean isGetValidNow = false;
    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if(msg.what == 0){
                isGetValidNow = false;
                bt_checkNumber.setText("重新获取");
                bt_checkNumber.setBackgroundDrawable(activity.getResources().getDrawable(R.drawable.registe_check_bt));
                bt_checkNumber.setTextColor(Color.WHITE);
                bt_checkNumber.setEnabled(true);
                return;
            }
            bt_checkNumber.setText(resend+"("+msg.what+"s"+")");
        }
    };

    public UserViewManager(Activity activity,LoginLinstener loginLinstener){
        this.activity = activity;
        this.loginLinstener = loginLinstener;
        WindowManager wm = activity.getWindowManager();
        screenWidth = wm.getDefaultDisplay().getWidth();
        userPersenter = new UserPersenter();
        userPersenter.attachView(this);
        initView();
    }

    public void showUserCenterWindow(){
        usernameEdit.setText("");
        passwordEdit.setText("");
        setAble();
        if(!isLoginView){
            isLoginView = true;
            rotateAnimationUtil.applyRotateAnimation(-1, 0, -90);
        }
        popupWindow.showAtLocation(activity.getWindow().getDecorView(), Gravity.FILL, 0, 0);
    }

    public void showUserCenterWindow(String username,String password){
        usernameEdit.setText(username);
        passwordEdit.setText(password);
        setAble();
        if(!isLoginView){
            isLoginView = true;
            rotateAnimationUtil.applyRotateAnimation(-1, 0, -90);
        }
        popupWindow.showAtLocation(activity.getWindow().getDecorView(), Gravity.FILL, 0, 0);
    }

    public boolean isShowUserPopupWindow(){
        return popupWindow!=null && popupWindow.isShowing();
    }

    public void dissmissPopupWindow(){
        if(isCandismiss) {
            popupWindow.dismiss();
            linearLayout_login_error.setVisibility(View.GONE);
            linearLayout_registe_error.setVisibility(View.GONE);
        }
    }

    private void initView(){
        layoutInflater = LayoutInflater.from(activity);
        userView = layoutInflater.inflate(R.layout.user,null);
        popupWindow = new PopupWindow(userView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        popupWindow.setContentView(userView);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        popupWindow.setFocusable(true);
        popupWindow.setAnimationStyle(R.style.main_pop_anim);
        findView();
    }

    private void findView(){
        linearLayout_login = (LinearLayout)userView.findViewById(R.id.login_container);
        linearLayout_registe = (LinearLayout)userView.findViewById(R.id.registe_container);
        container = (RelativeLayout)userView.findViewById(R.id.container);
        ViewGroup.LayoutParams layoutParams = container.getLayoutParams();
        layoutParams.width = (int)(screenWidth*0.8);
        container.setLayoutParams(layoutParams);
        contaier_parent = (RelativeLayout)userView.findViewById(R.id.container_parent);
        textView_title = (TextView)userView.findViewById(R.id.top_title_text);
        usernameEdit = (EditText)userView.findViewById(R.id.login_username_et);
        usernameEdit.addTextChangedListener(loginName);
        passwordEdit = (EditText)userView.findViewById(R.id.login_psw_et);
        login_bt = (Button)userView.findViewById(R.id.login_bt);
        editText_phone = (EditText)userView.findViewById(R.id.registe_phone_et);
        editText_checknumber = (EditText)userView.findViewById(R.id.registe_phone_checknumber_et);
        editText_checknumber.addTextChangedListener(valiCode);
        editText_psw = (EditText)userView.findViewById(R.id.registe_psw_et);
        editText_psw.addTextChangedListener(registeWatcher);
        bt_checkNumber = (TextView)userView.findViewById(R.id.registe_get_checcknumber_bt);
        button_registe = (Button)userView.findViewById(R.id.registe_bt);
        login_imageView_claer = (ImageView)userView.findViewById(R.id.login_clear);
        registe_imageView_checkNum_clear = (ImageView)userView.findViewById(R.id.registe_clear);
        registe_imageView_psw_clear = (ImageView)userView.findViewById(R.id.registe_psw_clear_img);
        imageView_seePsw = (ImageView)userView.findViewById(R.id.registe_psw_seePsw_img);
        textView_login_pass = (TextView)userView.findViewById(R.id.login_pass);
        textView_registe_pass = (TextView)userView.findViewById(R.id.registe_pass);
        linearLayout_login_error = (LinearLayout)userView.findViewById(R.id.login_error_body);
        linearLayout_registe_error = (LinearLayout)userView.findViewById(R.id.registe_error_body);
        login_error_hint = (TextView)userView.findViewById(R.id.login_error_hint);
        registe_error_hint = (TextView)userView.findViewById(R.id.registe_error_hint);

        registe_imageView_psw_clear.setOnClickListener(this);
        contaier_parent.setOnClickListener(this);
        textView_registe_pass.setOnClickListener(this);
        textView_login_pass.setOnClickListener(this);
        login_imageView_claer.setOnClickListener(this);
        registe_imageView_checkNum_clear.setOnClickListener(this);
        imageView_seePsw.setOnClickListener(this);
        userView.findViewById(R.id.login_registe).setOnClickListener(this);
        userView.findViewById(R.id.registe_login).setOnClickListener(this);
        userView.findViewById(R.id.registe_ruel_tv).setOnClickListener(this);
        userView.findViewById(R.id.registe_get_checcknumber_bt).setOnClickListener(this);
        userView.findViewById(R.id.login_bt).setOnClickListener(this);
        userView.findViewById(R.id.registe_bt).setOnClickListener(this);
        rotateAnimationUtil = new RotateAnimationUtil(container, linearLayout_login,
                linearLayout_registe);
    }

    @Override
    public String username() {
        return usernameEdit.getText().toString();
    }

    @Override
    public String password() {
        return passwordEdit.getText().toString();
    }

    @Override
    public void loginSuccess() {
        loginLinstener.loginSuccess();
        dissmissPopupWindow();
    }

    @Override
    public String phoneNumber() {
        return editText_phone.getText().toString();
    }

    @Override
    public String checkNumber() {
        return editText_checknumber.getText().toString();
    }

    @Override
    public String pswRegiste() {
        return editText_psw.getText().toString();
    }

    @Override
    public void registeSuccess() {
        login_bt.setEnabled(true);
        button_registe.setEnabled(true);
        editText_checknumber.setEnabled(true);
        editText_phone.setEnabled(true);
        editText_psw.setEnabled(true);
        passwordEdit.setEnabled(true);
        usernameEdit.setEnabled(true);
        usernameEdit.setText(phoneNumber());
        passwordEdit.setText(pswRegiste());
        rotateAnimationUtil.applyRotateAnimation(-1, 0, -90);
        Toast.makeText(activity,"注册成功",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void switchBtStatus() {
        isGetValidNow = true;
        bt_checkNumber.setText( 60 + "s");
        bt_checkNumber.setTextColor(Color.parseColor("#999999"));
        bt_checkNumber.setBackgroundDrawable(activity.getResources().getDrawable(R.drawable.registe_check_enable_bt));
        new Thread() {
            @Override
            public void run() {
                try {
                    int i = 59;
                    while (i >= 0) {
                        handler.sendEmptyMessage(i);
                        i--;
                        Thread.sleep(1000);
                    }
                } catch (InterruptedException e) {
                }
            }
        }.start();
    }

    @Override
    public void showLoading() {
        linearLayout_login_error.setVisibility(View.GONE);
        linearLayout_registe_error.setVisibility(View.GONE);
        setUnable();
    }

    @Override
    public void hideLoading() {
        isCandismiss = true;
        setAble();
    }

    private void setAble(){
        imageView_seePsw.setEnabled(true);
        login_imageView_claer.setEnabled(true);
        registe_imageView_checkNum_clear.setEnabled(true);
        login_bt.setEnabled(true);
        button_registe.setEnabled(true);
        editText_checknumber.setEnabled(true);
        editText_phone.setEnabled(true);
        editText_psw.setEnabled(true);
        passwordEdit.setEnabled(true);
        usernameEdit.setEnabled(true);
    }

    private void setUnable(){
        imageView_seePsw.setEnabled(false);
        login_imageView_claer.setEnabled(false);
        registe_imageView_checkNum_clear.setEnabled(false);
        login_bt.setEnabled(false);
        button_registe.setEnabled(false);
        editText_checknumber.setEnabled(false);
        editText_phone.setEnabled(false);
        editText_psw.setEnabled(false);
        passwordEdit.setEnabled(false);
        usernameEdit.setEnabled(false);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {
        hideLoading();
        if(isLoginView){
            linearLayout_login_error.setVisibility(View.VISIBLE);
            linearLayout_registe_error.setVisibility(View.GONE);
            login_error_hint.setText(message);
        }else{
            linearLayout_login_error.setVisibility(View.GONE);
            linearLayout_registe_error.setVisibility(View.VISIBLE);
            registe_error_hint.setText(message);
        }
    }

    @Override
    public Context context() {
        return activity;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.login_registe:
                isLoginView = false;
                rotateAnimationUtil.applyRotateAnimation(1, 0, 90);
                break;
            case R.id.registe_login:
                isLoginView = true;
                rotateAnimationUtil.applyRotateAnimation(-1, 0, -90);
                break;
            case R.id.registe_ruel_tv:
                Intent intent = new Intent(activity, WebViewActivity.class);
                intent.putExtra(Constants.APP_WEBVIEW_TITLE, "服务条款和细则");
                intent.putExtra(Constants.APP_WEBVIEW_URL, "http://member.bjsteach.com/api/showIosTerms.html");
                activity.startActivity(intent);
                break;
            case R.id.registe_get_checcknumber_bt:
                if(NetUtils.checkNet().available) {
                    if (editText_phone.getText().toString() == null || !editText_phone.getText().toString().matches(PHONE)) {
                        showError("请输入正确的手机号");
                        return;
                    }
                    if (!isGetValidNow && bt_checkNumber.getText().toString().contains("重新获取") || bt_checkNumber.getText().toString().equals("获取验证码")) {
                        isGetValidNow = true;
                        userPersenter.getMobileValid();
                    }
                }
                break;
            case R.id.login_bt:
                isCandismiss = false;
                userPersenter.getData();
                break;
            case R.id.registe_psw_clear_img:
                editText_psw.setText("");
                break;
            case R.id.registe_bt:
                userPersenter.regitse();
                break;
            case R.id.login_clear:
                usernameEdit.setText("");
                break;
            case R.id.registe_clear:
                editText_checknumber.setText("");
                break;
            case R.id.registe_psw_seePsw_img:
                if(!isShowPswNow){
                    isShowPswNow = true;
                    editText_psw.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }else{
                    isShowPswNow = false;
                    editText_psw.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
                break;
            case R.id.login_pass:
                dissmissPopupWindow();
                break;
            case R.id.registe_pass:
                dissmissPopupWindow();
                break;
            case R.id.container_parent:
//                dissmissPopupWindow();
                break;
        }
    }

    private TextWatcher loginName = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (usernameEdit.getText().toString() != null && !usernameEdit.getText().toString().equals("")) {
                login_imageView_claer.setVisibility(View.VISIBLE);
            } else {
                login_imageView_claer.setVisibility(View.INVISIBLE);
            }
        }
    };

    private TextWatcher valiCode = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (editText_checknumber.getText().toString() != null && !editText_checknumber.getText().toString().equals("")) {
                registe_imageView_checkNum_clear.setVisibility(View.VISIBLE);
            } else {
                registe_imageView_checkNum_clear.setVisibility(View.INVISIBLE);
            }
        }
    };

    private TextWatcher registeWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (editText_psw.getText().toString() != null && !editText_psw.getText().toString().equals("")) {
                imageView_seePsw.setVisibility(View.VISIBLE);
                registe_imageView_psw_clear.setVisibility(View.VISIBLE);
            } else {
                imageView_seePsw.setVisibility(View.INVISIBLE);
                registe_imageView_psw_clear.setVisibility(View.INVISIBLE);
            }
        }
    };


    public interface LoginLinstener{
        /**
         * 登录成功的回调
         */
        void loginSuccess();
    }

}
