package com.bjjy.mainclient.phone.view.zb.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.view.zb.bean.LivePlayDataBean;
import com.bjjy.mainclient.phone.view.zb.utils.GlideUtils;
import com.bjjy.mainclient.phone.view.zb.view.AVLoadingImageView;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.dongao.mainclient.core.util.StringUtil;

import java.util.ArrayList;

/**
 * Created by dell on 2016/1/4.
 */
public class LivePlayAdapter extends BaseQuickAdapter<LivePlayDataBean, BaseViewHolder> {
    private LivePlayDataBeanItemClickListener livePlayDataBeanItemClickListener;
    private Context context;
    private String[] bgs=new String[]{"未直播","直播中","已结束"};


    public LivePlayAdapter(Context context, ArrayList<LivePlayDataBean> models) {
        super(R.layout.live_play_activity_item,models);
        this.context=context;
    }
    public void setLivePlayDataBeanClickListener(LivePlayDataBeanItemClickListener livePlayDataBeanItemClickListener){
        this.livePlayDataBeanItemClickListener=livePlayDataBeanItemClickListener;
    }

    @Override
    protected void convert(final BaseViewHolder helper, LivePlayDataBean item) {
        ImageView courseplay_img=helper.getView(R.id.courseplay_img);
        if (!StringUtil.isEmpty(item.getImageUrl())){
            GlideUtils.loadImage(context,courseplay_img,item.getImageUrl());
        }

        helper.setText(R.id.courseplay_title,item.getChannelName());
        AVLoadingImageView avloading_iv=helper.getView(R.id.avloading_iv);
        avloading_iv.startAnim();
        if(!StringUtil.isEmpty(item.getChannelStartTimes())&&item.getChannelStartTimes().contains(":")&&item.getChannelStartTimes().lastIndexOf(":")!=item.getChannelStartTimes().indexOf(":")){
            item.setChannelStartTimes(item.getChannelStartTimes().substring(0,item.getChannelStartTimes().lastIndexOf(":")));
        }
        if(!StringUtil.isEmpty(item.getChannelEndTimes())&&item.getChannelEndTimes().contains(":")&&item.getChannelEndTimes().lastIndexOf(":")!=item.getChannelEndTimes().indexOf(":")){
            item.setChannelEndTimes(item.getChannelEndTimes().substring(0,item.getChannelEndTimes().lastIndexOf(":")));
        }
        String time=item.getChannelEndTimes()==null?"":("-"+item.getChannelEndTimes());
        helper.setText(R.id.courseplay_count,"直播时间："+item.getChannelStartTimes()+time);
        helper.setText(R.id.courseplay_status,bgs[item.getStatus()%3]);
        if (item.getStatus()==1){
            helper.getView(R.id.ll_live_ing).setEnabled(true);
        }else{
            helper.getView(R.id.ll_live_ing).setEnabled(false);
        }

        helper.getConvertView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (livePlayDataBeanItemClickListener!=null){
                    livePlayDataBeanItemClickListener.LivePlayDataBeanClick(helper.getPosition(),0);
                }
            }
        });
    }

    public  interface LivePlayDataBeanItemClickListener{
        public void  LivePlayDataBeanClick(int position, long l);
    }

}
