package com.bjjy.mainclient.phone.view.play.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseFragment;
import com.bjjy.mainclient.phone.utils.NetworkUtil;
import com.bjjy.mainclient.phone.view.play.PlayActivity;
import com.dongao.mainclient.core.util.FileUtil;
import com.dongao.mainclient.core.util.StringUtil;
import com.dongao.mainclient.model.bean.play.CourseWare;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.remote.bean.BaseBean;

import java.io.File;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 我的课程中的 课程介绍
 */
public class CourseIntroFragment extends BaseFragment {

    @Bind(R.id.course_info)
    TextView courseInfo;
    @Bind(R.id.wv_content)
    WebView wv_content;
    private String intro;
    private static final String mimeType = "text/html";
    private static final String encoding = "utf-8";
    private PlayActivity playActivity;
    private CourseWare courseWare;

    public CourseIntroFragment() {
    }

    public static CourseIntroFragment getInstance(String intro) {
        CourseIntroFragment f = new CourseIntroFragment();
        Bundle args = new Bundle();
        args.putString("intro", intro);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        playActivity = (PlayActivity) getActivity();
//        Bundle arguments = getArguments();
//        intro = arguments.getString("intro");
    }

    @Override
    public void initView() {
        
    }

    @Override
    public void initData() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.play_course_intro_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    public void setCourseInfo(String info) {
        courseWare = playActivity.getPlayingCw();
//        if(!TextUtils.isEmpty(info)){
//            courseInfo.setText(info);
//        }else{
//            courseInfo.setText("暂无介绍");
//        }
        
        loadUrl(info);
    }

    private void loadUrl(String urlString) {
        if (StringUtil.isEmpty(urlString)) {
            File lectrueFile = new File(FileUtil.getDownloadPath(getActivity()) + SharedPrefHelper.getInstance(getActivity()).getUserId() + "_" + courseWare.getExamId() + "_" + courseWare.getSubjectId() + "_" + courseWare.getClassId() + "_" + courseWare.getSectionId() + "_" + courseWare.getCwId() + "/"+ FileUtil.INTRO+"/intro.htm");
            if (lectrueFile.exists()) {
                urlString = "http://localhost:" + Constants.SERVER_PORT + "/" + SharedPrefHelper.getInstance(getActivity()).getUserId() + "_" + courseWare.getExamId() + "_" + courseWare.getSubjectId() + "_" + courseWare.getClassId() + "_" + courseWare.getSectionId() + "_" + courseWare.getCwId() + "/"+ FileUtil.INTRO+"/intro.htm";
                wv_content.loadUrl(urlString);
            } else {
                if (NetworkUtil.isNetworkAvailable(getActivity())) {
                    wv_content.loadDataWithBaseURL("", "暂无课程简介", mimeType, encoding, "");
                } else {
                    wv_content.loadDataWithBaseURL("", "网络不可用 请检查网络连接", mimeType, encoding, "");
                }
            }
        } else if (urlString.contains("http")) {
            //判断有没有网络
            if (NetworkUtil.isNetworkAvailable(getActivity())) {
                wv_content.loadUrl(urlString);
            } else {
                File lectrueFile = new File(FileUtil.getDownloadPath(getActivity()) + SharedPrefHelper.getInstance(getActivity()).getUserId() + "_" + courseWare.getExamId() + "_" + courseWare.getSubjectId() + "_" + courseWare.getClassId() + "_" + courseWare.getSectionId() + "_" + courseWare.getCwId() + "/"+ FileUtil.INTRO+"/intro.htm");
                if (lectrueFile.exists()) {
                    urlString = "http://localhost:" + Constants.SERVER_PORT + "/" + SharedPrefHelper.getInstance(getActivity()).getUserId() + "_" + courseWare.getExamId() + "_" + courseWare.getSubjectId() + "_" + courseWare.getClassId() + "_" + courseWare.getSectionId() + "_" + courseWare.getCwId()+ "/"+ FileUtil.INTRO+"/intro.htm";
                    wv_content.loadUrl(urlString);
                } else {
                    wv_content.loadDataWithBaseURL("", "网络不可用 请检查网络连接", mimeType, encoding, "");
                }
            }
        } else {
            try {
                BaseBean baseBean = JSON.parseObject(urlString, BaseBean.class);
                if (baseBean.getResult().getCode() == 9) {
//						playActivity.pauseVideo();
//						playActivity.showLoginOtherPlace();
                } else {
                    wv_content.loadDataWithBaseURL("", "课程详情加载失败", mimeType, encoding, "");
                }
            } catch (Exception e) {
                e.printStackTrace();
                wv_content.loadDataWithBaseURL("", "课程详情加载失败", mimeType, encoding, "");
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onClick(View v) {

    }
}
