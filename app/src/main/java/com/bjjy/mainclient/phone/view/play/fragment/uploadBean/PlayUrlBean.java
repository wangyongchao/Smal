package com.bjjy.mainclient.phone.view.play.fragment.uploadBean;

import java.util.List;

/**
 * Created by dell on 2016/12/6.
 */
public class PlayUrlBean {

    /**
     * speeds : 1.5|1.2|1.0
     * isEncrypt : 1
     * logoURL :
     * timeLenDeal : http://course.phoenix.com/exp/record/add
     * initialSound : 0.8
     * cwID : 6
     * hasGoOnPlay : true
     * scheme : sd|cif
     * videoId : 20101090
     * timeLenDealInterval : 10
     * captionTextColor :
     * memberID : 179
     * video : {"sd":{"three":["http://172.16.208.9:8080/b62/c8f/48a/024/xyt_gkch_zcyy_20_1_854x480_400kbps_m3u8_15/xyt_gkch_zcyy_20_1_854x480_400kbps_m3u8_15.m3u8"],"one":["http://172.16.208.9:8080/b62/c8f/48a/024/xyt_gkch_zcyy_20_1_854x480_400kbps_m3u8_10/xyt_gkch_zcyy_20_1_854x480_400kbps_m3u8_10.m3u8"],"two":["http://172.16.208.9:8080/b62/c8f/48a/024/xyt_gkch_zcyy_20_1_854x480_400kbps_m3u8_12/xyt_gkch_zcyy_20_1_854x480_400kbps_m3u8_12.m3u8"]},"cif":{"three":["http://172.16.208.9:8080/b62/c8f/48a/024/xyt_gkch_zcyy_20_1_640x360_200kbps_m3u8_15/xyt_gkch_zcyy_20_1_640x360_200kbps_m3u8_15.m3u8"],"one":["http://172.16.208.9:8080/b62/c8f/48a/024/xyt_gkch_zcyy_20_1_640x360_200kbps_m3u8_10/xyt_gkch_zcyy_20_1_640x360_200kbps_m3u8_10.m3u8"],"two":["http://172.16.208.9:8080/b62/c8f/48a/024/xyt_gkch_zcyy_20_1_640x360_200kbps_m3u8_12/xyt_gkch_zcyy_20_1_640x360_200kbps_m3u8_12.m3u8"]}}
     * captionTextSize :
     * allVideoMsg : http://course.phoenix.com/exp/video/kp?lectureID=34&cwID=6
     * initialScheme : sd
     * captionTextBold :
     * initialPlayerType : m3u8
     * recordID : 0afa1eb9-df88-467c-a524-6f5eed146506
     * endPlayTime :
     * cipherkeyDeal : http://course.phoenix.com/exp/cipherkey?videoId=20101090&appId=6380&sign=9af43d5159d8aaa967eee0db303dae54&timeStamp=1479469473
     * hasVideoTitle : false
     * logoY :
     * hasVideoTail : false
     * pauseType : 0
     * beginPlayTime : 00:00:00
     * lectureID : 34
     * logoX :
     * nextLectureID : null
     * isDrag : 1
     * captionURL :
     * initialSpeed : 1.0
     */

    private String speeds;
    private String isEncrypt;
    private String logoURL;
    private String timeLenDeal;
    private String initialSound;
    private String cwID;
    private String hasGoOnPlay;
    private String scheme;
    private String videoId;
    private String timeLenDealInterval;
    private String captionTextColor;
    private String memberID;
    /**
     * sd : {"three":["http://172.16.208.9:8080/b62/c8f/48a/024/xyt_gkch_zcyy_20_1_854x480_400kbps_m3u8_15/xyt_gkch_zcyy_20_1_854x480_400kbps_m3u8_15.m3u8"],"one":["http://172.16.208.9:8080/b62/c8f/48a/024/xyt_gkch_zcyy_20_1_854x480_400kbps_m3u8_10/xyt_gkch_zcyy_20_1_854x480_400kbps_m3u8_10.m3u8"],"two":["http://172.16.208.9:8080/b62/c8f/48a/024/xyt_gkch_zcyy_20_1_854x480_400kbps_m3u8_12/xyt_gkch_zcyy_20_1_854x480_400kbps_m3u8_12.m3u8"]}
     * cif : {"three":["http://172.16.208.9:8080/b62/c8f/48a/024/xyt_gkch_zcyy_20_1_640x360_200kbps_m3u8_15/xyt_gkch_zcyy_20_1_640x360_200kbps_m3u8_15.m3u8"],"one":["http://172.16.208.9:8080/b62/c8f/48a/024/xyt_gkch_zcyy_20_1_640x360_200kbps_m3u8_10/xyt_gkch_zcyy_20_1_640x360_200kbps_m3u8_10.m3u8"],"two":["http://172.16.208.9:8080/b62/c8f/48a/024/xyt_gkch_zcyy_20_1_640x360_200kbps_m3u8_12/xyt_gkch_zcyy_20_1_640x360_200kbps_m3u8_12.m3u8"]}
     */

    private VideoBean video;
    private String captionTextSize;
    private String allVideoMsg;
    private String initialScheme;
    private String captionTextBold;
    private String initialPlayerType;
    private String recordID;
    private String endPlayTime;
    private String cipherkeyDeal;
    private String hasVideoTitle;
    private String logoY;
    private String hasVideoTail;
    private String pauseType;
    private String beginPlayTime;
    private String lectureID;
    private String logoX;
    private Object nextLectureID;
    private String isDrag;
    private String captionURL;
    private String initialSpeed;

    public String getSpeeds() {
        return speeds;
    }

    public void setSpeeds(String speeds) {
        this.speeds = speeds;
    }

    public String getIsEncrypt() {
        return isEncrypt;
    }

    public void setIsEncrypt(String isEncrypt) {
        this.isEncrypt = isEncrypt;
    }

    public String getLogoURL() {
        return logoURL;
    }

    public void setLogoURL(String logoURL) {
        this.logoURL = logoURL;
    }

    public String getTimeLenDeal() {
        return timeLenDeal;
    }

    public void setTimeLenDeal(String timeLenDeal) {
        this.timeLenDeal = timeLenDeal;
    }

    public String getInitialSound() {
        return initialSound;
    }

    public void setInitialSound(String initialSound) {
        this.initialSound = initialSound;
    }

    public String getCwID() {
        return cwID;
    }

    public void setCwID(String cwID) {
        this.cwID = cwID;
    }

    public String getHasGoOnPlay() {
        return hasGoOnPlay;
    }

    public void setHasGoOnPlay(String hasGoOnPlay) {
        this.hasGoOnPlay = hasGoOnPlay;
    }

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public String getTimeLenDealInterval() {
        return timeLenDealInterval;
    }

    public void setTimeLenDealInterval(String timeLenDealInterval) {
        this.timeLenDealInterval = timeLenDealInterval;
    }

    public String getCaptionTextColor() {
        return captionTextColor;
    }

    public void setCaptionTextColor(String captionTextColor) {
        this.captionTextColor = captionTextColor;
    }

    public String getMemberID() {
        return memberID;
    }

    public void setMemberID(String memberID) {
        this.memberID = memberID;
    }

    public VideoBean getVideo() {
        return video;
    }

    public void setVideo(VideoBean video) {
        this.video = video;
    }

    public String getCaptionTextSize() {
        return captionTextSize;
    }

    public void setCaptionTextSize(String captionTextSize) {
        this.captionTextSize = captionTextSize;
    }

    public String getAllVideoMsg() {
        return allVideoMsg;
    }

    public void setAllVideoMsg(String allVideoMsg) {
        this.allVideoMsg = allVideoMsg;
    }

    public String getInitialScheme() {
        return initialScheme;
    }

    public void setInitialScheme(String initialScheme) {
        this.initialScheme = initialScheme;
    }

    public String getCaptionTextBold() {
        return captionTextBold;
    }

    public void setCaptionTextBold(String captionTextBold) {
        this.captionTextBold = captionTextBold;
    }

    public String getInitialPlayerType() {
        return initialPlayerType;
    }

    public void setInitialPlayerType(String initialPlayerType) {
        this.initialPlayerType = initialPlayerType;
    }

    public String getRecordID() {
        return recordID;
    }

    public void setRecordID(String recordID) {
        this.recordID = recordID;
    }

    public String getEndPlayTime() {
        return endPlayTime;
    }

    public void setEndPlayTime(String endPlayTime) {
        this.endPlayTime = endPlayTime;
    }

    public String getCipherkeyDeal() {
        return cipherkeyDeal;
    }

    public void setCipherkeyDeal(String cipherkeyDeal) {
        this.cipherkeyDeal = cipherkeyDeal;
    }

    public String getHasVideoTitle() {
        return hasVideoTitle;
    }

    public void setHasVideoTitle(String hasVideoTitle) {
        this.hasVideoTitle = hasVideoTitle;
    }

    public String getLogoY() {
        return logoY;
    }

    public void setLogoY(String logoY) {
        this.logoY = logoY;
    }

    public String getHasVideoTail() {
        return hasVideoTail;
    }

    public void setHasVideoTail(String hasVideoTail) {
        this.hasVideoTail = hasVideoTail;
    }

    public String getPauseType() {
        return pauseType;
    }

    public void setPauseType(String pauseType) {
        this.pauseType = pauseType;
    }

    public String getBeginPlayTime() {
        return beginPlayTime;
    }

    public void setBeginPlayTime(String beginPlayTime) {
        this.beginPlayTime = beginPlayTime;
    }

    public String getLectureID() {
        return lectureID;
    }

    public void setLectureID(String lectureID) {
        this.lectureID = lectureID;
    }

    public String getLogoX() {
        return logoX;
    }

    public void setLogoX(String logoX) {
        this.logoX = logoX;
    }

    public Object getNextLectureID() {
        return nextLectureID;
    }

    public void setNextLectureID(Object nextLectureID) {
        this.nextLectureID = nextLectureID;
    }

    public String getIsDrag() {
        return isDrag;
    }

    public void setIsDrag(String isDrag) {
        this.isDrag = isDrag;
    }

    public String getCaptionURL() {
        return captionURL;
    }

    public void setCaptionURL(String captionURL) {
        this.captionURL = captionURL;
    }

    public String getInitialSpeed() {
        return initialSpeed;
    }

    public void setInitialSpeed(String initialSpeed) {
        this.initialSpeed = initialSpeed;
    }

    public static class VideoBean {
        private SdBean sd;
        private CifBean cif;

        public SdBean getSd() {
            return sd;
        }

        public void setSd(SdBean sd) {
            this.sd = sd;
        }

        public CifBean getCif() {
            return cif;
        }

        public void setCif(CifBean cif) {
            this.cif = cif;
        }

        public static class SdBean {
            private List<String> three;
            private List<String> one;
            private List<String> two;

            public List<String> getThree() {
                return three;
            }

            public void setThree(List<String> three) {
                this.three = three;
            }

            public List<String> getOne() {
                return one;
            }

            public void setOne(List<String> one) {
                this.one = one;
            }

            public List<String> getTwo() {
                return two;
            }

            public void setTwo(List<String> two) {
                this.two = two;
            }
        }

        public static class CifBean {
            private List<String> three;
            private List<String> one;
            private List<String> two;

            public List<String> getThree() {
                return three;
            }

            public void setThree(List<String> three) {
                this.three = three;
            }

            public List<String> getOne() {
                return one;
            }

            public void setOne(List<String> one) {
                this.one = one;
            }

            public List<String> getTwo() {
                return two;
            }

            public void setTwo(List<String> two) {
                this.two = two;
            }
        }
    }
}
