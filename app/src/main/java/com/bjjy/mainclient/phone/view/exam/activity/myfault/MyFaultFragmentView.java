package com.bjjy.mainclient.phone.view.exam.activity.myfault;

import com.dongao.mainclient.model.mvp.MvpView;
import com.bjjy.mainclient.phone.widget.EmptyViewLayout;

/**
 * Created by wyc on 2016/5/18.
 */
public interface MyFaultFragmentView extends MvpView {
    void initAdapter();
    EmptyViewLayout getEmptyView();
    void setIsRefresh(boolean isRefresh);
}
