package com.bjjy.mainclient.phone.view.download;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bjjy.mainclient.phone.app.BaseFragmentActivity;
import com.bjjy.mainclient.phone.event.StatusEvent;
import com.bjjy.mainclient.phone.view.download.db.DownloadDB;
import com.bjjy.mainclient.phone.R;
import com.dongao.mainclient.model.local.SharedPrefHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;


public class DownloadActivity extends BaseFragmentActivity {

    @Bind(R.id.top_title_left)
    ImageView top_title_left;

    @Bind(R.id.top_title_right)
    ImageView top_title_right;

    @Bind(R.id.top_title_text)
    TextView top_title_text;

    @Bind(R.id.download_list)
    ListView mListView;

    private List<DownloadTask> mList ;
    private DownloadAdapter mAdapter;


    private DownloadDB downloadDB;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(com.bjjy.mainclient.phone.R.layout.download);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        initView();
        initData();

    }

    @Override
    public void initView() {
        // TODO Auto-generated method stub
        top_title_left.setImageResource(R.drawable.back);
        top_title_left.setOnClickListener(this);
        top_title_right.setOnClickListener(this);
        top_title_text.setText("下载列表");
        setlistener();
    }


    @Override
    public void initData() {

        downloadDB = new DownloadDB(this);
        mList = downloadDB.findAllByUserId(SharedPrefHelper.getInstance(this).getUserId());
        initListView();
    }



    private void initListView() {

        mAdapter = new DownloadAdapter(DownloadActivity.this);
        mAdapter.setList((ArrayList)mList);
        mListView.setAdapter(mAdapter);

    }



    private void setlistener(){
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case com.bjjy.mainclient.phone.R.id.top_title_left:
                onBackPressed();
                break;
            case com.bjjy.mainclient.phone.R.id.top_title_right:

                break;
            default:
                break;
        }
    }

    @Subscribe
    public void onEventAsync(StatusEvent event)
    {
       // mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        this.finish();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
        EventBus.getDefault().unregister(this);
    }
}
