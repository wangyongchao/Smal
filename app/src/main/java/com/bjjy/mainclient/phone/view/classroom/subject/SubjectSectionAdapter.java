package com.bjjy.mainclient.phone.view.classroom.subject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dongao.mainclient.model.bean.course.Exam;
import com.dongao.mainclient.model.bean.course.Subject;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.widget.SectionedBaseAdapter;

import java.util.ArrayList;

public class SubjectSectionAdapter extends SectionedBaseAdapter {

	private Context context;
	private ArrayList<Exam> mlist;

	public SubjectSectionAdapter(Context context) {
		super();
		this.context = context;
	}

	public void setList(ArrayList<Exam> mlist) {
		this.mlist = mlist;
	}


    @Override
    public Object getItem(int section, int position) {
        return mlist.get(section).getSubjectInfoList().get(position);
    }

    @Override
    public long getItemId(int section, int position) {
      //  return mlist.get(section).getMobileSubjectList().get(position).getId();
        return 0;
    }

    @Override
    public int getSectionCount() {
        return mlist.size();
    }

    @Override
    public int getCountForSection(int section) {
        return mlist.get(section).getSubjectInfoList().size();
    }

    @Override
    public View getItemView(int section, int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(com.bjjy.mainclient.phone.R.layout.course_item, null);
            viewHolder.titleTv = (TextView) convertView.findViewById(com.bjjy.mainclient.phone.R.id.title);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Subject subject = mlist.get(section).getSubjectInfoList().get(position);
        viewHolder.titleTv.setText(subject.getSubjectName()+"("+subject.getYear()+")");

        return convertView;
    }

    @Override
    public View getSectionHeaderView(int section, View convertView, ViewGroup parent) {
        final ViewSectionHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewSectionHolder();
            convertView = LayoutInflater.from(context).inflate(com.bjjy.mainclient.phone.R.layout.course_item, null);
            viewHolder.titleTv = (TextView) convertView.findViewById(com.bjjy.mainclient.phone.R.id.title);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewSectionHolder) convertView.getTag();
        }
        Exam exam = mlist.get(section);
        viewHolder.titleTv.setText(exam.getExamName());
        viewHolder.titleTv.setTextColor(context.getResources().getColor(R.color.color_primary));
        return convertView;
    }

    class ViewSectionHolder {
        TextView titleTv;
    }

    class ViewHolder {
		TextView titleTv;
	}
}
