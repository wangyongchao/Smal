package com.bjjy.mainclient.phone.view.play.downloadmanager.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.dongao.mainclient.model.bean.play.CourseWare;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.view.play.downloadmanager.AbsCoursListFragment;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.dongao.mainclient.model.common.Constants.STATE_DownLoaded;
import static com.dongao.mainclient.model.common.Constants.STATE_DownLoading;
import static com.dongao.mainclient.model.common.Constants.STATE_Error;
import static com.dongao.mainclient.model.common.Constants.STATE_Pause;
import static com.dongao.mainclient.model.common.Constants.STATE_Waiting;

/**
 * Created by yunfei on 2016/11/30.
 */

public class CourseWareAdapter extends BaseAdapter {

    private final LayoutInflater inflater;
    private final AbsCoursListFragment.OnItemSelectNumChangedListener onItemSelectNumChangedListener;
    private List<CourseWare> data = Collections.emptyList();
    private HashSet<CourseWare> courseWaresChecked;

    public CourseWareAdapter(Context context, AbsCoursListFragment.OnItemSelectNumChangedListener onItemSelectNumChangedListener, HashSet<CourseWare> courseWaresChecked) {
        inflater = LayoutInflater.from(context);
        this.courseWaresChecked = courseWaresChecked;
        this.onItemSelectNumChangedListener = onItemSelectNumChangedListener;
    }

    public void setData(List<CourseWare> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.pay_download_course_ware_item, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        final CourseWare item = data.get(position);
        viewHolder.tv_title.setText(item.getCwName());
        int state = item.getState();
        if (state == STATE_Pause
                || state == STATE_DownLoading
                || state == STATE_Error
                || state == STATE_Waiting) { //不可下载
            viewHolder.checkbox.setVisibility(View.INVISIBLE);
            viewHolder.iv_state.setVisibility(View.VISIBLE);
            viewHolder.iv_state.setImageResource(R.drawable.downloadmanager_loading);
            viewHolder.tv_title.setTextColor(viewHolder.tv_title.getResources().getColor(R.color.text_color_primary_hint));
        } else if (state == STATE_DownLoaded) {
            viewHolder.iv_state.setVisibility(View.VISIBLE);
            viewHolder.iv_state.setImageResource(R.drawable.downloadmanager_bendi);
        } else { //可下载
            viewHolder.iv_state.setVisibility(View.GONE);
            viewHolder.checkbox.setVisibility(View.INVISIBLE);
            viewHolder.tv_title.setTextColor(viewHolder.tv_title.getResources().getColor(R.color.text_color_primary_dark));
        }

        if (courseWaresChecked.contains(item)) {
            viewHolder.checkbox.setChecked(true);
        } else {
            viewHolder.checkbox.setChecked(false);
        }
        viewHolder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    courseWaresChecked.add(item);
                } else {
                    courseWaresChecked.remove(item);
                }
                onItemSelectNumChangedListener.onItemSelectNumChanged();
            }
        });
//        convertView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                viewHolder.checkbox.setChecked(!viewHolder.checkbox.isChecked());
//            }
//        });
        return convertView;
    }


    static class ViewHolder {
        @Bind(R.id.checkbox)
        CheckBox checkbox;
        @Bind(R.id.tv_title)
        TextView tv_title;
        @Bind(R.id.iv_state)
        ImageView iv_state;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
