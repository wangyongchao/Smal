package com.bjjy.mainclient.phone.view.setting;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.bjjy.mainclient.phone.event.PaySuccessEvent;
import com.bjjy.mainclient.phone.view.setting.bean.Action;
import com.bjjy.mainclient.phone.view.setting.bean.JsRegisterList;
import com.bjjy.mainclient.phone.view.studybar.Constant;
import com.bjjy.mainclient.phone.widget.EmptyViewLayout;
import com.dongao.mainclient.core.util.MD5Util;
import com.dongao.mainclient.core.util.StringUtil;
import com.dongao.mainclient.core.util.SystemUtils;
import com.dongao.mainclient.core.webview.WVJBWebViewClient;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseFragmentActivity;
import com.bjjy.mainclient.phone.utils.NetworkUtil;
import com.bjjy.mainclient.phone.view.main.UserBean;
import com.bjjy.mainclient.phone.view.user.LoginNewActivity;

import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

import org.greenrobot.eventbus.EventBus;


public class WebViewActivity extends BaseFragmentActivity {


    ArrayList<String> loadHistoryUrls = new ArrayList<String>();

    @Bind(R.id.top_title_left)
    ImageView imageView_back;

    @Bind(R.id.top_title_text)
    TextView textView_title;

    WebView webView;

    EmptyViewLayout mEmptyViewLayout;

    @Bind(R.id.progressBar)
    ProgressBar progressBar;
    private UserBean userBean;

    MyWebViewClient myWebViewClient;
    private String intentType = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        setContentView(R.layout.app_webview);
        ButterKnife.bind(this);
        initView();
        initData();

    }

    @Override
    public void initView() {
        userBean = new UserBean();
        userBean.setType("app");
        if (SharedPrefHelper.getInstance(appContext).isLogin()) {
            userBean.setUserId(SharedPrefHelper.getInstance(WebViewActivity.this).getUserId() + "");
            String userId = SharedPrefHelper.getInstance(WebViewActivity.this).getUserId() + Constants.USER_MD5_YAN;
            String signstr = MD5Util.encrypt(userId);
            userBean.setSignstr(signstr);
        } else {
            userBean.setUserId("");
        }
        // TODO Auto-generated method stub
        imageView_back.setImageResource(R.drawable.btn_close_nor);
        imageView_back.setVisibility(View.VISIBLE);
        imageView_back.setOnClickListener(this);
        webView = (WebView) findViewById(R.id.webview);


        mEmptyViewLayout = new EmptyViewLayout(this, webView);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebChromeClient(new WebChromeClients());
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setSaveFormData(false);
        webView.getSettings().setSupportZoom(false);
        // 修改ua使得web端正确判断
        String ua = webView.getSettings().getUserAgentString();
        webView.getSettings().setUserAgentString(ua + ";dongao/android/PayH5");
        myWebViewClient = new MyWebViewClient(webView);
        webView.setWebViewClient(myWebViewClient);

        webView.clearCache(true);
        webView.clearHistory();

    }

    class WebChromeClients extends WebChromeClient {

        @Override
        public void onReceivedTitle(WebView view, String title) {
//            if (textView_title != null)
//                textView_title.setText(title);
        }

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            if (progressBar != null) {
                if (newProgress < 100) {
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.setProgress(newProgress);
                }
                if (newProgress == 100) {
                    progressBar.setVisibility(View.GONE);
                }
            }

        }
    }

    List<String> handList = new ArrayList<>();

    class MyWebViewClient extends WVJBWebViewClient {
        public MyWebViewClient(WebView webView) {
            super(webView, new WVJBWebViewClient.WVJBHandler() {
                @Override
                public void request(Object data, WVJBResponseCallback callback) {
                    callback.callback("Response for message from Android!");
                }
            });

            send(getMDUserInfo(), new WVJBWebViewClient.WVJBResponseCallback() {
                @Override
                public void callback(Object data) {
                }
            });

            // not support js send
            registerHandler("returnCommonParams", new WVJBWebViewClient.WVJBHandler() {
                @Override
                public void request(Object data, WVJBResponseCallback callback) {
//                    if(textView_title.getText().equals("关于")){
//                        callback.callback(SystemUtils.getVersion(WebViewActivity.this));
//                    }else {
                        if (SharedPrefHelper.getInstance(appContext).isLogin()) {
                            userBean.setUserId(SharedPrefHelper.getInstance(WebViewActivity.this).getUserId() + "");
                            String userId = SharedPrefHelper.getInstance(WebViewActivity.this).getUserId() + Constants.USER_MD5_YAN;
                            String signstr = MD5Util.encrypt(userId);
                            userBean.setAppVersion(SystemUtils.getVersion(WebViewActivity.this));
                            userBean.setSignstr(signstr);
                        } else {
                            userBean.setAppVersion(SystemUtils.getVersion(WebViewActivity.this));
                            userBean.setUserId("");
                        }
                        String json = JSON.toJSONString(userBean);
                        if (userBean.getUserId().isEmpty()) {
                            json = "";
                        }
                        callback.callback(json);
//                    }
                }
            });

            registerHandler("remoteShowLogin", new WVJBHandler() {
                @Override
                public void request(Object data, WVJBResponseCallback callback) {
                    Intent intent = new Intent(WebViewActivity.this, LoginNewActivity.class);
                    startActivity(intent);
                    callback.callback("response payTableViewController");
                }
            });

            // not support js send
           try {
                callHandler("getJSRegisterInfoHandler",
                        new JSONObject(getUserInfo()),
                        new WVJBResponseCallback() {
                            @Override
                            public void callback(Object data) {
                                //Toast.makeText(WebViewActivity.this, "Android got response! :" + data, Toast.LENGTH_LONG).show();
                                JsRegisterList jsRegisters = JSON.parseObject(data.toString(), JsRegisterList.class);
                                for (final Action action : jsRegisters.getJsRegisterList()) {
                                    // not support js send
                                    if (handList.contains(action.getAction())) {
                                        return;
                                    }
                                    handList.add(action.getAction());
                                    registerHandler(action.getAction(), new WVJBWebViewClient.WVJBHandler() {
                                        @Override
                                        public void request(Object data, WVJBResponseCallback callback) {
                                            //Intent intent = new Intent(IntentAction.COURSE_LIST_ACTIVITY,url);
                                            if(textView_title.getText().equals("关于")){
                                                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "400-627-5566"));
                                                startActivity(intent);
                                            }else{
                                                Intent intent = new Intent();
                                                intentType = action.getTarget(); //标示跳转到那个activity
                                                intent.setClassName(SystemUtils.getPackageName(appContext), action.getPackageName());
                                                if (!StringUtil.isEmpty(data.toString())) { //这是传过来的参数值
                                                    try {
                                                        JSONObject jsonObject = new JSONObject(data.toString());
                                                        Iterator it = jsonObject.keys();
                                                        // 遍历jsonObject数据，添加到Map对象
                                                        while (it.hasNext()) {
                                                            String key = String.valueOf(it.next());
                                                            String value = jsonObject.optString(key);
                                                            intent.putExtra(key, value);
                                                        }
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                                startActivity(intent);
                                            }
                                            callback.callback(getUserInfo());
                                        }
                                    });

                                }
                            }
                        });
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return super.shouldOverrideUrlLoading(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            mEmptyViewLayout.showLoading();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            mEmptyViewLayout.showContentView();
        }
    }

    private String getUserInfo() {
        String json;
        String userId = SharedPrefHelper.getInstance(this).getUserId();
        userBean.setUserId(userId);
        if (!SharedPrefHelper.getInstance(this).isLogin()) {
            json = "{\"type\":\"app\",\"appVersion\":\"" + SystemUtils.getVersion(this) + "\"}";
        } else {
            json = "{\"userId\":\"" + userBean.getUserId() + "\",\"type\":\"app\" ,\"appVersion\":\"" + SystemUtils.getVersion(this) + "\"}";
        }
        return json;
    }

    private String getMDUserInfo() {
        if (SharedPrefHelper.getInstance(appContext).isLogin()) {
            userBean.setUserId(SharedPrefHelper.getInstance(WebViewActivity.this).getUserId() + "");
            userBean.setAppVersion(SystemUtils.getVersion(this));
            String userId = SharedPrefHelper.getInstance(WebViewActivity.this).getUserId() + Constants.USER_MD5_YAN;
            String signstr = MD5Util.encrypt(userId);
            userBean.setSignstr(signstr);
        } else {
            userBean.setAppVersion(SystemUtils.getVersion(this));
            userBean.setUserId("");
        }
        String json = JSON.toJSONString(userBean);
        if (userBean.getUserId().isEmpty()) {
            json = "{\"type\":\"app\",\"appVersion\":\"" + SystemUtils.getVersion(this) + "\" }";
        }
        return json;
    }


    @Override
    public void initData() {
        Intent intent = getIntent();
        String title = intent.getStringExtra(Constants.APP_WEBVIEW_TITLE);
        final String url = intent.getStringExtra(Constants.APP_WEBVIEW_URL);
        textView_title.setText(title);

        if (!NetworkUtil.isNetworkAvailable(WebViewActivity.this)) {
            mEmptyViewLayout.showNetErrorView();
        } else {
            webView.loadUrl(url.toString());
            loadHistoryUrls.add(url.toString());
//			webView.reload();
        }
        isload = true;
    }


    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.top_title_left:
                setResult(100000, getIntent());
                this.finish();
                super.onBackPressed();
                break;
            case R.id.top_title_right:
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        //判断是否可以返回操作
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            setResult(100000, getIntent());
            this.finish();
            super.onBackPressed();
        }
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        handList.clear();
        if (webView != null) {
            CookieSyncManager.createInstance(this);
            CookieSyncManager.getInstance().startSync();
            CookieManager.getInstance().removeAllCookie();
            webView.clearCache(true);
            webView.clearHistory();
           // webView.removeAllViews();
            webView.destroy();
        }
        ButterKnife.unbind(this);
        EventBus.getDefault().unregister(this);
    }

    /**
     * 支付成功后关闭界面
     *
     * @param event
     */
    @Subscribe
    public void onEventMainThread(PaySuccessEvent event) {
        String payWay = SharedPrefHelper.getInstance(this).getPayFromWay();
        if (payWay.equals(Constant.PAY_WAY_FROM_SHOUYE)) {
            this.finish();
            super.onBackPressed();
        }
    }

    boolean isload = false;
    @Override
    protected void onResume() {
        super.onResume();
        if(isload){
            myWebViewClient.send("", new WVJBWebViewClient.WVJBResponseCallback() {
                @Override
                public void callback(Object data) {
                   // Toast.makeText(WebViewActivity.this, "sendMessage got response: " + data, Toast.LENGTH_LONG).show();
                }
            });

            try {
                myWebViewClient.callHandler("getAppUserInfoTransToM",
                        new JSONObject(getMDUserInfo()),
                        new WVJBWebViewClient.WVJBResponseCallback() {
                            @Override
                            public void callback(Object data) {
//								Toast.makeText(WebViewActivity.this, "方法走通" + data, Toast.LENGTH_LONG).show();
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }

            if(intentType.equals("PaymentWeb")){
                webView.reload();
            }
        }
    }

}
