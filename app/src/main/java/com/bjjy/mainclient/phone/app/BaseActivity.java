package com.bjjy.mainclient.phone.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bjjy.mainclient.phone.view.studybar.Constant;
import com.bjjy.mainclient.phone.view.user.utils.UserViewManager;
import com.dongao.mainclient.core.app.AppManager;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.bjjy.mainclient.phone.R;
import com.umeng.analytics.MobclickAgent;
import com.umeng.message.PushAgent;

import me.drakeet.materialdialog.MaterialDialog;


public abstract class BaseActivity extends Activity implements View.OnClickListener {

	protected AppContext appContext;
	protected ProgressDialog progress;

	protected UserViewManager userViewManager;
	public PushAgent mpAgent;

	protected MaterialDialog materialDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		userViewManager = new UserViewManager(this, new UserViewManager.LoginLinstener() {
			@Override
			public void loginSuccess() {
				loginSuccessed();
			}
		});
		init();
		initProgress();
	}

	/**
	 * 登陆成功
	 */
	protected void loginSuccessed(){
		String userId=SharedPrefHelper.getInstance(this).getUserId()+"";
		if (userId.isEmpty()||userId.equals("0")){
			userId="123456";
		}
		mpAgent.setAlias(userId, Constant.UMENG_BIECHENG_TYPE);
	}


	protected void showLoginView() {
		String userId=SharedPrefHelper.getInstance(this).getUserId()+"";
		try {
			mpAgent.removeAlias(userId, Constant.UMENG_BIECHENG_TYPE);
		}catch (Exception e){
		}
		userViewManager.showUserCenterWindow();
	}

	private void showProgress(String title,String content){
	}

	private void initProgress(){
		progress=new ProgressDialog(this);
		progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progress.setMessage("正在加载…………");
	}

	/**
	 * 显示动画的加载中
	 * @param loadingtv
	 */
	protected void showAnimProgress(String loadingtv){
		materialDialog = new MaterialDialog(this);
		materialDialog.setCanceledOnTouchOutside(false);
		View view = LayoutInflater.from(this).inflate(R.layout.app_view_loading,null);
		TextView tv = (TextView) view.findViewById(R.id.app_loading_tv);
		ImageView imageView = (ImageView)view.findViewById(R.id.empty_layout_loading_img);
		AnimationDrawable animationDrawable = (AnimationDrawable)imageView.getBackground();
		animationDrawable.start();
		tv.setText(loadingtv);
		materialDialog.setContentView(view);
		materialDialog.show();
	}

	protected void dismissAnimProgress(){
		if(materialDialog!=null)
			materialDialog.dismiss();
	}

	// 初始化
	private void init() {
		// 添加Activity到堆栈
		AppManager.getAppManager().addActivity(this);
		appContext = (AppContext) AppContext.getApp().getApplicationContext();
        //注册消息组件
      //  EventBus.getDefault().register(this);
		//初始化推送
		mpAgent= PushAgent.getInstance(this);
		mpAgent.onAppStart();
	}

	public abstract void initView();

	public abstract void initData();

	//
	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onStop() {
		super.onStop();
	}

	@Override
	public void onDestroy() {
		// 结束Activity&从堆栈中移除
		AppManager.getAppManager().finishActivity(this);
        //EventBus.getDefault().unregister(this);
		super.onDestroy();
	}
	


}
