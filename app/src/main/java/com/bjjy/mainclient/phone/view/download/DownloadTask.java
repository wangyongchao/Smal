package com.bjjy.mainclient.phone.view.download;

import com.dongao.mainclient.model.bean.course.CourseWare;
import com.yunqing.core.db.annotations.Id;

import java.util.ArrayList;
import java.util.List;

public class DownloadTask {

    public final static int BUFFING = 1; //解析中
    public final static int WAITING = 2; //等待中
	public final static int DOWNLOADING = 3; //下载中
	public final static int PAUSE = 4; //暂停
	public final static int ERROR = 5; //网络错误
	public final static int ERROR_404 = 6; //服务器错误 比如404
	public final static int FINISHED = 7; //下载完成
	public final static int RESOLVE_FINISHED = 8; //解析m3u8完成 当再次下载此任务时候将不再重复解析
    public final static int BUFFING_ERROR = 9; //解析失败

    @Id
	private int id;

    private int taskId;
	
	private int status;

    private int cwId;

    private int userId;
	
	private CourseWare cw;
	
	private int percent;

    private String videoUrl;

    private String updateDate;

    private String downloadSpeed;

    //下载任务列表
    private List<String> fileList;
    private List<String> fileFinishedList;
    private List<String> fileFailList;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public DownloadTask(){
		super();
		fileList = new ArrayList<String>();
        fileFinishedList = new ArrayList<String>();
        fileFailList = new ArrayList<String>();
	}
	
	public List<String> getFileList() {
		return fileList;
	}

	public void setFileList(List<String> fileList) {
		this.fileList = fileList;
	}


    public CourseWare getCw() {
        return cw;
    }

    public void setCw(CourseWare cw) {
        this.cw = cw;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<String> getFileFinishedList() {
        return fileFinishedList;
    }

    public void setFileFinishedList(List<String> fileFinishedList) {
        this.fileFinishedList = fileFinishedList;
    }

    public List<String> getFileFailList() {
        return fileFailList;
    }

    public void setFileFailList(List<String> fileFailList) {
        this.fileFailList = fileFailList;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public String getDownloadSpeed() {
        return downloadSpeed;
    }

    public void setDownloadSpeed(String downloadSpeed) {
        this.downloadSpeed = downloadSpeed;
    }

    @Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		
		if(o instanceof DownloadTask) {
			DownloadTask task = (DownloadTask) o;
			if(this.getId() == task.getId()) {
				return true;
			}else {
				return false;
			}
		}
		
		return super.equals(o);
	}

	public int getPercent() {
		return percent;
	}

	public void setPercent(int percent) {
		this.percent = percent;
	}

    public int getCwId() {
        return cwId;
    }

    public void setCwId(int cwId) {
        this.cwId = cwId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    @Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

    public static String getStatusInfo(DownloadTask ts){
        String statusInfo = "未下载";
        if(ts == null){
            return statusInfo;
        }
        switch (ts.getStatus()){
            case BUFFING:
                statusInfo = "解析中";
                break;
            case WAITING:
                statusInfo = "等待中";
                break;
            case DOWNLOADING://已经下载了多少，有必要显示出来吧

                statusInfo = "下载中("+ ts.getPercent() +"%)";
                break;
            case PAUSE:
                statusInfo = "暂停";
                break;
            case ERROR:
                statusInfo = "错误";
                break;
            case FINISHED:
                statusInfo = "已下载";
                break;
            case BUFFING_ERROR:
                statusInfo = "解析失败";
                break;
        }

        return statusInfo;
    }

	
}
