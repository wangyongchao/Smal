package com.bjjy.mainclient.phone.view.course;

import com.bjjy.mainclient.phone.view.download.local.bean.YearInfo;
import com.dongao.mainclient.model.mvp.MvpView;

/**
 * Created by dell on 2017/10/30.
 */

public interface CourseFragmentView extends MvpView{
	void showContentView(int type);
	void initAdapter();
	void initYearAdapter();
	void showCurrentYear(YearInfo currYear);
	void hideYearPop(boolean isHide);
	void showYearOrNot(boolean show);
}
