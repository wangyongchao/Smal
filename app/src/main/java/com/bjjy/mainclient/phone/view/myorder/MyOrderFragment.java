package com.bjjy.mainclient.phone.view.myorder;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bjjy.mainclient.persenter.MyOrderPersenter;
import com.bjjy.mainclient.phone.app.BaseFragment;
import com.bjjy.mainclient.phone.view.myorder.adpter.MyOrderFragmentAdapter;
import com.bjjy.mainclient.phone.widget.xlistview.XListView;
import com.dongao.mainclient.core.util.NetUtils;
import com.dongao.mainclient.model.bean.myorder.MyOrderItem;
import com.bjjy.mainclient.phone.R;

import java.util.List;



/**
 * Created by fengzongwei on 2016/4/6.
 */
public class MyOrderFragment extends BaseFragment implements XListView.IXListViewListener,MyOrderView{
    private String type;
    private XListView xListView;
    private MyOrderPersenter myOrderPersenter;
    private MyOrderFragmentAdapter myOrderFragmentAdapter;
    private RelativeLayout relativeLayout_hint;
    private LinearLayout linearLayout_hint;
    private TextView textView_hint;
    private List<MyOrderItem> orderProductList;
    public MyOrderFragment(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myOrderPersenter = new MyOrderPersenter();
        myOrderPersenter.attachView(this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        RelativeLayout relativeLayout = new RelativeLayout(getActivity());
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
        relativeLayout.setLayoutParams(layoutParams);
        relativeLayout_hint = (RelativeLayout)inflater.inflate(R.layout.error_empty, container, false);
        linearLayout_hint = (LinearLayout)relativeLayout_hint.findViewById(R.id.hint_click_body);
        linearLayout_hint.setOnClickListener(this);
        textView_hint = (TextView)relativeLayout_hint.findViewById(R.id.hint_tv);
        relativeLayout_hint.setOnClickListener(this);
        type = getArguments().getString("type");
        xListView = new XListView(getActivity());
        xListView.setPullLoadEnable(false);
        xListView.setDividerHeight(2);
//        xListView.setDivider(new ColorDrawable(Color.parseColor("#DBDBDB")));
//        xListView.setDividerHeight(1);
        xListView.setBackgroundColor(Color.parseColor("#DBDBDB"));
        AbsListView.LayoutParams params = new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT,
                AbsListView.LayoutParams.MATCH_PARENT);
        xListView.setLayoutParams(params);
        xListView.setXListViewListener(this);
        xListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), MyOrderDetailActivity.class);
                intent.putExtra("isPay", type.equals("all"));
                intent.putExtra("orderId", orderProductList.get(position - 1).getId());//0为下拉刷新的header，所以要减1
                startActivity(intent);
            }
        });

        xListView.setVisibility(View.GONE);
        xListView.setHeaderTimeViewGone();
        relativeLayout_hint.setVisibility(View.GONE);
        relativeLayout.addView(xListView);
        relativeLayout.addView(relativeLayout_hint);
        initData();
        return relativeLayout;
    }

    @Override
    public void initView() {

    }
    @Override
    public void initData() {
        myOrderPersenter.checkToken();
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.hint_click_body:
                freshData();
                break;
        }
    }

    @Override
    public void onRefresh() {
        freshData();
    }
    @Override
    public void onLoadMore() {

    }

    @Override
    public void freshData() {
        if(NetUtils.checkNet(getActivity()).available){
            relativeLayout_hint.setVisibility(View.GONE);
            myOrderPersenter.getData();
        }else{
            relativeLayout_hint.setVisibility(View.VISIBLE);
            textView_hint.setText("当前无网络连接");
        }
    }

    @Override
    public String type() {
        return type;
    }

    @Override
    public void showLoading() {
        progress.show();
    }

    @Override
    public void hideLoading() {
        progress.dismiss();
    }

    @Override
    public void loadSuccess(List<MyOrderItem> orderItemList) {
        if(orderItemList!=null && orderItemList.size()>0){
            xListView.setVisibility(View.VISIBLE);
            relativeLayout_hint.setVisibility(View.GONE);
            orderProductList = orderItemList;
            xListView.stopRefresh();
            myOrderFragmentAdapter = new MyOrderFragmentAdapter(getActivity(),orderItemList,type.equals("all"));
            xListView.setAdapter(myOrderFragmentAdapter);
        }else {
            relativeLayout_hint.setVisibility(View.VISIBLE);
            xListView.setVisibility(View.GONE);
            textView_hint.setText("暂无数据");
        }
//        if(orderItemList.size()==0){
//            Toast.makeText(getActivity(),"暂无数据",Toast.LENGTH_SHORT).show();
//        }else{
//            myOrderFragmentAdapter = new MyOrderFragmentAdapter(getActivity(),orderItemList);
//            xListView.setAdapter(myOrderFragmentAdapter);
//        }
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {
        relativeLayout_hint.setVisibility(View.VISIBLE);
        textView_hint.setText(message);
    }

    @Override
    public Context context() {
        return getActivity();
    }

    @Override
    public void deviceTokenSuccess() {
        myOrderPersenter.getData();
    }

    @Override
    public void deviceTokenFailed() {
        ((MyOrderActivity)getActivity()).showLoginDialog();
    }

}
