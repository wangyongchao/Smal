package com.bjjy.mainclient.phone.view.user;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.bjjy.mainclient.persenter.UserPersenter;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseActivity;
import com.bjjy.mainclient.phone.event.CancelLoginEvent;
import com.bjjy.mainclient.phone.view.setting.WebViewActivity;
import com.bjjy.mainclient.phone.view.setting.myfavs.MyFavouritesActivity;
import com.bjjy.mainclient.phone.view.studybar.message.MyMessageActivity;
import com.bjjy.mainclient.phone.view.studybar.push.PushUtils;
import com.bjjy.mainclient.phone.view.studybar.push.bean.Push;
import com.bjjy.mainclient.phone.view.user.utils.RotateAnimationUtil;
import com.dongao.mainclient.core.util.NetUtils;
import com.dongao.mainclient.core.util.StringUtil;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.mvp.ApiModel;
import com.dongao.mainclient.model.mvp.ResultListener;
import com.dongao.mainclient.model.remote.ApiService;

import org.greenrobot.eventbus.EventBus;

import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by dell on 2016/4/5.
 */
public class RegisteActivity extends BaseActivity implements UserView{
    private LinearLayout linearLayout_login;
    private LinearLayout linearLayout_registe;
    private RelativeLayout container;
    private  RelativeLayout contaier_parent;
    private TextView textView_title;
    private ImageView login_new_back;
    //登录相关view
    private EditText usernameEdit;
    private  EditText passwordEdit;
    private Button login_bt;
    private ImageView login_imageView_claer,login_imageview_see;
    private LinearLayout linearLayout_login_error;
    private TextView login_error_hint,login_forget_psw;
    private ImageView login_imageView_clear_name;

    //注册相关view
    private  EditText editText_phone;
    private  EditText editText_checknumber;
    private  EditText editText_psw;
    private  TextView bt_checkNumber;
    private  Button button_registe;
    private ImageView imageView_seePsw,registe_imageView_checkNum_clear,registe_imageView_psw_clear;
    private LinearLayout registe_error_body;
    private TextView registe_error_hint;
    private ImageView registe_imageView_clear_name;

    private UserPersenter userPersenter;

    private RotateAnimationUtil rotateAnimationUtil;
    private int screenWidth;

    private final String PHONE = "^1[3|4|5|8|7][0-9]\\d{8}$";

    private boolean isShowPswNow = false;//当前密码是否可见
    private boolean isLoginView = true;//当前是否登录布局在显示
    private boolean isShowLoginPsw = false;//当前是否正在显示登录的密码
    private String resend = "倒计时";

    private boolean isAnimEnd = true;//当前翻转翻页动画是否结束

    private MaterialDialog mMaterialDialog;

    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if(msg.what == 0){
                bt_checkNumber.setEnabled(true);
                bt_checkNumber.setText("重新获取");
                bt_checkNumber.setBackgroundDrawable(getResources().getDrawable(R.drawable.registe_check_bt));
                bt_checkNumber.setTextColor(Color.WHITE);
                bt_checkNumber.setEnabled(true);
                return;
            }
            bt_checkNumber.setText(resend+"("+msg.what+"s"+")");
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_new);
        userPersenter = new UserPersenter();
        userPersenter.attachView(this);
        initView();
        initData();
    }

    @Override
    public void initView() {
        login_forget_psw = (TextView)findViewById(R.id.login_forget_psw);
        login_imageView_clear_name = (ImageView)findViewById(R.id.login_new_clear_name);
        registe_imageView_clear_name = (ImageView)findViewById(R.id.registe_new_clear_name);
        login_imageView_claer = (ImageView)findViewById(R.id.login_clear_psw);
        login_imageview_see = (ImageView)findViewById(R.id.login_see_psw);
        login_new_back = (ImageView)findViewById(R.id.login_new_back);
        login_new_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        linearLayout_login = (LinearLayout) findViewById(R.id.login_container);
        linearLayout_registe = (LinearLayout)findViewById(R.id.registe_container);
        container = (RelativeLayout)findViewById(R.id.container);
//        contaier_parent = (RelativeLayout)findViewById(R.id.container_parent);
        textView_title = (TextView)findViewById(R.id.top_title_text);
        usernameEdit = (EditText)findViewById(R.id.login_username_et);
//        usernameEdit.addTextChangedListener(loginName);
        passwordEdit = (EditText)findViewById(R.id.login_psw_et);
        login_bt = (Button)findViewById(R.id.login_bt);
        editText_phone = (EditText)findViewById(R.id.registe_phone_et);
        editText_checknumber = (EditText)findViewById(R.id.registe_phone_checknumber_et);
        editText_checknumber.addTextChangedListener(valiCode);
        editText_psw = (EditText)findViewById(R.id.registe_psw_et);
        editText_psw.addTextChangedListener(registeWatcher);
        bt_checkNumber = (TextView)findViewById(R.id.registe_get_checcknumber_bt);
        button_registe = (Button)findViewById(R.id.registe_bt);
//        login_imageView_claer = (ImageView)findViewById(R.id.login_clear);
        registe_imageView_checkNum_clear = (ImageView)findViewById(R.id.registe_clear);
        registe_imageView_psw_clear = (ImageView)findViewById(R.id.registe_psw_clear_img);
        imageView_seePsw = (ImageView)findViewById(R.id.registe_psw_seePsw_img);
//        textView_login_pass = (TextView)findViewById(R.id.login_pass);
//        textView_registe_pass = (TextView)findViewById(R.id.registe_pass);
        linearLayout_login_error = (LinearLayout)findViewById(R.id.login_error_body);
        registe_error_body = (LinearLayout)findViewById(R.id.registe_error_body);
        login_error_hint = (TextView)findViewById(R.id.login_error_hint);
        registe_error_hint = (TextView)findViewById(R.id.registe_error_hint);

        editText_phone.addTextChangedListener(registeName);
        passwordEdit.addTextChangedListener(loginPsw);
        usernameEdit.addTextChangedListener(loginName);
//        login_new_back.setOnClickListener(this);
        registe_imageView_psw_clear.setOnClickListener(this);
//        contaier_parent.setOnClickListener(this);
//        textView_registe_pass.setOnClickListener(this);
//        textView_login_pass.setOnClickListener(this);
//        login_imageView_claer.setOnClickListener(this);
        registe_imageView_checkNum_clear.setOnClickListener(this);
        imageView_seePsw.setOnClickListener(this);
        login_imageView_claer.setOnClickListener(this);
        login_imageview_see.setOnClickListener(this);
        login_imageView_clear_name.setOnClickListener(this);
        registe_imageView_clear_name.setOnClickListener(this);
        login_forget_psw.setOnClickListener(this);
        findViewById(R.id.login_registe).setOnClickListener(this);
        findViewById(R.id.registe_login).setOnClickListener(this);
        findViewById(R.id.registe_ruel_tv).setOnClickListener(this);
        findViewById(R.id.registe_get_checcknumber_bt).setOnClickListener(this);
        findViewById(R.id.login_bt).setOnClickListener(this);
        findViewById(R.id.registe_bt).setOnClickListener(this);
        rotateAnimationUtil = new RotateAnimationUtil(container, linearLayout_login,
                linearLayout_registe);
        rotateAnimationUtil.setOnFinishListener(new RotateAnimationUtil.AllAnimEnd() {
            @Override
            public void onFinish() {
                isAnimEnd = true;
            }
        });
        if(!TextUtils.isEmpty(SharedPrefHelper.getInstance(this).getLoginUsername())){
            usernameEdit.setText(SharedPrefHelper.getInstance(this).getLoginUsername());
            usernameEdit.setSelection(SharedPrefHelper.getInstance(this).getLoginUsername().length());
        }

        linearLayout_registe.setVisibility(View.VISIBLE);
        linearLayout_login.setVisibility(View.GONE);
    }

    @Override
    public void initData() {
        String username = getIntent().getStringExtra("username");
        String psw = getIntent().getStringExtra("psw");
        if(username!=null && !username.equals("")
                && psw!=null && !psw.equals("")){
            usernameEdit.setText(username);
            usernameEdit.setSelection(username.length());
            passwordEdit.setText(psw);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_forget_psw:
                Intent intent_forget = new Intent(RegisteActivity.this,ForgetPswActivity.class);
                startActivity(intent_forget);
                break;
            case R.id.login_new_clear_name:
                usernameEdit.setText("");
                login_imageView_clear_name.setVisibility(View.INVISIBLE);
                break;
            case R.id.registe_new_clear_name:
                editText_phone.setText("");
                registe_imageView_clear_name.setVisibility(View.INVISIBLE);
                break;
            case R.id.login_see_psw:
                if (!isShowLoginPsw) {
                    isShowLoginPsw = true;
                    login_imageview_see.setImageResource(R.drawable.psw_see);
                    passwordEdit.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    passwordEdit.setSelection(passwordEdit.getText().toString().length());
                } else {
                    isShowLoginPsw = false;
                    login_imageview_see.setImageResource(R.drawable.login_clear_psw_nor);
                    passwordEdit.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    passwordEdit.setSelection(passwordEdit.getText().toString().length());
                }
                break;
            case R.id.login_clear_psw:
                passwordEdit.setText("");
                break;
            case R.id.login_new_back:
                onBackPressed();
                break;
            case R.id.login_registe:
//                if(isAnimEnd){
//                    isLoginView = false;
//                    isAnimEnd = false;
//                    rotateAnimationUtil.applyRotateAnimation(1, 0, 90);
//                }
//                Animation animation = AnimationUtils.loadAnimation(this,R.anim.user_login_anim);
//                animation.setInterpolator(new DecelerateInterpolator());
//                linearLayout_login.startAnimation(animation);
                break;
            case R.id.registe_login:
//                if(isAnimEnd){
//                    isAnimEnd = false;
//                    isLoginView = true;
//                    rotateAnimationUtil.applyRotateAnimation(-1, 0, -90);
//                }
                onBackPressed();
                break;
            case R.id.registe_ruel_tv:
                Intent intent = new Intent(this, WebViewActivity.class);
                intent.putExtra(Constants.APP_WEBVIEW_TITLE, "《用户注册协议》");
                intent.putExtra(Constants.APP_WEBVIEW_URL, "http://member.bjsteach.com/study/static/html/agreement.html");
                startActivity(intent);
                break;
            case R.id.registe_get_checcknumber_bt:
                if (editText_phone.getText().toString() == null || !editText_phone.getText().toString().matches(PHONE)) {
                    showError("请输入正确的手机号");
                    return;
                }
                if (bt_checkNumber.getText().toString().contains("重新获取") || bt_checkNumber.getText().toString().equals("获取验证码")) {
                    userPersenter.getMobileValid();
                }
                break;
            case R.id.login_bt:
                userPersenter.getData();
                break;
            case R.id.registe_psw_clear_img:
                registe_imageView_psw_clear.setVisibility(View.INVISIBLE);
                imageView_seePsw.setVisibility(View.INVISIBLE);
                editText_psw.setText("");
                break;
            case R.id.registe_bt:
                userPersenter.regitse();
                break;
            case R.id.login_clear:
                usernameEdit.setText("");
                break;
            case R.id.registe_clear:
                editText_checknumber.setText("");
                break;
            case R.id.registe_psw_seePsw_img:
                if (!isShowPswNow) {
                    isShowPswNow = true;
                    imageView_seePsw.setImageResource(R.drawable.psw_see);
                    editText_psw.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    editText_psw.setSelection(editText_psw.getText().toString().length());
                } else {
                    isShowPswNow = false;
                    imageView_seePsw.setImageResource(R.drawable.login_clear_psw_nor);
                    editText_psw.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    editText_psw.setSelection(editText_psw.getText().toString().length());
                }
                break;
            case R.id.login_pass:
                break;
            case R.id.registe_pass:
                break;
            case R.id.container_parent:
//                dissmissPopupWindow();
                break;
        }
    }

    @Override
    public String username() {
        return usernameEdit.getText().toString();
    }

    @Override
    public String password() {
        return passwordEdit.getText().toString();
    }

    @Override
    public void loginSuccess() {
        setAliasForUM();
        if(getIntent().getStringExtra(Constants.LOGIN_TYPE_KEY)!=null)
            switch (getIntent().getStringExtra(Constants.LOGIN_TYPE_KEY)){
                case Constants.LOGIN_TYPE_PERSENAL_COLLECTION:
                    Intent myfavs=new Intent(this,MyFavouritesActivity.class);
                    startActivity(myfavs);
                    break;
                case Constants.LOGIN_TYPE_PERSENAL_DOWNLOAD:
//                Intent cache=new Intent(this,StudyBarActivity.class);
                    Intent cache=new Intent(this,MyMessageActivity.class);
                    startActivity(cache);
                    break;
                case Constants.LOGIN_TYPE_PERSENAL_DINGDAN:
                    Intent intent_order = new Intent(this, WebViewActivity.class);
                    intent_order.putExtra(Constants.APP_WEBVIEW_TITLE,"我的订单");
                    intent_order.putExtra(Constants.APP_WEBVIEW_URL, ApiService.ORDER_URL);//我的订单页面
                    startActivity(intent_order);
                    break;
            }
        pushJudgeIntent();
        finish();
    }

    private void pushJudgeIntent() {
        Intent intent=getIntent();
        String data=intent.getStringExtra("pushMessage");
        if (data!=null&&!data.isEmpty()){
            Push pushMessage= JSON.parseObject(data,Push.class);
            PushUtils.openActivity(RegisteActivity.this,pushMessage);
        }
    }

    public void removeAliasForUM(){
        String userId= SharedPrefHelper.getInstance(this).getUserId()+"";
        if (!userId.isEmpty()){
            try {
                // mpAgent.removeAlias(userId, Constant.UMENG_BIECHENG_TYPE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void setAliasForUM(){
        String device_token=SharedPrefHelper.getInstance(RegisteActivity.this).getUmDevicetoken();
        if (device_token!=null&&!device_token.isEmpty()){
            if (SharedPrefHelper.getInstance(RegisteActivity.this).isLogin()){
                if (NetUtils.checkNet(RegisteActivity.this).isAvailable()) {
                    ApiModel apiModel = new ApiModel(new ResultListener() {
                        @Override
                        public void onSuccess(String json) {

                        }

                        @Override
                        public void onError(Exception e) {

                        }
                    });
                    //  apiModel.getData(ApiClient.getClient().CommitDevicetoken(ParamsUtils.getInstance(RegisteActivity.this).commitDeviceToken(device_token,1)));

                }
            }
        }
    }


    @Override
    public String phoneNumber() {
        return editText_phone.getText().toString();
    }

    @Override
    public String checkNumber() {
        return editText_checknumber.getText().toString();
    }

    @Override
    public String pswRegiste() {
        return editText_psw.getText().toString();
    }

    @Override
    public void registeSuccess() {
//        rotateAnimationUtil.applyRotateAnimation(-1, 0, -90);
        setAliasForUM();
        finish();
    }

    @Override
    public void switchBtStatus() {
        bt_checkNumber.setText( 60 + "s");
        bt_checkNumber.setTextColor(Color.parseColor("#999999"));
        bt_checkNumber.setEnabled(false);
        new Thread() {
            @Override
            public void run() {
                try {
                    int i = 59;
                    while (i >= 0) {
                        handler.sendEmptyMessage(i);
                        i--;
                        Thread.sleep(1000);
                    }
                } catch (InterruptedException e) {
                }
            }
        }.start();
    }

    @Override
    public void showLoading() {
        showProgressDialog("正在进行网络请求");
        linearLayout_login_error.setVisibility(View.INVISIBLE);
        registe_error_body.setVisibility(View.INVISIBLE);
        setUnable();
    }

    @Override
    public void hideLoading() {
        dismissProgressDialog();
        setAble();
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {
        hideLoading();
//        if(isLoginView){
//            linearLayout_login_error.setVisibility(View.VISIBLE);
//            registe_error_body.setVisibility(View.INVISIBLE);
//            login_error_hint.setText(message);
//        }else{
            linearLayout_login_error.setVisibility(View.INVISIBLE);
            registe_error_body.setVisibility(View.VISIBLE);
            registe_error_hint.setText(message);
//        }
    }

    @Override
    public Context context() {
        return this;
    }

    private void setUnable(){
        imageView_seePsw.setEnabled(false);
        login_imageView_claer.setEnabled(false);
        registe_imageView_checkNum_clear.setEnabled(false);
        login_bt.setEnabled(false);
        button_registe.setEnabled(false);
        editText_checknumber.setEnabled(false);
        editText_phone.setEnabled(false);
        editText_psw.setEnabled(false);
        passwordEdit.setEnabled(false);
        usernameEdit.setEnabled(false);
    }

    private void setAble(){
        imageView_seePsw.setEnabled(true);
        login_imageView_claer.setEnabled(true);
        registe_imageView_checkNum_clear.setEnabled(true);
        login_bt.setEnabled(true);
        button_registe.setEnabled(true);
        editText_checknumber.setEnabled(true);
        editText_phone.setEnabled(true);
        editText_psw.setEnabled(true);
        passwordEdit.setEnabled(true);
        usernameEdit.setEnabled(true);
    }

    private TextWatcher loginName = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (usernameEdit.getText().toString() != null && !usernameEdit.getText().toString().equals("")) {
                login_imageView_clear_name.setVisibility(View.VISIBLE);
            } else {
                login_imageView_clear_name.setVisibility(View.INVISIBLE);
            }
        }
    };

    private TextWatcher registeName = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (editText_phone.getText().toString() != null && !editText_phone.getText().toString().equals("")) {
                registe_imageView_clear_name.setVisibility(View.VISIBLE);
            } else {
                registe_imageView_clear_name.setVisibility(View.INVISIBLE);
            }
        }
    };

    private TextWatcher loginPsw = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (passwordEdit.getText().toString() != null && !passwordEdit.getText().toString().equals("")) {
                login_imageView_claer.setVisibility(View.VISIBLE);
                login_imageview_see.setVisibility(View.VISIBLE);
            } else {
                login_imageView_claer.setVisibility(View.INVISIBLE);
                login_imageview_see.setVisibility(View.INVISIBLE);
            }
        }
    };

    private TextWatcher valiCode = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (editText_checknumber.getText().toString() != null && !editText_checknumber.getText().toString().equals("")) {
                registe_imageView_checkNum_clear.setVisibility(View.VISIBLE);
            } else {
                registe_imageView_checkNum_clear.setVisibility(View.INVISIBLE);
            }
        }
    };

    private TextWatcher registeWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (editText_psw.getText().toString() != null && !editText_psw.getText().toString().equals("")) {
                imageView_seePsw.setVisibility(View.VISIBLE);
                registe_imageView_psw_clear.setVisibility(View.VISIBLE);
            } else {
                imageView_seePsw.setVisibility(View.INVISIBLE);
                registe_imageView_psw_clear.setVisibility(View.INVISIBLE);
            }
        }
    };


    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        // Intent intent = new Intent(RegisteActivity.this, MainActivity.class);
        //startActivity(intent);
        // SharedPrefHelper.getInstance(RegisteActivity.this).setIsOtherLogin(false);
        String login_type = getIntent().getStringExtra(Constants.LOGIN_TYPE_KEY);
        if(!StringUtil.isEmpty(login_type)){
            if(!login_type.equals(Constants.LOGIN_TYPE_MY)){
                EventBus.getDefault().post(new CancelLoginEvent());
            }
        }else{
            EventBus.getDefault().post(new CancelLoginEvent());
        }
        this.finish();
    }

    /**
     * 显示进度条
     */
    public void showProgressDialog(String loadingtv){
        if(mMaterialDialog==null){
            mMaterialDialog = new MaterialDialog(this);
            mMaterialDialog.setCanceledOnTouchOutside(false);
            View view = LayoutInflater.from(this).inflate(R.layout.app_view_loading,null);
            TextView tv = (TextView) view.findViewById(R.id.app_loading_tv);
            ImageView imageView = (ImageView)view.findViewById(R.id.empty_layout_loading_img);
            AnimationDrawable animationDrawable = (AnimationDrawable)imageView.getBackground();
            animationDrawable.start();
            tv.setText(loadingtv);
            mMaterialDialog.setContentView(view);
        }
        mMaterialDialog.show();
    }

    public void dismissProgressDialog(){
        if(mMaterialDialog!=null)
            mMaterialDialog.dismiss();
    }
    
    

}
