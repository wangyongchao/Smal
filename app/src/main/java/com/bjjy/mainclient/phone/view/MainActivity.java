package com.bjjy.mainclient.phone.view;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseActivity;
import com.bjjy.mainclient.phone.intent.CommonUtils;
import com.bjjy.mainclient.phone.intent.Constants;
import com.umeng.message.IUmengRegisterCallback;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements View.OnClickListener{

    @Bind(R.id.button_1)
    Button button_1;
    @Bind(R.id.button_2)
    Button button_2;
    @Bind(R.id.button_3)
    Button button_3;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_test);
        ButterKnife.bind(this);
        initView();
    }

    public void initView() {


        button_1.setOnClickListener(this);
        button_2.setOnClickListener(this);
        button_3.setOnClickListener(this);

        TextView tv = (TextView) findViewById(R.id.main_test_tv);

        Intent intent = getIntent();
        String scheme = intent.getScheme();
        Uri uri = intent.getData();
        System.out.println("scheme:" + scheme);
        if (uri != null) {
            String host = uri.getHost();
            String dataString = intent.getDataString();
            String id = uri.getQueryParameter("d");
            String path = uri.getPath();
            String path1 = uri.getEncodedPath();
            String queryString = uri.getQuery();
            System.out.println("host:"+host);
            System.out.println("dataString:"+dataString);
            System.out.println("id:"+id);
            System.out.println("path:"+path);
            System.out.println("path1:"+path1);
            System.out.println("queryString:"+queryString);

            tv.setText("host:"+host+"=dataString:"+dataString+"=id:"+id+
                    "=path:"+path+"=path1:"+path1+"=queryString:"+queryString);
        }

    }

    @Override
    public void initData() {

    }

    private void youmeng(){
        mpAgent.enable();
        mpAgent.enable(new MyCallBack());
        mpAgent.getRegistrationId();
        loginSuccessed();
    }

    /**重写友盟的callback方法*/
    private class MyCallBack implements IUmengRegisterCallback {

        @Override
        public void onRegistered(String arg0) {
            // TODO Auto-generated method stub

        }

    }

    /**
     * Button1 是直接打开Activity，没有参数{
     * 跳转的规则如下：
     * "intent:#Intent;component=com.dongao.mainclient.phone/.view.TestActivity;end"
     * }
     * Button2 是直接打开接收参数的Activity，有参数
     * {
     * 跳转的规则如下：
     * intent:#Intent;component=com.dongao.mainclient.phone/.view.TestActivity;B.flag=true;S.name=大家好Plus-28天;i.code=132412;end
     * }
     * Button3 是直接打开WebViewActivity，没有参数
     * {
     * 跳转的规则如下：
     * "http://www.bjsteach.com"
     * }
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_1:
                CommonUtils.jumpWithUri(MainActivity.this, Constants.intent_rule_1);
                break;
            case R.id.button_2:
                CommonUtils.jumpWithUri(MainActivity.this,Constants.intent_rule_2);
                break;
            case R.id.button_3:
                CommonUtils.jumpWithUri(MainActivity.this,Constants.intent_rule_3);
                break;
            default:
                break;
        }
    }
}
