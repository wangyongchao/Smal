package com.bjjy.mainclient.phone.view.play.downloadmanager;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bjjy.mainclient.phone.app.BaseFragment;
import com.bjjy.mainclient.phone.widget.EmptyViewLayout;
import com.bjjy.mainclient.phone.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by yunfei on 2016/11/29.
 * 子类只负责 list 刷新
 */

public abstract class AbsCoursListFragment extends BaseFragment implements DownLoadManagerView {

    public interface OnItemSelectNumChangedListener {
        void onItemSelectNumChanged();
    }

    @Bind(R.id.tv_select)
    TextView tv_select;
    @Bind(R.id.tv_download)
    TextView tv_download;
    MaterialDialog progressDialog;

    //要判断已经下载的数据
    private EmptyViewLayout emptyViewLayout;
    protected View rootView;
    protected DownloadManagerPresenter presenter;
    protected OnItemSelectNumChangedListener onItemSelectNumChangedListener;

    /**
     * 设置内容id
     *
     * @return
     */
    protected abstract int getContentId();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(getContentId(), container, false);
        ButterKnife.bind(this, rootView);
        presenter = new DownloadManagerPresenter();
        onItemSelectNumChangedListener = presenter;
        presenter.attachView(this);
        tv_select.setOnClickListener(this);
        tv_download.setOnClickListener(this);
        showSelectAllText();
        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
        presenter.detachView();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        emptyViewLayout = new EmptyViewLayout(context(), rootView);
        initView();
    }

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_download:
                presenter.download();
                break;

        }
    }

    @Override
    public void showLoading() {
        if (progressDialog == null){
            progressDialog = new MaterialDialog(getActivity());
        }
        progressDialog.setContentView(R.layout.app_view_loading);
        progressDialog.show();
    }

    @Override
    public void hideLoading() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        emptyViewLayout.showContentView();
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        emptyViewLayout.showContentView();
        Toast.makeText(appContext, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public Context context() {
        return getActivity();
    }

    @Override
    public void setDownloadNum(int num) {
        tv_download.setText("下载(" + num + ")");
        tv_download.setTextColor(num == 0 ? getResources().getColor(R.color.text_color_primary_hint) : getResources().getColor(R.color.color_primary));
    }

    @Override
    public void showClearAllText() {
        tv_select.setText("取消全选");
        tv_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.clearAll();
            }
        });
    }

    @Override
    public void showSelectAllText() {
        tv_select.setText("全选");
        tv_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.selectAll();
            }
        });
    }

    @Override
    public void showNoSelectHit() {
        Toast.makeText(appContext, "您还没有选择", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showRepeatDownLoadHit() {
        Toast.makeText(appContext, "该视频已在下载列表中", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void downloadingHit() {
        Toast.makeText(appContext, "玩命为您下载中", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showNetWorkErrorHit() {
        Toast.makeText(appContext, "网络连接失败", Toast.LENGTH_SHORT).show();
    }


}
