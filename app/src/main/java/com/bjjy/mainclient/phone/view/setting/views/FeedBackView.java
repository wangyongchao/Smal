package com.bjjy.mainclient.phone.view.setting.views;


import com.dongao.mainclient.model.mvp.MvpView;


/**
 * 登录UI 定义UI 看此UI中有何事件
 */
public interface FeedBackView extends MvpView {
  void setData();
  String getContent();
//  void refresh();
}
