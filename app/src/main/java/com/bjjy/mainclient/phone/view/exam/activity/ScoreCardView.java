package com.bjjy.mainclient.phone.view.exam.activity;

import android.content.Intent;

import com.dongao.mainclient.model.mvp.MvpView;
import com.bjjy.mainclient.phone.app.AppContext;


/**
 * Created by wyc on 2016/5/6.
 */
public interface ScoreCardView extends MvpView {
    Intent getTheIntent();
    AppContext getAppContext();
    void showStatueView(int state);
    void finishActivity();
}
