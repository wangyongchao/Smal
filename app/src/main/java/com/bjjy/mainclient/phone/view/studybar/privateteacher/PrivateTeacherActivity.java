package com.bjjy.mainclient.phone.view.studybar.privateteacher;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bjjy.mainclient.phone.app.BaseFragmentActivity;
import com.bjjy.mainclient.phone.event.PushMsgNotification;
import com.bjjy.mainclient.phone.view.studybar.Constant;
import com.bjjy.mainclient.phone.view.studybar.privateteacher.adapter.PrivateTeacherAdapter;
import com.bjjy.mainclient.phone.view.studybar.view.RefreshLayout;
import com.bjjy.mainclient.phone.widget.EmptyViewLayout;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.widget.coverflow.RollPagerView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by wyc on 2016/6/6.
 */
public class PrivateTeacherActivity extends BaseFragmentActivity implements PrivateTeacherView, PrivateTeacherAdapter.ReclerViewItemClick {
    @Bind(R.id.top_title_left)
    ImageView top_title_left;
    @Bind(R.id.top_title_right)
    ImageView top_title_right;
    @Bind(R.id.top_title_text)
    TextView top_title_text;
    @Bind(R.id.swipe_container)
	RefreshLayout swipe_container;
    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;
    @OnClick(R.id.top_title_left) void onBackClick(){
        onBackPressed();
    }
    private PrivateTeacherPercenter privateTeacherPercenter;
    private EmptyViewLayout mEmptyLayout;
    private RollPagerView mRollViewPager;
    private PrivateTeacherAdapter privateTeacherAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.studybar_privateteacher_activity);
        ButterKnife.bind(this);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        privateTeacherPercenter = new PrivateTeacherPercenter();
        privateTeacherPercenter.attachView(this);
        initView();
        initData();
    }

    @Override
    public void initView() {
        top_title_left.setVisibility(View.VISIBLE);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(PrivateTeacherActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        mEmptyLayout = new EmptyViewLayout(this, swipe_container);
        mEmptyLayout.setErrorButtonClickListener(mErrorClickListener);
        mEmptyLayout.setEmptyButtonClickListener(emptyButtonClickListener);
        swipe_container.setColorSchemeColors(getResources().getColor(R.color.color_primary));
        swipe_container.setChildView(recyclerView);
        setListener();
       
    }

    private void setListener() {
        swipe_container.setOnLoadListener(new RefreshLayout.OnLoadListener() {
            @Override
            public void onLoad() {
                privateTeacherPercenter.getLoadData();
            }
        });
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int topRowVerticalPosition =
                        (recyclerView == null || recyclerView.getChildCount() == 0) ? 0 : recyclerView.getChildAt(0).getTop();
                swipe_container.setEnabled(topRowVerticalPosition >= 0);

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                privateTeacherPercenter.currentPage=1;
                setNoDataMoreShow(false);
                privateTeacherPercenter.getData();
            }
        });
    }


    /**
     * 错误监听
     */
    private View.OnClickListener mErrorClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            privateTeacherPercenter.currentPage=1;
            setNoDataMoreShow(false);
            privateTeacherPercenter.getData();
        }
    };
    /**
     * 无数据监听
     */
    private View.OnClickListener emptyButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //showAppMsg("显示kong");
        }
    };

    @Override
    public void initData() {
        mEmptyLayout.showLoading();
        privateTeacherPercenter.initData();
    }

    @Override
    public void showLoading() {
       if(swipe_container != null){
           swipe_container.setRefreshing(true);
        }
    }

    @Override
    public void hideLoading() {
        if(swipe_container != null){
            swipe_container.setRefreshing(false);
            swipe_container.setLoading(false);
        }
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public Context context() {
        return PrivateTeacherActivity.this;
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void initAdapter() {
        if (privateTeacherAdapter==null){
            privateTeacherAdapter = new PrivateTeacherAdapter(this,privateTeacherPercenter.privateTeacherList);
            privateTeacherAdapter.setOnItemClick(this);
            recyclerView.setAdapter(privateTeacherAdapter);
        }else{
            privateTeacherAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void showContentView(int type) {
        if (type== Constant.VIEW_TYPE_0){
            mEmptyLayout.showContentView();
        }else  if (type==Constant.VIEW_TYPE_1){
            mEmptyLayout.showNetErrorView();
        }else  if (type==Constant.VIEW_TYPE_2){
            mEmptyLayout.showEmpty();
        }else  if (type==Constant.VIEW_TYPE_3){
            mEmptyLayout.showError();
        }
        hideLoading();
    }

    @Override
    public boolean isRefreshNow() {
        if (swipe_container==null){
            return false;
        }
        return swipe_container.isRefreshing();
    }

    @Override
    public RefreshLayout getRefreshLayout() {
        return swipe_container;
    }

    @Override
    public Intent getTheIntent() {
        return getIntent();
    }

    @Override
    public void showTopTitle(String title) {
        top_title_text.setText(title);
    }

    @Override
    public void setNoDataMoreShow(boolean isShow) {
        if (privateTeacherAdapter!=null){
            privateTeacherAdapter.setNoDataShow(isShow);
            privateTeacherAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onItemClickListenerPosition(int position) {
        privateTeacherPercenter.setOnItemClick(position);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Subscribe
    public void onEventAsync(PushMsgNotification event) {
        initData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        initData();
    }
}
