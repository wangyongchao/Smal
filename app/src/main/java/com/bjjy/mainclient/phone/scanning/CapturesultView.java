package com.bjjy.mainclient.phone.scanning;


import android.app.Activity;

import com.dongao.mainclient.model.mvp.MvpView;
import com.dongao.mainclient.model.bean.play.CourseWare;

import java.util.List;

/**
 * UI 定义UI 看此UI中有何事件
 */
public interface CapturesultView extends MvpView {
  void setData(List<CourseWare> papers);
  String getQrUrl();
  void setType(String type);
  Activity getActivity();
}
