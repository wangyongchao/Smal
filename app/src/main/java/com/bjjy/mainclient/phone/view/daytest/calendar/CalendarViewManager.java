package com.bjjy.mainclient.phone.view.daytest.calendar;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.bjjy.mainclient.phone.view.daytest.DayTestActivity;
import com.bjjy.mainclient.phone.view.daytest.calendar.cons.DPMode;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.view.daytest.calendar.views.MonthView;

import java.util.Calendar;

/**
 * Created by fengzongwei on 2016/5/26 0026.
 * 显示日历的popwindow管理器
 */
public class CalendarViewManager implements MonthView.OnDateChangeListener, MonthView.OnDatePickedListener{

    private PopupWindow popupWindow;//生成的popwindow对象
    private Context context;

    private MonthView monthView;
    private Calendar now;
    private TextView textView_month;
    private TextView textView_subjectName;//当前页面的科目名称
    private LinearLayout linearLayout_monthView_body;//装载月份view的容器
    private OnCalendarDatePickListener onCalendarDatePickListener;
    private String currentSubjectName;
    public CalendarViewManager(Context context,final PopupWindow.OnDismissListener dismissListener){
        this.context = context;
        View contentView = LayoutInflater.from(context).inflate(R.layout.day_test_calendar,null);
        textView_month = (TextView) contentView.findViewById(R.id.calenfar_month_num_tv);
        textView_subjectName = (TextView) contentView.findViewById(R.id.day_test_calendar_subjectName_tv);
        linearLayout_monthView_body = (LinearLayout)contentView.findViewById(R.id.calenfar_month_monthview_body);
        now = Calendar.getInstance();
        textView_month.setText(now.get(Calendar.YEAR) + "." + (now.get(Calendar.MONTH) + 1));
        popupWindow = new PopupWindow();
        popupWindow = new PopupWindow(contentView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        popupWindow.setContentView(contentView);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        popupWindow.setFocusable(true);
        popupWindow.setOnDismissListener(dismissListener);
        popupWindow.setAnimationStyle(R.style.main_pop_anim);
    }

    public void setOnCalendarDatePickListener(OnCalendarDatePickListener onCalendarDatePickListener){
        this.onCalendarDatePickListener = onCalendarDatePickListener;
    }

    public void showCalendarView(View archar){
        addMonthViewToContainer(((DayTestActivity)context).getCurrentSubject().getSubjectName());
        textView_subjectName.setText(currentSubjectName);
        popupWindow.showAsDropDown(archar);
    }

    public void dissmissPopWindow(){
        popupWindow.dismiss();
    }

    @Override
    public void onDateChange(int year, int month) {
        textView_month.setText(year + "." + month);
    }

    @Override
    public void onDatePicked(String date) {
        dissmissPopWindow();
        if(onCalendarDatePickListener!=null){
            onCalendarDatePickListener.onDatePicked(date);
        }
    }

    public interface OnCalendarDatePickListener{
        void onDatePicked(String date);
    }

    /**
     * 将月份控件添加到容器
     */
    private void addMonthViewToContainer(String subjectName){
            this.currentSubjectName = subjectName;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
            monthView = new MonthView(context);
            monthView.setLayoutParams(params);
            textView_month.setText(((DayTestActivity) context).getCurrentYear() + "." + ((DayTestActivity) context).getCurrentMonth());
//            monthView.setDate(now.get(Calendar.YEAR), now.get(Calendar.MONTH) + 1);
            monthView.setDate(((DayTestActivity) context).getCurrentYear(),((DayTestActivity) context).getCurrentMonth());
            monthView.setDPMode(DPMode.SINGLE);
            monthView.setSelectedDay(((DayTestActivity) context).getCurrentDay());
            monthView.setTodayDisplay(true);
            monthView.setSubjectId(((DayTestActivity) context).getCurrentSubjectId());
            monthView.setExmaId(((DayTestActivity)context).getCurrentExamId());
            monthView.setFestivalDisplay(false);
            monthView.setOnDateChangeListener(this);
            monthView.setOnDatePickedListener(this);
            linearLayout_monthView_body.removeAllViews();
            linearLayout_monthView_body.addView(monthView);
    }

}
