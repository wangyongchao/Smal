package com.bjjy.mainclient.phone.view.studybar.fragment;

import com.dongao.mainclient.model.mvp.MvpView;

/**
 * @author wyc
 */
public interface StudyBarFragmentView extends MvpView {
    void initAdapter();
    void refreshAdapter();
    void showCurrentView(int type);//0正常 1：网络错误 2：数据错误
}
