package com.bjjy.mainclient.phone.view.classroom.courseplay;


import com.dongao.mainclient.model.mvp.MvpView;
import com.dongao.mainclient.model.bean.course.CoursePlayBean;

import java.util.List;

/**
 * UI 定义UI 看此UI中有何事件
 */
public interface CoursePlayFragmentView extends MvpView {
  void setData(List<CoursePlayBean> coursePlayBean);
  String getSubjectId();
}
