package com.bjjy.mainclient.phone.view.goodsubject.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dongao.mainclient.model.bean.courselect.Exams;
import com.bjjy.mainclient.phone.R;

import java.util.ArrayList;

/**
 * Created by dell on 2016/4/5.
 */
public class CourseCenterAdapter extends BaseAdapter {

    private Context mcontext;
    private ArrayList<Exams> mlist;

    public CourseCenterAdapter(Context context) {
        super();
        this.mcontext = context;
    }

    public void setList(ArrayList<Exams> mlist) {
        this.mlist = mlist;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mlist.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup arg2) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = View.inflate(mcontext, R.layout.courselectcenter_item, null);
            holder.title = (TextView) convertView.findViewById(R.id.tv_coursename);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.title.setText(mlist.get(position).getExamName());
        return convertView;
    }
}

class ViewHolder {
    TextView title;
}