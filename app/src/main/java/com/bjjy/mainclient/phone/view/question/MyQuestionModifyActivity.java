package com.bjjy.mainclient.phone.view.question;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dongao.mainclient.model.local.MyQuestionAskTextDb;
import com.bjjy.mainclient.persenter.ModifyQuePersenter;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseActivity;
import com.bjjy.mainclient.phone.utils.NetworkUtil;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by fengzongwei on 2016/5/3 0003.
 */
public class MyQuestionModifyActivity extends BaseActivity implements MyQuestionModifyView{
    @Bind(R.id.top_title_text)
    TextView textView_title;
    @Bind(R.id.top_title_right)
    TextView textView_post;
    @Bind(R.id.my_que_fontCount)
    TextView textView_count;
    @Bind(R.id.my_que_ed)
    EditText editText;

    public static  final int
            CONTINUE_ASK = 1;//追问  2.修改
    private int maxCount = 500//最多的输入文字数
            ,copy_count;//剩余字数
    private ForegroundColorSpan redSpan = new ForegroundColorSpan(Color.RED);
    private InputMethodManager imm;//软键盘控制
    private int mode;//当前页面操作的类型
    private Intent intent;
    private String title,content,questionId;//问题相关数据
    private boolean isCanPost = true;//当前点击提交按钮是否可以进行提交
    private String subjectId;//科目id
    private MyQuestionAskTextDb myQuestionAskTextDb;//未提交的提问数据库
    private ModifyQuePersenter modifyQuePersenter;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question_modify);
        ButterKnife.bind(this);
        initData();
        modifyQuePersenter = new ModifyQuePersenter(mode);
        modifyQuePersenter.attachView(this);
        initView();
        setTitle();
    }

    @Override
    public void initView() {
        textView_post.setVisibility(View.VISIBLE);
        /**
         * 给输入框设置输入监听
         */
        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (copy_count >= 0) {
                    copy_count = maxCount - editText.getText().length();
                    if (copy_count < 0) {
                        copy_count = 0;
                        editText.setText(s.subSequence(0, maxCount));
                        editText.setSelection(maxCount);
                    }
                    textView_count.setText(hintStr("剩余" + copy_count + "字"));
                    if (copy_count < maxCount) {
                        textView_post.setTextColor(Color.parseColor("#ffffff"));
                    } else {
                        textView_post.setTextColor(Color.parseColor("#DBDBDB"));
                    }
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @Override
    public void initData() {
        intent = getIntent();
        mode = intent.getIntExtra("mode", -1);
        imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        myQuestionAskTextDb = new MyQuestionAskTextDb(this);
        questionId = intent.getStringExtra("questionId");//追问时候用到的Id
        if(mode == 2) {
            editText.setText(intent.getStringExtra("neirong"));
            editText.setSelection(intent.getStringExtra("neirong").length());
            copy_count = 500 - intent.getStringExtra("neirong").length();
        }
        modifyQuePersenter = new ModifyQuePersenter(mode);
    }

    @Override
    public void onClick(View v) {

    }

    /**
     * 提交
     */
    @OnClick(R.id.top_title_right) void post(){
//        if(copy_count < maxCount && copy_count>0) {
            if(editText.getText().length()>15)
                title = editText.getText().toString().substring(0,15);
            else
                title = editText.getText().toString();
            content = editText.getText().toString();
            if(isCanPost) {
                isCanPost = false;
                imm.hideSoftInputFromWindow(editText.getWindowToken(),0);
                if (NetworkUtil.isNetworkAvailable(appContext)) {
                    modifyQuePersenter.getData();
//                    if(mode == 2)
//                        modifyQuePersenter.getData();
//                    else
//                        modifyQuePersenter.continueAskQuestion();
                } else {
                    isCanPost = true;
                    Toast.makeText(MyQuestionModifyActivity.this, "网络异常", Toast.LENGTH_SHORT).show();
                }
//            }
        }
    }


    @OnClick(R.id.top_title_left) void back(){
        finish();
    }

    /**
     * 根据当前页面不同的业务，设置不同的title标题
     */
    private  void setTitle(){
        switch (mode) {
            case CONTINUE_ASK:
                textView_title.setText("追问我的答疑");
                break;
            case 2:
                textView_title.setText("修改我的答疑");
                textView_count.setText(hintStr("剩余" + copy_count + "字"));
                textView_post.setTextColor(Color.parseColor("#ffffff"));
                break;
            default:
                textView_title.setText("试题提问");
                break;
        }
    }

    /**
     * 修改剩余文字数量的显示，改变颜色
     * @param countLast
     * @return
     */
    private SpannableStringBuilder hintStr(String countLast){
        SpannableStringBuilder builder_ques = new SpannableStringBuilder(countLast);
        builder_ques.setSpan(redSpan, 2, countLast.length()-1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return builder_ques;
    }



    @Override
    public void resultAction() {
        Intent intent1 = new Intent();
        intent1.putExtra("mode", mode);
        if(mode == 2) {//修改
            intent.putExtra("content", content);
            intent.putExtra("questionId",intent.getStringExtra("questionId"));
            Toast.makeText(this,"修改成功",Toast.LENGTH_SHORT).show();
        }else if(mode == 1){
            Toast.makeText(this,"追问成功",Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this,"提问成功",Toast.LENGTH_SHORT).show();
        }
        setResult(RESULT_OK, intent1);
        finish();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {
        isCanPost = true;
        Toast.makeText(MyQuestionModifyActivity.this,message,Toast.LENGTH_SHORT).show();
    }

    @Override
    public String getParamsFromIntent(String key) {
        return intent.getStringExtra(key);
    }

    @Override
    public String getQuesContent() {
        return content;
    }

    @Override
    public String getQuesTitle() {
        return title;
    }

    @Override
    public Context context() {
        return this;
    }

    @Override
    public void showAnimProgress() {
        showAnimProgress("数据上传中，请稍后...");
    }

    @Override
    public void dissmissAnimProgress() {
        dismissAnimProgress();
    }
}
