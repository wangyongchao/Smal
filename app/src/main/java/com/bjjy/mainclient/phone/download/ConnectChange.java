package com.bjjy.mainclient.phone.download;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.dongao.mainclient.core.app.AppManager;
import com.dongao.mainclient.model.bean.play.CourseWare;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.bjjy.mainclient.phone.app.AppConfig;
import com.bjjy.mainclient.phone.app.AppContext;
import com.bjjy.mainclient.phone.download.db.DownloadDB;

import java.util.List;
import java.util.Stack;

/**
 * 网络状态改变的监听
 * Created by wp on 2016/2/1.
 */
public class ConnectChange extends BroadcastReceiver {
    private DownloadDB db;
    private Context context;
    private List<CourseWare> list;
    private static final int NETCHANGEPLAY = 44;//切网播放暂停
    private static final int NETCHANGEPLAYING = 55;//切网播放jixu

    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        db = new DownloadDB(context);
        Stack<Activity> activityList = AppManager.getAppManager().getStack();
        boolean isLogin = SharedPrefHelper.getInstance(context).isLogin();
        if (activityList == null || activityList.size() == 0 || !isLogin) {
            return;
        }

        list = db.findAllNotFinish(SharedPrefHelper.getInstance(context).getUserId());

        //这里要判断应用是否退出
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mobileInfo = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo wifiInfo = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo activeInfo = manager.getActiveNetworkInfo();

        Activity activity= AppManager.getAppManager().currentActivity();
        boolean isInPlayAcitivy=activity.getLocalClassName().equals("view.play.PlayActivity");

        if (wifiInfo!=null && wifiInfo.isConnected()) { //切换到wifi
            //这里判断下载是否正在下载（要清除下载列表？？）
            if (AppContext.getInstance().isStart == 0) {
                continueDownload();
            }else{  //这里要加dns重新解析的逻辑
                AppContext.getInstance().isStart = 0;
                DownloadExcutor.getInstance(context).setFlag(false);
                db.updateState(Constants.STATE_Pause);
                continueDownload();
            }
        } else if (mobileInfo!=null && mobileInfo.isConnected()) { //切换到流量
            boolean isPlayDownload = SharedPrefHelper.getInstance(context).getIsNoWifiPlayDownload();
            if(!isInPlayAcitivy && isPlayDownload){     //不在播放页，控制下载
                if (AppContext.getInstance().isStart == 0) {
                    continueDownload();
                }
            }else if(isInPlayAcitivy && isPlayDownload){  //在播放页，控制下载播放（播放不用处理）
                if (AppContext.getInstance().isStart == 0) {
                    continueDownload();
                }
            }else if(isInPlayAcitivy && !isPlayDownload){
                AppContext.getInstance().getPlayHandler().sendEmptyMessage(NETCHANGEPLAY);
            }

//            if (isDownload) {
//                if (AppContext.getInstance().isStart == 0) {
//                    continueDownload();
//                }
//            } else {
//                if (list != null && list.size() > 0 && isInPlayAcitivy) {
//                    AppContext.getInstance().getPlayHandler().sendEmptyMessage(NETCHANGEPLAY);
//                    DialogManager.showNormalDialog(AppManager.getAppManager().currentActivity(), "您当前连接的3G/4G，有未完成的下载和播放，确定继续吗？", "提示", "取消", "确定", new DialogManager.CustomDialogCloseListener() {
//                        @Override
//                        public void yesClick() {
//                            AppContext.getInstance().getPlayHandler().sendEmptyMessage(NETCHANGEPLAYING);
//                            continueDownload();
//                        }
//
//                        @Override
//                        public void noClick() {
//                        }
//                    });
//                } else if (list != null && list.size() > 0) {
//                    //这里要弹出提示 ，是否继续下载
//                    DialogManager.showNormalDialog(AppManager.getAppManager().currentActivity(), "您当前连接的3G/4G，有未完成的下载，确定下载吗？", "提示", "取消", "确定", new DialogManager.CustomDialogCloseListener() {
//                        @Override
//                        public void yesClick() {
//                            continueDownload();
//                        }
//
//                        @Override
//                        public void noClick() {
//                        }
//                    });
//                } else if (isInPlayAcitivy) {
//                    AppContext.getInstance().getPlayHandler().sendEmptyMessage(NETCHANGEPLAY);
//                    DialogManager.showNormalDialog(AppManager.getAppManager().currentActivity(), "您当前连接的3G/4G，当前正在播放视频，确定继续吗？", "提示", "取消", "确定", new DialogManager.CustomDialogCloseListener() {
//                        @Override
//                        public void yesClick() {
//                            AppContext.getInstance().getPlayHandler().sendEmptyMessage(NETCHANGEPLAYING);
//                        }
//
//                        @Override
//                        public void noClick() {
//
//                        }
//                    });
//                }
//            }
        }

    }

    private void continueDownload() {
        if (list == null || list.size() <= 0) {
            return;
        }
        DownloadTaskManager.getInstance().clearList();
        for (int i = 0; i < list.size(); i++) {
            AppConfig.getAppConfig(context).download(list.get(i));
        }
    }
}
