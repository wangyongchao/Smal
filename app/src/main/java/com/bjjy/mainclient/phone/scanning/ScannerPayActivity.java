package com.bjjy.mainclient.phone.scanning;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.remote.ApiService;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.view.setting.WebViewActivity;

/**
 * 二维码扫描
 * @author wyc
 */
public class ScannerPayActivity extends Activity  {


    private TextView tv_pay,tv_call;
    private AlertDialog.Builder alertDialog;
    private ImageView top_title_left;
    private TextView top_title_text;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pay_scanner_activity);
        initViews();
    }

    private void initViews() {
        tv_pay= (TextView) findViewById(R.id.tv_pay);
        tv_call= (TextView) findViewById(R.id.tv_call);
        top_title_left= (ImageView) findViewById(R.id.top_title_left);
        top_title_text= (TextView) findViewById(R.id.top_title_text);
        top_title_left.setVisibility(View.VISIBLE);
        top_title_text.setText("辅导书知识点课程");
        top_title_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
       
        tv_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(Intent.ACTION_DIAL);
//                Uri data = Uri.parse("tel:" + "400-627-5566");
//                intent.setData(data);
//                startActivity(intent);
                callDialog();
            }
        });
        tv_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ScannerPayActivity.this, WebViewActivity.class);
                intent.putExtra(Constants.APP_WEBVIEW_TITLE, "选课购买");
                intent.putExtra(Constants.APP_WEBVIEW_URL, ApiService.SELECT_COURSE_URL);
                startActivity(intent);
            }
        });
    }

    /**
     * 点击电话号码时弹出提示
     */
    private void callDialog() {
        if (alertDialog == null) {
            alertDialog = new AlertDialog.Builder(ScannerPayActivity.this);
            alertDialog.setTitle("提示");
            alertDialog.setMessage("是否确定拨打？");
            alertDialog.setNegativeButton("取消", null);
            alertDialog.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + tv_call.getText().toString()));
                    startActivity(intent);
                }
            });
            alertDialog.create().show();
        } else {
            alertDialog.show();
        }
    }

}