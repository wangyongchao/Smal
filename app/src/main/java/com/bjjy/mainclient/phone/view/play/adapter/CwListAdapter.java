package com.bjjy.mainclient.phone.view.play.adapter;

import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.AppConfig;
import com.bjjy.mainclient.phone.download.DownloadExcutor;
import com.bjjy.mainclient.phone.download.db.DownloadDB;
import com.bjjy.mainclient.phone.utils.NetWorkUtils;
import com.bjjy.mainclient.phone.view.play.PlayActivity;
import com.bjjy.mainclient.phone.view.play.widget.RoundProgressBar;
import com.bjjy.mainclient.phone.widget.DialogManager;
import com.dongao.mainclient.model.bean.play.CourseWare;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;

import java.util.List;

/**
 * Created by fengzongwei on 2016/5/16 0016.
 */
public class CwListAdapter extends BaseAdapter {

    private List<CourseWare> courseWareWareList;
    private PlayActivity context;
    private DownloadDB db;
    private final int[] viewType = {0, 1};//0 普通条目  1，是课后作业提示
    private Handler handler;

    public CwListAdapter(PlayActivity context, List<CourseWare> courseWareWareList, Handler handler) {
        this.context = context;
        this.courseWareWareList = courseWareWareList;
        db = new DownloadDB(context);
        this.handler = handler;
    }

    @Override
    public int getCount() {
        return courseWareWareList.size();
    }

    @Override
    public int getViewTypeCount() {
        return viewType.length;
    }

    @Override
    public int getItemViewType(int position) {
//        if(courseWareWareList.get(position) instanceof CoursePractice){
//            return viewType[2];
//        }else{
//            if(((CourseWare)courseWareWareList.get(position)).getId()!=null &&
//                    ((CourseWare)courseWareWareList.get(position)).getId().equals("kehouzuoye")){
//                return viewType[1];
//            }else
//                return viewType[0];
//        }
        if (courseWareWareList.get(position).getCwId().equals("kehouzuoye")) {
            return viewType[1];
        }
        return viewType[0];
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        int viewType = getItemViewType(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.play_courelist_nosection_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.play_courselist_nosection_course_body = (LinearLayout) convertView.findViewById(R.id.play_courselist_nosection_course_body);
            viewHolder.play_courselist_fragment_practice_body = (LinearLayout) convertView.findViewById(R.id.play_courselist_fragment_practice_body);
            viewHolder.play_courselist_child_test_img = (ImageView) convertView.findViewById(R.id.play_courselist_child_test_img);
            viewHolder.play_courselist_child_tv = (TextView) convertView.findViewById(R.id.play_courselist_child_tv);
            viewHolder.play_courselist_child_download_img = (ImageView) convertView.findViewById(R.id.play_courselist_child_download_img);
            viewHolder.play_courselist_child_download_circle_pb = (RoundProgressBar) convertView.findViewById(R.id.play_courselist_child_download_circle_pb);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (courseWareWareList.get(position).isPlayFinished())
            viewHolder.play_courselist_child_tv.setTextColor(Color.parseColor("#DBDBDB"));
        else
            viewHolder.play_courselist_child_tv.setTextColor(Color.BLACK);

        if (courseWareWareList.get(position).getChapterNo() != null && courseWareWareList.get(position).getChapterNo().equals("kehouzuoye") ||
                courseWareWareList.get(position).getCwId().equals("kehouzuoye")) {
            viewHolder.play_courselist_child_download_circle_pb.setVisibility(View.GONE);
            viewHolder.play_courselist_child_download_img.setVisibility(View.GONE);
        } else {
            int state = db.getState(SharedPrefHelper.getInstance(context).getUserId(),
                    courseWareWareList.get(position).getClassId(),
                    courseWareWareList.get(position).getCwId());
//
            if (state == Constants.STATE_DownLoaded) {
                viewHolder.play_courselist_child_download_img.setVisibility(View.VISIBLE);
            } else {
                viewHolder.play_courselist_child_download_img.setVisibility(View.GONE);
            }
        }
        viewHolder.play_courselist_child_download_circle_pb.setVisibility(View.GONE);
//        viewHolder.play_courselist_child_download_img.setVisibility(View.GONE);
        if (viewType == 1) {
            viewHolder.play_courselist_nosection_course_body.setVisibility(View.GONE);
            viewHolder.play_courselist_fragment_practice_body.setVisibility(View.VISIBLE);
        } else {
            viewHolder.play_courselist_nosection_course_body.setVisibility(View.VISIBLE);
            viewHolder.play_courselist_fragment_practice_body.setVisibility(View.GONE);

            if (context.getPlayingCWId().equals(courseWareWareList.get(position).getCwId())) {
                viewHolder.play_courselist_child_tv.setTextColor(Color.parseColor("#ff7666"));
            } else {
//                viewHolder.play_courselist_child_tv.setTextColor(Color.BLACK);
            }
                viewHolder.play_courselist_child_test_img.setVisibility(View.GONE);
//            viewHolder.play_courselist_child_test_img.setVisibility(View.GONE);
            viewHolder.play_courselist_child_test_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });

            viewHolder.play_courselist_child_tv.setText(
                    courseWareWareList.get(position).getCwName());
        }

        viewHolder.play_courselist_child_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Message message = handler.obtainMessage();
                message.what = 3;
                message.arg1 = position;
                handler.sendMessage(message);
            }
        });

        viewHolder.play_courselist_child_tv.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Message message = handler.obtainMessage();
                message.what = 1;
                message.obj = courseWareWareList.get(position);
                handler.sendMessage(message);
                return false;
            }
        });

//        viewHolder.play_courselist_child_download_img.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                MobclickAgent.onEvent(context, PushConstants.PLAY_DOWNLOAD);
//                context.checkMachine(new DownloadView() {
//                    @Override
//                    public void isCheckMachineOk(String msg, boolean flag) {
//                        if (flag) {
//                            CourseWare cw = courseWareWareList.get(position);
//                            checkDownState(cw);
//                            if (cw.getPracticeFlag() == 1) {
//                                context.downloadExam();
//                            }
//                        } else {
//                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                });
//            }
//        });

        viewHolder.play_courselist_child_download_circle_pb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CourseWare cw = courseWareWareList.get(position);
                checkDownState(cw);
            }
        });

        return convertView;
    }

    static class ViewHolder {
        LinearLayout play_courselist_nosection_course_body;
        LinearLayout play_courselist_fragment_practice_body;
        ImageView play_courselist_child_test_img;
        ImageView play_courselist_child_download_img;
        TextView play_courselist_child_tv;
        RoundProgressBar play_courselist_child_download_circle_pb;
    }

    private void checkDownState(CourseWare cw) {
        int state = db.getState(SharedPrefHelper.getInstance(context).getUserId(), cw.getClassId(), cw.getCwId());
        if (state == Constants.STATE_DownLoading) {
            DownloadExcutor.getInstance(context).setFlag(false);
            db.updateState(SharedPrefHelper.getInstance(context).getUserId(), cw.getClassId(), cw.getCwId(), Constants.STATE_Pause);
            notifyDataSetChanged();
        } else if (state == Constants.STATE_Waiting) {
            db.updateState(SharedPrefHelper.getInstance(context).getUserId(), cw.getClassId(), cw.getCwId(), Constants.STATE_Pause);
            notifyDataSetChanged();
            if (DownloadExcutor.getInstance(context).task != null) {
                if (DownloadExcutor.getInstance(context).task.getCourseWareId().equals(cw.getCwId()) && DownloadExcutor.getInstance(context).task.getCourseId().equals(cw.getClassId())) {
                    DownloadExcutor.getInstance(context).setFlag(false);
                }
            }
        } else if (state == Constants.STATE_Pause || state == Constants.STATE_Error || state == 0) {
            CheckSettingDownload(cw);
        }
    }

    /**
     * 下载前检查网络
     */
    private void CheckSettingDownload(final CourseWare cw) {
        boolean isDownload = SharedPrefHelper.getInstance(context).getIsNoWifiPlayDownload();
        NetWorkUtils type = new NetWorkUtils(context);
        if (type.getNetType() == 0) {//无网络
            DialogManager.showMsgDialog(context, context.getResources().getString(R.string.dialog_message_vedio), context.getResources().getString(R.string.dialog_title_download), "确定");
        } else if (type.getNetType() == 2) { //流量
            if (!isDownload) {
                DialogManager.showNormalDialog(context, context.getResources().getString(R.string.dialog_warnning_vedio), "提示", "取消", "确定", new DialogManager.CustomDialogCloseListener() {
                    @Override
                    public void yesClick() {
                        db.updateState(SharedPrefHelper.getInstance(context).getUserId(), cw.getClassId(), cw.getCwId(), Constants.STATE_Waiting);
                        notifyDataSetChanged();
                        AppConfig.getAppConfig(context).download(cw);
                    }

                    @Override
                    public void noClick() {
                    }
                });
            } else {
                db.updateState(SharedPrefHelper.getInstance(context).getUserId(), cw.getClassId(), cw.getCwId(), Constants.STATE_Waiting);
                notifyDataSetChanged();
                AppConfig.getAppConfig(context).download(cw);
            }
        } else if (type.getNetType() == 1) {//wifi
            db.updateState(SharedPrefHelper.getInstance(context).getUserId(), cw.getClassId(), cw.getCwId(), Constants.STATE_Waiting);
            notifyDataSetChanged();
            AppConfig.getAppConfig(context).download(cw);
        }
    }

}
