package com.bjjy.mainclient.phone.view.daytest;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bjjy.mainclient.persenter.DayTestPersenter;
import com.bjjy.mainclient.phone.widget.DialogManager;
import com.dongao.mainclient.model.bean.daytest.DayExSelectedSubject;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseFragmentActivity;
import com.bjjy.mainclient.phone.view.daytest.adapter.DayTestSubjectAdapter;
import com.bjjy.mainclient.phone.view.daytest.calendar.CalendarViewManager;
import com.bjjy.mainclient.phone.widget.EmptyViewLayout;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by wyc on 2016/5/24 0024.
 * 每日一练
 */
public class DayTestActivity extends BaseFragmentActivity implements DayTestView{
    @Bind(R.id.top_title_left)
    ImageView imageView_back;
    @Bind(R.id.top_title_right)
    ImageView imageView_calendar;
    @Bind(R.id.day_test_viewpagertab)
    SmartTabLayout smartTabLayout;
    @Bind(R.id.day_test_vp)
    ViewPager viewPager;
    @Bind(R.id.top_title_bar_layout)
    View top_bar;
    @Bind(R.id.top_title_text)
    TextView textView_title;
    @Bind(R.id.day_test_add_subject_img)
    ImageView imageView_add_subject;
    @Bind(R.id.day_test_body)
    LinearLayout linearLayout_body;
    private CalendarViewManager calendarViewManager;
    private EmptyViewLayout emptyViewLayout;

    private List<DayExSelectedSubject> selectedSubjects = new ArrayList<>();
    private long[] fragmentTimes;
    private DayTestSubjectAdapter dayTestSubjectAdapter;
    private DayTestPersenter dayTestPersenter;

    private SimpleDateFormat format = new SimpleDateFormat("yyyy-M-d");

    private int currentIndex = -1;//页面当前显示的角标

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.day_test);
        ButterKnife.bind(this);
        dayTestPersenter = new DayTestPersenter();
        dayTestPersenter.attachView(this);
        initView();
        if(SharedPrefHelper.getInstance(this).isLogin() &&
                !SharedPrefHelper.getInstance(this).getIsTransformUser()){
            dayTestPersenter.transFormDayTestData();
        }
//        showProgressDialog(this);
    }


    @Override
    protected void onResume() {
        super.onResume();
        initData();
    }

    @Override
    public void initView() {
        emptyViewLayout = new EmptyViewLayout(this,linearLayout_body);
        imageView_calendar.setOnClickListener(this);
        calendarViewManager = new CalendarViewManager(this,null);
        calendarViewManager.setOnCalendarDatePickListener(new CalendarViewManager.OnCalendarDatePickListener() {
            @Override
            public void onDatePicked(String date) {
//                emptyViewLayout.showLoading();
                try{
                    for(int i=0;i<fragmentTimes.length;i++){
                        fragmentTimes[i] = format.parse(date).getTime();
                    }
                    dayTestSubjectAdapter.freshData((format.parse(date)).getTime());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        textView_title.setText("每日一练");
        imageView_back.setImageResource(R.drawable.back);
        imageView_back.setVisibility(View.VISIBLE);
        imageView_calendar.setImageResource(R.drawable.btn_calendar);
        imageView_calendar.setVisibility(View.VISIBLE);
        imageView_back.setOnClickListener(this);
        imageView_calendar.setOnClickListener(this);
        imageView_add_subject.setOnClickListener(this);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                currentIndex = i;
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    @Override
    public void initData() {
        dayTestPersenter.getData();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.top_title_right:
                calendarViewManager.showCalendarView(top_bar);
                break;
            case R.id.top_title_left:
                finish();
                break;
            case R.id.day_test_add_subject_img:
                Intent intent = new Intent(DayTestActivity.this,SelectSubjectActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void showData(List<DayExSelectedSubject> selectedSubjects) {
        if(selectedSubjects == null || selectedSubjects.size()==0){
            DialogManager.showNormalDialog(this, "您还未进行选课，请先选课！", "提示", "取消", "选课", new DialogManager.CustomDialogCloseListener() {
                @Override
                public void yesClick() {
                    Intent intent = new Intent(DayTestActivity.this,SelectSubjectActivity.class);
                    startActivity(intent);
                }
                @Override
                public void noClick() {
                    finish();
                }
            });
            return;
        }
        this.selectedSubjects.clear();
//        this.selectedSubjects = selectedSubjects;
        this.selectedSubjects.addAll(selectedSubjects);
        fragmentTimes = new long[selectedSubjects.size()];
        for(int i=0;i<selectedSubjects.size();i++){
            fragmentTimes[i] = System.currentTimeMillis();
        }
        if(dayTestSubjectAdapter == null){
            dayTestSubjectAdapter = new DayTestSubjectAdapter(getSupportFragmentManager(),this.selectedSubjects);
            viewPager.setAdapter(dayTestSubjectAdapter);
            viewPager.setOffscreenPageLimit(3);
        }else{
            dayTestSubjectAdapter.notifyDataChange();
            dayTestSubjectAdapter.notifyDataSetChanged();
        }
        smartTabLayout.setViewPager(viewPager);
        if(currentIndex>=selectedSubjects.size()){
            viewPager.setCurrentItem(selectedSubjects.size()-1);
            currentIndex = selectedSubjects.size()-1;
        }
    }

    @Override
    public void showLoading() {
    }

    @Override
    public void hideLoading() {
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {
    }

    @Override
    public Context context() {
        return this;
    }

    /**
     * 当前显示的fragment角标
     * @return
     */
    public int getCurrentIndex(){
        return viewPager.getCurrentItem();
    }

    /**
     * 获取当前正在显示的科目信息
     * @return
     */
    public DayExSelectedSubject getCurrentSubject(){
        return selectedSubjects.get(viewPager.getCurrentItem());
    }

    /**
     * 获取当前时间
     * @param index
     * @return
     */
    public long getCurrentTime(int index){
        if(fragmentTimes!=null && index<fragmentTimes.length)
            return fragmentTimes[index];
        else
            return System.currentTimeMillis();
    }

    /**
     * 获取当前正在展示index的fragment对应的日期（这个月的第几天）
     * @return
     */
    public int getCurrentDay(){
        String timeStr = format.format(new Date(fragmentTimes[viewPager.getCurrentItem()]));
        int currentDay = Integer.parseInt(timeStr.split("-")[2]);
        int today = Integer.parseInt(format.format(new Date()).split("-")[2]);
        return currentDay==today? 0 : currentDay;
    }
    /**
     * 获取当前正在展示index的fragment对应的日期（月份）
     * @return
     */
    public int getCurrentMonth(){
        String timeStr = format.format(new Date(fragmentTimes[viewPager.getCurrentItem()]));
        int currentMonth = Integer.parseInt(timeStr.split("-")[1]);
        return currentMonth;
    }
    /**
     * 获取当前正在展示index的fragment对应的日期（年份）
     * @return
     */
    public int getCurrentYear(){
        String timeStr = format.format(new Date(fragmentTimes[viewPager.getCurrentItem()]));
        int currentYear = Integer.parseInt(timeStr.split("-")[0]);
        return currentYear;
    }

    /**
     * 切换上一题下一题时改变的时间
     * @param index
     * @param time
     */
    public void setCurrentTime(int index,long time){
        fragmentTimes[index] = time;
    }

    /**
     * 获取当前展示科目id
     * @return
     */
    public String getCurrentSubjectId() {
        return selectedSubjects.get(viewPager.getCurrentItem()).getSubjectId();
    }

    /**
     * 获取当前展示的考试id
     * @return
     */
    public String getCurrentExamId(){
        return selectedSubjects.get(viewPager.getCurrentItem()).getExamId();
    }

    /**
     * 获取指定角标下的examId
     * @param index
     * @return
     */
    public String  getIndexExamId(int index){
        if(index<selectedSubjects.size())
            return selectedSubjects.get(index).getExamId();
        else
            return "";
    }

    /**
     * 获取指定角标下的SubjectId
     * @param index
     * @return
     */
    public String getIndexSubjectId(int index){
        if(index<selectedSubjects.size())
            return selectedSubjects.get(index).getSubjectId();
        else
            return "";
    }

    public void hideEmptyLayout(){
        emptyViewLayout.showContentView();
    }

}
