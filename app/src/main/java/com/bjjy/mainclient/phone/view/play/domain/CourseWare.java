package com.bjjy.mainclient.phone.view.play.domain;

import com.yunqing.core.db.annotations.Id;
import com.yunqing.core.db.annotations.Table;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by fengzongwei on 2016/1/13.
 * 课程详情接口返回数据中的视频对象
 */

public class CourseWare implements Serializable{

    private int dbId;

    private String videoID;//视频ID
    private String videoName;//视频名称
    private String videoUrl;//视频地址
    private String videoLen;//视频长度
    private String videoLastPlayTime;//视频上次播放时间点
    private String jyUrl;//讲义地址
    private String effectiveStudyTime;//课件累计学习时间

    private int status;//下载状态
    private int percent;//下载百分比
    private int playStatus;//播放状态

    private String courseId;//当前课节所属课程的id
    private String courseWareBean;//当前课节的json
    private String courseBean;//当前课程的json
    private int courseCount;//总课节数
    private String years;//课节年份
    private String userId;//用户id

    public String getEffectiveStudyTime() {
        return effectiveStudyTime;
    }

    public void setEffectiveStudyTime(String effectiveStudyTime) {
        this.effectiveStudyTime = effectiveStudyTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getDbId() {
        return dbId;
    }

    public void setDbId(int dbId) {
        this.dbId = dbId;
    }

    public int getPlayStatus() {
        return playStatus;
    }

    public void setPlayStatus(int playStatus) {
        this.playStatus = playStatus;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    public String getVideoID() {
        return videoID;
    }

    public void setVideoID(String videoID) {
        this.videoID = videoID;
    }

    public String getVideoName() {
        return videoName;
    }

    public void setVideoName(String videoName) {
        this.videoName = videoName;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getVideoLen() {
        return videoLen;
    }

    public void setVideoLen(String videoLen) {
        this.videoLen = videoLen;
    }

    public String getVideoLastPlayTime() {
        return videoLastPlayTime;
    }

    public void setVideoLastPlayTime(String videoLastPlayTime) {
        this.videoLastPlayTime = videoLastPlayTime;
    }

    public String getJyUrl() {
        return jyUrl;
    }

    public void setJyUrl(String jyUrl) {
        this.jyUrl = jyUrl;
    }

    public String getCourseWareBean() {
        return courseWareBean;
    }

    public void setCourseWareBean(String courseWareBean) {
        this.courseWareBean = courseWareBean;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getCourseBean() {
        return courseBean;
    }

    public void setCourseBean(String courseBean) {
        this.courseBean = courseBean;
    }

    public int getCourseCount() {
        return courseCount;
    }

    public void setCourseCount(int courseCount) {
        this.courseCount = courseCount;
    }

    public String getYears() {
        return years;
    }

    public void setYears(String years) {
        this.years = years;
    }
}
