package com.bjjy.mainclient.phone.view.main;

public interface OnTabReselectListener {
    void onTabReselect();
}