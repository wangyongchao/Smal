package com.bjjy.mainclient.phone.event;

/**
 * Created by wyc on 9/4/15.
 */
public class StatusEvent {
    public boolean isRefresh;
    private static final String TAG = "StatusEvent";
    public StatusEvent(boolean isRefresh)
    {
        this.isRefresh = isRefresh;
    }
}
