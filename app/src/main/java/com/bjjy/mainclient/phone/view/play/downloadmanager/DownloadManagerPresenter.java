package com.bjjy.mainclient.phone.view.play.downloadmanager;

import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.bjjy.mainclient.phone.app.AppConfig;
import com.bjjy.mainclient.phone.download.db.DownloadDB;
import com.bjjy.mainclient.phone.download.db.PlayParamsDB;
import com.bjjy.mainclient.phone.view.exam.db.ExamPaperDB;
import com.dongao.mainclient.model.bean.play.CourseChapter;
import com.dongao.mainclient.model.bean.play.CourseWare;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.mvp.BasePersenter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static com.dongao.mainclient.model.common.Constants.STATE_DownLoaded;
import static com.dongao.mainclient.model.common.Constants.STATE_DownLoading;
import static com.dongao.mainclient.model.common.Constants.STATE_Error;
import static com.dongao.mainclient.model.common.Constants.STATE_Pause;
import static com.dongao.mainclient.model.common.Constants.STATE_Waiting;

/**
 * Created by yunfei on 2016/11/29.
 */

public class DownloadManagerPresenter extends BasePersenter<DownLoadManagerView> implements AbsCoursListFragment.OnItemSelectNumChangedListener {

    //下载数据库
    private DownloadDB downloadDB;

    private PlayParamsDB playParamsDB;

    private String userId;

    //
    private List<CourseWare> courseWareList;
    private List<CourseChapter> courseChapterList;
    //多选用
    private HashSet<CourseWare> courseWaresChecked;

    //单选
    private CourseWare downLoadCourseWare;

    //多选用
    private int canDownLoadNum = 0; //可下载总数
    private ExamPaperDB examPaperDB;

    public HashSet<CourseWare> getCourseWaresChecked() {
        return courseWaresChecked;
    }

    //防止RxJava 内存泄漏
    protected CompositeSubscription subscription = new CompositeSubscription();

    protected void addSub(Subscription sub) {
        if (sub != null && !sub.isUnsubscribed()) {
            subscription.add(sub);
        }
    }


    @Override
    public void attachView(DownLoadManagerView mvpView) {
        super.attachView(mvpView);
        downloadDB = new DownloadDB(mvpView.context());
        examPaperDB = new ExamPaperDB(mvpView.context());
        playParamsDB = new PlayParamsDB(mvpView.context());
        userId = SharedPrefHelper.getInstance(getMvpView().context()).getUserId();
        courseWaresChecked = new HashSet<>();
    }

    @Override
    public void detachView() {
        if (subscription.hasSubscriptions()) {
            subscription.unsubscribe();
        }
        super.detachView();
    }

    public void setCourseWareList(List<CourseWare> courseWareList) {
        this.courseWareList = courseWareList;
    }

    public void setCourseChapterList(List<CourseChapter> courseChapterList) {
        this.courseChapterList = courseChapterList;
    }

    @Override
    public void getData() {
        if (courseWareList != null) {

            Subscription subscribe = setCourseWareDownLoadState(courseWareList)
                    .compose(this.<List<CourseWare>>applySchedulers())
                    .subscribe(new Action1<List<CourseWare>>() {
                        @Override
                        public void call(List<CourseWare> list) {
                            getMvpView().hideLoading();
                            getMvpView().setCourseWareList(list);
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            getMvpView().hideLoading();
                            getMvpView().setCourseWareList(courseWareList);
                        }
                    });
            addSub(subscribe);
        }
        if (courseChapterList != null) {
            Subscription subscribe = setCourseChapterDownLoadState(courseChapterList)
                    .compose(this.<List<CourseChapter>>applySchedulers())
                    .subscribe(new Action1<List<CourseChapter>>() {
                        @Override
                        public void call(List<CourseChapter> courseChapters) {
                            getMvpView().hideLoading();
                            getMvpView().setCourseChapterList(courseChapters);
                        }
                    });
            addSub(subscribe);
        }

    }

    //查询数据库设置 课程 下载状态
    public Observable<List<CourseWare>> setCourseWareDownLoadState(final List<CourseWare> list) {
        return Observable.create(new Observable.OnSubscribe<List<CourseWare>>() {
            @Override
            public void call(Subscriber<? super List<CourseWare>> subscriber) {
                //异步查询数据库
                setCourseWareListDownloadState(list);
                subscriber.onNext(list);
                subscriber.onCompleted();
            }
        });
    }

    public Observable<List<CourseChapter>> setCourseChapterDownLoadState(final List<CourseChapter> list) {
        return Observable.create(new Observable.OnSubscribe<List<CourseChapter>>() {
            @Override
            public void call(Subscriber<? super List<CourseChapter>> subscriber) {
                setCourseChapterListDownLoadState(list);
                subscriber.onNext(list);
                subscriber.onCompleted();
            }
        });
    }

    private void setCourseChapterListDownLoadState(List<CourseChapter> list) {
        for (int i = 0; i < list.size(); i++) {
            setCourseWareListDownloadState(list.get(i).getMobileCourseWareList());
        }
    }

    private void setCourseWareListDownloadState(List<CourseWare> list) {

        Iterator<CourseWare> iterator = list.iterator();
        while (iterator.hasNext()) {
            CourseWare courseWare = iterator.next();
            if (courseWare.getCwId().equals("kehouzuoye")) {
                iterator.remove();
            } else {
                setDownLoadState(courseWare);
            }
        }


    }

    //查询 当前课程 下载状态
    private void setDownLoadState(CourseWare cw) {
        int state = downloadDB.getState(SharedPrefHelper.getInstance(getMvpView().context()).getUserId(), cw.getClassId(), cw.getCwId());
        cw.setState(state);
        if (!isAddDownloadDB(state))
            //统计所有可以下载的数量
            canDownLoadNum++;
    }


    @Override
    public void setData(String obj) {
        try {
            //获取下载路径
            getMvpView().hideLoading();
            JSONObject object = new JSONObject(obj);
            if (object.getInt("code") == 1000) {
                JSONObject body = object.getJSONObject("body");
                JSONObject video = body.getJSONObject("video");
                JSONObject sd = video.getJSONObject("sd");
                JSONArray one = sd.optJSONArray("1.0");
                String handOut = body.optString("handOut","");
                if (one != null && one.length() > 0) {
                    String url = one.getString(0);
                    downLoadCourseWare.setMobileDownloadUrl(url);
                    downLoadCourseWare.setMobileLectureUrl(handOut);
                    downLoadCourseWare.setCwBean(JSON.toJSONString(downLoadCourseWare));
                    AppConfig.getAppConfig(getMvpView().context()).download(downLoadCourseWare);
                    downLoadCourseWare.setState(STATE_DownLoading);
                    getMvpView().downloadingHit();
                    getMvpView().updateList();

                    JSONObject cipherkeyVo = body.getJSONObject("cipherkeyVo");
                    String app = cipherkeyVo.optString("app");
                    String type = cipherkeyVo.optString("type");
                    String vid = cipherkeyVo.optString("vid");
                    String vkey = cipherkeyVo.optString("key");
                    String vcode = cipherkeyVo.optString("code");
                    String message = cipherkeyVo.optString("message");
                    playParamsDB.add(userId, downLoadCourseWare.getCwId(), app, type, vid, vkey, vcode, message);
                } else {
                    getMvpView().showError("请求下载地址失败！");
                }

            } else {
                getMvpView().showError("请求下载地址失败！");
            }
        } catch (JSONException e) {
            getMvpView().showError("请求下载地址失败！");
        }

    }

    @Override
    public void onItemSelectNumChanged() {
        if (courseWaresChecked.size() == canDownLoadNum) {
            //已经全选
            getMvpView().showClearAllText();
        } else {
            //非全选
            getMvpView().showSelectAllText();
        }
        getMvpView().updateList();
        getMvpView().setDownloadNum(courseWaresChecked.size());
    }


    public void download() {
        //点击下载
        if (courseWaresChecked.size() == 0) {
            getMvpView().showNoSelectHit();
            return;
        }

    }

    public void clearAll() {
        courseWaresChecked.clear();
        onItemSelectNumChanged();
    }

    public void selectAll() {
        courseWaresChecked.clear();
        if (courseWareList != null) {
            addSub(selectCourseObserable(courseWareList)
                    .subscribe(getSubscriberSelectAll()));
        }

        if (courseChapterList != null) {
            addSub(Observable.from(courseChapterList)
                    .flatMap(new Func1<CourseChapter, Observable<CourseWare>>() {
                        @Override
                        public Observable<CourseWare> call(CourseChapter courseChapter) {
                            return selectCourseObserable(courseChapter.getMobileCourseWareList());
                        }
                    }).subscribe(getSubscriberSelectAll()));
        }
    }


    private Subscriber<CourseWare> getSubscriberSelectAll() {
        return new Subscriber<CourseWare>() {
            @Override
            public void onCompleted() {
                onItemSelectNumChanged();
            }

            @Override
            public void onError(Throwable e) {
                Log.e("yunfei", e.getMessage());
            }

            @Override
            public void onNext(CourseWare courseWare) {
                courseWaresChecked.add(courseWare);
            }
        };
    }


    private Observable<CourseWare> selectCourseObserable(List<CourseWare> list) {
        return Observable.from(list) //遍历 数组
                .filter(new Func1<CourseWare, Boolean>() {
                    @Override
                    public Boolean call(CourseWare courseWare) { //添加过滤条件
                        if (courseWare.getCwId().equals("kehouzuoye") || isAddDownloadDB(courseWare.getState()))
                            return false;
                        return true;
                    }
                });
    }


    public boolean isAddDownloadDB(int state) {
        if (state == STATE_DownLoading
                || state == STATE_Pause
                || state == STATE_Error
                || state == STATE_DownLoaded
                || state == STATE_Waiting)
            return true;
        return false;
    }


    private <T> Observable.Transformer<T, T> applySchedulers() {
        return new Observable.Transformer<T, T>() {
            @Override
            public Observable<T> call(Observable<T> tObservable) {
                return tObservable.subscribeOn(Schedulers.io()) //查询数据库使用 io线程
                        .doOnSubscribe(new Action0() {
                            @Override
                            public void call() {
                                getMvpView().hideLoading(); // 需要在主线程执行
                            }
                        })
                        .subscribeOn(AndroidSchedulers.mainThread()) // 指定主线程
                        .observeOn(AndroidSchedulers.mainThread());
            }
        };
    }

    public void setOnCoursWareListItemClick(int position) {
        downLoadCourseWare = courseWareList.get(position);

        //请求下载数据
        getPlayInfo(downLoadCourseWare);
    }

    public void setOnChapterChildClick(int groupPosition, int childPosition) {
        downLoadCourseWare = courseChapterList.get(groupPosition).getMobileCourseWareList().get(childPosition);
        //请求下载数据
        getPlayInfo(downLoadCourseWare);
    }

    public void getPlayInfo(CourseWare courseWare) {
        getMvpView().showLoading();

//        apiModel.getData(ApiClient.getClient().getCoursePlayInfo(ParamsUtils.getInstance(getMvpView().context()).getPlayInfo(courseWare.getCwId())));
        downloadCourse();
    }

    private void downloadCourse() {
        downLoadCourseWare.setCwBean(JSON.toJSONString(downLoadCourseWare));
        AppConfig.getAppConfig(getMvpView().context()).download(downLoadCourseWare);
        downLoadCourseWare.setState(STATE_DownLoading);
        getMvpView().downloadingHit();
        getMvpView().updateList();
        getMvpView().hideLoading();
    }

    private String typeId = "1003";

}
