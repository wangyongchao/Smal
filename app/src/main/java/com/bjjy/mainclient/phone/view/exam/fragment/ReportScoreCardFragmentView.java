package com.bjjy.mainclient.phone.view.exam.fragment;

import android.content.Intent;
import android.view.View;
import com.dongao.mainclient.model.mvp.MvpView;
import com.bjjy.mainclient.phone.app.AppContext;

/**
 * Created by wyc on 2016/5/6.
 */
public interface ReportScoreCardFragmentView extends MvpView {
    void setView(View view);
    void finishActivity();
    AppContext getAppContext();
    Intent getTheIntent();
}
