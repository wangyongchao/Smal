package com.bjjy.mainclient.phone.view.play.fragment.uploadBean;


import java.io.Serializable;
import java.util.List;

/**
 * Created by fengzongwei on 2016/5/30 0030.
 * 讲
 */
public class UploadVideo implements Serializable{

    private String userCode;
    private List<VideoLogs> listenDtos;

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public List<VideoLogs> getListenDtos() {
        return listenDtos;
    }

    public void setListenDtos(List<VideoLogs> listenDtos) {
        this.listenDtos = listenDtos;
    }
}
