package com.bjjy.mainclient.phone.view.download.util;

public class DownloadException extends Exception {

	// 1 M3U8文件解析错误
	// 2 M3U8 404
	// 3 M3U8 网络错误

	public static final int FILERESOLVE_ERROR = 0;
	public static final int NETWORK_FILE_404_ERROR = 1;
	public static final int NETWORK_ERROR = 2;
	public static final int FILEWRITE_ERROR = 3;
	public static final int STROAGE_NO_SPACE_ERROR = 4;
	private int code;

	private static final long serialVersionUID = 8341866403057223643L;

	public DownloadException(String msg, int code) {
		super(msg);
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

}
