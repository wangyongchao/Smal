package com.bjjy.mainclient.phone.scanning;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.bjjy.mainclient.phone.app.BaseActivity;
import com.bjjy.mainclient.phone.view.play.audition.AuditionPlayActivity;
import com.dongao.mainclient.model.bean.play.CourseWare;
import com.bjjy.mainclient.phone.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by dell on 2016/6/17.
 */
public class CaptureResultActivity extends BaseActivity implements AdapterView.OnItemClickListener {

    @Bind(R.id.top_title_left)
    ImageView topTitleLeft;
    @Bind(R.id.top_title_right)
    ImageView topTitleRight;
    @Bind(R.id.top_title_text)
    TextView topTitleText;
    @Bind(R.id.tv_right)
    TextView tvRight;

    @Bind(R.id.capture_ls)
    ListView captureLs;
    @Bind(R.id.capture_nocollect)
    LinearLayout captureNocollect;

    private CapturesultAdapter adapter;
    private List<CourseWare> CourseWares;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capturesult);
        ButterKnife.bind(this);

        initView();
        initData();
    }

    @Override
    public void initView() {
        adapter = new CapturesultAdapter(this);
        topTitleLeft.setImageResource(R.drawable.nav_left);
        topTitleLeft.setVisibility(View.VISIBLE);
        topTitleLeft.setOnClickListener(this);
        topTitleText.setText("辅导书知识点课程");

        captureLs.setOnItemClickListener(this);
    }

    @Override
    public void initData() {
        CourseWares = (List<CourseWare>) getIntent().getSerializableExtra("list");
        if (CourseWares != null && CourseWares.size() > 0) {

            adapter.setList(CourseWares);
            captureLs.setAdapter(adapter);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.top_title_left:
                finish();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(CaptureResultActivity.this, AuditionPlayActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("cw", CourseWares.get(position));
        intent.putExtras(bundle);
        startActivity(intent);
    }
}
