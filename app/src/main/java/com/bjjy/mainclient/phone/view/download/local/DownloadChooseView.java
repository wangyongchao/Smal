package com.bjjy.mainclient.phone.view.download.local;

import android.content.Intent;

import com.bjjy.mainclient.phone.view.download.local.bean.YearInfo;
import com.bjjy.mainclient.phone.view.studybar.view.RefreshLayout;
import com.dongao.mainclient.model.mvp.MvpView;

/**
 * Created by wyc on 2016/6/6.
 */
public interface DownloadChooseView extends MvpView{
    void initAdapter();
    void showContentView(int type);//0:正常 1：网络错误 2：没有数据
    boolean isRefreshNow();//是否正在刷新
    RefreshLayout getRefreshLayout();
    void showTopTitle(String title);
    void setNoDataMoreShow(boolean isShow);
    Intent getTheIntent();
    void finishActivity();
    void showCurrentYear( YearInfo currYear);
    void hideYearPop(boolean isHide);
    void setCancelText(String string);
    void setDeleteText(String string);
}
