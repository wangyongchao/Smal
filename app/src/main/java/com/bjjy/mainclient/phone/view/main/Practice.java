package com.bjjy.mainclient.phone.view.main;

import java.io.Serializable;

public class Practice implements Serializable{

    /**
     * 考试
     */
    private String id;
    private int userIstoDo;
    private String name;
    private String examType;
    private String examId;
    private String subjectId;
    private String sectionId;
    private String contentType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getUserIstoDo() {
        return userIstoDo;
    }

    public void setUserIstoDo(int userIstoDo) {
        this.userIstoDo = userIstoDo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExamType() {
        return examType;
    }

    public void setExamType(String examType) {
        this.examType = examType;
    }

    public String getExamId() {
        return examId;
    }

    public void setExamId(String examId) {
        this.examId = examId;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }


}
