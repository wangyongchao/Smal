package com.bjjy.mainclient.phone.view.question;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bjjy.mainclient.phone.event.DeleteQuestionCollection;
import com.dongao.libs.dongaoumengpushlib.constants.PushConstants;
import com.dongao.mainclient.core.util.DateUtil;
import com.dongao.mainclient.model.bean.question.Question;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.bjjy.mainclient.persenter.MyQuestionListPersenter;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseActivity;
import com.bjjy.mainclient.phone.utils.NetworkUtil;
import com.bjjy.mainclient.phone.view.exam.ExamActivity;
import com.bjjy.mainclient.phone.view.question.utils.QuestionParserUtil;
import com.bjjy.mainclient.phone.view.user.utils.UserViewManager;
import com.bjjy.mainclient.phone.widget.DialogManager;
import com.bjjy.mainclient.phone.widget.EmptyViewLayout;
import com.bjjy.mainclient.phone.widget.RefreshScrollView;
import com.bjjy.mainclient.phone.widget.ScrollViewFooter;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.umeng.analytics.MobclickAgent;

import org.greenrobot.eventbus.EventBus;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by fengzongwei on 2016/5/3 0003.
 */
public class ExamRecommQuestionListActivity extends BaseActivity implements MyQuestionListView,RefreshScrollView.OnRefreshScrollViewListener{
    @Bind(R.id.top_title_left)
    ImageView imageView_back;
    @Bind(R.id.top_title_text)
    TextView textView_title;
    @Bind(R.id.myquestion_scroll)
    RefreshScrollView refreshScrollView;
    @Bind(R.id.my_questiont_hintImg)
    ImageView imageView_hint;
    @Bind(R.id.my_ques_list_lodingBody)
    RelativeLayout relativeLayout_hint_body;
    @Bind(R.id.animation_progress_img)
    ImageView imageView_loading;
    @Bind(R.id.my_ques_list_picBody)
    LinearLayout linearLayout_pic_body;
    private EmptyViewLayout emptyViewLayout;

    private DisplayImageOptions options = new DisplayImageOptions.Builder()
            .showImageOnLoading(R.drawable.ic_empty) // resource or drawable
            .showImageForEmptyUri(R.drawable.ic_empty) // resource or drawable
            .showImageOnFail(R.drawable.ic_error) // resource or drawable
            .resetViewBeforeLoading(false)  // default
            .cacheInMemory(true) // default
            .cacheOnDisk(false) // default
            .build();

    //当前执行动画的控件
    private LinearLayout linearLayout_btBody,linearLayout_zhuiwenContainer;

    private final int maxHeight = 360;
    private static final String encoding = "utf-8";
    private static final String mimeType = "text/html";
    private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private String pushExamId,questionAnswerId,examId,subjectId,userId,examinationQuestionId;
    // 一次点击是否结束
    private boolean isClickOver =true;
    //点击动画是否完成
    private boolean isAnimationOver = false;
    //所点击按钮的标示
    private String compareId = null;
    //进行展开还是收起动作的标示
    private int tag;
    private int modify_ques_position;
    private int childParentWidght;
    private int zhuiwenBtBodyWidth,copyzhuiwenBtBodyWidth;
    private LinearLayout.LayoutParams layoutParams_content;
    private LinearLayout.LayoutParams layoutParams_contentAll = new LinearLayout.LayoutParams
            (LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

    private ArrayList<View> viewArrayList = new ArrayList<>();
    private ArrayList<LinearLayout> zhuiwenContainerList = new ArrayList<>();
    private ArrayList<LinearLayout> zhuiwenByBodyList = new ArrayList<>();

    private List<Question> lisMyQuestionObjs;//存放当前页面所有数据的集合

    private MyQuestionListPersenter myQuestionListPersenter;

    private UserViewManager userViewManager;

    private String bookList;//根据科目获取的章节接口

    private Handler handler = new Handler(){
        public void handleMessage(Message msg) {
            //展示的追问的根布局现在为隐藏状态
            if(msg.what == 1){
                if(linearLayout_btBody == null){
                    for(int i=0;i<zhuiwenByBodyList.size();i++){
                        String id = (String)zhuiwenByBodyList.get(i).getTag();
                        if(id.equals(compareId)){
                            linearLayout_btBody = zhuiwenByBodyList.get(i);
                        }
                    }
                    for(int i=0;i<zhuiwenContainerList.size();i++){
                        String id = (String)zhuiwenContainerList.get(i).getTag();
                        if(id.equals(compareId)){
                            linearLayout_zhuiwenContainer = zhuiwenContainerList.get(i);
                        }
                    }
                }
                if (copyzhuiwenBtBodyWidth == 0) {
                    copyzhuiwenBtBodyWidth = zhuiwenBtBodyWidth;
                }
                if (copyzhuiwenBtBodyWidth < childParentWidght && linearLayout_btBody != null) {
                    copyzhuiwenBtBodyWidth = copyzhuiwenBtBodyWidth + childParentWidght / 100;
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(copyzhuiwenBtBodyWidth, RelativeLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                    linearLayout_btBody.setLayoutParams(layoutParams);
                } else {
                    if (linearLayout_zhuiwenContainer != null)
                        linearLayout_zhuiwenContainer.setVisibility(View.VISIBLE);
                    if(linearLayout_zhuiwenContainer!=null && linearLayout_zhuiwenContainer.getChildCount()==0){
                        List<Question> list = null;
                        for(int i=0;i<lisMyQuestionObjs.size();i++){
                            if(lisMyQuestionObjs.get(i).getAskId().equals(compareId))
                                list = lisMyQuestionObjs.get(i).getChildQuestions();
                        }
                        addZhuiWenItem(list,linearLayout_zhuiwenContainer);
                    }
                    copyzhuiwenBtBodyWidth = 0;
                    isClickOver = true;
                    isAnimationOver = true;
                    linearLayout_btBody = null;
                    linearLayout_zhuiwenContainer = null;
                    compareId = null;
                }
            }else if(msg.what == 2){//展示的追问的根布局现在为显示状态
                if(linearLayout_btBody == null){
                    for(int i=0;i<zhuiwenByBodyList.size();i++){
                        String id = (String)zhuiwenByBodyList.get(i).getTag();
                        if(id.equals(compareId)){
                            linearLayout_btBody = zhuiwenByBodyList.get(i);
                        }
                    }
//					linearLayout_zhuiwenContainer = findLinearById(compareId);
                    for(int i=0;i<zhuiwenContainerList.size();i++){
                        String id = (String)zhuiwenContainerList.get(i).getTag();
                        if(id.equals(compareId)){
                            linearLayout_zhuiwenContainer = zhuiwenContainerList.get(i);
                        }
                    }
                }
                if (copyzhuiwenBtBodyWidth == 0) {
                    copyzhuiwenBtBodyWidth = childParentWidght;
                }
                if (copyzhuiwenBtBodyWidth >= (zhuiwenBtBodyWidth+10) && linearLayout_btBody != null) {
                    copyzhuiwenBtBodyWidth = copyzhuiwenBtBodyWidth - childParentWidght / 100;
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(copyzhuiwenBtBodyWidth, RelativeLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                    linearLayout_btBody.setLayoutParams(layoutParams);
                } else {
                    if (linearLayout_zhuiwenContainer != null)
                        linearLayout_zhuiwenContainer.setVisibility(View.GONE);
                    copyzhuiwenBtBodyWidth = 0;
                    isClickOver = true;
                    isAnimationOver = true;
                    linearLayout_btBody = null;
                    linearLayout_zhuiwenContainer = null;
                    compareId = null;
                }
            }
//            else if (msg.what == TaskType.TS_ISCONNECT_BACKGROUND){
//                String list_str = (String) msg.obj;
//                initListView(list_str);
//            }
        };
    };

    private int page = 1;//加载数据的页数
    private int totalPage;//数据总页数
    private boolean isLoadmore = false;//当前是否是进行的加载更多请求

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question_mian);
        ButterKnife.bind(this);
        myQuestionListPersenter = new MyQuestionListPersenter();
        myQuestionListPersenter.attachView(this);
        initData();
        initView();
        ImageLoaderConfiguration configuration = ImageLoaderConfiguration
                .createDefault(this);
        ImageLoader.getInstance().init(configuration);
    }

    @Override
    public void initView() {
        AnimationDrawable animationDrawable = (AnimationDrawable)imageView_loading.getDrawable();
        animationDrawable.start();
        refreshScrollView.setOnRefreshScrollViewListener(this);
//        emptyViewLayout = new EmptyViewLayout(this,refreshScrollView);
//        emptyViewLayout.setEmptyButtonClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getData();
//            }
//        });
//        ((ImageView)findViewById(R.id.top_title_right)).setImageResource(R.drawable.my_question_title_add);
//        findViewById(R.id.top_title_right).setVisibility(View.VISIBLE);
        ((ImageView)findViewById(R.id.top_title_left)).setImageResource(R.drawable.back);
        findViewById(R.id.top_title_left).setVisibility(View.VISIBLE);
        ((TextView)findViewById(R.id.top_title_text)).setText("推荐答疑");
        if(!SharedPrefHelper.getInstance(this).getMainTypeId().equals("4")){
            ((ImageView)findViewById(R.id.top_title_right)).setImageResource(R.drawable.my_question_title_add);
            if(questionAnswerId == null)
                findViewById(R.id.top_title_right).setVisibility(View.VISIBLE);
            else{
                findViewById(R.id.top_title_right).setVisibility(View.INVISIBLE);
                ((TextView)findViewById(R.id.top_title_text)).setText("我的收藏");
            }

        }
        else
            findViewById(R.id.top_title_right).setVisibility(View.INVISIBLE);
    }

    @Override
    public void initData() {
        examinationQuestionId = getIntent().getStringExtra("examinationQuestionId");
        questionAnswerId = getIntent().getStringExtra("questionAnswerId");
//        myQuestionListPersenter.getSectionsBySubjectId();
        getData();
    }

    /**
     * 加载数据总方法
     */
    private void getData(){
        SimpleDateFormat sm=new SimpleDateFormat("MM-dd HH:mm:ss");
//        SharedPrefHelper.getInstance(this).setMyQuestionListTime(userId, sm.format(new Date()));
//        refreshScrollView.setRefreshTime(SharedPrefHelper.getInstance().getMyQuestionListTime(userId));
        if(NetworkUtil.isNetworkAvailable(appContext)) {
            if(lisMyQuestionObjs == null || lisMyQuestionObjs.size()==0) {
                relativeLayout_hint_body.setVisibility(View.VISIBLE);
                imageView_loading.setVisibility(View.VISIBLE);
                linearLayout_pic_body.setVisibility(View.GONE);
            }
            if (questionAnswerId==null || questionAnswerId.isEmpty()){//试题推荐答疑 （全部的）
                myQuestionListPersenter.getExamRecommQuestion();
            }else{//收藏的推荐答疑详情
                refreshScrollView.setEnableRefresh(false);
                refreshScrollView.setFooterViewGone();
                refreshScrollView.setHeaderViewGone();
                myQuestionListPersenter.getQuestionDetail(questionAnswerId);
            }
        }else{
            relativeLayout_hint_body.setVisibility(View.VISIBLE);
            imageView_loading.setVisibility(View.GONE);
            linearLayout_pic_body.setVisibility(View.VISIBLE);
            imageView_hint.setImageResource(R.drawable.pic_network2);
//            emptyViewLayout.showNetErrorView();
//            showError("网路异常，点击重试");
        }
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @OnClick(R.id.top_title_right) void addQuestion(){
//        Intent intent = new Intent(MyQuestionListActivity.this,AddQuestionActivity.class);
//        intent.putExtra("examId","711");
//        intent.putExtra("subjectId","71901");
////        intent.putExtra("bookList","[{\"alias\":\"轻一\",\"bookId\":\"7035593\",\"bookName\":\"轻松过关一《2016年会计专业技术资格考试应试指导及全真模拟测试》\",\"checked\":false,\"dbId\":0,\"id\":\"1\",\"isActive\":\"1\",\"sectionList\":[{\"bookId\":\"7035593\",\"checked\":false,\"courseBookSectionId\":\"20358\",\"dbid\":0,\"description\":\"\",\"id\":\"20380\",\"name\":\"第一章 资产\",\"sindex\":1,\"specialtyId\":\"\",\"status\":2,\"subjectId\":\"71901\",\"versionno\":\"\"},{\"bookId\":\"7035593\",\"checked\":false,\"courseBookSectionId\":\"20360\",\"dbid\":0,\"description\":\"\",\"id\":\"20381\",\"name\":\"第二章 负债\",\"sindex\":2,\"specialtyId\":\"\",\"status\":2,\"subjectId\":\"71901\",\"versionno\":\"\"},{\"bookId\":\"7035593\",\"checked\":false,\"courseBookSectionId\":\"20362\",\"dbid\":0,\"description\":\"\",\"id\":\"20382\",\"name\":\"第三章 所有者权益\",\"sindex\":3,\"specialtyId\":\"\",\"status\":2,\"subjectId\":\"71901\",\"versionno\":\"\"},{\"bookId\":\"7035593\",\"checked\":false,\"courseBookSectionId\":\"20364\",\"dbid\":0,\"description\":\"\",\"id\":\"20383\",\"name\":\"第四章 收入\",\"sindex\":4,\"specialtyId\":\"\",\"status\":2,\"subjectId\":\"71901\",\"versionno\":\"\"},{\"bookId\":\"7035593\",\"checked\":false,\"courseBookSectionId\":\"20366\",\"dbid\":0,\"description\":\"\",\"id\":\"20384\",\"name\":\"第五章 费用\",\"sindex\":5,\"specialtyId\":\"\",\"status\":2,\"subjectId\":\"71901\",\"versionno\":\"\"},{\"bookId\":\"7035593\",\"checked\":false,\"courseBookSectionId\":\"20368\",\"dbid\":0,\"description\":\"\",\"id\":\"20385\",\"name\":\"第六章 利润\",\"sindex\":6,\"specialtyId\":\"\",\"status\":2,\"subjectId\":\"71901\",\"versionno\":\"\"},{\"bookId\":\"7035593\",\"checked\":false,\"courseBookSectionId\":\"20370\",\"dbid\":0,\"description\":\"\",\"id\":\"20386\",\"name\":\"第七章 财务报告\",\"sindex\":7,\"specialtyId\":\"\",\"status\":2,\"subjectId\":\"71901\",\"versionno\":\"\"},{\"bookId\":\"7035593\",\"checked\":false,\"courseBookSectionId\":\"20372\",\"dbid\":0,\"description\":\"\",\"id\":\"20387\",\"name\":\"第八章 产品成本核算\",\"sindex\":8,\"specialtyId\":\"\",\"status\":2,\"subjectId\":\"71901\",\"versionno\":\"\"},{\"bookId\":\"7035593\",\"checked\":false,\"courseBookSectionId\":\"20374\",\"dbid\":0,\"description\":\"\",\"id\":\"20388\",\"name\":\"第九章 产品成本计算与分析\",\"sindex\":9,\"specialtyId\":\"\",\"status\":2,\"subjectId\":\"71901\",\"versionno\":\"\"},{\"bookId\":\"7035593\",\"checked\":false,\"courseBookSectionId\":\"20376\",\"dbid\":0,\"description\":\"\",\"id\":\"20389\",\"name\":\"第十章 事业单位会计基础\",\"sindex\":10,\"specialtyId\":\"\",\"status\":2,\"subjectId\":\"71901\",\"versionno\":\"\"},{\"bookId\":\"7035593\",\"checked\":false,\"courseBookSectionId\":\"16931\",\"dbid\":0,\"description\":\"\",\"id\":\"20390\",\"name\":\"跨章节不定项选择题演练\",\"sindex\":11,\"specialtyId\":\"\",\"status\":2,\"subjectId\":\"71901\",\"versionno\":\"\"},{\"bookId\":\"7035593\",\"checked\":false,\"courseBookSectionId\":\"16931\",\"dbid\":0,\"description\":\"\",\"id\":\"20391\",\"name\":\"全真模拟测试题（一）\",\"sindex\":12,\"specialtyId\":\"\",\"status\":2,\"subjectId\":\"71901\",\"versionno\":\"\"},{\"bookId\":\"7035593\",\"checked\":false,\"courseBookSectionId\":\"16931\",\"dbid\":0,\"description\":\"\",\"id\":\"20392\",\"name\":\"全真模拟测试题（二）\",\"sindex\":13,\"specialtyId\":\"\",\"status\":2,\"subjectId\":\"71901\",\"versionno\":\"\"},{\"bookId\":\"7035593\",\"checked\":false,\"courseBookSectionId\":\"16931\",\"dbid\":0,\"description\":\"\",\"id\":\"20393\",\"name\":\"全真模拟测试题（三）\",\"sindex\":14,\"specialtyId\":\"\",\"status\":2,\"subjectId\":\"71901\",\"versionno\":\"\"},{\"bookId\":\"7035593\",\"checked\":false,\"courseBookSectionId\":\"16931\",\"dbid\":0,\"description\":\"\",\"id\":\"20394\",\"name\":\"附录1\",\"sindex\":15,\"specialtyId\":\"\",\"status\":2,\"subjectId\":\"71901\",\"versionno\":\"\"},{\"bookId\":\"7035593\",\"checked\":false,\"courseBookSectionId\":\"16931\",\"dbid\":0,\"description\":\"\",\"id\":\"20395\",\"name\":\"附录2\",\"sindex\":16,\"specialtyId\":\"\",\"status\":2,\"subjectId\":\"71901\",\"versionno\":\"\"},{\"bookId\":\"7035593\",\"checked\":false,\"courseBookSectionId\":\"16931\",\"dbid\":0,\"description\":\"\",\"id\":\"20396\",\"name\":\"其他章节\",\"sindex\":17,\"specialtyId\":\"\",\"status\":2,\"subjectId\":\"71901\",\"versionno\":\"\"}],\"status\":\"2\",\"subjectId\":\"71901\"}]");
//        intent.putExtra("bookList",bookList);
//        startActivity(intent);
//        Intent intent = new Intent(MyQuestionListActivity.this,MyQuestionModifyActivity.class);
//        startActivity(intent);
        if(SharedPrefHelper.getInstance(this).getMainTypeId().equals("4"))
            return;
        if(questionAnswerId==null){
//            showAnimProgress("校验您当前是否可以进行提问...");
            myQuestionListPersenter.isCanAsk(examinationQuestionId);
        }
        MobclickAgent.onEvent(ExamRecommQuestionListActivity.this, PushConstants.EXAM_TO_ASKQUESTION);
    }

    @OnClick(R.id.my_ques_list_picBody) void retry(){
        getData();
    }

    @OnClick(R.id.top_title_left) void back(){
//        Intent intent = new Intent(MyQuestionListActivity.this, UserActivity.class);
//        startActivity(intent);
        finish();
    }

    /**
     * 刷新scrollView的内容
     */
    private void freshContentView(final List<Question> questionList){
        LinearLayout.LayoutParams viewParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
        viewParams.setMargins(0,20,0,0);
        if(!isLoadmore){
            viewArrayList.clear();
            zhuiwenContainerList.clear();
            zhuiwenByBodyList.clear();
        }else{
            lisMyQuestionObjs.addAll(questionList);
        }
        for(int i=0;i<questionList.size();i++){
            View view = LayoutInflater.from(ExamRecommQuestionListActivity.this).inflate(R.layout.my_question_item,null);
            if(viewArrayList.size()>0)
                view.setLayoutParams(viewParams);
            final TextView textView_bookName,textView_queTime,textView_allContent,textView_anwTime,
                    textView_subjectName,textView_continue,textView_modify,textView_zhuiwenText,textView_go,textView_collect;
            final LinearLayout linearLayout_answerBody,linearLayout_answerContainer11,linearLayout_zhuiwenBtBody,linearLayout_go,linearLayout_continue,
                    linearLayout_modify,linearLayout_allContent,linearLayout_img_body,linearLayout_recom_bottom_body,linearLayout_recomm_goQuestion,linearLayout_recomm_collect;
            final ImageView imageView_continue,imageView_modify,imageView_allContent,imageView_look_continue_ask,imageView_zhuiwen,
                    imageView1,imageView2,imageView_from,imageView_go,imageView_collect;
            final HtmlTextView htmlTextView_anw,htmlTextView_que;
            final WebView webView_anw,webView_que;
            final RelativeLayout relativeLayout_zhuiwenBody;
            final LinearLayout linearLayout_function_body,linearLayout_recomm_function_body;
            linearLayout_recomm_function_body = (LinearLayout)view.findViewById(R.id.recomm_question_item_function_body);
            textView_go = (TextView)view.findViewById(R.id.recomm_question_item_go_tv);
            imageView_go = (ImageView)view.findViewById(R.id.recomm_question_item_go_img);
            imageView_collect = (ImageView)view.findViewById(R.id.recomm_question_item_continueAsk_img);
            textView_collect = (TextView)view.findViewById(R.id.recomm_question_item_continueAsk_text);
            if(questionAnswerId == null){
                linearLayout_recomm_function_body.setVisibility(View.GONE);
            }else{
                imageView_collect.setImageResource(R.drawable.icon_collected);
                textView_collect.setText("已收藏");
                textView_collect.setTextColor(Color.parseColor("#ff7666"));
            }
            linearLayout_function_body = (LinearLayout)view.findViewById(R.id.my_question_item_function_body);
            linearLayout_function_body.setVisibility(View.GONE);
            linearLayout_recomm_goQuestion = (LinearLayout)view.findViewById(R.id.recomm_question_item_goQuestion);
            linearLayout_recomm_goQuestion.setTag(i);
            linearLayout_recomm_collect = (LinearLayout)view.findViewById(R.id.recomm_question_item_continueAsk);
            linearLayout_recomm_collect.setTag(questionList.get(i).getAskId());
            imageView_from = (ImageView)view.findViewById(R.id.my_question_item_from_img);
            textView_subjectName = (TextView)view.findViewById(R.id.my_question_item_subjectName);
            linearLayout_img_body = (LinearLayout)view.findViewById(R.id.my_question_item_img_body);
            imageView1 = (ImageView)view.findViewById(R.id.my_question_item_img_1);
            imageView2 = (ImageView)view.findViewById(R.id.my_question_item_img_2);
            htmlTextView_que = (HtmlTextView)view.findViewById(R.id.my_question_item_queContentHtml);
            webView_que = (WebView)view.findViewById(R.id.my_question_item_queContentWeb);
            textView_queTime = (TextView)view.findViewById(R.id.my_question_item_queTime);
            textView_bookName = (TextView)view.findViewById(R.id.my_question_item_bookName);
            linearLayout_answerBody = (LinearLayout)view.findViewById(R.id.my_question_item_answerBody);
            htmlTextView_anw = (HtmlTextView)view.findViewById(R.id.my_question_item_anwContentHtml);
            webView_anw = (WebView)view.findViewById(R.id.my_question_item_anwContentWeb);
            textView_anwTime = (TextView)view.findViewById(R.id.my_question_item_answerTime);
            linearLayout_allContent = (LinearLayout)view.findViewById(R.id.my_question_item_allContent);
            linearLayout_allContent.setTag(i);
            textView_allContent = (TextView)view.findViewById(R.id.my_question_item_allContent_text);
            imageView_allContent = (ImageView)view.findViewById(R.id.my_question_item_allContent_img);
            relativeLayout_zhuiwenBody = (RelativeLayout)view.findViewById(R.id.my_question_item_zhuiwenBody);
            linearLayout_zhuiwenBtBody = (LinearLayout)view.findViewById(R.id.my_question_item_zhuiwenBtBody);
            linearLayout_zhuiwenBtBody.setTag(questionList.get(i).getAskId());
            imageView_zhuiwen = (ImageView)view.findViewById(R.id.my_question_item_zhuiwenBt);
            textView_zhuiwenText = (TextView)view.findViewById(R.id.my_question_item_zhuiwenBt_text);
            zhuiwenByBodyList.add(linearLayout_zhuiwenBtBody);
            imageView_look_continue_ask = (ImageView)view.findViewById(R.id.my_question_item_zhuiwenBt);
            imageView_look_continue_ask.setTag(questionList.get(i).getAskId());
            linearLayout_answerContainer11 = (LinearLayout)view.findViewById(R.id.my_question_item_zhuiwenContainer);
            linearLayout_answerContainer11.setTag(questionList.get(i).getAskId());
            linearLayout_go = (LinearLayout)view.findViewById(R.id.my_question_item_goQuestion);
            linearLayout_go.setTag(i);
            linearLayout_continue = (LinearLayout)view.findViewById(R.id.my_question_item_continueAsk);
            textView_continue = (TextView)view.findViewById(R.id.my_question_item_continueAsk_text);
            imageView_continue = (ImageView)view.findViewById(R.id.my_question_item_continueAsk_img);
            linearLayout_modify = (LinearLayout)view.findViewById(R.id.my_question_item_modify);
            textView_modify = (TextView)view.findViewById(R.id.my_question_item_modify_text);
            imageView_modify = (ImageView)view.findViewById(R.id.my_question_item_modify_img);

            /**
             * 只添加父问题
             */
            if(QuestionParserUtil.isParent(questionList.get(i).getParentId())){
                if(questionList.get(i).getQasTypeFlag().equals("2")){
                    textView_go.setTextColor(Color.parseColor("#DBDBDB"));
                    imageView_go.setImageResource(R.drawable.icon_subject_gray);
                }
                if(!questionList.get(i).getSectionName().isEmpty())
                    textView_bookName.setText(questionList.get(i).getSubjectName()+" "+questionList.get(i).getSectionName());
                else
                    textView_bookName.setVisibility(View.INVISIBLE);
                if(questionList.get(i).getQasTypeFlag().equals("2")){
                    textView_go.setTextColor(Color.parseColor("#DBDBDB"));
                    imageView_go.setImageResource(R.drawable.icon_subject_gray);
                }
                if(questionList.get(i).getImageUrlList()!=null && questionList.get(i).getImageUrlList().size()>0){
                    if(questionList.get(i).getImageUrlList().size() == 1){
                        imageView2.setVisibility(View.GONE);
                        ImageLoader.getInstance().displayImage(questionList.get(i).getImageUrlList().get(0), imageView1, options);
                    }else{
                        ImageLoader.getInstance().displayImage(questionList.get(i).getImageUrlList().get(0), imageView1, options);
                        ImageLoader.getInstance().displayImage(questionList.get(i).getImageUrlList().get(1), imageView2, options);
                    }
                }else{
                    linearLayout_img_body.setVisibility(View.GONE);
                }
                try{
                    textView_queTime.setText(DateUtil.twoDateDistanceForStudyBarList(format.parse(questionList.get(i).getCreateTime()).getTime()+""));
                }catch (Exception e){
                    e.printStackTrace();
                    textView_queTime.setText(questionList.get(i).getCreateTime());
                }
                textView_subjectName.setText(questionList.get(i).getSubjectName());
                if(questionList.get(i).getContent().contains("</td>")){
                    webView_que.setVisibility(View.VISIBLE);
                    htmlTextView_que.setVisibility(View.GONE);
                    webView_que.loadDataWithBaseURL("", "<font color='#808080' style='font-size:15px;'>" + (questionList.get(i).getContent()) + "</font>", mimeType, encoding, "");
                }else{
                    htmlTextView_que.setHtmlFromString("<font color='#808080' style='font-size:15px;'>" + (questionList.get(i).getContent()) + "</font>", new HtmlTextView.RemoteImageGetter());
                }
                if(!questionList.get(i).getReadStatus().equals("0")){
                    try{
                        textView_anwTime.setText(DateUtil.twoDateDistanceForStudyBarList(format.parse(questionList.get(i).getAnswerTime()).getTime() + ""));
                    }catch (Exception e){
                        e.printStackTrace();
                        textView_anwTime.setText(questionList.get(i).getAnswerTime());
                    }
                    if(questionList.get(i).getAnswer().contains("</td>")){
                        webView_anw.setVisibility(View.VISIBLE);
                        layoutParams_content = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,maxHeight);
                        layoutParams_content.setMargins(webView_anw.getLeft(),webView_anw.getTop(),webView_anw.getRight(),0);
                        layoutParams_contentAll.setMargins(webView_anw.getLeft(),webView_anw.getTop(),webView_anw.getRight(),0);
                        webView_anw.setLayoutParams(layoutParams_content);
                        questionList.get(i).setShowAllAns(1);
                        htmlTextView_anw.setVisibility(View.GONE);
                        webView_anw.loadDataWithBaseURL("", "<font color='#808080' style='font-size:15px;'>" + (questionList.get(i).getAnswer()) + "</font>", mimeType, encoding, "");
                    }else{
                        htmlTextView_anw.setTag(i);
                        htmlTextView_anw.setHtmlFromString("<font color='#808080' style='font-size:15px;'>" + (questionList.get(i).getAnswer()) + "</font>", new HtmlTextView.RemoteImageGetter());
                        if(layoutParams_content == null) {
                            layoutParams_content = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,maxHeight);
                            layoutParams_contentAll = (LinearLayout.LayoutParams) htmlTextView_anw.getLayoutParams();
                            layoutParams_content.setMargins(layoutParams_contentAll.leftMargin,layoutParams_contentAll.topMargin,layoutParams_contentAll.rightMargin, 0);
                        }
                        ViewTreeObserver viewTreeObserver = htmlTextView_anw.getViewTreeObserver();
                        viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                            @Override
                            public void onGlobalLayout() {
                                if (htmlTextView_anw.getVisibility() == View.VISIBLE && questionList.get((int) htmlTextView_anw.getTag()).isShowAllAns() == -1) {
                                    if (htmlTextView_anw.getHeight() >= maxHeight) {
                                        questionList.get((int) htmlTextView_anw.getTag()).setShowAllAns(1);
                                        htmlTextView_anw.setLayoutParams(layoutParams_content);
                                    } else {
                                        questionList.get((int) htmlTextView_anw.getTag()).setShowAllAns(0);
                                        linearLayout_allContent.setVisibility(View.GONE);
                                    }
                                }
                                htmlTextView_anw.getViewTreeObserver().removeGlobalOnLayoutListener(this);//没有此句话，此监听listerner会一直监听，从而影响之后的setLayoutParams
                            }
                        });
                    }
                    if(questionList.get(i).getChildAskList() == null || questionList.get(i).getChildAskList().size() == 0) {
                        relativeLayout_zhuiwenBody.setVisibility(View.GONE);
                    }else{
                        zhuiwenContainerList.add(linearLayout_answerContainer11);
                    }
//                    if(questionList.get(i).isCanAsk()){
////                        textView_isAnswer.setText("已回复");
////                        textView_isAnswer.setTextColor(Color.parseColor("#0ac7c4"));
//                        linearLayout_continue.setTag(i);
//                        textView_modify.setTextColor(Color.parseColor("#C2C2C2"));
//                        imageView_modify.setImageResource(R.drawable.icon_modify_disabled);
//                    }else{
//                        linearLayout_modify.setTag(i);
//                        textView_continue.setTextColor(Color.parseColor("#C2C2C2"));
//                        imageView_continue.setImageResource(R.drawable.icon_ask_disabled);
////                        textView_isAnswer.setText("未回复");
////                        textView_isAnswer.setTextColor(Color.parseColor("#ff661a"));
//                    }
                }else{
                    textView_continue.setTextColor(Color.parseColor("#C2C2C2"));
                    imageView_continue.setImageResource(R.drawable.icon_ask_disabled);
                    linearLayout_modify.setTag(i);
                    linearLayout_answerBody.setVisibility(View.GONE);
//                    textView_isAnswer.setText("未回复");
//                    textView_isAnswer.setTextColor(Color.parseColor("#ff661a"));
                }

                linearLayout_allContent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(htmlTextView_anw.getVisibility() == View.VISIBLE){
                            if(questionList.get((int)linearLayout_allContent.getTag()).isShowAllAns() == 1){
                                questionList.get((int) linearLayout_allContent.getTag()).setShowAllAns(2);
                                htmlTextView_anw.setLayoutParams(layoutParams_contentAll);
                                textView_allContent.setText("收起全文");
                                imageView_allContent.setImageResource(R.drawable.icon_arrow_up);
                            }else{
                                questionList.get((int) linearLayout_allContent.getTag()).setShowAllAns(1);
                                htmlTextView_anw.setLayoutParams(layoutParams_content);
                                textView_allContent.setText("查看全文");
                                imageView_allContent.setImageResource(R.drawable.icon_arrow_down);
                            }
                        }else{
                            if(questionList.get((int)linearLayout_allContent.getTag()).isShowAllAns() == 1){
                                questionList.get((int) linearLayout_allContent.getTag()).setShowAllAns(2);
                                webView_anw.setLayoutParams(layoutParams_contentAll);
                                textView_allContent.setText("收起全文");
                                imageView_allContent.setImageResource(R.drawable.icon_arrow_up);
                            }else{
                                questionList.get((int) linearLayout_allContent.getTag()).setShowAllAns(1);
                                webView_anw.setLayoutParams(layoutParams_content);
                                textView_allContent.setText("查看全文");
                                imageView_allContent.setImageResource(R.drawable.icon_arrow_down);
                            }
                        }
                    }
                });

                imageView_look_continue_ask.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(childParentWidght == 0){
                            childParentWidght = relativeLayout_zhuiwenBody.getWidth();
                            zhuiwenBtBodyWidth = linearLayout_zhuiwenBtBody.getWidth();
                        }
                        if(isClickOver){
                            isClickOver = false;
                            isAnimationOver = false;
                            Animation animation = AnimationUtils.loadAnimation(ExamRecommQuestionListActivity.this, R.anim.my_question_item_rotate);
                            imageView_zhuiwen.startAnimation(animation);
                            if(linearLayout_answerContainer11.getVisibility() == View.GONE){
                                tag = 1;
                                textView_zhuiwenText.setText("收起追问");
                            }else {
                                tag = 2;
                                textView_zhuiwenText.setText("查看追问");
                            }
                            if(compareId == null)
                                compareId = (String)imageView_look_continue_ask.getTag();
                            new Thread(){
                                @Override
                                public void run() {
                                    while(!isAnimationOver) {
                                        try{
                                            Thread.sleep(2);
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }
                                        handler.sendEmptyMessage(tag);
                                    }
                                }
                            }.start();
                        }
                    }
                });

                linearLayout_recomm_collect.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DialogManager.showNormalDialog(ExamRecommQuestionListActivity.this, "确定要取消对此答疑的收藏吗？", "提示", "取消", "确定", new DialogManager.CustomDialogCloseListener() {
                            @Override
                            public void yesClick() {
                                ExamRecommQuestionListActivity.this.finish();
                                if(questionAnswerId!=null && !questionAnswerId.equals(""))
                                    myQuestionListPersenter.deleteCollection(questionAnswerId);
                                else
                                    myQuestionListPersenter.deleteCollection(((String)linearLayout_recomm_collect.getTag()));
                                EventBus.getDefault().post(new DeleteQuestionCollection(((String)linearLayout_recomm_collect.getTag())));
                            }

                            @Override
                            public void noClick() {

                            }
                        });
                    }
                });
                linearLayout_recomm_goQuestion.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(questionList.get((int) linearLayout_go.getTag()).getQasTypeFlag().equals("2"))
                            return;
                        Intent intent = new Intent(ExamRecommQuestionListActivity.this, ExamActivity.class);
                        intent.putExtra("questionId", questionList.get((int) linearLayout_go.getTag()).getExaminationQuestionId());
                        SharedPrefHelper.getInstance(ExamRecommQuestionListActivity.this).setExamTag(Constants.EXAM_ORIGINAL_QUESTION);
                        startActivity(intent);
                    }
                });

                if(questionList.get(i).getQasSource().equals("1"))
                    imageView_from.setImageResource(R.drawable.my_question_item_from_phone);
                else
                    imageView_from.setImageResource(R.drawable.my_question_item_from_pc);

                linearLayout_go.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(questionList.get((int) linearLayout_go.getTag()).getQasTypeFlag().equals("2"))
                            return;
                        Intent intent = new Intent(ExamRecommQuestionListActivity.this, ExamActivity.class);
                        intent.putExtra("questionId", questionList.get((int) linearLayout_go.getTag()).getExaminationQuestionId());
                        SharedPrefHelper.getInstance(ExamRecommQuestionListActivity.this).setExamTag(Constants.EXAM_ORIGINAL_QUESTION);
                        ExamRecommQuestionListActivity.this.startActivity(intent);
                    }
                });

                linearLayout_continue.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(linearLayout_continue.getTag()!=null){
                            if(questionList.get(((int)linearLayout_continue.getTag())).getQasTypeFlag().equals("2")){//试题答疑修改
                                Intent intent = new Intent(ExamRecommQuestionListActivity.this, MyQuestionModifyActivity.class);
                                intent.putExtra("mode", 1);
                                if (QuestionParserUtil.isParent(questionList.get((int) linearLayout_continue.getTag()).getParentId()))
                                    intent.putExtra("qaFatherId", questionList.get((int) linearLayout_continue.getTag()).getAskId());
                                else
                                    intent.putExtra("qaFatherId", questionList.get((int) linearLayout_continue.getTag()).getParentId());
                                ExamRecommQuestionListActivity.this.startActivityForResult(intent, 0);
                            }else{//教材答疑修改
                                Intent intent = new Intent(ExamRecommQuestionListActivity.this, AddQuestionActivity.class);
                                intent.putExtra("modifyQuestion", questionList.get(((int) linearLayout_continue.getTag())));
                                ExamRecommQuestionListActivity.this.startActivityForResult(intent, 0);
                            }
                        }
                    }
                });

                linearLayout_modify.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(linearLayout_modify.getTag()!=null){
                            modify_ques_position = (int) linearLayout_modify.getTag();
                            Intent intent = new Intent(ExamRecommQuestionListActivity.this, MyQuestionModifyActivity.class);
                            intent.putExtra("mode", 2);
                            /**
                             * 此题有没有追问
                             */
                            String questionId = null,neirong =null;
                            if(questionList.get(modify_ques_position).getChildQuestionCount()>0){
                                int childCount = questionList.get(modify_ques_position).getChildQuestionCount();
                                questionId = questionList.get(modify_ques_position+childCount).getAskId();
                                neirong = questionList.get(modify_ques_position+childCount).getContent();
                            }else{
                                questionId = questionList.get(modify_ques_position).getAskId();
                                neirong = questionList.get(modify_ques_position).getContent();
                            }
                            intent.putExtra("questionId", questionId);
                            intent.putExtra("neirong",neirong);
                            ExamRecommQuestionListActivity.this.startActivityForResult(intent, 0);
                        }
                    }
                });
                viewArrayList.add(view);
            }

        }
        refreshScrollView.refreshContainer(viewArrayList);
//        for(int i=0;i<zhuiwenContainerList.size();i++){
//            if(lisMyQuestionObjs.get(i).getChildAskList()==null || lisMyQuestionObjs.get(i).getChildAskList().size()==0)
//                zhuiwenContainerList.get(i).setVisibility(View.GONE);
//        }
    }

        /**
         * 将追问的条目逐条加入到追问显示主体中
         * @param listQuestion  某个父问题所对应的追问集合
         * @param linearLayout_body 显示追问条目的主体
         */
        private void addZhuiWenItem(final List<Question> listQuestion,final LinearLayout linearLayout_body) {
            for (int i = 0; i < listQuestion.size(); i++) {
                View view = LayoutInflater.from(ExamRecommQuestionListActivity.this).inflate(R.layout.my_question_item_zhuiwen_item, null);
                final HtmlTextView htmlTextView_que, htmlTextView_anw;
                final TextView textView_createTime, textView_answTime, textView_isAnswer, textView_allContent;
                final WebView webView_que, webView_ans;
                final ImageView imageView_allContent;
                final LinearLayout linearLayout_anwsBody, linearLayout_allContent;
                htmlTextView_que = (HtmlTextView) view.findViewById(R.id.my_question_zhuiwen_item_queContentHtml);
                webView_que = (WebView) view.findViewById(R.id.my_question_zhuiwen_item_queContentWeb);
                textView_isAnswer = (TextView) view.findViewById(R.id.my_question_zhuiwen_item_isAnswer);
                textView_createTime = (TextView) view.findViewById(R.id.my_question_zhuiwen_item_queTime);
                linearLayout_anwsBody = (LinearLayout) view.findViewById(R.id.my_question_zhuiwen_item_answBody);
                htmlTextView_anw = (HtmlTextView) view.findViewById(R.id.my_question_zhuiwen_item_anwContentHtml);
                webView_ans = (WebView) view.findViewById(R.id.my_question_zhuiwen_item_anwContentWeb);
                webView_ans.setTag(listQuestion.get(i).getAskId());
                linearLayout_allContent = (LinearLayout) view.findViewById(R.id.my_question_zhuiwen_item_allContent);
                textView_allContent = (TextView) view.findViewById(R.id.my_question_zhuiwen_item_allContent_text);
                imageView_allContent = (ImageView) view.findViewById(R.id.my_question_zhuiwen_item_allContent_img);
                textView_answTime = (TextView) view.findViewById(R.id.my_question_zhuiwen_item_answerTime);

                try{
                    textView_createTime.setText(DateUtil.twoDateDistanceForStudyBarList(format.parse(listQuestion.get(i).getCreateTime()).getTime() + ""));
                }catch (Exception e){
                    e.printStackTrace();
                    textView_createTime.setText(listQuestion.get(i).getCreateTime());
                }

                if (listQuestion.get(i).getContent().contains("</td>")) {
                    webView_que.setVisibility(View.VISIBLE);
                    htmlTextView_que.setVisibility(View.GONE);
                    webView_que.loadDataWithBaseURL("", "<font color='#808080' style='font-size:15px;'>" + (listQuestion.get(i).getContent()) + "</font>", mimeType, encoding, "");
                } else {
                    htmlTextView_que.setHtmlFromString("<font color='#808080' style='font-size:15px;'>" + (listQuestion.get(i).getContent()) + "</font>", new HtmlTextView.RemoteImageGetter());
                }

                if (listQuestion.get(i).getReadStatus().equals("0")) {
                    linearLayout_anwsBody.setVisibility(View.GONE);
                    textView_isAnswer.setText("未回复");
                    textView_isAnswer.setTextColor(Color.parseColor("#ff661a"));
                } else {
                    textView_isAnswer.setText("已回复");
                    textView_isAnswer.setTextColor(Color.parseColor("#0ac7c4"));
                    try{
                        textView_answTime.setText(DateUtil.twoDateDistanceForStudyBarList(format.parse(listQuestion.get(i).getAnswerTime()).getTime()+""));
                    }catch (Exception e){
                        e.printStackTrace();
                        textView_answTime.setText(listQuestion.get(i).getAnswerTime());
                    }
                    if (listQuestion.get(i).getAnswer().contains("</td>") || listQuestion.get(i).getAnswer().contains("<img")) {
                        webView_ans.setVisibility(View.VISIBLE);
                        listQuestion.get(i).setIsShowAllAns(1);
                        layoutParams_content = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, maxHeight);
                        layoutParams_content.setMargins(webView_ans.getLeft(), webView_ans.getTop(), webView_ans.getRight(), 0);
                        layoutParams_contentAll.setMargins(webView_ans.getLeft(), webView_ans.getTop(), webView_ans.getRight(), 0);
                        webView_ans.setLayoutParams(layoutParams_content);
                        htmlTextView_anw.setVisibility(View.GONE);
                        webView_ans.loadDataWithBaseURL("", "<font color='#808080' style='font-size:15px;'>" + (listQuestion.get(i).getAnswer()) + "</font>", mimeType, encoding, "");
                    } else {
                        htmlTextView_anw.setTag(listQuestion.get(i).getAskId());
                        htmlTextView_anw.setHtmlFromString("<font color='#808080' style='font-size:15px;'>" + (listQuestion.get(i).getAnswer()) + "</font>", new HtmlTextView.RemoteImageGetter());
                        if (layoutParams_content == null) {
                            layoutParams_content = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, maxHeight);
                            layoutParams_contentAll = (LinearLayout.LayoutParams) htmlTextView_anw.getLayoutParams();
                            layoutParams_content.setMargins(layoutParams_contentAll.leftMargin, layoutParams_contentAll.topMargin, layoutParams_contentAll.rightMargin, 0);
                        }
                        freshShowAllAnSta(listQuestion, (String) htmlTextView_anw.getTag(), 1);
                        htmlTextView_anw.setLayoutParams(layoutParams_content);
//                        ViewTreeObserver viewTreeObserver = htmlTextView_anw.getViewTreeObserver();
//                        viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//                            @Override
//                            public void onGlobalLayout() {
//                                if (htmlTextView_anw.getVisibility() == View.VISIBLE && getQuestionIsShowAllAns((String) htmlTextView_anw.getTag()) == -1) {
//                                    if (htmlTextView_anw.getHeight() >= maxHeight) {
//                                        freshShowAllAnSta((String) htmlTextView_anw.getTag(), 1);
//                                        htmlTextView_anw.setLayoutParams(layoutParams_content);
//                                    } else {
//                                        freshShowAllAnSta((String) htmlTextView_anw.getTag(), 0);
//                                        linearLayout_allContent.setVisibility(View.GONE);
//                                    }
//                                }
//                                htmlTextView_anw.getViewTreeObserver().removeGlobalOnLayoutListener(this);//没有此句话，此监听listerner会一直监听，从而影响之后的setLayoutParams
//                            }
//                        });
                    }
                }
                linearLayout_allContent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (htmlTextView_anw.getVisibility() == View.VISIBLE) {
                            if (getQuestionIsShowAllAns(listQuestion, (String) htmlTextView_anw.getTag()) == 1) {
                                freshShowAllAnSta(listQuestion, (String) htmlTextView_anw.getTag(), 2);
                                htmlTextView_anw.setLayoutParams(layoutParams_contentAll);
                                textView_allContent.setText("收起全文");
                                imageView_allContent.setImageResource(R.drawable.icon_arrow_up);
                            } else {
                                freshShowAllAnSta(listQuestion, (String) htmlTextView_anw.getTag(), 1);
                                htmlTextView_anw.setLayoutParams(layoutParams_content);
                                textView_allContent.setText("查看全文");
                                imageView_allContent.setImageResource(R.drawable.icon_arrow_down);
                            }
                        } else {
                            if (getQuestionIsShowAllAns(listQuestion, (String) webView_ans.getTag()) == 1) {
                                freshShowAllAnSta(listQuestion, (String) webView_ans.getTag(), 2);
                                webView_ans.setLayoutParams(layoutParams_contentAll);
                                textView_allContent.setText("收起全文");
                                imageView_allContent.setImageResource(R.drawable.icon_arrow_up);
                            } else {
                                freshShowAllAnSta(listQuestion, (String) webView_ans.getTag(), 1);
                                webView_ans.setLayoutParams(layoutParams_content);
                                textView_allContent.setText("查看全文");
                                imageView_allContent.setImageResource(R.drawable.icon_arrow_down);
                            }
                        }
                    }
                });
                linearLayout_body.addView(view);
                if (i != (listQuestion.size() - 1)) {//添加条目间的横隔线
                    View view1 = new View(ExamRecommQuestionListActivity.this);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 2);
                    layoutParams.setMargins(0, 15, 0, 0);
                    view1.setLayoutParams(layoutParams);
                    view1.setBackgroundColor(Color.parseColor("#ececec"));
                    linearLayout_body.addView(view1);
                }
                }
            }

    /**
     * 获取当前所要控制的问题 是否显示了全部内容
     * @param id
     * @return
     */
    private int getQuestionIsShowAllAns(List<Question> questionList,String id){
        int isShowAllAns = -2;
        for(int i=0;i<questionList.size();i++){
            if(questionList.get(i).getAskId().equals(id))
                isShowAllAns = questionList.get(i).isShowAllAns();
        }
        return isShowAllAns;
    }


    /**
     * 点击全文或者收起时设置对应题目的显示情况
     * @param id
     * @param status
     */
    private void freshShowAllAnSta(List<Question> questionList,String id,int status){
        for(int i=0;i<questionList.size();i++){
            if(questionList.get(i).getAskId().equals(id))
                questionList.get(i).setShowAllAns(status);
        }
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {
        refreshScrollView.stopRefresh();
        relativeLayout_hint_body.setVisibility(View.VISIBLE);
        imageView_loading.setVisibility(View.GONE);
        linearLayout_pic_body.setVisibility(View.VISIBLE);
        imageView_hint.setImageResource(R.drawable.pic_failure2);
    }

    @Override
    public void showData(List<Question> questionList) {
        if(totalPage == page)
            refreshScrollView.setFooterState(ScrollViewFooter.STATE_NOMORE);
        refreshScrollView.stopRefresh();
//        if(questionList==null || questionList.size()==0){
//            emptyViewLayout.showEmpty();
//            return;
//        }else{
//            emptyViewLayout.showContentView();
//        }
//        if(questionList.size()>=Constants.PAGESIZE){
//            refreshScrollView.setFooterState(ScrollViewFooter.STATE_NORMAL);
//        }else{
//            refreshScrollView.setFooterState(ScrollViewFooter.STATE_NOMORE);
//        }
        this.lisMyQuestionObjs = questionList;
        relativeLayout_hint_body.setVisibility(View.GONE);
        if(lisMyQuestionObjs.size()>0)
            freshContentView(lisMyQuestionObjs);
        else
            showNoDataHint();
    }

    @Override
    public Context context() {
        return this;
    }

    @Override
    public void onRefresh() {
        isLoadmore = false;
        page = 1;
        myQuestionListPersenter.setLoadmore(false);
        getData();
    }

    @Override
    public void onLoadMore() {
        isLoadmore = true;
        page++;
        myQuestionListPersenter.setLoadmore(true);
        getData();
    }

    /**
     * 显示没有数据的提示
     */
    public void showNoDataHint(){
        relativeLayout_hint_body.setVisibility(View.VISIBLE);
        imageView_loading.setVisibility(View.GONE);
        linearLayout_pic_body.setVisibility(View.VISIBLE);
        imageView_hint.setVisibility(View.VISIBLE);
        imageView_hint.setImageResource(R.drawable.pic_empty2);
    }

    @Override
    public int getPage() {
        return page;
    }

    @Override
    public void showLoadMoreError(String message) {
        refreshScrollView.setFooterState(ScrollViewFooter.STATE_NORMAL);
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showMoreData(List<Question> questionList) {
        if(totalPage == page)
            refreshScrollView.setFooterState(ScrollViewFooter.STATE_NOMORE);
        if(questionList.size()>=10){
            refreshScrollView.setFooterState(ScrollViewFooter.STATE_NORMAL);
        }else{
            refreshScrollView.setFooterState(ScrollViewFooter.STATE_NOMORE);
        }
        if(questionList.size()>0)
            freshContentView(questionList);
    }

    @Override
    public void bookListJson(String json) {
        this.bookList = json;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(data!=null){
            if(data.getIntExtra("mode",-1) == 2 || data.getIntExtra("mode",-1) == 1){//追问、修改成功的回调
                onRefresh();
            }
        }
    }

    @Override
    public void showNoData() {
        showNoDataHint();
    }

    @Override
    public void isCanAsk(String flag) {
        dismissAnimProgress();
        if(flag.equals("1")){
            Intent intent = new Intent(this,MyQuestionModifyActivity.class);
            intent.putExtra("examinationQuestionId",examinationQuestionId);
            startActivity(intent);
        }else{
            Toast.makeText(this,"您已提问，此题已不能提问",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void checkIsCanAskFail() {
        dismissAnimProgress();
        Toast.makeText(this,"校验失败",Toast.LENGTH_SHORT).show();
    }

    @Override
    public String getQuestionId() {
//        return examinationQuestionId;
        return examinationQuestionId;
    }

    @Override
    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }
}
