package com.bjjy.mainclient.phone.view.main;


import com.dongao.mainclient.model.mvp.MvpView;

/**
 * UI 定义UI 看此UI中有何事件
 */
public interface MainView extends MvpView {
  void setResult();
  double getLatitude();
  double getLontitude();
  String getAddr();
  void setCodet(int code);
}
