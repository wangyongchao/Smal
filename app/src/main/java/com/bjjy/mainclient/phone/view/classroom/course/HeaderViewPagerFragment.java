package com.bjjy.mainclient.phone.view.classroom.course;

import com.bjjy.mainclient.phone.app.BaseFragment;
import com.bjjy.mainclient.phone.widget.parallaxviewpager.HeaderScrollHelper;

public abstract class HeaderViewPagerFragment extends BaseFragment implements HeaderScrollHelper.ScrollableContainer {}
