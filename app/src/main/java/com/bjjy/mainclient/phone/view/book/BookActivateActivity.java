package com.bjjy.mainclient.phone.view.book;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.dongao.mainclient.core.util.StringUtil;
import com.dongao.mainclient.model.common.Constants;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseActivity;
import com.bjjy.mainclient.phone.utils.NetworkUtil;
import com.bjjy.mainclient.phone.widget.msg.AppMsg;

import butterknife.Bind;
import butterknife.ButterKnife;


public class BookActivateActivity extends BaseActivity implements BookActivateView {


    @Bind(R.id.top_title_left)
    ImageView top_title_left;

    @Bind(R.id.top_title_text)
    TextView top_title_text;

    @Bind(R.id.tv_hit)
    TextView tv_hit;

    @Bind(R.id.tv_book)
    TextView tv_book;

    @Bind(R.id.tv_tmall)
    TextView tv_tmall;

    @Bind(R.id.v_tmall_line)
    View v_tmall_line;

    @Bind(R.id.v_book_line)
    View v_book_line;



    @Bind(R.id.book_card_num_et)
    EditText usernameEdit;

    @Bind(R.id.mBtn)
    Button mButton;

    @Bind(R.id.clean_btn)
    ImageView clean_btn;

    @Bind(R.id.imageView)
    ImageView imageView;

    private String username;
    Intent intent;

    private int messageHeight;
    private BookActivatePersenter bookActivatePersenter;

    private static final String HIT_BOOK = "激活说明：\n" +
            "1、购买会计类考试“轻松过关”指定系列辅导书或会计从业教材的用户，凭图书上的“随书赠卡”在此激活\n" +
            "2、同时任一“轻松过关”辅导书随书赠卡都可以解锁对应科目下的题库和电子书服务";
    private static final String HIT_MATLL = "激活说明：\n" +
            "1、在天猫东奥会计在线官方旗舰店预订“轻松过关”指定系列辅导书的用户，凭天猫购书订单号来激活解锁。一个用户一科仅激活一次。\n" +
            "2、每考季具体活动规则详见东奥会计在线官网";

    private int mType = BookActivatePersenter.TYPE_BOOK;
    //切换tab 时的临时存储
    private String tmpBook = "";
    private String tmpMatll = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.book_activate);
        ButterKnife.bind(this);
        initView();
    }

    @Override
    public void initView() {

        top_title_left.setImageResource(R.drawable.back);
        top_title_left.setVisibility(View.VISIBLE);
        top_title_left.setOnClickListener(this);
        top_title_text.setText("图书激活");

        usernameEdit.addTextChangedListener(new TextChange());
        mButton.setOnClickListener(this);

        clean_btn.setOnClickListener(this);
        tv_book.setOnClickListener(this);
        tv_tmall.setOnClickListener(this);

        bookActivatePersenter = new BookActivatePersenter();
        bookActivatePersenter.attachView(this);
        messageHeight = (int) getResources().getDimension(R.dimen.dip_size_50);
        updateUIbyType();


    }

    private void updateUIbyType(){
        if (mType == BookActivatePersenter.TYPE_BOOK){
            tv_hit.setText(HIT_BOOK);
            tv_book.setTextColor(getResources().getColor(R.color.color_primary));
            tv_tmall.setTextColor(getResources().getColor(R.color.text_color_primary));
            v_book_line.setVisibility(View.VISIBLE);
            v_tmall_line.setVisibility(View.INVISIBLE);
            usernameEdit.setHint("请输入随书赠卡激活码");
            imageView.setImageResource(R.drawable.ic_activation_max);
            tmpMatll = usernameEdit.getText().toString();
            usernameEdit.setText(tmpBook);
        }else {
            tv_hit.setText(HIT_MATLL);
            tv_tmall.setTextColor(getResources().getColor(R.color.color_primary));
            tv_book.setTextColor(getResources().getColor(R.color.text_color_primary));
            v_tmall_line.setVisibility(View.VISIBLE);
            v_book_line.setVisibility(View.INVISIBLE);
            usernameEdit.setHint("请输入天猫购书订单号");
            imageView.setImageResource(R.drawable.ic_activation_max_mtall);
            tmpBook = usernameEdit.getText().toString();
            usernameEdit.setText(tmpMatll);
        }
    }

    @Override
    public void initData() {

        intent = getIntent();
        username = usernameEdit.getText().toString();
        if (validate()) {
            //usernameEdit.setEnabled(false);
            mButton.setEnabled(false);
            mButton.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (usernameEdit.getText().length() > 0){
                        mButton.setEnabled(true);
                    }else {
                        mButton.setEnabled(false);
                    }
                }
            },1000);
            bookActivatePersenter.getData(mType);
        }
    }

    /**
     * 验证
     *
     * @return
     */
    private boolean validate() {

        if (StringUtil.isEmpty(username.trim())) {
            showAppMsg("卡号不能为空", messageHeight);
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.top_title_left:
                onBackPressed();
                break;
            case R.id.clean_btn:
                usernameEdit.setText("");
                break;
            case R.id.mBtn:
//                messageHeight = findViewById(R.id.top_title_bar_layout).getMeasuredHeight()
//                        +findViewById(R.id.book_active_top_layout).getMeasuredHeight();
                if (NetworkUtil.isNetworkAvailable(BookActivateActivity.this))
                    initData();
                else
                    showAppMsg("请先连接网络", messageHeight);
                break;
            case R.id.tv_book:
                mType = BookActivatePersenter.TYPE_BOOK;
                updateUIbyType();
                break;
            case R.id.tv_tmall:
                mType = BookActivatePersenter.TYPE_MATLL;
                updateUIbyType();
                break;
        }
    }

    @Override
    public String cardNum() {
        return usernameEdit.getText().toString();
    }


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {
        showAppMsg(message, messageHeight);
    }

    @Override
    public Context context() {
        return this;
    }

    //EditText监听器
    class TextChange implements TextWatcher {

        @Override
        public void afterTextChanged(Editable arg0) {

        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

        }

        @Override
        public void onTextChanged(CharSequence cs, int start, int before, int count) {
            boolean editText1 = usernameEdit.getText().length() > 0;
            if (editText1) {
                mButton.setEnabled(true);
            }
            //在layout文件中，对Button的text属性应预先设置默认值，否则刚打开程序的时候Button是无显示的
            else {
                mButton.setEnabled(false);
            }

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    public void showAppMsg(String msg, int height) {
        final AppMsg.Style style;
        style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.info);
        AppMsg appMsg = AppMsg.makeText(this, msg, style);
        appMsg.setLayoutGravity(Gravity.TOP);
        appMsg.show(height);
    }

    @Override
    public void showAppMsg(String msg) {
        showAppMsg(msg,messageHeight);
    }

    @Override
    public void activitySuccess() {
        setResult(Constants.ACTIVITY_RESULT_CODE_BOOK_ACTIVATE, intent);
        this.finish();
    }
}
