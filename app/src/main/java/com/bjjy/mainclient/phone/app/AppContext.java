package com.bjjy.mainclient.phone.app;

import android.content.Context;
import android.os.Handler;
import android.support.multidex.MultiDex;
import android.text.TextUtils;
import android.widget.Toast;

import com.bjjy.mainclient.phone.download.DownloadExcutor;
import com.bjjy.mainclient.phone.view.exam.bean.Question;
import com.bjjy.mainclient.phone.view.play.utils.NanoHTTPD;
import com.bjjy.mainclient.phone.view.studybar.push.CustomNotification;
import com.bjjy.mainclient.phone.view.studybar.push.OfficalNotification;
import com.dongao.mainclient.core.app.MypAppContext;
import com.dongao.mainclient.core.util.FileUtil;
import com.dongao.mainclient.model.bean.courselect.Goods;
import com.dongao.mainclient.model.common.Constants;
import com.easefun.polyvsdk.live.PolyvLiveSDKClient;
import com.easefun.polyvsdk.live.chat.PolyvChatManager;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.umeng.message.PushAgent;
import com.yunqing.core.db.DBExecutor;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


/**
 * 所有初始化的工作可在这里面进行
 * 
 * @author Xice
 * 
 */
public class AppContext extends MypAppContext implements Thread.UncaughtExceptionHandler{
    /**
     * 登录聊天室/ppt直播所需，请填写自己的appId和appSecret，否则无法登陆
     * appId和appSecret在直播系统管理后台的用户信息页的API设置中用获取
     */
    private static final String appId = "ettrxaqogh";
    private static final String appSecret = "735340e6c0014ec7bffe0d30a4ea24be";

    public DBExecutor dbExecutor;

    private static AppContext instance;

    public List<Goods> goods;
    
    //题库
    private List<Question> questionlist;
    //答题报告中的数据
    private List<Question> allList;//所有的题--题冒题只是一个题
    private List<Question> errorList;//错误的题包括题冒题

    private Handler handler;//下载消息通知(缓存页面)
    private Handler handlerDetail;//下载消息通知(课程详情页面)
    private Handler playHandler;//网络切换通知播放的handler

    public int isStart=0;//0表示没有开启下载线程  1表示已经开启下载线程

    private String scanExamId;//扫描的examId
    private String scanSubjectId;//扫描的subjectId
    private boolean isScanTakIn=false;//只有扫描的时候才为true，出来后设为false
    private String scanExaminationId;//扫描的试卷ID
    private String scanTypeId;//扫描的试卷类型的ID
    public PushAgent myPushAgent;
    private List<String> domainNameList;//存储的域名集合

    private byte[] entryData;//将要进行加密的字节数组
    private NanoHTTPD httpserver;
    @Override
	public void onCreate() {
		super.onCreate();
//        CrashReport.initCrashReport(getApplicationContext(), "b94d6642b6", false);
		// 注册App异常崩溃处理器
		//Thread.setDefaultUncaughtExceptionHandler(AppException.getAppExceptionHandler());
        dbExecutor = DBExecutor.getInstance(this);
        goods=new ArrayList<>();
        push();
        startApplicationService();
        initImageLoader(getApplicationContext());

        initPolyvChatConfig();
        initPolyvCilent();
	}

    /**
     * 初始化聊天室配置
     */
    public void initPolyvChatConfig() {
        PolyvChatManager.initConfig(appId, appSecret);
    }

    public void initPolyvCilent() {
        PolyvLiveSDKClient client = PolyvLiveSDKClient.getInstance();
    }

    public static void initImageLoader(Context context) {
        // This configuration tuning is custom. You can tune every option, you may tune some of them,
        // or you can create default configuration by
        //  ImageLoaderConfiguration.createDefault(this);
        // method.
        ImageLoaderConfiguration config = ImageLoaderConfiguration.createDefault(context);

        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config);
    }

    /**
     * 推送
     */
    private void push() {
        myPushAgent= PushAgent.getInstance(this);
        myPushAgent.setMessageHandler(new OfficalNotification(this));
        myPushAgent.setNotificationClickHandler(new CustomNotification());

    }

    public static AppContext getInstance() {
        if (instance == null) {
            instance = new AppContext();
        }
        return instance;

    }

    public Handler getPlayHandler() {
        return playHandler;
    }

    public void setPlayHandler(Handler playHandler) {
        this.playHandler = playHandler;
    }

    /**
     * 分割 Dex 支持
     * @param base
     */
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public List<String> getDomainNameList() {
        return domainNameList;
    }

    public void setDomainNameList(List<String> domainNameList) {
        this.domainNameList = domainNameList;
    }

    public Handler getHandlerDetail() {
        return handlerDetail;
    }

    public void setHandlerDetail(Handler handlerDetail) {
        this.handlerDetail = handlerDetail;
    }

    public Handler getHandler() {
        return handler;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    public List<Goods> getGoods() {
        return goods;
    }

    public void setGoods(List<Goods> goods) {
        this.goods = goods;
    }

    public List<Question> getAllList() {
        return allList;
    }

    public void setAllList(List<Question> allList) {
        this.allList = allList;
    }

    public List<Question> getQuestionlist() {
        return questionlist;
    }

    public void setQuestionlist(List<Question> questionlist) {
        this.questionlist = questionlist;
    }

    public List<Question> getErrorList() {
        return errorList;
    }

    public void setErrorList(List<Question> errorList) {
        this.errorList = errorList;
    }
    
    

    @Override
    public void uncaughtException(Thread thread, Throwable ex) {
        AppContext.getInstance().isStart=0;
        DownloadExcutor.getInstance(getApplicationContext()).setFlag(false);
    }

    public String getScanExamId() {
        return scanExamId;
    }

    public void setScanExamId(String scanExamId) {
        this.scanExamId = scanExamId;
    }

    public String getScanSubjectId() {
        return scanSubjectId;
    }

    public void setScanSubjectId(String scanSubjectId) {
        this.scanSubjectId = scanSubjectId;
    }

    public boolean getIsScanTakIn() {
        return isScanTakIn;
    }

    public void setIsScanTakIn(boolean isScanTakIn) {
        this.isScanTakIn = isScanTakIn;
    }

    public String getScanExaminationId() {
        return scanExaminationId;
    }

    public void setScanExaminationId(String scanExaminationId) {
        this.scanExaminationId = scanExaminationId;
    }

    public String getScanTypeId() {
        return scanTypeId;
    }

    public void setScanTypeId(String scanTypeId) {
        this.scanTypeId = scanTypeId;
    }

    public byte[] getEntryData() {
        return entryData;
    }


    public void setEntryData(byte[] entryData) {
        this.entryData = entryData;
    }

    /**
     * 启动NanoHTTP服务
     */
    private void startApplicationService() {

        // 启动HTTP服务
        if (TextUtils.isEmpty(FileUtil.getDownloadPath(this))) {
            Toast.makeText(this, "当前没有SD卡存储，下载课程功能无法使用", Toast.LENGTH_LONG)
                    .show();
        }
        try {
            int port = Constants.SERVER_PORT;
            if (httpserver == null) {
                httpserver = new NanoHTTPD(port,new File(FileUtil.getDownloadPath(this)),getApplicationContext());
            }
            System.out.println("代理服务启动成功");
        } catch (Exception e) {

        }
    }


}
