package com.bjjy.mainclient.phone.view.setting.cache.domain;

import com.bjjy.mainclient.phone.view.exam.activity.exampaperlist.bean.Course;

import java.util.List;

/**
 * Created by dell on 2016/8/29.
 */
public class GourpDomain {

    private String name;
    private String subjectId;
    private int year;
    private List<Course> childs;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Course> getChilds() {
        return childs;
    }

    public void setChilds(List<Course> childs) {
        this.childs = childs;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
