package com.bjjy.mainclient.phone.view.setting.update;


import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;


/**
 * Created by wyc on 14-5-8.
 */
public class JSONHandler {

    public static UpdateInfo toUpdateInfo(InputStream is) throws Exception {
        if (is == null){
            return null;
        }
        String byteData = new String(readStream(is));
        android.util.Log.i("update","update_str=="+byteData);
        is.close();
        JSONObject jsonObject = new JSONObject(byteData).getJSONObject("version");
        UpdateInfo updateInfo = new UpdateInfo();
        updateInfo.setDownloadUrl(jsonObject.getString("appUrl"));
        updateInfo.setIsUpdate(jsonObject.getString("isForce"));
        updateInfo.setCurrentVersion(jsonObject.getString("versionCode"));
        updateInfo.setVersionName(jsonObject.getString("versionName"));
        updateInfo.setDescrip(jsonObject.getString("changeLog"));
        return updateInfo;
    }

    private static byte[] readStream(InputStream inputStream) throws IOException {
        ByteArrayOutputStream outputStream=new ByteArrayOutputStream();
        byte [] array=new byte[1024];
        int len = 0;
        while( (len = inputStream.read(array)) != -1){
            outputStream.write(array,0,len);
        }
        inputStream.close();
        outputStream.close();
        return outputStream.toByteArray();
    }

}
