package com.bjjy.mainclient.phone.view.download.local;

import android.content.Intent;

import com.alibaba.fastjson.JSON;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.download.db.DownloadDB;
import com.bjjy.mainclient.phone.view.download.local.bean.YearInfo;
import com.bjjy.mainclient.phone.view.exam.activity.exampaperlist.bean.Course;
import com.bjjy.mainclient.phone.view.exam.activity.exampaperlist.bean.MyCourse;
import com.bjjy.mainclient.phone.view.exam.utils.CommenUtils;
import com.bjjy.mainclient.phone.view.play.domain.CourseWare;
import com.bjjy.mainclient.phone.view.studybar.Constant;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.mvp.BasePersenter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wyc on 2016/5/19.
 */
public class DownloadChoosePercenter extends BasePersenter<DownloadChooseView> {
    public ArrayList<Course> courseList;
    private ArrayList<YearInfo> yearList;
    private ArrayList<MyCourse> list;
    public boolean isShowCheck=false;//是否显示选择图标


    private String courseId;
    private String userId;
    private YearInfo currYear;
    private DownloadDB downloadDB;

    public void initData() {
        Intent intent=getMvpView().getTheIntent();
        userId= SharedPrefHelper.getInstance(getMvpView().context()).getUserId();
        list=new ArrayList<>();
        yearList=new ArrayList<>();
        currYear=new YearInfo();
        downloadDB=new DownloadDB(getMvpView().context());
        String yearNow=SharedPrefHelper.getInstance(getMvpView().context()).getCurYear();
        if(!yearNow.isEmpty()){
            currYear= JSON.parseObject(yearNow, YearInfo.class);
        }
        String listYears=SharedPrefHelper.getInstance(getMvpView().context()).getYearList();
        if (!listYears.isEmpty()){
            yearList.clear();
            yearList= (ArrayList<YearInfo>) JSON.parseArray(listYears,YearInfo.class);
        }
        getCourseListData();
        getMvpView().showCurrentYear(currYear);
        getData();
    }


    /**
     * 查询数据库，并解析,数据库查出的数据是jason字符串
     */
    private void getCourseListData(){
        List<Course> newCourseList=  downloadDB.findCourses(userId,currYear.getYearName());
        if (courseList==null){
            courseList=new ArrayList<>();
        }
        courseList.clear();
        if (newCourseList!=null){
            courseList.addAll(newCourseList);
            /*for (int i = 0; i < newCourseList.size(); i++) {
                String courseBean=newCourseList.get(i).getCwName();
                Course newCourse=JSON.parseObject(courseBean,Course.class);
                courseList.add(newCourse);
            }*/
        }
    }

    @Override
    public void getData() {
        String yearNow=SharedPrefHelper.getInstance(getMvpView().context()).getCurYear();
        if(!yearNow.isEmpty()){
            currYear= JSON.parseObject(yearNow, YearInfo.class);
        }
        getMvpView().showCurrentYear(currYear);
//        if (currYear.getIsPay().equals(Constants.IS_LOCK_NO)){//支付过
            getCourseListData();
            if (courseList.size()==0) {
                getMvpView().showContentView(Constant.VIEW_TYPE_2);
            } else {
                getMvpView().initAdapter();
                getMvpView().showContentView(Constant.VIEW_TYPE_0);
            }
//        }else {//2 未支付
//            getMvpView().showContentView(Constant.VIEW_TYPE_3);
//        }
    }

    /**
     * TODO
     * 获取数据库数据并解析其中的json字符串
     */
    private void getCourseWareList(){
//        List<CourseWare> downloadCourseList=  downloadDB.findCourseWares(courseId,userId,currYear.getYearName());
//
//        List<CourseWare> newCourseList=  courseWareDB.queryByYearAndCourseId(userId, courseId, currYear.getYearName());
//        if (courseList==null){
//            courseList=new ArrayList<>();
//        }
//        courseList.clear();
//        if (downloadCourseList!=null&&newCourseList!=null){
//            for (int i = 0; i < downloadCourseList.size(); i++) {
//                for (int j = 0; j < newCourseList.size(); j++) {
//                    if (downloadCourseList.get(i).getVideoID().equals(newCourseList.get(j).getVideoID())){
//                        courseList.add(newCourseList.get(j)); 
//                    }
//                }
//            }
//        }
    }

    private String courseString;



    private void getInterData() {
    }


    @Override
    public void setData(String obj) {
    }

    public void setOnItemClick(int position) {
        currYear = yearList.get(position);
        getMvpView().showCurrentYear(currYear);
        SharedPrefHelper.getInstance(getMvpView().context()).setCurYear(JSON.toJSONString(currYear));
        getMvpView().hideYearPop(true);
        getData();
    }

    @Override
    public void onError(Exception e) {
        super.onError(e);
        getMvpView().showContentView(Constant.VIEW_TYPE_1);
    }

    /**
     * 删除数据库中的数据
     */
    private void deleteDBDta(CourseWare courseWare) {
//        downloadDB.deleteCourseWare(userId,courseWare);
    }

    public void remove(int position) {
        courseList.remove(position);
        getMvpView().initAdapter();
    }

    public void courseClick(int position,long l) {
        if (isShowCheck){
            if (courseList.get((int)l).isCheck()){
                courseList.get((int)l).setIsCheck(false);
            }else {
                courseList.get((int)l).setIsCheck(true);
            }
            int selectSize= CommenUtils.getSelectedSize(courseList);
            if (selectSize==courseList.size()){
                getMvpView().setCancelText(getMvpView().context().getResources().getString(R.string.cancel_all));
                getMvpView().setDeleteText("删除("+courseList.size()+")");
            }else if(selectSize==0){
                getMvpView().setCancelText(getMvpView().context().getResources().getString(R.string.select_all));
                getMvpView().setDeleteText("删除");
            }else {
                getMvpView().setCancelText(getMvpView().context().getResources().getString(R.string.select_all));
                getMvpView().setDeleteText("删除("+selectSize+")");
            }
            getMvpView().initAdapter();

        }else {
            if (courseList.get((int)l).getDownloadCount()==1){
                   /* Intent intent=new Intent(LocalCourseActivity.this,PlayActivity.class);
                    String courseString=JSON.toJSONString(courseList.get((int)l));
                    if (courseString==null){
                        courseString="";
                    }
                    intent.putExtra("course",courseString);
                    startActivity(intent);*/
                Intent intent=new Intent(getMvpView().context(),DownloadLocalActivity.class);
                String courseString= JSON.toJSONString(courseList.get((int)l));
                if (courseString==null){
                    courseString="";
                }
                intent.putExtra("courseId",courseList.get((int)l).getCwCode());
                intent.putExtra("course",courseString);
                getMvpView().context().startActivity(intent);
            }else {
                Intent intent=new Intent(getMvpView().context(),DownloadLocalActivity.class);
                String courseString= JSON.toJSONString(courseList.get((int)l));
                if (courseString==null){
                    courseString="";
                }
                intent.putExtra("courseId",courseList.get((int)l).getCwCode());
                intent.putExtra("course",courseString);
                getMvpView().context().startActivity(intent);
            }

        }
    }
}
