package com.bjjy.mainclient.phone.view.classroom.subject;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.dongao.mainclient.model.bean.course.Exam;
import com.dongao.mainclient.model.bean.course.Subject;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.bjjy.mainclient.phone.R;

import java.util.ArrayList;

/**
 * Created by wyc on 16/5/13.
 */
public class SubjectExpandableListViewAdapter extends BaseExpandableListAdapter
{


    private Context context;
    private ArrayList<Exam> mlist;
    private String selectedId = null;

    public SubjectExpandableListViewAdapter(Context context)
    {
        this.context = context;
        selectedId = SharedPrefHelper.getInstance(context).getSubjectId();
    }

    public void setList(ArrayList<Exam> mlist) {
        this.mlist = mlist;
    }

    /**
     *
     * 获取组的个数
     *
     * @return
     * @see android.widget.ExpandableListAdapter#getGroupCount()
     */
    @Override
    public int getGroupCount()
    {
        return mlist.size();
    }

    /**
     *
     * 获取指定组中的子元素个数
     *
     * @param groupPosition
     * @return
     * @see android.widget.ExpandableListAdapter#getChildrenCount(int)
     */
    @Override
    public int getChildrenCount(int groupPosition)
    {
        return mlist.get(groupPosition).getSubjectInfoList().size();
    }

    /**
     *
     * 获取指定组中的数据
     *
     * @param groupPosition
     * @return
     * @see android.widget.ExpandableListAdapter#getGroup(int)
     */
    @Override
    public Object getGroup(int groupPosition)
    {
        return mlist.get(groupPosition);
    }

    /**
     *
     * 获取指定组中的指定子元素数据。
     *
     * @param groupPosition
     * @param childPosition
     * @return
     * @see android.widget.ExpandableListAdapter#getChild(int, int)
     */
    @Override
    public Object getChild(int groupPosition, int childPosition)
    {
        return mlist.get(groupPosition).getSubjectInfoList().get(childPosition);
    }

    /**
     *
     * 获取指定组的ID，这个组ID必须是唯一的
     *
     * @param groupPosition
     * @return
     * @see android.widget.ExpandableListAdapter#getGroupId(int)
     */
    @Override
    public long getGroupId(int groupPosition)
    {
        return groupPosition;
    }

    /**
     *
     * 获取指定组中的指定子元素ID
     *
     * @param groupPosition
     * @param childPosition
     * @return
     * @see android.widget.ExpandableListAdapter#getChildId(int, int)
     */
    @Override
    public long getChildId(int groupPosition, int childPosition)
    {
        return childPosition;
    }

    /**
     *
     * 组和子元素是否持有稳定的ID,也就是底层数据的改变不会影响到它们。
     *
     * @return
     * @see android.widget.ExpandableListAdapter#hasStableIds()
     */
    @Override
    public boolean hasStableIds()
    {
        return true;
    }

    /**
     *
     * 获取显示指定组的视图对象
     *
     * @param groupPosition 组位
     * @param isExpanded 该组是展开状态还是伸缩状态
     * @param convertView 重用已有的视图对象
     * @param parent 返回的视图对象始终依附于的视图
     * @return
     * @see android.widget.ExpandableListAdapter#getGroupView(int, boolean, android.view.View,
     *      android.view.ViewGroup)
     */
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent)
    {
        final ViewSectionHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewSectionHolder();
            convertView = LayoutInflater.from(context).inflate(com.bjjy.mainclient.phone.R.layout.course_item, null);
            viewHolder.titleTv = (TextView) convertView.findViewById(com.bjjy.mainclient.phone.R.id.title);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewSectionHolder) convertView.getTag();
        }
        Exam exam = mlist.get(groupPosition);
        viewHolder.titleTv.setText(exam.getExamName());
        viewHolder.titleTv.setTextColor(context.getResources().getColor(R.color.color_primary));
        return convertView;
    }

    /**
     *
     * 获取一个视图对象，显示指定组中的指定子元素数据。
     *
     * @param groupPosition 组位
     * @param childPosition 子元素位
     * @param isLastChild 子元素是否处于组中的最后一个
     * @param convertView 重用已有的视图(View)对象
     * @param parent 返回的视图(View)对象始终依附于的视图
     * @return
     * @see android.widget.ExpandableListAdapter#getChildView(int, int, boolean, android.view.View,
     *      android.view.ViewGroup)
     */
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent)
    {
        final ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(com.bjjy.mainclient.phone.R.layout.course_item, null);
            viewHolder.titleTv = (TextView) convertView.findViewById(com.bjjy.mainclient.phone.R.id.title);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Subject subject = mlist.get(groupPosition).getSubjectInfoList().get(childPosition);
        viewHolder.titleTv.setText(subject.getSubjectName()+"("+subject.getYear()+")");

        if (!TextUtils.isEmpty(selectedId)) {
            if (selectedId.equals(subject.getSubjectId()+"")){
                viewHolder.titleTv.setTextColor(viewHolder.titleTv.getContext().getResources().getColor(R.color.green));
            }else {
                viewHolder.titleTv.setTextColor(viewHolder.titleTv.getContext().getResources().getColor(R.color.text_color_primary));
            }
        }else {
            viewHolder.titleTv.setTextColor(viewHolder.titleTv.getContext().getResources().getColor(R.color.text_color_primary));
        }


        return convertView;
    }

    /**
     *
     * 是否选中指定位置上的子元素。
     *
     * @param groupPosition
     * @param childPosition
     * @return
     * @see android.widget.ExpandableListAdapter#isChildSelectable(int, int)
     */
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition)
    {
        return true;
    }

    class ViewSectionHolder {
        TextView titleTv;
    }

    class ViewHolder {
        TextView titleTv;
    }

}