package com.bjjy.mainclient.phone.view.question;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bjjy.mainclient.phone.app.BaseFragmentActivity;
import com.bjjy.mainclient.phone.event.AddQuestionSuccess;
import com.bjjy.mainclient.phone.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by fengzongwei on 2016/12/9 0009.
 */
public class SelectBookAndKpActivity extends BaseFragmentActivity {

    @Bind(R.id.top_title_left)
    ImageView imageView_back;
    @Bind(R.id.top_title_text)
    TextView textView_title;
    @Bind(R.id.question_select_book_body)
    LinearLayout linearLayout_body;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question_select_book);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        initView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void initView() {
        imageView_back.setVisibility(View.VISIBLE);
        imageView_back.setImageResource(R.drawable.back);
        textView_title.setText("选择图书");
        FragmentManager manager = getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.question_select_book_body,new TabQuestionFragment()).commit();
        imageView_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Subscribe
    public void onEventAsync(AddQuestionSuccess event) {
        finish();
    }

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View v) {

    }
}
