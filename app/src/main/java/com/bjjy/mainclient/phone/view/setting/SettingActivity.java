package com.bjjy.mainclient.phone.view.setting;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.AppContext;
import com.bjjy.mainclient.phone.app.BaseActivity;
import com.bjjy.mainclient.phone.download.DownloadExcutor;
import com.bjjy.mainclient.phone.download.db.DownloadDB;
import com.bjjy.mainclient.phone.event.LogOutEvent;
import com.bjjy.mainclient.phone.view.setting.utils.DataCleanManager;
import com.bjjy.mainclient.phone.widget.DialogManager;
import com.dongao.mainclient.core.util.NetUtils;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.mvp.ApiModel;
import com.dongao.mainclient.model.mvp.ResultListener;

import org.greenrobot.eventbus.EventBus;

import butterknife.Bind;
import butterknife.ButterKnife;
import me.drakeet.materialdialog.MaterialDialog;


public class SettingActivity extends BaseActivity implements CompoundButton.OnCheckedChangeListener {

    @Bind(R.id.top_title_left)
    ImageView topTitleLeft;
    @Bind(R.id.top_title_text)
    TextView topTitleText;
    @Bind(R.id.setting_opinion)
    RelativeLayout settingOpinion;
    @Bind(R.id.setting_about)
    RelativeLayout settingAbout;

    @Bind(R.id.setting_noWifi_play_img)
    CheckBox settingNoWifiPlay;

    @Bind(R.id.setting_exit_bt)
    Button setting_exit_bt;
    @Bind(R.id.rl_cleanCache)
    RelativeLayout rlCleanCache;
    @Bind(R.id.tv_cacheSize)
    TextView tvCacheSize;

    private MaterialDialog alertDialog;
    private MaterialDialog mMaterialDialog;
    private DownloadDB db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_set);
        ButterKnife.bind(this);

        initView();
        initData();
    }

    @Override
    public void initView() {
        // TODO Auto-generated method stub
        db=new DownloadDB(this);

        topTitleLeft.setImageResource(R.drawable.nav_left);
        topTitleLeft.setVisibility(View.VISIBLE);
        topTitleLeft.setOnClickListener(this);
        topTitleText.setText("设置");
        setting_exit_bt.setOnClickListener(this);
        rlCleanCache.setOnClickListener(this);

        settingOpinion.setOnClickListener(this);
        settingAbout.setOnClickListener(this);

        settingNoWifiPlay.setOnCheckedChangeListener(this);

        if (SharedPrefHelper.getInstance(this).isLogin()) {
            setting_exit_bt.setVisibility(View.VISIBLE);
        } else {
            setting_exit_bt.setVisibility(View.GONE);
        }
    }

    @Override
    public void initData() {
        if (SharedPrefHelper.getInstance(this).getIsNoWifiPlayDownload()) {
            settingNoWifiPlay.setChecked(false);
        } else {
            settingNoWifiPlay.setChecked(true);
        }
        cleanCache();
    }

    private String cleanCache() {
        String size="";
        try {
            size = DataCleanManager.getTotalCacheSize(this);
            if (!TextUtils.isEmpty(size)) {
                tvCacheSize.setText(size);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return size;
    }


    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.top_title_left:
                onBackPressed();
                break;
            case R.id.setting_exit_bt:
                DialogManager.showNormalDialog(SettingActivity.this, "确定退出登录", "提示", "取消", "确定", new DialogManager.CustomDialogCloseListener() {
                    @Override
                    public void yesClick() {
                        setAliasForUM();
                        //暂停下载
                        AppContext.getInstance().isStart = 0;
                        DownloadExcutor.getInstance(SettingActivity.this).setFlag(false);
                        db.updateState(Constants.STATE_Pause);

                        SharedPrefHelper.getInstance(SettingActivity.this).setIsLogin(false);
                        SharedPrefHelper.getInstance(SettingActivity.this).setUserId("0");
//                        startActivity(intent1);
                        EventBus.getDefault().post(new LogOutEvent(1));
                        setting_exit_bt.setVisibility(View.GONE);
                        finish();
                    }

                    @Override
                    public void noClick() {

                    }
                });
                break;
            case R.id.setting_opinion:
                Intent opinion = new Intent(SettingActivity.this, FeedBackActivity.class);
                startActivity(opinion);
                break;
            case R.id.setting_about:
                Intent intent = new Intent(this, WebViewActivity.class);
                intent.putExtra(com.dongao.mainclient.model.common.Constants.APP_WEBVIEW_TITLE,"关于");
                intent.putExtra(com.dongao.mainclient.model.common.Constants.APP_WEBVIEW_URL,"http://p.m.bjsteach.com/appInfo/about.html");
                startActivity(intent);
                break;
            case R.id.rl_cleanCache:
                String size=cleanCache();
                if(size.equals("0K")){
                    Toast.makeText(SettingActivity.this, "暂无缓存", Toast.LENGTH_SHORT).show();
                }else{
                    DataCleanManager.clearAllCache(this);
                    cleanCache();
                    Toast.makeText(SettingActivity.this, "清除完毕", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }
    }

    public void setAliasForUM(){
        String device_token=SharedPrefHelper.getInstance(SettingActivity.this).getUmDevicetoken();
        if (device_token!=null&&!device_token.isEmpty()){
            if (SharedPrefHelper.getInstance(SettingActivity.this).isLogin()){
                if (NetUtils.checkNet(SettingActivity.this).isAvailable()) {
                    ApiModel apiModel = new ApiModel(new ResultListener() {
                        @Override
                        public void onSuccess(String json) {

                        }

                        @Override
                        public void onError(Exception e) {

                        }
                    });
//                    apiModel.getData(ApiClient.getClient().CommitDevicetoken(ParamsUtils.getInstance(SettingActivity.this).commitDeviceToken(device_token,0)));
                }
            }
        }
    }

    /**
     * 点app图标时提示
     */
    private void downloadDialog(String str, final String url) {
        alertDialog = new MaterialDialog(SettingActivity.this);
        alertDialog.setTitle("提示");
        alertDialog.setMessage("是否确定下载" + str);
        alertDialog.setNegativeButton("取消", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.setPositiveButton("确定", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent();
                intent1.setAction("android.intent.action.VIEW");
                Uri content_url1 = Uri.parse(url);
                intent1.setData(content_url1);
                startActivity(intent1);
            }
        });
        alertDialog.show();

    }

    /**
     * 退出App
     */
    private void exitLogin() {
        mMaterialDialog = new MaterialDialog(this)
                .setTitle("退出登录?")
                .setMessage("确定退出登录？")
                .setPositiveButton("确定", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();
                        SharedPrefHelper.getInstance(SettingActivity.this).setIsLogin(false);
                        SharedPrefHelper.getInstance(SettingActivity.this).setUserId("");

                    }
                })
                .setNegativeButton("取消", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();
                    }
                });
        mMaterialDialog.show();
    }


    @Override
    public void onBackPressed() {
        this.finish();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.setting_noWifi_play_img:
                if (isChecked) {
                    SharedPrefHelper.getInstance(SettingActivity.this).isNoWifiPlayDownload(false);
                } else {
                    SharedPrefHelper.getInstance(SettingActivity.this).isNoWifiPlayDownload(true);
                }
                break;
        }
    }

}
