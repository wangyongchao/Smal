package com.bjjy.mainclient.phone.view.exam.activity.ExamList.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.view.exam.activity.exampaperlist.bean.Knowledge;

import java.util.List;

/**
 * Created by wangyongchao on 2016/05/11
 * */
public class ExamListAdapter extends BaseAdapter{

    private List<Knowledge> list;
    private Context context;
    public ExamListAdapter(Context context){
        this.context=context;
    }


    public void setList(List<Knowledge> list){
        this.list=list;
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder=null;
        if(null==view){
            viewHolder=new ViewHolder();
            view= LayoutInflater.from(context).inflate(R.layout.exam_list_item,null);
            viewHolder.tv_name= (TextView) view.findViewById(R.id.tv_name);
            viewHolder.tv_done= (TextView) view.findViewById(R.id.tv_done);
            viewHolder.tv_total= (TextView) view.findViewById(R.id.tv_total);
            viewHolder.tv_error_number= (TextView) view.findViewById(R.id.tv_error_number);
            viewHolder.tv_right_rate= (TextView) view.findViewById(R.id.tv_right_rate);
            viewHolder.ll_have_done= (LinearLayout) view.findViewById(R.id.ll_have_done);
            view.setTag(viewHolder);

        }else{
            viewHolder= (ViewHolder) view.getTag();
        }
        if (list.get(i).getFinishedQuestions()==0){
            viewHolder.ll_have_done.setVisibility(View.INVISIBLE);
        }else {
            viewHolder.ll_have_done.setVisibility(View.VISIBLE);
        }
        viewHolder.tv_right_rate.setText(list.get(i).getCorrectRate());
        viewHolder.tv_error_number.setText(list.get(i).getCorrectRate());
        viewHolder.tv_name.setText(list.get(i).getExaminationName()+"");
        viewHolder.tv_done.setText(list.get(i).getFinishedQuestions()+"");
        viewHolder.tv_total.setText(list.get(i).getTotalQuestions()+"");

        return view;
    }


    static class ViewHolder{
        TextView tv_name;
        TextView tv_done;
        TextView tv_total;
        TextView tv_error_number;
        TextView tv_right_rate;
        LinearLayout ll_have_done;

    }
}
