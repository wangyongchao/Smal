package com.bjjy.mainclient.phone.view.studybar.message.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bjjy.mainclient.phone.view.studybar.message.bean.MyMessageBean;
import com.bjjy.mainclient.phone.view.studybar.privateteacher.adapter.FootViewHolder;
import com.bjjy.mainclient.phone.R;

import java.util.List;

public class MyMessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_ITEM = 0;
    private static final int TYPE_FOOTER = 1;
    private List<MyMessageBean> mFeedList;
    private Context context;
    private ReclerViewItemClick reclerViewItemClick;
    private boolean isShow=false;

    public MyMessageAdapter(Context context, List<MyMessageBean> feedList) {
        mFeedList = feedList;
        this.context = context;
    }

    public void setNoDataShow(boolean isShow){
        this.isShow=isShow;
    }
    
    public void setOnItemClick(ReclerViewItemClick reclerViewItemClick){
        this.reclerViewItemClick=reclerViewItemClick;
    }

    @Override
    public int getItemViewType(int position) {
        if (position + 1 == getItemCount()) {
            return TYPE_FOOTER;
        } else {
            return TYPE_ITEM;
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       
        if (viewType == TYPE_ITEM) {
            View view = View.inflate(parent.getContext(), R.layout.studybar_mymessage_fg_item, null);
            return new MyMessageViewHolder(view);
        } else if (viewType == TYPE_FOOTER) {
            View view = LayoutInflater.from(context).inflate(R.layout.swiperefresh_item_foot, parent,
                    false);
            return new FootViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof MyMessageViewHolder) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (reclerViewItemClick!=null){
                        reclerViewItemClick.onItemClickListenerPosition(position);
                    }
                }
            });
            MyMessageBean homeModel = mFeedList.get(position);

            ((MyMessageViewHolder)holder).tv_title.setText(homeModel.getTitle());
            ((MyMessageViewHolder)holder).tv_time.setText((homeModel.getCreateDate()));
            ((MyMessageViewHolder)holder).tv_content.setText((homeModel.getContent()));

        }else if (holder instanceof FootViewHolder) {
            if (getItemCount()<10){
                isShow=true;
            }
            if (isShow){
                ((FootViewHolder)holder).no_data.setVisibility(View.VISIBLE);
            }else{
                ((FootViewHolder)holder).no_data.setVisibility(View.GONE);
            }
           
        }
    }

    @Override
    public int getItemCount() {
        return (mFeedList!=null? (mFeedList.size()+1):0);
    }
   public interface ReclerViewItemClick{
        void onItemClickListenerPosition(int position);
    }
}
