package com.bjjy.mainclient.phone.view.download.local.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.view.play.domain.CourseWare;
import com.bjjy.mainclient.phone.widget.swipe.BaseSwipListAdapter;

import java.util.List;

/**
 * Created by wyc on 2016/1/4.
 */
public class OfflineSwipeAdapter extends BaseSwipListAdapter {
    private Context context;
    private List<CourseWare> offlineList;
    public OfflineSwipeAdapter(Context context, List<CourseWare> offlineList){
        this.context=context;
        this.offlineList=offlineList;
    }
    @Override
    public int getCount() {
        return offlineList.size();
    }

    @Override
    public Object getItem(int i) {
        return offlineList.get(i);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = View.inflate(context,
                    R.layout.localcourse_offline_activity_item, null);
            new ViewHolder(convertView);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.name_tv.setText(offlineList.get(position).getVideoName());
        if(offlineList.get(position).getEffectiveStudyTime()==null || offlineList.get(position).getEffectiveStudyTime().equals("")){
             holder.havelearned_tv.setText("该课节还未学习");
        }else{
            if(Integer.parseInt(offlineList.get(position).getEffectiveStudyTime())< Integer.parseInt(offlineList.get(position).getVideoLen())) {
                int min = Integer.parseInt(offlineList.get(position).getEffectiveStudyTime()) / 60;
                int yushu = Integer.parseInt(offlineList.get(position).getEffectiveStudyTime()) % 60;
                 holder.havelearned_tv.setText("已学习" + min + "分" + yushu + "秒");
            }else{
                 holder.havelearned_tv.setText("该课节已学完");
            }
        }
        //ImageLoader.getInstance().displayImage("",holder.picture_iv);
        return convertView;
    }

    class ViewHolder {
        ImageView picture_iv;
        TextView name_tv;
        TextView havelearned_tv;

        public ViewHolder(View view) {
            picture_iv = (ImageView) view.findViewById(R.id.picture_iv);
            name_tv = (TextView) view.findViewById(R.id.name_tv);
            havelearned_tv = (TextView) view.findViewById(R.id.havelearned_tv);
            view.setTag(this);
        }
    }

    @Override
    public boolean getSwipEnableByPosition(int position) {
       /* if(position % 2 == 0){
            return false;
        }*/
        return true;
    }
}
