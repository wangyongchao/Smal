package com.bjjy.mainclient.phone.view.studybar.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.view.exam.activity.exampaperlist.bean.Knowledge;

import java.util.List;

/**
 * Created by wangyongchao on 2016/05/19
 * */
public class StudyBarAdapter extends BaseAdapter{

    private List<Knowledge> list;
    private Context context;
    public StudyBarAdapter(Context context){
        this.context=context;
    }


    public void setList(List<Knowledge> list){
        this.list=list;
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder=null;
        if(null==view){
            viewHolder=new ViewHolder();
            view= LayoutInflater.from(context).inflate(R.layout.study_bar_activity_item,null);
            viewHolder.tv_main_title= (TextView) view.findViewById(R.id.tv_main_title);
            viewHolder.tv_main_time= (TextView) view.findViewById(R.id.tv_main_time);
            viewHolder.tv_sub_title= (TextView) view.findViewById(R.id.tv_sub_title);
            viewHolder.iv_pic= (ImageView) view.findViewById(R.id.iv_pic);
            viewHolder.iv_point= (ImageView) view.findViewById(R.id.iv_point);
            view.setTag(viewHolder);

        }else{
            viewHolder= (ViewHolder) view.getTag();
        }

        return view;
    }


    static class ViewHolder{
        ImageView iv_pic,iv_point;
        TextView tv_main_title,tv_main_time,tv_sub_title;

    }
}
