package com.bjjy.mainclient.phone.view.setting.cache;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.bjjy.mainclient.phone.event.DeleteEvent;
import com.bjjy.mainclient.phone.view.download.local.SelectYearPopupwindow.SelectYearPopwindow;
import com.bjjy.mainclient.phone.view.download.local.bean.YearInfo;
import com.bjjy.mainclient.phone.view.exam.activity.exampaperlist.bean.Course;
import com.bjjy.mainclient.phone.view.exam.activity.myexam.adapter.PopUpWindowAdapter;
import com.bjjy.mainclient.phone.widget.DialogManager;
import com.dongao.mainclient.core.util.StringUtil;
import com.dongao.mainclient.model.bean.play.CourseWare;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.AppContext;
import com.bjjy.mainclient.phone.app.BaseFragmentActivity;
import com.bjjy.mainclient.phone.download.db.DownloadDB;
import com.bjjy.mainclient.phone.view.setting.cache.adapter.CacheCourseAdapter;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by dell on 2016/5/16.
 */
public class CacheActivity extends BaseFragmentActivity implements ViewPager.OnPageChangeListener {

    @Bind(R.id.top_title_left)
    ImageView topTitleLeft;
    @Bind(R.id.top_title_right)
    ImageView topTitleRight;
    @Bind(R.id.top_title_text)
    TextView topTitleText;
    @Bind(R.id.top_title_bar_layout)
    RelativeLayout topTitleBarLayout;

    @Bind(R.id.download_menu_tv)
    TextView downloadMenuTv;
    @Bind(R.id.course_line)
    View courseLine;
    @Bind(R.id.course_menu_tv)
    TextView courseMenuTv;
    @Bind(R.id.download_line)
    View downloadLine;
    @Bind(R.id.cache_vp)
    ViewPager cacheVp;
    @Bind(R.id.tv_pointnum)
    TextView tvPointnum;
    @Bind(R.id.ll_point)
    RelativeLayout llPoint;
    @Bind(R.id.tv_right)
    TextView tv_right;
    @Bind(R.id.ll_top_right)
    LinearLayout ll_top_right;
    @Bind(R.id.iv_right_pop)
    ImageView iv_right_pop;

    private CacheCourseAdapter adapter;
    private View view_now_showLine;
    private List<CourseWare> list;
    private DownloadDB db;
    private List<Course> courses;
    private int postion;
    private SelectYearPopwindow selectYearPopwindow;
    public ArrayList<YearInfo> yearList=new ArrayList<>();
    private YearInfo currenYear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cache_activity);
        ButterKnife.bind(this);
        initView();
        initData();
    }

    @Override
    public void initView() {
        db = new DownloadDB(this);
//        ll_top_right.setVisibility(View.VISIBLE);
        topTitleLeft.setImageResource(R.drawable.nav_left);
        topTitleRight.setImageResource(R.drawable.my_question_title_add);
        topTitleRight.setVisibility(View.INVISIBLE);
        topTitleRight.setImageResource(R.drawable.local_delete_light);
        topTitleLeft.setVisibility(View.VISIBLE);
        topTitleLeft.setOnClickListener(this);
        topTitleRight.setOnClickListener(this);
        topTitleText.setText("我的下载");
        ll_top_right.setOnClickListener(this);
        downloadMenuTv.setOnClickListener(this);
        courseMenuTv.setOnClickListener(this);

        view_now_showLine = courseLine;
        adapter = new CacheCourseAdapter(getSupportFragmentManager());
        cacheVp.setAdapter(adapter);
        cacheVp.addOnPageChangeListener(this);
    }

    @Override
    public void initData() {
        getYearData();
        list = db.findDownloadList(SharedPrefHelper.getInstance(this).getUserId());
        if (list != null && list.size() > 0) {
            llPoint.setVisibility(View.VISIBLE);
            tvPointnum.setText(list.size() + "");
        } else {
            llPoint.setVisibility(View.GONE);
        }

        courses = db.findCourses(SharedPrefHelper.getInstance(this).getUserId());
        if (postion == 0) {
            if (courses.size() > 0) {
                topTitleRight.setVisibility(View.VISIBLE);
            } else {
                topTitleRight.setVisibility(View.INVISIBLE);
            }
        } else {
            if (list.size() > 0) {
                topTitleRight.setVisibility(View.VISIBLE);
            } else {
                topTitleRight.setVisibility(View.INVISIBLE);
            }
        }

//        if(courses!=null && courses.size()>0){
//            if(postion==0){
//                topTitleRight.setVisibility(View.INVISIBLE);
//            }
//        }else{
//            topTitleRight.setVisibility(View.INVISIBLE);
//        }
    }

    private void getYearData() {
        selectYearPopwindow = new SelectYearPopwindow(CacheActivity.this, ll_top_right, iv_right_pop, onItemClickListener);
        String year=SharedPrefHelper.getInstance(this).getCurYear();
        String years=SharedPrefHelper.getInstance(this).getYearList();
        if (!StringUtil.isEmpty(year)){
            currenYear= JSON.parseObject(year,YearInfo.class);
        }
        List<YearInfo> yearInfos=new ArrayList<>();
        if (!StringUtil.isEmpty(years)){
            yearInfos=JSON.parseArray(years,YearInfo.class);
        }
       
        if (yearInfos==null||yearInfos.size()==0) {
            ll_top_right.setVisibility(View.INVISIBLE);
            return;
        }
        yearList.addAll(yearInfos);
        if (currenYear==null&&yearList.size()>0){
            currenYear=yearList.get(0);
            SharedPrefHelper.getInstance(this).setCurYear(JSON.toJSONString(currenYear));
            showCurrentYear(currenYear);
        }
        
    }

    /**
     * 年份的点击事件
     */
    private PopUpWindowAdapter.MainTypeItemClick onItemClickListener = new PopUpWindowAdapter.MainTypeItemClick() {
        @Override
        public void itemClick(int mainPosition, int itemType, int type, int position) {
            setOnItemClick(position);
        }
    };

    public void setOnItemClick(int position) {
        currenYear = yearList.get(position);
        showCurrentYear(currenYear);
        SharedPrefHelper.getInstance(this).setCurYear(JSON.toJSONString(currenYear));
        hideYearPop(true);
    }

    public void hideYearPop(boolean isHide) {
        if (isHide && selectYearPopwindow != null) {
            selectYearPopwindow.dissmissPop();
        }
    }

    public void showCurrentYear(YearInfo currenYear) {
        if (currenYear.getShowYear() == null || currenYear.getShowYear().isEmpty()) {
            tv_right.setText(currenYear.getYearName());
        } else {
            tv_right.setText(currenYear.getShowYear());
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        initData();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_top_right:
                selectYearPopwindow.showPop(yearList, currenYear);
                break;
            case R.id.download_menu_tv:
                setAnimation(downloadLine);
                courseMenuTv.setTextColor(Color.parseColor("#000000"));
                downloadMenuTv.setTextColor(getResources().getColor(R.color.color_primary));
                cacheVp.setCurrentItem(1);
                break;
            case R.id.course_menu_tv:
                setAnimation(courseLine);
                downloadMenuTv.setTextColor(Color.parseColor("#000000"));
                courseMenuTv.setTextColor(getResources().getColor(R.color.color_primary));
                cacheVp.setCurrentItem(0);
                break;
            case R.id.top_title_left:
                finish();
                break;
            case R.id.top_title_right:
                //删除全部课程
                if (postion == 0) {
                    DialogManager.showNormalDialog(this, "确定要全部删除吗", "提示", "取消", "确定",
                            new DialogManager.CustomDialogCloseListener() {
                                @Override
                                public void yesClick() {
                                    db.deleteCourses(SharedPrefHelper.getInstance(CacheActivity.this).getUserId(), courses);
                                    initData();
                                    EventBus.getDefault().post(new DeleteEvent());
                                }

                                @Override
                                public void noClick() {
                                }
                            });
                } else {
                    DialogManager.showNormalDialog(this, "确定要全部删除吗", "提示", "取消", "确定",
                            new DialogManager.CustomDialogCloseListener() {
                                @Override
                                public void yesClick() {
//                                db.deletes(SharedPrefHelper.getInstance(getActivity()).getUserId());
                                    db.deleteCourseWaresDownloading(SharedPrefHelper.getInstance(CacheActivity.this).getUserId(), list);
                                    initData();
                                    EventBus.getDefault().post(new DeleteEvent());
                                }

                                @Override
                                public void noClick() {
                                }
                            });
                }

                break;
        }
    }

    private void setAnimation(View lineView) {
        if (view_now_showLine != lineView) {
            Animation animation = AnimationUtils.loadAnimation(CacheActivity.this, R.anim.play_menu_line_out);
            view_now_showLine.startAnimation(animation);
            view_now_showLine.setVisibility(View.INVISIBLE);
            view_now_showLine = lineView;
            view_now_showLine.setVisibility(View.VISIBLE);
            Animation animation1 = AnimationUtils.loadAnimation(CacheActivity.this, R.anim.play_menu_line_in);
            view_now_showLine.startAnimation(animation1);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        adapter.notifyDataSetChanged();
        this.postion = position;
        if (position == 0) {
            setAnimation(courseLine);
            downloadMenuTv.setTextColor(Color.parseColor("#000000"));
            courseMenuTv.setTextColor(getResources().getColor(R.color.color_primary));
            if (courses != null && courses.size() > 0) {
                topTitleRight.setVisibility(View.VISIBLE);
            } else {
                topTitleRight.setVisibility(View.INVISIBLE);
            }
        } else if (position == 1) {
            setAnimation(downloadLine);
            courseMenuTv.setTextColor(Color.parseColor("#000000"));
            downloadMenuTv.setTextColor(getResources().getColor(R.color.color_primary));
            if (list != null && list.size() > 0) {
                topTitleRight.setVisibility(View.VISIBLE);
            } else {
                topTitleRight.setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        AppContext.getInstance().setHandler(null);
    }
}
