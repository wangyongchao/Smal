package com.bjjy.mainclient.phone.view.play.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.AppContext;
import com.bjjy.mainclient.phone.download.db.DownloadDB;
import com.bjjy.mainclient.phone.download.db.PlayParamsDB;
import com.bjjy.mainclient.phone.event.FreshHandOut;
import com.bjjy.mainclient.phone.utils.NetWorkUtils;
import com.bjjy.mainclient.phone.view.exam.utils.CommenUtils;
import com.bjjy.mainclient.phone.view.play.PlayActivity;
import com.dongao.mainclient.model.bean.play.CwStudyLog;
import com.bjjy.mainclient.phone.view.play.domain.CwStudyLogDB;
import com.bjjy.mainclient.phone.view.play.fragment.uploadBean.PlayParamsBean;
import com.bjjy.mainclient.phone.view.play.fragment.uploadBean.UploadVideo;
import com.bjjy.mainclient.phone.widget.DialogManager;
import com.dongao.mainclient.core.util.FileUtil;
import com.dongao.mainclient.core.util.LogUtils;
import com.dongao.mainclient.model.bean.course.CoursePlay;
import com.dongao.mainclient.model.bean.home.HomeItem;
import com.dongao.mainclient.model.bean.play.CourseWare;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.HomeDB;
import com.dongao.mainclient.model.local.SharedPrefHelper;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import io.vov.vitamio.MediaPlayer;
import io.vov.vitamio.MediaPlayer.OnBufferingUpdateListener;
import io.vov.vitamio.MediaPlayer.OnInfoListener;
import io.vov.vitamio.Vitamio;
import io.vov.vitamio.widget.CenterLayout;
import io.vov.vitamio.widget.MediaController;
import io.vov.vitamio.widget.VideoView;

public class FragmentPlayer extends Fragment implements OnInfoListener, OnBufferingUpdateListener, FragmentPlayView {

    private View view;
    private String paths;
    private Uri uri;
    public VideoView mVideoView;
    public ProgressBar pb;
    //    private TextView downloadRateView, loadRateView;
    private MediaController mediaController;
    private CenterLayout centerLayout;
    private RelativeLayout all;

    private static final int DATA_POST_STARTTIME = 30001;//同步开始学习时间
    private static final int DATA_POST = 20001;//数据同步
    private static final int SWITCH_VIDEO = 1;//切换视频
    private static final int POST_STUDYLOG = 14; //上传数据
    private static final int PROGRESSBAR = 66;//进度条控制
    private static final int NETCHANGEPLAY = 44;//切网播放暂停
    private static final int NETCHANGEPLAYING = 55;//切网播放jixu
    private static final int EXAM_DOWNLOAD = 13;    //下载随堂练习
    private static final int PLAY_ISTART = 131313;
    private static final int SCREEN_PORTRAIT = 1111;
    private static final int SCREEN_LANDSCAPE = 7777;
    private static final int SCREEN_ISLOCK = 9879;
    private DownloadDB db;
    private PlayParamsDB playParamsDB;
    private static final int SEEK_TO = 16;
    private boolean seekto;
    private PlayActivity activity;

    private FragmentPlayPersenter persenter;
    private boolean isInit = true;

    public Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case DATA_POST:
                    long endTime = (long) msg.obj;
                    insertStudyLog(endTime);
                    break;
                case DATA_POST_STARTTIME:
                    long post_startTime = (long) msg.obj;
                    startTime = post_startTime;
                    break;
                case SWITCH_VIDEO:
//                    /****wyc***///切换视频时同步记录上传
//                    insertStudyLog(mVideoView.getCurrentPosition());
//                    doSync();
                    CourseWare courseWare = (CourseWare) msg.obj;
                    playVedio(courseWare);
                    break;
                case POST_STUDYLOG:
                    doSync();
                    break;
                case SEEK_TO:
                    PowerManager pm = (PowerManager) getActivity().getSystemService(Context.POWER_SERVICE);
                    boolean isScreenOn = pm.isScreenOn();
                    if (!isScreenOn) {//防止锁屏了，数据返回后播放的bug
                        return;
                    }
                    if (isStart) {
                        mVideoView.seekTo(reStartTime);
                    } else {
                        if (isInit) {
//                            initSensor();
                            isInit = false;
                        }
                        mVideoView.seekTo(startTime);
                        updataStartData();
                        insertStudyLog(startTime);
                    }
                    seekto = true;
                    if (mediaController.isLock) {
                        mediaController.hide();
                        mediaController.lock.setImageResource(R.drawable.media_btn_lock);
                    }
                    break;
                case NETCHANGEPLAY:
                    if (mVideoView.isPlaying()) {
                        mVideoView.pause();
                    }
                    break;
                case NETCHANGEPLAYING:
                    mVideoView.start();
                    break;
                case EXAM_DOWNLOAD:
                    break;
                case PLAY_ISTART:
                    isStart = false;
                    break;
                case 888:
                    try {
                        int orientation = msg.arg1;
                        if (orientation > 45 && orientation < 135) {

                        } else if (orientation > 135 && orientation < 225) {

                        } else if (orientation > 225 && orientation < 315) {
                            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
//                            sensor_flag = false;
//                            stretch_flag = false;
                        } else if ((orientation > 315 && orientation < 360)
                                || (orientation > 0 && orientation < 45)) {
                            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//                            sensor_flag = true;
//                            stretch_flag = true;

                        }
                    } catch (Exception e) {

                    }
                    break;
                case SCREEN_PORTRAIT:
//                    if (sm != null) {
//                        sm.unregisterListener(listener);
//                    }
//                    stretch_flag = true;
                    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    break;
                case SCREEN_LANDSCAPE:
//                    if (sm != null) {
//                        sm.unregisterListener(listener);
//                    }
//                    stretch_flag = false;
                    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    break;
                case SCREEN_ISLOCK:
                    if (mediaController.isLock) {
//                        if (sm != null) {
//                            sm.unregisterListener(listener);
//                        }
//                        if (sm1 != null) {
//                            sm1.unregisterListener(listener1);
//                        }
                        int rotation = getDisplayRotation(getActivity());
                        if (rotation == 270 && mediaController.isLock) {
                            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
                        } else if (rotation == 90 && mediaController.isLock) {
                            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                        }
                    } else {
//                        if (sm != null) {
//                            sm.registerListener(listener, sensor, SensorManager.SENSOR_DELAY_UI);
//                        }
//                        if (sm1 != null) {
//                            sm1.registerListener(listener1, sensor1, SensorManager.SENSOR_DELAY_UI);
//                        }
                    }
                    break;
                case 9898: //下载
                    CourseWare ware = (CourseWare) msg.obj;
                    NetWorkUtils type = new NetWorkUtils(getActivity());
                    if (type.getNetType() == 0) {
                        Toast.makeText(getActivity(), "请确认网络是否联通", Toast.LENGTH_SHORT).show();
                    } else {
                        persenter.getDownloadUrl(ware);
                    }
                    break;
            }
        }
    };

    public static FragmentPlayer getInstance(Bundle args) {
        FragmentPlayer f = new FragmentPlayer();
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        persenter = new FragmentPlayPersenter();
        persenter.attachView(this);
        AppContext.getInstance().setPlayHandler(handler);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Vitamio.isInitialized(getActivity());
        view = inflater.inflate(R.layout.fragment_player, null);
        db = new DownloadDB(getActivity());
        playParamsDB = new PlayParamsDB(getActivity());
        activity = (PlayActivity) getActivity();
        initStartTime();
        initVideo();
        return view;
    }

    private void initVideo() {
        mVideoView = (VideoView) view.findViewById(R.id.buffer);
        Rect outRect = new Rect();
        getActivity().getWindow().getDecorView().getWindowVisibleDisplayFrame(outRect);
        int mWidth = outRect.width();
        int mHeight = outRect.height();
        CenterLayout.LayoutParams pp = new CenterLayout.LayoutParams(mWidth, mHeight / 3 - 15, 0, 0);
        mVideoView.setLayoutParams(pp);
        centerLayout = (CenterLayout) view.findViewById(R.id.centerlayout);
        all = (RelativeLayout) view.findViewById(R.id.all);

        pb = (ProgressBar) view.findViewById(R.id.probar);
    }

    private boolean flag;

    public void playVedio(CourseWare courseWare) {
        if (courseWare == null) {
            return;
        }
        seekto = false;
        isHaveKeHou = false;
        startTime = 0;
        activity.times.clear();
        activity.urls.clear();
        CwStudyLog logstart = cwStudyLogDB.query(userId, courseWare.getCwId(), courseWare.getClassId());
//        /****wyc***///切换视频时同步记录上传
        if (cw != null) { //说明本身已经有正在播放的视频，把原来的课节记录进行同步
            doSync();
            mVideoView.stopPlayback();
        }
        cw = courseWare;

        if (!TextUtils.isEmpty(cw.getBeginSec())) {
//            startTime = Integer.parseInt(cw.getBeginSec()) * 1000;
            startTime = Integer.parseInt(cw.getBeginSec());
        }

        if (logstart != null) {
            long totalTime = logstart.getTotalTime();
            if (startTime < logstart.getEndTime()) {
                startTime = logstart.getEndTime();
            }
            if (totalTime != 0) {
                if (totalTime - startTime < 15000) {
                    startTime = 0;
                }
            }
        }

        pb.setVisibility(View.VISIBLE);
        CourseWare localCw = db.getDownloadedModel(SharedPrefHelper.getInstance(getActivity()).getUserId(), cw.getClassId(), cw.getCwId());
        if (localCw != null) {
            if (localCw.getState() == Constants.STATE_DownLoaded) {
                flag = true;
            } else {
                flag = false;
            }
        } else {
            flag = false;
        }
//        flag = db.CheckIsDownloaded(SharedPrefHelper.getInstance(getActivity()).getUserId(), cw.getClassId(), cw.getCwId());
        SharedPrefHelper.getInstance(getActivity()).setIsOldPlay(0);
        if (!flag) {
            if (activity.isPlayFromLoacl()) {
                initM3u8(localCw);
                paths = getLocalPlayUrl(localCw);
                activity.setIsPlayFromLocal(false);
//                activity.isPlayLocalNow=true;
                int isold = db.getIsold(cw.getClassId(), cw.getCwId());
//                if (isold == 1) {
//                    SharedPrefHelper.getInstance(getActivity()).setIsOldPlay(1);
//                } else {
//                }
                SharedPrefHelper.getInstance(getActivity()).setIsOldPlay(0);
                play(flag);
            } else {
                paths = getOnlinePlayUrl(cw);// TODO: 2017/11/7  
//                paths = cw.getVideoUrl();
                boolean isPlay = SharedPrefHelper.getInstance(getActivity()).getIsNoWifiPlayDownload();
                NetWorkUtils type = new NetWorkUtils(getActivity());
                if (type.getNetType() == 0) {//无网络
                    DialogManager.showMsgDialog(getActivity(), getResources().getString(R.string.dialog_message_vedio), getResources().getString(R.string.dialog_title_vedio), "确定");
                    pb.setVisibility(View.GONE);
                } else if (type.getNetType() == 2) { //流量
//                    if (isPlay) {
                    if (mVideoView.isPlaying()) {
                        mVideoView.pause();
                    }
                    if (SharedPrefHelper.getInstance(getActivity()).getIsPlayOnet()) {
                        if (isStart) {
                            isStart = false;
                            mVideoView.start();
                        } else {
                            play(flag);
                        }
                    }else{
                        DialogManager.showNormalDialog(getActivity(), getResources().getString(R.string.dialog_warnning_vedio), "提示", "取消", "确定",
                                new DialogManager.CustomDialogCloseListener() {
                                    @Override
                                    public void yesClick() {
//                                        if (isStart) {
//                                            isStart = false;
//                                            mVideoView.start();
//                                        } else {
//                                            play(flag);
//                                        }
                                        play(flag);
                                        SharedPrefHelper.getInstance(getActivity()).setIsPlayOnet(true);
                                    }

                                    @Override
                                    public void noClick() {
                                        pb.setVisibility(View.GONE);
                                    }
                                });
                    }

//                    }
                } else if (type.getNetType() == 1) {//wifi
                    play(flag);
                }
            }
        } else {

            int isold = db.getIsold(cw.getClassId(), cw.getCwId());
            if (isold == 1) {
                SharedPrefHelper.getInstance(getActivity()).setIsOldPlay(1);
                paths = getLocalPlayUrl(localCw);
            } else {
                SharedPrefHelper.getInstance(getActivity()).setIsOldPlay(0);
                initM3u8(localCw);
                paths = getLocalPlayUrl(localCw);
            }
//            activity.isPlayLocalNow=true;
//            paths = "http://localhost:" + Constants.SERVER_PORT + "/" + SharedPrefHelper.getInstance(getActivity()).getUserId() + "_" + cw.getExamId() + "_" + cw.getSubjectId() + "_" + cw.getClassId()+ "_" + cw.getSectionId()+ "_" + cw.getCwId() + "/video/vedio.m3u8";
            play(flag);
        }
    }

    @Override
    public void playError(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    public void play(boolean flag) {
//      paths = "http://localhost:" + Constants.SERVER_PORT + "/" +666 + "/video/video.m3u8";
//      paths = "https://v.dongaocloud.com/test/zjf_zjyk_kj_st_13_854x480_400kbps_m3u8_10/video.m3u8";
//        paths = "http://localhost:12344/5e01bafc2c1642d6971ece28b6d33edd_2017_2017_2017_2017_0/playOnline/video.m3u8";
//        paths = "http://localhost:12344/online.m3u8";
//        persenter.getData();
        PowerManager pm = (PowerManager) getActivity().getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = pm.isScreenOn();
        if (!isScreenOn) {//防止锁屏了，数据返回后播放的bug
            return;
        }

        if (TextUtils.isEmpty(paths)) {
            Toast.makeText(getActivity(), "视频数据错误", Toast.LENGTH_LONG).show();
            return;
        } else {
            if (mediaController == null) {
                mediaController = new MediaController(getActivity(), mVideoView, handler);
            }
            mVideoView.setMediaController(mediaController);
            mVideoView.setOnInfoListener(this);
            mVideoView.setOnBufferingUpdateListener(this);
            mediaController.setCourseWare(cw, flag);
            uri = Uri.parse(paths);
            mVideoView.setVideoURI(uri);
            mVideoView.requestFocus();
        }
    }

    public void seekToSec(long time) {
        mVideoView.seekTo(time);
    }


    //"http://localhost:" + Constants.SERVER_PORT + "/"
    private String getLocalPlayUrl(CourseWare coursew) {
        String localPath = "http://localhost:" + Constants.SERVER_PORT + "/" + SharedPrefHelper.getInstance(getActivity()).getUserId() + "_" + coursew.getExamId() + "_" + coursew.getSubjectId() + "_" + coursew.getClassId() + "_" + coursew.getSectionId() + "_" + coursew.getCwId() + "/video/video.m3u8";
        return localPath;
    }

    private void initM3u8(CourseWare coursew) {// TODO: 2017/11/7  
        String localPath = FileUtil.getDownloadPath(getActivity())  + SharedPrefHelper.getInstance(getActivity()).getUserId() + "_" + coursew.getExamId() + "_" + coursew.getSubjectId() + "_" + coursew.getClassId() + "_" + coursew.getSectionId() + "_" + coursew.getCwId() + "/video/video.m3u8";
        PlayParamsBean playParamsBean = playParamsDB.getParamById(SharedPrefHelper.getInstance(getActivity()).getUserId(), coursew.getCwId());
        String key;
        if (playParamsBean==null)key="";
        else key=playParamsBean.getKey();
        persenter.getKeyTxt("", localPath, key, coursew);
    }

//    private String getLocalPlayUrlNew(CourseWare coursew) {
//        String localPath = "http://localhost:" + Constants.SERVER_PORT + "/" + SharedPrefHelper.getInstance(getActivity()).getUserId() + "_" + coursew.getExamId() + "_" + coursew.getSubjectId() + "_" + coursew.getClassId() + "_" + coursew.getSectionId() + "_" + coursew.getCwId() + "/video/video.m3u8";
//        return localPath;
//    }

    private String getOnlinePlayUrl(CourseWare coursew) {
        String localPath = FileUtil.getDownloadPath(getActivity())  + SharedPrefHelper.getInstance(getActivity()).getUserId() + "_" + coursew.getExamId() + "_" + coursew.getSubjectId() + "_" + coursew.getClassId() + "_" + coursew.getSectionId() + "_" + coursew.getCwId() + "/playOnline/online.m3u8";
        return localPath;
    }

    @Override
    public boolean onInfo(MediaPlayer mp, int what, int extra) {
        switch (what) {
            case MediaPlayer.MEDIA_INFO_BUFFERING_START:
                if (mVideoView.isPlaying()) {
                    mVideoView.pause();
                    mediaController.setIsClickable(false);
                    pb.setVisibility(View.VISIBLE);
//                    downloadRateView.setText("");
//                    loadRateView.setText("");
//                    downloadRateView.setVisibility(View.VISIBLE);
//                    loadRateView.setVisibility(View.VISIBLE);

                }
                break;
            case MediaPlayer.MEDIA_INFO_BUFFERING_END:
                mediaController.setIsClickable(true);
                if (!seekto) {
                    Message msg = Message.obtain();
                    msg.what = SEEK_TO;
                    handler.sendMessage(msg);
                }
                if (!isHaveKeHou) {
                    mVideoView.start();
                }
//                if (!isStart || flag) {
//                    mVideoView.start();
//                }
                pb.setVisibility(View.GONE);
                break;
            case MediaPlayer.MEDIA_INFO_DOWNLOAD_RATE_CHANGED:
//                downloadRateView.setText("" + extra + "kb/s" + "  ");
                break;
        }
        return true;
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
//        loadRateView.setText(percent + "%");
    }

    public void onBack() {
        if (mediaController != null) {
            if (mediaController.isLock) {
                Toast.makeText(getActivity(), "请先解除屏幕锁定", Toast.LENGTH_SHORT).show();
            } else {
//                if (sm != null) {
//                    sm.unregisterListener(listener);
//                }
//                stretch_flag = true;
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }
        }
    }


    /*********
     * 以下为同步相关
     ***********/


    private void initStartTime() {
        /**
         *  点击课程列表中某一项进入课程详情
         *  首先要找到要播放的课节（找出此课程ID对应的数据库中的所有课节，然后通过最后更新的时间排序，最新的一条就是要播放的课件）
         *
         *  1.查找CourseWare数据库，找出endTime的值
         *  2.设置此字段为开始播放时间
         *
         *  所用到的字段主要有
         *  userId 登录用户的Id
         *  mYear 当前选择的年份
         *  courseId 当前课程的Id
         */
        cwStudyLogDB = new CwStudyLogDB(getActivity());
        homeDB = new HomeDB(getActivity());
//        courseWareDB = new CourseWareDB(getActivity());
//        courseDetailDB = new CourseDetailDb(getActivity());
        getParam();
    }

    /**
     * 获取当前年份和其他相关参数
     */
    private void getParam() {
        userId = SharedPrefHelper.getInstance(getActivity()).getUserId();
    }

    private boolean isStart;
    private long reStartTime;

    @Override
    public void onPause() {
        super.onPause();
        isStart = true;
        reStartTime = mVideoView.getCurrentPosition();
        mVideoView.pause();
        /**
         * 在这里插入学习记录，不会有错吧
         * 会
         */
        if (mVideoView == null) return;
        if (mVideoView.getCurrentPosition() > 0) {
//            insertStudyLog(mVideoView.getCurrentPosition());
            doSync();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mediaController != null && mediaController.toExcise) {
            mediaController.toExcise = false;
            isStart = false;
            toNext();
        } else { // if(mediaController!=null)
            if (isStart) {
                playVedio(cw);
            }
        }
    }

    private boolean isHaveKeHou;

    private void toNext() {
        boolean isFinished = cwStudyLogDB.isFinished(SharedPrefHelper.getInstance(getActivity()).getUserId(), cw.getCwId(), cw.getClassId());

        if (activity.isHaveChapter()) {
            int gourp = activity.getPlayingGroupPosition();
            int child = activity.getPlayingChildPosition();

//            activity.courseDetail.getMobileSectionList().get(gourp).getMobileCourseWareList().get(child).setIsPlayFinished(isFinished);
//
//            if (child < activity.courseDetail.getMobileSectionList().get(gourp).getMobileCourseWareList().size() - 1) {
//                if (!activity.courseDetail.getMobileSectionList().get(gourp).getMobileCourseWareList().get(child + 1).getCwId().equals("kehouzuoye")) {
//                    activity.playVedio(activity.courseDetail.getMobileSectionList().get(gourp).getMobileCourseWareList().get(child + 1), gourp, (child + 1));
//                } else {
//                    isHaveKeHou = true;
//                }
//            } else if (gourp < activity.courseDetail.getMobileSectionList().size() - 1) {
//                activity.playVedio(activity.courseDetail.getMobileSectionList().get(gourp + 1).getMobileCourseWareList().get(0), (gourp + 1), 0);
//            }
        } else {
            int postion = activity.getPlayingChildPosition();
            activity.courseWareList.get(postion).setIsPlayFinished(isFinished);
            if (postion < activity.courseWareList.size() - 1) {
                activity.playVedio(activity.courseWareList.get(postion + 1), -1, (postion + 1));

            }
        }
        activity.notifyPlayStatus();

    }

    CwStudyLogDB cwStudyLogDB;
    public long startTime = 0; //开始时间
    CwStudyLog cwLog; //本次学习记录
    public String userId; //当前播放的用户
    public String createdTime; //什么时候开始听的
    private CourseWare cw; //当前播放的视频
    HomeDB homeDB;

    /**
     * 插入同步记录
     *
     * @param endTime 获取最后播放的时间点，单位为毫秒
     *                这是参数只有在用户拖动进度条的时候有用，其余的时间没什么用,直接设置为0即可
     */
    public synchronized void insertStudyLog(long endTime) {
        if (mVideoView == null) return;
        if (endTime == 0) {//所传参数为0  就获取当前最新的播放位置
            endTime = mVideoView.getCurrentPosition();
        }
        LogUtils.d("jumpTime方法 startTime=" + startTime);
        if (Math.abs(endTime - startTime) > 0 && Math.abs(endTime - startTime) < 200000) {//判断只有大于0的时候才保存，要不然保存也没有意义

            cwLog = cwStudyLogDB.query(userId, cw.getCwId(), cw.getClassId());//播放记录会时时变化，因此要提前判断一下
            final CoursePlay coursePlay = JSON.parseObject(cw.getCourseBean(), CoursePlay.class);
            HomeItem homeItem = HomeItem.getNewFromCourse(userId, cw.getSubjectId(), coursePlay.getName(), cw.getClassId(), cw.getChapterNo() + " " + cw.getCwName(), cw.getCwId());
            homeDB.insertOrUpdateCourseWare(homeItem);
            if (cwLog == null) {
                cwLog = new CwStudyLog();
                cwLog.setCwid(cw.getCwId());
                cwLog.setCwName(cw.getCwName());
                cwLog.setCourseId(cw.getClassId());

                cwLog.setStartTime(startTime);
                cwLog.setEndTime(endTime);
                cwLog.setStatus(1);
                cwLog.setUserId(userId);
                cwLog.setSubjectId(cw.getSubjectId());
                cwLog.setTotalTime(mVideoView.getDuration());
//                createdTime = DateUtil.getCurrentTimeInString();// TODO: 2017/11/20  
//                cwLog.setCreatedTime(createdTime);
                cwLog.setEffectiveStudyTime(cw.getEffectiveStudyTime());
                cwLog.setCreatedTime(System.currentTimeMillis() + "");
                cwLog.setLastUpdateTime(System.currentTimeMillis() + "");//DateUtil.getCurrentTimeInString()

                cwLog.setWatchedAt(Math.abs(endTime - startTime));
                cwLog.setNativeWatcheAt(Math.abs(endTime - startTime));
                cwStudyLogDB.insert(cwLog);
            } else {//不为null的话，说明库中已经存在了

                LogUtils.d("startTime=" + startTime);
                if (endTime == 0) {
                    endTime = mVideoView.getCurrentPosition();
                }
                LogUtils.d("endTime=" + endTime);
                cwLog.setWatchedAt(cwLog.getWatchedAt() + Math.abs(endTime - startTime));
                cwLog.setNativeWatcheAt(cwLog.getNativeWatcheAt() + Math.abs(endTime - startTime));
                cwLog.setLastUpdateTime(System.currentTimeMillis() + "");//DateUtil.getCurrentTimeInString()
                cwLog.setEndTime(endTime);
                cwLog.setStartTime(startTime);
                cwLog.setEffectiveStudyTime(cw.getEffectiveStudyTime());
                cwStudyLogDB.update(cwLog);
            }
        }
        //跟新同步本地记录的上次时间
        startTime = endTime;
    }

    private void updataStartData() {
        cwLog = cwStudyLogDB.query(userId, cw.getCwId(), cw.getClassId());
        if (cwLog == null) {
            cwLog = new CwStudyLog();
            cwLog.setCwid(cw.getCwId());
            cwLog.setCwName(cw.getCwName());
            cwLog.setCourseId(cw.getClassId());
            cwLog.setStatus(1);
            cwLog.setUserId(userId);
            cwLog.setSubjectId(cw.getSubjectId());
            cwLog.setEffectiveStudyTime(cw.getEffectiveStudyTime());
            cwLog.setTotalTime(mVideoView.getDuration());
            cwLog.setCreatedTime(System.currentTimeMillis() + "");
            cwLog.setStartData(System.currentTimeMillis() + "");
            cwLog.setLastUpdateTime(System.currentTimeMillis() + "");
            cwStudyLogDB.insert(cwLog);
        } else {
            cwLog.setLastUpdateTime(System.currentTimeMillis() + "");
            cwLog.setEffectiveStudyTime(cw.getEffectiveStudyTime());
            cwLog.setStartData(System.currentTimeMillis() + "");
            cwStudyLogDB.update(cwLog);
        }
    }

    /**
     * 同步上传接口
     */
    private void doSync() {
//        if (NetworkUtil.isNetworkAvailable(getActivity())) {
//            String listenStr = getStudyLogs();
//            if (!TextUtils.isEmpty(listenStr)) {
//                persenter.upLoadVideos(listenStr);
//            }
//        }
    }

    private List<CwStudyLog> studyLogs;

    private String getStudyLogs() {
        UploadVideo uploadVideo = new UploadVideo();
        studyLogs = cwStudyLogDB.queryByUserId(userId);
//        return CommenUtils.getStudyLogs(studyLogs,userId); //换成userCode
        return CommenUtils.getStudyLogs(studyLogs,SharedPrefHelper.getInstance(getActivity()).getUserCode());

//        List<VideoLogs> list = new ArrayList();
//        if (studyLogs != null && studyLogs.size() > 0) {
//            for (int i = 0; i < studyLogs.size(); i++) {
//                VideoLogs log = new VideoLogs();
//                CommenUtils.uploadUtils(log,studyLogs.get(i));
////                log.setCourseId(studyLogs.get(i).getCourseId());
////                log.setCoursewareId(studyLogs.get(i).getCwid());
////                log.setDateBeginTimeStr(studyLogs.get(i).getStartData());
////                log.setDateEndTimeStr(studyLogs.get(i).getLastUpdateTime());
////                log.setMinutesTime(studyLogs.get(i).getWatchedAt() / 1000 + "");
////                log.setSecondEndTime(studyLogs.get(i).getEndTime() / 1000 + "");
////                if (studyLogs.get(i).getNativeWatcheAt() + 20000 >= studyLogs.get(i).getTotalTime()) {
////                    if (studyLogs.get(i).getEndTime() + 20000 >= studyLogs.get(i).getTotalTime()) {
////                        log.setAccomplished("1");
////                    } else {
////                        log.setAccomplished("0");
////                    }
////                } else {
////                    log.setAccomplished("0");
////                }
////                log.setSource("1");
////                log.setCoursewareType("4");
//                list.add(log);
//            }
//            if (list != null && list.size() > 0) {
//                uploadVideo.setUserId(userId);
//                uploadVideo.setCllList(list);
//
//                return JSON.toJSONString(uploadVideo);
//            }
//        }
//        return "";
    }


    private static final int MSG_STUDY_LOG = 8; //同步数据库

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mediaController != null) {  //页面销毁时remove  messages
            mediaController.mHandler.removeMessages(POST_STUDYLOG);
            mediaController.mHandler.removeMessages(MSG_STUDY_LOG);
        }
        mVideoView.stopPlayback();
//        if (sm != null) {
//            sm.unregisterListener(listener);
//        }
//        if (sm1 != null) {
//            sm1.unregisterListener(listener1);
//        }
    }

    @Override
    public void showData(List<Integer> list, List<String> urls) {
        activity.times = list;
        activity.urls = urls;
    }

    @Override
    public String getPath() {
        return paths;
    }

    @Override
    public void setResult(int code) {
        if (code == 1) {
            for (CwStudyLog cwStudyLog : studyLogs) {
                cwStudyLog.setWatchedAt(0);
                cwStudyLogDB.update(cwStudyLog);
            }
        }
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public Context context() {
        return getActivity();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (getActivity() == null || mediaController == null) {
            return;
        }
        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            // 改变布局
            getActivity().getWindow().setFlags(0, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            DisplayMetrics dm = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
            int ScreenW = dm.widthPixels;
            int ScreenH = dm.heightPixels;
            mediaController.setHight(SharedPrefHelper.getInstance(getActivity()).getMiniScreenHight());//mediaController.screen_mini
            mediaController.setGone();
            // centerLayout.setLayoutParams(p);
            CenterLayout.LayoutParams pp = new CenterLayout.LayoutParams(
                    ScreenW, SharedPrefHelper.getInstance(getActivity()).getMiniScreenHight(), 0, 0);
            mVideoView.setLayoutParams(pp);
            EventBus.getDefault().post(new FreshHandOut());
        } else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            // 改变布局
            //去掉Activity上面的状态栏
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            DisplayMetrics dm = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
            int ScreenW = dm.widthPixels;
            int ScreenH = dm.heightPixels;
            mediaController.setHight_land();
            mediaController.setVisible();
            SharedPrefHelper.getInstance(getActivity()).setFullScreenHight(ScreenH);
            CenterLayout.LayoutParams pp = new CenterLayout.LayoutParams(
                    ScreenW, ScreenH, 0, 0);
            mVideoView.setLayoutParams(pp);
        }
    }

//    private boolean sensor_flag = true; // 重力感应的
//    private boolean stretch_flag = true; // 点击的
//    private SensorManager sm;
//    private OrientationSensorListener listener;
//    private Sensor sensor;
//
//    private SensorManager sm1;
//    private Sensor sensor1;
//    private OrientationSensorListener2 listener1;

    private int width = 0;
    private int height = 0;

//    private void initSensor() {
//
//        width = (int) DensityUtil.getWidthInPx(getActivity());
//        height = (int) DensityUtil.getHeightInPx(getActivity());
//
//        // 系统的重力感应  注册监听两个的目的就是其中一个取消了，不影响另外一个
//        sm = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
//        sensor = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
//        listener = new OrientationSensorListener(handler);
//        sm.registerListener(listener, sensor, SensorManager.SENSOR_DELAY_UI);
//
//        // 系统的重力感应2
//        sm1 = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
//        sensor1 = sm1.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
//        listener1 = new OrientationSensorListener2();
//        sm1.registerListener(listener1, sensor1, SensorManager.SENSOR_DELAY_UI);
//    }

//    /**
//     * 感应横竖屏变化
//     */
//    public class OrientationSensorListener implements SensorEventListener {
//        private static final int _DATA_X = 0;
//        private static final int _DATA_Y = 1;
//        private static final int _DATA_Z = 2;
//
//        public static final int ORIENTATION_UNKNOWN = -1;
//
//        private Handler rotateHandler;
//
//        public OrientationSensorListener(Handler handler) {
//            rotateHandler = handler;
//        }
//
//        public void onAccuracyChanged(Sensor arg0, int arg1) {
//            // TODO Auto-generated method stub
//
//        }
//
//        public void onSensorChanged(SensorEvent event) {
//
//            if (sensor_flag != stretch_flag) // 说明了什么？如果两个监听不一致，则说明目前的方向不一致，则继续往下监听，否则没有必要
//            {
//                float[] values = event.values;
//                int orientation = ORIENTATION_UNKNOWN;
//                float X = -values[_DATA_X];
//                float Y = -values[_DATA_Y];
//                float Z = -values[_DATA_Z];
//                float magnitude = X * X + Y * Y;
//                // Don't trust the angle if the magnitude is small compared to
//                // the y value
//                if (magnitude * 4 >= Z * Z) {
//                    //
//                    float OneEightyOverPi = 57.29577957855f;
//                    float angle = (float) Math.atan2(-Y, X) * OneEightyOverPi;
//                    orientation = 90 - (int) Math.round(angle);
//                    // normalize to 0 - 359 range
//                    while (orientation >= 360) {
//                        orientation -= 360;
//                    }
//                    while (orientation < 0) {
//                        orientation += 360;
//                    }
//                }
//                if (rotateHandler != null) {
//                    rotateHandler.obtainMessage(888, orientation, 0)
//                            .sendToTarget();
//                }
//
//            } else {
//                if (getActivity() != null) {
//                    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
//                }
//            }
//        }
//    }
//
//
//    /**
//     * 感应横竖屏变化，此监听的目的
//     *
//     * @author wyc
//     */
//    public class OrientationSensorListener2 implements SensorEventListener {
//        private static final int _DATA_X = 0;
//        private static final int _DATA_Y = 1;
//        private static final int _DATA_Z = 2;
//
//        public static final int ORIENTATION_UNKNOWN = -1;
//
//        public void onAccuracyChanged(Sensor arg0, int arg1) {
//            // TODO Auto-generated method stub
//
//        }
//
//        public void onSensorChanged(SensorEvent event) {
//
//            float[] values = event.values;
//
//            int orientation = ORIENTATION_UNKNOWN;
//            float X = -values[_DATA_X];
//            float Y = -values[_DATA_Y];
//            float Z = -values[_DATA_Z];
//
//            /**
//             *
//             */
//            float magnitude = X * X + Y * Y;
//            // Don't trust the angle if the magnitude is small compared to the y
//            // value
//            if (magnitude * 4 >= Z * Z) {
//                //
//                float OneEightyOverPi = 57.29577957855f;
//                float angle = (float) Math.atan2(-Y, X) * OneEightyOverPi;
//                orientation = 90 - (int) Math.round(angle);
//                // normalize to 0 - 359 range
//                while (orientation >= 360) {
//                    orientation -= 360;
//                }
//                while (orientation < 0) {
//                    orientation += 360;
//                }
//            }
//
//            if (orientation > 225 && orientation < 315) { //
//                sensor_flag = false;
//            } else if ((orientation > 315 && orientation < 360)
//                    || (orientation > 0 && orientation < 45)) { //
//                sensor_flag = true;
//            }
//
//            if (stretch_flag == sensor_flag) { //
//                if (mediaController != null && !mediaController.isLock) {
//                    sm.registerListener(listener, sensor,
//                            SensorManager.SENSOR_DELAY_UI);
//                } else if ((mediaController != null && mediaController.isLock)) {
//                    sm.unregisterListener(listener);
//                }
//            }
//        }
//    }

    /**
     * 获取当前屏幕旋转角度
     *
     * @param activity
     * @return 0表示是竖屏; 90表示是左横屏; 180表示是反向竖屏; 270表示是右横屏
     */
    public int getDisplayRotation(Activity activity) {
        if (activity == null)
            return 0;

        int rotation = activity.getWindowManager().getDefaultDisplay()
                .getRotation();
        switch (rotation) {
            case Surface.ROTATION_0:
                return 0;
            case Surface.ROTATION_90:
                return 90;
            case Surface.ROTATION_180:
                return 180;
            case Surface.ROTATION_270:
                return 270;
        }
        return 0;
    }

}
