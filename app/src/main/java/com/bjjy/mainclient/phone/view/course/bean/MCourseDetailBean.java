package com.bjjy.mainclient.phone.view.course.bean;

import java.io.Serializable;

/**
 * Created by dell on 2017/10/30.
 */

public class MCourseDetailBean implements Serializable{
	private String cwCode;
	private String status;
	private String credit;//时长
	private String courseid;
	private String listenDuration;
	private String teacherName;
	private String courseName;

	public String getCwCode() {
		return cwCode;
	}

	public void setCwCode(String cwCode) {
		this.cwCode = cwCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCredit() {
		return credit;
	}

	public void setCredit(String credit) {
		this.credit = credit;
	}

	public String getCourseid() {
		return courseid;
	}

	public void setCourseid(String courseid) {
		this.courseid = courseid;
	}

	public String getListenDuration() {
		return listenDuration;
	}

	public void setListenDuration(String listenDuration) {
		this.listenDuration = listenDuration;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
}
