package com.bjjy.mainclient.phone.view.exam.activity.myexam;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;

import com.dongao.mainclient.model.bean.exam.ExamRuleInfo;
import com.dongao.mainclient.model.mvp.MvpView;

/**
 * Created by wyc on 2017/6/6.
 */
public interface ExamMineView extends MvpView{
    void initAdapter();
    void showContentView(int type);//0:正常 1：网络错误 2：没有数据
    boolean isRefreshNow();//是否正在刷新
    SwipeRefreshLayout getRefreshLayout();
    Intent getTheIntent();
    void showTopTextTitle(String title);
    void setNoDataMoreShow(boolean isShow);
    void showExamInfo(ExamRuleInfo examRuleInfo);
}
