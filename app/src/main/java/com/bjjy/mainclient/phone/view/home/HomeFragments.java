package com.bjjy.mainclient.phone.view.home;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.AppContext;
import com.bjjy.mainclient.phone.app.BaseFragment;
import com.bjjy.mainclient.phone.event.LogOutEvent;
import com.bjjy.mainclient.phone.event.LoginSuccessEvent;
import com.bjjy.mainclient.phone.scanning.CaptureActivity;
import com.bjjy.mainclient.phone.view.main.HomeView;
import com.bjjy.mainclient.phone.view.setting.WebViewActivity;
import com.bjjy.mainclient.phone.view.studybar.Constant;
import com.bjjy.mainclient.phone.view.user.LoginNewActivity;
import com.bjjy.mainclient.phone.widget.EmptyViewLayout;
import com.bjjy.mainclient.phone.widget.parallaxviewpager.PullZoomListView;
import com.bjjy.mainclient.phone.widget.statusbar.Utils;
import com.dongao.libs.dongaoumengpushlib.constants.PushConstants;
import com.dongao.mainclient.core.util.LogUtils;
import com.dongao.mainclient.model.bean.home.HomeItem;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.umeng.analytics.MobclickAgent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author wyc
 */
public class HomeFragments extends BaseFragment implements HomeView, PullZoomListView.PullToZoomListViewListener, PullZoomListView.ScrollDistanceListener {
    private String mTitles[] = {"坚持 是成就你成功的基础", "不要犹豫 开始做题！", "生活有一种英雄主义，经历错题后依然做题", "不是天生丽质，只能天生励志","没什么胜利可言，挺住意味着一切",
            "种一棵树最好的时间是十年前，其次是现在。","一切都会好起来的，即使不是在今天","成功比你想象的慢，但规模可能比想象的大"};
    private View mRootView;

    @Bind(R.id.top_title_left)
    ImageView top_title_left;

    @Bind(R.id.top_title_right)
    ImageView top_title_right;

    @Bind(R.id.top_title_text)
    TextView top_title_text;

    @Bind(R.id.recyclerView)
    PullZoomListView mRecyclerView;

    private TimeLineAdapter mTimeLineAdapter;
    private List<HomeItem> mList;

    HomesPersenter homePersenter;

    @Bind(R.id.status_bar_fix)
    View status_bar_fix;

    // @Bind(R.id.top_title_bar_view)
    View top_title_bar_view;

    private EmptyViewLayout mEmptyLayout;

    int headHeight = 200;

    private boolean isLoad = false;

    private boolean mHasLoadedOnce = false;
    private boolean isLogin = false;
    private Random random;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (this.isVisible()) {
            if (isVisibleToUser && !mHasLoadedOnce && mList == null) {
                initData();
            }
        }
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setUserVisibleHint(true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.unbind(this);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (random == null) {
            random = new Random();
        }
        int num = random.nextInt(7);
        showHomeTitle(mTitles[num]);
        if (mRecyclerView != null) {
            mRecyclerView.reFreshTime();
        }
//        if (mHasLoadedOnce && isLogin != SharedPrefHelper.getInstance(getActivity()).isLogin()) {
//            isLogin = SharedPrefHelper.getInstance(getActivity()).isLogin();
        homePersenter.getData();
//        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.home_fragments, container, false);
        }
        ButterKnife.bind(this, mRootView);
        ViewGroup parent = (ViewGroup) mRootView.getParent();
        if (parent != null) {
            parent.removeView(mRootView);
        }

        homePersenter = new HomesPersenter();
        homePersenter.attachView(this);
        initView();
        return mRootView;
    }

    @Override
    public void initView() {
        random = new Random();
        top_title_left.setImageResource(R.drawable.nav_xkzx);
        top_title_right.setImageResource(R.drawable.nav_sweep);
        top_title_right.setVisibility(View.INVISIBLE);
        top_title_left.setVisibility(View.INVISIBLE);
        top_title_left.setOnClickListener(this);
        top_title_right.setOnClickListener(this);
		top_title_text.setOnClickListener(this);
        top_title_text.setText("");
        top_title_bar_view = mRootView.findViewById(R.id.top_title_bar_view);

        setTranslucentStatus();

        mRecyclerView.setPullToZoomListViewListener(this);
        mRecyclerView.setLoadFinish(true);
        mTimeLineAdapter = new TimeLineAdapter(getActivity());
        headHeight = getActivity().getResources().getDimensionPixelSize(R.dimen.imageview_height);

        mEmptyLayout = new EmptyViewLayout(getActivity(), mRecyclerView);
        mEmptyLayout.setErrorButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initData();
            }
        });

    }

    @TargetApi(19)
    public void setTranslucentStatus() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            //当状态栏透明后，内容布局会上移，这里使用一个和状态栏高度相同的view来修正内容区域
            status_bar_fix.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Utils.getStatusHeight(getActivity())));
            status_bar_fix.setAlpha(1);
        }
    }

    @Override
    public void initData() {
        if (random==null){
            random=new Random();
        }
        int num=random.nextInt(7);
        showHomeTitle(mTitles[num]);
        mHasLoadedOnce = true;
        isLogin = SharedPrefHelper.getInstance(getActivity()).isLogin();
//        showLoading();
        mList = new ArrayList<>();
        homePersenter.getData();
    }


    @Override
    public void setData(List<HomeItem> result) {
        this.mList = result;
        mTimeLineAdapter.setList(result);
        if (!isLoad) {
            // mRecyclerView.setProgress(false);
            hideLoading();
            isLoad = true;
            top_title_bar_view.setAlpha(0);
            status_bar_fix.setAlpha(0);
            mRecyclerView.setAdapter(mTimeLineAdapter);
            mTimeLineAdapter.notifyDataSetChanged();
        } else {
            mRecyclerView.setLoadFinish(true);
            mTimeLineAdapter.notifyDataSetChanged();
        }
    }


    public int getScrollY() {
        View c = mRecyclerView.getChildAt(0);
        if (c == null) {
            return 0;
        }
        int firstVisiblePosition = mRecyclerView.getFirstVisiblePosition();
        if (firstVisiblePosition > 0) {
            return 900;
        }
        int top = c.getTop();

        return -top + firstVisiblePosition * c.getHeight();
    }

    @Override
    public void onScollTo() {
        // LogUtils.d("getScrollY="+getScrollY()+"height="+headHeight);
        float alpha = 1.0f * getScrollY() / headHeight;
        top_title_bar_view.setAlpha(alpha);
        //注意头部局的颜色也需要改变
        status_bar_fix.setAlpha(alpha);
    }

    @Override
    public void onLoadRefresh() {
        if (!mRecyclerView.isRefreshing()){
            mRootView.postDelayed(new Runnable() { // 拉伸恢复的动画是500ms,这里延迟500再请求
                public void run() {
                    isLoad = false;
                    homePersenter.refreshData(true);
                }
            }, 500);
        }
    }

    @Override
    public void onLoadMore() {
        mRecyclerView.setLoadFinish(true);
//        homePersenter.loadMore();
    }

    @Override
    public void setLoadFinish() {
        mRecyclerView.setLoadFinish(true);
        mTimeLineAdapter.notifyDataSetChanged();
    }

    @Override
    public void showLoading() {
        if (mEmptyLayout != null) {
            mEmptyLayout.showLoading();
        }
    }

    @Override
    public void hideLoading() {
        if (mEmptyLayout != null) {
            mEmptyLayout.showContentView();
        }
    }

    @Override
    public void showRetry() {

    }

    @Override
    public TimeLineAdapter getAdapter() {
        return mTimeLineAdapter;
    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {
        Toast.makeText(getActivity(), message + "", Toast.LENGTH_SHORT).show();
        showProgress(false);
    }

    @Override
    public void showNetError() {
        try{
            Toast.makeText(getActivity(), "无网络连接", Toast.LENGTH_SHORT).show();
        }catch (Exception e){}
    }

    @Override
    public Context context() {
        return getActivity();
    }

    @Override
    public void showLogin() {
        //userViewManager.showUserCenterWindow();
        Intent intent = new Intent(getActivity(), LoginNewActivity.class);
        startActivity(intent);
    }

    @Override
    public void showProgress(boolean visible) {
        mRecyclerView.setProgress(visible);
    }

    @Override
    public AppContext getApplicationContext() {
        return appContext;
    }

    @Override
    protected void loginSuccessed() {

    }

    public void showHomeTitle(String title){
        if (mRecyclerView!=null){
            mRecyclerView.setHomeTitle(title);
        }
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.top_title_left:
//                if (SharedPrefHelper.getInstance(getActivity()).isLogin()) {
                    MobclickAgent.onEvent(getActivity(), PushConstants.MAIN_TO_BUY);
                    Intent intent = new Intent(getActivity(), WebViewActivity.class);
                    intent.putExtra(Constants.APP_WEBVIEW_TITLE, "选课购买");
                    String url="http://192.168.22.153:8082/appInfo/toApp.html";
                    intent.putExtra(Constants.APP_WEBVIEW_URL, url);
                    startActivity(intent);
//                } else {
//                    // userViewManager.showUserCenterWindow();
//                    Intent intent = new Intent(getActivity(), LoginNewActivity.class);
//                    startActivity(intent);
//                }
                break;
            case R.id.top_title_right:
                Intent scan = new Intent(getActivity(), CaptureActivity.class);
                startActivity(scan);
                break;
            case R.id.top_title_text:
                break;
            default:
                break;
        }
    }

    @Override
    public void onScrollDistanceChanged(int delta, int total) {
        LogUtils.d("delta==" + delta + "total==" + total);
    }
    /**判断当前显示的页面*/
    @Override
    public void showCurrentView(int type) {
        showProgress(false);
        if (type == Constant.VIEW_TYPE_0) {
            mEmptyLayout.showContentView();
        } else if (type == Constant.VIEW_TYPE_1) {
            mEmptyLayout.showNetErrorView();
        } else if (type == Constant.VIEW_TYPE_2) {
            mEmptyLayout.showEmpty();
        } else {
            mEmptyLayout.showError();
        }
    }


    @Subscribe
    public void onEventAsync(LoginSuccessEvent event) {
        if (isLogin != SharedPrefHelper.getInstance(getActivity()).isLogin()) {
            isLogin = true;
            showProgress(true);
            homePersenter.getData();
        }
    }
    @Subscribe
    public void onEventAsync(LogOutEvent event) {
        if (isLogin != SharedPrefHelper.getInstance(getActivity()).isLogin()) {
            isLogin = false;
            showProgress(true);
            homePersenter.getData();
        }
    }

   /* @Subscribe
    public void onEventMainThread(LogOutEvent event)
    {
        isLogin = false;
    }*/

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);// 反注册EventBus
    }

}
