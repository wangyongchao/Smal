package com.bjjy.mainclient.phone.view.zb;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseFragmentActivity;
import com.bjjy.mainclient.phone.event.PushMsgNotification;
import com.bjjy.mainclient.phone.view.download.local.SelectYearPopupwindow.SelectYearPopwindow;
import com.bjjy.mainclient.phone.view.download.local.bean.YearInfo;
import com.bjjy.mainclient.phone.view.exam.activity.myexam.adapter.PopUpWindowAdapter;
import com.bjjy.mainclient.phone.view.studybar.Constant;
import com.bjjy.mainclient.phone.view.studybar.view.RefreshLayout;
import com.bjjy.mainclient.phone.view.zb.adapter.LivePlayAdapter;
import com.bjjy.mainclient.phone.widget.EmptyViewLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by wyc on 2016/6/6.
 */
public class LivePlayActivity extends BaseFragmentActivity implements LivePlayView, LivePlayAdapter.LivePlayDataBeanItemClickListener {
	@Bind(R.id.top_title_left)
	ImageView top_title_left;
	@Bind(R.id.top_title_right)
	ImageView top_title_right;
	@Bind(R.id.top_title_text)
	TextView top_title_text;
	@Bind(R.id.content_ll)
	LinearLayout content_ll;
	@Bind(R.id.swipe_container)
	SwipeRefreshLayout swipe_container;
	@Bind(R.id.rv_course)
	RecyclerView rv_course;
	@Bind(R.id.tv_right)
	TextView tv_right;
	@Bind(R.id.ll_top_right)
	LinearLayout ll_top_right;
	@Bind(R.id.iv_right_pop)
	ImageView iv_right_pop;
	private SelectYearPopwindow selectYearPopwindow;
	private LivePlayAdapter localCourseAdapter;

	@OnClick(R.id.top_title_left)
	void onBackClick() {
		onBackPressed();
	}

	private LivePlayPercenter privateTeacherPercenter;
	private EmptyViewLayout mEmptyLayout;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.live_play_activity);
		ButterKnife.bind(this);
		if (!EventBus.getDefault().isRegistered(this)) {
			EventBus.getDefault().register(this);
		}
		privateTeacherPercenter = new LivePlayPercenter();
		privateTeacherPercenter.attachView(this);
		initView();
		initData();
	}

	@Override
	public void initView() {
		ll_top_right.setOnClickListener(this);
		top_title_left.setVisibility(View.VISIBLE);
		top_title_right.setVisibility(View.INVISIBLE);
		ll_top_right.setVisibility(View.VISIBLE);
		iv_right_pop.setVisibility(View.VISIBLE);
		top_title_text.setText(getResources().getText(R.string.live_play_list_top));
		mEmptyLayout = new EmptyViewLayout(this, content_ll);
		mEmptyLayout.setErrorButtonClickListener(mErrorClickListener);
		mEmptyLayout.setEmptyButtonClickListener(emptyButtonClickListener);
		rv_course.setLayoutManager(new LinearLayoutManager(rv_course.getContext(), LinearLayoutManager.VERTICAL, false));
	}

	/**
	 * 年份的点击事件
	 */
	private PopUpWindowAdapter.MainTypeItemClick onItemClickListener = new PopUpWindowAdapter.MainTypeItemClick() {
		@Override
		public void itemClick(int mainPosition, int itemType, int type, int position) {
			privateTeacherPercenter.setOnItemClick(position);
		}
	};

	/**
	 * 错误监听
	 */
	private View.OnClickListener mErrorClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
		}
	};
	/**
	 * 无数据监听
	 */
	private View.OnClickListener emptyButtonClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View view) {
			//showAppMsg("显示kong");
		}
	};

	@Override
	public void initData() {
		mEmptyLayout.showLoading();
		selectYearPopwindow = new SelectYearPopwindow(this, ll_top_right, iv_right_pop, onItemClickListener);
		privateTeacherPercenter.initData();
	}

	@Override
	public void showLoading() {
	}

	@Override
	public void hideLoading() {
	}

	@Override
	public void showRetry() {

	}

	@Override
	public void hideRetry() {

	}

	@Override
	public void showError(String message) {

	}

	@Override
	public Context context() {
		return LivePlayActivity.this;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.tv_right:
			case R.id.ll_top_right:
				selectYearPopwindow.showPop(privateTeacherPercenter.yearList, privateTeacherPercenter.currYear);
				break;
		}

	}

	@Override
	public void initAdapter() {
		if (localCourseAdapter == null) {
			localCourseAdapter = new LivePlayAdapter(this, privateTeacherPercenter.courseList);
			localCourseAdapter.setLivePlayDataBeanClickListener(this);
			rv_course.setAdapter(localCourseAdapter);
		} else {
			localCourseAdapter.notifyDataSetChanged();
		}
	}

	@Override
	public void showContentView(int type) {
		if (type == Constant.VIEW_TYPE_0) {
			mEmptyLayout.showContentView();
		} else if (type == Constant.VIEW_TYPE_1) {
			mEmptyLayout.showNetErrorView();
		} else if (type == Constant.VIEW_TYPE_2) {
			mEmptyLayout.showEmpty();
		} else if (type == Constant.VIEW_TYPE_3) {
			mEmptyLayout.showError();
		}
		hideLoading();
	}

	@Override
	public boolean isRefreshNow() {
		return false;
	}

	@Override
	public RefreshLayout getRefreshLayout() {
		return null;
	}

	@Override
	public Intent getTheIntent() {
		return getIntent();
	}

	@Override
	public void finishActivity() {
		onBackPressed();
	}

	@Override
	public void showCurrentYear(YearInfo currYear) {
		if (currYear.getShowYear() == null || currYear.getShowYear().isEmpty()) {
			tv_right.setText(currYear.getYearName() + "年");
		} else {
			tv_right.setText(currYear.getShowYear());
		}
	}

	@Override
	public void hideYearPop(boolean isHide) {
		if (isHide && selectYearPopwindow != null) {
			selectYearPopwindow.dissmissPop();
		}
	}

	@Override
	public void loadMoreStatus(int status) {
		
	}

	@Override
	public void showTopTitle(String title) {
		top_title_text.setText(title);
	}

	@Override
	public void setNoDataMoreShow(boolean isShow) {
	}


	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

	@Subscribe
	public void onEventAsync(PushMsgNotification event) {
		initData();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		EventBus.getDefault().unregister(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
	}


	@Override
	public void LivePlayDataBeanClick(int position, long l) {

	}
}
