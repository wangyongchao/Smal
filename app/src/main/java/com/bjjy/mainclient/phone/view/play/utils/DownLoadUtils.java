package com.bjjy.mainclient.phone.view.play.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import android.content.Context;
import android.os.Handler;

/**
 * 下载相关的工具类
 * @author fengzongwei
 *
 */
public class DownLoadUtils {

	private Context context;
	private DownLoadCallBack callBack;
	
	/** 最大连接数  */
	public final static int MAX_TOTAL_CONNECTIONS = 128;
	/** 获取连接的最大等待时间 */
	public final static int WAIT_TIMEOUT = 60000;
	/** 每个路由最大连接数 */
	public final static int MAX_ROUTE_CONNECTIONS = 64;
	/** 连接超时时间 */
	public final static int CONNECT_TIMEOUT = 10000;
	/** 读取超时时间 */
	public final static int READ_TIMEOUT = 10000;
	
	private static DefaultHttpClient defaultHttpClient;
	
	private static final int CODE_SUCCESS = 1,CODE_ERROR = 2;
	
	private Handler handler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case CODE_SUCCESS:
				callBack.onDownLoadFinished();
				break;
			case CODE_ERROR:
				callBack.onDownLoadError();
				break;
			default:
				break;
			}
		};
	};
	
	public DownLoadUtils(Context context,DownLoadCallBack callBack){
		this.context = context;
		this.callBack = callBack;
		if (null == defaultHttpClient) {
			// 初始化工作
			HttpParams params = new BasicHttpParams();
			//params.setParameter("REFERER", "android_phone_kq://dongaoss.com");
			// 设置最大连接数
			ConnManagerParams.setMaxTotalConnections(params, MAX_TOTAL_CONNECTIONS);
			// 设置获取连接的最大等待时间
			ConnManagerParams.setTimeout(params, WAIT_TIMEOUT);
			// 设置每个路由最大连接数
			ConnPerRouteBean connPerRoute = new ConnPerRouteBean(MAX_ROUTE_CONNECTIONS);
			ConnManagerParams.setMaxConnectionsPerRoute(params, connPerRoute);
			// 设置连接超时时间
			HttpConnectionParams.setConnectionTimeout(params, CONNECT_TIMEOUT);
			// 设置读取超时时间
			HttpConnectionParams.setSoTimeout(params, READ_TIMEOUT);

			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
			HttpProtocolParams.setContentCharset(params, HTTP.DEFAULT_CONTENT_CHARSET);
			HttpProtocolParams.setUseExpectContinue(params, true);

			SchemeRegistry schReg = new SchemeRegistry();
			schReg.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
			schReg.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));

			ClientConnectionManager conManager = new ThreadSafeClientConnManager(params, schReg);

			defaultHttpClient = new DefaultHttpClient(conManager, params);
		
		}
	}
	
	/**
	 * 下载m3u8文件的方法
	 * @param videoId 对应的视频Id
	 * @param url 对应的下载地址
	 */
	public void downM3u8ToFile(final String videoId,final String url){
		final File m3u8File = new File(getPath(videoId,url));
		new Thread(){
			public void run() {
				HttpGet get = new HttpGet(url);
//				get.addHeader("user-agent", user_agent);
				//get.setHeader("refer","android_phone_kq://bjsteach.com"); 
				HttpResponse response = null;
				FileWriter wf = null;
				try {
					response = defaultHttpClient.execute(get);

					int status = response.getStatusLine().getStatusCode();
					if (status == 200) {
						String m3u8String = EntityUtils.toString(response.getEntity());
						// 得到下载路径 视频和讲义分别放入不同目录
						wf = new FileWriter(m3u8File);
						wf.write(m3u8String);
						handler.sendEmptyMessage(CODE_SUCCESS);
					}else{
						handler.sendEmptyMessage(CODE_ERROR);
					}
				} catch (Exception e) {
					e.printStackTrace();
					handler.sendEmptyMessage(CODE_ERROR);
				} finally {
					if (wf != null) {
						try {
							wf.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}

				}
			};
		}.start();
	}
	
	/**
	 * 得到存储路径
	 * 
	 * @param vid  当前m3u8对应的视频id
	 * @param url  当前要下载数据对应的url
	 * @return
	 */
	private String getPath(String vid,String url) {
		if (url == null || url.equals(""))
			return "";

		int index = url.lastIndexOf(".");
		String prefix = url.substring(index + 1);
		if (prefix.equals("m3u8")) {
//			return FileUtil.getDownloadPath(context) + vid + "/" + FileUtil.VIDEOPATH;
			return FileUtils.getDownloadPath(context) + vid + ".m3u8";
		} 
//		else {
//			return FileUtil.getDownloadPath(context) + vid + "/" + FileUtil.LECTURE;
//		}
		return "";

	}
	
	/**
	 * 获取已经下载好的m3u8文件地址
	 * @param videoId
	 * @return
	 */
	public static String getDownLoadedM3u8FilePath(String videoId){
		return "http://localhost:"
				+ Constans.port + "/" +videoId + ".m3u8";
	}
	
	/**
	 * 下载完成后的回调
	 * @author dell
	 *
	 */
	public interface DownLoadCallBack{
		void onDownLoadFinished();//下载成功
		void onDownLoadError();//下载失败
	}
	
	
}
