package com.bjjy.mainclient.phone.view.classroom.liveplay.bean;

import com.yunqing.core.db.annotations.Id;
import com.yunqing.core.db.annotations.Table;

import java.io.Serializable;

/**
 * 答题记录
 * wyc
 */

@Table(name="t_livesplay_log")
public class LivesPlayBean implements Serializable {
    @Id
    private int dbId;
    private String channelId; //直播ID
    private String teachingPlan;//计划ID
    private String channelStartTimes;//直播开始时间
    private String channelEndTimes;//直播结束时间
    private String tacherName;//老师名称
    private int isPay;//1.收费，0.免费
    private String cst;//免费则不显示价格
    private int status;//0.未直播  1.直播中 2.已结束
    private String subheading;//副标题
    private String curse;//计划简介

    public int getDbId() {
        return dbId;
    }

    public void setDbId(int dbId) {
        this.dbId = dbId;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getTeachingPlan() {
        return teachingPlan;
    }

    public void setTeachingPlan(String teachingPlan) {
        this.teachingPlan = teachingPlan;
    }

    public String getChannelStartTimes() {
        return channelStartTimes;
    }

    public void setChannelStartTimes(String channelStartTimes) {
        this.channelStartTimes = channelStartTimes;
    }

    public String getChannelEndTimes() {
        return channelEndTimes;
    }

    public void setChannelEndTimes(String channelEndTimes) {
        this.channelEndTimes = channelEndTimes;
    }

    public String getTacherName() {
        return tacherName;
    }

    public void setTacherName(String tacherName) {
        this.tacherName = tacherName;
    }

    public int getIsPay() {
        return isPay;
    }

    public void setIsPay(int isPay) {
        this.isPay = isPay;
    }

    public String getCst() {
        return cst;
    }

    public void setCst(String cst) {
        this.cst = cst;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getSubheading() {
        return subheading;
    }

    public void setSubheading(String subheading) {
        this.subheading = subheading;
    }

    public String getCurse() {
        return curse;
    }

    public void setCurse(String curse) {
        this.curse = curse;
    }
}
