package com.bjjy.mainclient.phone.view.main;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.widget.coverflow.LoopPagerAdapter;
import com.bjjy.mainclient.phone.widget.coverflow.RollPagerView;

/**
 * Created by wyc on 16/6/2.
 */
public class TestLoopAdapter extends LoopPagerAdapter {
    private int[] imgs = {
            R.drawable.pic_banner1,
            R.drawable.pic_banner2,
            R.drawable.pic_banner1,
            R.drawable.pic_banner2,
            R.drawable.pic_banner1,
    };
    private int count = imgs.length;

    public void add(){
        Log.i("RollViewPager","Add");
        count++;
        if (count>imgs.length)count = imgs.length;
        notifyDataSetChanged();
    }
    public void minus(){
        Log.i("RollViewPager","Minus");
        count--;
        if (count<1)count=1;
        notifyDataSetChanged();
    }

    public TestLoopAdapter(RollPagerView viewPager) {
        super(viewPager);
    }

    @Override
    public View getView(ViewGroup container, int position) {
        //ImageView view = new ImageView(container.getContext());
       /* RoundedImageView riv = new RoundedImageView(container.getContext());
        riv.setScaleType(ImageView.ScaleType.CENTER);
        riv.setCornerRadius((float) 30);
        riv.setBorderWidth((float) 1);
        riv.setBorderColor(Color.TRANSPARENT);
        riv.mutateBackground(true);
        riv.setImageResource(imgs[position]);
        riv.setOval(false);
        riv.setTileModeX(Shader.TileMode.REPEAT);
        riv.setTileModeY(Shader.TileMode.REPEAT);
        riv.setImageResource(imgs[position]);
        riv.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
*/
            /*Transformation transformation = new RoundedTransformationBuilder()
                    .borderColor(Color.BLACK)
                    .borderWidthDp(3)
                    .cornerRadiusDp(30)
                    .oval(false)
                    .build();

            Picasso.with(context)
                    .load(url)
                    .fit()
                    .transform(transformation)
                    .into(imageView);*/
        return null;
    }

    @Override
    public int getRealCount() {
        return count;
    }

}
