package com.bjjy.mainclient.phone.widget;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.view.main.MainActivity;

/**
 * Created by fengzongwei on 2016/1/6.
 * 弹出dialog的管理类
 */
public class DialogManager {

    private static Dialog dialog;
    private static final String LOGINERRORMSG = "账号已在其他设备登录\r\n为了您的账号安全，请您重新登录";
    /**
     * 只是简单的弹出提示信息窗口，关闭时并没有其他响应(只有确定按钮)
     * @param context
     * @param msg 所要弹出的提示信息内容
     * @param confirmText 确定按钮显示的文字信息
     */
    public static void showMsgDialog(Context context,String msg,String title,String confirmText){
        if(dialog == null || !dialog.isShowing()) {
            View view = LayoutInflater.from(context).inflate(R.layout.widget_custom_dialog,
                    null);
            TextView textView = (TextView) view.findViewById(R.id.custom_dialog_content_tv);
            textView.setText(msg);
            TextView textView_title = (TextView) view.findViewById(R.id.custom_dialog_title_tv);
            textView_title.setVisibility(View.VISIBLE);
            if (title != null && !title.equals(""))
                textView_title.setText(title);
            TextView textView_close = (TextView) view.findViewById(R.id.custom_dialog_confirm_tv);
            if (confirmText != null && !confirmText.equals(""))
                textView_close.setText(confirmText);
            textView_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            dialog = new Dialog(context, R.style.customDialog);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            dialog.setContentView(view);
            dialog.show();
        }
    }

    /**
     * 弹出账号异地登录的提示窗口，并且点击确定时打开登录页面(只有确定按钮)
     * @param activity 当前需要弹出此dialog的activity
     */
    public static void showUserLoginOtherPlace(final Activity activity){
        if(dialog == null || !dialog.isShowing()) {
            View view = LayoutInflater.from(activity).inflate(R.layout.widget_custom_dialog,
                    null);
            TextView textView = (TextView) view.findViewById(R.id.custom_dialog_content_tv);
            textView.setText(LOGINERRORMSG);
            TextView textView_close = (TextView) view.findViewById(R.id.custom_dialog_confirm_tv);
            textView_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    SharedPrefHelper.getInstance(activity).setIsLogin(false);
                    Intent intent = new Intent(activity, MainActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    activity.startActivity(intent);
                }
            });
            dialog = new Dialog(activity, R.style.customDialog);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            dialog.setContentView(view);
            dialog.show();
        }
    }

    /**
     * 弹出显示自定义信息，并且点击确定后能进行自定义响应处理的dialog(只有确定按钮)
     * @param context
     * @param msg 消息内容
     * @param title  消息title
     * @param clickListener 进行自定义响应的接口
     */
    public static void showMsgWithCloseAction(final Context context,String msg,String title,final CustomDialogCloseListener clickListener){
        if(dialog == null || !dialog.isShowing()) {
            View view = LayoutInflater.from(context).inflate(R.layout.widget_custom_dialog,
                    null);
            TextView textView = (TextView) view.findViewById(R.id.custom_dialog_content_tv);
            textView.setText(msg);
            TextView textView_title = (TextView) view.findViewById(R.id.custom_dialog_title_tv);
            textView_title.setVisibility(View.VISIBLE);
            if (title != null && !title.equals(""))
                textView_title.setText(title);
            TextView textView_close = (TextView) view.findViewById(R.id.custom_dialog_confirm_tv);
            textView_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    clickListener.yesClick();
                }
            });
            dialog = new Dialog(context, R.style.customDialog);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            dialog.setContentView(view);
            dialog.show();
        }
    }

    /**
     * 弹出普通的dialog，具有取消和确定按钮
     * @param activity
     * @param msg 消息内容
     * @param title 消息title
     * @param clickListener 确定按钮的回调
     */
    public static void showNormalDialog(final Activity activity,String msg, String title,String cancleText,String confirmText,final CustomDialogCloseListener clickListener){
        if(dialog == null || !dialog.isShowing()) {
            View view = LayoutInflater.from(activity).inflate(R.layout.widget_custom_dialog,
                    null);
            TextView textView = (TextView) view.findViewById(R.id.custom_dialog_content_tv);
            textView.setText(msg);
            TextView textView_title = (TextView) view.findViewById(R.id.custom_dialog_title_tv);
            textView_title.setVisibility(View.VISIBLE);
            if (title != null && !title.equals(""))
                textView_title.setText(title);
            TextView textView_close = (TextView) view.findViewById(R.id.custom_dialog_confirm_tv);
            if (confirmText != null && !confirmText.equals(""))
                textView_close.setText(confirmText);
            textView_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    clickListener.yesClick();
                }
            });
            TextView textView_cancle = (TextView) view.findViewById(R.id.custom_dialog_cancle_tv);
            textView_cancle.setVisibility(View.VISIBLE);
            if (cancleText != null && !cancleText.equals(""))
                textView_cancle.setText(cancleText);
            textView_cancle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    clickListener.noClick();
                }
            });
            dialog = new Dialog(activity, R.style.customDialog);
            WindowManager window = activity.getWindowManager();
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
//            dialog.setContentView(view);
            int widget = ((int)window.getDefaultDisplay().getWidth()*8/10);
            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(widget,ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.setContentView(view,layoutParams);
            dialog.show();
        }
    }

    /**
     * 弹出的窗口，点击确定按钮是进行的回调接口
     */
    public static interface CustomDialogCloseListener{
        public void yesClick();
        public void noClick();
    }

}
