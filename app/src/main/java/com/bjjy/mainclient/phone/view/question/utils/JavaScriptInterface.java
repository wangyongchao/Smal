package com.bjjy.mainclient.phone.view.question.utils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.JavascriptInterface;

import com.bjjy.mainclient.phone.view.question.QuestionImageActivity;

/**
 * Created by fengzongwei on 2016/7/26 0026.
 */
public class JavaScriptInterface {

    private Activity activity;

    public JavaScriptInterface(Activity activity) {
        this.activity = activity;
    }

    @JavascriptInterface
    public void closeApp() {
        activity.finish();
    }

    @JavascriptInterface
    public void showImage(final String imagePath) {
        Intent i = new Intent(activity, QuestionImageActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("imagePath", imagePath);
        i.putExtras(bundle);
        activity.startActivity(i);
    }

}
