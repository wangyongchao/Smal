package com.bjjy.mainclient.phone.view.book;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.dongao.mainclient.model.common.Constants;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseFragmentActivity;
import com.bjjy.mainclient.phone.utils.NetworkUtil;
import com.bjjy.mainclient.phone.widget.EmptyViewLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class BookListActivity extends BaseFragmentActivity implements BookListView {


	@Bind(R.id.top_title_left)
	ImageView top_title_left;

	@Bind(R.id.top_title_text)
	TextView top_title_text;

	@Bind(R.id.top_title_right)
	ImageView top_title_right;

	@Bind(R.id.book_lv)
	ListView mListView;

	private EmptyViewLayout mEmptyLayout;
	private List<Book> mList;

	private BookListAdapter mAdapter;

	private boolean isActive = false;

	private BookListPersenter bookListPersenter;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.book_list);
		ButterKnife.bind(this);
		initView();
		initData();

	}

	@Override
	public void initView() {
		// TODO Auto-generated method stub
		top_title_left.setImageResource(R.drawable.back);
		top_title_left.setVisibility(View.VISIBLE);
		top_title_left.setOnClickListener(this);

		top_title_right.setImageResource(R.drawable.ic_add_selector);
		top_title_right.setVisibility(View.VISIBLE);
		top_title_right.setOnClickListener(this);
		top_title_text.setText("随书赠卡");


		// initialize the empty view
		mEmptyLayout = new EmptyViewLayout(this,mListView);
		ViewGroup mEmptyView = (ViewGroup) LayoutInflater.from(this).inflate(R.layout.app_view_empty_book,null);
		TextView emptyTv = (TextView) mEmptyView.findViewById(R.id.app_message_tv2);
		mEmptyLayout.setEmptyView(mEmptyView);
		mEmptyLayout.setErrorButtonClickListener(mErrorClickListener);
		mEmptyLayout.setEmptyImageId(R.drawable.pic_one);
		mEmptyLayout.setShowEmptyImage(true);
		mEmptyLayout.setShowEmptyButton(true);
		mEmptyLayout.setTitleEmptyButton("立即绑定");
		mEmptyLayout.setEmptyButtonClickListener(mEmptyClickListener);
		String str1 = "您还没有绑定随书赠卡，请点击";
		SpannableString spannableString1 = new SpannableString(str1);
		String str2 = "‘ + ’ ";
		SpannableString spannableString2 = new SpannableString(str2);
		spannableString2.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.color_primary)),0,5, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

		String str3 = "进行绑定";
		SpannableString spannableString3 = new SpannableString(str3);

		SpannableStringBuilder ssb = new SpannableStringBuilder();
		ssb.append(spannableString1).append(spannableString2).append(spannableString3);
		emptyTv.setText(ssb);

		bookListPersenter = new BookListPersenter();
		bookListPersenter.attachView(this);
	}

	private View.OnClickListener mErrorClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			initData();//重新刷新数据
		}
	};

	private View.OnClickListener mEmptyClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent_activate = new Intent(BookListActivity.this, BookActivateActivity.class);
			startActivityForResult(intent_activate, Constants.ACTIVITY_REQUEST_CODE_BOOK_LIST_ACTIVATE);
		}
	};

	@Override
	public void initData() {
		//mEmptyLayout.showEmpty();
		mEmptyLayout.showLoading();
		mList = new ArrayList<Book>();
		getData();
	}

	/**
	 * 获取网络数据
	 */
	private void getData(){
		if(NetworkUtil.isNetworkAvailable(this)){
			bookListPersenter.getData();
		} else {
			if(mList != null && !mList.isEmpty()){

			}else{
				mEmptyLayout.showNetErrorView();
			}
		}
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
			case  R.id.top_title_left:
				onBackPressed();
				break;
			case R.id.top_title_right:
				Intent intent_activate = new Intent(BookListActivity.this, BookActivateActivity.class);
				startActivityForResult(intent_activate, Constants.ACTIVITY_REQUEST_CODE_BOOK_LIST_ACTIVATE);
				break;
			default:
				break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
			case Constants.ACTIVITY_REQUEST_CODE_BOOK_LIST_ACTIVATE:
				isActive = true;
				initData();
				break;
		}

	}

	@Override
	public void onBackPressed() {
			if(isActive){
				setResult(Constants.ACTIVITY_RESULT_CODE_BOOK_ACTIVATE, getIntent());
			}
			this.finish();
	}

	@Override
	public void setAdapter(List<Book> books) {
		mList = books;
		if(mList != null && !mList.isEmpty()){
			mAdapter = new BookListAdapter(this);
			mAdapter.setList((ArrayList<Book>) mList);
			mListView.setAdapter(mAdapter);
			mEmptyLayout.showContentView();
		}else{
			mEmptyLayout.showEmpty();
		}
	}

	@Override
	public void showLoading() {
		mEmptyLayout.showLoading();
	}

	@Override
	public void hideLoading() {
		mEmptyLayout.showContentView();
	}

	@Override
	public void showRetry() {

	}

	@Override
	public void hideRetry() {

	}

	@Override
	public void showError(String message) {
		mEmptyLayout.showError();
	}

	@Override
	public Context context() {
		return this;
	}
}
