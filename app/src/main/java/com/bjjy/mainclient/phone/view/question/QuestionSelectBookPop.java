package com.bjjy.mainclient.phone.view.question;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.PopupWindow;

import com.bjjy.mainclient.phone.view.question.adapter.QuestionSelectBookAdapter;
import com.dongao.mainclient.model.bean.question.QustionBook;
import com.bjjy.mainclient.phone.R;

import java.util.List;

/**
 * Created by fengzongwei on 2016/11/29 0029.
 */
public class QuestionSelectBookPop implements View.OnClickListener{

    private PopupWindow popupWindow;

    private Context context;
    private TabQuestionFragment tabQuestionFragment;

    private View contentView;
    private ListView listView;

    private List<QustionBook> qustionBooks;

    private QuestionSelectBookAdapter questionSelectBookAdapter;

    public QuestionSelectBookPop(Context context,TabQuestionFragment tabQuestionFragment){
        this.context = context;
        this.tabQuestionFragment = tabQuestionFragment;
        initPop();
    }

    private void initPop(){
        if(popupWindow == null){
            contentView = LayoutInflater.from(context).inflate(R.layout.question_select_examination_pop,null);
            popupWindow = new PopupWindow(contentView, ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            // 需要设置一下此参数，点击外边可消失
            popupWindow.setBackgroundDrawable(new BitmapDrawable());
            // 设置点击窗口外边窗口消失
            popupWindow.setOutsideTouchable(true);
            // 设置此参数获得焦点，否则无法点击
            popupWindow.setFocusable(true);

            listView = (ListView)contentView.findViewById(R.id.question_select_exm_lv);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    onPopItemClick(qustionBooks.get(position));
                }
            });
        }
    }

    public void showPop(View archer,List<QustionBook> qustionBooks){
        this.qustionBooks = qustionBooks;
        popupWindow.showAsDropDown(archer,0,0);
        questionSelectBookAdapter = new QuestionSelectBookAdapter(context,qustionBooks);
        listView.setAdapter(questionSelectBookAdapter);
    }

    public void onPopItemClick(QustionBook qustionBook){
        tabQuestionFragment.setSelectBookName(qustionBook);
        dismissPop();
    }

    public void dismissPop(){
        popupWindow.dismiss();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
        }
    }

}
