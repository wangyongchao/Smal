package com.bjjy.mainclient.phone.view.setting.cache.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.StatFs;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bjjy.mainclient.phone.app.BaseFragment;
import com.bjjy.mainclient.phone.event.DeleteEvent;
import com.bjjy.mainclient.phone.utils.NetWorkUtils;
import com.bjjy.mainclient.phone.view.play.PlayActivity;
import com.bjjy.mainclient.phone.view.setting.cache.CacheActivity;
import com.bjjy.mainclient.phone.widget.DialogManager;
import com.dongao.mainclient.model.bean.play.CourseWare;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.AppConfig;
import com.bjjy.mainclient.phone.app.AppContext;
import com.bjjy.mainclient.phone.download.DownloadExcutor;
import com.bjjy.mainclient.phone.download.db.DownloadDB;
import com.bjjy.mainclient.phone.view.setting.cache.adapter.CachingAdapter;
import com.bjjy.mainclient.phone.view.studybar.down.CacheFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by dell on 2016/5/16.
 */
public class CachedownloadFragment extends BaseFragment implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    @Bind(R.id.cached_ls)
    ListView cachedLs;
    @Bind(R.id.caching_top)
    LinearLayout cachingTop;
    @Bind(R.id.local_notask)
    LinearLayout localNotask;
    @Bind(R.id.caching_bottom)
    RelativeLayout cachingBottom;
    @Bind(R.id.tv_pause)
    TextView tvPause;
    @Bind(R.id.tv_clean)
    TextView tvClean;
    @Bind(R.id.tv_space)
    TextView tvSpace;
    @Bind(R.id.pro_space)
    ProgressBar proSpace;
    private View view;
    private CachingAdapter adapter;
    private DownloadDB db;
    private List<CourseWare> list;
//    private MainActivity activity;
    private String userId;

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 1313) {
                initData();
                activity.initData();
            } else {
                //这里可以传下载百分比，100的时候initData()
                if (msg.obj != null && (int) msg.obj == 100) {
                    initData();
                    activity.initData();
                } else {
                    adapter.notifyDataSetChanged();
                }
            }
        }
    };
    private CacheActivity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        view = inflater.inflate(R.layout.cachedownload_fragment, null);
        ButterKnife.bind(this, view);
        initView();
        initSpace();
        initData();
        return view;
    }

    private void initSpace() {
        File path = Environment.getExternalStorageDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long totalBlocks = stat.getBlockCount();
        long availableBlocks = stat.getAvailableBlocks();

        long totalSize = totalBlocks * blockSize;
        long availSize = availableBlocks * blockSize;
        long usedSize = totalSize - availSize;

//        String totalStr = Formatter.formatFileSize(getActivity(), totalSize);
        String usedStr = Formatter.formatFileSize(getActivity(), usedSize);
        String availStr = Formatter.formatFileSize(getActivity(), availSize);
        tvSpace.setText("已占用空间" + usedStr + "," + "可用空间" + availStr);
        int percent = (int) (usedSize * 100 / totalSize);
        proSpace.setProgress(percent);
    }

    @Override
    public void initView() {
        userId = SharedPrefHelper.getInstance(getActivity()).getUserId();

        activity = (CacheActivity) getActivity();
//        fragment=(CacheFragment)getParentFragment();
        db = new DownloadDB(getActivity());
        adapter = new CachingAdapter(getActivity(), handler);
        cachedLs.setOnItemClickListener(this);
        cachedLs.setOnItemLongClickListener(this);
        AppContext.getInstance().setHandler(handler);

        tvPause.setOnClickListener(this);
        tvClean.setOnClickListener(this);

        if (AppContext.getInstance().isStart == 1) {
            tvPause.setText("全部暂停");
        } else {
            tvPause.setText("全部下载");
        }
    }

    @Override
    public void initData() {
        list = db.findDownloadList(SharedPrefHelper.getInstance(getActivity()).getUserId() + "");
        if(localNotask==null){
            return;
        }
        if (list != null && list.size() > 0) {
            localNotask.setVisibility(View.GONE);
            cachingTop.setVisibility(View.VISIBLE);
            cachingBottom.setVisibility(View.VISIBLE);
            cachedLs.setVisibility(View.VISIBLE);

            adapter.setList(list);
            cachedLs.setAdapter(adapter);
        } else {
            localNotask.setVisibility(View.VISIBLE);
            cachingTop.setVisibility(View.GONE);
            cachingBottom.setVisibility(View.GONE);
            cachedLs.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_pause:
                if (AppContext.getInstance().isStart == 1) {
                    AppContext.getInstance().isStart = 0;
                    DownloadExcutor.getInstance(getActivity()).setFlag(false);
                    db.updateState(Constants.STATE_Pause);
                    adapter.notifyDataSetChanged();
                    tvPause.setText("全部下载");
                } else {    //下载全部逻辑
                    boolean isDownload = SharedPrefHelper.getInstance(getActivity()).getIsNoWifiPlayDownload();
                    if (!isDownload) {
                        NetWorkUtils type = new NetWorkUtils(getActivity());
                        if (type.getNetType() == 0) {//无网络
                            Toast.makeText(getActivity(), "请检查网络", Toast.LENGTH_SHORT).show();
                        } else if (type.getNetType() == 2) { //流量
                            DialogManager.showNormalDialog(getActivity(), getResources().getString(R.string.dialog_warnning_vedio), "提示", "取消", "确定",
                                    new DialogManager.CustomDialogCloseListener() {
                                        @Override
                                        public void yesClick() {
                                            tvPause.setText("全部暂停");
                                            List<CourseWare> downloads = db.findAllNotFinish(SharedPrefHelper.getInstance(getActivity()).getUserId());
                                            for (int i = 0; i < downloads.size(); i++) {
                                                AppConfig.getAppConfig(getActivity()).download(list.get(i));
                                            }
                                        }

                                        @Override
                                        public void noClick() {
                                        }
                                    });

                        } else if (type.getNetType() == 1) {
                            tvPause.setText("全部暂停");
                            List<CourseWare> downloads = db.findAllNotFinish(SharedPrefHelper.getInstance(getActivity()).getUserId());
                            for (int i = 0; i < downloads.size(); i++) {
                                AppConfig.getAppConfig(getActivity()).download(list.get(i));
                            }
                        }
                    } else {
                        tvPause.setText("全部暂停");
                        List<CourseWare> downloads = db.findAllNotFinish(SharedPrefHelper.getInstance(getActivity()).getUserId());
                        for (int i = 0; i < downloads.size(); i++) {
                            AppConfig.getAppConfig(getActivity()).download(list.get(i));
                        }
                    }
                }
                break;
            case R.id.tv_clean:
                DialogManager.showNormalDialog(getActivity(), "确定要全部删除吗", "提示", "取消", "确定",
                        new DialogManager.CustomDialogCloseListener() {
                            @Override
                            public void yesClick() {
//                                db.deletes(SharedPrefHelper.getInstance(getActivity()).getUserId());
                                db.deleteCourseWaresDownloading(SharedPrefHelper.getInstance(getActivity()).getUserId(), list);
                                initData();
                                activity.initData();
                            }

                            @Override
                            public void noClick() {
                            }
                        });
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Subscribe
    public void onEventMainThread(DeleteEvent event) {
        initData();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        CourseWare cw = list.get(position);
        int state = db.getState(userId, cw.getClassId(), cw.getCwId());
        if (state == Constants.STATE_DownLoading) {
            DownloadExcutor.getInstance(getActivity()).setFlag(false);
            db.updateState(userId, cw.getClassId(), cw.getCwId(), Constants.STATE_Pause);
            adapter.notifyDataSetChanged();
        } else if (state == Constants.STATE_Waiting) {
            db.updateState(userId, cw.getClassId(), cw.getCwId(), Constants.STATE_Pause);
            adapter.notifyDataSetChanged();
            if (DownloadExcutor.getInstance(getActivity()).task != null) {
                if (DownloadExcutor.getInstance(getActivity()).task.getCourseWareId().equals(cw.getCwId()) && DownloadExcutor.getInstance(getActivity()).task.getCourseId().equals(cw.getClassId())) {
                    DownloadExcutor.getInstance(getActivity()).setFlag(false);
                }
            }
        } else if (state == Constants.STATE_Pause || state == Constants.STATE_Error || state == 0) {
//            db.updateState(userId, cw.getClassId(), cw.getCwId(), Constants.STATE_Waiting);
            showDialog(cw);
            adapter.notifyDataSetChanged();
        }
        CourseWare courseWare = db.findStatusDownloading(SharedPrefHelper.getInstance(getActivity()).getUserId());
        List<CourseWare> list = db.findStatusWating(SharedPrefHelper.getInstance(getActivity()).getUserId());
        if(courseWare==null && list.size()==0){
            tvPause.setText("全部下载");
        }else{
            tvPause.setText("全部暂停");
        }
    }

    private void showDialog(final CourseWare cw){
        boolean isDownload = SharedPrefHelper.getInstance(getActivity()).getIsNoWifiPlayDownload();
        if (!isDownload) {
            NetWorkUtils type = new NetWorkUtils(getActivity());
            if (type.getNetType() == 0) {//无网络
                Toast.makeText(getActivity(), "请检查网络", Toast.LENGTH_SHORT).show();
            } else if (type.getNetType() == 2) { //流量
                DialogManager.showNormalDialog(getActivity(), getResources().getString(R.string.dialog_warnning_vedio), "提示", "取消", "确定",
                        new DialogManager.CustomDialogCloseListener() {
                            @Override
                            public void yesClick() {
                                db.updateState(userId, cw.getClassId(), cw.getCwId(), Constants.STATE_Waiting);
                                AppConfig.getAppConfig(getActivity()).download(cw);
                            }

                            @Override
                            public void noClick() {
                            }
                        });

            } else if (type.getNetType() == 1) {
                db.updateState(userId, cw.getClassId(), cw.getCwId(), Constants.STATE_Waiting);
                AppConfig.getAppConfig(getActivity()).download(cw);
            }
        } else {
            db.updateState(userId, cw.getClassId(), cw.getCwId(), Constants.STATE_Waiting);
            AppConfig.getAppConfig(getActivity()).download(cw);
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view,int position, long id) {
        final CourseWare courseWare=list.get(position);
        int percent = db.getPercent(SharedPrefHelper.getInstance(getActivity()).getUserId(), courseWare.getClassId(), courseWare.getCwId());
        if(percent>=70 && percent<100){
            DialogManager.showNormalDialog(getActivity(), "还没下载完成,确定播放吗?", "提示", "取消", "确定",
                    new DialogManager.CustomDialogCloseListener() {
                        @Override
                        public void yesClick() {
                            Intent intent = new Intent(getActivity(), PlayActivity.class);
                            intent.putExtra("classId", courseWare.getClassId());
                            intent.putExtra("playCwId", courseWare.getCwId());
                            intent.putExtra("courseBean", courseWare.getCourseBean());
                            intent.putExtra("subjectId", courseWare.getSubjectId());
                            intent.putExtra("examId", courseWare.getExamId());
                            intent.putExtra("sectionId", courseWare.getSectionId());
                            intent.putExtra("isPlayFromLoacl", true);
                            intent.putExtra("version", courseWare.getCwVersion());
                            intent.putExtra("isFromLocal", "yes");
                            startActivity(intent);
                        }

                        @Override
                        public void noClick() {
                        }
                    });
        }

        return true;
    }
}
