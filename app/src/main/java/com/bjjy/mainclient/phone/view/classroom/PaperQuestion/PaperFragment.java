/*
 * Copyright (C) 2013 Andreas Stuetz <andreas.stuetz@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bjjy.mainclient.phone.view.classroom.PaperQuestion;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bjjy.mainclient.phone.event.UpdateEvent;
import com.bjjy.mainclient.phone.view.classroom.course.HeaderViewPagerFragment;
import com.bjjy.mainclient.phone.view.exam.ExamActivity;
import com.bjjy.mainclient.phone.view.exam.activity.myfault.bean.FaultClass;
import com.bjjy.mainclient.phone.view.exam.bean.AnswerLog;
import com.bjjy.mainclient.phone.view.exam.db.AnswerLogDB;
import com.bjjy.mainclient.phone.view.exam.db.FaltQuestionDB;
import com.bjjy.mainclient.phone.view.setting.myfavs.MyFavouritesActivity;
import com.bjjy.mainclient.phone.widget.EmptyViewLayout;
import com.dongao.libs.dongaoumengpushlib.constants.PushConstants;
import com.dongao.mainclient.model.bean.course.Paper;
import com.dongao.mainclient.model.bean.user.MyCollection;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.MyCollectionDB;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.remote.bean.BaseBean;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.event.CourseTabTypeEvent;
import com.bjjy.mainclient.phone.utils.NetworkUtil;
import com.bjjy.mainclient.phone.view.exam.activity.myfault.MyFaultActivity;
import com.umeng.analytics.MobclickAgent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PaperFragment extends HeaderViewPagerFragment implements PaperFragmentView {


    @Bind(R.id.course_fragment_exam_rl)
    RelativeLayout courseFragmentExamRl;
    @Bind(R.id.question_wrong)
    LinearLayout questionWrong;
    @Bind(R.id.question_collect)
    LinearLayout questionCollect;
    @Bind(R.id.paper_listview)
    ListView paperListview;
    @Bind(R.id.paper_scroll)
    ScrollView paperScroll;

    @Bind(R.id.tv_errornum)
    TextView tvErrornum;
    @Bind(R.id.tv_continu_title)
    TextView tvContinuTitle;
    @Bind(R.id.collect_num)
    TextView collectNum;
    @Bind(R.id.img_continuepaper)
    ImageView imgContinuepaper;

    @Bind(R.id.empty)
    LinearLayout empty;

    private PaperAdapter adapter;
    private List<Paper> papers;

    private PagperFragmentPersenter paperFragmentPersenter;

    private View mRootView;
    private EmptyViewLayout mEmptyLayout;

    private FaltQuestionDB questionDB;
    private String userId, examId, subjectId;
    private List<FaultClass> faults;

    private AnswerLogDB answerLogDB;
    private MyCollectionDB collectionDB;
    private AnswerLog answerLog;
    private int errnum;
    private List<MyCollection> collections;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    private boolean mHasLoadedOnce = false;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if (isVisibleToUser && !mHasLoadedOnce && faults == null) {
                // async http request here
                mHasLoadedOnce = true;
                initData();
            } else if (isVisibleToUser && mHasLoadedOnce && !userId.equals(SharedPrefHelper.getInstance(getActivity()).getUserId())) {
                userId = SharedPrefHelper.getInstance(getActivity()).getUserId();
                initData();
            } else {
                if (paperScroll != null) {
                    paperScroll.smoothScrollTo(0, 0);
                }
            }
        }
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.paper_fragment, container, false);
        }
        ButterKnife.bind(this, mRootView);
        ViewGroup parent = (ViewGroup) mRootView.getParent();
        if (parent != null) {
            parent.removeView(mRootView);
        }
        paperFragmentPersenter = new PagperFragmentPersenter();
        paperFragmentPersenter.attachView(this);
        initView();
        return mRootView;
    }

    private void initDao() {
        subjectId = SharedPrefHelper.getInstance(getActivity()).getSubjectId();
        examId = SharedPrefHelper.getInstance(getActivity()).getExamId();
        userId = SharedPrefHelper.getInstance(getActivity()).getUserId();

        questionDB = new FaltQuestionDB(getActivity());
        answerLogDB = new AnswerLogDB(getActivity());
        collectionDB = new MyCollectionDB(getActivity());

        errnum = questionDB.findAllCount(userId, examId, subjectId);
        tvErrornum.setText(errnum + "");

        answerLog = answerLogDB.findLastAnswerLog(userId, examId, subjectId);
        if (answerLog != null) {
            imgContinuepaper.setVisibility(View.VISIBLE);
            tvContinuTitle.setText(answerLog.getExaminationTitle());
        } else {
            imgContinuepaper.setVisibility(View.INVISIBLE);
            tvContinuTitle.setText("暂未做题");
        }

        collections = collectionDB.findOneTypeCollection(userId, "1",subjectId);
        if (collections != null) {
            collectNum.setText(collections.size() + "");
        } else {
            collectNum.setText("1");
        }

        if (adapter != null && papers != null) {
            adapter.setList(papers);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void initView() {
        empty.setOnClickListener(this);
        questionWrong.setOnClickListener(this);
        questionCollect.setOnClickListener(this);
//        tvContinuTitle.setOnClickListener(this);
        courseFragmentExamRl.setOnClickListener(this);
        adapter = new PaperAdapter(getActivity());

        paperListview.setFocusable(false);
        paperScroll.smoothScrollTo(0, 0);


        mEmptyLayout = new EmptyViewLayout(getActivity(), paperListview);
        mEmptyLayout.setErrorButtonClickListener(mErrorClickListener);
    }

    private View.OnClickListener mErrorClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            initData();//重新刷新数据
        }
    };

    @Override
    public void initData() {
        faults = new ArrayList<>();
        if (NetworkUtil.isNetworkAvailable(getActivity())) {
            paperFragmentPersenter.getData();
        } else {
            showError("");
        }
        initDao();
    }


    @Override
    public void setData(List<Paper> papers) {
        faults.clear();
        if (papers != null && papers.size() > 0 && paperListview!=null) {
//            tvErrornum.setText(errnum+"");
            this.papers = papers;
            adapter.setList(papers);
            paperListview.setAdapter(adapter);
            mEmptyLayout.showContentView();
            empty.setVisibility(View.GONE);
            for (Paper paper : papers) {
                FaultClass fault = new FaultClass();
                fault.setClassId(paper.getType());
                fault.setClassName(paper.getName());
                faults.add(fault);
            }
        } else {
            tvErrornum.setText("0");
            mEmptyLayout.showEmpty();
            empty.setVisibility(View.VISIBLE);
        }
        paperScroll.smoothScrollTo(0, 0);
        EventBus.getDefault().post(new CourseTabTypeEvent(Constants.COURSE_TAB_TYPE_EXAM));
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.question_collect:
                if (collections != null) {
                    Intent favs = new Intent(getActivity(), MyFavouritesActivity.class);
                    favs.putExtra("flag", "exam");
                    startActivity(favs);
                }
                break;
            case R.id.question_wrong:
                if (faults != null && faults.size() > 0) {
                    Intent intent = new Intent(getActivity(), MyFaultActivity.class);
                    SharedPrefHelper.getInstance(getActivity()).setExamTag(Constants.EXAM_TAG_FALT);
                    intent.putExtra("list", (Serializable) faults);
                    startActivity(intent);
                }
                break;
            case R.id.course_fragment_exam_rl:
                if (answerLog != null) {
                    Intent top = new Intent(getActivity(), ExamActivity.class);
                    SharedPrefHelper.getInstance(getActivity()).setMainTypeId("");
                    SharedPrefHelper.getInstance(getActivity()).setExamTag(Constants.EXAM_DO_CONTINUE);
                    startActivity(top);
                    MobclickAgent.onEvent(getActivity(), PushConstants.CLASSROOM_PAPER_CONTINUE_TO_EXAM);
                }
                break;
            case R.id.empty:
                initData();
                break;
            default:
                break;
        }
    }

    @Override
    public void showLoading() {
        mEmptyLayout.showLoading();
    }

    @Override
    public void hideLoading() {
        mEmptyLayout.showContentView();
    }

    @Override
    public void showRetry() {
    }

    @Override
    public void hideRetry() {

    }

    @Override
    public String getSubjectId() {
        return subjectId + userId + "P";
    }

    @Override
    public void showError(String message) {
        userId = SharedPrefHelper.getInstance(getActivity()).getUserId();
        subjectId = SharedPrefHelper.getInstance(getActivity()).getSubjectId();
        String json = SharedPrefHelper.getInstance(getActivity()).getPaperCache(subjectId + userId + "P");
        if (!TextUtils.isEmpty(json)) {
            BaseBean baseBean = JSON.parseObject(json, BaseBean.class);
            JSONObject object = JSON.parseObject(baseBean.getBody());
            papers = JSON.parseArray(object.getString("examinationList"), Paper.class);
            if(tvErrornum!=null){
                setData(papers);
            }
        } else {
            if(mEmptyLayout!=null){
                mEmptyLayout.setErrorMessage("网络连接失败");
                mEmptyLayout.showError();
            }
        }
    }

    @Override
    public Context context() {
        return getActivity();
    }


    @Subscribe
    public void onEventAsync(UpdateEvent event) {
        initData();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        EventBus.getDefault().unregister(this);
    }

    @Override
    public View getScrollableView() {
        return mRootView;
    }

    boolean isfirst;

    @Override
    public void onResume() {
        super.onResume();
        if (!isfirst) {
            isfirst = true;
            return;
        }
        if(questionDB==null){
            return;
        }
        errnum = questionDB.findAllCount(userId, examId, subjectId);
        tvErrornum.setText(errnum + "");

        examId = SharedPrefHelper.getInstance(getActivity()).getExamId();
        subjectId = SharedPrefHelper.getInstance(getActivity()).getSubjectId();
        answerLog = answerLogDB.findLastAnswerLog(userId, examId, subjectId);
        if (answerLog != null) {
            imgContinuepaper.setVisibility(View.VISIBLE);
            tvContinuTitle.setText(answerLog.getExaminationTitle());
        } else {
            imgContinuepaper.setVisibility(View.INVISIBLE);
            tvContinuTitle.setText("暂未做题");
        }

        collections = collectionDB.findOneTypeCollection(userId, "1",subjectId);
        if (collections != null) {
            collectNum.setText(collections.size() + "");
        } else {
            collectNum.setText("1");
        }

        if (adapter != null && papers != null) {
            adapter.setList(papers);
            adapter.notifyDataSetChanged();
        }
    }
}