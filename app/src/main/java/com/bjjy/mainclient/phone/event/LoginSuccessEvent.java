package com.bjjy.mainclient.phone.event;

/**
 * Created by fengzongwei on 2016/6/28 0028.
 * 登录成功event
 */
public class LoginSuccessEvent {
    public String type;
    private static final String TAG = "LoginSuccessEvent";
    public LoginSuccessEvent(String type)
    {
        this.type = type;
    }

    public String getType(){
        return type;
    }

}
