package com.bjjy.mainclient.phone.view.book;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.dongao.mainclient.core.util.DateUtil;
import com.dongao.mainclient.core.util.StringUtil;
import com.bjjy.mainclient.phone.R;

import java.util.ArrayList;

public class BookListAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<Book> mlist;

	public BookListAdapter(Context context) {
		super();
		this.context = context;
	}

	public void setList(ArrayList<Book> mlist) {
		this.mlist = mlist;
	}


	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mlist.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stubho
		final ViewHolder viewHolder;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(R.layout.book_list_item, null);
			viewHolder.bookLayout = convertView.findViewById(R.id.book_layout);
			viewHolder.cardNumTv = (TextView) convertView.findViewById(R.id.book_card_num_tv);
            viewHolder.subjectTv = (TextView) convertView.findViewById(R.id.book_subject_tv);
			viewHolder.expiryTv = (TextView) convertView.findViewById(R.id.book_expiry_tv);
			viewHolder.activeTv = (TextView) convertView.findViewById(R.id.book_active_btn);
			viewHolder.bookNameTv = (TextView) convertView.findViewById(R.id.book_bookname_tv);
			viewHolder.serviceTv = (TextView) convertView.findViewById(R.id.book_service_tv);
			viewHolder.courseTv = (TextView) convertView.findViewById(R.id.book_course_tv);
			viewHolder.statusIv = (ImageView) convertView.findViewById(R.id.book_activie_status_iv);
			viewHolder.bookCourseLayout = convertView.findViewById(R.id.book_course_layout);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		Book book = mlist.get(position);
		viewHolder.cardNumTv.setText("激活码：" +book.getCouponNo());
		viewHolder.bookNameTv.setText(book.getCouponBookRuleDto().getGoodsName());
		viewHolder.subjectTv.setText(book.getCouponBookRuleDto().getCategoryName());
		viewHolder.expiryTv.setText("有效期至："+book.getCouponBookRuleDto().getEnddateStr());
		/*String bookNameStr = "";
		if(book.getBookNameList() != null && !book.getBookNameList().isEmpty()){
			for(Book.BookName bookName : book.getBookNameList()){
				bookNameStr = bookNameStr +  bookName.getBookName() + " ";
			}
			viewHolder.bookNameTv.setText(bookNameStr);
			viewHolder.bookNameTv.setVisibility(View.VISIBLE);
		}else{
			viewHolder.bookNameTv.setVisibility(View.GONE);
		}

		if(book.getServiceNameList() != null && !book.getServiceNameList().isEmpty()){
			String serviceNameStr = "";
			for(Book.ServiceName serviceName : book.getServiceNameList()){
				serviceNameStr = serviceNameStr +  getServiceName(serviceName.getServiceName()) + " ";
			}
			viewHolder.serviceTv.setText(serviceNameStr);
			viewHolder.serviceTv.setVisibility(View.VISIBLE);
		}else{
			viewHolder.serviceTv.setVisibility(View.GONE);
		}

		if(book.getCourseNameList() != null && !book.getCourseNameList().isEmpty()){
			String courseNameStr = "";
			for(Book.CourseName courseName : book.getCourseNameList()){
				courseNameStr = courseNameStr +  courseName.getCourseName() + " ";
			}
			viewHolder.courseTv.setText(courseNameStr);
			viewHolder.bookCourseLayout.setVisibility(View.VISIBLE);
			if(book.getStatus() == 2){ //未过期
				viewHolder.bookLayout.setBackgroundResource(R.drawable.bg_activated_two);
				viewHolder.statusIv.setVisibility(View.GONE);
			}else{
				viewHolder.bookLayout.setBackgroundResource(R.drawable.bg_expired_two);
				viewHolder.statusIv.setVisibility(View.VISIBLE);
			}
		}else{
			if(book.getStatus() == 2){ //未过期
				viewHolder.bookLayout.setBackgroundResource(R.drawable.bg_activated_one);
				viewHolder.statusIv.setVisibility(View.GONE);
			}else{
				viewHolder.bookLayout.setBackgroundResource(R.drawable.bg_expired_one);
				viewHolder.statusIv.setVisibility(View.VISIBLE);
			}
			viewHolder.bookCourseLayout.setVisibility(View.GONE);
		}
        */

//		if(!StringUtil.isEmpty(book.getBookName())){
//			viewHolder.bookNameTv.setText(book.getBookName());
//			viewHolder.bookNameTv.setVisibility(View.VISIBLE);
//		}else{
//			viewHolder.bookNameTv.setVisibility(View.GONE);
//		}

//		if(!StringUtil.isEmpty(book.getService())){
//			viewHolder.serviceTv.setText(book.getService());
//			viewHolder.serviceTv.setVisibility(View.VISIBLE);
//		}else{
//			viewHolder.serviceTv.setVisibility(View.GONE);
//		}

//		if(!StringUtil.isEmpty(book.getCourse())){
//			viewHolder.courseTv.setText(book.getCourse());
//			viewHolder.bookCourseLayout.setVisibility(View.VISIBLE);
//			if(!StringUtil.isEmpty(book.getEndDate())){
//				if(book.getEndDate().compareTo(DateUtil.getCurrentDate())>=0){ //未过期
//					viewHolder.bookLayout.setBackgroundResource(R.drawable.bg_activated_two);
//					viewHolder.statusIv.setVisibility(View.GONE);
//				}else{
//					viewHolder.bookLayout.setBackgroundResource(R.drawable.bg_expired_two);
//					viewHolder.statusIv.setVisibility(View.VISIBLE);
//				}
//			}else{
//				viewHolder.bookLayout.setBackgroundResource(R.drawable.bg_activated_two);
//				viewHolder.statusIv.setVisibility(View.GONE);
//			}
//
//		}else{
			if(!StringUtil.isEmpty(book.getCouponBookRuleDto().getEnddateStr())){
				if(book.getCouponBookRuleDto().getEnddateStr().compareTo(DateUtil.getCurrentDate())>=0){ //未过期
					viewHolder.bookLayout.setBackgroundResource(R.drawable.bg_activated_one);
					viewHolder.statusIv.setVisibility(View.GONE);
				}else{
					viewHolder.bookLayout.setBackgroundResource(R.drawable.bg_expired_one);
					viewHolder.statusIv.setVisibility(View.VISIBLE);
				}
			}else{
				viewHolder.bookLayout.setBackgroundResource(R.drawable.bg_activated_one);
				viewHolder.statusIv.setVisibility(View.GONE);
			}

			viewHolder.bookCourseLayout.setVisibility(View.GONE);
//		}

		viewHolder.activeTv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//Intent intent = new Intent(context, BookSelectActivity.class);
				//context.startActivity(intent);
			}
		});
		return convertView;
	}

	class ViewHolder {
		View bookLayout;
		View bookCourseLayout;
		TextView cardNumTv;
        TextView subjectTv;
		TextView expiryTv;
		TextView activeTv;
		TextView bookNameTv;
		TextView serviceTv;
		TextView courseTv;
		ImageView statusIv;
	}

	private String getServiceName(String str){
		String serviceName = "";
		if("da-exam-app".equals(str)){
			serviceName = "题库APP";
		}else if("da-bookqa-app".equals(str)){
			serviceName = "答疑";
		}else {
			serviceName = "东奥APP";
		}
		return serviceName;
	}
}
