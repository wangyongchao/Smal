package com.bjjy.mainclient.phone.view.question;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dongao.mainclient.model.bean.question.QuestionKonwLedge;
import com.dongao.mainclient.model.bean.question.QuestionRecomm;
import com.dongao.mainclient.model.bean.question.QustionBook;
import com.bjjy.mainclient.persenter.TabQuestionPresenter;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseFragment;
import com.bjjy.mainclient.phone.event.ReloadQuestion;
import com.bjjy.mainclient.phone.event.UpdateEvent;
import com.bjjy.mainclient.phone.view.book.BookActivateActivity;
import com.bjjy.mainclient.phone.view.question.adapter.QuestionListAdapter;
import com.bjjy.mainclient.phone.widget.EmptyViewLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by fengzongwei on 2016/11/28 0028.
 */
public class TabQuestionFragment extends BaseFragment implements TabQuestionView{

    private View contentView;
    @Bind(R.id.tab_question_fragment_bookName_tv)
    TextView textView_bookName;
    @Bind(R.id.tab_question_fragment_et)
    EditText editText;
    @Bind(R.id.tab_question_fragment_choice_ll)
    LinearLayout linearLayout_choice;
    @Bind(R.id.tab_question_fragment_lv)
    ListView listView;
    @Bind(R.id.tab_question_fragment_choice_tv)
    TextView textView_exm_name;
    @Bind(R.id.tab_question_fragment_ask_tv)
    TextView textView_add_ques;
    @Bind(R.id.tab_question_fragment_bookName_img)
    ImageView imageView_bookName_img;
    @Bind(R.id.tab_question_fragment_bookName_loaderror_tv)
    TextView textView_bookName_error;
    @Bind(R.id.tab_question_fragment_knowledgepoint_loaderror_tv)
    TextView textView_know_error;
    @Bind(R.id.tab_question_fragment_knowledgepoint_img)
    ImageView imageView_know;
    @Bind(R.id.tab_question_fragment_jihuo_tv)
    ImageView imageView_jihuo;
    @Bind(R.id.tab_question_fragment_ask_body_ll)
    LinearLayout linearLayout_ask_body;

    private QuestionListAdapter questionListAdapter;
    private QuestionSelectExmPop questionSelectExmPop;
    private QuestionSelectBookPop questionSelectBookPop;
    private QustionBook qustionBookNow;
    private QuestionKonwLedge questionKonwLedgeNow;

    private TabQuestionPresenter tabQuestionPresenter;

    private List<QuestionRecomm> questionRecomms;
    private List<QustionBook> qustionBooks;
    private List<QuestionKonwLedge> questionKonwLedges;

    private EmptyViewLayout emptyViewLayout;

    private String pageNum;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        contentView = inflater.inflate(R.layout.tab_question_fragment,container,false);
        ButterKnife.bind(this,contentView);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        ViewGroup parent = (ViewGroup) contentView.getParent();
        if (parent != null) {
            parent.removeView(contentView);
        }
        initView();
        return contentView;
    }

    @Override
    public void onResume() {
        super.onResume();
        initData();
    }

    @Override
    public void initView() {
        emptyViewLayout = new EmptyViewLayout(getActivity(),listView);
        emptyViewLayout.setEmptyButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emptyViewLayout.showLoading();
                tabQuestionPresenter.getRecommQuesList(questionKonwLedgeNow);
            }
        });
        emptyViewLayout.setErrorButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emptyViewLayout.showLoading();
                tabQuestionPresenter.getRecommQuesList(questionKonwLedgeNow);
            }
        });
        questionSelectBookPop = new QuestionSelectBookPop(getActivity(),this);
        questionSelectExmPop = new QuestionSelectExmPop(getActivity(),this);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), QuestionDetailActivity.class);
                intent.putExtra("questionAnswerId", questionRecomms.get(position).getQuestionId() + "");
                startActivity(intent);
            }
        });
        linearLayout_choice.setOnClickListener(this);
        textView_add_ques.setOnClickListener(this);
        textView_bookName.setOnClickListener(this);
        textView_know_error.setOnClickListener(this);
        textView_bookName_error.setOnClickListener(this);
        imageView_jihuo.setOnClickListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onEventMainThread(ReloadQuestion reloadQuestion){
        tabQuestionPresenter.getData();
    }

    @Override
    public void initData() {
        questionKonwLedgeNow = null;
        textView_exm_name.setText("点击选择知识点或试题");
        if(tabQuestionPresenter == null)
            tabQuestionPresenter = new TabQuestionPresenter();
        tabQuestionPresenter.attachView(this);
        tabQuestionPresenter.getData();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tab_question_fragment_choice_ll:
                if(TextUtils.isEmpty(editText.getText().toString().trim())){
                    showError("请输入页码");
                    return;
                }
                if(qustionBookNow == null){
                    showError("当前科目无图书");
                    return;
                }
                if(questionKonwLedges == null || questionKonwLedges.size()==0){
                    pageNum = editText.getText().toString();
                    tabQuestionPresenter.getCanChoiceKnowLedgePointList(qustionBookNow.getId(),editText.getText().toString());
//                    tabQuestionPresenter.getCanChoiceKnowLedgePointList(qustionBookNow.getId(), editText.getText().toString().trim());
                }else if(questionKonwLedges.size()>1){
                    questionSelectExmPop.showPop(textView_exm_name, questionKonwLedges);
                }
                break;
            case R.id.tab_question_fragment_ask_tv:
                if(TextUtils.isEmpty(editText.getText().toString())){
                    showError("请先选择页码");
                    return;
                }
                if(questionKonwLedgeNow == null){
                    showError("请先选择知识点或试题");
                    return;
                }
                Intent intent = new Intent(getActivity(),AddQuestionActiivtyNew.class);
                Bundle bundle = new Bundle();
                bundle.putString("bookId",qustionBookNow.getId());
                bundle.putString("bookName",qustionBookNow.getName());
                bundle.putString("pageNum",pageNum);
                bundle.putString("bookQaType",questionKonwLedgeNow.getType()+"");
                bundle.putString("bookQaId",questionKonwLedgeNow.getId());
                bundle.putString("chapterIds",questionKonwLedgeNow.getChapterId());
                if(questionKonwLedgeNow.getType() == 1){
                    if(!TextUtils.isEmpty(questionKonwLedgeNow.getQuestionNum()))
                        bundle.putString("questionNum",questionKonwLedgeNow.getQuestionNum());
                    else
                        bundle.putString("questionNum","");
                }
                intent.putExtra("type",0);
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case R.id.tab_question_fragment_bookName_tv:
                if(qustionBooks == null || qustionBooks.size()==0){
                    tabQuestionPresenter.getData();
                }else if(qustionBooks.size()>1){
                    questionSelectBookPop.showPop(textView_bookName,qustionBooks);
                }
                break;
            case R.id.tab_question_fragment_bookName_loaderror_tv:
                tabQuestionPresenter.getData();
                textView_bookName_error.setVisibility(View.GONE);
                break;
            case R.id.tab_question_fragment_knowledgepoint_loaderror_tv:
                if(TextUtils.isEmpty(editText.getText().toString().trim())){
                    showError("请输入页码");
                    return;
                }
                questionKonwLedgeNow = null;
                pageNum = editText.getText().toString();
                tabQuestionPresenter.getCanChoiceKnowLedgePointList(qustionBookNow.getId(),editText.getText().toString());
                textView_know_error.setVisibility(View.GONE);
                break;
            case R.id.tab_question_fragment_jihuo_tv:
                //TODO  激活
                Intent intent_jihuo = new Intent(getActivity(), BookActivateActivity.class);
                startActivity(intent_jihuo);
                break;
        }
    }

    public void setSelectBookName(QustionBook qustionBook){
        if(qustionBook == null)
            return;
        this.qustionBookNow = qustionBook;
        textView_bookName.setText(qustionBook.getName());
    }

    public void setSelectExm(QuestionKonwLedge questionKonwLedge){
        if(questionKonwLedge == null)
            return;
        linearLayout_ask_body.setVisibility(View.VISIBLE);
        this.questionKonwLedgeNow = questionKonwLedge;
        if(!TextUtils.isEmpty(questionKonwLedge.getQuestionNum()))
            textView_exm_name.setText(questionKonwLedge.getName() + " 第" + questionKonwLedge.getQuestionNum() + "题");
        else
            textView_exm_name.setText(questionKonwLedge.getName());
        emptyViewLayout.showLoading();
        tabQuestionPresenter.getRecommQuesList(questionKonwLedgeNow);
    }

    @Override
    public void showBookList(List<QustionBook> qustionBooks) {
//        if(questionRecomms!=null){
//            questionRecomms.clear();
//            questionListAdapter.notifyDataSetChanged();
//        }
        this.qustionBooks = qustionBooks;
        if(qustionBooks.size()>0 && qustionBooks.get(0)!=null && !TextUtils.isEmpty(qustionBooks.get(0).getName())) {
            textView_bookName.setText(qustionBooks.get(0).getName());
            qustionBookNow = qustionBooks.get(0);
            if(qustionBooks.size()>1){
                imageView_bookName_img.setVisibility(View.VISIBLE);
            }else{
                imageView_bookName_img.setVisibility(View.GONE);
            }
        }else {
            textView_bookName.setText("当前科目下没有图书");
            imageView_bookName_img.setVisibility(View.GONE);
        }
    }

    @Override
    public void showKnowLedgeList(List<QuestionKonwLedge> questionKonwLedges) {
        textView_know_error.setVisibility(View.VISIBLE);
        this.questionKonwLedges = questionKonwLedges;
        if(questionKonwLedges.size()>0 && questionKonwLedges.get(0)!=null && !TextUtils.isEmpty(questionKonwLedges.get(0).getName())) {
            linearLayout_ask_body.setVisibility(View.VISIBLE);
            textView_exm_name.setText("点击选择知识点或试题");
            if(questionKonwLedges.size()>1){
                imageView_know.setVisibility(View.VISIBLE);
            }else{
                if(!TextUtils.isEmpty(questionKonwLedges.get(0).getQuestionNum()))
                    textView_exm_name.setText(questionKonwLedges.get(0).getName() + " 第" + questionKonwLedges.get(0).getQuestionNum() + "题");
                else
                    textView_exm_name.setText(questionKonwLedges.get(0).getName());
                questionKonwLedgeNow = questionKonwLedges.get(0);
                imageView_know.setVisibility(View.GONE);
                tabQuestionPresenter.getRecommQuesList(questionKonwLedgeNow);
                emptyViewLayout.showLoading();
            }
//            textView_exm_name.setText(questionKonwLedges.get(0).getName());
//            questionKonwLedgeNow = questionKonwLedges.get(0);
//            questionSelectExmPop.showPop(textView_exm_name,questionKonwLedges);
//            tabQuestionPresenter.getRecommQuesList(questionKonwLedgeNow.getId());
        }else {
            linearLayout_ask_body.setVisibility(View.GONE);
            textView_exm_name.setText("图书当前页码下，无知识点或试题");
            imageView_know.setVisibility(View.GONE);
        }
    }

    @Subscribe
    public void onEventAsync(UpdateEvent event)
    {
        tabQuestionPresenter.getData();
    }

    @Override
    public void showRecommQuesList(List<QuestionRecomm> questionRecommLis) {
        if(questionRecommLis == null || questionRecommLis.size() == 0){
            emptyViewLayout.showEmpty();
            return;
        }
        emptyViewLayout.showContentView();
        this.questionRecomms = questionRecommLis;
        questionListAdapter = new QuestionListAdapter(getActivity(),questionRecomms);
        listView.setAdapter(questionListAdapter);
    }

    @Override
    public void bookListLoadError() {
        textView_bookName.setText("图书加载失败");
        imageView_bookName_img.setVisibility(View.GONE);
        textView_bookName_error.setVisibility(View.VISIBLE);
    }

    @Override
    public void knowledgepointLoadError() {
        imageView_know.setVisibility(View.GONE);
        textView_exm_name.setText("知识点或试题加载失败");
        textView_know_error.setVisibility(View.VISIBLE);
    }

    @Override
    public void recommQuesListLoadError() {
        emptyViewLayout.showError();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    private Toast toast;
    @Override
    public void showError(String message) {
        if(toast == null){
            toast = Toast.makeText(getActivity(),message,Toast.LENGTH_SHORT);
        }else
            toast.setText(message);
        toast.show();
    }

    @Override
    public Context context() {
        return getActivity();
    }
}
