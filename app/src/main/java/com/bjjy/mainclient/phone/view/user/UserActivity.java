package com.bjjy.mainclient.phone.view.user;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bjjy.mainclient.persenter.UserPersenter;
import com.bjjy.mainclient.phone.app.BaseActivity;
import com.bjjy.mainclient.phone.view.setting.WebViewActivity;
import com.bjjy.mainclient.phone.view.user.utils.RotateAnimationUtil;
import com.dongao.mainclient.core.util.NetUtils;
import com.dongao.mainclient.model.common.Constants;
import com.bjjy.mainclient.phone.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by fengzongwei on 2016/5/5 0005.
 * 登录注册页面
 */
public class UserActivity extends BaseActivity implements UserView{
    @Bind(R.id.login_container)
    LinearLayout linearLayout_login;
    @Bind(R.id.registe_container)
    LinearLayout linearLayout_registe;
    @Bind(R.id.container)
    RelativeLayout container;
    @Bind(R.id.container_parent)
    RelativeLayout contaier_parent;
    @Bind(R.id.top_title_text)
    TextView textView_title;
    //登录相关view
    @Bind(R.id.login_username_et)
    EditText usernameEdit;
    @Bind(R.id.login_psw_et)
    EditText passwordEdit;
    @Bind(R.id.login_bt)
    Button login_bt;

    //注册相关view
    @Bind(R.id.registe_phone_et)
    EditText editText_phone;
    @Bind(R.id.registe_phone_checknumber_et)
    EditText editText_checknumber;
    @Bind(R.id.registe_psw_et)
    EditText editText_psw;
    @Bind(R.id.registe_get_checcknumber_bt)
    TextView bt_checkNumber;
    @Bind(R.id.registe_bt)
    Button button_registe;

    private UserPersenter userPersenter;

    private RotateAnimationUtil rotateAnimationUtil;
    private int screenWidth;

    private static final String PHONE = "^1[3|4|5|8|7][0-9]\\d{8}$";

    private String resend = "倒计时";
    private boolean isGetValidNow = false;
    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if(msg.what == 0){
                bt_checkNumber.setText("重新发送");
                bt_checkNumber.setBackgroundDrawable(getResources().getDrawable(R.drawable.registe_check_bt));
                bt_checkNumber.setTextColor(Color.parseColor("#feaf32"));
                bt_checkNumber.setEnabled(true);
                return;
            }
            bt_checkNumber.setText(resend+msg.what+"s");
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user);
        WindowManager wm = this.getWindowManager();
        screenWidth = wm.getDefaultDisplay().getWidth();
        ButterKnife.bind(this);
        userPersenter = new UserPersenter();
        userPersenter.attachView(this);
        initView();
    }

    @Override
    public void initView() {
        textView_title.setText("用户中心");
        rotateAnimationUtil = new RotateAnimationUtil(container, linearLayout_login,
                linearLayout_registe);
        ViewGroup.LayoutParams layoutParams = container.getLayoutParams();
        layoutParams.width = (int)(screenWidth*0.8);
        container.setLayoutParams(layoutParams);
        Animation animation = AnimationUtils.loadAnimation(this,R.anim.user_dialog_in);
        contaier_parent.startAnimation(animation);
    }

    @OnClick(R.id.login_registe) void goRegiste(){
        rotateAnimationUtil.applyRotateAnimation(1, 0, 90);
    }

    @OnClick(R.id.registe_login) void goLogin(){
        rotateAnimationUtil.applyRotateAnimation(-1, 0, -90);
    }

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View v) {

    }

    @OnClick(R.id.registe_ruel_tv) void openRule(){
        Intent intent = new Intent(UserActivity.this, WebViewActivity.class);
        intent.putExtra(Constants.APP_WEBVIEW_TITLE, "服务条款和细则");
        intent.putExtra(Constants.APP_WEBVIEW_URL, "http://member.bjsteach.com/study/static/html/agreement.html");
        startActivity(intent);
    }

    @OnClick(R.id.registe_get_checcknumber_bt) void getCheckNumber(){
        if(NetUtils.checkNet().available) {
            if (editText_phone.getText().toString() == null || !editText_phone.getText().toString().matches(PHONE)) {
                showError("请输入正确的手机号");
                return;
            }
            if (!isGetValidNow && bt_checkNumber.getText().equals("重新发送") || bt_checkNumber.getText().equals("获取")) {
                isGetValidNow = true;
                userPersenter.getMobileValid();
            }
        }
    }

    @OnClick(R.id.login_bt) void submit() {
        userPersenter.getData();
    }

    @Override
    public String username() {
        return usernameEdit.getText().toString();
    }

    @Override
    public String password() {
        return passwordEdit.getText().toString();
    }

    @Override
    public void loginSuccess() {
        Toast.makeText(UserActivity.this,"登录成功",Toast.LENGTH_SHORT).show();
    }

    @Override
    public String phoneNumber() {
        return editText_phone.getText().toString();
    }

    @Override
    public String checkNumber() {
        return editText_checknumber.getText().toString();
    }

    @Override
    public String pswRegiste() {
        return editText_psw.getText().toString();
    }

    @Override
    public void registeSuccess() {
        usernameEdit.setText(phoneNumber());
        passwordEdit.setText(pswRegiste());
        rotateAnimationUtil.applyRotateAnimation(-1, 0, -90);
        Toast.makeText(UserActivity.this,"注册成功",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void switchBtStatus() {
        isGetValidNow = true;
        bt_checkNumber.setText( 60 + "s");
        bt_checkNumber.setTextColor(Color.parseColor("#999999"));
        bt_checkNumber.setBackgroundDrawable(getResources().getDrawable(R.drawable.registe_check_enable_bt));
//            registerPersenter.getMobileValid();
        new Thread() {
            @Override
            public void run() {
                try {
                    int i = 59;
                    while (i >= 0) {
                        handler.sendEmptyMessage(i);
                        i--;
                        Thread.sleep(1000);
                    }
                } catch (InterruptedException e) {
                }
            }
        }.start();
    }

    @Override
    public void showLoading() {
        login_bt.setEnabled(false);
        button_registe.setEnabled(false);
        editText_checknumber.setEnabled(false);
        editText_phone.setEnabled(false);
        editText_psw.setEnabled(false);
        passwordEdit.setEnabled(false);
        usernameEdit.setEnabled(false);
    }

    @Override
    public void hideLoading() {
        login_bt.setEnabled(true);
        button_registe.setEnabled(true);
        editText_checknumber.setEnabled(true);
        editText_phone.setEnabled(true);
        editText_psw.setEnabled(true);
        passwordEdit.setEnabled(true);
        usernameEdit.setEnabled(true);
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {
        hideLoading();
    }

    @Override
    public Context context() {
        return this;
    }

}
