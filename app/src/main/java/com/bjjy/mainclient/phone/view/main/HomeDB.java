package com.bjjy.mainclient.phone.view.main;

import android.content.Context;

import com.dongao.mainclient.model.local.KaoQianDBHelper;
import com.yunqing.core.db.DBExecutor;
import com.yunqing.core.db.sql.Sql;
import com.yunqing.core.db.sql.SqlFactory;

import java.util.List;

/**
 * Created by wyc on 15/4/15.
 * 此类DAO只用于查询和特殊操作
 */
public class HomeDB {
    Context mContext;
    DBExecutor dbExecutor = null;
    Sql sql = null;

    public HomeDB(Context mContext){
        this.mContext = mContext;
        dbExecutor =  DBExecutor.getInstance(KaoQianDBHelper.getInstance(mContext));
    }

    /**
     * 如果相同就不追加
     * @param messageValue
     */
    public void insert(MessageValue messageValue){
        dbExecutor.insert(messageValue);
//        if(findById(messageValue.getId()) == null){
//            dbExecutor.insert(messageValue);
//        }else{
//            update(messageValue);
//        }
    }

    public void insertAll(List<MessageValue> recommens){
        dbExecutor.insertAll(recommens);
    }

    public void update(MessageValue collection){
        dbExecutor.updateById(collection);
    }

    public boolean delete(String userId){
        sql = SqlFactory.makeSql(MessageValue.class,"delete from t_home where userId="+userId);
        return dbExecutor.execute(sql);
    }

    public MessageValue find(String id){
        return dbExecutor.findById(MessageValue.class,id);
    }

    /**
     * 搜索某一记录
     * @param id
     * @return
     */
    public MessageValue findById(String id){
        sql = SqlFactory.find(MessageValue.class).where("id=?", new Object[]{id});
        return dbExecutor.executeQueryGetFirstEntry(sql);
    }


    /**
     * 搜索全部记录count
     * @return
     */
    public long findAllCount(){
       return dbExecutor.count(MessageValue.class);
    }

    /**
     * 搜索某一用户全部记录
     * @param userId
     * @return
     */
    public int findAllCount(String userId){
        sql = SqlFactory.find(MessageValue.class).where("userId=?", new Object[]{userId});
        return dbExecutor.executeQuery(sql).size();
    }


    /**
     * 搜索某一用户全部记录
     * @param userId
     * @return
     */
    public List<MessageValue> findAll(String userId, int page, int pageCount){
        sql = SqlFactory.find(MessageValue.class).where("userId=?", new Object[]{userId}).limit(pageCount).offset((page-1)*pageCount).orderBy("id",true);
        return dbExecutor.executeQuery(sql);
    }
}
