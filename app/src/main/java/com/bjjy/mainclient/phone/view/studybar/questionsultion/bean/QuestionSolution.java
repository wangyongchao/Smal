package com.bjjy.mainclient.phone.view.studybar.questionsultion.bean;

/**
 * Created by wyc on 2016/6/6.
 */
public class QuestionSolution {
    private String title;//公告标题
    private String createTime; //公告时间
    private String questionId;//试题Id

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }
}
