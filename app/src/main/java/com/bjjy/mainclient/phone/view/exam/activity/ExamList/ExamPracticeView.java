package com.bjjy.mainclient.phone.view.exam.activity.ExamList;



import com.dongao.mainclient.model.mvp.MvpView;
import com.bjjy.mainclient.phone.widget.EmptyViewLayout;
import com.bjjy.mainclient.phone.widget.pulltorefresh.PullToRefreshExpandableListView;

/**
 * Created by wyc on 2016/5/10.
 */
public interface ExamPracticeView extends MvpView {
    void refreshAdapter();
    void setAllCollapsed();
    EmptyViewLayout getEmptyLayout();
    PullToRefreshExpandableListView getPTREListView();
    void initAdapter();
}
