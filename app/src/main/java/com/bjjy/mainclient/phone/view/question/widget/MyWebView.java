package com.bjjy.mainclient.phone.view.question.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.webkit.WebView;

/**
 * Created by fengzongwei on 2016/7/22 0022.
 * 为了解决scrollview嵌套webview的横向滑动困难问题
 */
public class MyWebView extends WebView{

        public MyWebView(Context context, AttributeSet attrs) {
            super(context, attrs);
        }
        public MyWebView(Context context) {
            super(context);
        }
        @Override
        public boolean onTouchEvent(MotionEvent event){
//            requestDisallowInterceptTouchEvent(true);
            float startX = 0,startY = 0;
            if(event.getAction() == MotionEvent.ACTION_DOWN){
                startX = event.getX();
                startY = event.getY();
            }
            if(event.getAction() == MotionEvent.ACTION_MOVE){
                if (Math.abs(event.getX() - startX) > 100.0 && Math.abs(event.getY() - startY) < 300)
                    requestDisallowInterceptTouchEvent(true);
                else
                    requestDisallowInterceptTouchEvent(false);
            }
            return super.onTouchEvent(event);
        }
}
