package com.bjjy.mainclient.phone.view.studybar.message;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;

import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseFragmentActivity;

import org.greenrobot.eventbus.EventBus;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by wyc on 2016/6/6.
 */
public class MyMessageActivity extends BaseFragmentActivity {
    @Bind(R.id.fragment)
    FrameLayout fragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.studybar_mymessage_activity);
        ButterKnife.bind(this);
        initView();
    }

    @Override
    public void initView() {
       MyMessageFragment myMessageFragment=new MyMessageFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment,myMessageFragment).commit();
    }



    @Override
    public void initData() {
    }



    @Override
    public void onClick(View v) {

    }




    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        initData();
    }
}
