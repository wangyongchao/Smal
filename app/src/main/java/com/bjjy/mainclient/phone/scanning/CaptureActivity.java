package com.bjjy.mainclient.phone.scanning;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bjjy.mainclient.phone.scanning.decoding.CaptureActivityHandler;
import com.bjjy.mainclient.phone.scanning.view.ViewfinderView;
import com.dongao.mainclient.model.bean.play.CourseWare;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.scanning.camera.CameraManager;
import com.bjjy.mainclient.phone.scanning.control.AmbientLightManager;
import com.bjjy.mainclient.phone.scanning.control.BeepManager;
import com.bjjy.mainclient.phone.scanning.decoding.InactivityTimer;
import com.bjjy.mainclient.phone.utils.NetworkUtil;
import com.bjjy.mainclient.phone.view.exam.view.CustomDialog;
import com.bjjy.mainclient.phone.view.play.audition.AuditionPlayActivity;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.DecodeHintType;
import com.google.zxing.Result;

import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Vector;

/**
 * 二维码扫描
 *
 * @author zhanghongliu
 */
public class CaptureActivity extends Activity implements Callback, OnClickListener, CapturesultView {

    private CaptureActivityHandler handler;
    private ViewfinderView viewfinderView;
    private boolean hasSurface;
    private Vector<BarcodeFormat> decodeFormats;
    private String characterSet;
    private InactivityTimer inactivityTimer;
    private MediaPlayer mediaPlayer;
    private boolean playBeep;
    private static final float BEEP_VOLUME = 0.10f;
    private boolean vibrate;
    private CameraManager cameraManager;
    private BeepManager beepManager;
    private AmbientLightManager ambientLightManager;
    private Map<DecodeHintType, ?> decodeHints;
    private CustomDialog dialog_permission;
    // private Button cancelScanButton;
    private boolean isOpen;
    private Camera camera;
    private ProgressDialog progress;
    private String userId;
    private CapturesultPersenter persenter;
    private TextView qrCode;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.camera);
        // ViewUtil.addTopView(getApplicationContext(), this,
        // R.string.scan_card);
        persenter = new CapturesultPersenter();
        persenter.attachView(this);

        viewfinderView = (ViewfinderView) findViewById(R.id.viewfinder_view);
        // cancelScanButton = (Button) this.findViewById(R.id.btn_cancel_scan);
        hasSurface = false;
        inactivityTimer = new InactivityTimer(this);

        beepManager = new BeepManager(this);
        ambientLightManager = new AmbientLightManager(this);
        initViews();
    }

    private void initViews() {
        userId = SharedPrefHelper.getInstance(this).getUserId() + "";
        dialog_permission = new CustomDialog(this, R.layout.scanning_dialog, R.style.Theme_dialog);
        dialog_permission.setCanceledOnTouchOutside(false);
        dialog_permission.findViewById(R.id.sanning_sure).setOnClickListener(this);
        progress = new ProgressDialog(this);
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setMessage("请稍后…………");

        qrCode=(TextView)findViewById(R.id.tv_qrcode);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int ScreenW = dm.widthPixels;
        int ScreenH = dm.heightPixels;
        qrCode.setPadding(75, 0, 75, ScreenH*10/35);//左上右下

        FrameLayout.LayoutParams p = new FrameLayout.LayoutParams(ScreenW, ScreenH);
    }

    @Override
    protected void onResume() {
        super.onResume();
        cameraManager = new CameraManager(getApplication());
        viewfinderView.setCameraManager(cameraManager);
        handler = null;
        resetStatusView();

        SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
        SurfaceHolder surfaceHolder = surfaceView.getHolder();
        if (hasSurface) {
            initCamera(surfaceHolder);
        } else {
            surfaceHolder.addCallback(this);
            surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        }

        beepManager.updatePrefs();
        ambientLightManager.start(cameraManager);

        inactivityTimer.onResume();

        decodeFormats = null;
        characterSet = null;

        findViewById(R.id.btnNavBack).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                CaptureActivity.this.finish();
            }
        });
        findViewById(R.id.light).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (isOpen) {
                    turnOff();
                    isOpen = false;
                } else {
                    turnOn();
                    isOpen = true;
                }
            }
        });
    }

    private void turnOn() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                camera = cameraManager.getCamera();
                if (camera != null) {
                    camera = cameraManager.getCamera();
                    Camera.Parameters mParameters = camera.getParameters();
                    mParameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);//打开Camera.Parameters.FLASH_MODE_OFF则为关闭
                    camera.setParameters(mParameters);
                }
            }
        }).start();
    }

    private void turnOff() {
        camera = cameraManager.getCamera();
        if (camera != null) {
            Camera.Parameters mParameters = camera.getParameters();
            mParameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);//打开Camera.Parameters.FLASH_MODE_OFF则为关闭
            camera.setParameters(mParameters);
        }
    }

    private void resetStatusView() {
        viewfinderView.setVisibility(View.VISIBLE);
    }

    /**
     * 把onPause中的 closeDriver放到 onStop 解决（不了） 退出可能黑屏的问题 这个只在小米2上遇到过。
     */
    @Override
    protected void onPause() {
        super.onPause();
        inactivityTimer.onPause();
        if (handler != null) {
            handler.quitSynchronously();
            handler = null;
        }
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        Log.d("callback", "closeDriver前:" + Calendar.getInstance().get(Calendar.SECOND));
        cameraManager.closeDriver();
        Log.d("callback", "closeDriver后:" + Calendar.getInstance().get(Calendar.SECOND));
//        finish();
    }

    @Override
    protected void onDestroy() {
        inactivityTimer.shutdown();
        super.onDestroy();
    }


    private int id, type;
    private String qrUrl;

    /**
     * Handler scan result
     *
     * @param result
     * @param barcode
     */
    public void handleDecode(Result result, Bitmap barcode, float scaleFactor) {
        inactivityTimer.onActivity();
//        handler.restartPreviewAndDecode();
        // viewfinderView.drawResultBitmap(barcode);
        playBeepSoundAndVibrate();
        qrUrl = result.getText();
//        qrUrl="http://member.bjsteach.com/qr/book/40687.html";
//        qrUrl="http:/qrcode.api.bjsteach.com/qr/api/decodeQrCode?id=73688&type=2";
        // FIXME
//		if (resultString.equals("")) {
//			Toast.makeText(CaptureActivity.this, "Scan failed!", Toast.LENGTH_SHORT).show();
//			return;
//		}
        if (qrUrl.indexOf("bjsteach.com") == -1) {
            Toast.makeText(CaptureActivity.this, "暂不提供非东奥二维码扫描功能", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
//		int id = 0;
        if(qrUrl.contains("http://member.bjsteach.com/qr/book")){
            persenter.getData();
        }else if(qrUrl.contains("qrcode.api.bjsteach.com")){
            qrUrl=qrUrl.replace("&","@");
            persenter.getData();
        }
    }


    private void checkIsActive(int id) {
        progress.show();
//        Task ts_message = new Task(TaskType.TS_CHECKACTIVE, URLs.checkIsActive(id+"",userId,"1.5.0"));
//        ApiClient.getData(ts_message, achandler);
    }

    private void initCamera(SurfaceHolder surfaceHolder) {
        if (surfaceHolder == null) {
            return;
        }
        if (cameraManager.isOpen()) {
            return;
        }
        try {
            cameraManager.openDriver(surfaceHolder);
            if (handler == null) {
                handler = new CaptureActivityHandler(this, decodeFormats, decodeHints, characterSet, cameraManager);
            }
        } catch (Exception e) {
            // Toast.makeText(this, "请到设置页面开启摄像头权限", Toast.LENGTH_LONG).show();
//			new AlertDialog.Builder(this).setCancelable(false).setMessage("温馨提示：此功能需要开启相机权限，如您还未开启，请在系统设置查找“东奥会计课堂”并启用访问。")
//					.setPositiveButton("确定", new DialogInterface.OnClickListener() {
//
//						@Override
//						public void onClick(DialogInterface paramDialogInterface, int paramInt) {
//							// TODO Auto-generated method stub
//							// GlobalModel.getInstance().getDownloadTask().removeAllTast();
//							finish();
//						}
//					}).show();
            dialog_permission.show();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (!hasSurface) {
            hasSurface = true;
            initCamera(holder);
        }

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        hasSurface = false;

    }

    public ViewfinderView getViewfinderView() {
        return viewfinderView;
    }

    public Handler getHandler() {
        return handler;
    }

    public void drawViewfinder() {
        viewfinderView.drawViewfinder();

    }

    private void initBeepSound() {
        if (playBeep && mediaPlayer == null) {
            // The volume on STREAM_SYSTEM is not adjustable, and users found it
            // too loud,
            // so we now play on the music stream.
            setVolumeControlStream(AudioManager.STREAM_MUSIC);
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setOnCompletionListener(beepListener);

            AssetFileDescriptor file = getResources().openRawResourceFd(R.raw.beep);
            try {
                mediaPlayer.setDataSource(file.getFileDescriptor(), file.getStartOffset(), file.getLength());
                file.close();
                mediaPlayer.setVolume(BEEP_VOLUME, BEEP_VOLUME);
                mediaPlayer.prepare();
            } catch (IOException e) {
                mediaPlayer = null;
            }
        }
    }

    private static final long VIBRATE_DURATION = 200L;

    private void playBeepSoundAndVibrate() {
        if (playBeep && mediaPlayer != null) {
            mediaPlayer.start();
        }
        if (vibrate) {
            Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
            vibrator.vibrate(VIBRATE_DURATION);
        }
    }

    /**
     * When the beep has finished playing, rewind to queue up another one.
     */
    private final OnCompletionListener beepListener = new OnCompletionListener() {
        public void onCompletion(MediaPlayer mediaPlayer) {
            mediaPlayer.seekTo(0);
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
//		Utils.pushOpenScreenEvent(this, "PHONE_SCANNER_SCREEN");
    }

    public CameraManager getCameraManager() {
        return cameraManager;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sanning_sure:
                finish();
                break;
        }
    }

    @Override
    public void setData(List<CourseWare> CourseWares) {

        if (CourseWares != null && CourseWares.size() > 0) {
            if (CourseWares.size() == 1) {
                if (true) {
                    Intent intent = new Intent(CaptureActivity.this, AuditionPlayActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("cw", CourseWares.get(0));
                    intent.putExtras(bundle);
                    startActivity(intent);
                    finish();
                } else {
                    if (!CourseWares.get(0).getCourseDto().isHaveBuy()) {
                        if (SharedPrefHelper.getInstance(this).isLogin()) {
                            //提示去购买
                            Toast.makeText(CaptureActivity.this, "该课程为付费课程，请购买", Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent(CaptureActivity.this,ScannerPayActivity.class);
                            startActivity(intent);
                        } else {
                            //提示去登陆
                            Toast.makeText(CaptureActivity.this, "您还没有登陆，请登录", Toast.LENGTH_SHORT).show();
                        }
                        finish();
                    } else {
                        Intent intent = new Intent(CaptureActivity.this, AuditionPlayActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("cw", CourseWares.get(0));
                        intent.putExtras(bundle);
                        startActivity(intent);
                        finish();
                    }
                }
            } else {
                if (true) {
                    Intent intent = new Intent(CaptureActivity.this, CaptureResultActivity.class);
                    intent.putExtra("list", (Serializable) CourseWares);
                    startActivity(intent);
                    finish();
                } else {
                    if (!CourseWares.get(0).getCourseDto().isHaveBuy()) {
                        if (SharedPrefHelper.getInstance(this).isLogin()) {
                            //提示去购买
                            Toast.makeText(CaptureActivity.this, "该课程为付费课程，请购买", Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent(CaptureActivity.this,ScannerPayActivity.class);
                            startActivity(intent);
                        } else {
                            //提示去登陆
                            Toast.makeText(CaptureActivity.this, "您还没有登陆，请登录", Toast.LENGTH_SHORT).show();
                        }
                        finish();
                    } else {
                        Intent intent = new Intent(CaptureActivity.this, CaptureResultActivity.class);
                        intent.putExtra("list", (Serializable) CourseWares);
                        startActivity(intent);
                        finish();
                    }
                }
            }
        }else{
            Toast.makeText(CaptureActivity.this, "课件不存在", Toast.LENGTH_SHORT).show();
            finish();
        }

    }

    @Override
    public String getQrUrl() {
        return qrUrl;
    }

    @Override
    public void setType(String type) {
//        if (type.equals("1")) {
//            persenter.geDataType1();
//        } else if (type.equals("2")) {
//
//        }
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {
        if (NetworkUtil.isNetworkAvailable(getActivity())) {
            Toast.makeText(CaptureActivity.this, message, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(CaptureActivity.this, "请确认网络是否联通", Toast.LENGTH_SHORT).show();
        }
        finish();
    }

    @Override
    public Context context() {
        return this;
    }

    @Override
    public Activity getActivity() {
        return this;
    }
}