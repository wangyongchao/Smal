package com.bjjy.mainclient.phone.view.play.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.bjjy.mainclient.phone.view.play.fragment.CourseHandOutFragment;
import com.bjjy.mainclient.phone.view.play.fragment.CourseIntroFragment;
import com.bjjy.mainclient.phone.view.play.fragment.CourseListFragment;
import com.dongao.mainclient.model.bean.play.CourseDetail;
import com.dongao.mainclient.model.bean.play.CourseWare;

import java.util.List;

public class PlayContentFrameAdapter extends FragmentPagerAdapter{

	private String[] titles;
	public PlayContentFrameAdapter(FragmentManager fm) {
		super(fm);
		this.courseListFragment = new CourseListFragment();
		this.courseHandOutFragment = new CourseHandOutFragment();
		this.courseInfoFragment=new CourseIntroFragment();
	}
	private CourseListFragment courseListFragment;
	private CourseHandOutFragment courseHandOutFragment;
	private CourseIntroFragment courseInfoFragment;
	
	public void setTitles(String[] titles) {
		this.titles = titles;
	}

	@Override
	public Fragment getItem(int pos) {
		// TODO Auto-generated method stub
		Fragment fragment = null;
		switch (pos){
			case 0:
				fragment = courseInfoFragment;
				break;
			case 1:
				fragment = courseListFragment;
				break;
			default:
				fragment = courseHandOutFragment;
				break;
		}
		return fragment;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 3;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		if (titles != null){
			return titles[position].toString();
		}
		return super.getPageTitle(position);
	}

	/**
	 * 设置课程详情的Url
	 * @param url
	 */
	public void setCourseInfoUrl(String url){
		courseInfoFragment.setCourseInfo(url);
	}


	/**
	 * 显示没有分组的数据
	 */
	public void setCwListData(List<CourseWare> courseWareList){
		courseListFragment.showCwData(courseWareList);
	}

	/**
	 * 加载课程目录数据失败(接口失败且数据库无数据)
	 */
	public void setCourseListLoadDataError(){
		courseListFragment.setLoadDataError();
	}

	/**
	 * 设置列表页面的数据
	 * @param courseListData  数据集合
	 * @param cwCode 当前播放视频的课程id
	 * @param year  当期播放视频的所属年份
	 */
//	public void setCourseListData(List<CourseWare> courseListData,String cwCode,String year){
//		courseListFragment.setCourseListData(courseListData,cwCode,year);
//	}

	/**
	 * 加载课程目录数据失败(接口失败且数据库无数据)
	 */
//	public void setCourseListLoadDataError(){
//		courseListFragment.setLoadDataError();
//	}

	/**
	 * 更新播放状态
	 */
	public void notifyPlayStatus(){
		courseListFragment.notifyAdapter();
	}

	/**
	 * 设置课程讲义url
	 * @param courseHandOutUrl
	 * @param isCanChangeProgress  当前是否可以通过拖动讲义改变视频播放进度
	 */
	public void setCourseHandOutUrl(String courseHandOutUrl,boolean isCanChangeProgress){
		courseHandOutFragment.setCourseHandOutUrl(courseHandOutUrl, true);
	}
//

	/**
	 * 设置当前讲义进度与视频播放进度一致
	 * @param position
	 */
	public void setCourseHandOutFragmentWebviewProgress(long position){
		courseHandOutFragment.setWebViewProgress(position);
	}

	/**
	 * 刷新课程课件列表的播放时间
	 */
//	public void refreshPlayTime(String courseWareId){
//		courseListFragment.refreshStudyTime(courseWareId);
//	}

}
