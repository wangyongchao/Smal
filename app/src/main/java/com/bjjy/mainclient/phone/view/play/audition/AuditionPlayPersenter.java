package com.bjjy.mainclient.phone.view.play.audition;

import android.util.Base64;

import com.dongao.mainclient.core.util.FileUtil;
import com.dongao.mainclient.model.bean.play.CourseWare;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ParamsUtils;
import com.bjjy.mainclient.phone.download.db.PlayParamsDB;
import com.bjjy.mainclient.phone.view.play.fragment.uploadBean.PlayUrlBean;
import com.bjjy.mainclient.phone.view.play.utils.AESHelper;
import com.bjjy.mainclient.phone.view.play.utils.Constans;
import com.bjjy.mainclient.phone.view.play.utils.SignUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by fengzongwei on 2016/5/30 0030.
 */
public class AuditionPlayPersenter extends BasePersenter<AuditionPlayView> {

    private String userId;
    private PlayParamsDB playParamsDB;
    CourseWare courseWare;

    @Override
    public void attachView(AuditionPlayView mvpView) {
        super.attachView(mvpView);
        playParamsDB=new PlayParamsDB(getMvpView().context());
        userId=SharedPrefHelper.getInstance(getMvpView().context()).getUserId();
    }

    @Override
    public void getData() {
        getMvpView().showLoading();
        courseWare=getMvpView().getCw();
        apiModel.getData(ApiClient.getClient().getPlayUrl(ParamsUtils.getInstance(getMvpView().context()).getPlayPath(courseWare.getCwId())));
    }

    @Override
    public void setData(String obj) {
        try {
            JSONObject total = new JSONObject(obj);
            String code = total.optString("code");
            if (!code.equals("1000")) {
                return;
            }
            PlayUrlBean playUrlBean = new PlayUrlBean();

            PlayUrlBean.VideoBean videoBean = new PlayUrlBean.VideoBean();
            PlayUrlBean.VideoBean.SdBean sdModel = new PlayUrlBean.VideoBean.SdBean();
            PlayUrlBean.VideoBean.CifBean cifModel = new PlayUrlBean.VideoBean.CifBean();

            JSONObject body = total.getJSONObject("body");
            JSONObject video = body.getJSONObject("video");

            String key = body.optString("cipherkeyDeal");
            playUrlBean.setCipherkeyDeal(key);

            JSONObject sd = video.getJSONObject("sd");
            JSONObject cif = video.getJSONObject("cif");

            JSONArray three = sd.optJSONArray("1.5");
            JSONArray two = sd.optJSONArray("1.2");
            JSONArray one = sd.optJSONArray("1.0");

            JSONArray cfthree = cif.optJSONArray("1.5");
            JSONArray cftwo = cif.optJSONArray("1.2");
            JSONArray cfone = cif.optJSONArray("1.0");

            /**
             *设置sd（标清）不同倍速url
             */
            List<String> sdthree = new ArrayList();
            for (int i = 0; i < three.length(); i++) {
                String rrr = (String) three.get(i);
                sdthree.add(rrr);
            }
            sdModel.setThree(sdthree);

            List<String> sdtwo = new ArrayList();
            for (int i = 0; i < two.length(); i++) {
                String rrr = (String) two.get(i);
                sdtwo.add(rrr);
            }
            sdModel.setTwo(sdtwo);

            List<String> sdone = new ArrayList();
            for (int i = 0; i < one.length(); i++) {
                String rrr = (String) one.get(i);
                sdone.add(rrr);
            }
            sdModel.setOne(sdone);
            videoBean.setSd(sdModel);

            /**
             *设置cif（流畅）不同倍速url
             */
            List<String> cf_three = new ArrayList();
            for (int i = 0; i < cfthree.length(); i++) {
                String rrr = (String) cfthree.get(i);
                cf_three.add(rrr);
            }
            cifModel.setThree(cf_three);

            List<String> cf_two = new ArrayList();
            for (int i = 0; i < cftwo.length(); i++) {
                String rrr = (String) cftwo.get(i);
                cf_two.add(rrr);
            }
            cifModel.setTwo(cf_two);

            List<String> cf_one = new ArrayList();
            for (int i = 0; i < cfone.length(); i++) {
                String rrr = (String) cfone.get(i);
                cf_one.add(rrr);
            }
            cifModel.setOne(cf_one);
            videoBean.setCif(cifModel);
            playUrlBean.setVideo(videoBean);

            JSONObject cipherkeyVo = body.getJSONObject("cipherkeyVo");

            String app = cipherkeyVo.optString("app");
            String type = cipherkeyVo.optString("type");
            String vid = cipherkeyVo.optString("vid");
            String vkey = cipherkeyVo.optString("key");
            String vcode = cipherkeyVo.optString("code");
            String message = cipherkeyVo.optString("message");
            String lecture = body.optString("handOut");
            playParamsDB.add(userId, courseWare.getCwId(), app, type, vid, vkey, vcode, message);
            courseWare.setMobileLectureUrl(lecture);
            courseWare.setMobileVideoUrl(playUrlBean.getVideo().getSd().getOne().get(0));

            String jm = new String(AESHelper.decrypt(Base64.decode(vkey, Base64.DEFAULT),
                    SignUtils.getKey(app,vid, Integer.parseInt(type)).getBytes(),
                    SignUtils.getIV(vid).getBytes()));
            downloadM3U8(playUrlBean.getVideo().getSd().getOne().get(0),jm);
        } catch (Exception e) {
            e.printStackTrace();
//            getMvpView().loadError();
        }

    }

    FileWriter wf;

    public void downloadM3U8(final String path,final String key) {

        Call<String> call = ApiClient.getClient().downloadM3U8(path);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    String str = response.body();
                    try {
                        String m3u8path = getPath(courseWare);
                        File des = new File(m3u8path);
                        if (!des.exists()) {
                            des.mkdirs();
                        }
                        File videoFile = new File(m3u8path + "online.m3u8");
                        wf = new FileWriter(videoFile);
                        wf.write(str);


                    } catch (Exception e) {
                        getMvpView().playError("analysis failed");
                    } finally {
                        if (wf != null) {
                            try {
                                wf.close();
                                int index = path.lastIndexOf("/") + 1;
                                String rootUrl = path.substring(0, index);
                                getKeyTxt(rootUrl, getPath(courseWare) + "online.m3u8",key);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } else {
                    getMvpView().playError("m3u8数据错误");
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                getMvpView().playError("网络错误");
            }
        });
    }

    /**
     * 获取并保存Key到文件中
     *
     * @param urlHead
     */
    public void getKeyTxt(final String urlHead, final String m3u8Path,final String keyText) {

        try {
            File file = new File(getPath(courseWare) + "keyText.txt");
            if (!file.exists()) {
                file.createNewFile();
                FileWriter fileWriter = new FileWriter(file);
                fileWriter.write(keyText);
                fileWriter.flush();
                fileWriter.close();
            }
            String keyUrl = null;
            FileOutputStream fos = null;
            File m3u8File = new File(m3u8Path);
            FileReader fileReader = new FileReader(m3u8File);
            BufferedReader reader11 = new BufferedReader(fileReader);
            String line1 = null;
            StringBuffer stringBuffer1 = new StringBuffer();
            String line_copy = null;
            boolean isNeedKey = false;//是否需要进行加解密
            while ((line1 = reader11.readLine()) != null) {
                line_copy = line1;
                if (line1.contains("EXT-X-KEY"))
                    isNeedKey = true;
                if (line1.contains("EXT-X-KEY") && !line1.contains(Constans.M3U8_KEY_SUB)) {
                    //如果当前视频需要进行解密，则修改m3u8文件中EXT-X-KEY对应的值指向本地key文件，来标示需要进行解密
                    String[] keyPart = line1.split(",");
                    String keyUrll = keyPart[1].split("URI=")[1].trim();
                    keyUrl = keyUrll.substring(1, keyUrll.length() - 1);
                    line_copy = keyPart[0] + Constans.M3U8_KEY_SUB +getKeyPath(courseWare) +"keyText.txt\"," + keyPart[2];
//                    line_copy = keyPart[0] + ",URI=\""+keyText+"\"," + keyPart[2];
                } else if (line1.contains(".ts") && !line1.contains("http:") && isNeedKey) {
                    line_copy = urlHead + line_copy;//补全ts的网络路径
                }
                stringBuffer1.append(line_copy);
                stringBuffer1.append("\r\n");
            }
            reader11.close();
            FileWriter writer = new FileWriter(m3u8File, false);
            BufferedWriter writer2 = new BufferedWriter(writer);
            writer2.write(stringBuffer1.toString());
            writer2.flush();
            writer2.close();
            getMvpView().startPlay(courseWare);
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    getMvpView().startPlay(courseWare);
//                }
//            },2000);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private String getPath(CourseWare courseWare) {
        String userId = SharedPrefHelper.getInstance(getMvpView().context()).getUserId();
        String desPath = FileUtil.getDownloadPath(getMvpView().context());

        return desPath + userId + "_" +"sanning_"+ courseWare.getCwId() + "/";

    }

    private String getKeyPath(CourseWare courseWare) {
        String userId = SharedPrefHelper.getInstance(getMvpView().context()).getUserId();

        return userId + "_" +"sanning_"+ courseWare.getCwId() + "/";

    }
}
