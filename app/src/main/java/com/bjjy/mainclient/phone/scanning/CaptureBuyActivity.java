package com.bjjy.mainclient.phone.scanning;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bjjy.mainclient.phone.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by dell on 2016/10/28.
 */
public class CaptureBuyActivity extends Activity implements View.OnClickListener{

    @Bind(R.id.top_title_left)
    ImageView topTitleLeft;
    @Bind(R.id.top_title_text)
    TextView topTitleText;
    @Bind(R.id.tv_buy)
    TextView tvBuy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.capturebuy_activity);
        ButterKnife.bind(this);
        initView();
    }

    private void initView() {
        topTitleLeft.setVisibility(View.VISIBLE);
        topTitleLeft.setOnClickListener(this);
        topTitleText.setText("辅导书知识点课程");

        tvBuy.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.top_title_left:
                finish();
                break;
            case R.id.tv_buy:
                Toast.makeText(CaptureBuyActivity.this, "toBuy", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
