package com.bjjy.mainclient.phone.view.question;

import com.dongao.mainclient.model.mvp.MvpView;

/**
 * Created by fengzongwei on 2016/5/4 0004.
 */
public interface MyQuestionModifyView extends MvpView{

    void resultAction();//提交后的动作
    String getParamsFromIntent(String key);//从intent中根据具体的key获取value值
    String getQuesTitle();//获取提交问题的title
    String getQuesContent();//获取提交问题的内容
    void showAnimProgress();//显示上传progressDialog
    void dissmissAnimProgress();//隐藏progressDialog
}
