package com.bjjy.mainclient.phone.view.classroom.course;


import com.dongao.mainclient.model.mvp.MvpView;
import com.dongao.mainclient.model.bean.course.CourseJson;

/**
 * UI 定义UI 看此UI中有何事件
 */
public interface CourseFragmentView extends MvpView {
  void setView(CourseJson courseJson);
}
