package com.bjjy.mainclient.phone.view.studybar.privateteacher.bean;

import java.util.List;

/**
 * Created by wyc on 2016/6/25.
 */
public class PrivateTeacherListInfo {
    private List<PrivateTeacher> gdjsNoticeList;//公告ID
    private int totalPages;//总页数

    public List<PrivateTeacher> getGdjsNoticeList() {
        return gdjsNoticeList;
    }

    public void setGdjsNoticeList(List<PrivateTeacher> gdjsNoticeList) {
        this.gdjsNoticeList = gdjsNoticeList;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }
}
