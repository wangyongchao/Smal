package com.bjjy.mainclient.phone.view.studybar.fragment.bean;

import java.util.List;

/**
 * Created by wyc on 2016/6/3.
 */
public class StudyBarData {
    private List<StudyBar> studyBarList;

    public List<StudyBar> getStudyBarList() {
        return studyBarList;
    }

    public void setStudyBarList(List<StudyBar> studyBarList) {
        this.studyBarList = studyBarList;
    }
}
