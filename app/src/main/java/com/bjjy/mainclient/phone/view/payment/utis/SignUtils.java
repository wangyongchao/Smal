package com.bjjy.mainclient.phone.view.payment.utis;

import com.dongao.mainclient.core.util.DateUtil;
import com.dongao.mainclient.core.util.MD5Util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created by dell on 2016/12/5.
 */
public class SignUtils {
    public static String createWxSign(HashMap<String,String> map){
        String date=DateUtil.getCurrentDate();
        String key="sqrewq32zaq432143esfass65weda2dc";
        SortedMap<String,Object> parameters = new TreeMap<String,Object>();
//        parameters.put("date_time",date);
        parameters.putAll(map);
        StringBuffer sb = new StringBuffer();
        Set es = parameters.entrySet();//所有参与传参的参数按照accsii排序（升序）  
        Iterator it = es.iterator();
        while(it.hasNext()) {
            Map.Entry entry = (Map.Entry)it.next();
            String k = (String)entry.getKey();
            Object v = entry.getValue();
            if(null != v && !"".equals(v)
                    && !"sign".equals(k) && !"key".equals(k)) {
                sb.append(k + "=" + v + "&");
            }
        }
        sb.append("key=" + key);//TODO 签名密钥
//        CryptoUtil.md5HexDigest(sb.toString(), null);
        String str= MD5Util.encrypt(sb.toString()).toUpperCase();
        return  str;
    }
}
