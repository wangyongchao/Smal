/*
 *
 *  * Copyright (C) 2015 by  wyc@qq.com
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  * http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *
 */

package com.bjjy.mainclient.phone.dict;

/**
 * 首页类型
 *
 * @author wyc
 */
public enum MainTypeEnum implements BaseEnum {

    //1、每日一练；2、去选课吧;3、已购图书用户绑定;4、终于等到你 5,是听课标记 6,课后作业 7,模拟试卷 8,历年真题

    MAIN_TYPE_DAY {
        @Override
        public int getId() {
            return 1;
        }

        @Override
        public String getName() {
            return "每日一练";
        }
    },
    MAIN_TYPE_SELECT_COURSE {
        @Override
        public int getId() {
            return 2;
        }

        @Override
        public String getName() {
            return "去选课吧";
        }
    },
    MAIN_TYPE_BOOK {
        @Override
        public int getId() {
            return 3;
        }

        @Override
        public String getName() {
            return "已购图书绑定";
        }
    },
    MAIN_TYPE_WAIT {
        @Override
        public int getId() {
            return 4;
        }

        @Override
        public String getName() {
            return "终于等到你";
        }
    },
    MAIN_TYPE_COUTSE {
        @Override
        public int getId() {
            return 5;
        }

        @Override
        public String getName() {
            return "听课";
        }
    },
    MAIN_TYPE_KEHOUZUOYE {
        @Override
        public int getId() {
            return 6;
        }

        @Override
        public String getName() {
            return "课后作业";
        }
    },
    MAIN_TYPE_SUITANGLIANXI {
        @Override
        public int getId() {
            return 5;
        }

        @Override
        public String getName() {
            return "随堂练习";
        }
    },
    MAIN_TYPE_MONISHIJUAN {
        @Override
        public int getId() {
            return 7;
        }

        @Override
        public String getName() {
            return "模拟试卷";
        }
    },
    MAIN_TYPE_LINIANZHENTI {
        @Override
        public int getId() {
            return 8;
        }

        @Override
        public String getName() {
            return "历年真题";
        }
    };


    public static String getValue(int key) {
        String value = "";
        switch (key) {
            case 1:
                value = MainTypeEnum.MAIN_TYPE_DAY.getName();
                break;
            case 2:
                value = MainTypeEnum.MAIN_TYPE_SELECT_COURSE.getName();
                break;
            case 3:
                value = MainTypeEnum.MAIN_TYPE_BOOK.getName();
                break;
            case 4:
                value = MainTypeEnum.MAIN_TYPE_WAIT.getName();
                break;
            case 5:
                value = MainTypeEnum.MAIN_TYPE_COUTSE.getName();
                break;
            case 6:
                value = MainTypeEnum.MAIN_TYPE_KEHOUZUOYE.getName();
                break;
            case 7:
                value = MainTypeEnum.MAIN_TYPE_MONISHIJUAN.getName();
                break;
            case 8:
                value = MainTypeEnum.MAIN_TYPE_LINIANZHENTI.getName();
                break;
            default:
                break;
        }
        return value;
    }

}
