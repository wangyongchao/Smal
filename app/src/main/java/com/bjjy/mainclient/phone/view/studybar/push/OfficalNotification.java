package com.bjjy.mainclient.phone.view.studybar.push;

import android.app.Notification;
import android.content.Context;

import com.bjjy.mainclient.phone.event.PushMsgNotification;
import com.dongao.libs.dongaoumengpushlib.NotifycationHelper;
import com.dongao.libs.dongaoumengpushlib.OfficalUmengMessageHandler;
import com.umeng.message.entity.UMessage;
import org.greenrobot.eventbus.EventBus;

/**
 * Created by wyc on 2016/6/13.
 */
public class OfficalNotification extends OfficalUmengMessageHandler {
    public OfficalNotification(Context applicationContext) {
        super(applicationContext);
    }
    /**
     * 参考集成文档的1.6.4
     * http://dev.umeng.com/push/android/integration#1_6_4
     * */
    @Override
    public Notification getNotification(Context context,
                                        UMessage msg) {
        EventBus.getDefault().post(new PushMsgNotification(false));
        switch (msg.builder_id) {
            case 1:
                Notification mNotification = NotifycationHelper.getDefultNotifycation(context, msg, this);
                return mNotification;
            default:
                //默认为0，若填写的builder_id并不存在，也使用默认。
                return super.getNotification(context, msg);
        }
    }
}
