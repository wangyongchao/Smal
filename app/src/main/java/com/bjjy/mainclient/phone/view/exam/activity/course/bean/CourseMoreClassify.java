package com.bjjy.mainclient.phone.view.exam.activity.course.bean;


import java.util.List;

/**
 * Created by wyc on 2015/12/30.
 */
public class CourseMoreClassify {
    private String courseTypeName;// 课程类别
    private List<CourseMore> courseItems;// 课程列表

    public String getCourseTypeName() {
        return courseTypeName;
    }

    public void setCourseTypeName(String courseTypeName) {
        this.courseTypeName = courseTypeName;
    }

    public List<CourseMore> getCourseItems() {
        return courseItems;
    }

    public void setCourseItems(List<CourseMore> courseItems) {
        this.courseItems = courseItems;
    }
}
