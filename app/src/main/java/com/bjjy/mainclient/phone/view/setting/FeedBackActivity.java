package com.bjjy.mainclient.phone.view.setting;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bjjy.mainclient.persenter.FeedBackPersenter;
import com.bjjy.mainclient.phone.view.setting.views.FeedBackView;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseActivity;
import com.bjjy.mainclient.phone.utils.NetworkUtil;
import com.bjjy.mainclient.phone.utils.StringUtil;
import com.bjjy.mainclient.phone.widget.msg.AppMsg;

import butterknife.Bind;
import butterknife.ButterKnife;


public class FeedBackActivity extends BaseActivity implements FeedBackView {

    @Bind(R.id.top_title_left)
    ImageView topTitleLeft;
    @Bind(R.id.top_title_right)
    ImageView topTitleRight;
    @Bind(R.id.top_title_text)
    TextView topTitleText;
    @Bind(R.id.top_title_bar_layout)
    RelativeLayout topTitleBarLayout;
    @Bind(R.id.content)
    EditText content_edit;
    @Bind(R.id.tv_right)
    TextView tvRight;
    @Bind(R.id.tv_limit)
    TextView tvLimit;
    private TextView textView_post;

    private String content;
    private FeedBackPersenter precenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_feedback);
        ButterKnife.bind(this);
        precenter = new FeedBackPersenter();
        precenter.attachView(this);
        initView();
    }

    @Override
    public void initView() {
        topTitleLeft.setImageResource(R.drawable.nav_left);
        topTitleLeft.setVisibility(View.VISIBLE);
        tvRight.setVisibility(View.VISIBLE);
        topTitleLeft.setOnClickListener(this);
        tvRight.setOnClickListener(this);
        topTitleText.setText("意见反馈");
        content_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(200-s.length()>=0){
                    tvLimit.setText(200-s.length()+"字");
                }
                if(200-s.length()<=0){
                    Toast.makeText(FeedBackActivity.this, "超过限制字数", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void initData() {

        content = content_edit.getText().toString();
        if (StringUtil.isEmpty(content)) {
            showAppMsg("反馈内容不能为空");
        } else {
            precenter.getData();
        }

        //进度条
//        content_edit.setEnabled(false);
    }

    public void showAppMsg(String msg) {
        final AppMsg.Style style;
        style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.info);
        AppMsg appMsg = AppMsg.makeText(this, msg, style);
        appMsg.setLayoutGravity(Gravity.TOP);
        int messageHeight = findViewById(R.id.top_title_bar_layout).getMeasuredHeight();
        appMsg.show(messageHeight);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.top_title_left:
                onBackPressed();
                break;
            case R.id.tv_right:
                if (NetworkUtil.isNetworkAvailable(this)) {
                    initData();
                } else {
                    Toast.makeText(FeedBackActivity.this, "请检查网络……", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        this.finish();
    }

    @Override
    public void setData() {
        Toast.makeText(this, "反馈成功!", Toast.LENGTH_SHORT).show();
        onBackPressed();
    }

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public void showLoading() {
        progress.show();
    }

    @Override
    public void hideLoading() {
        progress.dismiss();
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {
        hideLoading();
        Toast.makeText(FeedBackActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public Context context() {
        return this;
    }
}
