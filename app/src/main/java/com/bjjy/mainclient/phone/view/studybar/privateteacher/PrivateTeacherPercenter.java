package com.bjjy.mainclient.phone.view.studybar.privateteacher;

import android.content.Intent;
import com.alibaba.fastjson.JSON;
import com.bjjy.mainclient.phone.view.setting.WebViewActivity;
import com.bjjy.mainclient.phone.view.studybar.Constant;
import com.bjjy.mainclient.phone.view.studybar.privateteacher.bean.PrivateTeacher;
import com.bjjy.mainclient.phone.view.studybar.privateteacher.bean.PrivateTeacherListInfo;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.core.util.NetUtils;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ParamsUtils;
import com.dongao.mainclient.model.remote.bean.BaseBean;
import com.bjjy.mainclient.phone.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by wyc on 2016/5/19.
 */
public class PrivateTeacherPercenter extends BasePersenter<PrivateTeacherView> {

    public List<PrivateTeacher> privateTeacherList;
    private String userId;
    public int currentPage = 1;
    public int totalPage = 0;
    private String type;
    private String classId;
    private String title;

    public void initData() {
        userId = SharedPrefHelper.getInstance(getMvpView().context()).getUserId() + "";
        privateTeacherList = new ArrayList<>();
        Intent intent=getMvpView().getTheIntent();
        type=intent.getStringExtra("typeId");
        classId=intent.getStringExtra("classId");
        title=intent.getStringExtra(Constants.APP_WEBVIEW_TITLE);
        getMvpView().showTopTitle(title);
        getData();
    }

    @Override
    public void getData() {
        currentPage = 1;
        getMvpView().showLoading();
        if (NetUtils.checkNet(getMvpView().context()).isAvailable()) {
            getInterData();
        } else {
            getMvpView().showContentView(Constant.VIEW_TYPE_1);
        }
    }

    public void getLoadData() {
        if (getMvpView().isRefreshNow()) {
            getMvpView().initAdapter();
            getMvpView().showContentView(Constant.VIEW_TYPE_0);
            getMvpView().showError(getMvpView().context().getResources().getString(R.string.check_net));
            return;
        }
        getMvpView().showContentView(Constant.VIEW_TYPE_0);
        if (NetUtils.checkNet(getMvpView().context()).isAvailable()) {
            if (currentPage>totalPage){
                getMvpView().setNoDataMoreShow(true);
                return;
            }
            loadMore(ParamsUtils.getInstance(getMvpView().context()).getPrivateTeacher(type, classId, currentPage));
        } else {
            getMvpView().showContentView(Constant.VIEW_TYPE_0);
            getMvpView().showError(getMvpView().context().getResources().getString(R.string.check_net));
        }
    }


    private void getInterData() {
       
        apiModel.getData(ApiClient.getClient().getPrivateClass(ParamsUtils.getInstance(getMvpView().context()).getPrivateTeacher(type, classId, currentPage)));
    }

    private void loadMore(HashMap<String, String> params) {
        Call<String> call = ApiClient.getClient().getPrivateClass(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    String str = response.body();
                    try {
                        getMvpView().hideLoading();
                        BaseBean baseBean = JSON.parseObject(str, BaseBean.class);
                        if (baseBean == null) {
                            return;
                        } else {
                            if (baseBean.getResult().getCode() != 1000) {
                                if (baseBean.getResult().getCode() == 9) {
                                } else {
//                                    getMvpView().showContentView(Constant.VIEW_TYPE_1);
                                }
                                return;
                            }
                        }
                        getMvpView().showContentView(Constant.VIEW_TYPE_0);
                        currentPage++;
                        String body = baseBean.getBody();
                        PrivateTeacherListInfo privateTeacherListInfo = JSON.parseObject(body, PrivateTeacherListInfo.class);
                        List<PrivateTeacher> newList = privateTeacherListInfo.getGdjsNoticeList();
                        if (newList == null) {
                            newList = new ArrayList<>();
                        }
                        totalPage = privateTeacherListInfo.getTotalPages();
                        privateTeacherList.addAll(newList);
                        getMvpView().initAdapter();
                    } catch (Exception e) {
                        getMvpView().showContentView(Constant.VIEW_TYPE_1);
                    }

                } else {
                    getMvpView().showContentView(Constant.VIEW_TYPE_1);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }

    @Override
    public void setData(String obj) {
        try{
            BaseBean baseBean = JSON.parseObject(obj, BaseBean.class);
            if (baseBean == null) {
                getMvpView().showContentView(Constant.VIEW_TYPE_1);
                return;
            } else {
                int result = baseBean.getResult().getCode();
                if (result != 1000) {
                    getMvpView().showContentView(Constant.VIEW_TYPE_3);
                    return;
                }
            }
            currentPage++;
            String body = baseBean.getBody();
            //设置返回的数据
            PrivateTeacherListInfo privateTeacherListInfo = JSON.parseObject(body, PrivateTeacherListInfo.class);
            privateTeacherList =privateTeacherListInfo.getGdjsNoticeList();
            if (privateTeacherList == null ||privateTeacherList.size()==0) {
                getMvpView().showContentView(Constant.VIEW_TYPE_2);
            }else {
                getMvpView().showContentView(Constant.VIEW_TYPE_0);
            }
            totalPage=privateTeacherListInfo.getTotalPages();
            getMvpView().initAdapter();
        }catch (Exception e){
            getMvpView().showContentView(Constant.VIEW_TYPE_3);
        }
    }

    public void setOnItemClick(int position) {
        Intent intent=new Intent(getMvpView().context(),WebViewActivity.class);
        String url=privateTeacherList.get(position).getmUrl()+"?id="+privateTeacherList.get(position).getNoticeId();
//        url="http://p.m.test.com/appInfo/appLetter.html?userId=23684720";
        Intent intent1=getMvpView().getTheIntent();
        String  title=intent1.getStringExtra(Constants.APP_WEBVIEW_TITLE);
//        intent.putExtra(Constants.APP_WEBVIEW_TITLE,privateTeacherList.get(position).getTitle());
        intent.putExtra(Constants.APP_WEBVIEW_TITLE,title);
        intent.putExtra(Constants.APP_WEBVIEW_URL, url);
        getMvpView().context().startActivity(intent);
    }

    @Override
    public void onError(Exception e) {
        super.onError(e);
        getMvpView().showContentView(Constant.VIEW_TYPE_1);
    }
}
