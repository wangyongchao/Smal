package com.bjjy.mainclient.phone.view.exam.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.bjjy.mainclient.phone.view.exam.bean.OldAnswerLog;
import com.dongao.mainclient.model.local.BaseDao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 答题记录同步对应的成绩单
 * @author lizhangfeng
 *
 */
public class PaperCardDao2 extends BaseDao {
	private static final String EXIST_SQL = "select * from paperhistory where userid=? and subjectid=? and examid=?";
	private static final String SELECT_INDEX_SQL = "select writenum,totaltime from paperhistory where userid=? and subjectid=? and examid=? ";
	private static final String SELECT_REPORT_SQL = "select * from paperhistory where userid=? and subjectid=? and examid=?";
	private static final String ISSUBMMITED_SQL = "select * from paperhistory where userid=? and subjectid=? and examid=? and issaved=1";
	private static final String SELECT_REPORTED_EXAMPAPER_SQL = "select * from exampaper,(select * from paperhistory ) temp where" +
			" exampaper.userid=temp.userid and exampaper.subjectid=temp.subjectid and exampaper.examid=temp.examid";

	public PaperCardDao2(Context context) {
		super(context);
	}
	public boolean exist(OldAnswerLog paper){
		SQLiteDatabase db = getWritableDB();
		Cursor cursor = db.rawQuery(EXIST_SQL, new String[]{String.valueOf(paper.getUserId()), String.valueOf(paper.getSubjectId()),
				   String.valueOf(paper.getUid())});
		if(cursor.moveToNext()){
			return true;
		}
		cursor.close();
		return false;
		
	}
	public Map<String,String> getLastQUestionIndex(OldAnswerLog paper){
		SQLiteDatabase db = getReadableDB();
		 Map<String,String> map=new HashMap<String, String>();
		int index=-1;
		Cursor cursor = db.rawQuery(SELECT_INDEX_SQL, new String[]{String.valueOf(paper.getUserId()), String.valueOf(paper.getSubjectId()), String.valueOf(paper.getUid())} );
		if(cursor.moveToNext()){
			map.put("writenum", String.valueOf(cursor.getInt(cursor.getColumnIndex("writenum"))));
			map.put("totaltime", String.valueOf(cursor.getLong(cursor.getColumnIndex("totaltime"))));
			
		}
		cursor.close();
		return map;
		
	}
	/**
	 * 获取成绩单
	 * @param paper
	 * @return
	 */
	public Map<String,String> getReport(OldAnswerLog paper){
		 Map<String,String> map=new HashMap<String, String>();
		SQLiteDatabase db = getReadableDB();
		Cursor cursor = db.rawQuery(SELECT_REPORT_SQL,  new String[]{String.valueOf(paper.getUserId()), String.valueOf(paper.getSubjectId()), String.valueOf(paper.getUid())});
		while(cursor.moveToNext()){
			map.put("userid", String.valueOf(cursor.getInt(cursor.getColumnIndex("userid"))));
			map.put("subjectid", String.valueOf(cursor.getInt(cursor.getColumnIndex("subjectid"))));
			map.put("examid", String.valueOf(cursor.getInt(cursor.getColumnIndex("examid"))));
			map.put("examtype", String.valueOf(cursor.getInt(cursor.getColumnIndex("examtype"))));
			map.put("updatetime", String.valueOf(cursor.getLong(cursor.getColumnIndex("updaterecordtime"))));
			map.put("totalpoints",  String.valueOf(cursor.getInt(cursor.getColumnIndex("totalpoints"))));//得分
			map.put("issaved", String.valueOf(cursor.getInt(cursor.getColumnIndex("issaved"))));//是否交卷 1交卷2未交卷
			map.put("correct",cursor.getString(cursor.getColumnIndex("correct")));//正确率
			map.put("errornum", cursor.getString(cursor.getColumnIndex("errornum")));//错误数
			map.put("rightnum", cursor.getString(cursor.getColumnIndex("rightnum")));//正确数
			map.put("totaltime", String.valueOf(cursor.getLong(cursor.getColumnIndex("totaltime"))));//所用时间
			map.put("qustionscores", String.valueOf(cursor.getInt(cursor.getColumnIndex("qustionscores"))));//客观题总分
		}
		cursor.close();
		return map;
		
	}
	
	public long insertDoPaperTime(long time){
		SQLiteDatabase db = getWritableDB();
		
		ContentValues values=new ContentValues();
//		values.put(key, value)
		db.insert("paperhistory", null, values);
		return 0;
		
	}
	/**
	 * 获取生成答题记录的试卷列表
	 * @return
	 */
	public List<OldAnswerLog> getReportedExamPaper(){
		SQLiteDatabase db = getReadableDB();
		List<OldAnswerLog> OldAnswerLogs=new ArrayList<OldAnswerLog>();
//		Cursor cursor = db.query("paperhistory", null, null, null, null, null,null);
		Cursor cursor = db.rawQuery(SELECT_REPORTED_EXAMPAPER_SQL,null);

		int i=0;//设置只能取前五百条数据
		while(cursor.moveToNext()){
			if (i>500){
				break;
			}
			OldAnswerLog oldAnswerLog=new OldAnswerLog();
			oldAnswerLog.setUid(cursor.getInt(cursor.getColumnIndex("examid")));
			oldAnswerLog.setUserId(cursor.getInt(cursor.getColumnIndex("userid")));
			oldAnswerLog.setSubjectId(cursor.getInt(cursor.getColumnIndex("subjectid")));
			oldAnswerLog.setExamType(cursor.getInt(cursor.getColumnIndex("examtype")));
			oldAnswerLog.setSubject(cursor.getString(cursor.getColumnIndex("subject")));
			oldAnswerLog.setDescription(cursor.getString(cursor.getColumnIndex("description")));
			oldAnswerLog.setExamTime(cursor.getLong(cursor.getColumnIndex("examtime")));
			oldAnswerLog.setLimitTime(cursor.getLong(cursor.getColumnIndex("limittime")));
			oldAnswerLog.setCheckStatue(cursor.getInt(cursor.getColumnIndex("checkstatue")));
			oldAnswerLog.setExaminationHistoryId(cursor.getInt(cursor.getColumnIndex("examinationhistoryid")));
			int saved = cursor.getInt(cursor.getColumnIndex("saved"));
			int issaved = cursor.getInt(cursor.getColumnIndex("issaved"));
			oldAnswerLog.setSaved(saved == 1);
			oldAnswerLog.setUpdateTime(cursor.getLong(cursor.getColumnIndex("updaterecordtime")));
			oldAnswerLog.setReaded(cursor.getInt(cursor.getColumnIndex("readed")) == 1);
			oldAnswerLog.setDownloaded(cursor.getInt(cursor.getColumnIndex("downloaded")) == 1);
			oldAnswerLog.setSubmmited(cursor.getInt(cursor.getColumnIndex("submmited"))==1);
			//新添加的
			if (cursor.getColumnIndex("rightnum")>0){
				oldAnswerLog.setRightnum(cursor.getInt(cursor.getColumnIndex("rightnum")));
			}else{
				oldAnswerLog.setRightnum(0);
			}
			if (cursor.getColumnIndex("errornum")>0){
				oldAnswerLog.setErrornum(cursor.getInt(cursor.getColumnIndex("errornum")));
			}else{
				oldAnswerLog.setErrornum(0);
			}
			if(issaved==1){
				OldAnswerLogs.add(oldAnswerLog);
				i++;
			}
		}
		cursor.close();
		return OldAnswerLogs ;
		
		
	}
	/**
	 * 判断试卷是否提交
	 * @param paper
	 * @return
	 */
	public boolean isSubmmited(OldAnswerLog paper){
		SQLiteDatabase db = getReadableDB();
		Cursor cursor = db.rawQuery(ISSUBMMITED_SQL, new String[]{String.valueOf(paper.getUserId()), String.valueOf(paper.getSubjectId()), String.valueOf(paper.getUid())});
		if(cursor.moveToNext()){
			return true;
		}
		cursor.close();
		return false;
		
	}
}
