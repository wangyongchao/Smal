package com.bjjy.mainclient.phone.view.question;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bjjy.mainclient.persenter.QuestionExamRecommPersenter;
import com.bjjy.mainclient.phone.view.question.adapter.QuestionListAdapter;
import com.dongao.mainclient.model.bean.question.QuestionRecomm;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseFragmentActivity;
import com.bjjy.mainclient.phone.widget.EmptyViewLayout;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by fengzongwei on 2016/12/13 0013.
 */
public class ExamRecommQuestionNewActivity extends BaseFragmentActivity implements ExamRecommView{
    @Bind(R.id.top_title_left)
    ImageView imageView_back;
    @Bind(R.id.top_title_text)
    TextView textView_title;
    @Bind(R.id.question_exam_recomm_lv)
    ListView listView;
    @Bind(R.id.question_recomm_ask_img)
    ImageView imageView_ask;

    private QuestionListAdapter questionListAdapter;

    private int page = 0,pageSize = 1000;

    private QuestionExamRecommPersenter questionExamRecommPersenter;

    private EmptyViewLayout emptyViewLayout;

    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question_exam_recomm);
        ButterKnife.bind(this);
        initView();
        initData();
    }

    @Override
    public void initView() {
        textView_title.setText("推荐答疑");
        imageView_back.setImageResource(R.drawable.back);
        imageView_back.setVisibility(View.VISIBLE);

        emptyViewLayout = new EmptyViewLayout(this,listView);
        emptyViewLayout.setEmptyButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                questionExamRecommPersenter.getData();
            }
        });
        emptyViewLayout.setErrorButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                questionExamRecommPersenter.getData();
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(ExamRecommQuestionNewActivity.this, QuestionDetailActivity.class);
                intent.putExtra("questionAnswerId", questionExamRecommPersenter.questionRecomms.get(position).getQuestionId() + "");
                startActivity(intent);
            }
        });

        imageView_back.setOnClickListener(this);
        imageView_ask.setOnClickListener(this);
    }

    @Override
    public void initData() {
        bundle = getIntent().getExtras();
        questionExamRecommPersenter = new QuestionExamRecommPersenter();
        questionExamRecommPersenter.attachView(this);
        questionExamRecommPersenter.getData();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.top_title_left:
                finish();
                break;
            case R.id.question_recomm_ask_img:
                Intent intent = new Intent(this,AddQuestionActiivtyNew.class);
                bundle.putString("examquestionId",examQuestionId());
                intent.putExtras(bundle);
                intent.putExtra("isExamQuestionAsk",true);
                startActivity(intent);
                break;
        }
    }

    @Override
    public String examQuestionId() {
        return getIntent().getStringExtra("examinationQuestionId");
    }

    @Override
    public void showLoading() {
        emptyViewLayout.showLoading();
    }

    @Override
    public void hideLoading() {
        emptyViewLayout.showLoading();
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {
        emptyViewLayout.showError();
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }

    @Override
    public Context context() {
        return this;
    }

    @Override
    public void showRecommQuesList(List<QuestionRecomm> questionRecomms) {
        emptyViewLayout.showContentView();
        questionListAdapter = new QuestionListAdapter(this,questionRecomms);
        listView.setAdapter(questionListAdapter);
    }

    @Override
    public void showNoData() {
        emptyViewLayout.showEmpty();
    }

    @Override
    public void showNetError() {
        emptyViewLayout.showNetErrorView();
    }
}
