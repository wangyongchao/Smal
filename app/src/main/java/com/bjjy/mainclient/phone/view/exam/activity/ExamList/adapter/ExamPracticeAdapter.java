package com.bjjy.mainclient.phone.view.exam.activity.ExamList.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.view.exam.activity.exampaperlist.bean.Course;
import com.bjjy.mainclient.phone.view.exam.activity.exampaperlist.bean.MyCourse;
import com.bjjy.mainclient.phone.widget.pulltorefresh.view.CustomExpandableListView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExamPracticeAdapter extends BaseExpandableListAdapter implements CustomExpandableListView.HeaderAdapter {

	private Context mContext;

	private List<MyCourse> myCourses;
	private List<Course> courses;
	private CustomExpandableListView listView;
	private LayoutInflater mLayoutInflater;

	private HashMap<Integer,Integer> groupStatusMap = new HashMap<Integer, Integer>();
	private boolean isExpanded;

	private final int LIST_GROUP_HEIGHT = 40;
	private int selectGourpIndex = -1;
	private int selectChildIndex = -1;
	private MyclassLearnListener myclassLearnListener;
	public ExamPracticeAdapter(Context context, List<MyCourse> myCourses, boolean isExpanded){
		this.setMyCourses(myCourses);
		this.mContext = context;
		this.isExpanded = isExpanded;
		this.mLayoutInflater =  LayoutInflater.from(context);
	}

	public ExamPracticeAdapter(Context context, List<MyCourse> myCourses, boolean isExpanded, MyclassLearnListener myclassLearnListener){
		this.setMyCourses(myCourses);
		this.mContext = context;
		this.isExpanded = isExpanded;
		this.mLayoutInflater =  LayoutInflater.from(context);
		this.myclassLearnListener=myclassLearnListener;
	}

	public void setMyCourses(List<MyCourse> myCourses) {
		long mbeginTime = System.currentTimeMillis();
		if(myCourses == null)
			return;
		this.myCourses = myCourses;
		this.courses = new ArrayList<Course>();
		for (MyCourse exam : myCourses) {
			List<Course> ss = exam.getCourseList();
			if(ss != null){
				for (Course subject : ss) {
					courses.add(subject);
				}
			}

		}

		this.notifyDataSetChanged();
	}

	public CustomExpandableListView getListView() {
		return listView;
	}



	public void setListView(CustomExpandableListView listView) {
		this.listView = listView;
	}



	public int getSelectChildIndex() {
		return selectChildIndex;
	}



	public void setSelectChildIndex(int selectChildIndex) {
		this.selectChildIndex = selectChildIndex;
	}




	public int getSelectGourpIndex() {
		return selectGourpIndex;
	}



	public void setSelectGourpIndex(int selectGourpIndex) {
		this.selectGourpIndex = selectGourpIndex;
	}



	@Override
	public boolean areAllItemsEnabled() {
		return false;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return childPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {

		return childPosition;
	}

	@Override
	public View getChildView(final int groupPosition, final int childPosition,boolean isLastChild, View convertView, ViewGroup parent) {
		ChildViewHolder holder = null;

		if(convertView == null) {
			holder = new ChildViewHolder();
			convertView = mLayoutInflater.inflate(R.layout.exampractice_list_child, null);
			holder.tv_title = (TextView) convertView.findViewById(R.id.tv_title);
			holder.iv_check = (ImageView) convertView.findViewById(R.id.iv_check);
			holder.iv_picture = (ImageView) convertView.findViewById(R.id.iv_picture);
			holder.tv_name = (TextView) convertView.findViewById(R.id.tv_name);
			holder.tv_time = (TextView) convertView.findViewById(R.id.tv_time);
			holder.tv_learned = (TextView) convertView.findViewById(R.id.tv_learned);
			holder.tv_do=(TextView) convertView.findViewById(R.id.tv_do);
			holder.ll_do=(RelativeLayout) convertView.findViewById(R.id.ll_do);
			holder.iv_do=(ImageView) convertView.findViewById(R.id.iv_do);
			holder.tv_danwei=(TextView) convertView.findViewById(R.id.tv_danwei);
			convertView.setTag(holder);
		}else {
			holder = (ChildViewHolder) convertView.getTag();
		}
		
//		ImageLoader.getInstance().displayImage(myCourses.get(groupPosition).getCourseList().get(childPosition).getCourseImg(), holder.iv_picture);
		holder.tv_title.setText(myCourses.get(groupPosition).getCourseList().get(childPosition).getCwName());
		holder.tv_name.setText(myCourses.get(groupPosition).getCourseList().get(childPosition).getCourseTeacher());
		holder.tv_time.setText(myCourses.get(groupPosition).getCourseList().get(childPosition).getCourseCredit());
		holder.tv_learned.setText(myCourses.get(groupPosition).getCourseList().get(childPosition).getCourseCredit() );
//		if (myCourses.get(groupPosition).getCourseList().get(childPosition).getCourseState().equals(Constants.COURSE_STATE_1)){
//			holder.tv_do.setText(mContext.getResources().getString(R.string.mycourse_start_learn));
//			holder.iv_do.setBackgroundResource(R.drawable.myclass_play_start);
//		}else if (myCourses.get(groupPosition).getCourseList().get(childPosition).getCourseState().equals(Constants.COURSE_STATE_2)){
//			holder.tv_do.setText(mContext.getResources().getString(R.string.mycourse_continue_learn));
//			holder.iv_do.setBackgroundResource(R.drawable.myclass_play_medium);
//		}else if (myCourses.get(groupPosition).getCourseList().get(childPosition).getCourseState().equals(Constants.COURSE_STATE_3)){
//			holder.tv_do.setText(mContext.getResources().getString(R.string.mycourse_end_learn));
//			holder.iv_do.setBackgroundResource(R.drawable.mycourse_play_end);
//		}

//		if (SharedPrefHelper.getInstance().getCreditType().equals(Constants.CREDIT_TYPE_1)) {
//			holder.tv_danwei.setText(mContext.getResources().getString(R.string.credit_type_1));
//		} else if (SharedPrefHelper.getInstance().getCreditType().equals(Constants.CREDIT_TYPE_2)) {
//			holder.tv_danwei.setText(mContext.getResources().getString(R.string.credit_type_2));
//		} else if (SharedPrefHelper.getInstance().getCreditType().equals(Constants.CREDIT_TYPE_3)) {
//			holder.tv_danwei.setText(mContext.getResources().getString(R.string.credit_type_3));
//		}

		holder.ll_do.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				myclassLearnListener.myClassLearnListner(childPosition,groupPosition);
			}
		});
		return convertView;
	}

	/**
	 * 子tittle的点击事件
	 */
	public interface   MyclassLearnListener{
		public void myClassLearnListner(int childPosition, int fuPosition);
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		if (myCourses == null|| myCourses.isEmpty()) {
			return 0;
		}
		List<Course> courses = myCourses.get(groupPosition).getCourseList();
		if(courses!= null && !courses.isEmpty()) {
			return courses.size();
		}
		return 0;
	}

	@Override
	public long getCombinedChildId(long groupId, long childId) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public long getCombinedGroupId(long groupId) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		return courses.get(groupPosition);
	}

	@Override
	public int getGroupCount() {

		if(this.myCourses != null &&!this.myCourses.isEmpty()) {
			return this.myCourses.size();
		}
		return 0;
	}

	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,View convertView, ViewGroup parent) {

		ViewHolder holder = null;

		if(convertView == null) {
			holder = new ViewHolder();
			convertView = mLayoutInflater.inflate(R.layout.exampractice_list_group, null);
			holder.tv_name = (TextView) convertView.findViewById(R.id.tv_name);
			convertView.setTag(holder);
		}else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.tv_name.setText(myCourses.get(groupPosition).getCourseTypeName());
		return convertView;
	}



	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return true;
	}


	public List<MyCourse> getMyCourses() {
		return myCourses;
	}


	static class ViewHolder{
		TextView tv_name;
		//ImageView ivGroup;
	}
	static class ChildViewHolder{
		RelativeLayout ll_do;
		TextView tv_title,tv_name,tv_time,tv_learned,tv_do,tv_danwei;
		ImageView iv_check,iv_picture,iv_do;
	}


	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}



	@Override
	public void onGroupCollapsed(int groupPosition) {
		// TODO Auto-generated method stub
		super.onGroupCollapsed(groupPosition);
	}


	@Override
	public void onGroupExpanded(int groupPosition) {
		this.selectGourpIndex = groupPosition;
		super.onGroupExpanded(groupPosition);
	}





	@Override
	public int getHeaderState(int groupPosition, int childPosition) {
		final int childCount = getChildrenCount(groupPosition);
		if(childPosition == childCount - 1){
			return PINNED_HEADER_PUSHED_UP;
		}
		else if(childPosition == -1 && !listView.isGroupExpanded(groupPosition)){
			return PINNED_HEADER_GONE;
		}
		else{
			return PINNED_HEADER_VISIBLE;
		}
	}



	@Override
	public void configureHeader(View header, int groupPosition,int childPosition, int alpha) {
		if (courses != null) {
			Course subject = this.courses.get(groupPosition);
			if (subject != null) {
				((TextView)header.findViewById(R.id.tv_name)).setText(subject.getCwName());
			}
		}
	}


	@Override
	public void setGroupClickStatus(int groupPosition, int status) {
		groupStatusMap.put(groupPosition, status);
	}



	@Override
	public int getGroupClickStatus(int groupPosition) {
		if(groupStatusMap.containsKey(groupPosition)){
			return groupStatusMap.get(groupPosition);
		}else{
			if(isExpanded){
				return 1;
			}else{
				return 0;
			}
		}
	}



	public void clear() {
//		courseBean.clear();
//	notifyDataSetChanged();

	}




}
