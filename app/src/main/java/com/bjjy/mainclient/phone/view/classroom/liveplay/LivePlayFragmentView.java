package com.bjjy.mainclient.phone.view.classroom.liveplay;


import com.dongao.mainclient.model.bean.course.CoursePlayBean;
import com.dongao.mainclient.model.mvp.MvpView;

import java.util.List;

/**
 * UI 定义UI 看此UI中有何事件
 */
public interface LivePlayFragmentView extends MvpView {
  void setData(List<CoursePlayBean> coursePlayBean);
  String getSubjectId();
}
