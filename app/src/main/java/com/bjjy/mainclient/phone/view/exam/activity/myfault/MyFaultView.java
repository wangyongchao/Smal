package com.bjjy.mainclient.phone.view.exam.activity.myfault;



import android.content.Intent;

import com.dongao.mainclient.model.mvp.MvpView;

/**
 * Created by wyc on 2016/5/10.
 */
public interface MyFaultView extends MvpView {
    void initAdapter();
    Intent getTheIntent();
}
