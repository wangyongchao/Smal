package com.bjjy.mainclient.phone.view.download.local;

import android.content.Intent;

import com.alibaba.fastjson.JSON;
import com.bjjy.mainclient.phone.download.db.DownloadDB;
import com.bjjy.mainclient.phone.view.download.local.bean.YearInfo;
import com.bjjy.mainclient.phone.view.exam.activity.exampaperlist.bean.MyCourse;
import com.bjjy.mainclient.phone.view.play.PlayActivity;
import com.bjjy.mainclient.phone.view.play.domain.CourseWare;
import com.bjjy.mainclient.phone.view.studybar.Constant;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.mvp.BasePersenter;

import java.util.ArrayList;

/**
 * Created by wyc on 2016/5/19.
 */
public class DownloadLocalPercenter extends BasePersenter<DownloadLocalView> {
    public ArrayList<CourseWare> courseList;
    private ArrayList<YearInfo> yearList;
    private ArrayList<MyCourse> list;

    private String courseId;
    private String userId;
    private YearInfo currYear;
    private DownloadDB downloadDB;

    public void initData() {
        Intent intent=getMvpView().getTheIntent();
        courseId=intent.getStringExtra("courseId");
        courseString=intent.getStringExtra("course");
        courseList=new ArrayList<>();
        userId= SharedPrefHelper.getInstance(getMvpView().context()).getUserId();
        list=new ArrayList<>();
        yearList=new ArrayList<>();
        currYear=new YearInfo();
        downloadDB=new DownloadDB(getMvpView().context());
//        courseWareDB=new CourseWareDB(this);
        String yearNow=SharedPrefHelper.getInstance(getMvpView().context()).getCurYear();
        if(!yearNow.isEmpty()){
            currYear= JSON.parseObject(yearNow, YearInfo.class);
        }
        getData();
    }

    @Override
    public void getData() {
        getCourseWareList();
        getMvpView().initAdapter();
    }

    /**
     * TODO
     * 获取数据库数据并解析其中的json字符串
     */
    private void getCourseWareList(){
//        List<CourseWare> downloadCourseList=  downloadDB.findCourseWares(courseId,userId,currYear.getYearName());
//
//        List<CourseWare> newCourseList=  courseWareDB.queryByYearAndCourseId(userId, courseId, currYear.getYearName());
//        if (courseList==null){
//            courseList=new ArrayList<>();
//        }
//        courseList.clear();
//        if (downloadCourseList!=null&&newCourseList!=null){
//            for (int i = 0; i < downloadCourseList.size(); i++) {
//                for (int j = 0; j < newCourseList.size(); j++) {
//                    if (downloadCourseList.get(i).getVideoID().equals(newCourseList.get(j).getVideoID())){
//                        courseList.add(newCourseList.get(j)); 
//                    }
//                }
//            }
//        }
    }

    private String courseString;



    private void getInterData() {
    }


    @Override
    public void setData(String obj) {
    }

    public void setOnItemClick(int position) {
        Intent intent = new Intent(getMvpView().context(), PlayActivity.class);
        intent.putExtra("course", courseString);
        intent.putExtra("courseWareId", courseList.get(position).getVideoID());
        getMvpView().context().startActivity(intent);
    }

    @Override
    public void onError(Exception e) {
        super.onError(e);
        getMvpView().showContentView(Constant.VIEW_TYPE_1);
    }

    public void deleteCourse(int position) {
        deleteDBDta(courseList.get(position));
        courseList.remove(position);
       getMvpView().initAdapter();
        if (courseList.size()==0){
            getMvpView().finishActivity();
        }
    }
    /**
     * 删除数据库中的数据
     */
    private void deleteDBDta(CourseWare courseWare) {
//        downloadDB.deleteCourseWare(userId,courseWare);
    }

    public void remove(int position) {
        courseList.remove(position);
        getMvpView().initAdapter();
    }
}
