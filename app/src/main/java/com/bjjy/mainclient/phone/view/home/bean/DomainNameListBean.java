package com.bjjy.mainclient.phone.view.home.bean;

import java.util.List;

/**
 * Created by dell on 2016/8/4.
 */
public class DomainNameListBean {
    private List<String> downloadList;
    private List<String> videoList;

    public List<String> getDownloadList() {
        return downloadList;
    }

    public void setDownloadList(List<String> downloadList) {
        this.downloadList = downloadList;
    }

    public List<String> getVideoList() {
        return videoList;
    }

    public void setVideoList(List<String> videoList) {
        this.videoList = videoList;
    }
}
