package com.bjjy.mainclient.phone.view.studybar.fragment.bean;


import com.yunqing.core.db.annotations.Id;
import com.yunqing.core.db.annotations.Table;

/**
 * Created by wyc on 2016/6/3.
 */

@Table(name = "t_studybar_fragment_log")
public class StudyBar {
    @Id
    private int dbId;
    private int type; //类型、1：高端私教;2:消息中心；3：答疑提醒
    private String title;//标题、例如：高端私教 ； 消息。。。
    private String messageTitle; //小标题
    private int messageCount;//总数
    private String classId;//班级ID
    private String updateTime; //最新更新时间
    private String url;//站内信的url地址
    private String newsType;//专指私教班的类型
    //数据库中需要的数据
    private String userId;
    private boolean isRead = true;//是否已读
    public int getDbId() {
        return dbId;
    }

    public void setDbId(int dbId) {
        this.dbId = dbId;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public int getMessageCount() {
        return messageCount;
    }

    public void setMessageCount(int messageCount) {
        this.messageCount = messageCount;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean isRead() {
        return isRead;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setIsRead(boolean isRead) {
        this.isRead = isRead;
    }

    public String getMessageTitle() {
        return messageTitle;
    }

    public void setMessageTitle(String messageTitle) {
        this.messageTitle = messageTitle;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNewsType() {
        return newsType;
    }

    public void setNewsType(String newsType) {
        this.newsType = newsType;
    }
    
}
