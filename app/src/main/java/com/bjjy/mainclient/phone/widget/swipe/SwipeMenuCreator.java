package com.bjjy.mainclient.phone.widget.swipe;


/**
 * 
 * @author wyc
 *
 */
public interface SwipeMenuCreator {

	void create(SwipeMenu menu);
}
