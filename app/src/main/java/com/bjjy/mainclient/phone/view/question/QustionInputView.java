package com.bjjy.mainclient.phone.view.question;

import com.dongao.mainclient.model.mvp.MvpView;

/**
 * Created by fengzongwei on 2016/12/5 0005.
 */
public interface QustionInputView extends MvpView {
    String getQuestionId();//答疑主干id
    String getQaInfoId();//答疑提问id
    String title();//标题
    String content();//内容
    void modifySuccess();//修改成功
    void zhuiSuccess();//追问成功
}
