package com.bjjy.mainclient.phone.view.classroom.liveplay;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.event.CourseTabTypeEvent;
import com.bjjy.mainclient.phone.event.FromPlayEvent;
import com.bjjy.mainclient.phone.event.UpdateEvent;
import com.bjjy.mainclient.phone.utils.NetworkUtil;
import com.bjjy.mainclient.phone.view.classroom.course.HeaderViewPagerFragment;
import com.bjjy.mainclient.phone.view.classroom.courseplay.CoursePlayExpandAdapter;
import com.bjjy.mainclient.phone.view.play.PlayActivity;
import com.bjjy.mainclient.phone.view.play.domain.CoursePlayDB;
import com.dongao.mainclient.model.bean.play.CwStudyLog;
import com.bjjy.mainclient.phone.view.play.domain.CwStudyLogDB;
import com.bjjy.mainclient.phone.widget.EmptyViewLayout;
import com.dongao.libs.dongaoumengpushlib.constants.PushConstants;
import com.dongao.mainclient.model.bean.course.CoursePlay;
import com.dongao.mainclient.model.bean.course.CoursePlayBean;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.remote.bean.BaseBean;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.umeng.analytics.MobclickAgent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by dell on 2016/5/16.
 */
public class LivePlayFragment extends HeaderViewPagerFragment implements LivePlayFragmentView, ExpandableListView.OnChildClickListener,
        ExpandableListView.OnGroupClickListener {

    @Bind(R.id.course_fragment_exam_rl)
    RelativeLayout courseFragmentExamRl;
    @Bind(R.id.courseplay_listview)
    ExpandableListView courseplayListview;
    @Bind(R.id.play_scroll)
    ScrollView playScroll;
    @Bind(R.id.continu_learn)
    TextView continuLearn;
    @Bind(R.id.img_continue)
    ImageView imgContinue;

    private View view;
    private CoursePlayExpandAdapter expandAdapter;
    private LivePlayFragmentPersenter persenter;

    private String subjectId;
    private String userId;
    private List<CoursePlayBean> coursePlayBean;

    private EmptyViewLayout mEmptyLayout;
    private CwStudyLogDB cwStudyLogDB;
    private CoursePlayDB coursePlayDB;
    private String courseBean, courseId, cwId;
    private CoursePlay coursePlay;
    private DisplayImageOptions options;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        // setUserVisibleHint(true);
        super.onActivityCreated(savedInstanceState);
    }

    private boolean mHasLoadedOnce = false;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if (isVisibleToUser && !mHasLoadedOnce && coursePlayBean == null) {
                // async http request here
                mHasLoadedOnce = true;
                initData();
            } else if (isVisibleToUser && mHasLoadedOnce && !userId.equals(SharedPrefHelper.getInstance(getActivity()).getUserId())) {
                userId = SharedPrefHelper.getInstance(getActivity()).getUserId();
                initData();
            } else {
                if (playScroll != null) {
                    playScroll.smoothScrollTo(0, 0);
                }
            }
        }
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.course_live_fragment, null);
        ButterKnife.bind(this, view);
        EventBus.getDefault().register(this);
        persenter = new LivePlayFragmentPersenter();
        persenter.attachView(this);

        ViewGroup parent = (ViewGroup) view.getParent();
        if (parent != null) {
            parent.removeView(view);
        }
        initView();
        return view;
    }

    @Override
    public void initView() {
        options = new DisplayImageOptions.Builder()
//                .showStubImage(R.drawable.ic_stub)          // 设置图片下载期间显示的图片
//                .showImageForEmptyUri(R.drawable.ic_empty)  // 设置图片Uri为空或是错误的时候显示的图片
//                .showImageOnFail(R.drawable.ic_error)       // 设置图片加载或解码过程中发生错误显示的图片
                .cacheInMemory(true)                        // 设置下载的图片是否缓存在内存中
                .cacheOnDisc(true)                          // 设置下载的图片是否缓存在SD卡中
//                .displayer(new RoundedBitmapDisplayer(20))  // 设置成圆角图片
                .build();
        coursePlayDB = new CoursePlayDB(getActivity());
        cwStudyLogDB = new CwStudyLogDB(getActivity());
        expandAdapter = new CoursePlayExpandAdapter(getActivity(), imageLoader,options);
        courseplayListview.setOnChildClickListener(this);
        courseplayListview.setOnGroupClickListener(this);
        courseFragmentExamRl.setOnClickListener(this);
//        continuLearn.setOnClickListener(this);

        courseplayListview.setFocusable(false);
        playScroll.smoothScrollBy(0, 0);

        mEmptyLayout = new EmptyViewLayout(getActivity(), courseplayListview);
        mEmptyLayout.setErrorButtonClickListener(mErrorClickListener);
        userId = SharedPrefHelper.getInstance(getActivity()).getUserId();
    }

    private View.OnClickListener mErrorClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            initData();//重新刷新数据
        }
    };

    @Override
    public void initData() {

        if (NetworkUtil.isNetworkAvailable(getActivity())) {
            persenter.getData();
        } else {
            showError("");
        }

        subjectId = SharedPrefHelper.getInstance(getActivity()).getSubjectId();
        if(cwStudyLogDB==null || getActivity()==null){
            return;
        }
        CwStudyLog cwStudyLog = cwStudyLogDB.queryByUserIdLastUpdateTime(SharedPrefHelper.getInstance(getActivity()).getUserId(), subjectId);
        if (cwStudyLog != null) {
            continuLearn.setText(cwStudyLog.getCwName());
            coursePlay = coursePlayDB.findCourse(cwStudyLog.getCourseId());
            imgContinue.setVisibility(View.VISIBLE);
            if (coursePlay != null) {
                courseBean = coursePlay.getCourseBean();
                courseId = cwStudyLog.getCourseId();
                cwId = cwStudyLog.getCwid();
            }
        } else {
            coursePlay = null;
            continuLearn.setText("还未开始学习");
            imgContinue.setVisibility(View.INVISIBLE);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.course_fragment_exam_rl:
                if (coursePlay != null) {
                    Intent intent = new Intent(getActivity(), PlayActivity.class);
                    intent.putExtra("classId", courseId);
                    intent.putExtra("playCwId", cwId);
                    intent.putExtra("courseBean", courseBean);
                    startActivity(intent);
                    MobclickAgent.onEvent(getActivity(), PushConstants.CLASSROOM_COURSE_TO_PLAY);
                }else{  //移植的听课记录 处理需要
                    subjectId = SharedPrefHelper.getInstance(getActivity()).getSubjectId();
                    CwStudyLog cwStudyLog = cwStudyLogDB.queryByUserIdLastUpdateTime(SharedPrefHelper.getInstance(getActivity()).getUserId(), subjectId);
                    if (cwStudyLog != null) {
                        continuLearn.setText(cwStudyLog.getCwName());
                        coursePlay = coursePlayDB.findCourse(cwStudyLog.getCourseId());
                        imgContinue.setVisibility(View.VISIBLE);
                        if (coursePlay != null) {
                            courseBean = coursePlay.getCourseBean();
                            courseId = cwStudyLog.getCourseId();
                            cwId = cwStudyLog.getCwid();
                            Intent intent = new Intent(getActivity(), PlayActivity.class);
                            intent.putExtra("classId", courseId);
                            intent.putExtra("playCwId", cwId);
                            intent.putExtra("courseBean", courseBean);
                            startActivity(intent);
                            MobclickAgent.onEvent(getActivity(), PushConstants.CLASSROOM_COURSE_TO_PLAY);
                        }
                    } else {
                        coursePlay = null;
                        continuLearn.setText("还未开始学习");
                        imgContinue.setVisibility(View.INVISIBLE);
                    }
                }
                break;
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
        EventBus.getDefault().unregister(this);
    }

    private List<CoursePlay> coursePlays=new ArrayList<>();
    private List<CoursePlay> studys=new ArrayList<>();
    private List<CoursePlay> noYets=new ArrayList<>();
    @Override
    public void setData(List<CoursePlayBean> coursePlayBean) {
        coursePlays.clear();
        if (coursePlayBean != null && coursePlayBean.size() > 0 && courseplayListview!=null) {
            for(int k=0;k<coursePlayBean.size();k++){
                if(coursePlayBean.get(k).getCourseType().equals("0")){
                    for(int i=0;i<coursePlayBean.get(k).getCourseItems().size();i++){
                        coursePlayBean.get(k).getCourseItems().get(i).setType("66");
                    }
                }
                coursePlays.addAll(coursePlayBean.get(k).getCourseItems());
            }
            studys.clear();
            noYets.clear();
            for(int j=0;j<coursePlays.size();j++){
                CwStudyLog log=cwStudyLogDB.queryLogs(userId,coursePlays.get(j).getId(),subjectId);
                if(log==null){
                    noYets.add(coursePlays.get(j));
                }else{
                    studys.add(coursePlays.get(j));
                }
            }
            this.coursePlayBean=new ArrayList<>();
            if(studys.size()==0 && noYets.size()!=0){
                CoursePlayBean courselog=new CoursePlayBean();
                courselog.setCourseItems(noYets);
                courselog.setCourseTypeName("未学习");
                this.coursePlayBean.add(courselog);
            }else if(studys.size()!=0 && noYets.size()==0){
                CoursePlayBean courselog=new CoursePlayBean();
                courselog.setCourseItems(studys);
                courselog.setCourseTypeName("已学习");
                this.coursePlayBean.add(courselog);
            }else if(studys.size()!=0 && noYets.size()!=0){
                CoursePlayBean sts=new CoursePlayBean();
                sts.setCourseItems(studys);
                sts.setCourseTypeName("已学习");

                CoursePlayBean not=new CoursePlayBean();
                not.setCourseItems(noYets);
                not.setCourseTypeName("未学习");
                this.coursePlayBean.add(sts);
                this.coursePlayBean.add(not);
            }else{
                mEmptyLayout.showEmpty();
            }
//            this.coursePlayBean = coursePlayBean;
            expandAdapter.setList(this.coursePlayBean);
            courseplayListview.setAdapter(expandAdapter);
            int count = courseplayListview.getCount();
            for (int i = 0; i < count; i++) {
                courseplayListview.expandGroup(i);
            }
            mEmptyLayout.showContentView();

            for (int i = 0; i < coursePlayBean.size(); i++) {
                List<CoursePlay> courseItems = coursePlayBean.get(i).getCourseItems();
                for (int j = 0; j < courseItems.size(); j++) {
                    CoursePlay coursePlay = courseItems.get(j);
                    coursePlay.setCourseBean(JSON.toJSONString(coursePlay));
                    coursePlayDB.insert(coursePlay);
                }
            }
        } else {
            mEmptyLayout.showEmpty();
        }

        EventBus.getDefault().post(new CourseTabTypeEvent(Constants.COURSE_TAB_TYPE_COURSE));
    }

    @Subscribe
    public void onEventAsync(UpdateEvent event) {
        initData();
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        String type=coursePlayBean.get(groupPosition).getCourseItems().get(childPosition).getType();
        if(TextUtils.isEmpty(type)){
            Intent intent = new Intent(getActivity(), PlayActivity.class);
            intent.putExtra("classId", coursePlayBean.get(groupPosition).getCourseItems().get(childPosition).getId());
            intent.putExtra("courseBean", JSON.toJSONString(coursePlayBean.get(groupPosition).getCourseItems().get(childPosition)));
            startActivity(intent);
            MobclickAgent.onEvent(getActivity(), PushConstants.CLASSROOM_COURSE_TO_PLAY);
        }else{
            Toast.makeText(getActivity(), "该课程未开课", Toast.LENGTH_SHORT).show();
        }
//        if (coursePlayBean.get(groupPosition).getCourseItems().get(childPosition).getType().equals("66")) {
//            Toast.makeText(getActivity(), "该课程未开课", Toast.LENGTH_SHORT).show();
//        }else {
//            Intent intent = new Intent(getActivity(), PlayActivity.class);
//            intent.putExtra("classId", coursePlayBean.get(groupPosition).getCourseItems().get(childPosition).getId());
//            intent.putExtra("courseBean", JSON.toJSONString(coursePlayBean.get(groupPosition).getCourseItems().get(childPosition)));
//            startActivity(intent);
//            MobclickAgent.onEvent(getActivity(), PushConstants.CLASSROOM_COURSE_TO_PLAY);
//        }
        return false;
    }

    @Override
    public void showLoading() {
        mEmptyLayout.showLoading();
    }

    @Override
    public void hideLoading() {
        mEmptyLayout.showContentView();
    }

    @Override
    public void showRetry() {

    }

    @Override
    public String getSubjectId() {
        return subjectId + userId;
    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {
        userId = SharedPrefHelper.getInstance(getActivity()).getUserId();
        subjectId = SharedPrefHelper.getInstance(getActivity()).getSubjectId();
        String json = SharedPrefHelper.getInstance(getActivity()).getCoursePlayCache(subjectId + userId);
        if (!TextUtils.isEmpty(json)) {
            BaseBean baseBean = JSON.parseObject(json, BaseBean.class);
            JSONObject object = JSON.parseObject(baseBean.getBody());
            if(object!=null){
                coursePlayBean = JSON.parseArray(object.getString("courseList"), CoursePlayBean.class);
                setData(coursePlayBean);
            }
        } else {
            if(mEmptyLayout!=null){
                mEmptyLayout.setErrorMessage("网络连接失败");
                mEmptyLayout.showError();
            }
        }
    }

    @Override
    public Context context() {
        return getActivity();
    }

    @Override
    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
        return true;
    }

    @Override
    public View getScrollableView() {
        return view;
    }

//    private boolean isfirst;
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        if (!isfirst) {
//            isfirst = true;
//            return;
//        }
//        subjectId = SharedPrefHelper.getInstance(getActivity()).getSubjectId();
//        CwStudyLog cwStudyLog = cwStudyLogDB.queryByUserIdLastUpdateTime(SharedPrefHelper.getInstance(getActivity()).getUserId(), subjectId);
//        if (cwStudyLog != null) {
//            continuLearn.setText(cwStudyLog.getCwName());
//            coursePlay = coursePlayDB.findCourse(cwStudyLog.getCourseId());
//            imgContinue.setVisibility(View.VISIBLE);
//            if (coursePlay != null) {
//                courseBean = coursePlay.getCourseBean();
//                courseId = cwStudyLog.getCourseId();
//                cwId = cwStudyLog.getCwid();
//            }
//        } else {
//            coursePlay = null;
//            continuLearn.setText("还未开始学习");
//            imgContinue.setVisibility(View.INVISIBLE);
//        }
//        refreshData(coursePlayBean);
//        if (expandAdapter != null && coursePlayBean != null) {
//            expandAdapter.setList(coursePlayBean);
//            expandAdapter.notifyDataSetChanged();
//        }
//    }

    public void refreshData(List<CoursePlayBean> coursePlayBean) {
        coursePlays.clear();
        if (coursePlayBean != null && coursePlayBean.size() > 0 && courseplayListview!=null) {
            for(int k=0;k<coursePlayBean.size();k++){
                coursePlays.addAll(coursePlayBean.get(k).getCourseItems());
            }
            studys.clear();
            noYets.clear();
            for(int j=0;j<coursePlays.size();j++){
                CwStudyLog log=cwStudyLogDB.queryLogs(userId,coursePlays.get(j).getId(),subjectId);
                if(log==null){
                    noYets.add(coursePlays.get(j));
                }else{
                    studys.add(coursePlays.get(j));
                }
            }
            this.coursePlayBean=new ArrayList<>();
            if(studys.size()==0 && noYets.size()!=0){
                CoursePlayBean courselog=new CoursePlayBean();
                courselog.setCourseItems(noYets);
                courselog.setCourseTypeName("未学习");
                this.coursePlayBean.add(courselog);
            }else if(studys.size()!=0 && noYets.size()==0){
                CoursePlayBean courselog=new CoursePlayBean();
                courselog.setCourseItems(studys);
                courselog.setCourseTypeName("已学习");
                this.coursePlayBean.add(courselog);
            }else if(studys.size()!=0 && noYets.size()!=0){
                CoursePlayBean sts=new CoursePlayBean();
                sts.setCourseItems(studys);
                sts.setCourseTypeName("已学习");

                CoursePlayBean not=new CoursePlayBean();
                not.setCourseItems(noYets);
                not.setCourseTypeName("未学习");
                this.coursePlayBean.add(sts);
                this.coursePlayBean.add(not);
            }else{
                mEmptyLayout.showEmpty();
            }
//            this.coursePlayBean = coursePlayBean;
            expandAdapter.setList(this.coursePlayBean);
            courseplayListview.setAdapter(expandAdapter);
            int count = courseplayListview.getCount();
            for (int i = 0; i < count; i++) {
                courseplayListview.expandGroup(i);
            }
            mEmptyLayout.showContentView();

            for (int i = 0; i < coursePlayBean.size(); i++) {
                List<CoursePlay> courseItems = coursePlayBean.get(i).getCourseItems();
                for (int j = 0; j < courseItems.size(); j++) {
                    CoursePlay coursePlay = courseItems.get(j);
                    coursePlay.setCourseBean(JSON.toJSONString(coursePlay));
                    coursePlayDB.insert(coursePlay);
                }
            }
        } else {
            mEmptyLayout.showEmpty();
        }

        EventBus.getDefault().post(new CourseTabTypeEvent(Constants.COURSE_TAB_TYPE_COURSE));
    }


    @Subscribe
    public void onEventAsync(FromPlayEvent event) {
        subjectId = SharedPrefHelper.getInstance(getActivity()).getSubjectId();
        CwStudyLog cwStudyLog = cwStudyLogDB.queryByUserIdLastUpdateTime(SharedPrefHelper.getInstance(getActivity()).getUserId(), subjectId);
        if (cwStudyLog != null) {
            continuLearn.setText(cwStudyLog.getCwName());
            coursePlay = coursePlayDB.findCourse(cwStudyLog.getCourseId());
            imgContinue.setVisibility(View.VISIBLE);
            if (coursePlay != null) {
                courseBean = coursePlay.getCourseBean();
                courseId = cwStudyLog.getCourseId();
                cwId = cwStudyLog.getCwid();
            }
        } else {
            coursePlay = null;
            continuLearn.setText("还未开始学习");
            imgContinue.setVisibility(View.INVISIBLE);
        }
        refreshData(coursePlayBean);
        if (expandAdapter != null && coursePlayBean != null) {
            expandAdapter.setList(coursePlayBean);
            expandAdapter.notifyDataSetChanged();
        }
    }
}
