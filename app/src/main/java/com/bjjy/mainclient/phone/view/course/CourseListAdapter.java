package com.bjjy.mainclient.phone.view.course;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.bjjy.mainclient.phone.view.course.bean.MCourseBean;

import java.util.List;

/**
 * Created by dell on 2016/5/16.
 */
public class CourseListAdapter extends FragmentPagerAdapter {

    private final CourseClassFragment banxingFragment;
    private final CourseKechengFragment kechengFragment;
    private Fragment fragment;
    public CourseListAdapter(FragmentManager fm) {
        super(fm);
        banxingFragment= new CourseClassFragment();
        kechengFragment= new CourseKechengFragment();
    }

    @Override
    public Fragment getItem(int position) {
        if(position==0){
            fragment= banxingFragment;
        }else if(position==1){
            fragment= kechengFragment;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

//    @Override
//    public Object instantiateItem(ViewGroup container, int position) {
//        if(position==0){
//            fragment= (CourseClassFragment) super.instantiateItem(container, position);
//        }else if(position==1){
//            fragment= (CourseClassFragment) super.instantiateItem(container, position);
//        }
//        return fragment;
//    }
    
    public void setBanxingData(List<MCourseBean> list){
        banxingFragment.setData(list);
    }
    public void setKechengData(List<MCourseBean> list){
        kechengFragment.setData(list);
    }

//    @Override
//    public int getItemPosition(Object object) {
//        return POSITION_NONE;
//    }

    @Override
    public CharSequence getPageTitle(int position) {
        return super.getPageTitle(position);
    }
}
