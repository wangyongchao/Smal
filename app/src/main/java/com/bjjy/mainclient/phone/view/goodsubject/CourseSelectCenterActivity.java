package com.bjjy.mainclient.phone.view.goodsubject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.dongao.mainclient.model.bean.courselect.Exams;
import com.bjjy.mainclient.persenter.CourseCenterPersenter;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.AppContext;
import com.bjjy.mainclient.phone.app.BaseActivity;
import com.bjjy.mainclient.phone.widget.xlistview.XListView;
import com.bjjy.mainclient.phone.view.goodsubject.adapter.CourseCenterAdapter;
import com.bjjy.mainclient.phone.view.goodsubject.views.CourseCenterView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by dell on 2016/4/5.
 */
public class CourseSelectCenterActivity extends BaseActivity implements CourseCenterView, AdapterView.OnItemClickListener, XListView.IXListViewListener {

    @Bind(R.id.top_title_left)
    ImageView top_title_left;

    @Bind(R.id.top_title_text)
    TextView top_title_text;

    @Bind(R.id.tv_goodsnum)
    TextView tvGoodsnum;

    private XListView listview;
    private CourseCenterPersenter precenter;
    private CourseCenterAdapter adapter;
    private ArrayList<Exams> mList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.courseselectcenter_activity);
        ButterKnife.bind(this);
        precenter = new CourseCenterPersenter();
        precenter.attachView(this);
        initView();
        initData();
    }

    @Override
    public void initView() {
        top_title_left.setVisibility(View.VISIBLE);
        top_title_left.setImageResource(R.drawable.back);
        top_title_left.setOnClickListener(this);
        top_title_text.setText("选课中心");

        listview = (XListView) findViewById(R.id.course_center_ls);
        listview.setPullLoadEnable(false);
        listview.setXListViewListener(this);
        listview.setOnItemClickListener(this);

        adapter = new CourseCenterAdapter(CourseSelectCenterActivity.this);

    }

    @Override
    public void initData() {
        precenter.getData();
    }

    @Override
    public void setAdapter(ArrayList<Exams> examList) {
        listview.stopRefresh();
        this.mList = examList;
        adapter.setList(examList);
        listview.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.top_title_left:
                finish();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent itent = new Intent(this, CourseSelectDetailActivity.class);
        itent.putExtra("exam", mList.get(position - 1));
        startActivity(itent);
    }

    @Override
    public void onRefresh() {
        initData();
    }

    @Override
    public void onLoadMore() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public Context context() {
        return null;
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

//    public class CourseCenterAdapter extends BaseAdapter{
//        @Override
//        public int getCount() {
//            // TODO Auto-generated method stub
//            return 10;
//        }
//
//        @Override
//        public Object getItem(int arg0) {
//            // TODO Auto-generated method stub
//            return null;
//        }
//
//        @Override
//        public long getItemId(int arg0) {
//            // TODO Auto-generated method stub
//            return 0;
//        }
//
//        @Override
//        public View getView(int position, View convertView, ViewGroup arg2) {
//            ViewHolder holder;
//            if(convertView==null){
//                holder=new ViewHolder();
//                convertView=View.inflate(getApplicationContext(),R.layout.courselectcenter_item,null);
//                holder.title=(TextView)convertView.findViewById(R.id.tv_coursename);
//                convertView.setTag(holder);
//            }else{
//                holder=(ViewHolder)convertView.getTag();
//            }
//
//            holder.title.setText("超级会计职称");
//            return convertView;
//        }
//    }
//
//    class ViewHolder{
//        TextView title;
//    }


    @Override
    protected void onResume() {
        super.onResume();
        List list=AppContext.getInstance().getGoods();
        if (list!=null && list.size() > 0) {
            tvGoodsnum.setText("已选课程"+"（"+list.size()+"）");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        List list=AppContext.getInstance().getGoods();
        if (list!=null && list.size() > 0) {
            AppContext.getInstance().getGoods().clear();
        }
    }
}
