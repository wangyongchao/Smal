package com.bjjy.mainclient.phone.download;

import java.util.LinkedList;

public class DownloadTaskManager  {

	private static final String TAG = "DownloadTaskManager";
	// UI请求队列
	private LinkedList<DownloadTask> downloadTasks;

	private static DownloadTaskManager downloadTaskMananger;

	private DownloadTaskManager() {
		downloadTasks = new LinkedList<DownloadTask>();
	}

	public static synchronized DownloadTaskManager getInstance() {
		if (downloadTaskMananger == null) {
			downloadTaskMananger = new DownloadTaskManager();
		}
		return downloadTaskMananger;
	}

	// 1.先执行
	public void addDownloadTask(DownloadTask downloadTask) {
		synchronized (downloadTasks) {
			
			if (!isTaskRepeat(downloadTask)) {
				// 增加下载任务
				downloadTasks.addLast(downloadTask);
				System.out.println("添加成功");
			}else{
				System.out.println("已经存在列表中");
			}
			
		}

	}
	
	private boolean isTaskRepeat(DownloadTask downloadTask){
		//这里还要跟正在下载的url作对比（需求shi 点击下载之后不能点击，所以也可以不对比）
		if(downloadTasks!=null){
			for(DownloadTask task:downloadTasks){
				if(task.getVideo_url().equals(downloadTask.getVideo_url())){
					return true;
				}
			}
		}
		return false;
	}
	
	//删除任务
	public void delete(){//要处理下载中的和轮询到的下载任务（删除下载中的任务要先暂停    删除数据，和更新数据库可能会同事操作，可能有冲突）
		//判断删除的数据库中的任务，在任务列表中列表中的任务是否存在，如果删除了正在下载的（先删除列表中的，再删除下载中的）
	}

	public DownloadTask getDownloadTask() {
		synchronized (downloadTasks) {
			if (downloadTasks.size() > 0) {
				System.out.println("下载管理器增加下载任务：" + "取出任务");
				DownloadTask downloadTask = downloadTasks.removeFirst();
				return downloadTask;
			}
		}
		return null;
	}

	public void clearList(){
		if(downloadTasks!=null && downloadTasks.size()>0){
			downloadTasks.clear();
		}
	}
}
