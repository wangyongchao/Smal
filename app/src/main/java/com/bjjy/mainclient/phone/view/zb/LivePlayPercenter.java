package com.bjjy.mainclient.phone.view.zb;

import com.alibaba.fastjson.JSON;
import com.bjjy.mainclient.phone.view.download.local.bean.YearInfo;
import com.bjjy.mainclient.phone.view.studybar.Constant;
import com.bjjy.mainclient.phone.view.zb.bean.LiveBodyBean;
import com.bjjy.mainclient.phone.view.zb.bean.LivePlayDataBean;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ParamsUtils;
import com.dongao.mainclient.model.remote.bean.BaseBean;

import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by wyc on 2016/5/19.
 */
public class LivePlayPercenter extends BasePersenter<LivePlayView> {
	public ArrayList<LivePlayDataBean> courseList=new ArrayList<>();
	public ArrayList<YearInfo> yearList;
	private String[] years=new String[]{"精品课","公开课"};
	public int currentPage=1;
	public int totalPage=100;

	private String courseId;
	private String userId;
	public YearInfo currYear;
	public LivePlayDataBean currenLiveBean;

	public void initData() {
		userId = SharedPrefHelper.getInstance(getMvpView().context()).getUserId();
		yearList = new ArrayList<>();
		getTest();
		getMvpView().showCurrentYear(currYear);
		getData();
	}

	private void getTest() {
		if (currYear == null) {
			currYear = new YearInfo();
			currYear.setYearName(years[1]);
			currYear.setShowYear(years[1]);
		}
		if (yearList == null || yearList.size() == 0) {
			yearList = new ArrayList<>();
			for (int i = 0; i < 2; i++) {
				YearInfo currYear = new YearInfo();
				currYear.setYearName(years[i]);
				currYear.setShowYear(years[i]);
				yearList.add(currYear);
			}
		}
	}

	@Override
	public void getData() {
		getMvpView().showCurrentYear(currYear);
		getInternetData();
	}

	private void getInternetData() {
		String type;
		if (!isFreeCourse()){
			type="1";
		}else{
			type="2";
		}
		currentPage=1;
		totalPage=100;
		apiModel.getData(ApiClient.getClient().getMyLivePlayData(ParamsUtils.getInstance(getMvpView().context()).getLivePlayData(currentPage,type)));
	}
	
	public boolean isFreeCourse(){
		return currYear.getShowYear().equals(years[1]);
	}

	@Override
	public void setData(String obj) {
		try {
			BaseBean baseBean = JSON.parseObject(obj, BaseBean.class);
//			getMvpView().hideLoading();
			if (baseBean == null) {
				getMvpView().showContentView(Constant.VIEW_TYPE_3);
				return;
			} else {
				if (baseBean.getCode() != Constants.RESULT_CODE) {
					getMvpView().showContentView(Constant.VIEW_TYPE_1);
					return;
				}
			}
			JSONObject jsonObject=new JSONObject(baseBean.getBody());
			LiveBodyBean liveBodyBean=JSON.parseObject(jsonObject.getString("overTeachingPlanList"),LiveBodyBean.class);
			courseList.clear();
			currentPage++;
			totalPage=liveBodyBean.getTotalPage();
			courseList.addAll(liveBodyBean.getList());
			if (courseList.size() == 0) {
				getMvpView().showContentView(Constant.VIEW_TYPE_2);
			} else {
				getMvpView().initAdapter();
				getMvpView().showContentView(Constant.VIEW_TYPE_0);
			}
		} catch (Exception e) {
			getMvpView().showContentView(Constant.VIEW_TYPE_3);
		}
	}

	public void loadMore() {
		String type;
		if (currYear.getShowYear().equals(years[0])){
			type="1";
		}else{
			type="2";
		}
		Call<String> call = ApiClient.getClient().getMyLivePlayData(ParamsUtils.getInstance(getMvpView().context()).getLivePlayData(currentPage,type));
		call.enqueue(new Callback<String>() {
			@Override
			public void onResponse(Call<String> call, Response<String> response) {
				if (response.isSuccessful()) {
					try {
						String str = response.body();
						getMvpView().hideLoading();
						BaseBean baseBean = JSON.parseObject(str, BaseBean.class);
						if (baseBean == null) {
							getMvpView().loadMoreStatus(2);
							return;
						} else {
							if (baseBean.getCode() != Constants.RESULT_CODE) {
								getMvpView().loadMoreStatus(2);
								return;
							}
						}
						JSONObject jsonObject=new JSONObject(baseBean.getBody());
						LiveBodyBean liveBodyBean=JSON.parseObject(jsonObject.getString("overTeachingPlanList"),LiveBodyBean.class);
						currentPage++;
						totalPage=liveBodyBean.getTotalPage();
						if (totalPage < currentPage) {
							getMvpView().loadMoreStatus(1);
						} else {
							getMvpView().loadMoreStatus(0);
						}
						courseList.addAll(liveBodyBean.getList());
						getMvpView().initAdapter();
					} catch (Exception e) {
						getMvpView().loadMoreStatus(2);
					}

				} else {
					getMvpView().loadMoreStatus(2);
				}
			}

			@Override
			public void onFailure(Call<String> call, Throwable t) {
				getMvpView().loadMoreStatus(2);
			}
		});
	}

	public void setOnItemClick(int position) {
		currYear = yearList.get(position);
		getMvpView().showLoading();
		getMvpView().showCurrentYear(currYear);
		getMvpView().hideYearPop(true);
		getData();
	}

	@Override
	public void onError(Exception e) {
		super.onError(e);
		getMvpView().showContentView(Constant.VIEW_TYPE_1);
	}


	public void setLiveItemClick(int position, long l) {
		if (courseList==null)return;
		currenLiveBean=courseList.get(position);
		SharedPrefHelper.getInstance(getMvpView().context()).setLivePlayName(currenLiveBean.getChannelName());
	}

}
