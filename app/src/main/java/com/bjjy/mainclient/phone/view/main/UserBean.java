package com.bjjy.mainclient.phone.view.main;

import java.io.Serializable;
import java.util.List;

public class UserBean implements Serializable {

    // "{\"userId\":\"21878075\",\"type\":\"app\" }"
    private String userId;
    private String type;
    private String signstr;
    private String appVersion;

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getSignstr() {
        return signstr;
    }

    public void setSignstr(String signstr) {
        this.signstr = signstr;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
