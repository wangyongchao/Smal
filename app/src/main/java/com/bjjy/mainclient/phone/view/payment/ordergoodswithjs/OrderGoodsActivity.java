package com.bjjy.mainclient.phone.view.payment.ordergoodswithjs;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.dongao.mainclient.core.webview.WVJBWebViewClient;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseActivity;
import com.bjjy.mainclient.phone.view.payment.ordergoodswithjs.bean.UserInfoJS;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OrderGoodsActivity extends BaseActivity {
	@Bind(R.id.top_title_text)
	TextView top_title_text;
	@Bind(R.id.top_title_left)
	ImageView top_title_left;
	@Bind(R.id.webview)
	WebView webview;
	private MyWebViewClient webViewClient;
	private String userInfo;

	@OnClick(R.id.top_title_left) void onBack() {
		onBackPressed();
	}
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pay_order_goods_activity);
		ButterKnife.bind(this);
		initView();
		initData();
	}

	@Override
	public void initView() {
		top_title_left.setVisibility(View.VISIBLE);
		top_title_text.setText(getResources().getString(R.string.pay_order_title));
		UserInfoJS userInfoJS=new UserInfoJS();
		userInfoJS.setType("app");
		userInfoJS.setUserId(SharedPrefHelper.getInstance(this).getUserId());
		userInfo= JSON.toJSONString(userInfoJS);
		webview.getSettings().setJavaScriptEnabled(true);
		webview.setWebChromeClient(new WebChromeClient());
		webview.loadUrl("http://p.m.test.com/zsfa/to_swx.html");

		webViewClient = new MyWebViewClient(webview);
		webViewClient.enableLogging();
		webview.setWebViewClient(webViewClient);
		
		try {
			webViewClient.callHandler("returnCommonParams",
                    new JSONObject(userInfo),
                    new WVJBWebViewClient.WVJBResponseCallback() {
                        @Override
                        public void callback(Object data) {
                            Toast.makeText(OrderGoodsActivity.this, "Android call testJavascriptHandler got response! :" + data, Toast.LENGTH_LONG).show();
                        }
                    });
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void initData() {

	}


	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
	}

	class MyWebViewClient extends WVJBWebViewClient {
		public MyWebViewClient(WebView webView) {
			// support js send
			super(webView, new WVJBWebViewClient.WVJBHandler() {
				@Override
				public void request(Object data, WVJBResponseCallback callback) {
//					callback.callback("Response for message from Android!");
				}
			});

			// not support js send
			registerHandler("getJSRegisterInfoHandler", new WVJBWebViewClient.WVJBHandler() {
				@Override
				public void request(Object data, WVJBResponseCallback callback) {
					Toast.makeText(OrderGoodsActivity.this, "getJSRegisterInfoHandler" + data, Toast.LENGTH_LONG).show();
					callback.callback("Response from testAndroidCallback!");
				}
			});

			registerHandler("returnCommonParams", new WVJBWebViewClient.WVJBHandler() {
				@Override
				public void request(Object data, WVJBResponseCallback callback) {
					Toast.makeText(OrderGoodsActivity.this, "getJSRegisterInfoHandler" + data, Toast.LENGTH_LONG).show();
					callback.callback("Response from testAndroidCallback!");
				}
			});

//			send("A string sent from Android before Webview has loaded.",
//					new WVJBResponseCallback() {
//						@Override
//						public void callback(Object data) {
//							Toast.makeText(OrderGoodsActivity.this, "Android got response! :" + data, Toast.LENGTH_LONG).show();
//						}
//					});

			try {
				callHandler("returnCommonParams",
						new JSONObject(userInfo),
						new WVJBResponseCallback() {
							@Override
							public void callback(Object data) {
								Toast.makeText(OrderGoodsActivity.this, "Android call testJavascriptHandler got response! :" + data, Toast.LENGTH_LONG).show();
							}
						});

			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
		}

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			return super.shouldOverrideUrlLoading(view, url);
		}
	}


	@Override
	public void onClick(View v) {

	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}
}