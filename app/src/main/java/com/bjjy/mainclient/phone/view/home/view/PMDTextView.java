package com.bjjy.mainclient.phone.view.home.view;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by wyc on 2016/7/19.
 */
public class PMDTextView extends TextView {


    public PMDTextView(Context context) {
        super(context);
    }

    public PMDTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PMDTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    public boolean isFocused() {
        // TODO Auto-generated method stub
//        if (getEditableText().equals(TextUtils.TruncateAt.MARQUEE)) {
//            return true;
//        }
//        return super.isFocused();
        return true;
    }
}
