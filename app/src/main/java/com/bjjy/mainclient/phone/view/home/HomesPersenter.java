package com.bjjy.mainclient.phone.view.home;


import com.bjjy.mainclient.phone.event.MainTabChoosed;
import com.bjjy.mainclient.phone.view.studybar.Constant;
import com.dongao.mainclient.model.bean.course.Subject;
import com.dongao.mainclient.model.bean.home.HomeItem;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.local.SubjectDB;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.bjjy.mainclient.phone.dict.MainTypeEnum;
import com.bjjy.mainclient.phone.view.main.HomeView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;


/**
 * 首页Persenter
 */
public class HomesPersenter extends BasePersenter<HomeView> implements TimeLineAdapter.OnItemClickedListener {

    private String userId;
    private com.dongao.mainclient.model.local.HomeDB newHomeDB;
    private SubjectDB subjectDB;
    private List<Subject> subjectList;
    private CompositeSubscription mSubscriptions;


    @Override
    public void attachView(HomeView mvpView) {
        super.attachView(mvpView);
        initData();
    }

    @Override
    public void detachView() {
        super.detachView();
        mSubscriptions.clear();
    }

    private void initData() {
        newHomeDB = new com.dongao.mainclient.model.local.HomeDB(getMvpView().context());
        userId = SharedPrefHelper.getInstance(getMvpView().context()).getUserId();
        subjectDB = new SubjectDB(getMvpView().context());
        mSubscriptions = new CompositeSubscription();
    }

    private List<HomeItem> getHomeList(String userId) {
        return newHomeDB.getHomelist(userId);
    }


    @Override
    public void getData() {
        getMvpView().getAdapter().setmOnItemClickedListener(this);
        refreshData(true);
    }

    /**
     * 加载数据 新的添加逻辑
     */
    public void refreshData(boolean tag) {
        getMvpView().showProgress(true);

        Subscription subscribe = Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                userId = SharedPrefHelper.getInstance(getMvpView().context()).getUserId();
                subscriber.onNext(userId);
                subscriber.onCompleted();
            }
        })
                .subscribeOn(Schedulers.io())
                //数据库查询 io线程
                .observeOn(Schedulers.io())
                .map(new Func1<String, List<HomeItem>>() {
                    @Override
                    public List<HomeItem> call(String userid) {
                        return getHomeList(userid);
                    }
                })
                //分发每一个item
                .observeOn(Schedulers.computation())
                .flatMap(new Func1<List<HomeItem>, Observable<HomeItem>>() {
                    @Override
                    public Observable<HomeItem> call(List<HomeItem> homeItems) {
                        return Observable.from(homeItems);
                    }
                })
                //每个 item 查询数据库 修改subjectname io线程
                .observeOn(Schedulers.io())
                .map(new Func1<HomeItem, HomeItem>() {
                    @Override
                    public HomeItem call(HomeItem homeItem) {
                        Subject subject = subjectDB.findById(userId, homeItem.getSubjectId());
                        homeItem.setSubjectName(subject.getSubjectName());
                        if (homeItem.getContentType() == 1) {
                            homeItem.setTitle2(homeItem.getSubjectName());
                            homeItem.setTitle3(homeItem.getClazzName());
                        }
                        return homeItem;
                    }
                })
                .observeOn(Schedulers.computation())
                .toList()
                //最终显示在ui线程
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<HomeItem>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        getMvpView().setData(setEmptyData());
                        getMvpView().showProgress(false);
                    }

                    @Override
                    public void onNext(List<HomeItem> homeItems) {
                        if (homeItems == null || homeItems.size() == 0) {
                            homeItems = setEmptyData();
                        }
                        getMvpView().setData(homeItems);
                        getMvpView().showProgress(false);
                    }
                });

        mSubscriptions.add(subscribe);

    }


    @Override
    public void setData(String obj) {

    }

    /**
     * 设置首次进入无数据的情况
     */
    private List<HomeItem> setEmptyData() {
        List<HomeItem> msgValuesList = new ArrayList<>();
        HomeItem item = new HomeItem();
        item.setUserId(userId);
        item.setContentType(10);
        item.setId("2");
        item.setTitle1("录播课");
        item.setTitle2("中腾建华名师精讲课程（基础精讲、强化突破、模考冲刺）");
        HomeItem item2 = new HomeItem();
        item2.setUserId(userId);
        item2.setContentType(10);
        item2.setId("1");
        item2.setTitle1("直播课");
        item2.setTitle2("离消防工程师只差一个中腾建华的距离，全国资深名师每天20:00-21:00在线高清直播，敬请观看");
        HomeItem item3 = new HomeItem();
        item3.setUserId(userId);
        item3.setContentType(10);
        item3.setId("3");
        item3.setTitle1("模拟考");
        item3.setTitle2("海量免费模拟试题在线测试");
        msgValuesList.add(item);
        msgValuesList.add(item2);
        msgValuesList.add(item3);
        return msgValuesList;
    }

    @Override
    public void OnItemClickedListener(HomeItem msgValue) {
        int contentType = msgValue.getContentType();
        switch (contentType) {
            default:
                MainTypeEnum.MAIN_TYPE_SELECT_COURSE.getName();
                    if ("1".equals(msgValue.getId())) {
                        EventBus.getDefault().post(new MainTabChoosed(2));
                    }else if ("2".equals(msgValue.getId())) {
                        EventBus.getDefault().post(new MainTabChoosed(1));
                    }  else {
                        EventBus.getDefault().post(new MainTabChoosed(1));
                    }

                break;
        }

    }

    @Override
    public void onError(Exception e) {
        super.onError(e);
        getMvpView().showProgress(false);
        getMvpView().showCurrentView(Constant.VIEW_TYPE_3);
    }
}
