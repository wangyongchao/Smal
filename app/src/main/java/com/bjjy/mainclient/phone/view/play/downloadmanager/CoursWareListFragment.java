package com.bjjy.mainclient.phone.view.play.downloadmanager;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.dongao.mainclient.model.bean.play.CourseChapter;
import com.dongao.mainclient.model.bean.play.CourseWare;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.utils.NetWorkUtils;
import com.bjjy.mainclient.phone.view.play.downloadmanager.adapter.CourseWareAdapter;
import com.bjjy.mainclient.phone.widget.DialogManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

/**
 * Created by yunfei on 2016/11/30.
 */

public class CoursWareListFragment extends AbsCoursListFragment implements AdapterView.OnItemClickListener {

    private CourseWareAdapter adapter;

    public static CoursWareListFragment getNewInstance(ArrayList<CourseWare> data) {
        CoursWareListFragment coursWareListFragment = new CoursWareListFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("data", data);
        coursWareListFragment.setArguments(bundle);
        return coursWareListFragment;
    }

    @Bind(R.id.lv_main)
    ListView lv_main;

    @Override
    protected int getContentId() {
        return R.layout.pay_download_cours_ware_list_fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ArrayList<CourseWare> data = (ArrayList<CourseWare>) getArguments().getSerializable("data");
        presenter.setCourseWareList(data);
        presenter.getData();
    }

    @Override
    public void initView() {
        adapter = new CourseWareAdapter(context(), onItemSelectNumChangedListener, presenter.getCourseWaresChecked());
        lv_main.setAdapter(adapter);
        lv_main.setOnItemClickListener(this);
    }


    @Override
    public void setCourseWareList(List<CourseWare> list) {
        adapter.setData(list);
    }

    @Override
    public void setCourseChapterList(List<CourseChapter> courseChapters) {

    }

    @Override
    public void updateList() {
        adapter.notifyDataSetChanged();
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

        CourseWare courseWare = (CourseWare) parent.getAdapter().getItem(position);

        if (presenter.isAddDownloadDB(courseWare.getState())) {
            showRepeatDownLoadHit();
            return;
        }

        NetWorkUtils type = new NetWorkUtils(getActivity());
        if (type.getNetType() == 0) {//无网络
            showNetWorkErrorHit();
        } else if (type.getNetType() == 2) { //流量
//            if (SharedPrefHelper.getInstance(getActivity()).getIsNoWifiPlayDownload()) {
                DialogManager.showNormalDialog(getActivity(), getResources().getString(R.string.dialog_warnning_vedio), "提示", "取消", "确定",
                        new DialogManager.CustomDialogCloseListener() {
                            @Override
                            public void yesClick() {
                                presenter.setOnCoursWareListItemClick(position);
                            }

                            @Override
                            public void noClick() {

                            }
                        });
//            }
        } else {
            presenter.setOnCoursWareListItemClick(position);
        }

    }
}
