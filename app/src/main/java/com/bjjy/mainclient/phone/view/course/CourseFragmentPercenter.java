package com.bjjy.mainclient.phone.view.course;

import com.alibaba.fastjson.JSON;
import com.bjjy.mainclient.phone.view.course.bean.MCourseBean;
import com.bjjy.mainclient.phone.view.download.local.bean.YearInfo;
import com.bjjy.mainclient.phone.view.studybar.Constant;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ParamsUtils;
import com.dongao.mainclient.model.remote.bean.BaseBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by wyc on 2016/5/19.
 */
public class CourseFragmentPercenter extends BasePersenter<CourseFragmentView> {

    public List<MCourseBean> privateTeacherList;
    public List<MCourseBean> banxingList=new ArrayList<>();
    public List<MCourseBean> ketangList=new ArrayList<>();
    public ArrayList<YearInfo> yearList=new ArrayList<>();
    public YearInfo currYear;
    private String userId;

    public void initData() {
        userId = SharedPrefHelper.getInstance(getMvpView().context()).getUserId() + "";
        privateTeacherList = new ArrayList<>();
        getData();
    }

    @Override
    public void getData() {
        getMvpView().showLoading();
//        getTest();
        getMvpView().initYearAdapter();
        loadYear(ParamsUtils.getInstance(getMvpView().context()).getMyYear(""));
//        if (NetUtils.checkNet(getMvpView().context()).isAvailable()) {
//            getInterData();
//        } else {
//            getMvpView().showContentView(Constant.VIEW_TYPE_1);
//        }
    }

    private void loadYear(HashMap<String, String> params) {
        Call<String> call = ApiClient.getClient().getYearList(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    String str = response.body();
                    try {
                        BaseBean baseBean = JSON.parseObject(str, BaseBean.class);
                        if (baseBean == null) {
                            getMvpView().showContentView(Constant.VIEW_TYPE_3);
                            return;
                        } else {
                            if (baseBean.getCode() != Constants.RESULT_CODE) {
                                getMvpView().showContentView(Constant.VIEW_TYPE_3);
                                return;
                            }
                        }
                        String body = baseBean.getBody();
                        yearList.clear();
                        List<YearInfo>  yearInfos=JSON.parseArray(body,YearInfo.class);
                        yearList.addAll(yearInfos);
                        if (yearList.size()==0){getMvpView().showContentView(Constant.VIEW_TYPE_2);return;}
                        if (currYear==null) currYear = yearList.get(0);
                        getMvpView().showContentView(Constant.VIEW_TYPE_0);
                        SharedPrefHelper.getInstance(getMvpView().context()).setYearList(body);
                        SharedPrefHelper.getInstance(getMvpView().context()).setCurYear(JSON.toJSONString(currYear));
                        getMvpView().showCurrentYear(currYear);
                        getInterData();
                    } catch (Exception e) {
                        getMvpView().showContentView(Constant.VIEW_TYPE_1);
                    }

                } else {
                    getMvpView().showContentView(Constant.VIEW_TYPE_1);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                getMvpView().showContentView(Constant.VIEW_TYPE_1);
            }
        });
    }

    //TODO
    private void getTest() {
        if (currYear == null) {
            currYear = new YearInfo();
            currYear.setYearName(2017 + "");
            currYear.setShowYear(2017 + "");
        }
        if (yearList == null || yearList.size() == 0) {
            yearList = new ArrayList<>();
            for (int i = 2016; i < 2020; i++) {
                YearInfo currYear = new YearInfo();
                currYear.setYearName(i + "");
                currYear.setShowYear(i + "");
                yearList.add(currYear);
            }
        }
    }


    public void getInterData() {
        apiModel.getData(ApiClient.getClient().getMyCourseData(ParamsUtils.getInstance(getMvpView().context()).getMyCourseData(currYear.getYearName())));
    }

    @Override
    public void setData(String obj) {
        try{
            BaseBean baseBean = JSON.parseObject(obj, BaseBean.class);
            if (baseBean == null) {
                getMvpView().showContentView(Constant.VIEW_TYPE_1);
                return;
            } else {
                int result = baseBean.getCode();
                if (result != Constants.RESULT_CODE) {
                    getMvpView().showContentView(Constant.VIEW_TYPE_3);
                    return;
                }
            }
            String body = baseBean.getBody();
            List<MCourseBean> messageBeanList = JSON.parseArray(body, MCourseBean.class);
            privateTeacherList.clear();
            privateTeacherList.addAll(messageBeanList);
            if (privateTeacherList == null ||privateTeacherList.size()==0) {
                getMvpView().showContentView(Constant.VIEW_TYPE_2);
            }else {
                changeData();
                getMvpView().showContentView(Constant.VIEW_TYPE_0);
            }
        }catch (Exception e){
            getMvpView().showContentView(Constant.VIEW_TYPE_3);
        }
    }

    private void changeData() {
        ketangList.clear();
        banxingList.clear();
        for (int i = 0; i < privateTeacherList.size(); i++) {
            if (privateTeacherList.get(i).getPdId()==1){//课程
                ketangList.add(privateTeacherList.get(i));
            }else{
                banxingList.add(privateTeacherList.get(i));
            }
        }
        getMvpView().initAdapter();
    }

    public void setOnItemClick(int position) {
        currYear = yearList.get(position);
        getMvpView().showCurrentYear(currYear);
        SharedPrefHelper.getInstance(getMvpView().context()).setCurYear(JSON.toJSONString(currYear));
        SharedPrefHelper.getInstance(getMvpView().context()).setSubjectId(currYear.getYearName());
        getMvpView().hideYearPop(true);
        getInterData();
    }

    @Override
    public void onError(Exception e) {
        super.onError(e);
        getMvpView().showContentView(Constant.VIEW_TYPE_1);
    }
}
