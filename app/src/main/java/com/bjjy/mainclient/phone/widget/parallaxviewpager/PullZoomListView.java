package com.bjjy.mainclient.phone.widget.parallaxviewpager;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dongao.mainclient.core.util.DateUtil;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.view.home.view.PMDTextView;

import java.util.Calendar;

public class PullZoomListView extends ListView implements AbsListView.OnScrollListener {

    private ImageView cover_imageView;

    private ImageView mack_cover_imageView;

    private FrameLayout layout_header_container;


    private int cover_imageViewheight;
    private int cover_imageViewheightMaxHeight;

    private OnScrollListener mOnScrollListener;

    private View footerView;
    private TextView footTv;
    private View preTv;
    private boolean isNomore = false;
    private boolean isLoad = false;

    private View circleLoadingView;

    private ProgressBar pb;

    private int maxY = 200;


    private Context context;

    private ScrollDistanceListener mScrollDistanceListener;

    private boolean mListScrollStarted;
    private int mFirstVisibleItem;
    private int mFirstVisibleHeight;
    private int mFirstVisibleTop, mFirstVisibleBottom;
    private int mTotalScrollDistance;
    private PMDTextView home_title_for_excerice;
    private TextView month_tv;
    private TextView day_tv;
    private TextView week_tv;

    public PullZoomListView(Context context) {
        super(context);
        init(context);
    }

    public PullZoomListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public PullZoomListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public void setOnScrollListener(
            OnScrollListener paramOnScrollListener) {
        this.mOnScrollListener = paramOnScrollListener;
    }


    public void init(Context ctx) {
        context = ctx;
        setOverScrollMode(OVER_SCROLL_NEVER);
        View headView = View.inflate(ctx, R.layout.home_fragment_top, null);
        layout_header_container = (FrameLayout) headView.findViewById(R.id.layout_header_container);
        mack_cover_imageView = (ImageView) headView.findViewById(R.id.mack_cover_imageView);
        cover_imageView = (ImageView) headView.findViewById(R.id.cover_imageView);
        circleLoadingView = headView.findViewById(R.id.rl_circle_loading);
        pb = (ProgressBar) headView.findViewById(R.id.progressBar);
        home_title_for_excerice = (PMDTextView) headView.findViewById(R.id.home_title_for_excerice);
        pb.setVisibility(View.INVISIBLE);

        FrameLayout layout_day = (FrameLayout) headView.findViewById(R.id.day_layout);
        layout_day.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                MobclickAgent.onEvent(context, PushConstants.MAIN_TO_DAYTEST);
//                Intent intent = new Intent(context, DayTestActivity.class);
//                context.startActivity(intent);
            }
        });
        month_tv = (TextView) headView.findViewById(R.id.month);
        day_tv = (TextView) headView.findViewById(R.id.day);
        week_tv = (TextView) headView.findViewById(R.id.week);

        reFreshTime();

        cover_imageViewheight = context.getResources().getDimensionPixelSize(R.dimen.imageview_height);
        layout_header_container.getLayoutParams().height = cover_imageViewheight;
        cover_imageViewheightMaxHeight = cover_imageViewheight * 2;
        maxY = cover_imageViewheight;

        addHeaderView(headView);

        footerView = LayoutInflater.from(context).inflate(R.layout.xlistview_footer, null);
        footTv = (TextView) footerView.findViewById(R.id.xlistview_footer_hint_textview);
        preTv = footerView.findViewById(R.id.xlistview_footer_progressbar);
        footTv.setText("查看更多");
        addFooterView(footerView);
        super.setOnScrollListener(this);
        footerView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (!isNomore) {
                    //footTv.setText("加载中...");
                    footTv.setVisibility(View.GONE);
                    preTv.setVisibility(View.VISIBLE);
                    startLoadMore();
                }
            }
        });
    }

    public void reFreshTime() {
        Calendar cal = Calendar.getInstance();
        int day = cal.get(Calendar.DATE);
        int month = cal.get(Calendar.MONTH) + 1;

        month_tv.setText(month + "月");
        day_tv.setText(day + "");
        week_tv.setText(DateUtil.getWeekOfDate());
    }

    public void setProgress(boolean visible) {
        circleLoadingView.clearAnimation();
        if (visible) {
            ScaleAnimation scaleAnimation = new ScaleAnimation(1f, 1.5f, 1f, 1.5f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            scaleAnimation.setDuration(500);
            scaleAnimation.setRepeatCount(Animation.INFINITE);
            scaleAnimation.setRepeatMode(Animation.REVERSE);
            circleLoadingView.startAnimation(scaleAnimation);
            pb.setVisibility(View.VISIBLE);
        } else {
            pb.setVisibility(View.INVISIBLE);
        }
    }

    public boolean isRefreshing() {
        if (pb.getVisibility() == View.VISIBLE) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 设置首页的title
     */
    public void setHomeTitle(String title) {
        if (home_title_for_excerice != null) {
            home_title_for_excerice.setText(title);
        }
    }

    public void setHeadScaleImage(Bitmap bitmap) {
        cover_imageView.setImageBitmap(bitmap);
        mack_cover_imageView.setImageBitmap(bitmap);
        mack_cover_imageView.setAlpha(0);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        if (this.mOnScrollListener != null) {
            this.mOnScrollListener.onScrollStateChanged(view, scrollState);
        }
        if (view.getLastVisiblePosition() == (getCount() - 1)) {
            if (isNomore) {
                footerView.setVisibility(View.GONE);
                footerView.setPadding(0, -200, 0, 0);
            } else {
                footerView.setVisibility(View.VISIBLE);
                footerView.setPadding(0, 0, 0, 0);
                //footTv.setText("加载中...");
                footTv.setVisibility(View.GONE);
                preTv.setVisibility(View.VISIBLE);
                startLoadMore();
            }
        }

        if (view.getCount() == 0) return;
        switch (scrollState) {
            case SCROLL_STATE_IDLE: {
                mListScrollStarted = false;
                break;
            }
            case SCROLL_STATE_TOUCH_SCROLL: {
                final View firstChild = view.getChildAt(0);
                mFirstVisibleItem = view.getFirstVisiblePosition();
                mFirstVisibleTop = firstChild.getTop();
                mFirstVisibleBottom = firstChild.getBottom();
                mFirstVisibleHeight = firstChild.getHeight();
                mListScrollStarted = true;
                mTotalScrollDistance = 0;
                break;
            }
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (this.mOnScrollListener != null) {
            this.mOnScrollListener.onScroll(view, firstVisibleItem,
                    visibleItemCount, totalItemCount);
        }

        if (totalItemCount == 0 || !mListScrollStarted) return;
        final View firstChild = view.getChildAt(0);
        final int firstVisibleTop = firstChild.getTop(), firstVisibleBottom = firstChild.getBottom();
        final int firstVisibleHeight = firstChild.getHeight();
        final int delta;
        if (firstVisibleItem > mFirstVisibleItem) {
            mFirstVisibleTop += mFirstVisibleHeight;
            delta = firstVisibleTop - mFirstVisibleTop;
        } else if (firstVisibleItem < mFirstVisibleItem) {
            mFirstVisibleBottom -= mFirstVisibleHeight;
            delta = firstVisibleBottom - mFirstVisibleBottom;
        } else {
            delta = firstVisibleBottom - mFirstVisibleBottom;
        }
        mTotalScrollDistance += delta;
        if (mScrollDistanceListener != null) {
            mScrollDistanceListener.onScrollDistanceChanged(delta, mTotalScrollDistance);
        }
        mFirstVisibleTop = firstVisibleTop;
        mFirstVisibleBottom = firstVisibleBottom;
        mFirstVisibleHeight = firstVisibleHeight;
        mFirstVisibleItem = firstVisibleItem;
    }

    @Override
    protected boolean overScrollBy(int deltaX, int deltaY, int scrollX, int scrollY, int scrollRangeX, int scrollRangeY, int maxOverScrollX, int maxOverScrollY, boolean isTouchEvent) {

        if (layout_header_container.getHeight() <= cover_imageViewheightMaxHeight && isTouchEvent) {
            final int destImageViewHeight = layout_header_container.getHeight() - deltaY / 2;

            layout_header_container.getLayoutParams().height = Math.min(cover_imageViewheightMaxHeight, Math.max(destImageViewHeight, cover_imageViewheight));
            layout_header_container.requestLayout();

        }

        return super.overScrollBy(deltaX, deltaY, scrollX, scrollY, scrollRangeX, scrollRangeY, maxOverScrollX, maxOverScrollY, isTouchEvent);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        float currentX = ev.getX();
        float currentY = ev.getY();
        switch (ev.getActionMasked()) {
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                if (layout_header_container.getHeight() > cover_imageViewheight) {
                    ReleaseHandAnimator animator = new ReleaseHandAnimator(layout_header_container, cover_imageViewheight);
                    animator.setDuration(500);
                    layout_header_container.startAnimation(animator);

                    if (layout_header_container.getHeight() > cover_imageViewheight + 300) { //只有滑动到一定的距离才让其刷新
                        if (!pb.isShown()) {
                            mListViewListener.onLoadRefresh();
                            setProgress(true);
                        }

                    }
                }
                break;
        }

        return super.onTouchEvent(ev);
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);

       /* int containerHeight = layout_header_container.getHeight();
        //向下拉containerHeight会变大
        int diff = containerHeight - cover_imageViewheight;

        if (diff > 0) { //往下拉的时候会渐渐显示出模糊图层
            double factor = (diff + (double) cover_imageViewheight / 10.0) * 2 / (double) (cover_imageViewheightMaxHeight - cover_imageViewheight);
            mack_cover_imageView.setAlpha(Math.max(0, Math.min((int) (factor * 255), 255)));

        } else if (mack_cover_imageView.getAlpha() != 0) {
            mack_cover_imageView.setAlpha(0);
        }*/

        if (mListViewListener != null) {
            mListViewListener.onScollTo();
        }

    }


    public void setLoadFinish(boolean flag) {
        footTv.setVisibility(View.VISIBLE);
        footTv.setText("查看更多");
        preTv.setVisibility(View.INVISIBLE);
        isLoad = false;
        isNomore = flag;
        if (isNomore) {
            footerView.setVisibility(View.GONE);
            footerView.setPadding(0, -200, 0, 0);
        } else {
            footerView.setVisibility(View.VISIBLE);
            footerView.setPadding(0, 0, 0, 0);
        }
    }

    private void startLoadMore() {
        if (mListViewListener != null && !isLoad) {
            isLoad = true;
            mListViewListener.onLoadMore();
        }
    }

    public void setPullToZoomListViewListener(PullToZoomListViewListener l) {
        mListViewListener = l;
    }

    public interface PullToZoomListViewListener {
        public void onScollTo();

        public void onLoadRefresh();

        public void onLoadMore();
    }


    public int getTotalScrollDistance() {
        return mTotalScrollDistance;
    }

    public void setScrollDistanceListener(ScrollDistanceListener listener) {
        mScrollDistanceListener = listener;
    }

    public static interface ScrollDistanceListener {
        void onScrollDistanceChanged(int delta, int total);
    }


    private PullToZoomListViewListener mListViewListener;

    private class ReleaseHandAnimator extends Animation {
        private float targetHeight;
        private View targetView;
        private float diff;

        public ReleaseHandAnimator(View view, float targetHeight) {
            this.targetView = view;
            this.targetHeight = targetHeight;
            this.diff = targetHeight - view.getHeight();

        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            super.applyTransformation(interpolatedTime, t);
            targetView.getLayoutParams().height = (int) (targetHeight - diff * (1 - interpolatedTime));
            targetView.requestLayout();
        }
    }

}
