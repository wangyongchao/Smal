package com.bjjy.mainclient.phone.view.main;


import com.dongao.mainclient.model.bean.home.HomeItem;
import com.dongao.mainclient.model.mvp.MvpView;
import com.bjjy.mainclient.phone.app.AppContext;

import java.util.List;

/**
 * UI 定义UI 看此UI中有何事件
 */
public interface HomeView extends MvpView {
  void setData(List<HomeItem> books);
  void showNetError();
  void setLoadFinish();
  com.bjjy.mainclient.phone.view.home.TimeLineAdapter getAdapter();
  void showLogin();
  void showProgress(boolean visible);
  AppContext getApplicationContext();
  void showCurrentView(int type);
}
