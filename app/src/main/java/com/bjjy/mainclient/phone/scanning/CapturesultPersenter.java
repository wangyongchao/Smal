package com.bjjy.mainclient.phone.scanning;


import android.content.Intent;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.bean.play.CourseWare;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ParamsUtils;
import com.dongao.mainclient.model.remote.bean.BaseBean;
import com.bjjy.mainclient.phone.view.user.LoginNewActivity;

import java.util.List;


/**
 * 课堂首页Persenter
 */
public class CapturesultPersenter extends BasePersenter<CapturesultView> {
    private List<CourseWare> courseWares ;
    private String type;

    @Override
    public void attachView(CapturesultView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    @Override
    public void getData(){
        //一些业务逻辑的判断 有网没网 是请求网络还是请求数据库
        getMvpView().showLoading();
        String qrUrl=getMvpView().getQrUrl();

        apiModel.getData(ApiClient.getClient().getype(
                ParamsUtils.getInstance(getMvpView().context()).getType(qrUrl)));
    }

    @Override
    public void setData(String obj) {
        try {
            BaseBean baseBean = JSON.parseObject(obj,BaseBean.class);
            if(baseBean == null){
                getMvpView().showError("");
                return;
            }
            JSONObject ob=JSON.parseObject(obj);
            int code=ob.getInteger("code");
            if(code==1000){
                JSONObject object=JSON.parseObject(baseBean.getBody());
                courseWares=JSON.parseArray(object.getString("mobileCourseWare"),CourseWare.class);
                getMvpView().setData(courseWares);
            }else if(code==1013){
                Intent intent=new Intent(getMvpView().context(),ScannerPayActivity.class);
                getMvpView().context().startActivity(intent);
            }else if(code==1012){
                Intent intent=new Intent(getMvpView().context(),LoginNewActivity.class);
                getMvpView().context().startActivity(intent);
            }else if(code==1021){
                Toast.makeText(getMvpView().context(),"该二维码未关联讲次", Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(getMvpView().context(),ob.getString("msg"), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

//    public void geDataType1(){
//        getMvpView().showLoading();
//        String id=getMvpView().getQrUrl();
//        Call<String> call = ApiClient.getClient().getDataType1(ParamsUtils.getInstance(getMvpView().context()).getDataType1(id));
//        call.enqueue(new Callback<String>() {
//            @Override
//            public void onResponse(Call<String> call, Response<String> response) {
//                if (response.isSuccessful()) {
//                    getMvpView().hideLoading();
//                    String str = response.body();
//                    boolean isfree=false;
//                    try {
//                        BaseBean baseBean = JSON.parseObject(str,BaseBean.class);
//                        if(baseBean == null){
//                            getMvpView().showError("");
//                            return;
//                        }
//                        if(baseBean.getResult().getCode()==1000){
//                            JSONObject object=JSON.parseObject(baseBean.getBody());
//                            isfree=object.getBoolean("qrCoedFree");
//                            courseWares=JSON.parseArray(object.getString("mobileCourseWare"),CourseWare.class);
//                        }else{
//                            Toast.makeText(getMvpView().context(),baseBean.getResult().getMsg(), Toast.LENGTH_SHORT).show();
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    getMvpView().setData(courseWares, isfree);
//                } else {
//                    Toast.makeText(getMvpView().context(), "数据异常", Toast.LENGTH_SHORT).show();
//                    getMvpView().getActivity().finish();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<String> call, Throwable t) {
//                getMvpView().hideLoading();
//                Toast.makeText(getMvpView().context(), "请检查网络是否连接", Toast.LENGTH_SHORT).show();
//                getMvpView().getActivity().finish();
//            }
//        });
//    }

//    public void getQrcodeNew(String url){
//        getMvpView().showLoading();
//        String id=getMvpView().getQrUrl();
//        Call<String> call = ApiClient.getClient().getQrcodeNew(url);
//        call.enqueue(new Callback<String>() {
//            @Override
//            public void onResponse(Call<String> call, Response<String> response) {
//                if (response.isSuccessful()) {
//                    getMvpView().hideLoading();
//                    String str = response.body();
//                    boolean isfree=false;
//                    try {
//                        BaseBean baseBean = JSON.parseObject(str,BaseBean.class);
//                        if(baseBean == null){
//                            getMvpView().showError("");
//                            return;
//                        }
//                        if(baseBean.getResult().getCode()==1000){
//                            JSONObject object=JSON.parseObject(baseBean.getBody());
//                            isfree=object.getBoolean("qrCoedFree");
//                            courseWares=JSON.parseArray(object.getString("mobileCourseWare"),CourseWare.class);
//                        }else{
//                            Toast.makeText(getMvpView().context(),baseBean.getResult().getMsg(), Toast.LENGTH_SHORT).show();
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    getMvpView().setData(courseWares, isfree);
//                } else {
//                    Toast.makeText(getMvpView().context(), "数据异常", Toast.LENGTH_SHORT).show();
//                    getMvpView().getActivity().finish();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<String> call, Throwable t) {
//                getMvpView().hideLoading();
//                Toast.makeText(getMvpView().context(), "请检查网络是否连接", Toast.LENGTH_SHORT).show();
//                getMvpView().getActivity().finish();
//            }
//        });
//    }
}
