package com.bjjy.mainclient.phone.event;


/**
 * Created by wyc on 9/4/15.
 */
public class UpdateEvent {
    public boolean isRefresh;
    private static final String TAG = "UpdateEvent";
    public UpdateEvent(boolean isRefresh)
    {
        this.isRefresh = isRefresh;
    }
}
