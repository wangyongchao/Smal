package com.bjjy.mainclient.phone.view.exam.activity.ExamList;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bjjy.mainclient.phone.view.exam.activity.exampaperlist.bean.MyAllCourse;
import com.bjjy.mainclient.phone.view.exam.activity.exampaperlist.bean.MyCourse;
import com.bjjy.mainclient.phone.view.play.utils.SharedPrefHelper;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.remote.ApiClient;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by wyc on 2016/5/10.
 */
public class ExamPracticePercenter extends BasePersenter<ExamPracticeView> {
    private MyAllCourse myAllCourse;
    public ArrayList<MyCourse> courseList = new ArrayList<>();
    private String userId;

    @Override
    public void getData() {
        userId = SharedPrefHelper.getInstance(getMvpView().context()).getUserId();
        courseList = new ArrayList<>();
        getMvpView().showLoading();
        HashMap<String, String> params = new HashMap<>();
        apiModel.getData(ApiClient.getClient().getExamPracticeList(params));
    }

    @Override
    public void setData(String str) {
        try {
            getMvpView().hideLoading();
            JSONObject object = JSON.parseObject(str);
            String body = object.getString("body");
            getMvpView().getPTREListView().onRefreshComplete();
           /* BaseBean baseBean = JSON.parseObject(str, BaseBean.class);
            if (baseBean == null) {
                mEmptyLayout.showError();
                return;
            }*/

           /* int code = object.getString("result");
            if (code != 1) {
                if (code == 9) {
//                        showLoginOtherPlace();
                } else {
                    mEmptyLayout.showError();
                }
                return;
            }*/
            getMvpView().getEmptyLayout().showContentView();
            myAllCourse = JSON.parseObject(body, MyAllCourse.class);
            courseList.clear();
            courseList.addAll(myAllCourse.getCourseTypeList());
            MyAllCourse newMyAllCourse = new MyAllCourse();
            newMyAllCourse = myAllCourse;
            newMyAllCourse.setUserId(userId);
            newMyAllCourse.setAllDta(body);
         /* MyAllCourse dbMyAllCourse = myCourseDB.findByUserIdAndYear(userId, currYear.getYearName());
            if (dbMyAllCourse != null) {
                myCourseDB.delete(dbMyAllCourse);
            }
            myCourseDB.insert(newMyAllCourse);*/
            getMvpView().initAdapter();
            getMvpView().refreshAdapter();
            getMvpView().setAllCollapsed();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    @Override
    public void onError(Exception e) {
        getMvpView().showError("获取数据失败");
    }
}
