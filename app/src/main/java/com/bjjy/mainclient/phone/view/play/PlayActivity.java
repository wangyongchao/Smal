package com.bjjy.mainclient.phone.view.play;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bjjy.mainclient.persenter.PlayPersenter;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.AppContext;
import com.bjjy.mainclient.phone.app.BaseFragmentActivity;
import com.bjjy.mainclient.phone.event.FromPlayEvent;
import com.bjjy.mainclient.phone.utils.NetworkUtil;
import com.bjjy.mainclient.phone.view.play.adapter.PlayContentFrameAdapter;
import com.bjjy.mainclient.phone.view.play.fragment.FragmentPlayer;
import com.bjjy.mainclient.phone.view.play.utils.PrePlayVideoCenter;
import com.bjjy.mainclient.phone.widget.DialogManager;
import com.bjjy.mainclient.phone.widget.EmptyViewLayout;
import com.dongao.libs.dongaoumengpushlib.constants.PushConstants;
import com.dongao.mainclient.model.bean.play.CourseDetail;
import com.dongao.mainclient.model.bean.play.CourseWare;
import com.umeng.analytics.MobclickAgent;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 */
public class PlayActivity extends BaseFragmentActivity implements PlayView{
    @Bind(R.id.play_menu_courselist)
    TextView play_menu_courselist;
    @Bind(R.id.play_menu_coursehandout)
    TextView play_menu_coursehandout;
    @Bind(R.id.play_menu_coursedetail)
    TextView play_menu_coursedetail;
    @Bind(R.id.play_menu_coursedetail_line)
    View play_menu_coursedetail_line;
    @Bind(R.id.play_menu_courselist_line)
    View view_courseListLine;
    @Bind(R.id.play_menu_coursehandout_line)
    View view_courseHandoutLine;
    @Bind(R.id.play_viewpger)
    ViewPager viewPager;
    private View view_now_showLine;
    @Bind(R.id.container)
    LinearLayout linearLayout_container;
    private EmptyViewLayout emptyViewLayout;

    private String playingCWId = "21";
    private boolean isPlayFromLoacl = false;//是否是播放的本地视频

    private String examId,sectionId;//考前2.0移植需要

    private CourseWare courseWare;//当前正在播放的课节
    public CourseDetail courseDetail;//当前页面课程详情数据
    public List<CourseWare> courseWareList;//当前播放的无章节数据
    private String classId;//班次id

    private FragmentManager fragmentManager;
    private PlayContentFrameAdapter adapter;
    private PrePlayVideoCenter prePlayVideoCenter;
    private PlayPersenter playPersenter;
    public List<Integer> times=new ArrayList<>();
    public List<String> urls=new ArrayList<>();

    //正在播放的视频position
    private int groupPosition;
    private int childPostion;

    private boolean isCanLoadHandOut = true;
    private boolean isOldPlay=false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.play);
        ButterKnife.bind(this);
        classId = getIntent().getStringExtra("classId");
        initView();
        initPlayer();
        playPersenter = new PlayPersenter();
        playPersenter.attachView(this);
        initData();
    }

    @Override
    public void initView() {
        emptyViewLayout = new EmptyViewLayout(this,linearLayout_container);
        emptyViewLayout.setErrorButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initData();
            }
        });
        fragmentManager = getSupportFragmentManager();
        view_now_showLine = view_courseListLine;
        adapter = new PlayContentFrameAdapter(fragmentManager);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);
    }

    @Override
    public void initData() {
        examId = getIntent().getStringExtra("examId");
        sectionId = getIntent().getStringExtra("sectionId");
        isPlayFromLoacl = getIntent().getBooleanExtra("isPlayFromLoacl",false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                playPersenter.getData();
            }
        },1000);
    }

    @Override
    public void onClick(View v) {
    }

    private FragmentPlayer player;
    private void initPlayer(){
        Bundle bundle=new Bundle();
//        bundle.putSerializable("courseWare",playingCourseWare);
        player= FragmentPlayer.getInstance(bundle);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.media_play_fragment, player);
        ft.commit();
    }

    @OnClick(R.id.play_menu_coursedetail) void courseIntroduceClick(){
        setAnimation(play_menu_coursedetail_line);
        play_menu_coursehandout.setTextColor(Color.parseColor("#000000"));
        play_menu_coursehandout.setBackgroundColor(getResources().getColor(R.color.gray_back));
        play_menu_courselist.setTextColor(Color.parseColor("#000000"));
        play_menu_courselist.setBackgroundColor(getResources().getColor(R.color.gray_back));
        play_menu_coursedetail.setBackgroundColor(Color.WHITE);
        play_menu_coursedetail.setTextColor(getResources().getColor(R.color.color_primary));
        viewPager.setCurrentItem(0);
    }
    @OnClick(R.id.play_menu_courselist) void courseListClick(){
        setAnimation(view_courseListLine);
        play_menu_coursehandout.setTextColor(Color.parseColor("#000000"));
        play_menu_coursehandout.setBackgroundColor(getResources().getColor(R.color.gray_back));
        play_menu_coursedetail.setTextColor(Color.parseColor("#000000"));
        play_menu_coursedetail.setBackgroundColor(getResources().getColor(R.color.gray_back));
        play_menu_courselist.setBackgroundColor(Color.WHITE);
        play_menu_courselist.setTextColor(getResources().getColor(R.color.color_primary));
        viewPager.setCurrentItem(1);
    }

    @OnClick(R.id.play_menu_coursehandout) void courseHandoutClick() {
        setAnimation(view_courseHandoutLine);
        play_menu_coursehandout.setTextColor(getResources().getColor(R.color.color_primary));
        play_menu_coursehandout.setBackgroundColor(Color.WHITE);
        play_menu_courselist.setTextColor(Color.parseColor("#000000"));
        play_menu_courselist.setBackgroundColor(getResources().getColor(R.color.gray_back));
        play_menu_coursedetail.setTextColor(Color.parseColor("#000000"));
        play_menu_coursedetail.setBackgroundColor(getResources().getColor(R.color.gray_back));
        viewPager.setCurrentItem(2);
        MobclickAgent.onEvent(PlayActivity.this, PushConstants.PLAY_HANDOUT);
    }

    /**
     * 设置课程详情，课程目录，课程讲义切换时下方蓝色线展开和消失时的动画
     * @param lineView
     */
    private void setAnimation(View lineView){
        if(view_now_showLine != lineView) {
            Animation animation = AnimationUtils.loadAnimation(PlayActivity.this, R.anim.play_menu_line_out);
            view_now_showLine.startAnimation(animation);
            view_now_showLine.setVisibility(View.INVISIBLE);
            view_now_showLine = lineView;
            view_now_showLine.setVisibility(View.VISIBLE);
            Animation animation1 = AnimationUtils.loadAnimation(PlayActivity.this, R.anim.play_menu_line_in);
            view_now_showLine.startAnimation(animation1);
        }
    }

    @Override
    public void onBackPressed() {
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            player.onBack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void showData(CourseDetail courseDetail,String lastVideoId) {
    }

    /**
     * 传入视频的Url,进行播放
     * @param courseWare
     */
    public void playVedio(CourseWare courseWare,int groupPosition,int childPostion){
        if(player.mVideoView.isPlaying()){
            player.mVideoView.stopPlayback();
            player.pb.setVisibility(View.VISIBLE);
        }
        this.groupPosition = groupPosition;
        this.childPostion = childPostion;
        playingCWId = courseWare.getCwId();
        if (this.courseWare == null || !this.courseWare.getCwId().equals(courseWare.getCwId())) {
            startPlayVideo(courseWare);
        } else {
            if (isPlaying()) {
                pauseVideo();
            }else {
                startVideo();
            }
        }
    }

    /**
     * 当前是否在播放视频
     * @return
     */
    public boolean isPlaying(){
        return player.mVideoView.isPlaying();
    }

    public void pauseVideo(){
        player.mVideoView.pause();
    }

    public void startVideo(){
        player.mVideoView.start();
    }

    /**
     * 获取当前页面是否是竖屏模式
     * @return
     */
    public boolean isPortrait(){
        Configuration cf = getResources().getConfiguration();
        int ori = cf.orientation;
        if (ori == cf.ORIENTATION_LANDSCAPE)
            return false;
        else
            return true;
    }

    @Override
    public void isOldPlay(boolean isOld) {
        isOldPlay=isOld;
    }

    public void startPlayVideo(CourseWare courseWare){
//        isOldPlay=true;//TODO
//        CourseWare couseWare11 = courseWareDB.find(SharedPrefHelper.getInstance().getUserId(), courseDetail.getCwCode(), courseWare.getVideoID(), SharedPrefHelper.getInstance().getCurYear());
//        if(couseWare11!=null && !TextUtils.isEmpty(couseWare11.getEffectiveStudyTime()))
//            courseWare.setEffectiveStudyTime(couseWare11.getEffectiveStudyTime());
//        if(isOldPlay){
//            playNew(courseWare);
//        }else{
            if(NetworkUtil.isNetworkAvailable(this)){
//                playPersenter.getPlayUrl(courseWare);// TODO: 2017/11/5  
                playPersenter.getPlayKey(courseWare);
            }else{
                playNew(courseWare);
            }
//        }
    }

    public void playNew(CourseWare courseWare){
        if(isNull){
            return;
        }
        this.courseWare = courseWare;
        if(examId!=null && !examId.equals("") &&
                sectionId!=null && !sectionId.equals("")){
            courseWare.setExamId(examId);
            courseWare.setSectionId(sectionId);
        }
        player.playVedio(courseWare);
        if(courseWare.getMobileLectureUrl().startsWith("http")){
            adapter.setCourseHandOutUrl(courseWare.getMobileLectureUrl(), true);
        }else{
            adapter.setCourseHandOutUrl("", true);
        }
        if (courseWare.getCwDesc().startsWith("http")){
            adapter.setCourseInfoUrl(courseWare.getCwDesc());
        }else{
            adapter.setCourseInfoUrl("");
        }
        adapter.notifyPlayStatus();
    }

    public void setLectureUrl(String lectureUrl){
        adapter.setCourseHandOutUrl(lectureUrl, true);
    }

    /**
     * 查询当前播放的视频
     */
    private void checkPlayCourseWare(String lastVideoId){

        if(lastVideoId == null || lastVideoId.equals("")) {//当前课程无最后一次播放视频
            //一级页面点击进入则默认播放第一个视频
            if(courseDetail!=null){
                courseWare = courseDetail.getVideoDtos().get(0);
            }else{
                courseWare = courseWareList.get(0);
            }
            groupPosition = 0;
            childPostion = 0;
        }else{//当前课程有最后一次播放视频
            if(courseDetail!=null){
                    for(int j=0;j<courseDetail.getVideoDtos().size();j++){
                        if(courseDetail.getVideoDtos().get(j).getCwId().
                                equals(lastVideoId)){
                            groupPosition = 0;
                            childPostion = j;
                            courseWare = courseDetail.getVideoDtos().get(j);
                        }
                    }
            }else{
                for(int i=0;i<courseWareList.size();i++){
                    if(courseWareList.get(i).getCwId().equals(lastVideoId)){
                        courseWare = courseWareList.get(i);
                        childPostion = i;
                    }
                }
            }
        }
        if (courseWare==null)courseWare = courseWareList.get(0);

//        if(courseWare.getCwId()==null)
//            courseWare.setCourseId(courseDetail.getCwCode());
//        courseWare.setYears(SharedPrefHelper.getInstance().getCurYear());
    }

    /**
     * 当前正在播放的视频id
     * @return
     */
    public String getPlayingCWId(){
        return courseWare.getCwId();
    }
    public void setPlayingCWId(String cwId){
        this.playingCWId = cwId;
    }

    public CourseWare getPlayingCw(){
        return  courseWare;
    }

    @Override
    public void showLoading() {
        emptyViewLayout.showLoading();
    }

    @Override
    public void hideLoading() {
        emptyViewLayout.showContentView();
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        emptyViewLayout.showError();
        adapter.setCourseListLoadDataError();
        adapter.setCourseHandOutUrl("", false);
    }

    @Override
    public Context context() {
        return this;
    }

    @Override
    public Intent getPLayIntent() {
        return getIntent();
    }

    @Override
    public void showNetError() {
        emptyViewLayout.showNetErrorView();
    }

    @Override
    public CourseWare getCurrentCourseWare() {
        return courseWare;
    }

    @Override
    public void showOtherLogin() {

    }

    @Override
    public void showCwData(List<CourseWare> courseWareList,String lastVideoId) {
        if(courseWareList.size()>0){
            this.courseWareList = courseWareList;
            checkPlayCourseWare(lastVideoId);
            playingCWId = courseWare.getCwId();
            adapter.setCwListData(courseWareList);
            viewPager.setCurrentItem(1);
            startPlayVideo(courseWare);
        }else{
            emptyViewLayout.showEmpty();
            DialogManager.showMsgWithCloseAction(this, "课程暂未开放", "提示", new DialogManager.CustomDialogCloseListener() {
                @Override
                public void yesClick() {
                    finish();
                }

                @Override
                public void noClick() {

                }
            });
        }

    }



    public void checkMachine(DownloadView downloadView){
        playPersenter.checkMachineSign(downloadView);
    }
    /**
     * 当前详情数据是否还有章节
     * @return
     */
    public boolean isHaveChapter(){
        return courseDetail!=null && courseDetail.getSectionFlag().equals("1");
    }

    @Override
    public void loadError() {
//        DialogManager.showMsgWithCloseAction(this, "数据加载失败", "提示", new DialogManager.CustomDialogCloseListener() {
//            @Override
//            public void yesClick() {
//                finish();
//            }
//
//            @Override
//            public void noClick() {
//
//            }
//        });
        emptyViewLayout.showError();
    }

    public int getPlayingGroupPosition(){
        return groupPosition;
    }

    public int getPlayingChildPosition(){
        return childPostion;
    }

    boolean isNull;
    @Override
    public void onDestroy() {
        super.onDestroy();
        isNull=true;
        AppContext.getInstance().setHandlerDetail(null);
//        playPersenter.postOperat();
        EventBus.getDefault().post(new FromPlayEvent());
    }

    public void analysisDNS(CourseWare courseWare){
//        playPersenter.analysisIp(courseWare);// TODO: 2017/11/6  
    }

    private static final int EXAM_DOWNLOAD = 13;
    public void downloadExam(){ //下载随堂练习
        player.handler.sendEmptyMessage(EXAM_DOWNLOAD);
    }


    public void notifyPlayStatus(){
        adapter.notifyPlayStatus();
    }

    public void setIsPlayFromLocal(boolean isPlayFromLoacl){
        this.isPlayFromLoacl = isPlayFromLoacl;
    }

    public boolean isPlayFromLoacl(){
        return isPlayFromLoacl;
    }

    @Override
    public boolean isPlayLocal() {
        return isPlayFromLoacl;
    }

    public void setCourseHandOutTime(long timePosition){
        adapter.setCourseHandOutFragmentWebviewProgress(timePosition);
    }

    public void webSeekTo(long time){
        player.seekToSec(time);
    }

    public boolean isPlayMainVideo(){
        boolean isPlay = playPersenter.isPlayMainVideo();
        playPersenter.setPlayCwId();
        return isPlay;
    }

    @Override
    public void playError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    public boolean isCanLoadHandOut(){
        return  isCanLoadHandOut;
    }

    public void setCanLoadHandOut(boolean isCanLoadHandOut){
        this.isCanLoadHandOut = isCanLoadHandOut;
    }

}
