package com.bjjy.mainclient.phone.view.main;


import java.io.Serializable;

public class MsgValue implements Serializable{


    private String createTime;
    private String createTimeStr;


    private String id;
    private String nextCwId;
    private String classId;
    private String clazzName;

    private String examId;
    private String subjectId;
    private String sectionId;
    private String courseId;
    private boolean practiceHaveOrNo;

    private String title1;
    private String title2;
    private String title3;
    private int accomplished;
    private String secondEndTime;
    private int contentType;

    private String examType;
    private int userIstoDo;

    private Practice practice;


    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateTimeStr() {
        return createTimeStr;
    }

    public void setCreateTimeStr(String createTimeStr) {
        this.createTimeStr = createTimeStr;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNextCwId() {
        return nextCwId;
    }

    public void setNextCwId(String nextCwId) {
        this.nextCwId = nextCwId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getExamId() {
        return examId;
    }

    public void setExamId(String examId) {
        this.examId = examId;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    public boolean isPracticeHaveOrNo() {
        return practiceHaveOrNo;
    }

    public void setPracticeHaveOrNo(boolean practiceHaveOrNo) {
        this.practiceHaveOrNo = practiceHaveOrNo;
    }

    public String getTitle1() {
        return title1;
    }

    public void setTitle1(String title1) {
        this.title1 = title1;
    }

    public String getTitle2() {
        return title2;
    }

    public void setTitle2(String title2) {
        this.title2 = title2;
    }

    public String getTitle3() {
        return title3;
    }

    public void setTitle3(String title3) {
        this.title3 = title3;
    }

    public int getAccomplished() {
        return accomplished;
    }

    public void setAccomplished(int accomplished) {
        this.accomplished = accomplished;
    }

    public String getSecondEndTime() {
        return secondEndTime;
    }

    public void setSecondEndTime(String secondEndTime) {
        this.secondEndTime = secondEndTime;
    }

    public int getContentType() {
        return contentType;
    }

    public void setContentType(int contentType) {
        this.contentType = contentType;
    }

    public Practice getPractice() {
        return practice;
    }

    public void setPractice(Practice practice) {
        this.practice = practice;
    }

    public String getClazzName() {
        return clazzName;
    }

    public void setClazzName(String clazzName) {
        this.clazzName = clazzName;
    }

    public String getExamType() {
        return examType;
    }

    public void setExamType(String examType) {
        this.examType = examType;
    }

    public int getUserIstoDo() {
        return userIstoDo;
    }

    public void setUserIstoDo(int userIstoDo) {
        this.userIstoDo = userIstoDo;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }
}
