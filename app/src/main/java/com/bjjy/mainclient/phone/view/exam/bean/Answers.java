package com.bjjy.mainclient.phone.view.exam.bean;

import java.io.Serializable;

/**
 * 提交的答案个体
 * Created by wangyongchao on 2015/7/14 0014.
 */

public class Answers implements Serializable {
    private String questionId;
    private String paperQuestionId;//试卷题目id
    private String answer;
    //2016.11.29新加
//    private int isRight;
    //2017.04.10
//    private String score;


    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getAnswerLocal() {
        return answer;
    }

    public void setAnswerLocal(String answerLocal) {
        this.answer = answerLocal;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getPaperQuestionId() {
        return paperQuestionId;
    }

    public void setPaperQuestionId(String paperQuestionId) {
        this.paperQuestionId = paperQuestionId;
    }
}
