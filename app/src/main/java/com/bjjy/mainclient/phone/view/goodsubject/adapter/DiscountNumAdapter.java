package com.bjjy.mainclient.phone.view.goodsubject.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dongao.mainclient.model.bean.courselect.GoodsInfos;
import com.bjjy.mainclient.phone.R;

import java.util.List;

/**
 * Created by dell on 2016/4/8.
 */
public class DiscountNumAdapter extends BaseAdapter{

    private Context mContext;
    private List<GoodsInfos> mList;
    public DiscountNumAdapter(Context context) {
        super();
        this.mContext=context;
    }

    public void setList(List<GoodsInfos> list){
        this.mList=list;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = View.inflate(mContext, R.layout.selected_item, null);
            holder.title = (TextView) convertView.findViewById(R.id.tv_cousetitle);
            holder.price = (TextView) convertView.findViewById(R.id.tv_courseprice);
            holder.disCountPrice = (TextView) convertView.findViewById(R.id.tv_discountprice);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.title.setText(mList.get(position).getName());
        holder.price.setText("¥"+mList.get(position).getPrice());
        holder.disCountPrice.setText("¥"+mList.get(position).getDiscountPrice());
        return convertView;
    }

    class ViewHolder {
        TextView title;
        TextView disCountPrice;
        TextView price;
    }
}
