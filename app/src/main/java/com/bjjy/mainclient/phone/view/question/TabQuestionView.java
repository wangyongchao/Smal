package com.bjjy.mainclient.phone.view.question;

import com.dongao.mainclient.model.bean.question.QuestionKonwLedge;
import com.dongao.mainclient.model.bean.question.QuestionRecomm;
import com.dongao.mainclient.model.bean.question.QustionBook;
import com.dongao.mainclient.model.mvp.MvpView;

import java.util.List;

/**
 * Created by fengzongwei on 2016/12/1 0001.
 */
public interface TabQuestionView extends MvpView {
    void showBookList(List<QustionBook> qustionBooks);
    void showKnowLedgeList(List<QuestionKonwLedge> questionKonwLedges);
    void showRecommQuesList(List<QuestionRecomm> questionRecommLis);
    void bookListLoadError();
    void knowledgepointLoadError();
    void recommQuesListLoadError();
}
