package com.bjjy.mainclient.phone.event;

import com.dongao.mainclient.model.bean.course.CourseWare;

/**
 * Created by wyc on 9/4/15.
 */
public class CourseEvent {
    public CourseWare courseWare;
    private static final String TAG = "CourseEvent";
    public CourseEvent(CourseWare courseWare)
    {
        this.courseWare = courseWare;
    }
}
