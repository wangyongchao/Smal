package com.bjjy.mainclient.phone.view.daytest.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.bjjy.mainclient.persenter.DayTestFragmentPersenter;
import com.bjjy.mainclient.phone.app.BaseFragment;
import com.bjjy.mainclient.phone.view.daytest.DayTestActivity;
import com.bjjy.mainclient.phone.view.exam.adapter.RelevantPointAdapter;
import com.bjjy.mainclient.phone.view.exam.dict.ExamTypeEnum;
import com.bjjy.mainclient.phone.view.exam.fragment.QuestionFragmentView;
import com.bjjy.mainclient.phone.widget.EmptyViewLayout;
import com.dongao.mainclient.model.bean.daytest.DayExercise;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.utils.NetworkUtil;
import com.bjjy.mainclient.phone.view.exam.adapter.OptionAdapter;
import com.bjjy.mainclient.phone.view.exam.view.NoScrollListview;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by fengzongwei on 2016/5/24 0024.
 */
public class DayTestFragment extends BaseFragment implements QuestionFragmentView {
    @Bind(R.id.day_test_fragment_question_type_tv)
    TextView textView_questionType;//题目类型
    @Bind(R.id.day_test_fragment_time_tv)
    TextView textView_time;//当前选择的日期
    @Bind(R.id.day_test_fragment_last_tv)
    TextView textView_last;//上一题
    @Bind(R.id.day_test_fragment_next_tv)
    TextView textView_next;//下一题
    @Bind(R.id.day_test_fragment_answer_tv)
    TextView textView_showAnswer;//显示答案
    @Bind(R.id.day_test_fragment_body)
    LinearLayout linearLayout_body;
    @Bind(R.id.day_test_fragment_flag_view)
    RelativeLayout relativeLayout_flag;

    /*************显示问题相关布局***************/
    @Bind(R.id.ll_solution_thinking)
    LinearLayout ll_solution_thinking;
    @Bind(R.id.exam_question_my_ll)
    LinearLayout exam_question_my_ll;
    @Bind(R.id.ll_answer)
    LinearLayout ll_answer;
    @Bind(R.id.rl_answer_htwv)
    RelativeLayout rl_answer_htwv;
    @Bind(R.id.ll_answer_normal)
    LinearLayout ll_answer_normal;
    @Bind(R.id.tv_question_answer)
    HtmlTextView tv_question_answer;
    @Bind(R.id.wv_question_answer_wv)
    WebView wv_question_answer_wv;
    @Bind(R.id.ll_all_analysis)
    LinearLayout ll_all_analysis;
    @Bind(R.id.ll_point)
    LinearLayout ll_point;
    @Bind(R.id.sv_examination)
    ScrollView sv_examination;
    @Bind(R.id.lv_options)
    NoScrollListview lv_options;
    @Bind(R.id.lv_exam_question_relate_knowledge)
    NoScrollListview lv_exam_question_relate_knowledge;
    @Bind(R.id.ll_relevant_point)
    LinearLayout ll_relevant_point;
    @Bind(R.id.tv_question_name)
    HtmlTextView tv_question_name;
    @Bind(R.id.wv_question_name)
    WebView wv_question_name;
    @Bind(R.id.exam_pager_quiz_analyze_layout)
    LinearLayout exam_pager_quiz_analyze_layout;
    @Bind(R.id.exam_question_local_answer)
    TextView exam_question_local_answer;
    @Bind(R.id.exam_question_real_answer)
    TextView exam_question_real_answer;
    @Bind(R.id.exam_question_quiz_analyze_wv)
    WebView exam_question_quiz_analyze_wv;
    @Bind(R.id.ll_relevant_answer)
    LinearLayout ll_relevant_answer;
    @Bind(R.id.solution_thinking_wv)
    WebView solution_thinking_wv;
    @Bind(R.id.exam_question_quiz_analyze)
    HtmlTextView exam_question_quiz_analyze;

    private EmptyViewLayout emptyViewLayout;

    private RelevantPointAdapter relevantPointAdapter;
    private OptionAdapter adapter;
    private  final String encoding = "utf-8";
    private  final String mimeType = "text/html";
    private DayTestFragmentPersenter questionFragmentPersenter;
    private View baseView;
    private DayTestActivity dayTestActivity;
    private long currentTime;//当前时间
    private long oneDayTime = 86400000;//一天的毫秒值
    private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

    private int index;//当前fragment在viewpager中的角标

    public DayTestFragment(){

    }

    private boolean isShowAnswerNow = false;//当前是否显示了答案

    @Override
    public View onCreateView(LayoutInflater inflater,@Nullable ViewGroup container,@Nullable Bundle savedInstanceState) {
        dayTestActivity = (DayTestActivity)getActivity();
        index = getArguments().getInt("index");
        baseView = inflater.inflate(R.layout.day_test_fragment, container, false);
        ButterKnife.bind(this,baseView);
        currentTime = dayTestActivity.getCurrentTime(index);
        textView_time.setText(format.format(new Date(currentTime)));
        questionFragmentPersenter = new DayTestFragmentPersenter();
        questionFragmentPersenter.attachView(this);
        initView();
        initData();
        return baseView;
    }


    @Override
    public void initView() {
        emptyViewLayout = new EmptyViewLayout(dayTestActivity,linearLayout_body);
        ViewGroup viewGroup = (ViewGroup)LayoutInflater.from(getActivity()).inflate(R.layout.app_view_error_daytest, null);
        emptyViewLayout.setEmptyView(viewGroup);
        emptyViewLayout.setErrorButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(index == dayTestActivity.getCurrentIndex())
                    initData();
            }
        });
        textView_last.setOnClickListener(this);
        textView_next.setOnClickListener(this);
        textView_showAnswer.setOnClickListener(this);
        solution_thinking_wv.getSettings().setAppCacheEnabled(false);
        solution_thinking_wv.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        solution_thinking_wv.getSettings().setDefaultTextEncodingName("UTF-8");
        sv_examination.smoothScrollTo(0, 20);
        lv_options.setFocusable(false);
        lv_exam_question_relate_knowledge.setEnabled(false);
        wv_question_name.setFocusable(false);
        wv_question_name.getSettings().setAppCacheEnabled(false);
        wv_question_name.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        wv_question_name.getSettings().setDefaultTextEncodingName("UTF-8");
        exam_question_quiz_analyze_wv.getSettings().setAppCacheEnabled(false);
        exam_question_quiz_analyze_wv.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        exam_question_quiz_analyze_wv.getSettings().setDefaultTextEncodingName("UTF-8");
        wv_question_answer_wv.getSettings().setAppCacheEnabled(false);
        wv_question_answer_wv.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        wv_question_answer_wv.getSettings().setDefaultTextEncodingName("UTF-8");

    }

    @Override
    public void initData() {
        if(questionFragmentPersenter!=null && emptyViewLayout!=null)
        if(NetworkUtil.isNetworkAvailable(getActivity()))
            questionFragmentPersenter.getData();
        else
            emptyViewLayout.showNetErrorView();
    }

    /**
     * 刷新数据
     */
    public void freshData(long time){
        if(questionFragmentPersenter!=null){
            currentTime = time;
            questionFragmentPersenter.getData();
            textView_time.setText(format.format(new Date(currentTime)));
        }
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.day_test_fragment_answer_tv:
//                if(exam_pager_quiz_analyze_layout.getVisibility() == View.VISIBLE){
//                    return;
//                }
                if(questionFragmentPersenter.question==null || questionFragmentPersenter.question.getQuestionId() == null || questionFragmentPersenter.question.getQuestionId().equals("")){
                    return;
                }
                if(questionFragmentPersenter.question.getUserAnswer()!=null && !questionFragmentPersenter.question.getUserAnswer().equals("")){
                    showAnalyze(!isShowAnswerNow);
                    saveAnswer();
                }else
                    Toast.makeText(getActivity(),"此题您还未作答",Toast.LENGTH_SHORT).show();
                break;
            case R.id.day_test_fragment_last_tv:
                currentTime = currentTime - oneDayTime;
                dayTestActivity.setCurrentTime(index,currentTime);
                textView_time.setText(format.format(new Date(currentTime)));
                questionFragmentPersenter.getData();
                break;
            case R.id.day_test_fragment_next_tv:
                currentTime = currentTime + oneDayTime;
                dayTestActivity.setCurrentTime(index,currentTime);
                textView_time.setText(format.format(new Date(currentTime)));
                questionFragmentPersenter.getData();
                break;
        }
    }

    @Override
    public Bundle getArgumentData() {
        return null;
    }

    @Override
    public Intent getTheIntent() {
        return null;
    }

    @Override
    public void setNSListviewIsWebview(boolean isWebview) {
        lv_options.setIsWebView(isWebview);
    }

    @Override
    public void setLocalAnswer(String localAnswer) {
        if(localAnswer.equals("2")){
            exam_question_local_answer.setText("错");
            return;
        }
        if(localAnswer.equals("1")){
            exam_question_local_answer.setText("对");
            return;
        }
        exam_question_local_answer.setText(localAnswer);
    }

    @Override
    public void setRealAnswer(boolean choiceTypeIsNormal, boolean isTextView, String realAnswer) {
        if (choiceTypeIsNormal){
            exam_question_real_answer.setText(realAnswer);
            rl_answer_htwv.setVisibility(View.GONE);
            ll_answer_normal.setVisibility(View.VISIBLE);
            ll_point.setVisibility(View.GONE);
        }else{
            ll_point.setVisibility(View.VISIBLE);
            rl_answer_htwv.setVisibility(View.VISIBLE);
            ll_answer_normal.setVisibility(View.GONE);
            if (isTextView){
                tv_question_answer.setHtmlFromString("<font color='#808080' style='font-size:18px;'>" + (realAnswer) + "</font>",new HtmlTextView.RemoteImageGetter());
                tv_question_answer.setVisibility(View.VISIBLE);
                wv_question_answer_wv.setVisibility(View.GONE);
            }else{
                wv_question_answer_wv.loadDataWithBaseURL("", "<font color='#808080' style='font-size:18px;'>" + (realAnswer) + "</font>", mimeType, encoding, "");
                wv_question_answer_wv.setVisibility(View.VISIBLE);
                tv_question_answer.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void setAnalyzeIsWebview(boolean isWebview, String analyze) {
        if (isWebview){
            exam_question_quiz_analyze_wv.loadDataWithBaseURL("", "<font color='#808080' style='font-size:18px;'>" + (analyze) + "</font>", mimeType, encoding, "");
            exam_question_quiz_analyze_wv.setVisibility(View.VISIBLE);
            exam_question_quiz_analyze.setVisibility(View.GONE);
        }else{
            exam_question_quiz_analyze.setHtmlFromString("<font color='#808080' style='font-size:18px;'>" + (analyze) + "</font>", new HtmlTextView.RemoteImageGetter());
            exam_question_quiz_analyze.setVisibility(View.VISIBLE);
            exam_question_quiz_analyze_wv.setVisibility(View.GONE);
        }
    }

    @Override
    public void showRlevantPoint(boolean show) {
        if (show){
            ll_relevant_point.setVisibility(View.VISIBLE);
        }else{
            ll_relevant_point.setVisibility(View.GONE);
        }
    }

    /**
     * 显示下面显示答案和解析的布局
     * @param showAnalyze
     */
    @Override
    public void showAnalyze(boolean showAnalyze) {
        isShowAnswerNow = showAnalyze;
        if(isShowAnswerNow)
            textView_showAnswer.setText("解析");
        else
            textView_showAnswer.setText("解析");
        if (showAnalyze){
            exam_pager_quiz_analyze_layout.setVisibility(View.VISIBLE);
            lv_options.setEnabled(false);
            setLocalAnswer(questionFragmentPersenter.question.getUserAnswer());
        }else{
            lv_options.setEnabled(true);
            exam_pager_quiz_analyze_layout.setVisibility(View.GONE);
        }
    }

    @Override
    public void showNormalQuestionOrNot(boolean isNormal) {
        sv_examination.setVisibility(View.VISIBLE);
    }

    @Override
    public void setNSListViewHeight(ViewGroup.LayoutParams params) {
        lv_options.setLayoutParams(params);
    }

    @Override
    public ListView getOptionListView() {
        return lv_options;
    }

    @Override
    public void setOptionListViewIsEnable(boolean isEnable) {
        lv_options.setEnabled(isEnable);
    }

    @Override
    public void setNotNormalTipShow(boolean isShow) {
        if (isShow){
            ll_point.setVisibility(View.VISIBLE);
        }else{
            ll_point.setVisibility(View.GONE);
        }
    }

    @Override
    public void setShowAllAnalyze(boolean isShow) {
        if (isShow)
            exam_pager_quiz_analyze_layout.setVisibility(View.VISIBLE);
        else
            exam_pager_quiz_analyze_layout.setVisibility(View.GONE);
    }

    @Override
    public void setRelavantAnswers(boolean isShow) {
        if (isShow){
            ll_relevant_answer.setVisibility(View.VISIBLE);
        }else {
            ll_relevant_answer.setVisibility(View.GONE);
        }
    }

    @Override
    public void setQuestionTypeName(boolean titleIsWebview,String name) {
        textView_questionType.setText(name);
    }

    @Override
    public void setQuestionTitleName(boolean isNotWebview, String name) {
        if (isNotWebview){
            tv_question_name.setHtmlFromString("<font color='#808080' style='font-size:18px;'>" + (name) + "</font>",new HtmlTextView.RemoteImageGetter());
            tv_question_name.setVisibility(View.VISIBLE);
            wv_question_name.setVisibility(View.GONE);
        }else{
            wv_question_name.loadDataWithBaseURL("", "<font color='#808080' style='font-size:18px;'>" + (name) + "</font>", mimeType, encoding, "");
            wv_question_name.setVisibility(View.VISIBLE);
            tv_question_name.setVisibility(View.GONE);
        }
    }

    @Override
    public void setCompreQuestionTypeName(boolean titleIsWebview,String name) {

    }

    @Override
    public void setCompreQuestionTitleName(boolean isNotWebview, String name) {
    }

    @Override
    public void refreshOptionAdapter() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void initChildFragmentAdapter() {

    }

    @Override
    public void setChildVPPosition(int index) {

    }

    @Override
    public void setCompreHeight(RelativeLayout.LayoutParams lp) {

    }

    @Override
    public void setCustomRelativeLayoutHeight(RelativeLayout.LayoutParams lp) {

    }

    @Override
    public void setChildVPData() {

    }

    @Override
    public void showSolutions(boolean isShow, String solutions) {
        if (!isShow){
            ll_solution_thinking.setVisibility(View.GONE);
        }else{
            solution_thinking_wv.loadDataWithBaseURL("", "<font color='#808080' style='font-size:18px;'>" + (solutions) + "</font>", mimeType, encoding, "");
            ll_solution_thinking.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showLoading() {
        emptyViewLayout.showLoading();
    }

    @Override
    public void hideLoading() {
        emptyViewLayout.showContentView();
//        dayTestActivity.dismissProgressDialog();
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {
//        Toast.makeText(getActivity(),message,Toast.LENGTH_SHORT).show();
        emptyViewLayout.showError();
//        dayTestActivity.hideEmptyLayout();
    }

    @Override
    public Context context() {
        return getActivity();
    }

    @Override
    public String currentDate() {
        return format.format(new Date(currentTime));
    }

    @Override
    public String currentSubjectId() {
        return dayTestActivity.getIndexSubjectId(index);
    }

    @Override
    public String currentExamid() {
        return dayTestActivity.getIndexExamId(index);
    }

    @Override
    public void showData() {
        dayTestActivity.hideEmptyLayout();
//        emptyViewLayout.showContentView();
        relevantPointAdapter=new RelevantPointAdapter(getActivity(),questionFragmentPersenter.question.getPointList());
        lv_exam_question_relate_knowledge.setAdapter(relevantPointAdapter);
        if (!(questionFragmentPersenter.question.getOptionList()==null||questionFragmentPersenter.question.getOptionList().size()==0)){
            adapter = new OptionAdapter(getActivity(), questionFragmentPersenter.question.getOptionList(), lv_options, questionFragmentPersenter.index, questionFragmentPersenter.question,
                    new OptionAdapter.heightListener() {
                        @Override
                        public void onheightChange(int height, int position) {
                        }
                    });
            lv_options.setAdapter(adapter);
        }
        if ( questionFragmentPersenter.question.getChoiceType()== ExamTypeEnum.EXAM_TYPE_DANXUAN.getId()||questionFragmentPersenter.question.getChoiceType()== ExamTypeEnum.EXAM_TYPE_PANDUAN.getId()){//单选
            lv_options.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        }else{//非单选
            lv_options.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        }
        lv_options.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                questionFragmentPersenter.setListViewItemClick(position);
            }
        });
        questionFragmentPersenter.setListViewHeightBasedOnChildren(lv_options);
    }

    @Override
    public void showNoData() {
        emptyViewLayout.showEmpty();
    }

    /**
     * 保存数据
     */
    public void saveAnswer(){
        if(questionFragmentPersenter.question.getRealAnswer()==null ||
                questionFragmentPersenter.question.getRealAnswer().equals("")
                || questionFragmentPersenter.question.getUserAnswer()==null ||
                questionFragmentPersenter.question.getUserAnswer().equals("")){
            return;
        }
        DayExercise dayExercise = new DayExercise();
        dayExercise.setContent(JSON.toJSONString(questionFragmentPersenter.question));
        dayExercise.setSubjectId(questionFragmentPersenter.question.getSubjectId());
        dayExercise.setExamId(questionFragmentPersenter.question.getExamId());
        dayExercise.setQuestionId(questionFragmentPersenter.question.getQuestionId());
        dayExercise.setUserId(SharedPrefHelper.getInstance(getActivity()).getUserId() + "");
        dayExercise.setDataTime(format.format(new Date(currentTime)));
        dayExercise.setAnswerLocal(questionFragmentPersenter.question.getUserAnswer());
        if(questionFragmentPersenter.question.getUserAnswer().
                equals(questionFragmentPersenter.question.getRealAnswer())){
            dayExercise.setAnswerRight("1");
        }else{
            dayExercise.setAnswerRight("0");
        }
        dayExercise.setChoiceType(questionFragmentPersenter.question.getChoiceType()+"");
        questionFragmentPersenter.checkDb();
        if(questionFragmentPersenter.dayExercise==null)
            questionFragmentPersenter.insertDayTest(dayExercise);
        else {
            dayExercise.setDbId(questionFragmentPersenter.dayExercise.getDbId());
            questionFragmentPersenter.updateDayTest(dayExercise);
        }
    }

}
