package com.bjjy.mainclient.phone.view.classroom.course;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dongao.mainclient.model.bean.course.CourseItem;
import com.bjjy.mainclient.phone.R;

import java.util.ArrayList;

public class CourseListAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<CourseItem> mlist;

	public CourseListAdapter(Context context) {
		super();
		this.context = context;
	}

	public void setList(ArrayList<CourseItem> mlist) {
		this.mlist = mlist;
	}

	public boolean isEmpty() {
		return mlist.isEmpty();
	}

	public void remove(int index) {
		if (index > -1 && index < mlist.size()) {
			mlist.remove(index);
			notifyDataSetChanged();
		}
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mlist.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final ViewHolder viewHolder;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(R.layout.course_fragment_course_item, null);
			viewHolder.iv = (ImageView) convertView.findViewById(R.id.icon);
			viewHolder.title = (TextView) convertView.findViewById(R.id.title);
			viewHolder.subTitle = (TextView) convertView.findViewById(R.id.subTitle);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		CourseItem courseItem = mlist.get(position);
		/*Picasso.with(context)
				.load(courseItem.getImageUrl())
				.placeholder(R.drawable.ic_launcher)
				.error(R.drawable.ic_error)
				.into(viewHolder.iv);*/

		viewHolder.title.setText(courseItem.getName());
		viewHolder.subTitle.setText("开始学习 : "+ " "+ courseItem.getLastChapterNo() + " "+ courseItem.getLastCourseWareName()+"");

		/*viewHolder.iv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(context, PlayActivity.class);
				context.startActivity(intent);
			}
		});*/
		return convertView;
	}

	public class ViewHolder {
		ImageView iv;
		TextView title;
		TextView subTitle;
	}
}
