package com.bjjy.mainclient.phone.widget.coverflow;


public interface HintView {

	void initView(int length, int gravity);

	void setCurrent(int current);
}

