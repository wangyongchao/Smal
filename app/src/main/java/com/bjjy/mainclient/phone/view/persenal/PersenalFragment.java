package com.bjjy.mainclient.phone.view.persenal;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.bjjy.mainclient.persenter.PersenalPersenter;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.AppContext;
import com.bjjy.mainclient.phone.app.BaseFragment;
import com.bjjy.mainclient.phone.download.DownloadExcutor;
import com.bjjy.mainclient.phone.download.db.DownloadDB;
import com.bjjy.mainclient.phone.event.LogOutEvent;
import com.bjjy.mainclient.phone.event.LoginSuccessEvent;
import com.bjjy.mainclient.phone.event.NewMsgNotification;
import com.bjjy.mainclient.phone.event.PositionedEvent;
import com.bjjy.mainclient.phone.event.PushMsgNotification;
import com.bjjy.mainclient.phone.view.book.BookListActivity;
import com.bjjy.mainclient.phone.view.persenal.widget.PullToZoomBase;
import com.bjjy.mainclient.phone.view.persenal.widget.PullToZoomScrollViewEx;
import com.bjjy.mainclient.phone.view.setting.SettingActivity;
import com.bjjy.mainclient.phone.view.setting.WebViewActivity;
import com.bjjy.mainclient.phone.view.setting.cache.CacheActivity;
import com.bjjy.mainclient.phone.view.setting.myfavs.MyFavouritesActivity;
import com.bjjy.mainclient.phone.view.studybar.message.MyMessageActivity;
import com.bjjy.mainclient.phone.view.user.LoginNewActivity;
import com.bjjy.mainclient.phone.view.zb.utils.GlideUtils;
import com.bjjy.mainclient.phone.widget.DialogManager;
import com.dongao.mainclient.core.util.MD5Util;
import com.dongao.mainclient.core.util.NetUtils;
import com.dongao.mainclient.core.util.StringUtil;
import com.dongao.mainclient.model.bean.user.UserInfo;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.mvp.ApiModel;
import com.dongao.mainclient.model.mvp.ResultListener;
import com.dongao.mainclient.model.remote.ApiService;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by fengzongwei on 2016/6/2 0002.
 */
public class PersenalFragment extends BaseFragment implements PersenalView {
	@Bind(R.id.scroll_view)
	PullToZoomScrollViewEx pullToZoomListView;
	private ProgressBar progressBar;

	TextView persenal_name;
	TextView textView_scoal;
	ImageView persenal_header_loginicon;
	TextView persenal_position;//定位
	private View view;

	private DownloadDB db;

	private int zoomHeight;
	private PersenalPersenter persenalPersenter;
	private Handler handler = new Handler() {

	};

	private boolean mHasLoadedOnce = false;

	private String userId;
	private ImageView main_push_notify_iv;

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		if (this.isVisible()) {
			// we check that the fragment is becoming visible
			if (isVisibleToUser && !mHasLoadedOnce) {
				// async http request here
				mHasLoadedOnce = true;
				initData();
			} else if (isVisibleToUser && mHasLoadedOnce && userId != SharedPrefHelper.getInstance(getActivity()).getUserId()) {
				userId = SharedPrefHelper.getInstance(getActivity()).getUserId();
				initData();
			}
		}
		super.setUserVisibleHint(isVisibleToUser);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		EventBus.getDefault().register(this);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.persenal, container, false);
		persenalPersenter = new PersenalPersenter();
		persenalPersenter.attachView(this);
		ButterKnife.bind(this, view);
		initView();
		db = new DownloadDB(getActivity());
		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		if (mHasLoadedOnce && userId != SharedPrefHelper.getInstance(getActivity()).getUserId()) {
			userId = SharedPrefHelper.getInstance(getActivity()).getUserId();
			initData();
		}
	}

	@Override
	public void initView() {

		pullToZoomListView.setOnPullZoomListener(new PullToZoomBase.OnPullZoomListener() {
			@Override
			public void onPullZooming(int newScrollValue) {
				if (Math.abs(newScrollValue) > zoomHeight)
					zoomHeight = Math.abs(newScrollValue);
			}

			@Override
			public void onPullZoomEnd() {
				if (zoomHeight > 200 && SharedPrefHelper.getInstance(getActivity()).isLogin()) {
				}
				zoomHeight = 0;
			}
		});
		loadViewForCode();
	}

	private void loadViewForCode() {
		View headView = LayoutInflater.from(getActivity()).inflate(R.layout.persenal_header, null, false);
		persenal_header_loginicon = (ImageView) headView.findViewById(R.id.persenal_header_loginicon);
		persenal_position = (TextView) headView.findViewById(R.id.persenal_position);
		if (SharedPrefHelper.getInstance(getActivity()).getUserPosition() != null && !
				SharedPrefHelper.getInstance(getActivity()).getUserPosition().equals(""))
			persenal_position.setText(SharedPrefHelper.getInstance(getActivity()).getUserCode());
		persenal_header_loginicon.setOnClickListener(this);
		persenal_name = (TextView) headView.findViewById(R.id.persenal_name);
		textView_scoal = (TextView) headView.findViewById(R.id.persenal_scoal);
		View zoomView = LayoutInflater.from(getActivity()).inflate(R.layout.persenal_zoom, null, false);
		View contentView = LayoutInflater.from(getActivity()).inflate(R.layout.persenal_content, null, false);
		contentView.findViewById(R.id.persenal_order_body).setOnClickListener(this);
		contentView.findViewById(R.id.persenal_collection_body).setOnClickListener(this);
		contentView.findViewById(R.id.persenal_download_body).setOnClickListener(this);
		contentView.findViewById(R.id.persenal_setting_body).setOnClickListener(this);
		contentView.findViewById(R.id.persenal_book_body).setOnClickListener(this);
		contentView.findViewById(R.id.persenal_lixian).setOnClickListener(this);
		contentView.findViewById(R.id.persenal_selectsubject_body).setOnClickListener(this);
		contentView.findViewById(R.id.persenal_about_body).setOnClickListener(this);
		main_push_notify_iv = (ImageView) contentView.findViewById(R.id.main_push_notify_iv);
		if (SharedPrefHelper.getInstance(getActivity()).isShowStudybarNotify()) {
			main_push_notify_iv.setVisibility(View.VISIBLE);
		} else {
			main_push_notify_iv.setVisibility(View.INVISIBLE);
		}
		main_push_notify_iv.setVisibility(View.INVISIBLE);
		progressBar = (ProgressBar) headView.findViewById(R.id.persenal_pb);
		pullToZoomListView.setHeaderView(headView);
		pullToZoomListView.setZoomView(zoomView);
		pullToZoomListView.setScrollContentView(contentView);
	}

	@Override
	public void initData() {
		userId = SharedPrefHelper.getInstance(getActivity()).getUserId();
		if (SharedPrefHelper.getInstance(getActivity()).isLogin()) {
			if (!SharedPrefHelper.getInstance(getActivity()).getUserInfo().equals("")) {
				UserInfo userInfo = JSON.parseObject(SharedPrefHelper.getInstance(getActivity()).getUserInfo(), UserInfo.class);
				show(userInfo);
			} else {
                persenalPersenter.getData();
			}
		} else {
			persenal_name.setText("暂未登录");
			textView_scoal.setText("");
			persenal_position.setText("");
		}
		setUserImage();
	}
	
	private void setUserImage(){
		if (SharedPrefHelper.getInstance(getActivity()).isLogin()){
			String userImage=SharedPrefHelper.getInstance(getActivity()).getUserImage();
			if (!StringUtil.isEmpty(userImage)){
				GlideUtils.loadUserImage(getActivity(),persenal_header_loginicon,userImage);
			}else{
				persenal_header_loginicon.setImageResource(R.drawable.persenal_logined);
			}
		}else{
			persenal_header_loginicon.setImageResource(R.drawable.persenal_logined);
		}

	}

	@Override
	protected void loginSuccessed() {
		progressBar.setVisibility(View.VISIBLE);
		persenal_header_loginicon.setImageResource(R.drawable.persenal_logined);
		persenalPersenter.getData();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.persenal_about_body:
				Intent about = new Intent(getActivity(), AboutActivity.class);
				startActivity(about);
//                Intent intent11 = new Intent(getActivity(), WebViewActivity.class);
//                intent11.putExtra(com.dongao.mainclient.model.common.Constants.APP_WEBVIEW_TITLE,"关于");
//                intent11.putExtra(com.dongao.mainclient.model.common.Constants.APP_WEBVIEW_URL,"http://p.m.bjsteach.com/appInfo/about.html");
//                startActivity(intent11);
				break;
			case R.id.persenal_header_loginicon:
				if (!SharedPrefHelper.getInstance(getActivity()).isLogin()) {
					Intent intent = new Intent(getActivity(), LoginNewActivity.class);
					// intent.putExtra(Constants.LOGIN_TYPE_KEY,Constants.LOGIN_TYPE_PERSENAL_HEAD_IMG);
					getActivity().startActivity(intent);
				} else {
					DialogManager.showNormalDialog(getActivity(), "确定退出登录", "提示", "取消", "确定", new DialogManager.CustomDialogCloseListener() {
						@Override
						public void yesClick() {
							textView_scoal.setText("");
							persenal_name.setText("暂未登录");
							persenal_position.setText("");
							//persenal_name.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
							persenal_header_loginicon.setImageResource(R.drawable.persenal_logined);
							setAliasForUM();
							//暂停下载
							AppContext.getInstance().isStart = 0;
							DownloadExcutor.getInstance(getActivity()).setFlag(false);
							db.updateState(Constants.STATE_Pause);

							SharedPrefHelper.getInstance(getActivity()).setIsLogin(false);
							SharedPrefHelper.getInstance(getActivity()).setUserId("0");
							//persenal_name.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
//                        startActivity(intent1);
							EventBus.getDefault().post(new LogOutEvent(1));
						}

						@Override
						public void noClick() {

						}
					});
				}
				break;
			case R.id.persenal_order_body:
				if (SharedPrefHelper.getInstance(getActivity()).isLogin()) {
					Intent intent_order = new Intent(getActivity(), WebViewActivity.class);
					intent_order.putExtra(Constants.APP_WEBVIEW_TITLE, "我的订单");
					//intent_order.putExtra(Constants.APP_WEBVIEW_URL, "http://172.16.208.156/videoAct/to_video.html");
					//intent_order.putExtra(Constants.APP_WEBVIEW_URL, "http://172.16.208.19/jstc/jstc/views");
					//intent_order.putExtra(Constants.APP_WEBVIEW_URL,"http://p.m.test.com/appInfo/to_order.html");
					//intent_order.putExtra(Constants.APP_WEBVIEW_URL,"http://172.16.200.22/appInfo/to_order.html");//我的订单页面
					intent_order.putExtra(Constants.APP_WEBVIEW_URL, ApiService.ORDER_URL);//我的订单页面
					String userId1 = SharedPrefHelper.getInstance(getActivity()).getUserId();
					String userId = SharedPrefHelper.getInstance(getActivity()).getUserId() + Constants.USER_MD5_YAN;
					String signstr = MD5Util.encrypt(userId);
					String url = ApiService.ORDER_URL + "?userId=" + userId1 + "&signstr=" + signstr;
					intent_order.putExtra(Constants.APP_WEBVIEW_URL, url);
					startActivity(intent_order);
				} else {
					Intent intent = new Intent(getActivity(), LoginNewActivity.class);
					// intent.putExtra(Constants.LOGIN_TYPE_KEY,Constants.LOGIN_TYPE_PERSENAL_DINGDAN);
					startActivity(intent);
				}
				break;
			case R.id.persenal_collection_body:
				if (SharedPrefHelper.getInstance(getActivity()).isLogin()) {
					Intent myfavs = new Intent(getActivity(), MyFavouritesActivity.class);
					startActivity(myfavs);
				} else {
					Intent intent = new Intent(getActivity(), LoginNewActivity.class);
					intent.putExtra(Constants.LOGIN_TYPE_KEY, Constants.LOGIN_TYPE_PERSENAL_COLLECTION);
					startActivity(intent);
				}
				break;
			case R.id.persenal_download_body:
				if (SharedPrefHelper.getInstance(getActivity()).isLogin()) {
//                    Intent cache=new Intent(getActivity(),StudyBarActivity.class);
					Intent cache = new Intent(getActivity(), MyMessageActivity.class);
					startActivity(cache);
				} else {
					Intent intent = new Intent(getActivity(), LoginNewActivity.class);
					// intent.putExtra(Constants.LOGIN_TYPE_KEY,Constants.LOGIN_TYPE_PERSENAL_DOWNLOAD);
					startActivity(intent);
				}
				break;
			case R.id.persenal_setting_body:
			   /* Intent intent_order2 = new Intent(getActivity(), WebViewActivity.class);
                intent_order2.putExtra(Constants.APP_WEBVIEW_TITLE,"我的订单");
                //intent_order2.putExtra(Constants.APP_WEBVIEW_URL, "http://172.16.208.156/appInfo/to_order.html");
                //intent_order.putExtra(Constants.APP_WEBVIEW_URL, "http://172.16.208.19/jstc/jstc/views");
                intent_order2.putExtra(Constants.APP_WEBVIEW_URL,ApiService.ORDER_URL);//我的订单页面
                startActivity(intent_order2);*/
				Intent setting = new Intent(getActivity(), SettingActivity.class);
				startActivity(setting);
				break;
			case R.id.persenal_book_body:
				if (SharedPrefHelper.getInstance(getActivity()).isLogin()) {
					Intent bookList = new Intent(getActivity(), BookListActivity.class);
					startActivity(bookList);
				} else {
					Intent intent = new Intent(getActivity(), LoginNewActivity.class);
					intent.putExtra(Constants.LOGIN_TYPE_KEY, Constants.LOGIN_TYPE_MY);
					startActivity(intent);
				}
				break;
			case R.id.persenal_selectsubject_body:
				Intent intent = new Intent(getActivity(), WebViewActivity.class);
				intent.putExtra(Constants.APP_WEBVIEW_TITLE, "选课购买");
				//intent.putExtra(Constants.APP_WEBVIEW_URL, "http://172.16.208.19/jstc/jstc/views");
				// intent.putExtra(Constants.APP_WEBVIEW_URL, "http://172.16.200.22/zsfa/to_swx.html");
				//intent.putExtra(Constants.APP_WEBVIEW_URL, "http://172.16.200.22/appInfo/toApp.html");
				intent.putExtra(Constants.APP_WEBVIEW_URL, ApiService.SELECT_COURSE_URL);
				startActivity(intent);
				break;
			case R.id.persenal_lixian:
				goLixian();
				break;
		}
	}

	private void goLixian() {
//		startActivity(new Intent(getActivity(), DownloadChooseActivity.class));
		startActivity(new Intent(getActivity(), CacheActivity.class));
	}

	@Override
	public void show(UserInfo userInfo) {
		pullToZoomListView.setZoomEnabled(true);
		progressBar.setVisibility(View.GONE);
		persenal_header_loginicon.setImageResource(R.drawable.persenal_logined);
		textView_scoal.setText(" 成长值 0 ");
		//persenal_name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_me_lv1, 0, 0, 0);
		persenal_name.setText(SharedPrefHelper.getInstance(getActivity()).getLoginUsername());
		persenal_position.setText(SharedPrefHelper.getInstance(getActivity()).getUserCode());
	}

	public void setAliasForUM() {
		String device_token = SharedPrefHelper.getInstance(getActivity()).getUmDevicetoken();
		if (device_token != null && !device_token.isEmpty()) {
			if (SharedPrefHelper.getInstance(getActivity()).isLogin()) {
				if (NetUtils.checkNet(getActivity()).isAvailable()) {
					ApiModel apiModel = new ApiModel(new ResultListener() {
						@Override
						public void onSuccess(String json) {

						}

						@Override
						public void onError(Exception e) {

						}
					});
					//  apiModel.getData(ApiClient.getClient().CommitDevicetoken(ParamsUtils.getInstance(getActivity()).commitDeviceToken(device_token,0)));

				}
			}
		}
	}

	@Override
	public void showLoading() {

	}

	@Override
	public void hideLoading() {

	}

	@Override
	public void showRetry() {

	}

	@Override
	public void hideRetry() {

	}

	@Subscribe
	public void onEventAsync(LogOutEvent event) {
	}

	@Subscribe
	public void onEventAsync(PositionedEvent event) {
		persenal_position.setText(SharedPrefHelper.getInstance(getActivity()).getUserCode());
	}

	@Subscribe
	public void onEventAsync(LoginSuccessEvent event) {
		loginSuccessed();
	}

	@Subscribe
	public void onEventMainThread(PushMsgNotification pushMsgNotification) {
		changTab(pushMsgNotification.isRead);
	}

	@Subscribe
	public void onEventMainThread(NewMsgNotification newMsgNotification) {
		changTab(newMsgNotification.isRead);
	}

	private void changTab(boolean isRead) {
		if (isRead) {
			main_push_notify_iv.setVisibility(View.INVISIBLE);
		} else {
			main_push_notify_iv.setVisibility(View.VISIBLE);
		}
		main_push_notify_iv.setVisibility(View.INVISIBLE);
	}

	@Override
	public void showError(String message) {
		pullToZoomListView.setZoomEnabled(true);
		progressBar.setVisibility(View.GONE);
		Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
	}

	@Override
	public Context context() {
		return getActivity();
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		EventBus.getDefault().unregister(this);// 反注册EventBus
	}
	
	
}
