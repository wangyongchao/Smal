package com.bjjy.mainclient.phone.view.payment;


import android.content.Intent;

import com.dongao.mainclient.model.mvp.MvpView;

/**
 * @author wyc
 * 支付UI 
 */
public interface PayWayView extends MvpView {
  String payWay();//支付方式
  void intent();
  void out();
  String orderNumber();
  Intent getTheIntent();
  void totalPrice(String price);
  void finishActivity();
          
}
