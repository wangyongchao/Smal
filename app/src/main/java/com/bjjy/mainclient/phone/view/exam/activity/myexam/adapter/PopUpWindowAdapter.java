package com.bjjy.mainclient.phone.view.exam.activity.myexam.adapter;

import android.content.Context;
import android.view.View;

import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.view.download.local.bean.YearInfo;
import com.bjjy.mainclient.phone.view.studybar.Constant;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.ta.utdid2.android.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 *  Created by wyc on 2017/03/23.
 */
public class PopUpWindowAdapter extends BaseQuickAdapter<YearInfo, BaseViewHolder> {
    private Context context;
    private List<YearInfo> data = new ArrayList<>();
    private MainTypeItemClick mainTypeItemClick;
    private String checkedPosition;

    public PopUpWindowAdapter() {
        super(R.layout.query_add_pop_item);
    }
    public PopUpWindowAdapter(Context context) {
        super(R.layout.query_add_pop_item);
        this.context = context;
    }
    

    public PopUpWindowAdapter(Context context, List<YearInfo> data) {
        super(R.layout.query_add_pop_item, data);
        this.context = context;
        this.data = data;
       
    }
    
    public void setRvItemClick(MainTypeItemClick mainTypeItemClick){
        this.mainTypeItemClick = mainTypeItemClick;
    }

    @Override
    protected void convert(final BaseViewHolder helper, final YearInfo item) {
        helper.getConvertView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mainTypeItemClick!=null){
                    int position=helper.getPosition();
                    mainTypeItemClick.itemClick(0, Constant.MAIN_FRAGMENT_CLICK_TYPE1, 0, position);
                }
            }
        });
        if (!StringUtils.isEmpty(checkedPosition)){
            if (Integer.valueOf(checkedPosition)==helper.getPosition()){
                helper.setTextColor(R.id.question_select_exm_lv_item_tv,context.getResources().getColor(R.color.color_primary));
            }else{
                helper.setTextColor(R.id.question_select_exm_lv_item_tv,context.getResources().getColor(R.color.text_color_primary_dark));
            }
        }else{
            helper.setTextColor(R.id.question_select_exm_lv_item_tv,context.getResources().getColor(R.color.text_color_primary_dark));

        }
        helper.setText(R.id.question_select_exm_lv_item_tv, item.getYearName());
    }

    public void setContext(Context context){
        this.context=context;
    }

    public List<YearInfo> getResults() {
        return data;
    }

    public void setResults(List<YearInfo> data) {
        this.data = data;
    }

    public void setClickListener(MainTypeItemClick mainTypeItemClick){
        this.mainTypeItemClick=mainTypeItemClick;
    }
    
    public void checkedPosition(String checkedPosition){
        this.checkedPosition=checkedPosition;
    }
    

    public interface MainTypeItemClick {
        void itemClick(int mainPosition, int itemType, int type, int position);
    }
}
