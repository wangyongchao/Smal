package com.bjjy.mainclient.phone.view.studybar.questionsultion;

import android.content.Intent;
import com.alibaba.fastjson.JSON;
import com.bjjy.mainclient.phone.view.question.MyQuestionListActivity;
import com.bjjy.mainclient.phone.view.studybar.Constant;
import com.bjjy.mainclient.phone.view.studybar.questionsultion.bean.QuestionSolution;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.core.util.NetUtils;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ParamsUtils;
import com.dongao.mainclient.model.remote.bean.BaseBean;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.view.studybar.questionsultion.bean.QuestionSolutionListInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by wyc on 2016/5/19.
 */
public class QuestionSolutionPercenter extends BasePersenter<QuestionSolutionView> {

    public List<QuestionSolution> questionSolutionList;
    private String userId;
    public int currentPage = 1;
    public int totalPage = 0;
    private String type;
    private String classId;
    private String title;

    public void initData() {
        userId = SharedPrefHelper.getInstance(getMvpView().context()).getUserId() + "";
        questionSolutionList = new ArrayList<>();
        getData();
    }

    @Override
    public void getData() {
        currentPage = 1;
        Intent intent=getMvpView().getTheIntent();
        type=intent.getIntExtra("type",0)+"";
        classId=intent.getStringExtra("classId");
        title=intent.getStringExtra(Constants.APP_WEBVIEW_TITLE);
        getMvpView().showTopTitle(title);
        getMvpView().showLoading();
        if (NetUtils.checkNet(getMvpView().context()).isAvailable()) {
            getInterData();
        } else {
            getMvpView().showContentView(Constant.VIEW_TYPE_1);
        }
    }

    public void getLoadData() {
        if (getMvpView().isRefreshNow()) {
            getMvpView().initAdapter();
            getMvpView().showContentView(Constant.VIEW_TYPE_0);
            getMvpView().showError(getMvpView().context().getResources().getString(R.string.check_net));
            return;
        }
//        testLoad();
        getMvpView().hideLoading();
        getMvpView().showContentView(Constant.VIEW_TYPE_0);
        if (NetUtils.checkNet(getMvpView().context()).isAvailable()) {
            if (currentPage>totalPage){
                getMvpView().setNoDataMoreShow(true);
                return;
            }
            loadMore(ParamsUtils.getInstance(getMvpView().context()).getQuestionSolution(type, classId, currentPage));
        } else {
            getMvpView().hideLoading();
            getMvpView().showContentView(Constant.VIEW_TYPE_0);
            getMvpView().showError(getMvpView().context().getResources().getString(R.string.check_net));
        }
    }

    private void loadMore(HashMap<String, String> params) {
        Call<String> call = ApiClient.getClient().getQuestionSolution(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    String str = response.body();
                    try {
                        getMvpView().hideLoading();
                        BaseBean baseBean = JSON.parseObject(str, BaseBean.class);
                        if (baseBean == null) {
                            return;
                        } else {
                            if (baseBean.getResult().getCode() != 1000) {
                                if (baseBean.getResult().getCode() == 9) {
                                } else {
//                                    getMvpView().showContentView(Constant.VIEW_TYPE_1);
                                }
                                return;
                            }
                        }
                        getMvpView().showContentView(Constant.VIEW_TYPE_0);
                        currentPage++;
                        String body = baseBean.getBody();
                        QuestionSolutionListInfo questionSolutionListInfo = JSON.parseObject(body, QuestionSolutionListInfo.class);
                        List<QuestionSolution> newList = questionSolutionListInfo.getQasList();
                        if (newList == null) {
                            newList = new ArrayList<>();
                        }
                        questionSolutionList.addAll(newList);
                        getMvpView().initAdapter();
                    } catch (Exception e) {
                        getMvpView().showContentView(Constant.VIEW_TYPE_1);
                    }

                } else {
                    getMvpView().showContentView(Constant.VIEW_TYPE_1);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }




    private void getInterData() {
        apiModel.getData(ApiClient.getClient().getQuestionSolution(ParamsUtils.getInstance(getMvpView().context()).getQuestionSolution(type, classId, currentPage)));
    }

    @Override
    public void setData(String obj) {
        try{
            getMvpView().hideLoading();
            BaseBean baseBean = JSON.parseObject(obj, BaseBean.class);
            if (baseBean == null) {
                getMvpView().showContentView(Constant.VIEW_TYPE_1);
                return;
            } else {
                int result = baseBean.getResult().getCode();
                if (result != 1000) {
                    getMvpView().showContentView(Constant.VIEW_TYPE_3);
                    return;
                }
            }
            questionSolutionList.clear();

            currentPage++;
            String body = baseBean.getBody();
            //设置返回的数据
            QuestionSolutionListInfo questionSolutionListInfo=JSON.parseObject(body, QuestionSolutionListInfo.class);
            List<QuestionSolution> newList = questionSolutionListInfo.getQasList();
            if (newList.size()==0) {
                newList = new ArrayList<>();
                getMvpView().showContentView(Constant.VIEW_TYPE_2);
            }else{
                getMvpView().showContentView(Constant.VIEW_TYPE_0);
            }
            questionSolutionList.addAll(newList);
            if (questionSolutionList.size()<Integer.valueOf(Constants.PAGE_SIZE)){
                getMvpView().setNoDataMoreShow(true);
            }
            getMvpView().initAdapter();
        }catch (Exception e){}
       
    }

    public void setOnItemClick(int position) {
        Intent intent = new Intent(getMvpView().context(), MyQuestionListActivity.class);
        intent.putExtra("questionAnswerId",questionSolutionList.get(position).getQuestionId());
        getMvpView().context().startActivity(intent);
    }

    @Override
    public void onError(Exception e) {
        super.onError(e);
        getMvpView().showContentView(Constant.VIEW_TYPE_1);
    }
}
