package com.bjjy.mainclient.phone.view.home;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bjjy.mainclient.phone.dict.MainTypeEnum;
import com.bjjy.mainclient.phone.view.exam.ExamActivity;
import com.bjjy.mainclient.phone.view.play.PlayActivity;
import com.bjjy.mainclient.phone.widget.timeline.TimeLinesView;
import com.dongao.libs.dongaoumengpushlib.constants.PushConstants;
import com.dongao.mainclient.core.util.DateUtil;
import com.dongao.mainclient.model.bean.home.HomeItem;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.bjjy.mainclient.phone.R;
import com.umeng.analytics.MobclickAgent;

import java.util.List;


public class TimeLineAdapter extends BaseAdapter {


	private List<HomeItem> mlist;
	private Context context;

	public TimeLineAdapter(Context context) {
		this.context = context;
	}

	public void setList(List<HomeItem> mlist) {
		this.mlist = mlist;
	}

	public boolean isEmpty() {
		return mlist.isEmpty();
	}

	public void remove(int index) {
		if (index > -1 && index < mlist.size()) {
			mlist.remove(index);
			notifyDataSetChanged();
		}
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mlist.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(R.layout.home_frgment_item, null);
			holder.title = (TextView) convertView.findViewById(R.id.title);
			holder.subTitle = (TextView) convertView.findViewById(R.id.subTitle);
			holder.subTitle2 = (TextView) convertView.findViewById(R.id.subTitle2);
			holder.date = (TextView) convertView.findViewById(R.id.date);
			holder.mTimelineView = (TimeLinesView) convertView.findViewById(R.id.timeLineView);
			holder.home_fragment_item_course_layout = (LinearLayout) convertView.findViewById(R.id.home_fragment_item_course_layout);
			holder.action = (ImageView) convertView.findViewById(R.id.action_iv);
			holder.action_lianxi = (ImageView) convertView.findViewById(R.id.action_lianxi_iv);
			holder.line = convertView.findViewById(R.id.home_fragment_line);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		final HomeItem homeModel = mlist.get(position);
		switch (homeModel.getContentType()) {
			case 1://录播课
				MainTypeEnum.MAIN_TYPE_COUTSE.getName();
				holder.action_lianxi.setVisibility(View.INVISIBLE);
				holder.action.setVisibility(View.INVISIBLE);
				holder.mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_play_h));
				holder.subTitle.setVisibility(View.VISIBLE);
				holder.subTitle2.setVisibility(View.VISIBLE);
				holder.date.setVisibility(View.VISIBLE);
				holder.line.setVisibility(View.VISIBLE);
				if (mlist.get(position).getCreateTime() != null && !mlist.get(position).getCreateTime().isEmpty()) {
					holder.date.setText(DateUtil.twoDateDistanceForStudyBar(mlist.get(position).getCreateTime()));
				}
				break;
			case 2://直播课
				MainTypeEnum.MAIN_TYPE_KEHOUZUOYE.getName();
				holder.mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_task_n));
				holder.action.setVisibility(View.INVISIBLE);
				holder.action_lianxi.setVisibility(View.INVISIBLE);
				holder.subTitle.setVisibility(View.GONE);
				holder.subTitle2.setVisibility(View.GONE);
				holder.date.setVisibility(View.VISIBLE);
				holder.line.setVisibility(View.VISIBLE);
				if (mlist.get(position).getCreateTime() != null && !mlist.get(position).getCreateTime().isEmpty()) {
					holder.date.setText(DateUtil.twoDateDistanceForStudyBar(mlist.get(position).getCreateTime()));
				}
				break;
			case 3://模拟考
				MainTypeEnum.MAIN_TYPE_LINIANZHENTI.getName();
				holder.mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_task_n));
				holder.action.setVisibility(View.INVISIBLE);
				holder.action_lianxi.setVisibility(View.INVISIBLE);
				holder.subTitle.setVisibility(View.GONE);
				holder.subTitle2.setVisibility(View.GONE);
				holder.date.setVisibility(View.VISIBLE);
				holder.line.setVisibility(View.VISIBLE);
				if (mlist.get(position).getCreateTime() != null && !mlist.get(position).getCreateTime().isEmpty()) {
					holder.date.setText(DateUtil.twoDateDistanceForStudyBar(mlist.get(position).getCreateTime()));
				}
				break;
			case 4:
				MainTypeEnum.MAIN_TYPE_MONISHIJUAN.getName();
				holder.mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_task_n));
				holder.action.setVisibility(View.INVISIBLE);
				holder.action_lianxi.setVisibility(View.INVISIBLE);
				holder.subTitle.setVisibility(View.GONE);
				holder.subTitle2.setVisibility(View.GONE);
				holder.date.setVisibility(View.VISIBLE);
				holder.line.setVisibility(View.VISIBLE);
				if (mlist.get(position).getCreateTime() != null && !mlist.get(position).getCreateTime().isEmpty()) {
					holder.date.setText(DateUtil.twoDateDistanceForStudyBar(mlist.get(position).getCreateTime()));
				}
				break;
			case 5:
				MainTypeEnum.MAIN_TYPE_SUITANGLIANXI.getName();
				holder.mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_task_n));
				holder.action.setVisibility(View.INVISIBLE);
				holder.action_lianxi.setVisibility(View.INVISIBLE);
				holder.subTitle.setVisibility(View.GONE);
				holder.subTitle2.setVisibility(View.GONE);
				holder.date.setVisibility(View.VISIBLE);
				holder.line.setVisibility(View.VISIBLE);
				if (mlist.get(position).getCreateTime() != null && !mlist.get(position).getCreateTime().isEmpty()) {
					holder.date.setText(DateUtil.twoDateDistanceForStudyBar(mlist.get(position).getCreateTime()));
				}
				break;
			case 100:
				MainTypeEnum.MAIN_TYPE_DAY.getName();
				holder.mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_xke));
				holder.title.setText("每日一练");
				holder.action.setVisibility(View.INVISIBLE);
				holder.action_lianxi.setVisibility(View.INVISIBLE);
				holder.subTitle.setVisibility(View.GONE);
				holder.subTitle2.setVisibility(View.GONE);
				holder.line.setVisibility(View.GONE);
				break;
			case 10:
				if (homeModel.getId().equals("1")) {//录
					holder.title.setTextColor(context.getResources().getColor(R.color.main_item_color1));
					holder.mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_xbuy));
					holder.subTitle.setTextColor(context.getResources().getColor(R.color.main_item_color1));
				} else if (homeModel.getId().equals("2")) {//直
					holder.title.setTextColor(context.getResources().getColor(R.color.main_item_color2));
					holder.subTitle.setTextColor(context.getResources().getColor(R.color.main_item_color2));
					holder.mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_hi));
				} else {//模
					holder.title.setTextColor(context.getResources().getColor(R.color.main_item_color3));
					holder.subTitle.setTextColor(context.getResources().getColor(R.color.main_item_color3));
					holder.mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_book));
				}
				MainTypeEnum.MAIN_TYPE_WAIT.getName();
				holder.action.setVisibility(View.INVISIBLE);
				holder.action_lianxi.setVisibility(View.INVISIBLE);
				holder.subTitle.setVisibility(View.VISIBLE);
				holder.subTitle2.setVisibility(View.GONE);
				holder.line.setVisibility(View.GONE);
				holder.date.setVisibility(View.GONE);
				break;
			case 12:
				MainTypeEnum.MAIN_TYPE_SELECT_COURSE.getName();
				holder.mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_play_h));
				holder.action.setVisibility(View.INVISIBLE);
				holder.action_lianxi.setVisibility(View.INVISIBLE);
				holder.subTitle.setVisibility(View.VISIBLE);
				holder.subTitle2.setVisibility(View.GONE);
				holder.line.setVisibility(View.GONE);
				holder.date.setVisibility(View.GONE);
				break;
			case 30:
				MainTypeEnum.MAIN_TYPE_BOOK.getName();
				holder.mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_book));
				holder.action.setVisibility(View.INVISIBLE);
				holder.action_lianxi.setVisibility(View.INVISIBLE);
				holder.subTitle.setVisibility(View.VISIBLE);
				holder.subTitle2.setVisibility(View.GONE);
				holder.line.setVisibility(View.GONE);
				holder.date.setVisibility(View.GONE);
				break;
			default:
				holder.mTimelineView.setTimeLineMarker(context.getResources().getDrawable(R.drawable.ico_xke));
				holder.action_lianxi.setVisibility(View.GONE);
				holder.action.setVisibility(View.GONE);
				holder.line.setVisibility(View.GONE);
				holder.date.setVisibility(View.GONE);
				break;
		}

		holder.home_fragment_item_course_layout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mOnItemClickedListener.OnItemClickedListener(homeModel);
			}
		});

		holder.action.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent_course = new Intent(context, PlayActivity.class);
				intent_course.putExtra(Constants.SUBJECTID, homeModel.getSubjectId());
				intent_course.putExtra(Constants.CLASSID, homeModel.getClassId());
				context.startActivity(intent_course);
				MobclickAgent.onEvent(context, PushConstants.MAIN_TO_PLAY);

			}
		});

		holder.action_lianxi.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent_suitanglianxi = new Intent(context, ExamActivity.class);
				SharedPrefHelper.getInstance(context).setExamTag(Constants.EXAM_TAG_ABILITY);
				intent_suitanglianxi.putExtra(Constants.TYPEID, homeModel.getExamType());
				intent_suitanglianxi.putExtra(Constants.SUBJECTID, homeModel.getSubjectId());
				intent_suitanglianxi.putExtra(Constants.EXAMID, homeModel.getExamId());
				intent_suitanglianxi.putExtra(Constants.EXAMINATIONID, homeModel.getId());
				context.startActivity(intent_suitanglianxi);
				MobclickAgent.onEvent(context, PushConstants.MAIN_TO_EXAM);

			}
		});
		holder.title.setText(homeModel.getTitle1());
		holder.subTitle.setText(homeModel.getTitle2());
		holder.subTitle2.setText(homeModel.getTitle3());
		return convertView;
	}

	private OnItemClickedListener mOnItemClickedListener;

	public void setmOnItemClickedListener(OnItemClickedListener mOnItemClickedListener) {
		this.mOnItemClickedListener = mOnItemClickedListener;
	}

	public static interface OnItemClickedListener {
		void OnItemClickedListener(HomeItem homeItem);
	}

	public class ViewHolder {
		public TextView title;
		public TextView subTitle;
		public TextView subTitle2;
		public TextView date;
		public TimeLinesView mTimelineView;
		public LinearLayout home_fragment_item_course_layout;
		public ImageView action;
		public ImageView action_lianxi;
		public View line;
	}
}
