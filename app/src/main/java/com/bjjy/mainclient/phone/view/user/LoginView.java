package com.bjjy.mainclient.phone.view.user;


import com.dongao.mainclient.model.mvp.MvpView;

/**
 * 登录UI 定义UI 看此UI中有何事件
 */
public interface LoginView extends MvpView {
  String username();
  String password();
  void intent();
}
