package com.bjjy.mainclient.phone.view.question;

import android.content.Intent;

import com.dongao.mainclient.core.camera.bean.PhotoModel;
import com.dongao.mainclient.model.mvp.MvpView;

import java.util.List;

/**
 * Created by dell on 2016/5/7 0007.
 */
public interface AddQuestionView extends MvpView{
    Intent catchIntent();//获取此页面的intent
    String getEditString();//获取输入框中的内容
    String getPageNum();//获取选择的页码
    void notifyAdapter();//通知适配器更新
    void resetAdapter(List<PhotoModel> selected);//重设适配器
    void setGVOnItemClickListener();//给表格布局设置点击监听
    void setCameraViewVisibility(int visibility);//设置可见与不可见
    void showPostImgError();//图片上传失败
    void setBookName(String bookName);//修改教材答疑时显示上部书名
    String getChooseSectioName();//所选择章节的名称
    void showSaveDialog();//显示是否进行为提交修改答疑保存的dialog
    void goneListView();//若当前教材没有章节则隐藏显示章节的控件
}
