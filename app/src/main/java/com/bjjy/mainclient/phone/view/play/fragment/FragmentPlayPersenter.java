package com.bjjy.mainclient.phone.view.play.fragment;

import android.app.Activity;
import android.text.TextUtils;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.AppConfig;
import com.bjjy.mainclient.phone.download.db.PlayParamsDB;
import com.bjjy.mainclient.phone.utils.NetWorkUtils;
import com.bjjy.mainclient.phone.view.exam.bean.ExamPaper;
import com.bjjy.mainclient.phone.view.exam.db.ExamPaperDB;
import com.bjjy.mainclient.phone.view.play.fragment.uploadBean.PlayUrlBean;
import com.bjjy.mainclient.phone.view.play.utils.Constans;
import com.bjjy.mainclient.phone.widget.DialogManager;
import com.dongao.mainclient.core.util.FileUtil;
import com.dongao.mainclient.model.bean.play.CourseWare;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ParamsUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * 课堂首页Persenter
 */
public class FragmentPlayPersenter extends BasePersenter<FragmentPlayView> {
    private List<Integer> listimes = new ArrayList();
    private String path;
    private List<String> urlist = new ArrayList();
    private String userId, examId, subjectId;

    @Override
    public void attachView(FragmentPlayView mvpView) {
        super.attachView(mvpView);
        examPaperDB = new ExamPaperDB(getMvpView().context());
        playParamsDB = new PlayParamsDB(getMvpView().context());
        userId = SharedPrefHelper.getInstance(getMvpView().context()).getUserId();
        examId = SharedPrefHelper.getInstance(getMvpView().context()).getExamId();
        subjectId = SharedPrefHelper.getInstance(getMvpView().context()).getSubjectId();
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    @Override
    public void getData() {
        //一些业务逻辑的判断 有网没网 是请求网络还是请求数据库
        path = getMvpView().getPath();
        if (TextUtils.isEmpty(path)) {
            return;
        }
        apiModel.getData(ApiClient.getClient().CheckTimes(path));
    }

    @Override
    public void setData(String obj) {
        String[] as = obj.split("\n");
        for (int i = 0; i < as.length; i++) {
            if (!as[i].startsWith("#EXT")) {
                int index = path.lastIndexOf("/") + 1;
                final String fileName = as[i];
                if (fileName.contains(".ts")) {
                    String rootUrl = path.substring(0, index);
                    String url = rootUrl + fileName;
                    urlist.add(url);
                }
            } else if (as[i].startsWith("#EXTINF")) {
                String[] times = as[i].split(":");
                String time = times[1].substring(0, times[1].length() - 1);
                double mins = Double.parseDouble(time);
                int min = (int) mins;
//                int mins = Integer.parseInt(time);
                listimes.add(min);
            }
        }
        getMvpView().showData(listimes, urlist);
    }

    public void upLoadVideos(String json) {
        Call<String> call = ApiClient.getClient().upLoadVideos(ParamsUtils.getInstance(getMvpView().context()).uploadVideos(json));
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    String str = response.body();
                    try {
                        com.alibaba.fastjson.JSONObject json = JSON.parseObject(str);
                        com.alibaba.fastjson.JSONObject check_result = json.getJSONObject("result");
                        if (check_result != null) {
                            String message = check_result.getString("msg");
                            int code = check_result.getInteger("code");
                            getMvpView().setResult(code);
                        }

                    } catch (Exception e) {

                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    private ExamPaper examPaper;
    private ExamPaperDB examPaperDB;
    private String typeId = "1003";


    public void getDownloadUrl(final CourseWare courseWare) {
        Call<String> call = ApiClient.getClient().getPlayUrl(ParamsUtils.getInstance(getMvpView().context()).getPlayInfo(courseWare.getCwId()));
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    String str = response.body();
                    try {
                        JSONObject total = new JSONObject(str);
                        String code = total.optString("code");
                        if (!code.equals("1000")) {
                            Toast.makeText(getMvpView().context(), total.optString("msg"), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        PlayUrlBean playUrlBean = new PlayUrlBean();

                        PlayUrlBean.VideoBean videoBean = new PlayUrlBean.VideoBean();
                        PlayUrlBean.VideoBean.SdBean sdModel = new PlayUrlBean.VideoBean.SdBean();
                        PlayUrlBean.VideoBean.CifBean cifModel = new PlayUrlBean.VideoBean.CifBean();

                        JSONObject body = total.getJSONObject("body");
                        JSONObject video = body.getJSONObject("video");

                        String key = body.optString("cipherkeyDeal");
                        playUrlBean.setCipherkeyDeal(key);

                        JSONObject sd = video.getJSONObject("sd");
                        JSONObject cif = video.getJSONObject("cif");

                        JSONArray three = sd.optJSONArray("1.5");
                        JSONArray two = sd.optJSONArray("1.2");
                        JSONArray one = sd.optJSONArray("1.0");

                        JSONArray cfthree = cif.optJSONArray("1.5");
                        JSONArray cftwo = cif.optJSONArray("1.2");
                        JSONArray cfone = cif.optJSONArray("1.0");

                        /**
                         *设置sd（标清）不同倍速url
                         */
                        List<String> sdthree = new ArrayList();
                        for (int i = 0; i < three.length(); i++) {
                            String rrr = (String) three.get(i);
                            sdthree.add(rrr);
                        }
                        sdModel.setThree(sdthree);

                        List<String> sdtwo = new ArrayList();
                        for (int i = 0; i < two.length(); i++) {
                            String rrr = (String) two.get(i);
                            sdtwo.add(rrr);
                        }
                        sdModel.setTwo(sdtwo);

                        List<String> sdone = new ArrayList();
                        for (int i = 0; i < one.length(); i++) {
                            String rrr = (String) one.get(i);
                            sdone.add(rrr);
                        }
                        sdModel.setOne(sdone);
                        videoBean.setSd(sdModel);

                        /**
                         *设置cif（流畅）不同倍速url
                         */
                        List<String> cf_three = new ArrayList();
                        for (int i = 0; i < cfthree.length(); i++) {
                            String rrr = (String) cfthree.get(i);
                            cf_three.add(rrr);
                        }
                        cifModel.setThree(cf_three);

                        List<String> cf_two = new ArrayList();
                        for (int i = 0; i < cftwo.length(); i++) {
                            String rrr = (String) cftwo.get(i);
                            cf_two.add(rrr);
                        }
                        cifModel.setTwo(cf_two);

                        List<String> cf_one = new ArrayList();
                        for (int i = 0; i < cfone.length(); i++) {
                            String rrr = (String) cfone.get(i);
                            cf_one.add(rrr);
                        }
                        cifModel.setOne(cf_one);
                        videoBean.setCif(cifModel);
                        playUrlBean.setVideo(videoBean);

                        JSONObject cipherkeyVo = body.getJSONObject("cipherkeyVo");
                        String app = cipherkeyVo.optString("app");
                        String type = cipherkeyVo.optString("type");
                        String vid = cipherkeyVo.optString("vid");
                        String vkey = cipherkeyVo.optString("key");
                        String vcode = cipherkeyVo.optString("code");
                        String message = cipherkeyVo.optString("message");
                        String lecture = body.optString("handOut");
                        playParamsDB.add(userId, courseWare.getCwId(), app, type, vid, vkey, vcode, message);

                        courseWare.setMobileLectureUrl(lecture);
                        courseWare.setMobileDownloadUrl(playUrlBean.getVideo().getSd().getOne().get(0));
                        courseWare.setCwBean(JSON.toJSONString(courseWare));
                        CheckSettingNetDownload(courseWare);
                    } catch (Exception e) {
                        getMvpView().playError("analysis failed");
                    }
                } else {
                    getMvpView().playError("数据错误");
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                getMvpView().hideLoading();
                getMvpView().playError("网络错误");
            }
        });
    }

    /**
     * 下载前检查网络
     */
    private void CheckSettingNetDownload(final CourseWare courseWare) {
        final Activity mContext = (Activity) getMvpView().context();
        boolean isDownload = SharedPrefHelper.getInstance(mContext).getIsNoWifiPlayDownload();
        NetWorkUtils type = new NetWorkUtils(mContext);
        if (type.getNetType() == 0) {//无网络
            DialogManager.showMsgDialog(mContext, mContext.getResources().getString(R.string.dialog_message_vedio), mContext.getResources().getString(R.string.dialog_title_download), "确定");
        } else if (type.getNetType() == 2) { //流量
//            if (isDownload) {
                DialogManager.showNormalDialog(mContext, mContext.getResources().getString(R.string.dialog_warnning_vedio), "提示", "取消", "确定", new DialogManager.CustomDialogCloseListener() {
                    @Override
                    public void yesClick() {
                        Toast.makeText(mContext, "已加入下载列表", Toast.LENGTH_SHORT).show();
                        AppConfig.getAppConfig(mContext).download(courseWare);
                    }

                    @Override
                    public void noClick() {
                    }
                });
//            }
        } else if (type.getNetType() == 1) {//wifi
            Toast.makeText(mContext, "已加入下载列表", Toast.LENGTH_SHORT).show();
            AppConfig.getAppConfig(mContext).download(courseWare);
        }
    }


    private PlayParamsDB playParamsDB;
    CourseWare cw;

    private String getPath(CourseWare courseWare) {

        String userId = SharedPrefHelper.getInstance(getMvpView().context()).getUserId();
        String desPath = FileUtil.getDownloadPath(getMvpView().context());

        return desPath + userId + "_" + courseWare.getExamId() + "_" + courseWare.getSubjectId() + "_" + courseWare.getClassId() + "_" + courseWare.getSectionId() + "_" + courseWare.getCwId() + "/" + "playOnline/";

    }

    /**
     * 获取并保存Key到文件中
     *
     * @param urlHead
     */
    public void getKeyTxt(final String urlHead, final String m3u8Path,final String keyText,CourseWare coursew) {

        try {// TODO: 2017/11/12  
            if (keyText!=null){
                File file = new File(getPath(coursew)  + "keyText.txt");
                File file1 = new File(getPath(coursew));
                if (!file.exists()) {
                    if(!file1.exists()){
                        file1.mkdirs();
                    }
                    file.createNewFile();
                    FileWriter fileWriter = new FileWriter(file);
                    fileWriter.write(keyText);
                    fileWriter.flush();
                    fileWriter.close();
                }
            }
          
            String keyUrl = null;
            FileOutputStream fos = null;
            File m3u8File = new File(m3u8Path);
            FileReader fileReader = new FileReader(m3u8File);
            BufferedReader reader11 = new BufferedReader(fileReader);
            String line1 = null;
            StringBuffer stringBuffer1 = new StringBuffer();
            String line_copy = null;
            boolean isNeedKey = false;//是否需要进行加解密
            while ((line1 = reader11.readLine()) != null) {
                line_copy = line1;
                if (line1.contains("EXT-X-KEY"))
                    isNeedKey = true;
                if (line1.contains("EXT-X-KEY") && !line1.contains(Constans.M3U8_KEY_SUB)) {
                    //如果当前视频需要进行解密，则修改m3u8文件中EXT-X-KEY对应的值指向本地key文件，来标示需要进行解密
                    String[] keyPart = line1.split(",");
                    String keyUrll = keyPart[1].split("URI=")[1].trim();
                    keyUrl = keyUrll.substring(1, keyUrll.length() - 1);
                    line_copy = keyPart[0] + Constans.M3U8_KEY_SUB + getKeyPath(coursew) + "keyText.txt\"";// TODO: 2017/11/9  
//					line_copy = keyPart[0] + Constans.M3U8_KEY_SUB +  "video.key\"";
                } else if (line1.contains(".ts") && !line1.contains("http:") && isNeedKey) {
                    line_copy = urlHead + line_copy;//补全ts的网络路径
                }
                stringBuffer1.append(line_copy);
                stringBuffer1.append("\r\n");
            }
            reader11.close();
            FileWriter writer = new FileWriter(m3u8File, false);
            BufferedWriter writer2 = new BufferedWriter(writer);
            writer2.write(stringBuffer1.toString());
            writer2.flush();
            writer2.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private String getKeyPath(CourseWare courseWare) {
        String userId = SharedPrefHelper.getInstance(getMvpView().context()).getUserId();

        return userId + "_" + courseWare.getExamId() + "_" + courseWare.getSubjectId() + "_" + courseWare.getClassId() + "_" + courseWare.getSectionId() + "_" + courseWare.getCwId() + "/" + "playOnline/";
    }
}
