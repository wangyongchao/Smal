package com.bjjy.mainclient.phone.view.setting.cache.adapter;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bjjy.mainclient.phone.view.setting.cache.CachedCourseActivity;
import com.dongao.mainclient.model.bean.play.CourseWare;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.download.db.DownloadDB;
import com.bjjy.mainclient.phone.widget.DialogManager;

import java.util.List;

/**
 * Created by dell on 2016/5/16.
 */
public class CachedCourseAdapter extends BaseAdapter {

    private CachedCourseActivity mContext;
    private List<CourseWare> mList;
    private DownloadDB db;
    private String userId;

    public CachedCourseAdapter(CachedCourseActivity context) {
        super();
        this.mContext = context;
        db=new DownloadDB(context);
        userId= SharedPrefHelper.getInstance(context).getUserId();
    }

    public void setList(List<CourseWare> list) {
        this.mList = list;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        ViewHolder holder;
        if(convertView==null){
            holder = new ViewHolder();
            convertView=View.inflate(mContext, R.layout.cachedcourse_item, null);
            holder.title = (TextView) convertView.findViewById(R.id.tv_title);
            holder.teacher = (TextView) convertView.findViewById(R.id.tv_teacher);
            holder.delete = (ImageView) convertView.findViewById(R.id.course_delete_img);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        holder.title.setText(mList.get(position).getCwName());
        if(TextUtils.isEmpty(mList.get(position).getChapterNo())){
            holder.teacher.setVisibility(View.GONE);
        }else{
            holder.teacher.setText(mList.get(position).getChapterNo());
            holder.teacher.setVisibility(View.VISIBLE);
        }
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogManager.showNormalDialog(mContext, "确定要删除吗", "提示", "取消", "确定",
                        new DialogManager.CustomDialogCloseListener() {
                            @Override
                            public void yesClick() {
                                db.deleteCourseWare(userId, mList.get(position));
                                mList.remove(position);
                                if(mList!=null && mList.size()<1){
                                    mContext.finish();
                                }else{
                                    notifyDataSetChanged();
                                }
                            }

                            @Override
                            public void noClick() {
                            }
                        });
            }
        });
        return convertView;
    }

    class ViewHolder {
        TextView title;
        TextView teacher;
        ImageView delete;
    }
}
