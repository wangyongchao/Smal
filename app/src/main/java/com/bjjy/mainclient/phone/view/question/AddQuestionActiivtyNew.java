package com.bjjy.mainclient.phone.view.question;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dongao.mainclient.model.common.Constants;
import com.bjjy.mainclient.persenter.AddQuestionNewPersenter;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseActivity;
import com.bjjy.mainclient.phone.event.AddQuestionSuccess;
import com.bjjy.mainclient.phone.view.setting.WebViewActivity;

import org.greenrobot.eventbus.EventBus;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by fengzongwei on 2016/11/29 0029.
 */
public class AddQuestionActiivtyNew extends BaseActivity implements AddActivityNewView{
    @Bind(R.id.top_title_left)
    ImageView imageView_back;
    @Bind(R.id.top_title_text)
    TextView textView_title;
    @Bind(R.id.tv_right)
    TextView textView_right;
    @Bind(R.id.question_add_title_et)
    EditText editText_title;
    @Bind(R.id.question_add_title_count_tv)
    TextView textView_title_count;
    @Bind(R.id.question_add_content_et)
    EditText editText_content;
    @Bind(R.id.question_add_content_count_tv)
    TextView textView_content_count;
    @Bind(R.id.add_question_warning)
    TextView textView_warning;

    private final int TITLE_MAX_COUNT = 30,CONTENT_MAX_COUNT = 500;

    private Toast toast;

    private AddQuestionNewPersenter addQuestionNewPersenter;

    private boolean isExamQuestionAsk = false;//是否是试题提问

    private final String WARNING_HINT = "温馨提示：请您仔细阅读提问规范，对不符合条件的提问，我们将按照规范进行处理，感谢您的理解和配合。点击查看完整《提问规范》";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question_add);
        ButterKnife.bind(this);
        initView();
        initData();
    }

    @Override
    public void initView() {
        SpannableStringBuilder spannable = new SpannableStringBuilder(WARNING_HINT);
        spannable.setSpan(new ForegroundColorSpan(Color.parseColor("#ff8d34")),48,WARNING_HINT.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView_warning.setText(spannable);
        imageView_back.setVisibility(View.VISIBLE);
        imageView_back.setImageResource(R.drawable.back);
        textView_title.setText("我要提问");
        textView_right.setText("提交");
        textView_right.setVisibility(View.VISIBLE);
        imageView_back.setOnClickListener(this);
        textView_right.setOnClickListener(this);
        textView_warning.setOnClickListener(this);

        editText_title.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int count_title = editText_title.getText().toString().length();
                textView_title_count.setText(count_title + "/" + TITLE_MAX_COUNT);
            }
        });

        editText_content.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int count_title = editText_content.getText().toString().length();
                textView_content_count.setText(count_title + "/" + CONTENT_MAX_COUNT);
            }
        });

    }

    @Override
    public void initData() {
        isExamQuestionAsk = getIntent().getBooleanExtra("isExamQuestionAsk",false);
        addQuestionNewPersenter = new AddQuestionNewPersenter();
        addQuestionNewPersenter.attachView(this);
    }

    private void showMsg(String msg){
        if(toast == null){
            toast = Toast.makeText(this,msg,Toast.LENGTH_SHORT);
        }
        toast.setText(msg);
        toast.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.add_question_warning:
                Intent intent_warning = new Intent(this, WebViewActivity.class);
                intent_warning.putExtra(Constants.APP_WEBVIEW_TITLE,"提问规范说明");
                intent_warning.putExtra(Constants.APP_WEBVIEW_URL,"http://m.bjsteach.com/app/question/rule.html");
                startActivity(intent_warning);
                break;
            case R.id.top_title_left:
                finish();
                break;
            case R.id.tv_right:
                if(TextUtils.isEmpty(editText_title.getText().toString())){
                    showMsg("请输入提问标题");
                    return;
                }
                if(TextUtils.isEmpty(editText_content.getText().toString())){
                    showMsg("请输入问题描述");
                    return;
                }
                if(editText_content.getText().toString().length()<10){
                    showMsg("提问内容应不少于10个字");
                    return;
                }
                if(isExamQuestionAsk)
                    addQuestionNewPersenter.examQuestionAdd();
                else
                    addQuestionNewPersenter.getData();
                break;
        }
    }

    @Override
    public void addSuccess() {
        EventBus.getDefault().post(new AddQuestionSuccess());
        showError("提问成功！");
        finish();
    }

    @Override
    public Bundle getIntentBundle() {
        return getIntent().getExtras();
    }

    @Override
    public int type() {
        return getIntent().getIntExtra("type",0);
    }

    @Override
    public String title() {
        return editText_title.getText().toString();
    }

    @Override
    public String content() {
        return editText_content.getText().toString();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {
        showMsg(message);
    }

    @Override
    public Context context() {
        return this;
    }
}
