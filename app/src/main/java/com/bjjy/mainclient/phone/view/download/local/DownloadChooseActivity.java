package com.bjjy.mainclient.phone.view.download.local;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseFragmentActivity;
import com.bjjy.mainclient.phone.event.PushMsgNotification;
import com.bjjy.mainclient.phone.view.download.local.SelectYearPopupwindow.SelectYearPopwindow;
import com.bjjy.mainclient.phone.view.download.local.adapter.LocalCourseAdapter;
import com.bjjy.mainclient.phone.view.download.local.bean.YearInfo;
import com.bjjy.mainclient.phone.view.exam.activity.myexam.adapter.PopUpWindowAdapter;
import com.bjjy.mainclient.phone.view.studybar.Constant;
import com.bjjy.mainclient.phone.view.studybar.view.RefreshLayout;
import com.bjjy.mainclient.phone.widget.EmptyViewLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by wyc on 2016/6/6.
 */
public class DownloadChooseActivity extends BaseFragmentActivity implements DownloadChooseView, LocalCourseAdapter.CourseItemClickListener {
    @Bind(R.id.top_title_left)
    ImageView top_title_left;
    @Bind(R.id.top_title_right)
    ImageView top_title_right;
    @Bind(R.id.top_title_text)
    TextView top_title_text;
    @Bind(R.id.content_ll)
    LinearLayout content_ll;
    @Bind(R.id.localcourse_bottom_ll)
    LinearLayout localcourse_bottom_ll;
    @Bind(R.id.localcourse_lixian_tv)
    LinearLayout localcourse_lixian_tv;
    @Bind(R.id.swipe_container)
    SwipeRefreshLayout swipe_container;
    @Bind(R.id.rv_course)
    RecyclerView rv_course;
    @Bind(R.id.localcourse_cancel_tv)
    TextView localcourse_cancel_tv;
    @Bind(R.id.localcourse_delete_tv)
    TextView localcourse_delete_tv;
    @Bind(R.id.tv_right)
    TextView top_title_year;
    private SelectYearPopwindow selectYearPopwindow;
    private LocalCourseAdapter localCourseAdapter;

    @OnClick(R.id.top_title_left) void onBackClick(){
        onBackPressed();
    }
    private DownloadChoosePercenter privateTeacherPercenter;
    private EmptyViewLayout mEmptyLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.localcourse_activity);
        ButterKnife.bind(this);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        privateTeacherPercenter = new DownloadChoosePercenter();
        privateTeacherPercenter.attachView(this);
        initView();
        initData();
    }

    @Override
    public void initView() {
        top_title_left.setVisibility(View.VISIBLE);
        top_title_right.setVisibility(View.VISIBLE);
        top_title_text.setText(getResources().getText(R.string.localcourse_title));
        mEmptyLayout = new EmptyViewLayout(this, content_ll);
        mEmptyLayout.setErrorButtonClickListener(mErrorClickListener);
        mEmptyLayout.setEmptyButtonClickListener(emptyButtonClickListener);
        selectYearPopwindow=new SelectYearPopwindow(this,top_title_right,top_title_right,onItemClickListener);
        rv_course.setLayoutManager(new LinearLayoutManager(rv_course.getContext(), LinearLayoutManager.VERTICAL, false));

    }

    /**
     * 年份的点击事件
     */
    private PopUpWindowAdapter.MainTypeItemClick onItemClickListener=new PopUpWindowAdapter.MainTypeItemClick() {
        @Override
        public void itemClick(int mainPosition, int itemType, int type, int position) {
            privateTeacherPercenter.setOnItemClick(position);
        }
//        @Override
//        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//            if (adapterView.getItemAtPosition(i)!=null){
//                privateTeacherPercenter.setOnItemClick(i);
//            }
//        }
    };

    /**
     * 错误监听
     */
    private View.OnClickListener mErrorClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        }
    };
    /**
     * 无数据监听
     */
    private View.OnClickListener emptyButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //showAppMsg("显示kong");
        }
    };

    @Override
    public void initData() {
        mEmptyLayout.showLoading();
        privateTeacherPercenter.initData();
    }

    @Override
    public void showLoading() {
    }

    @Override
    public void hideLoading() {
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public Context context() {
        return DownloadChooseActivity.this;
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void initAdapter() {
        if (localCourseAdapter==null){
            localCourseAdapter=new LocalCourseAdapter(this,privateTeacherPercenter.courseList,privateTeacherPercenter.isShowCheck);
            localCourseAdapter.setCourseClickListener(this);
            rv_course.setAdapter(localCourseAdapter);
        }else{
            localCourseAdapter.notifyDataSetChanged();
        }
    }

    /**
     * 设置swipe的点击事件的响应
     */
    
    
    
    @Override
    public void showContentView(int type) {
        if (type== Constant.VIEW_TYPE_0){
            mEmptyLayout.showContentView();
        }else  if (type==Constant.VIEW_TYPE_1){
            mEmptyLayout.showNetErrorView();
        }else  if (type==Constant.VIEW_TYPE_2){
            mEmptyLayout.showEmpty();
        }else  if (type==Constant.VIEW_TYPE_3){
            mEmptyLayout.showError();
        }
        hideLoading();
    }

    @Override
    public boolean isRefreshNow() {
        return false;
    }

    @Override
    public RefreshLayout getRefreshLayout() {
        return null;
    }

    @Override
    public Intent getTheIntent() {
        return getIntent();
    }

    @Override
    public void finishActivity() {
        onBackPressed();
    }

    @Override
    public void showCurrentYear(YearInfo currYear) {
        if (currYear.getShowYear()==null||currYear.getShowYear().isEmpty()){
            top_title_year.setText(currYear.getYearName()+"年");
        }else{
            top_title_year.setText(currYear.getShowYear());
        }
    }

    @Override
    public void hideYearPop(boolean isHide) {
        if (isHide&&selectYearPopwindow!=null){
            selectYearPopwindow.dissmissPop();
        }
    }

    @Override
    public void setCancelText(String string) {
        localcourse_cancel_tv.setText(string);
    }

    @Override
    public void setDeleteText(String string) {
        localcourse_delete_tv.setText(string);
    }

    @Override
    public void showTopTitle(String title) {
        top_title_text.setText(title);
    }

    @Override
    public void setNoDataMoreShow(boolean isShow) {
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Subscribe
    public void onEventAsync(PushMsgNotification event) {
        initData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        initData();
    }

    @Override
    public void courseClick(int position, long l) {
        privateTeacherPercenter.courseClick(position,l);
    }
}
