package com.bjjy.mainclient.phone.view.classroom.PaperQuestion;


import com.dongao.mainclient.model.mvp.MvpView;
import com.dongao.mainclient.model.bean.course.Paper;

import java.util.List;

/**
 * UI 定义UI 看此UI中有何事件
 */
public interface PaperFragmentView extends MvpView {
  void setData(List<Paper> papers);
  String getSubjectId();
}
