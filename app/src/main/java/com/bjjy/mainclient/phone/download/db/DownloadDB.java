package com.bjjy.mainclient.phone.download.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.alibaba.fastjson.JSON;
import com.bjjy.mainclient.phone.app.AppContext;
import com.bjjy.mainclient.phone.download.DownloadExcutor;
import com.bjjy.mainclient.phone.download.DownloadTaskManager;
import com.bjjy.mainclient.phone.view.exam.activity.exampaperlist.bean.Course;
import com.bjjy.mainclient.phone.view.setting.cache.domain.GourpDomain;
import com.dongao.mainclient.core.util.FileUtil;
import com.dongao.mainclient.model.bean.course.CoursePlay;
import com.dongao.mainclient.model.bean.play.CourseWare;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.local.SubjectDB;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DownloadDB {
    private HelperDB db;
    private Context mContext;

    public DownloadDB(Context context) {
        mContext = context;
        db = new HelperDB(context);
    }

    /**
     *
     * @param isOld (1为老视频,0为新视频)
     * @return
     */
    public long add(String userId, String examId,String subjectId,String courseId, String videoID,int status, int percent, String desPath, String cwBean, String courseBean,int isOld,String version) {
        if (find(userId, courseId, videoID)) {
            updateState(userId,courseId,videoID, Constants.STATE_Waiting);
            return -2;
        }
        SQLiteDatabase sql = db.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("userId", userId);
        values.put("classId", courseId);
        values.put("videoID", videoID);
        values.put("status", status);
        values.put("percent", percent);
        values.put("desPath", desPath);
//        values.put("courseCount", courseCount);
        values.put("courseWareBean", cwBean);
        values.put("courseBean", courseBean);
        values.put("examId", examId);
        values.put("subjectId", subjectId);
        values.put("isOld", isOld);
        values.put("version", version);
        long result = sql.insert("downloadtable", null, values);
        sql.close();
        return result;
    }

    public long addTransDatas(String userId, String examId,String subjectId,String courseId, String videoID,String sectionId,int status, int percent, String desPath, String cwBean, String courseBean,int isOld) {
        if (find(userId, courseId, videoID)) {
            return -2;
        }
        SQLiteDatabase sql = db.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("userId", userId);
        values.put("classId", courseId);
        values.put("videoID", videoID);
        values.put("status", status);
        values.put("percent", percent);
        values.put("desPath", desPath);
//        values.put("courseCount", courseCount);
        values.put("sectionId",sectionId);
        values.put("courseWareBean", cwBean);
        values.put("courseBean", courseBean);
        values.put("examId", examId);
        values.put("subjectId", subjectId);
        values.put("isOld", isOld);
        long result = sql.insert("downloadtable", null, values);
        sql.close();
        return result;
    }

    public int updatePercent(String userId, String courseId,String videoID, int percent) {

        SQLiteDatabase sql = db.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("percent", percent);
        values.put("status", Constants.STATE_DownLoading);
        int update = sql.update("downloadtable", values, "userId=? and classId=? and videoID=?", new String[]{userId, courseId, videoID});
        sql.close();
        return update;
    }

    public int updatePercentState(String userId, String courseId,String videoID, int percent) {

        SQLiteDatabase sql = db.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("percent", percent);
        values.put("status", Constants.STATE_Error);
        int update = sql.update("downloadtable", values, "userId=? and classId=? and videoID=?", new String[]{userId, courseId, videoID});
        sql.close();
        return update;
    }


    public int updateState(int status) {

        SQLiteDatabase sql = db.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("status", status);
        int update = sql.update("downloadtable", values, "status=? or status=?", new String[]{"1", "5"});
        sql.close();
        return update;
    }

    public int updateState(String userId, String courseId,String videoID, int status) {

        SQLiteDatabase sql = db.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("status", status);
        int update = sql.update("downloadtable", values, "userId=? and classId=? and videoID=?", new String[]{userId, courseId, videoID});
        sql.close();
        return update;
    }

    public int updateIsOld(String userId, String courseId,String videoID,int isOld) {

        SQLiteDatabase sql = db.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("isOld", isOld);
        int update = sql.update("downloadtable", values, "userId=? and classId=? and videoID=?", new String[]{userId, courseId, videoID});
        sql.close();
        return update;
    }

    public int delete(String ids) {

        SQLiteDatabase sql = db.getWritableDatabase();
        int result = sql.delete("coursedown", "id in(" + ids + ")", null);
        sql.close();
        return result;
    }

    public int deletes(String userId) {
        AppContext.getInstance().isStart = 0;
        DownloadExcutor.getInstance(mContext).setFlag(false);
        SQLiteDatabase sql = db.getWritableDatabase();
        int result = sql.delete("downloadtable", "userId=? and status!=?", new String[]{userId, Constants.STATE_DownLoaded + ""});
        sql.close();
        return result;
    }

    public int deletesOldatas() {
        SQLiteDatabase sql = db.getWritableDatabase();
        int result = sql.delete("downloadtable", "status!=?", new String[]{Constants.STATE_DownLoaded + ""});
        sql.close();
        return result;
    }


    public boolean find(String userId, String classId, String videoID) {
        SQLiteDatabase sql = db.getReadableDatabase();
        Cursor cursor = sql.query("downloadtable", null, "userId=? and classId=? and videoID=?",
                new String[]{userId, classId, videoID}, null, null, null);
        if (cursor.moveToNext()) {
            cursor.close();
            sql.close();
            return true;
        } else {
            cursor.close();
            sql.close();
            return false;
        }
    }

    public List<String> findCourseIds(String userId) {
        List<String> courseIds=new ArrayList<>();
        SQLiteDatabase sql = db.getReadableDatabase();
        Cursor cursor = sql.query("downloadtable", null, "userId=?",
                new String[]{userId}, null, null, null);
        while (cursor.moveToNext()) {
            String courseId=cursor.getString(2);
            if(!courseIds.contains(courseId)){
                courseIds.add(courseId);
            }
        }
        cursor.close();
        sql.close();
        return courseIds;
    }

    public boolean CheckIsDownloaded(String userId, String courseId,String videoID) {
        SQLiteDatabase sql = db.getReadableDatabase();
        Cursor cursor = sql.query("downloadtable", null, "userId=? and classId=? and videoID=? and status=?",
                new String[]{userId, courseId, videoID, Constants.STATE_DownLoaded + ""}, null, null, null);
        while (cursor.moveToNext()) {
            return true;
        }
        cursor.close();
        sql.close();
        return false;
    }

    public CourseWare getDownloadedModel(String userId, String courseId,String videoID) {
        CourseWare course=null;
        SQLiteDatabase sql = db.getReadableDatabase();
        Cursor cursor = sql.query("downloadtable", null, "userId=? and classId=? and videoID=?",
                new String[]{userId, courseId, videoID}, null, null, null);
        if (cursor.moveToNext()) {
            course = new CourseWare();
            course.setClassId(cursor.getString(2));
            course.setCwId(cursor.getString(3));
            course.setSubjectId(cursor.getString(12));
            course.setExamId(cursor.getString(14));
            course.setSectionId(cursor.getString(13));
            course.setState(cursor.getInt(5));
        }
        cursor.close();
        sql.close();
        return course;
    }

    public int getState(String userId, String courseId,String videoID) {
        SQLiteDatabase sql = db.getReadableDatabase();
        int state = 0;
        try {
            Cursor cursor = sql.query("downloadtable", null, "userId=? and classId=? and videoID=?",
                    new String[]{userId, courseId, videoID}, null, null, null);
            while (cursor.moveToNext()) {
                state = cursor.getInt(5);
            }
            cursor.close();
            sql.close();
            return state;  
        }catch (Exception e){
            return Constants.STATE_Pause;
        }
        
    }

    public int getIsold(String courseId,String videoID) {
        SQLiteDatabase sql = db.getReadableDatabase();
        int isOld = 0;
        Cursor cursor = sql.query("downloadtable", null, "classId=? and videoID=?",
                new String[]{courseId, videoID}, null, null, null);
        while (cursor.moveToNext()) {
            isOld = cursor.getInt(15);
        }
        cursor.close();
        sql.close();
        return isOld;
    }

    public int getPercent(String userId, String classId,String videoID) {
        SQLiteDatabase sql = db.getReadableDatabase();
        int percent = 0;
        Cursor cursor = sql.query("downloadtable", null, "userId=? and classId=? and videoID=?",
                new String[]{userId, classId, videoID}, null, null, null);
        while (cursor.moveToNext()) {
            percent = cursor.getInt(6);//下载百分比
        }
        cursor.close();
        sql.close();
        return percent;
    }

    public int deleteCourseWare(String userId, CourseWare courseWare) {  //删除课节
        SQLiteDatabase sql = db.getWritableDatabase();
        if(courseWare.getCwId().equals(SharedPrefHelper.getInstance(mContext).getDownloadTaskId())){
            DownloadExcutor.getInstance(mContext).setFlag(false);
        }
        int result = sql.delete("downloadtable", "userId=? and classId=? and videoID=?",
                new String[]{userId, courseWare.getClassId(), courseWare.getCwId()});
        deleteFileCourseWare(userId, courseWare);
        sql.close();
        return result;
    }

    public void deleteCourseWares(String userId, List<CourseWare> courseWares) {  //删除课节
        SQLiteDatabase sql = db.getWritableDatabase();
        for(int i=0;i<courseWares.size();i++){
            sql.delete("downloadtable", "userId=? and classId=? and videoID=?",
                    new String[]{userId, courseWares.get(i).getClassId(), courseWares.get(i).getCwId()});
            deleteFileCourseWare(userId, courseWares.get(i));
        }
        sql.close();
    }

    public void deleteCourseWaresDownloading(String userId, List<CourseWare> courseWares) {  //删除课节
        AppContext.getInstance().isStart = 0;
        DownloadExcutor.getInstance(mContext).setFlag(false);
        DownloadTaskManager.getInstance().clearList();

        SQLiteDatabase sql = db.getWritableDatabase();
        for(int i=0;i<courseWares.size();i++){
            sql.delete("downloadtable", "userId=? and classId=? and videoID=?",
                    new String[]{userId, courseWares.get(i).getClassId(), courseWares.get(i).getCwId()});
            deleteFileCourseWare(userId, courseWares.get(i));
        }
        sql.close();
    }

    //
//    public void deleteCourseWares(List<CourseWare> list) {
//        SQLiteDatabase sql = db.getWritableDatabase();
//        for (int i = 0; i < list.size(); i++) {
//            sql.delete("downloadtable", "videoID=?", new String[]{list.get(i).getVideoID()});
//        }
//        sql.close();
//    }
//
    public int deleteCourse(String userId, Course course) { //删除整个课程
        SQLiteDatabase sql = db.getWritableDatabase();
        int result = sql.delete("downloadtable", "userId=? and classId=?",
                new String[]{userId, course.getCwCode()});
        sql.close();
        deleteFileCourse(userId, course);
        return result;
    }

    public void deleteCourses(String userId, List<Course> courses) { //删除整个课程
        SQLiteDatabase sql = db.getWritableDatabase();
        for(int i=0;i<courses.size();i++){
            sql.delete("downloadtable", "userId=? and classId=?",
                    new String[]{userId, courses.get(i).getCwCode()});
            deleteFileCourse(userId, courses.get(i));
        }
        sql.close();
    }
//
//    public void deleteCourses(String userId,List<CourseWare> list) {
//        SQLiteDatabase sql = db.getWritableDatabase();
//        for (int i = 0; i < list.size(); i++) {
//            sql.delete("downloadtable", "userId=? and courseId=?", new String[]{userId,list.get(i).getCwCode()});
//            deleteFileCourse(userId,list.get(i));
//        }
//        sql.close();
//    }

    private void deleteFileCourse(String userId,Course course) {    //删除文件
        File file = new File(FileUtil.getDownloadPath(mContext));
        for (File f : file.listFiles()) {
            String name = f.getName();
            if (name.contains(userId + "_" + course.getExamId()+ "_" + course.getSubjectId()+ "_" + course.getCwCode())) {
                FileUtil.deleteFile(f.getPath());
            }
        }
    }

    private void deleteFileCourseWare(String userId, CourseWare courseWare) {
        FileUtil.deleteFile(FileUtil.getDownloadPath(mContext) + userId + "_" + courseWare.getExamId() + "_" + courseWare.getSubjectId() + "_" + courseWare.getClassId() + "_" + courseWare.getSectionId() + "_" + courseWare.getCwId());
    }

    public List<GourpDomain> getSubjects(String userId){
        SubjectDB subjectDB=new SubjectDB(mContext);
        List<GourpDomain> list = new ArrayList<GourpDomain>();
        SQLiteDatabase sql = db.getReadableDatabase();
        Cursor cursor = sql.query("downloadtable", new String[]{"subjectId"}, "status=? and userId=?",
                new String[]{Constants.STATE_DownLoaded + "", userId}, "subjectId", null, null);
        while (cursor.moveToNext()) {
            GourpDomain domain=new GourpDomain();
            String subjectId=cursor.getString(0);
            domain.setSubjectId(subjectId);
//            Subject subject=subjectDB.findById(userId,subjectId);
//            if(subject!=null){
//                domain.setName(subject.getSubjectName()+subject.getYear());
//            }else{
//                domain.setName("其他");
//            }
            domain.setName(subjectId);
            domain.setChilds(findCourses(userId,subjectId));
            list.add(domain);
        }
        cursor.close();
        sql.close();
        return list;
    }

    public List<Course> findCourses(String userId,String subjectId) {
        List<CourseWare> list = new ArrayList<CourseWare>();
        SQLiteDatabase sql = db.getReadableDatabase();
        Cursor cursor = sql.query("downloadtable", null, "status=? and userId=? and subjectId=?",
                new String[]{Constants.STATE_DownLoaded + "", userId,subjectId}, null, null, null);
        while (cursor.moveToNext()) {
            CourseWare course = new CourseWare();
            course.setClassId(cursor.getString(2));
            course.setCwId(cursor.getString(3));
            course.setSubjectId(cursor.getString(12));
            course.setExamId(cursor.getString(14));
            course.setSectionId(cursor.getString(13));

            course.setSubjectId(parseWareBean(cursor.getString(10)).getSubjectId());
//            course.setCourseCount(cursor.getInt(8));
            course.setCwBean(cursor.getString(10));
            course.setCourseBean(cursor.getString(9));
            list.add(course);
        }
        cursor.close();
        sql.close();
        return setCourses(list, userId);
    }

    public List<Course> findCourses(String userId) {
        List<CourseWare> list = new ArrayList<CourseWare>();
        SQLiteDatabase sql = db.getReadableDatabase();
        Cursor cursor = sql.query("downloadtable", null, "status=? and userId=?",
                new String[]{Constants.STATE_DownLoaded + "", userId}, null, null, null);
        while (cursor.moveToNext()) {
            CourseWare course = new CourseWare();
            course.setClassId(cursor.getString(2));
            course.setCwId(cursor.getString(3));
            course.setSubjectId(cursor.getString(12));
            course.setExamId(cursor.getString(14));
            course.setSectionId(cursor.getString(13));

            course.setSubjectId(parseWareBean(cursor.getString(10)).getSubjectId());
//            course.setCourseCount(cursor.getInt(8));
            course.setCwBean(cursor.getString(10));
            course.setCourseBean(cursor.getString(9));
            list.add(course);
        }
        cursor.close();
        sql.close();
        return setCourses(list, userId);
    }

    //
    public List<Course> setCourses(List<CourseWare> list, String userId) {
        List<Course> courses = new ArrayList<Course>();
        List<String> courseIds = new ArrayList<String>();
        for (CourseWare course : list) {
            String courseId = course.getClassId();
            if (!courseIds.contains(courseId)) {
                courseIds.add(courseId);
                Course cours = new Course();
                cours.setCwCode(course.getClassId());
                cours.setExamId(course.getExamId());
                cours.setSubjectId(course.getSubjectId());
                CoursePlay coursePlay=parseBean(course.getCourseBean());
                if(coursePlay!=null){
                    cours.setCwName(coursePlay.getName());
                    cours.setCourseImg(coursePlay.getDaPersonImg());
                    cours.setCourseTeacher(coursePlay.getDaPersonName());
                }else{
                    cours.setCwName("其他");
                    cours.setCourseImg("");
                    cours.setCourseTeacher("老师");
                }
                cours.setCourseCount(findCourseWares(courseId, userId).size());
                cours.setDownloadCount(findCourseWares(courseId, userId).size());
                courses.add(cours);
            }
        }
        return courses;
    }

    private CoursePlay parseBean(String bean) {
        CoursePlay course = JSON.parseObject(bean, CoursePlay.class);
        return course;
    }

    public List<CourseWare> findCourseWares(String classId, String userId) {
        List<CourseWare> list = new ArrayList<CourseWare>();
        SQLiteDatabase sql = db.getReadableDatabase();
        Cursor cursor = sql.query("downloadtable", null, "classId=? and status=? and userId=?",
                new String[]{classId, Constants.STATE_DownLoaded + "", userId}, null, null, null);
        while (cursor.moveToNext()) {
            CourseWare course = new CourseWare();
            course.setClassId(cursor.getString(2));
            course.setCwId(cursor.getString(3));
            course.setSubjectId(cursor.getString(12));
            course.setExamId(cursor.getString(14));
            course.setSectionId(cursor.getString(13));

//            course.setYear(cursor.getString(4));
            course.setCourseBean(cursor.getString(9));
//            course.setCourseCount(cursor.getInt(8));
            course.setCwBean(cursor.getString(10));

            if(cursor.getString(9)!=null){
                course.setTagName(parseBean(cursor.getString(9)).getName());
            }else{
                course.setTagName("其他");
            }
            course.setCwVersion(cursor.getString(16));
            course.setCwName(parseWareBean(cursor.getString(10)).getCwName());
            course.setChapterNo(parseWareBean(cursor.getString(10)).getChapterNo());
            list.add(course);
        }
        cursor.close();
        sql.close();
        return list;
    }

    public List<CourseWare> findAllNotFinish(String userId) {
        List<CourseWare> list = new ArrayList<CourseWare>();
        SQLiteDatabase sql = db.getReadableDatabase();
        Cursor cursor = sql.query("downloadtable", null, "status!=? and userId=?",
                new String[]{Constants.STATE_DownLoaded + "", userId}, null, null, null);
        while (cursor.moveToNext()) {
            CourseWare course = new CourseWare();
            course.setClassId(cursor.getString(2));
            course.setCwId(cursor.getString(3));
            course.setSubjectId(cursor.getString(12));
            course.setExamId(cursor.getString(14));
            course.setSectionId(cursor.getString(13));

            course.setCourseBean(cursor.getString(9));
//            course.setCourseCount(cursor.getInt(8));
            course.setCwBean(cursor.getString(10));
//            course.setYears(cursor.getString(4));
            course.setMobileLectureUrl(parseWareBean(cursor.getString(10)).getMobileLectureUrl());
            course.setMobileVideoUrl(parseWareBean(cursor.getString(10)).getMobileVideoUrl());
            course.setMobileDownloadUrl(parseWareBean(cursor.getString(10)).getMobileDownloadUrl());
            course.setCwDesc(parseWareBean(cursor.getString(10)).getCwDesc());
            list.add(course);
        }
        cursor.close();
        sql.close();
        return list;
    }

    public List<CourseWare> findDownloadList(String userId) {
        List<CourseWare> list = new ArrayList();
        SQLiteDatabase sql = db.getReadableDatabase();
        Cursor cursor = sql.query("downloadtable", null, "status!=? and userId=?",
                new String[]{Constants.STATE_DownLoaded + "", userId}, null, null, null);
        while (cursor.moveToNext()) {
            CourseWare course = new CourseWare();
            course.setClassId(cursor.getString(2));
            course.setCwId(cursor.getString(3));
            course.setSubjectId(cursor.getString(12));
            course.setExamId(cursor.getString(14));
            course.setSectionId(cursor.getString(13));

            course.setCourseBean(cursor.getString(9));
//            course.setCourseCount(cursor.getInt(8));
            course.setCwBean(cursor.getString(10));
//            course.setYear(cursor.getString(4));
            course.setCwVersion(cursor.getString(16));
            course.setMobileLectureUrl(parseWareBean(cursor.getString(10)).getMobileLectureUrl());
            course.setMobileDownloadUrl(parseWareBean(cursor.getString(10)).getMobileDownloadUrl());
            course.setCwDesc(parseWareBean(cursor.getString(10)).getCwDesc());
            course.setCwName(parseWareBean(cursor.getString(10)).getCwName());
            list.add(course);
        }
        cursor.close();
        sql.close();
        return list;
    }


    public CourseWare findStatusDownloading(String userId) {
        CourseWare course=null;
        SQLiteDatabase sql = db.getReadableDatabase();
        Cursor cursor = sql.query("downloadtable", null, "status=? and userId=?",
                new String[]{Constants.STATE_DownLoading + "", userId}, null, null, null);
        while (cursor.moveToNext()) {
            course = new CourseWare();
            course.setClassId(cursor.getString(2));
            course.setCwId(cursor.getString(3));
            course.setSubjectId(cursor.getString(12));
            course.setExamId(cursor.getString(14));
            course.setSectionId(cursor.getString(13));

            course.setCourseBean(cursor.getString(9));
//            course.setCourseCount(cursor.getInt(8));
            course.setCwBean(cursor.getString(10));
//            course.setYears(cursor.getString(4));
            course.setMobileLectureUrl(parseWareBean(cursor.getString(10)).getMobileLectureUrl());
            course.setMobileVideoUrl(parseWareBean(cursor.getString(10)).getMobileVideoUrl());
            course.setMobileDownloadUrl(parseWareBean(cursor.getString(10)).getMobileDownloadUrl());
        }
        cursor.close();
        sql.close();
        return course;
    }

    public List<CourseWare> findStatusWating(String userId) {
        List<CourseWare> list = new ArrayList<CourseWare>();
        SQLiteDatabase sql = db.getReadableDatabase();
        Cursor cursor = sql.query("downloadtable", null, "status=? and userId=?",
                new String[]{Constants.STATE_Waiting + "", userId}, null, null, null);
        while (cursor.moveToNext()) {
            CourseWare course = new CourseWare();
            course.setClassId(cursor.getString(2));
            course.setCwId(cursor.getString(3));
            course.setSubjectId(cursor.getString(12));
            course.setExamId(cursor.getString(14));
            course.setSectionId(cursor.getString(13));

            course.setCourseBean(cursor.getString(9));
//            course.setCourseCount(cursor.getInt(8));
            course.setCwBean(cursor.getString(10));
//            course.setYears(cursor.getString(4));
            course.setMobileLectureUrl(parseWareBean(cursor.getString(10)).getMobileLectureUrl());
            course.setMobileVideoUrl(parseWareBean(cursor.getString(10)).getMobileVideoUrl());
            course.setCwDesc(parseWareBean(cursor.getString(10)).getCwDesc());
            course.setMobileDownloadUrl(parseWareBean(cursor.getString(10)).getMobileDownloadUrl());
            list.add(course);
        }
        cursor.close();
        sql.close();
        return list;
    }

    private CourseWare parseWareBean(String bean) {
        CourseWare courseWare = JSON.parseObject(bean, CourseWare.class);
        return courseWare;
    }

    public void addCourseOld(String subjectId,String courseId,String imgUrl,String name,String teacherName) {
        if(find(subjectId,courseId)){
            return;
        }
        SQLiteDatabase sql = db.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("subjectId", subjectId);
        values.put("courseId", courseId);
        values.put("imgUrl", imgUrl);
        values.put("name", name);
        values.put("teacherName", teacherName);
        sql.insert("courseOld", null, values);
        sql.close();
    }

    public boolean find(String subjectId,String courseId) {
        SQLiteDatabase sql = db.getReadableDatabase();
        Cursor cursor = sql.query("courseOld", null, "courseId=? and subjectId=?",
                new String[]{subjectId,courseId}, null, null, null);
        if (cursor.moveToNext()) {
            cursor.close();
            sql.close();
            return true;
        } else {
            cursor.close();
            sql.close();
            return false;
        }
    }
}
