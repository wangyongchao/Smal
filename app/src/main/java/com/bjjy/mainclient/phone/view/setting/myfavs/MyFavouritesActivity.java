package com.bjjy.mainclient.phone.view.setting.myfavs;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.bjjy.mainclient.phone.event.DeleteQuestionCollection;
import com.bjjy.mainclient.phone.view.exam.ExamActivity;
import com.bjjy.mainclient.phone.view.question.ExamRecommQuestionListActivity;
import com.bjjy.mainclient.phone.widget.DialogManager;
import com.dongao.mainclient.model.bean.course.CoursePlay;
import com.dongao.mainclient.model.bean.user.MyCollection;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.MyCollectionDB;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseActivity;
import com.bjjy.mainclient.phone.view.play.PlayActivity;
import com.bjjy.mainclient.phone.view.play.domain.CoursePlayDB;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by dell on 2016/5/17.
 */
public class MyFavouritesActivity extends BaseActivity implements AdapterView.OnItemClickListener,AdapterView.OnItemLongClickListener {

    @Bind(R.id.top_title_left)
    ImageView topTitleLeft;
    @Bind(R.id.top_title_right)
    ImageView topTitleRight;
    @Bind(R.id.top_title_text)
    TextView topTitleText;
    
    @Bind(R.id.myfavs_ls)
    ListView myfavsLs;
    @Bind(R.id.favs_nocollect)
    LinearLayout favsNocollect;
    private MyFavsAdapter adapter;
    private List<MyCollection> list;
    private MyCollectionDB collectionDB;
    private String userId,subjectId;
    private String flag;

    private CoursePlayDB coursePlayDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myfavs);
        ButterKnife.bind(this);
        initView();
        initData();
    }

    @Override
    public void initView() {
        coursePlayDB=new CoursePlayDB(this);
        topTitleLeft.setImageResource(R.drawable.nav_left);
        topTitleRight.setImageResource(R.drawable.my_question_title_add);
//        topTitleRight.setVisibility(View.VISIBLE);
        topTitleLeft.setVisibility(View.VISIBLE);
        topTitleLeft.setOnClickListener(this);
        topTitleRight.setOnClickListener(this);
        topTitleText.setText("收藏");

        adapter = new MyFavsAdapter(MyFavouritesActivity.this);
        myfavsLs.setOnItemClickListener(this);
        myfavsLs.setOnItemLongClickListener(this);

        collectionDB=new MyCollectionDB(this);
        userId= SharedPrefHelper.getInstance(this).getUserId();

        flag=getIntent().getStringExtra("flag");
    }

    @Override
    public void initData() {
//        myfavsLs.setAdapter(adapter);
        //获取到数据库的data（list）
        subjectId= SharedPrefHelper.getInstance(this).getSubjectId();
        if(TextUtils.isEmpty(flag)){
            list=collectionDB.findOneTypeCollection(userId,"3",subjectId);
        }else{
            list=collectionDB.findOneTypeCollection(userId,"1",subjectId);
        }

        if (list != null && list.size() > 0) {
            myfavsLs.setVisibility(View.VISIBLE);
            favsNocollect.setVisibility(View.GONE);

            adapter.setList(list);
            myfavsLs.setAdapter(adapter);
        } else {
            myfavsLs.setVisibility(View.GONE);
            favsNocollect.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.top_title_left:
                finish();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String type=list.get(position).getType();
//        String classId=list.get(position).getClasssId();
//        String questionId=list.get(position).getQuestionId();
        Intent intent = new Intent();
        if(type.equals("1")){//做题
            SharedPrefHelper.getInstance(MyFavouritesActivity.this).setExamTag(Constants.EXAM_TAG_COLLECTION);
            intent.setClass(MyFavouritesActivity.this, ExamActivity.class);
            intent.putExtra("questionId", list.get(position).getQuestionId());
        }else if(type.equals("2")){//答疑
            SharedPrefHelper.getInstance(MyFavouritesActivity.this).setMainTypeId(list.get(position).getMainType());
            intent.setClass(MyFavouritesActivity.this, ExamRecommQuestionListActivity.class);
            intent.putExtra("questionAnswerId", list.get(position).getCollectionId());
        }else if(type.equals("3")){//播放
            String year=list.get(position).getYear();
            if(!year.equals("666")){
                Toast.makeText(MyFavouritesActivity.this, "视频不存在", Toast.LENGTH_SHORT).show();
                return;
            }
            CoursePlay play=coursePlayDB.findCourse(list.get(position).getClasssId());
            intent.setClass(MyFavouritesActivity.this, PlayActivity.class);
            intent.putExtra("classId", list.get(position).getClasssId());
            intent.putExtra("playCwId", list.get(position).getCwId());
            intent.putExtra("courseBean", JSON.toJSONString(play));
            intent.putExtra("subjectId", list.get(position).getSubjectId());
        }else if(type.equals("4")){//web
            intent.setClass(MyFavouritesActivity.this, PlayActivity.class);
        }
        startActivity(intent);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
//        AlertDialog.Builder builder = new AlertDialog.Builder(MyFavouritesActivity.this);
//        builder.setTitle("确定删除吗");
//        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                collectionDB.delete(list.get(position));
//                initData();
//                adapter.notifyDataSetChanged();
//            }
//        });
//        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//            }
//        });
//        builder.show();
        DialogManager.showNormalDialog(this, "确定删除吗", "提示", "取消", "确定",
                new DialogManager.CustomDialogCloseListener() {
                    @Override
                    public void yesClick() {
                        if(list.get(position).getType().equals(MyCollection.TYPE_QUESTION))
                            EventBus.getDefault().post(new DeleteQuestionCollection(list.get(position).getCollectionId()));
                        collectionDB.delete(list.get(position));
                        initData();
                        adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void noClick() {
                    }
                });
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        initData();
    }
}
