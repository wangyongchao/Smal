package com.bjjy.mainclient.phone.view.exam.activity.myexam;

import android.content.Intent;

import com.alibaba.fastjson.JSON;
import com.bjjy.mainclient.phone.view.download.local.bean.YearInfo;
import com.dongao.mainclient.model.bean.exam.ExamRuleInfo;
import com.bjjy.mainclient.phone.view.studybar.Constant;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ParamsUtils;
import com.dongao.mainclient.model.remote.bean.BaseBean;

/**
 * Created by wyc on 2016/5/19.
 */
public class ExamMinePercenter extends BasePersenter<ExamMineView> {

	public int currentPage = 1;
	YearInfo cuYearInfo;
	public String ruleId;
	public ExamRuleInfo examRuleInfo;
	private String accountId;

	@Override
	public void attachView(ExamMineView payWayView) {
		super.attachView(payWayView);
	}

	public void initData() {
		getMvpView().showTopTextTitle("我的考试");
		Intent intent = getMvpView().getTheIntent();
		ruleId = intent.getStringExtra("ruleId");
		accountId = intent.getStringExtra("accountId");
		String yearInfo = SharedPrefHelper.getInstance(getMvpView().context()).getCurYear();
		cuYearInfo = JSON.parseObject(yearInfo, YearInfo.class);
		getMvpView().showLoading();
		getInterData();
	}

	@Override
	public void getData() {
	}

	public void getInterData() {
		apiModel.getData(ApiClient.getClient().getExamPaperRule(ParamsUtils.getInstance(getMvpView().context()).getExamPaperRule(cuYearInfo.getYearName(), ruleId)));
	}

	@Override
	public void setData(String obj) {
		try {
			getMvpView().hideLoading();
			BaseBean baseBean = JSON.parseObject(obj, BaseBean.class);
			if (baseBean == null) {
				getMvpView().showContentView(Constant.VIEW_TYPE_1);
				return;
			} else {
				int result = baseBean.getCode();
				if (result != Constants.RESULT_CODE) {
					getMvpView().showContentView(Constant.VIEW_TYPE_2);
					return;
				}
			}
			getMvpView().showContentView(Constant.VIEW_TYPE_0);
			String body = baseBean.getBody();
			examRuleInfo = JSON.parseObject(body, ExamRuleInfo.class);
			examRuleInfo.setAccountId(accountId);
			getMvpView().showExamInfo(examRuleInfo);
		} catch (Exception e) {
			getMvpView().showContentView(Constant.VIEW_TYPE_3);
		}
	}

	@Override
	public void onError(Exception e) {
		super.onError(e);
		getMvpView().showContentView(Constant.VIEW_TYPE_1);
	}

	public void setOnItemClick(int position) {
	}
}
