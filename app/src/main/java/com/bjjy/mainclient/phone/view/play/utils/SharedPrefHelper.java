package com.bjjy.mainclient.phone.view.play.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * sp存储工具�?
 * @author fengzongwei
 *
 */
public class SharedPrefHelper {

	private static SharedPrefHelper  sharedPrefHelper;
	private static Context context;
	private SharedPreferences sharedPreferences;
	
	private static final String PLAY_VIDEO_ID = "play_video_id";//当前在播放的视频id
	private static final String SP_FILE_NAME = "sp_name";
	private static final String USER_ID = "user_id";

	private static final String IS_ONLINE = "is_online";//是否是在线播放

	public static synchronized SharedPrefHelper getInstance(Context context1) {
		context = context1;
		if (null == sharedPrefHelper) {
			sharedPrefHelper = new SharedPrefHelper();
		}
		return sharedPrefHelper;
	}

	private SharedPrefHelper() {
		sharedPreferences = context.getSharedPreferences(
                SP_FILE_NAME, Context.MODE_PRIVATE);
	}

	public void setPlayVideoId(String videoId){
		sharedPreferences.edit().putString(PLAY_VIDEO_ID, videoId).commit();
	}
	public String getPlayVideoId(){
		return sharedPreferences.getString(PLAY_VIDEO_ID, "");
	}
	
	public void setUserId(String userId){
		sharedPreferences.edit().putString(USER_ID, userId).commit();
	}
	public String getUserId(){
		return sharedPreferences.getString(USER_ID, "");
	}


	/**
	 * 是否是在线播放
	 * @param isOnline
	 */
	public void setIsOnline(boolean isOnline){
		sharedPreferences.edit().putBoolean(IS_ONLINE,isOnline).commit();
	}

	public boolean getIsOnline(){
		return  sharedPreferences.getBoolean(IS_ONLINE,false);
	}

	
}
