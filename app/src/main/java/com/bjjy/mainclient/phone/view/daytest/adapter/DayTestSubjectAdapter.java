package com.bjjy.mainclient.phone.view.daytest.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.bjjy.mainclient.phone.view.daytest.fragment.DayTestFragment;
import com.dongao.mainclient.model.bean.daytest.DayExSelectedSubject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fengzongwei on 2016/5/24 0024.
 */
public class DayTestSubjectAdapter extends FragmentPagerAdapter {

    private List<DayExSelectedSubject> dayExSelectedSubjects;
    private List<DayTestFragment> fragmentList = new ArrayList<>();
    public DayTestSubjectAdapter(FragmentManager fm,List<DayExSelectedSubject> dayExSelectedSubjects){
        super(fm);
        this.dayExSelectedSubjects = dayExSelectedSubjects;
        for(int i=0;i<dayExSelectedSubjects.size();i++){
            DayTestFragment dayTestFragment = new DayTestFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("index",i);
            bundle.putString("name",dayExSelectedSubjects.get(i).getSubjectName());
            dayTestFragment.setArguments(bundle);
            fragmentList.add(dayTestFragment);
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return dayExSelectedSubjects.get(position).getSubjectName();
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return dayExSelectedSubjects.size();
    }

    public void freshData(long time){
        for(int i=0;i<fragmentList.size();i++){
            fragmentList.get(i).freshData(time);
        }
    }

    public void notifyDataChange(){
        int j = fragmentList.size();
        while(fragmentList.size()<dayExSelectedSubjects.size()){
            DayTestFragment dayTestFragment = new DayTestFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("index",j);
            bundle.putString("name",dayExSelectedSubjects.get(j).getSubjectName());
            dayTestFragment.setArguments(bundle);
            fragmentList.add(dayTestFragment);
            j++;
        }
        for(int i=0;i<fragmentList.size();i++){
            fragmentList.get(i).initData();
        }
    }

}
