package com.bjjy.mainclient.phone.view.studybar.message;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bjjy.mainclient.phone.app.BaseFragment;
import com.bjjy.mainclient.phone.view.studybar.Constant;
import com.bjjy.mainclient.phone.widget.EmptyViewLayout;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.view.studybar.message.adapter.MyMessageAdapter;
import com.bjjy.mainclient.phone.view.studybar.view.RefreshLayout;
import com.bjjy.mainclient.phone.widget.coverflow.RollPagerView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author wyc
 */
public class MyMessageFragment extends BaseFragment implements MyMessageView {

    @Bind(R.id.top_title_left)
    ImageView top_title_left;
    @Bind(R.id.top_title_right)
    ImageView top_title_right;
    @Bind(R.id.top_title_text)
    TextView top_title_text;
    @Bind(R.id.swipe_container)
    RefreshLayout swipe_container;
    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;
    private View mRootView;
    private boolean mHasLoadedOnce;

    @OnClick(R.id.top_title_left) void onBackClick(){
        getActivity().finish();
    }
    private MyMessagePercenter myMessagePercenter;
    private EmptyViewLayout mEmptyLayout;
    private RollPagerView mRollViewPager;
    private MyMessageAdapter myMessageAdapter;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if (isVisibleToUser && !mHasLoadedOnce) {
                // async http request here
                mHasLoadedOnce = true;
                initData();
            }
        }
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.studybar_mymessage_fg, container, false);
        }
        ButterKnife.bind(this, mRootView);
        myMessagePercenter = new MyMessagePercenter();
        myMessagePercenter.attachView(this);
        initView();
        initData();
        return mRootView;
    }

    @Override
    public void initView() {
        top_title_left.setVisibility(View.VISIBLE);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        mEmptyLayout = new EmptyViewLayout(getActivity(), swipe_container);
        mEmptyLayout.setErrorButtonClickListener(mErrorClickListener);
        mEmptyLayout.setEmptyButtonClickListener(emptyButtonClickListener);
        swipe_container.setColorSchemeColors(getResources().getColor(R.color.color_primary));
        swipe_container.setChildView(recyclerView);
        setListener();

    }

    private void setListener() {
        swipe_container.setOnLoadListener(new RefreshLayout.OnLoadListener() {
            @Override
            public void onLoad() {
                myMessagePercenter.getLoadData();
            }
        });
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int topRowVerticalPosition =
                        (recyclerView == null || recyclerView.getChildCount() == 0) ? 0 : recyclerView.getChildAt(0).getTop();
                swipe_container.setEnabled(topRowVerticalPosition >= 0);

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
        swipe_container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                myMessagePercenter.currentPage = 1;
                myMessagePercenter.getData();
            }
        });
    }


    /**
     * 错误监听
     */
    private View.OnClickListener mErrorClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            myMessagePercenter.currentPage=1;
            myMessagePercenter.getData();
        }
    };
    /**
     * 无数据监听
     */
    private View.OnClickListener emptyButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //showAppMsg("显示kong");
        }
    };

    @Override
    public void initData() {
        mEmptyLayout.showLoading();
        myMessagePercenter.initData();
    }


    @Override
    public void onClick(View v) {

    }

    @Override
    public void showLoading() {
        if(swipe_container != null){
            swipe_container.setRefreshing(true);
        }
    }

    @Override
    public void hideLoading() {
        if(swipe_container != null){
            swipe_container.setRefreshing(false);
            swipe_container.setLoading(false);
        }
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public Context context() {
        return getActivity();
    }

    @Override
    public void initAdapter() {
        if (myMessageAdapter==null){
            myMessageAdapter = new MyMessageAdapter(getActivity(),myMessagePercenter.privateTeacherList);
//            myMessageAdapter.setOnItemClick(this);
            recyclerView.setAdapter(myMessageAdapter);
        }else{
            myMessageAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void showContentView(int type) {
        if (type== Constant.VIEW_TYPE_0){
            mEmptyLayout.showContentView();
        }else  if (type==Constant.VIEW_TYPE_1){
            mEmptyLayout.showNetErrorView();
        }else  if (type==Constant.VIEW_TYPE_2){
            mEmptyLayout.showEmpty();
        }else  if (type==Constant.VIEW_TYPE_3){
            mEmptyLayout.showError();
        }
        hideLoading();
    }

    @Override
    public boolean isRefreshNow() {
        if (swipe_container==null){
            return false;
        }
        return swipe_container.isRefreshing();
    }

    @Override
    public RefreshLayout getRefreshLayout() {
        return swipe_container;
    }

    @Override
    public Intent getTheIntent() {
        return getActivity().getIntent();
    }

    @Override
    public void showTopTitle(String title) {
        top_title_text.setText(title);
    }

    @Override
    public void setNoDataMoreShow(boolean isShow) {

    }
    
}
