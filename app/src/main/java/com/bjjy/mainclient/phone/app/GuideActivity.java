package com.bjjy.mainclient.phone.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.view.main.MainActivity;
import com.bjjy.mainclient.phone.view.user.LoginNewActivity;
import com.dongao.mainclient.model.local.SharedPrefHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GuideActivity extends BaseActivity {

    @Bind(R.id.viewpager)
	ViewPager viewPager;
    @Bind(R.id.guide_layout)
    View guide_layout;

    private int[] imageIds={R.mipmap.guide1,R.mipmap.guide2,R.mipmap.guide3};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide);
        ButterKnife.bind(this);
        initView();
        initData();
    }

    @Override
    public void initView() {
        List<ImageView> ivs=new ArrayList<>();
        for (int i = 0; i < imageIds.length; i++) {
            ImageView imageView=new ImageView(this);
            LinearLayout.LayoutParams lp=new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
            );
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            imageView.setLayoutParams(lp);
            imageView.setImageResource(imageIds[i]);
            ivs.add(imageView);

        }
        viewPager.setAdapter(new GuidePagerAdapter(ivs));
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (position == imageIds.length-1){
                    guide_layout.setVisibility(View.VISIBLE);
                }else{
                    guide_layout.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void initData() {

    }

    @OnClick({R.id.go_main_bt,R.id.go_login_bt})
    public void onClick(View view){
        SharedPrefHelper.getInstance(GuideActivity.this).setFirstIn(false);
        switch (view.getId()){
            case R.id.go_main_bt:
                Intent intent = new Intent(GuideActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.go_login_bt:
                Intent intent1 = new Intent(GuideActivity.this, MainActivity.class);
                startActivity(intent1);
                Intent intent2 = new Intent(GuideActivity.this, LoginNewActivity.class);
                startActivity(intent2);
                finish();
                break;
        }
    }


    private class GuidePagerAdapter extends PagerAdapter {
        private List<? extends View> ivs;

        public GuidePagerAdapter(List<? extends View> ivs) {
            this.ivs = ivs;
        }

        @Override
        public int getCount() {
            return ivs.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view==object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            container.addView(ivs.get(position));
            return ivs.get(position);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView(ivs.get(position));
        }
    }
}
