/*
 * Copyright (C) 2013 Andreas Stuetz <andreas.stuetz@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bjjy.mainclient.phone.view.classroom.course;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.bjjy.mainclient.phone.app.BaseFragment;
import com.bjjy.mainclient.phone.event.AddQuestionSuccess;
import com.bjjy.mainclient.phone.view.classroom.courseplay.CoursePlayFragment;
import com.bjjy.mainclient.phone.view.exam.db.AnswerLogDB;
import com.bjjy.mainclient.phone.widget.statusbar.Utils;
import com.dongao.mainclient.core.util.LogUtils;
import com.dongao.mainclient.core.util.StringUtil;
import com.dongao.mainclient.model.bean.course.CourseAnswer;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.event.CourseTabTypeEvent;
import com.bjjy.mainclient.phone.event.LoginSuccessEvent;
import com.bjjy.mainclient.phone.event.UpdateEvent;
import com.bjjy.mainclient.phone.utils.NetworkUtil;
import com.bjjy.mainclient.phone.view.classroom.PaperQuestion.PaperFragment;
import com.bjjy.mainclient.phone.view.classroom.question.ClassQuestionFragmentNew;
import com.bjjy.mainclient.phone.view.classroom.subject.SubjectActivity;
import com.bjjy.mainclient.phone.view.play.domain.CwStudyLogDB;
import com.bjjy.mainclient.phone.view.question.SelectBookAndKpActivity;
import com.bjjy.mainclient.phone.widget.parallaxviewpager.HeaderViewPager;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import com.dongao.mainclient.core.nanotasks.BackgroundWork;
import com.dongao.mainclient.core.nanotasks.Completion;
import com.dongao.mainclient.core.nanotasks.Tasks;

public class CourseFragments extends BaseFragment implements CourseFragmentsView {

    @Bind(R.id.scrollableLayout)
    HeaderViewPager scrollableLayout;

    @Bind(R.id.course_view_pager)
    ViewPager mViewPager;

    private List<HeaderViewPagerFragment> mFragmentList;

    private View mRootView;

    @Bind(R.id.ll_title)
    LinearLayout ll_title;

    @Bind(R.id.top_title_text)
    TextView top_title_text;

    @Bind(R.id.image)
    ImageView top_iv;

    @Bind(R.id.course_radio)
    RadioGroup rg;

    @Bind(R.id.radio_button_exam)
    RadioButton exam_rb;

    @Bind(R.id.radio_button_course)
    RadioButton course_rb;

    @Bind(R.id.radio_button_ask)
    RadioButton ask_rb;

    @Bind(R.id.line)
    View line;

    @Bind(R.id.top_title_bar_layout)
    View top_title_bar_layout;

    @Bind(R.id.progressBar)
    ProgressBar pb;

    @Bind(R.id.status_bar_fix)
    View status_bar_fix;

    /*** top title ***/
    @Bind(R.id.finish_num_tv)
    TextView finish_num_tv;
    @Bind(R.id.finish_num_unit_tv)
    TextView finish_num_unit_tv;
    @Bind(R.id.finish_num_tip_tv)
    TextView finish_num_tip_tv;

    @Bind(R.id.unfinish_num_tv)
    TextView unfinish_num_tv;
    @Bind(R.id.unfinish_num_unit_tv)
    TextView unfinish_num_unit_tv;
    @Bind(R.id.unfinish_num_tip_tv)
    TextView unfinish_num_tip_tv;

    @Bind(R.id.course_fragment_ask_img)
    ImageView imageView_ask;

    int finish_num;
    String finish_num_unit;
    String finish_num_tip;

    int unfinish_num;
    String unfinish_num_unit;
    String unfinish_num_tip;


    AnswerLogDB answerLogDB;
    CwStudyLogDB cwStudyLogDB;

    boolean isMeasured = false;

    private int cusPostion = 0;

    private String userId;
    private String examId;
    private String subjectId;

    FragmentPagerAdapter fragmentPagerAdapter;

    private CourseFragmentsPersenter courseFragmentPersenter;

    private boolean mHasLoadedOnce = false;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if (isVisibleToUser && !mHasLoadedOnce) {
                // async http request here
                mHasLoadedOnce = true;
                initData();
            }
        }
        super.setUserVisibleHint(isVisibleToUser);
    }

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 2;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public CourseFragments() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static CourseFragments newInstance(int columnCount) {
        CourseFragments fragment = new CourseFragments();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        //如果用户在其他其它地方登录了，就不重新加载了
        // !SharedPrefHelper.getInstance(getActivity()).isOtherLogin()
        if(mHasLoadedOnce){
            setTitle();
            updateTitleData(cusPostion);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mRootView == null){
            mRootView = inflater.inflate(R.layout.course_fragments,container,false);
        }
        ButterKnife.bind(this,mRootView);
        ViewGroup parent = (ViewGroup) mRootView.getParent();
        if (parent != null){
            parent.removeView(mRootView);
        }
        courseFragmentPersenter = new CourseFragmentsPersenter();
        courseFragmentPersenter.attachView(this);
        initView();
        return mRootView;
    }

    @Override
    public void initView() {

        ll_title.setOnClickListener(this);

        setTranslucentStatus();
        top_title_bar_layout.setAlpha(0);
        ViewTreeObserver observer = ll_title.getViewTreeObserver();
        observer.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                if (!isMeasured) {
                    if(ll_title!=null){
                        scrollableLayout.setTopOffset(ll_title.getHeight());
                    }
                    isMeasured = true;
                }
                return true;
            }
        });

        rg = (RadioGroup) mRootView.findViewById(R.id.course_radio);
        rg.check(R.id.radio_button_exam);
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radio_button_ask) {
                    imageView_ask.setVisibility(View.VISIBLE);
                } else
                    imageView_ask.setVisibility(View.GONE);
            }
        });
        pb.setVisibility(View.GONE);
        exam_rb.setOnClickListener(this);
        course_rb.setOnClickListener(this);
        ask_rb.setOnClickListener(this);
        imageView_ask.setOnClickListener(this);

        mViewPager.requestDisallowInterceptTouchEvent(true);

    }

    @TargetApi(19)
    public void setTranslucentStatus() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            //当状态栏透明后，内容布局会上移，这里使用一个和状态栏高度相同的view来修正内容区域
            status_bar_fix.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Utils.getStatusHeight(getActivity())));
            status_bar_fix.setAlpha(0);
        }
    }

    @Override
    public void initData() {
        answerLogDB = new AnswerLogDB(getActivity());
        cwStudyLogDB = new CwStudyLogDB(getActivity());
        userId = SharedPrefHelper.getInstance(getActivity()).getUserId();
        examId = SharedPrefHelper.getInstance(getActivity()).getExamId();
        subjectId = SharedPrefHelper.getInstance(getActivity()).getSubjectId();



        mFragmentList = new ArrayList<HeaderViewPagerFragment>();
        mFragmentList.add(new PaperFragment());
        mFragmentList.add(new CoursePlayFragment());
        mFragmentList.add(new ClassQuestionFragmentNew());

        fragmentPagerAdapter = new FragmentPagerAdapter(getChildFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return mFragmentList.get(position);
            }

            @Override
            public int getCount() {
                return mFragmentList.size();
            }
        };
        mViewPager.setAdapter(fragmentPagerAdapter);
        mViewPager.setOffscreenPageLimit(3);
        scrollableLayout.setCurrentScrollableContainer(mFragmentList.get(0));

        initEvent();

        setTitle();
    }

    private void setTitle(){
        String subjectName = SharedPrefHelper.getInstance(getActivity()).getSubjectName();
        if(StringUtil.isEmpty(subjectName)){
            top_title_text.setText("我的课堂");
        }else{
            top_title_text.setText(subjectName);
        }
    }


    private void initEvent() {

        scrollableLayout.setOnScrollListener(new HeaderViewPager.OnScrollListener() {
            @Override
            public void onScroll(int currentY, int maxY) {
               // LogUtils.d("currentY="+currentY);
                if(currentY>=maxY){
                    line.setVisibility(View.VISIBLE);
                }else{
                    line.setVisibility(View.GONE);
                }

                //让头部具有差速动画,如果不需要,可以不用设置
                // top_title_bar_layout.setTranslationY(currentY / 2);
                //动态改变标题栏的透明度,注意转化为浮点型
                float alpha = 1.0f * currentY / maxY;
                top_title_bar_layout.setAlpha(alpha);
                //注意头部局的颜色也需要改变
                status_bar_fix.setAlpha(alpha);
            }

            @Override
            public void onRefresh() {
                updateTitleData(cusPostion);
                updateContentData(cusPostion);
                pb.setVisibility(View.VISIBLE);
            }
        });

        mViewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if(pb.isShown()){
                    pb.setVisibility(View.GONE);
                }
                cusPostion = position;
                scrollableLayout.setCurrentScrollableContainer(mFragmentList.get(position));
                updateTitleData(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mViewPager.setCurrentItem(0);
        updateTitleData(0);
    }


   @Override
    public void onDestroyView() {
        super.onDestroyView();
       ButterKnife.unbind(this);
       EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onEventAsync(UpdateEvent event)
    {
        LogUtils.e("UpdateEvent");
        setTitle();
        updateTitleData(cusPostion);
       // updateContentData(cusPostion);
    }

    @Subscribe
    public void onEventAsync(AddQuestionSuccess event)
    {
        courseFragmentPersenter.getData();
        // updateContentData(cusPostion);
    }

   @Subscribe
    public void onEventAsync(LoginSuccessEvent event)
    {
        userId = SharedPrefHelper.getInstance(getActivity()).getUserId();
        examId = SharedPrefHelper.getInstance(getActivity()).getExamId();
        subjectId = SharedPrefHelper.getInstance(getActivity()).getSubjectId();
        setTitle();
        setUpdateData();
    }


    @Subscribe
    public void onEventAsync(CourseTabTypeEvent event)
    {
        pb.setVisibility(View.GONE);
    }

    /**
     * 更新title数据
     * @param position
     */
    public void updateTitleData(int position){
        switch (position){
            case 0:
                course_rb.setBackgroundResource(R.drawable.tab_class_bg);
                rg.check(R.id.radio_button_exam);
                //finish_num = 23;
                finish_num_unit = "%";
                finish_num_tip = "正确率";

                //unfinish_num = 33;
                unfinish_num_unit = "题";
                unfinish_num_tip = "刷题量";
                getExamLog();
                break;
            case 1:
                course_rb.setBackgroundResource(R.drawable.tab_class_bg);
                rg.check(R.id.radio_button_course);

                //finish_num = 43;
                finish_num_unit = "节";
                finish_num_tip = "看课";

                //unfinish_num = 83;
                unfinish_num_unit = "小时";
                unfinish_num_tip = "时长";

                getCourseLog();
                break;
            case 2:
                course_rb.setBackgroundResource(R.drawable.tab_class_two_bg);
                rg.check(R.id.radio_button_ask);

                finish_num = 0;
                finish_num_unit = "个";
                finish_num_tip = "已解决";

                unfinish_num = 0;
                unfinish_num_unit = "个";
                unfinish_num_tip = "全部";
                if(NetworkUtil.isNetworkAvailable(getActivity()))
                    courseFragmentPersenter.getData();
                else
                if(!StringUtil.isEmpty(SharedPrefHelper.getInstance(getActivity()).getAnswerCache(subjectId))){
                    setView(JSON.parseObject(SharedPrefHelper.getInstance(getActivity()).getAnswerCache(subjectId),CourseAnswer.class),false);
                }else{
                    setUpdateData();
                }
                break;
        }
    }

    /**
     * 更新内容数据
     * @param position
     */
    public void updateContentData(int position){
        switch (position){
            case 0:
                 PaperFragment paperFragment  = (PaperFragment) fragmentPagerAdapter.getItem(position);
                 paperFragment.initData();
                break;
            case 1:
                CoursePlayFragment coursePlayFragment = (CoursePlayFragment) fragmentPagerAdapter.getItem(position);
                coursePlayFragment.initData();
                break;
            case 2:
                courseFragmentPersenter.getData();
                ClassQuestionFragmentNew classQuestionFragment  = (ClassQuestionFragmentNew) fragmentPagerAdapter.getItem(position);
                classQuestionFragment.onRefresh();
                break;
        }
    }

    private void setUpdateData(){
        if(finish_num_tv!=null){
            finish_num_tv.setText(finish_num+"");
        }
        if(finish_num_unit_tv!=null){
            finish_num_unit_tv.setText(finish_num_unit);
        }
        if(finish_num_tip_tv!=null){
            finish_num_tip_tv.setText(finish_num_tip);
        }
        if(unfinish_num_tv!=null){
            unfinish_num_tv.setText(unfinish_num+"");
        }
        if(unfinish_num_unit_tv!=null){
            unfinish_num_unit_tv.setText(unfinish_num_unit);
        }
        if(unfinish_num_tip_tv!=null){
            unfinish_num_tip_tv.setText(unfinish_num_tip);
        }
    }


    /**
     * 获取试卷的数字
     */
    private void getExamLog(){

        Tasks.executeInBackground(getActivity(), new BackgroundWork<String>() {
            @Override
            public String doInBackground() throws Exception {
                subjectId = SharedPrefHelper.getInstance(getActivity()).getSubjectId();
                String finish_num_str = answerLogDB.findAllQuestionCorrectRate(userId,examId,subjectId);
                String unfinish_num_str = answerLogDB.findAllDoneQuestion(userId,examId,subjectId);
                if(!StringUtil.isEmpty(finish_num_str)){
                    finish_num = Integer.valueOf(finish_num_str);
                }else{
                    finish_num = 0;
                }
                if(!StringUtil.isEmpty(unfinish_num_str)){
                    unfinish_num = Integer.valueOf(unfinish_num_str);
                }else{
                    unfinish_num = 0;
                }
                return "";
            }
        }, new Completion<String>() {
            @Override
            public void onSuccess(Context context, String result) {
                setUpdateData();
            }

            @Override
            public void onError(Context context, Exception e) {
            }
        });
    }

    /**
     * 获取课程的数字
     */
    private void getCourseLog(){

        Tasks.executeInBackground(getActivity(), new BackgroundWork<String>() {
            @Override
            public String doInBackground() throws Exception {


                subjectId = SharedPrefHelper.getInstance(getActivity()).getSubjectId();
                long finish_num_str  = cwStudyLogDB.getAllListened(userId,subjectId);
                long unfinish_num_str = cwStudyLogDB.getListenedTimes(userId,subjectId);
                finish_num = (int)finish_num_str;
                unfinish_num = (int) unfinish_num_str/3600000;
                if(unfinish_num==0){
                    unfinish_num_unit = "分钟";
                    if((int)unfinish_num_str/1000>0 && (int)unfinish_num_str/1000<60){
                        unfinish_num=1;
                    }else{
                        unfinish_num=(int) unfinish_num_str/60000;
                    }
                }else{
                    unfinish_num_unit = "小时";
                }
                return "";
            }
        }, new Completion<String>() {
            @Override
            public void onSuccess(Context context, String result) {
                setUpdateData();
            }

            @Override
            public void onError(Context context, Exception e) {
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.radio_button_exam:
                mViewPager.setCurrentItem(0);
                break;
            case R.id.radio_button_course:
                mViewPager.setCurrentItem(1);
                break;
            case R.id.radio_button_ask:
                mViewPager.setCurrentItem(2);
                break;
            case R.id.ll_title:
                Intent intent = new Intent(getActivity(),SubjectActivity.class);
                startActivity(intent);
                break;
            case R.id.course_fragment_ask_img:
                Intent intent_selectbook = new Intent(getActivity(), SelectBookAndKpActivity.class);
                startActivity(intent_selectbook);
                break;
        }
    }

    @Override
    public void setView(CourseAnswer courseJson,boolean isModifySp) {
        if(courseJson != null){
            unfinish_num = courseJson.getTotal();
            finish_num = courseJson.getSolveNumber();
        }else{
            unfinish_num = 0;
            unfinish_num = 0;
        }
        if(isModifySp)
            SharedPrefHelper.getInstance(getActivity()).setAnswerCache(subjectId, JSON.toJSONString(courseJson));

        setUpdateData();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public Context context() {
        return getActivity();
    }
}