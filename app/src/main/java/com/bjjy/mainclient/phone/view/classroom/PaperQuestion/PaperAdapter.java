package com.bjjy.mainclient.phone.view.classroom.PaperQuestion;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bjjy.mainclient.phone.view.exam.ExamActivity;
import com.bjjy.mainclient.phone.view.exam.activity.exampaperlist.ExamPaperListActivity;
import com.bjjy.mainclient.phone.view.exam.db.AnswerLogDB;
import com.dongao.mainclient.model.bean.course.Paper;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.view.exam.bean.AnswerLog;

import java.util.List;

/**
 * Created by dell on 2016/6/14.
 */
public class PaperAdapter extends BaseAdapter {

    private Context context;
    private List<Paper> list;
    private AnswerLogDB answerLogDB;
    private String userId,examId,subjectId;

    public PaperAdapter(Context context) {
        this.context = context;
        answerLogDB=new AnswerLogDB(context);
        examId=SharedPrefHelper.getInstance(context).getExamId();
    }

    public void setList(List<Paper> list) {
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.course_fragment_exam_item, null);
            viewHolder.title = (TextView) convertView.findViewById(R.id.title);
            viewHolder.isdo = (TextView) convertView.findViewById(R.id.tv_isdo);
            viewHolder.subTitle = (TextView) convertView.findViewById(R.id.subTitle);
            viewHolder.more = (RelativeLayout) convertView.findViewById(R.id.rl_more);
            viewHolder.exam = (LinearLayout) convertView.findViewById(R.id.ll_exampaper);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        String type=list.get(position).getType();
        viewHolder.title.setText(list.get(position).getName());
        subjectId= SharedPrefHelper.getInstance(context).getSubjectId();
        userId=SharedPrefHelper.getInstance(context).getUserId();
        final AnswerLog answerLog=answerLogDB.find(userId, examId, subjectId, type);
        if(answerLog==null){
            viewHolder.isdo.setText("暂未做题");
            viewHolder.subTitle.setText(list.get(position).getStudyRecord());
        }else{
            viewHolder.isdo.setText("上次做到");
            viewHolder.subTitle.setText(answerLog.getExaminationTitle());
        }

        viewHolder.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ExamPaperListActivity.class);
                intent.putExtra("typeName",list.get(position).getName() );
                SharedPrefHelper.getInstance(context).setMainTypeId(list.get(position).getType());
                SharedPrefHelper.getInstance(context).setExamTag(Constants.EXAM_TAG_ABILITY);
                context.startActivity(intent);
            }
        });
        viewHolder.exam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(answerLog!=null){
                    Intent intent = new Intent(context, ExamActivity.class);
//                    intent.putExtra("typeId", list.get(position).getType());
                    SharedPrefHelper.getInstance(context).setMainTypeId(list.get(position).getType());
                    SharedPrefHelper.getInstance(context).setExamTag(Constants.EXAM_DO_CONTINUE);
                    context.startActivity(intent);
                }else{
                    Intent intent = new Intent(context, ExamPaperListActivity.class);
                    intent.putExtra("typeName",list.get(position).getName() );
                    SharedPrefHelper.getInstance(context).setMainTypeId(list.get(position).getType());
                    SharedPrefHelper.getInstance(context).setExamTag(Constants.EXAM_TAG_ABILITY);
                    context.startActivity(intent);
                }
            }
        });
        return convertView;
    }

    public class ViewHolder {
        ImageView iv;
        TextView title;
        TextView subTitle;
        TextView isdo;
        RelativeLayout more;
        LinearLayout exam;
    }
}
