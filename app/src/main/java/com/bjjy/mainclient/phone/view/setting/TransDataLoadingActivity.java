package com.bjjy.mainclient.phone.view.setting;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.dongao.mainclient.core.util.FileUtil;
import com.dongao.mainclient.model.bean.course.CourseOld;
import com.dongao.mainclient.model.bean.course.CoursePlay;
import com.dongao.mainclient.model.bean.course.SubjectOld;
import com.dongao.mainclient.model.bean.play.CourseWare;
import com.dongao.mainclient.model.bean.user.MyCollection;
import com.dongao.mainclient.model.bean.user.OldCollection;
import com.dongao.mainclient.model.bean.user.OldQuesCollection;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.MyCollectionDB;
import com.dongao.mainclient.model.local.OldCollectionDao;
import com.dongao.mainclient.model.local.OldQuestionCollectionDao;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseActivity;
import com.bjjy.mainclient.phone.download.db.BeforeDB;
import com.bjjy.mainclient.phone.download.db.DownloadDB;
import com.bjjy.mainclient.phone.view.exam.utils.CommenUtils;
import com.bjjy.mainclient.phone.view.main.MainActivity;
import com.dongao.mainclient.model.bean.play.CwStudyLog;
import com.bjjy.mainclient.phone.view.play.domain.CwStudyLogDB;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by fengzongwei on 2016/6/22 0022.
 * 数据迁移时显示进度
 */
public class TransDataLoadingActivity extends BaseActivity {
    @Bind(R.id.animation_progress_img)
    ImageView imageView;
    @Bind(R.id.progressBar1)
    ProgressBar progressBar;

    AnimationDrawable animationDrawable;

    //给pb设置的进度
    int progress = 0;
    //总的任务数
    private double total_count = 4;
    //当前已完成的任务数
    private double now_count;
    //进度动画时间
    private int progress_time = 500;
    //当前的进度
    private int now_progress, copy_progress;
    //要达到的进度
    private int go_progress;

    private int test_count;//测试数据

    private boolean isAnimEnd = false;//进度的动画是否进行完全

    private int collection_count = 0, download_count =0,studylog_count =0,collection_ques_count=0;//三个数据迁移方法执行的次数
    private boolean collection_done = false,download_done = false,studylog_done = false,collection_ques_done = false;//三个数据迁移方法是否执行完成
    private List<Object> tagList = new ArrayList<Object>();

    private DownloadDB db;
    private BeforeDB beforeDb;
    private CwStudyLogDB cwStudyLogDB;
    private List<CourseOld> courseLists;
    private MyCollectionDB myCollectionDB;
    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            progressBar.setProgress(now_progress);
            if (now_progress == 100) {
                SharedPrefHelper.getInstance(TransDataLoadingActivity.this).setIsTransformAllData(true);
                Intent intent = new Intent(TransDataLoadingActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }

        ;
    };

    private Handler progressHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            if (msg.what == 0) {
                test_count++;
                setDoneCount(test_count);
            }

        }

        ;
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trans_data_activity);
        ButterKnife.bind(this);
        initView();

//        new Thread(){
//            public void run() {
//                while(now_count<total_count){
//                    try {
//                        Random random = new Random();
//                        int sleepTime = random.nextInt(1000) + 1000;
//                        Thread.sleep(sleepTime);
//                        progressHandler.sendEmptyMessage(0);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//            };
//        }.start();


        new Thread() {
            @Override
            public void run() {
                CommenUtils.initDataFromOldHistory(TransDataLoadingActivity.this);
                progressHandler.sendEmptyMessage(0);
                while (!isAnimEnd) {

                }
                while(!collection_done){
                    transFormCollection();
                }
                progressHandler.sendEmptyMessage(0);
                while (!isAnimEnd) {

                }
//                transFormDayTestData();
                while(!download_done){
                    transDownload();
                }
                progressHandler.sendEmptyMessage(0);
                while (!isAnimEnd) {

                }
                while(!studylog_done){
                    transtudyLogs();
                }
                progressHandler.sendEmptyMessage(0);
                while(!isAnimEnd){

                }
                while(!collection_ques_done){
                    transQuesCollection();
                }
                progressHandler.sendEmptyMessage(0);
            }
        }.start();

    }

    /**
     * 设置进度
     */
    private void setProgress() {
        Random random = new Random();
        int sleepTotal = random.nextInt(900) + 100;
        final int sleepTime = sleepTotal / (go_progress - now_progress);
        new Thread() {
            public void run() {
                while (now_progress < go_progress) {
                    handler.sendEmptyMessage(progress);
                    now_progress++;
                    try {
                        Thread.sleep(sleepTime);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                isAnimEnd = true;
            }
        }.start();
    }


    /**
     * 设置总的任务数
     *
     * @param totalCount
     */
    public void setTotalCount(int totalCount) {
        this.total_count = totalCount;
    }

    /**
     * 设置已完成任务数
     *
     * @param now_count
     */
    public void setDoneCount(int now_count) {
        this.now_count = now_count;
        go_progress = (int) (now_count / total_count * 100);
        tagList.add(new Object());
        setProgress();
    }

    @Override
    public void initView() {
        db = new DownloadDB(this);
        beforeDb = new BeforeDB(this);
        cwStudyLogDB = new CwStudyLogDB(this);
        myCollectionDB = new MyCollectionDB(this);
        animationDrawable = (AnimationDrawable) imageView.getDrawable();
        animationDrawable.start();
    }

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onBackPressed() {

    }


    /**
     * 迁移收藏数据
     */
    private void transFormCollection() {
        isAnimEnd = false;
        if(collection_count>2){
            collection_done = true;
            return;
        }
        collection_count++;
        OldCollectionDao oldCollectionDao = new OldCollectionDao(TransDataLoadingActivity.this);
        List<OldCollection> oldCollectionList = oldCollectionDao.getAll();
        for (int i = 0; i < oldCollectionList.size(); i++) {
            MyCollection myCollection = new MyCollection();
            myCollection.setType(MyCollection.TYPE_EXAMNATION + "");
            myCollection.setCollectionId(oldCollectionList.get(i).getQuestionId()+"");
            myCollection.setQuestionId(oldCollectionList.get(i).getQuestionId() + "");
            myCollection.setSubjectId(oldCollectionList.get(i).getSubjectId() + "");
            myCollection.setTitle(oldCollectionList.get(i).getTag());
            myCollection.setUserId(oldCollectionList.get(i).getUserId() + "");
            myCollection.setTime(oldCollectionList.get(i).getSavaTime() + "");
            myCollection.setExamId(oldCollectionList.get(i).getExamId() + "");
            myCollectionDB.insert(myCollection);
        }
        collection_done = true;
    }

    /**
     * 下载表迁移
     */
    public void transDownload() {

        isAnimEnd = false;
        if(download_count>2){
            download_done = true;
            return;
        }
        download_count++;

        List<String> userIds = beforeDb.getUsers();
        if (userIds != null && userIds.size() > 0) {
            for (int p = 0; p < userIds.size(); p++) {
                String courseData = FileUtil.getCacheCources(this, userIds.get(p));
                if (!TextUtils.isEmpty(courseData)) {
                    JSONObject object = JSON.parseObject(courseData);
                    String data = object.getString("data");
                    courseLists = JSON.parseArray(data, CourseOld.class);
                    String subjectId;
                    if (courseLists != null && courseLists.size() > 0) {
                        for (int i = 0; i < courseLists.size(); i++) {
                            List<SubjectOld> subjectOlds = courseLists.get(i).getMobileSubjectList();
                            for (int j = 0; j < subjectOlds.size(); j++) {
                                subjectId = subjectOlds.get(j).getId();
                                List<CoursePlay> coursePlays = subjectOlds.get(j).getMobileCourseList();
                                for (int k = 0; k < coursePlays.size(); k++) {
                                    String courseId = coursePlays.get(k).getId();
                                    String name = coursePlays.get(k).getName();
                                    String teacherName = coursePlays.get(k).getDaPersonName();
                                    String imgUrl = coursePlays.get(k).getDaPersonImg();
                                    db.addCourseOld(subjectId, courseId, imgUrl, name, teacherName);
                                }
                            }
                        }
                    }
                }
            }
        }


        List<CourseWare> list = beforeDb.beforeData();
        if (list != null && list.size() > 0) {
            for (CourseWare cw : list) {
                if(cw.getState()>=0 && cw.getState()<100){
                    db.addTransDatas(cw.getUserId(), cw.getExamId(), cw.getSubjectId(), cw.getClassId(), cw.getCwId(), cw.getSectionId(),Constants.STATE_Pause, cw.getState(), FileUtil.getDownloadPath(this), cw.getCwBean(), cw.getCourseBean(), 1);
                }else if(cw.getState()==100){
                    db.addTransDatas(cw.getUserId(), cw.getExamId(), cw.getSubjectId(), cw.getClassId(), cw.getCwId(), cw.getSectionId(),Constants.STATE_DownLoaded, 100, FileUtil.getDownloadPath(this), cw.getCwBean(), cw.getCourseBean(), 1);
                }
            }
        }
        download_done = true;
    }

    /**
     * 学习记录表迁移
     */
    public void transtudyLogs() {
        isAnimEnd = false;
        if(studylog_count>2){
            studylog_done = true;
            return;
        }
        studylog_count++;
        List<CwStudyLog> studyLogs = beforeDb.getPreStudyLog();
        if (studyLogs != null && studyLogs.size() > 0) {
            for (CwStudyLog studylog : studyLogs) {
                CwStudyLog log = cwStudyLogDB.query(studylog.getUserId(), studylog.getCwid(), studylog.getCourseId());
                if (log == null) {
                    cwStudyLogDB.insert(studylog);
                }
            }
        }
        studylog_done = true;
    }

    /**
     * 收藏的答疑迁移
     */
    public void transQuesCollection(){
        isAnimEnd = false;
        if(collection_ques_count>2){
            collection_ques_done = true;
            return;
        }
        collection_ques_count++;
        OldQuestionCollectionDao oldQuestionCollectionDao = new OldQuestionCollectionDao(this);
        List<OldQuesCollection> quesCollections = oldQuestionCollectionDao.getAnswers();
        for(int i=0;i<quesCollections.size();i++){
            if(quesCollections.get(i).getSavedTime()>0){
                MyCollection myCollection = new MyCollection();
                if(!TextUtils.isEmpty(quesCollections.get(i).getFinalTitle()))
                    myCollection.setTitle(quesCollections.get(i).getFinalTitle());
                else
                    myCollection.setTitle(quesCollections.get(i).getTitle());
                myCollection.setCollectionId(quesCollections.get(i).getUid()+"");
                myCollection.setType(MyCollection.TYPE_QUESTION);
                myCollection.setUserId(quesCollections.get(i).getUserid() + "");
                myCollection.setTime(quesCollections.get(i).getSavedTime() + "");
                myCollectionDB.insert(myCollection);
            }
        }
        collection_ques_done = true;
    }


    public void transCourseDetail() {
        List<String> courseIds = db.findCourseIds(SharedPrefHelper.getInstance(this).getUserId());
        for (int i = 0; i < courseIds.size(); i++) {
            String courseData = FileUtil.getCacheCourcesDetail(this, SharedPrefHelper.getInstance(this).getUserId(), courseIds.get(i));
        }

    }

}
