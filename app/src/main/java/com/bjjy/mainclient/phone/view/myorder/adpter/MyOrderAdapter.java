package com.bjjy.mainclient.phone.view.myorder.adpter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


import com.bjjy.mainclient.phone.view.myorder.MyOrderFragment;

import java.util.List;

/**
 * Created by fengzongwei on 2016/4/6.
 */
public class MyOrderAdapter extends FragmentPagerAdapter {

    private static final String[] titles = { "未付款","已付款" };
    private MyOrderFragment myOrderFragment_unpay = new MyOrderFragment(),myOrderFragment_pay= new MyOrderFragment();
    private List<Object> list;

    public MyOrderAdapter(FragmentManager fm){
        super(fm);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    @Override
    public Fragment getItem(int position) {
        Bundle bundle = new Bundle();
        if(position == 0){
            bundle.putString("type","unpaid");
            myOrderFragment_unpay.setArguments(bundle);
            return myOrderFragment_unpay;
        }else{
            bundle.putString("type","paid");
            myOrderFragment_pay.setArguments(bundle);
            return myOrderFragment_pay;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    public void freshData(){
        myOrderFragment_unpay.initData();
        myOrderFragment_pay.initData();
    }

    /**
     * 重新登录后进行数据的刷新
     */
    public void loginCallBack(){
        myOrderFragment_pay.initData();
        myOrderFragment_unpay.initData();
    }

}
