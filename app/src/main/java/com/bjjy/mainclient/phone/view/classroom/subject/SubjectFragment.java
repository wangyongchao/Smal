/*
 * Copyright (C) 2013 Andreas Stuetz <andreas.stuetz@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bjjy.mainclient.phone.view.classroom.subject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bjjy.mainclient.phone.app.BaseFragment;
import com.bjjy.mainclient.phone.event.UpdateEvent;
import com.bjjy.mainclient.phone.view.setting.WebViewActivity;
import com.bjjy.mainclient.phone.widget.EmptyViewLayout;
import com.dongao.mainclient.model.bean.course.Exam;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.remote.ApiService;
import com.bjjy.mainclient.phone.R;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.bjjy.mainclient.phone.utils.NetworkUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class SubjectFragment extends BaseFragment implements SubjectFragmentView {


    private List<Exam> mList;
    private SubjectExpandableListViewAdapter mSectionAdapter;

    @Bind(R.id.course_list)
    ExpandableListView mListView;

    @Bind(R.id.swipe_container)
    SwipeRefreshLayout mSwipeRefreshLayout;

    private EmptyViewLayout mEmptyLayout;


    private SubjectFragmentPersenter courseFragmentPersenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //  EventBus.getDefault().register(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fl = inflater.inflate(R.layout.course_subject_fragment, null);
        ButterKnife.bind(this, fl);
        courseFragmentPersenter = new SubjectFragmentPersenter();
        courseFragmentPersenter.attachView(this);
        initView();
        initData();
        return fl;
    }


    @Override
    public void initView() {
        // TODO Auto-generated method stub
        mEmptyLayout = new EmptyViewLayout(getActivity(),mSwipeRefreshLayout);

        ViewGroup mEmptyView = (ViewGroup) LayoutInflater.from(getActivity()).inflate(R.layout.app_view_empty_book,null);
        TextView emptyTv = (TextView) mEmptyView.findViewById(R.id.app_message_tv2);
        ImageView emptyIv = (ImageView) mEmptyView.findViewById(R.id.app_view_iv);
        mEmptyLayout.setEmptyView(mEmptyView);
        mEmptyLayout.setEmptyImageId(R.drawable.pic_course_empty);
        emptyIv.setImageResource(R.drawable.pic_course_empty);
        mEmptyLayout.setShowEmptyImage(true);
        mEmptyLayout.setShowEmptyButton(true);
        mEmptyLayout.setTitleEmptyButton("购课中心");
        mEmptyLayout.setShowEmptyButton(true);
        emptyTv.setText("您还没有课程，先去购买吧！");
        mEmptyLayout.setEmptyButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), WebViewActivity.class);
                intent.putExtra(Constants.APP_WEBVIEW_TITLE, "选课购买");
                intent.putExtra(Constants.APP_WEBVIEW_URL, ApiService.SELECT_COURSE_URL);
                startActivity(intent);
            }
        });

        mEmptyLayout.setErrorButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initData();

            }
        });
        setlistener();
    }


    @Override
    public void initData() {
       // mEmptyLayout.showEmpty();
        if (NetworkUtil.isNetworkAvailable(getActivity())) {
            mEmptyLayout.showLoading();
            courseFragmentPersenter.getData();
        } else {
            mEmptyLayout.showNetErrorView();
        }

    }

    private void setlistener() {

        mSwipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.color_primary));
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                showLoading();
                courseFragmentPersenter.getData();
            }
        });

        mListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                SharedPrefHelper.getInstance(getActivity()).setSubjectId(mList.get(groupPosition).getSubjectInfoList().get(childPosition).getSubjectId() + "");
                SharedPrefHelper.getInstance(getActivity()).setSubjectName(mList.get(groupPosition).getSubjectInfoList().get(childPosition).getSubjectName() + "(" + mList.get(groupPosition).getSubjectInfoList().get(childPosition).getYear() + ")");
                SharedPrefHelper.getInstance(getActivity()).setExamId(mList.get(groupPosition).getExamId()+"");
                EventBus.getDefault().post(new UpdateEvent(true));
                getActivity().finish();

                return false;
            }
        });

        mListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                // TODO Auto-generated method stub
                return true;
            }
        });
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
    }

    @Override
    public void setAdapter(List<Exam> mList) {
        hideLoading();
        this.mList = mList;
        mSectionAdapter = new SubjectExpandableListViewAdapter(getActivity());
        mSectionAdapter.setList((ArrayList) mList);
        if (mList != null && !mList.isEmpty()) {
            mEmptyLayout.showContentView();
            mListView.setAdapter(mSectionAdapter);
            for(int i = 0; i < mSectionAdapter.getGroupCount(); i++){
                mListView.expandGroup(i);
            }
        }else{
            mEmptyLayout.showEmpty();
        }
    }

    @Override
    public void showLoading() {
        if(mSwipeRefreshLayout != null){
            mSwipeRefreshLayout.setRefreshing(true);
        }
    }

    @Override
    public void hideLoading() {
        if(mSwipeRefreshLayout != null){
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }


    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {
        hideLoading();
        mEmptyLayout.showError();
    }

    @Override
    public Context context() {
        return getActivity();
    }
}