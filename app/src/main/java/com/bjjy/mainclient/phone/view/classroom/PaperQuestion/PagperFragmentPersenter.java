package com.bjjy.mainclient.phone.view.classroom.PaperQuestion;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.bean.course.Paper;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ParamsUtils;
import com.dongao.mainclient.model.remote.bean.BaseBean;

import java.util.List;


/**
 * 课堂首页Persenter
 */
public class PagperFragmentPersenter extends BasePersenter<PaperFragmentView> {
    private List<Paper> papers ;

    @Override
    public void attachView(PaperFragmentView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    @Override
    public void getData(){
        //一些业务逻辑的判断 有网没网 是请求网络还是请求数据库
        getMvpView().showLoading();
        apiModel.getData(ApiClient.getClient().getPaperList(
                ParamsUtils.getInstance(getMvpView().context()).paperList()));
    }

    @Override
    public void setData(String obj) {
        getMvpView().hideLoading();
        try {
            BaseBean baseBean = JSON.parseObject(obj,BaseBean.class);
            if(baseBean == null){
                getMvpView().showError("");
                return;
            }

            if(baseBean.getCode()==1000){
                SharedPrefHelper.getInstance(getMvpView().context()).setPaperCache(getMvpView().getSubjectId(),obj);
                JSONObject object=JSON.parseObject(baseBean.getBody());
                papers=JSON.parseArray(object.getString("examinationList"),Paper.class);
                getMvpView().setData(papers);
            }else{
                getMvpView().showError("");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
