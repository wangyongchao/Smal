package com.bjjy.mainclient.phone.view.studybar.push.bean;

/**
 * Created by wyc on 2016/6/13.
 */
public class Push {
    private String pushMessageId;//消息Id
    private String title;//名称
    private String url;//打开网页的地址
    private String messageTypeValue;//消息的类型
    private String pushTypeName;//推送的消息的名称
    private String questionId;//问题ID
    private String subContent;//副标题

    public String getPushMessageId() {
        return pushMessageId;
    }

    public void setPushMessageId(String pushMessageId) {
        this.pushMessageId = pushMessageId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMessageTypeValue() {
        return messageTypeValue;
    }

    public void setMessageTypeValue(String messageTypeValue) {
        this.messageTypeValue = messageTypeValue;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getSubContent() {
        return subContent;
    }

    public void setSubContent(String subContent) {
        this.subContent = subContent;
    }

    public String getPushTypeName() {
        return pushTypeName;
    }

    public void setPushTypeName(String pushTypeName) {
        this.pushTypeName = pushTypeName;
    }
}
