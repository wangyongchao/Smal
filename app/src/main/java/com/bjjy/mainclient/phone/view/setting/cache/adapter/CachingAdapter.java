package com.bjjy.mainclient.phone.view.setting.cache.adapter;

import android.app.Activity;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bjjy.mainclient.phone.download.db.DownloadDB;
import com.bjjy.mainclient.phone.widget.DialogManager;
import com.dongao.mainclient.model.bean.play.CourseWare;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.bjjy.mainclient.phone.R;

import java.util.List;

/**
 * Created by dell on 2016/5/16.
 */
public class CachingAdapter extends BaseAdapter {

    private Activity mContext;
    private List<CourseWare> mList;
    private DownloadDB db;
    private String userId;
    private Handler mHandler;

    public CachingAdapter(Activity context, Handler handler) {
        super();
        this.mContext = context;
        this.mHandler = handler;
        db = new DownloadDB(context);
        userId = SharedPrefHelper.getInstance(context).getUserId();
    }

    public void setList(List<CourseWare> list) {
        this.mList = list;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = View.inflate(mContext, R.layout.caching_item, null);
            holder.title = (TextView) convertView.findViewById(R.id.download_tv_title);
            holder.tv_state = (TextView) convertView.findViewById(R.id.tv_state);
            holder.percent = (TextView) convertView.findViewById(R.id.tv_percent);
            holder.pb = (ProgressBar) convertView.findViewById(R.id.pg_down);
            holder.delete = (ImageView) convertView.findViewById(R.id.caching_img);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        int percent = db.getPercent(userId, mList.get(position).getClassId(), mList.get(position).getCwId());
        int state = db.getState(userId, mList.get(position).getClassId(), mList.get(position).getCwId());

        holder.pb.setProgress(percent);
        holder.percent.setText(percent + "%");

        holder.title.setText(mList.get(position).getCwName());

        if (state == Constants.STATE_DownLoaded) {
            holder.tv_state.setText("已下载");
        } else if (state == Constants.STATE_DownLoading) {
            holder.tv_state.setText("缓存中");
        } else if (state == Constants.STATE_Pause) {
            holder.tv_state.setText("已暂停");
        } else if (state == Constants.STATE_Waiting) {
            holder.tv_state.setText("等待中");
        } else if (state == Constants.STATE_Error) {
            holder.tv_state.setText("下载失败");
        }
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogManager.showNormalDialog(mContext, "确定要删除吗", "提示", "取消", "确定",
                        new DialogManager.CustomDialogCloseListener() {
                            @Override
                            public void yesClick() {
                                db.deleteCourseWare(userId, mList.get(position));
                                mHandler.sendEmptyMessage(1313);
                            }

                            @Override
                            public void noClick() {
                            }
                        });

            }
        });
        return convertView;
    }

    class ViewHolder {
        TextView title;
        TextView tv_state;
        TextView percent;
        ImageView delete;
        ProgressBar pb;
    }
}
