package com.bjjy.mainclient.phone.view.user.bean;

import java.io.Serializable;

/**
 * Created by wycwo on 2017/10/28.
 */

public class User implements Serializable{
	private String uid;
	private String periodId;//合作周期ID
	private String partnerId;//合作机构ID
	private String mobileAccessToken;

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getPeriodId() {
		return periodId;
	}

	public void setPeriodId(String periodId) {
		this.periodId = periodId;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public String getMobileAccessToken() {
		return mobileAccessToken;
	}

	public void setMobileAccessToken(String mobileAccessToken) {
		this.mobileAccessToken = mobileAccessToken;
	}
}
