package com.bjjy.mainclient.phone.view.play.downloadmanager;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ExpandableListView;

import com.dongao.mainclient.model.bean.play.CourseChapter;
import com.dongao.mainclient.model.bean.play.CourseWare;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.utils.NetWorkUtils;
import com.bjjy.mainclient.phone.view.play.adapter.WrapperExpandableListAdapter;
import com.bjjy.mainclient.phone.view.play.downloadmanager.adapter.CourseWareEpListAdapter;
import com.bjjy.mainclient.phone.widget.DialogManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

/**
 * Created by yunfei on 2016/11/30.
 */

public class CoursWareEpListFragment extends AbsCoursListFragment implements ExpandableListView.OnChildClickListener {

    private CourseWareEpListAdapter adapter;
    @Bind(R.id.lv_main)
    ExpandableListView lv_main;

    public static CoursWareEpListFragment getNewInstance(ArrayList<CourseChapter> data) {
        CoursWareEpListFragment coursWareListFragment = new CoursWareEpListFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("data", data);
        coursWareListFragment.setArguments(bundle);
        return coursWareListFragment;
    }

    @Override
    protected int getContentId() {
        return R.layout.pay_download_cours_ware_eplist_fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ArrayList<CourseChapter> data = (ArrayList<CourseChapter>) getArguments().getSerializable("data");
        presenter.setCourseChapterList(data);
        presenter.getData();
    }

    @Override
    public void initView() {
        adapter = new CourseWareEpListAdapter(context(), onItemSelectNumChangedListener, presenter.getCourseWaresChecked());
        lv_main.setAdapter(new WrapperExpandableListAdapter(adapter));
        lv_main.setOnChildClickListener(this);
    }


    @Override
    public void setCourseChapterList(List<CourseChapter> courseChapters) {
        adapter.setData(courseChapters);
    }


    @Override
    public void updateList() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void setCourseWareList(List<CourseWare> list) {
        //ignore
    }


    @Override
    public boolean onChildClick(ExpandableListView parent, View v, final int groupPosition, final int childPosition, long id) {

        CourseWare courseWare = (CourseWare) parent.getExpandableListAdapter().getChild(groupPosition, childPosition);

        if (presenter.isAddDownloadDB(courseWare.getState())) {
            showRepeatDownLoadHit();
            return false;
        }

        NetWorkUtils type = new NetWorkUtils(getActivity());
        if (type.getNetType() == 0) {//无网络
            showNetWorkErrorHit();
        } else if (type.getNetType() == 2) { //流量
//            if (SharedPrefHelper.getInstance(getActivity()).getIsNoWifiPlayDownload()) {
                DialogManager.showNormalDialog(getActivity(), getResources().getString(R.string.dialog_warnning_vedio), "提示", "取消", "确定",
                        new DialogManager.CustomDialogCloseListener() {
                            @Override
                            public void yesClick() {
                                presenter.setOnChapterChildClick(groupPosition, childPosition);
                            }

                            @Override
                            public void noClick() {

                            }
                        });
//            }
        } else {
            presenter.setOnChapterChildClick(groupPosition, childPosition);
        }


        return false;
    }
}
