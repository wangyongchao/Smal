package com.bjjy.mainclient.phone.view.play.utils;

import com.dongao.mainclient.model.common.Constants;

public interface Constans {

	public static final String M3U8_URL =  "http://172.16.0.220/gsj_zjjc_jjf_0207/gsj_zjjc_jjf_0207_640x360_200kbps_m3u8_10/gsj_zjjc_jjf_0207_640x360_200kbps_m3u8_10.m3u8";//获取m3u8的路径，在线播放
	
	public static final String M3U8_URL_OFF = "http://appdownload.bjsteach.com/2014zs/ck/zzf/jc/ydb/14zsjc_zzfjc_ck_061_qy_yd_2/upload/media/m3u8_10/video.m3u8";//获取m3u8的路径，本地播放
	
	public static final String USER_ID = "24271679";//测试数据
	
	public static final String VID = "video_01";//测试数据，在线播放

	public static final String VID_ONLINE = "20100962";
	
	public static final String VID_OFFONLINE = "videoOffline";//测试数据，本地播放
	
	public static final String M3U8_URL_HEAD = "http://172.16.0.220/gsj_zjjc_jjf_0207/gsj_zjjc_jjf_0207_640x360_200kbps_m3u8_10/";//拼接ts下载路径的url前缀
	
	public static final String DEVICE_NAME="ios-kaoqian-v1.0";//获取Key接口设置user-agent时用到的device-token
	
	public static final String TYPE1 = "1";//加密类型（暂时只支持1即AES-128-CBC）

	public static final int port = 13145;	//httpServer端口

	public static final String TIME_STAMP = "123";//测试数据，时间戳

	public static final String M3U8_KEY_SUB = ",URI=\"http://localhost:"+ Constants.SERVER_PORT+"/";
}
