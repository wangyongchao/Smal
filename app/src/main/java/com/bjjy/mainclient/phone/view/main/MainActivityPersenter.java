package com.bjjy.mainclient.phone.view.main;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ParamsUtils;
import com.dongao.mainclient.model.remote.bean.BaseBean;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * 课堂首页Persenter
 */
public class MainActivityPersenter extends BasePersenter<MainView> {

    @Override
    public void attachView(MainView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    @Override
    public void getData(){
        double latitude=getMvpView().getLatitude();
        double lontitude=getMvpView().getLontitude();
        String addr=getMvpView().getAddr();
        apiModel.getData(ApiClient.getClient().upLoadPositon(
                ParamsUtils.getInstance(getMvpView().context()).upLoadPosition(latitude, lontitude, addr)));
    }

    @Override
    public void setData(String obj) {
        try {
            JSONObject object=JSON.parseObject(obj);
            if(object.getIntValue("code")==1){  //成功
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        getMvpView().setResult();
    }

    public void upLoadVideos(String json){
//        json="{\"userCode\":\"tianya\",\"listenDtos\":[{\"year\":\"2017\",\"cwCode\":\"17xfks-tlz-jssw-1\",\"videoDtos\":[{\"videoID\":\"0\",\"listenTime\":\"30\",\"nowPlayingTime\":\"40\",\"createdTime\":\"1970-01-01 08:00:30\",\"lastUpdateTime\":\"2016-02-03 14:10:18\"}]}]}]}";
        Call<String> call = ApiClient.getClient().upLoadVideos(ParamsUtils.getInstance(getMvpView().context()).uploadVideos(json));
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    String str = response.body();
                    try {
                        BaseBean baseBean = JSON.parseObject(str, BaseBean.class);
                        int result = baseBean.getCode();
                        if (result == 0 && baseBean.getResult().getCode() == Constants.RESULT_CODE) {
                            result = Constants.RESULT_CODE;
                        }
                        getMvpView().setCodet(result);

                    } catch (Exception e) {

                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }
}
