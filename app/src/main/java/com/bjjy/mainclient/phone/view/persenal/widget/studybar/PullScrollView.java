package com.bjjy.mainclient.phone.view.persenal.widget.studybar;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.ScrollView;

import com.bjjy.mainclient.phone.R;


/**
 * 自定义ScrollView
 *
 * @author wyc
 * @date 2016-06-03
 */
public class PullScrollView extends ScrollView {
    /*  wyc  */
    private OnScrollListener onScrollListener;//通过setOnScrollListener()方法把Activity中的监听对象传入本类中使用
    /**
     * 主要是用在用户手指离开MyScrollView，MyScrollView还在继续滑动，我们用来保存Y的距离，然后做比较
     */
    private int lastScrollY;
    /**
     * 用于用户手指离开MyScrollView的时候获取MyScrollView滚动的Y距离，然后回调给onScroll方法中
     */
    private Handler handler = new Handler() {

        public void handleMessage(android.os.Message msg) {
            int scrollY = PullScrollView.this.getScrollY();//获得当前滚动到的y坐标值

            //此时的距离和记录下的距离不相等，再隔5毫秒给handler发送消息
            if(lastScrollY != scrollY){
                lastScrollY = scrollY;//把当前滚到的y坐标值赋给lastScrollY变量，以便下次比较
                handler.sendMessageDelayed(handler.obtainMessage(), 50);
            }
            if(onScrollListener != null){
                onScrollListener.onScroll(scrollY);//把获得的当前滚动值传入，执行监听器中的处理
            }

        };

    };

    /**
     * 滚动的回调接口，用于在滑动当前scroll组件时把滚动的值回传给Activity进行判断与处理
     * @author xiaanming
     *
     */
    public interface OnScrollListener{
        /**
         * 回调方法， 返回MyScrollView滑动的Y方向距离
         * @param scrollY
         */
        public void onScroll(int scrollY);
    }

    /**
     * 设置滚动接口
     * @param onScrollListener
     */
    public void setOnScrollListener(OnScrollListener onScrollListener) {
        this.onScrollListener = onScrollListener;
    }

    /**
     * 滑动事件
     */
//    @Override
//    public void fling(int velocityY) {
//        super.fling(velocityY / 4);
//    }
    
    //TODO 新方式
    /**
     *
     * 滚动的回调接口
     *
     * @author xiaanming
     *
     */
    public interface newOnScrollListener{
        /**
         * 回调方法， 返回MyScrollView滑动的Y方向距离
         * @param scrollY
         * 				、
         */
        public void onScroll(int scrollY);
    }
    private newOnScrollListener newListener;
    /**
     * 设置滚动接口
     * @param onScrollListener
     */
    public void setnewOnScrollListener(newOnScrollListener onScrollListener) {
        this.newListener = onScrollListener;
    }
    /**
     *
     * 滚动的回调接口
     *
     * @author xiaanming
     *
     */
    public interface onShowListener{
        /**
         * 回调方法， 返回MyScrollView滑动的Y方向距离
         * @param show
         * 				、
         */
        public void onShow(boolean show);
    }
    private onShowListener showListener;
    
    public void setShowListener(onShowListener showListener) {
        this.showListener = showListener;
    }

    /**
     *
     * 正常滚动的回调接口
     *
     * @author xiaanming
     *
     */
    public interface onNormalMovingListener{
        /**
         * 回调方法， 返回MyScrollView滑动的Y方向距离
         * @param show
         * 				、
         */
        public void onShow(boolean show);
    }
    private onNormalMovingListener normalMovingListener;

    public void setNormalMovingListener(onNormalMovingListener normalMovingListener) {
        this.normalMovingListener = normalMovingListener;
    }





    /** 阻尼系数,越小阻力就越大. */
    private static final float SCROLL_RATIO = 0.3f;

    /** 滑动至翻转的距离. */
    private static final int TURN_DISTANCE = 100;

    /** 头部view. */
    private View mHeader;

    /** 头部view高度. */
    private int mHeaderHeight;

    /** 头部view显示高度. */
    private int mHeaderVisibleHeight;

    /** ScrollView的content view. */
    private View mContentView;

    /** ScrollView的content view矩形. */
    private Rect mContentRect = new Rect();

    /** 首次点击的Y坐标. */
    private float mTouchDownY;

    /** 是否关闭ScrollView的滑动. */
    private boolean mEnableTouch = false;

    /** 是否开始移动. */
    private boolean isMoving = false;

    /** 是否移动到顶部位置. */
    private boolean isTop = false;

    /** 头部图片初始顶部和底部. */
    private int mInitTop, mInitBottom;

    /** 头部图片拖动时顶部和底部. */
    private int mCurrentTop, mCurrentBottom;

    /** 状态变化时的监听器. */
    private OnTurnListener mOnTurnListener;

    private enum State {
        /**顶部*/
        UP,
        /**底部*/
        DOWN,
        /**正常*/
        NORMAL
    }

    /** 状态. */
    private State mState = State.NORMAL;

    public PullScrollView(Context context) {
        super(context);
        init(context, null);
    }

    public PullScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public PullScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        // set scroll mode
        setOverScrollMode(OVER_SCROLL_NEVER);

        if (null != attrs) {
            TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.PullScrollView);

            if (ta != null) {
                mHeaderHeight = (int) ta.getDimension(R.styleable.PullScrollView_headerHeight, -1);
                mHeaderVisibleHeight = (int) ta.getDimension(R.styleable
                        .PullScrollView_headerVisibleHeight, -1);
                ta.recycle();
            }
        }
    }

    /**
     * 设置Header
     *
     * @param view
     */
    public void setHeader(View view) {
        mHeader = view;
    }

    /**
     * 设置状态改变时的监听器
     *
     * @param turnListener
     */
    public void setOnTurnListener(OnTurnListener turnListener) {
        mOnTurnListener = turnListener;
    }

    @Override
    protected void onFinishInflate() {
        if (getChildCount() > 0) {
            mContentView = getChildAt(0);
        }
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        
        if (getScrollY() == 0) {
            isTop = true;
        }
        //TODO
        if(newListener != null){
            newListener.onScroll(t);
        }
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        try {
            if (ev.getAction() == MotionEvent.ACTION_DOWN) {
                mTouchDownY = ev.getY();
                mCurrentTop = mInitTop = mHeader.getTop();
                mCurrentBottom = mInitBottom = mHeader.getBottom();
            }
        }catch (Exception e){}
       
        return super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        try {
            if(onScrollListener != null){//如果监听器已创建，则在有触屏事件时把滚动组件滚动到的y坐标记录到lastScrollY,并传给监听器
                onScrollListener.onScroll(lastScrollY = this.getScrollY());//一个赋值语句，一个事件处理语句
            }
            if (mContentView != null) {
                doTouchEvent(ev);
            }

            // 禁止控件本身的滑动.
            return mEnableTouch || super.onTouchEvent(ev);
        }catch (Exception e){
            return super.onTouchEvent(ev);
        }
       
    }

    /**
     * 触摸事件处理
     *
     * @param event
     */
    private void doTouchEvent(MotionEvent event) {
        int action = event.getAction();
        switch (action) {
            case MotionEvent.ACTION_MOVE:
                doActionMove(event);
                break;

            case MotionEvent.ACTION_UP:
                handler.sendMessageDelayed(handler.obtainMessage(), 5); // 手离开触屏,延时5毫秒发消息给handler
                // 回滚动画
                if (isNeedAnimation()) {
                    rollBackAnimation();
                }

                if (getScrollY() == 0) {
                    mState = State.NORMAL;

                    // 滑动经过顶部初始位置时，修正Touch down的坐标为当前Touch点的坐标
//                    if (isTop) {
//                        isTop = false;
//                        mTouchDownY = event.getY();
//                    }
//                    mState = State.NORMAL;
                    if (showListener!=null){
//                        showListener.onShow(true);
                    }
                }

                isMoving = false;
                mEnableTouch = false;
                break;

            default:
                break;
        }
    }

    /**
     * 执行移动动画
     *
     * @param event
     */
    private void doActionMove(MotionEvent event) {
        // 当滚动到顶部时，将状态设置为正常，避免先向上拖动再向下拖动到顶端后首次触摸不响应的问题
        int num=getScrollY();
//        if (getScrollY() == 0) {
//            mState = State.NORMAL;
//
//            // 滑动经过顶部初始位置时，修正Touch down的坐标为当前Touch点的坐标
//            if (isTop) {
//                isTop = false;
//                mTouchDownY = event.getY();
//            }
//        }

        float deltaY = event.getY() - mTouchDownY;

        // 对于首次Touch操作要判断方位：UP OR DOWN
        if (deltaY < 0 && mState == State.NORMAL) {
            mState = State.UP;
        } else if (deltaY > 0 && mState == State.NORMAL) {
            mState = State.DOWN;
        }

        if (mState == State.UP) {
            deltaY = deltaY < 0 ? deltaY : 0;

            isMoving = false;
            mEnableTouch = false;
            normalMovingListener.onShow(true);
        } else if (mState == State.DOWN) {
            if (getScrollY() <= deltaY) {
                mEnableTouch = true;
                isMoving = true;
                normalMovingListener.onShow(false);
            }
            deltaY = deltaY < 0 ? 0 : deltaY;
        }

        if (isMoving) {
            if (showListener!=null){
                showListener.onShow(false);
            }
            // 初始化content view矩形
            if (mContentRect.isEmpty()) {
                // 保存正常的布局位置
                mContentRect.set(mContentView.getLeft(), mContentView.getTop(), mContentView.getRight(),
                        mContentView.getBottom());
            }

            // 计算header移动距离(手势移动的距离*阻尼系数*0.5)
            float headerMoveHeight = deltaY * 0.5f * SCROLL_RATIO;
            mCurrentTop = (int) (mInitTop + headerMoveHeight);
            mCurrentBottom = (int) (mInitBottom + headerMoveHeight);

            // 计算content移动距离(手势移动的距离*阻尼系数)
            float contentMoveHeight = deltaY * SCROLL_RATIO;

            // 修正content移动的距离，避免超过header的底边缘
            int headerBottom = mCurrentBottom - mHeaderVisibleHeight;
            int top = (int) (mContentRect.top + contentMoveHeight);
            int bottom = (int) (mContentRect.bottom + contentMoveHeight);

            if (top <= headerBottom) {
                // 移动content view
                mContentView.layout(mContentRect.left, top, mContentRect.right, bottom);

                // 移动header view
                mHeader.layout(mHeader.getLeft(), mCurrentTop, mHeader.getRight(), mCurrentBottom);
            }
        }else {
            if (getScrollY() == 0) {
                mState = State.NORMAL;

                // 滑动经过顶部初始位置时，修正Touch down的坐标为当前Touch点的坐标
                if (isTop) {
                    isTop = false;
                    mTouchDownY = event.getY();
                }
            }
            if (showListener!=null){

                showListener.onShow(true);
            }
        }
    }

    private void rollBackAnimation() {
        TranslateAnimation tranAnim = new TranslateAnimation(0, 0,
                Math.abs(mInitTop - mCurrentTop), 0);
        tranAnim.setDuration(200);
        mHeader.startAnimation(tranAnim);

        mHeader.layout(mHeader.getLeft(), mInitTop, mHeader.getRight(), mInitBottom);

        // 开启移动动画
        TranslateAnimation innerAnim = new TranslateAnimation(0, 0, mContentView.getTop(), mContentRect.top);
        innerAnim.setDuration(200);
        mContentView.startAnimation(innerAnim);
        mContentView.layout(mContentRect.left, mContentRect.top, mContentRect.right, mContentRect.bottom);

        mContentRect.setEmpty();

        // 回调监听器
        if (mCurrentTop > mInitTop + TURN_DISTANCE && mOnTurnListener != null){
            mOnTurnListener.onTurn();
        }
    }

    /**
     * 是否需要开启动画
     */
    private boolean isNeedAnimation() {
        return !mContentRect.isEmpty() && isMoving;
    }

    /**
     * 翻转事件监听器
     *
     * @author markmjw
     */
    public interface OnTurnListener {
        /**
         * 翻转回调方法
         */
        public void onTurn();
    }
    
}
