package com.bjjy.mainclient.phone.view.daytest;

import com.dongao.mainclient.model.mvp.MvpView;
import com.dongao.mainclient.model.bean.daytest.DayTestExam;

import java.util.List;

/**
 * Created by feogzongwei on 2016/5/20 0020.
 */
public interface SelectSubjectView extends MvpView {

    void showData(List<DayTestExam> selectSubjectData);//显示数据
}
