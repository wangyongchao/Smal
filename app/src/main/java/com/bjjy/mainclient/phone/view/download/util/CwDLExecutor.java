package com.bjjy.mainclient.phone.view.download.util;

import android.content.Context;

import com.bjjy.mainclient.phone.view.download.DownloadTask;
import com.bjjy.mainclient.phone.view.download.db.DownloadDB;
import com.dongao.mainclient.core.download.DownloadManager;
import com.dongao.mainclient.core.download.DownloadRequest;
import com.dongao.mainclient.core.download.YunDownloadManager;
import com.dongao.mainclient.core.util.FileUtil;
import com.dongao.mainclient.core.util.LogUtils;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.core.download.DownloadStatusListener;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CwDLExecutor {

	/** 最大连接数 */
	public final static int MAX_TOTAL_CONNECTIONS = 128;
	/** 获取连接的最大等待时间 */
	public final static int WAIT_TIMEOUT = 60000;
	/** 每个路由最大连接数 */
	public final static int MAX_ROUTE_CONNECTIONS = 64;
	/** 连接超时时间 */
	public final static int CONNECT_TIMEOUT = 10000;
	/** 读取超时时间 */
	public final static int READ_TIMEOUT = 10000;

	private DefaultHttpClient defaultHttpClient;
	private DownloadTask currTask;

	private Context mContext;
    private DownloadDB downloadDB;

    private YunDownloadManager downloadManager;
    private static final int DOWNLOAD_THREAD_POOL_SIZE = 1;
    MyDownloadStatusListener myDownloadStatusListener = new MyDownloadStatusListener();


	public CwDLExecutor(Context context) {
        downloadManager = new YunDownloadManager(DOWNLOAD_THREAD_POOL_SIZE);
		this.defaultHttpClient = getHttpClient();
		HttpParams params = defaultHttpClient.getParams();
		HttpConnectionParams.setConnectionTimeout(params, 8000);
		HttpConnectionParams.setSoTimeout(params, 30000 * 2);
		DefaultHttpRequestRetryHandler retryHandler = new DefaultHttpRequestRetryHandler(3, true);
		defaultHttpClient.setHttpRequestRetryHandler(retryHandler);// 如果请求超时会继续重试请求3次
		this.mContext = context;
        downloadDB = new DownloadDB(mContext);

	}

	public void start(DownloadTask task) {
        //首先是在DownloadService里面已经做了判断，判断不重复执行相同的任务 这里再做第二层判断 ps:还是先去掉吧
      // if(currTask != null && currTask.equals(task)) return;

       this.currTask = task;
       currTask.getFileList().clear();
       try {
           currTask.setStatus(DownloadTask.BUFFING);
           downloadDB.update(currTask);
           //  EventBus.getDefault().post(new StatusEvent(true));

           // 解析M3U8文件加入到下载列表 ( 把TS加入到多线程下载列表 )
           CwDLExecutor.this.resolveM3U8File();
           // 检查硬盘空间
           CwDLExecutor.this.checkStorage();
           // 解析讲义加入到下载列表 ( 加入到多线程下载列表 )
           CwDLExecutor.this.resolveLecture();
           CwDLExecutor.this.resolveCaption();

           //解析完毕开始下载
           currTask.setStatus(DownloadTask.DOWNLOADING);
           downloadDB.update(currTask);
           CwDLExecutor.this.downloadFile();

       } catch (DownloadException e) {
           e.printStackTrace();
           currTask.setStatus(DownloadTask.BUFFING_ERROR);
           downloadDB.update(currTask);
       }
	}

    public void cancel() {
        downloadManager.cancelAll();
    }


	/**
	 * 检查存储空间 判断此文件的大致大小，并不准确
	 */
	private void checkStorage() throws DownloadException {
		if (currTask.getFileList() == null || currTask.getFileList().isEmpty()) {
			throw new DownloadException("M3U8文件解析失败", 4);
			// M3U8文件为空
		}
		String tsUrl = currTask.getFileList().get(0);
		HttpGet get = new HttpGet(tsUrl);
		HttpResponse response = null;
		try {
			response = defaultHttpClient.execute(get);
			int status = response.getStatusLine().getStatusCode();
			if (status == 404) {
				throw new DownloadException("ts文件404,下载失败", DownloadException.NETWORK_FILE_404_ERROR);
			}
			Header[] headers = response.getHeaders("Content-Length");
			double length = 0;
			for (Header header : headers) {
				length = Long.valueOf(header.getValue());
			}
			length = (length / (1024 * 1024)) * currTask.getFileList().size();
			long avilableMB = (FileUtil.getSizeByPath(FileUtil.getDownloadPath(mContext)) / (1024 * 1024));
			if (length > avilableMB) {
				throw new DownloadException("机器空间不足", 4);
			}

		} catch (ClientProtocolException e) {
			e.printStackTrace();
			throw new DownloadException("ts下载失败", DownloadException.NETWORK_ERROR);
		} catch (IOException e) {
			e.printStackTrace();
			throw new DownloadException("ts下载失败", DownloadException.NETWORK_ERROR);
		}

	}


	private void downloadFile() throws DownloadException {

        currTask.getFileFinishedList().clear();// 每一次更换任务的时候都要将已完成的url清空否则在同一个任务第二次下载的时候会重复添加导致进度计算错误
		if (currTask.getFileList() == null || currTask.getFileList().isEmpty() == true) {
			throw new DownloadException("M3U8文件404,下载失败", DownloadException.NETWORK_FILE_404_ERROR);
		}

	 for (int i = 0; i < currTask.getFileList().size(); i++) {
        String url = currTask.getFileList().get(i);
        String downloadId = url;
         String destinationUrl = getPath(url)+(url.substring(url.lastIndexOf("/") + 1));

         if(FileUtil.isFileExist(destinationUrl)){ // 文件已经下载
             System.out.println("###### isFileExist ######## "+destinationUrl);
             currTask.getFileFinishedList().add(destinationUrl);
             currTask.setPercent(currTask.getFileFinishedList().size()*100 / currTask.getFileList().size());
             downloadDB.update(currTask);
             //EventBus.getDefault().post(new StatusEvent(true));
             return;
         }

         final DownloadRequest downloadRequest = new DownloadRequest(url)
                 .setDestinationUrl(destinationUrl).setPriority(DownloadRequest.Priority.HIGH)
                 .setDownloadListener(myDownloadStatusListener);
         downloadRequest.setDownloadId(downloadId);
         if (downloadManager.query(downloadId) == DownloadManager.STATUS_NOT_FOUND) {
              downloadManager.add(downloadRequest);
         }
	 }
		
	}



    class MyDownloadStatusListener implements DownloadStatusListener {

        @Override
        public void onDownloadComplete(String id) {
           // System.out.println("###### onDownloadComplete ######## "+id);
            currTask.getFileFinishedList().add(id);
            // System.out.println("###### currTask.getFileList().size() ######## "+currTask.getFileList().size());
            if(currTask.getFileList().size() <= 0) return;
            currTask.setPercent(currTask.getFileFinishedList().size()*100 / currTask.getFileList().size());
            if(currTask.getFileFinishedList().size() == currTask.getFileList().size()){
                currTask.setStatus(DownloadTask.FINISHED);
            }
           /* else {
                if(currTask.getFileFailList().size() != 0
                        && currTask.getFileFinishedList().size() < currTask.getFileList().size()){

                    for (int i = 0; i < currTask.getFileFailList().size(); i++) {
                        String url = currTask.getFileFailList().get(i);
                        String downloadId = url;
                        String destinationUrl = getPath(url)+(url.substring(url.lastIndexOf("/") + 1));

                        final DownloadRequest downloadRequest = new DownloadRequest(url)
                                .setDestinationUrl(destinationUrl).setPriority(DownloadRequest.Priority.HIGH)
                                .setDownloadListener(myDownloadStatusListener);
                        downloadRequest.setDownloadId(downloadId);
                        //if (downloadManager.query(downloadId) == DownloadManager.STATUS_NOT_FOUND) {
                        downloadManager.add(downloadRequest);
                        //}
                    }
                }
            }*/
            downloadDB.update(currTask);
            DownloadTask ts =  downloadDB.find(currTask.getId());
            LogUtils.d("ts=" + ts.getId() + "进度=" + ts.getPercent());
            //EventBus.getDefault().post(new StatusEvent(true));
        }

        @Override
        public void onDownloadFailed(String id, int errorCode, String errorMessage) {
            //System.out.println("###### onDownloadFailed ######## "+id+" : "+errorCode+" : "+errorMessage);
            if(errorCode != 404) {
                currTask.getFileFailList().add(id);
                //重新加入下载队列
                String downloadId = id;
                String destinationUrl = getPath(id) + (id.substring(id.lastIndexOf("/") + 1));

                final DownloadRequest downloadRequest = new DownloadRequest(id)
                        .setDestinationUrl(destinationUrl).setPriority(DownloadRequest.Priority.HIGH)
                        .setDownloadListener(myDownloadStatusListener);
                downloadRequest.setDownloadId(downloadId);
                //if (downloadManager.query(downloadId) == DownloadManager.STATUS_NOT_FOUND) {
                downloadManager.add(downloadRequest);
                System.out.println("###### fail destinationUrl ######## "+id);
            }else{
                //上传到服务器
            }

        }

        @Override
        public void onProgress(String id, long totalBytes, long downloadedBytes,
                               int progress) {
            // TODO Auto-generated method stub
            currTask.setDownloadSpeed(getBytesDownloaded(progress, totalBytes)+"   "+progress+"%");
            downloadDB.update(currTask);
            //System.out.println("Download1 id: " + id + ", " + progress + "%" + "  " + getBytesDownloaded(progress, totalBytes));
            //单独的进度
            //currTask.getFileList().indexOf(id);
        }
    }

    private String getBytesDownloaded(int progress, long totalBytes) {
        //Greater than 1 MB
        long bytesCompleted = (progress * totalBytes)/100;
        if (totalBytes >= 1000000) {
            return (""+(String.format("%.1f", (float)bytesCompleted/1000000))+ "/"+ ( String.format("%.1f", (float)totalBytes/1000000)) + "MB");
        } if (totalBytes >= 1000) {
            return (""+(String.format("%.1f", (float)bytesCompleted/1000))+ "/"+ ( String.format("%.1f", (float)totalBytes/1000)) + "Kb");

        } else {
            return ( ""+bytesCompleted+"/"+totalBytes );
        }
    }

	/**
	 * 解析讲义文件加入下载列表
	 * 
	 * @throws DownloadException
	 */
	private void resolveLecture() throws DownloadException {
		String cwUrl = currTask.getVideoUrl()+ Constants.LECTURE_URL;
		String dlCwUrl = cwUrl.replace("videodl", "downloaddl");
		HttpGet get = new HttpGet(dlCwUrl);
		HttpResponse response = null;
		FileWriter wf = null;
		try {
			response = defaultHttpClient.execute(get);
			int status = response.getStatusLine().getStatusCode();
			if (status == 404) {
				throw new DownloadException("讲义文件404,下载失败", DownloadException.NETWORK_FILE_404_ERROR);
			}
			byte[] htmlData = EntityUtils.toByteArray(response.getEntity());
			String htmlContent = new String(htmlData, "UTF-8");
			// 得到根路径
			int index = dlCwUrl.lastIndexOf("/");
			String rootUrl = dlCwUrl.substring(0, index + 1);
			List<String> list = new ArrayList<String>();
			htmlContent = this.parseHtmlDownloadFile(htmlContent, rootUrl, list);

			currTask.getFileList().addAll(list);
			// 得到URL
			String lectureUrl = dlCwUrl;
			// 得到文件
			String file = lectureUrl.substring(lectureUrl.lastIndexOf("/") + 1);
			// 得到下载路径 视频和讲义分别放入不同目录
			File path = new File(this.getPath(lectureUrl));
			// 创建下载目录
			if (!path.exists()) {
				path.mkdirs();
			}

			// 写入文件
			File videoFile = new File(path.getPath() + "/" + file);
			wf = new FileWriter(videoFile);
			wf.write(htmlContent);

		} catch (ClientProtocolException e) {
			e.printStackTrace();
			throw new DownloadException("讲义下载失败", DownloadException.NETWORK_ERROR);
		} catch (IOException e) {
			e.printStackTrace();
			throw new DownloadException("讲义网络错误", DownloadException.NETWORK_ERROR);
		} finally {
			if (wf != null) {
				try {
					wf.close();
				} catch (IOException e) {
					e.printStackTrace();
					throw new DownloadException("文件关闭失败", DownloadException.FILEWRITE_ERROR);
				}
			}

		}
	}

	/**
	 * 解析讲义文件加入下载列表
	 * 
	 * @throws DownloadException
	 */
	private void resolveCaption() throws DownloadException {
		HttpGet get = new HttpGet(currTask.getVideoUrl()+ Constants.CAPTION_URL);
		HttpResponse response = null;
		FileWriter wf = null;
		try {
			response = defaultHttpClient.execute(get);
			int status = response.getStatusLine().getStatusCode();
			if (status == 404) {
				return;
			}
			byte[] htmlData = EntityUtils.toByteArray(response.getEntity());
			String htmlContent = new String(htmlData, "utf-8");

			// 得到URL
			String captionUrl = currTask.getVideoUrl()+ Constants.CAPTION_URL;
			// 得到文件
			String file = captionUrl.substring(captionUrl.lastIndexOf("/") + 1);
			// 得到下载路径 视频和讲义分别放入不同目录
			File path = new File(this.getPath(captionUrl));
			// 创建下载目录
			if (!path.exists()) {
				path.mkdirs();
			}

			// 写入文件
			File videoFile = new File(path.getPath() + "/" + file);
			wf = new FileWriter(videoFile);
			wf.write(htmlContent);

		} catch (Exception e) {
			e.printStackTrace();
			throw new DownloadException("字幕下载失败", DownloadException.NETWORK_ERROR);
		} finally {
			if (wf != null) {
				try {
					wf.close();
				} catch (IOException e) {
					e.printStackTrace();
					throw new DownloadException("文件关闭失败", DownloadException.FILEWRITE_ERROR);
				}
			}

		}

	}

	private void writeFile(byte[] data, File path) throws DownloadException {
		FileOutputStream wf = null;
		try {
			wf = new FileOutputStream(path);
			wf.write(data, 0, data.length);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new DownloadException("文件保存失败", DownloadException.FILEWRITE_ERROR);
		} catch (IOException e) {
			e.printStackTrace();
			throw new DownloadException("文件保存失败", DownloadException.FILEWRITE_ERROR);
		} finally {
			if (wf != null) {
				try {
					wf.close();
				} catch (IOException e) {
					e.printStackTrace();
					throw new DownloadException("文件保存失败", DownloadException.FILEWRITE_ERROR);
				}
			}
		}

	}

	/**
	 * 解析M3U8文件加入下载列表
	 */
	private void resolveM3U8File() throws DownloadException {

		String videoUrl = currTask.getVideoUrl()+ Constants.VIDEO_URL;// 原有下载地址跟播放地址是同一个现在将现在地址和播放地址分开
													// 下载地址为替换后的replaceDl
		String replaceDl = videoUrl.replace("videodl", "downloaddl");
		HttpGet get = new HttpGet(replaceDl);
		HttpResponse response = null;
		FileWriter wf = null;
		try {
			response = defaultHttpClient.execute(get);
			int status = response.getStatusLine().getStatusCode();
			if (status == 404) {
				throw new DownloadException("M3U8文件404,下载失败", DownloadException.NETWORK_FILE_404_ERROR);
			} else if (status == 503) {
				throw new DownloadException("M3U8文件503,下载失败", DownloadException.NETWORK_ERROR);
			}
			String resultString = EntityUtils.toString(response.getEntity());
			String m3u8String = resultString;

			// 得到URL
			String m3u8Url = replaceDl;
			// 得到文件
			String file = m3u8Url.substring(m3u8Url.lastIndexOf("/") + 1);
			// 得到下载路径 视频和讲义分别放入不同目录
			File path = new File(this.getPath(m3u8Url));
			// 创建下载目录
			if (!path.exists()) {
				path.mkdirs();
			}
			if (m3u8String != null && !m3u8String.contains("#EXT")) {
				// 避免解析 <html> 开头的内容提示页面
				throw new DownloadException("M3U8文件内容错误,下载失败", DownloadException.NETWORK_ERROR);
			}

			File videoFile = new File(path.getPath() + "/" + file);
			wf = new FileWriter(videoFile);
			wf.write(resultString);

			String[] as = m3u8String.split("\n");
			//暂时修改as.length  10
			for (int i = 0; i < as.length; i++) {
				if (!as[i].startsWith("#EXT")) {
					int index = replaceDl.lastIndexOf("/") + 1;
					final String fileName = as[i];
					if (fileName.contains(".ts")) {
						String rootUrl = replaceDl.substring(0, index);
						String url = rootUrl + fileName;
						currTask.getFileList().add(url);
					}
				}
			}
			System.out.println("downloadtest  m3u8文件个数："+as.length);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DownloadException("M3U8下载失败", DownloadException.NETWORK_ERROR);
		} finally {
			if (wf != null) {
				try {
					wf.close();
				} catch (IOException e) {
					e.printStackTrace();
					throw new DownloadException("文件关闭失败", DownloadException.FILEWRITE_ERROR);
				}
			}

		}
	}

	/**
	 * 得到存储路径
	 * 
	 * @param url
	 * @return
	 */
	private String getPath(String url) {
		if (url == null || url.equals(""))
			return "";

		int index = url.lastIndexOf(".");
		String prefix = url.substring(index + 1);
		if (prefix.equals("m3u8") || prefix.equals("ts")) {
			return FileUtil.getDownloadPath(mContext) + currTask.getId() + "/" + FileUtil.VIDEOPATH;
		} else {
			return FileUtil.getDownloadPath(mContext) + currTask.getId() + "/" + FileUtil.LECTURE;
		}

	}

	/**
	 * 解析HTML文件 返回需要下载的文件
	 * 
	 * @param htmlContent
	 * @return
	 */
	private String parseHtmlDownloadFile(String htmlContent, String rootUrl, List<String> list) {

		Document doc = Jsoup.parse(htmlContent);
		Elements csss = doc.select("link");
		Elements jss = doc.select("script");
		Elements imgs = doc.select("img");
		for (Element element : csss) {
			String result = element.attr("href");
			String path = toAbsolutePath(rootUrl, result);
			if (!list.contains(path))
				list.add(toAbsolutePath(rootUrl, result));
			// 替换文件为相对路径
			element.attr("href", result.substring(result.lastIndexOf("/") + 1));
		}
		for (Element element : jss) {
			String result = element.attr("src");
			String path = toAbsolutePath(rootUrl, result);
			if (!list.contains(path))
				list.add(path);
			// 替换文件为相对路径
			element.attr("src", result.substring(result.lastIndexOf("/") + 1));
		}
		for (Element element : imgs) {
			String result = element.attr("src");
			String path = toAbsolutePath(rootUrl, result);
			if (!list.contains(path))
				list.add(path);
			// 替换文件为相对路径
			element.attr("src", result.substring(result.lastIndexOf("/") + 1));
		}
		// 得到新的html文本
		String newHtmlContent = doc.html();
		return newHtmlContent;
	}

	/**
	 * 如果url为相对路径 转换为绝对路径
	 * 
	 * @param rootUrl
	 * @param url
	 * @return
	 */
	private String toAbsolutePath(String rootUrl, String url) {
		String result = "";
		if (url.startsWith("http://") || url.startsWith("https://")) {
			result = url;
		} else {
			result = rootUrl + url;
		}
		return result;
	}

	public synchronized DefaultHttpClient getHttpClient() {
		if (null == defaultHttpClient) {
			// 初始化工作
			HttpParams params = new BasicHttpParams();
			// 设置最大连接数
			ConnManagerParams.setMaxTotalConnections(params, MAX_TOTAL_CONNECTIONS);
			// 设置获取连接的最大等待时间
			ConnManagerParams.setTimeout(params, WAIT_TIMEOUT);
			// 设置每个路由最大连接数
			ConnPerRouteBean connPerRoute = new ConnPerRouteBean(MAX_ROUTE_CONNECTIONS);
			ConnManagerParams.setMaxConnectionsPerRoute(params, connPerRoute);
			// 设置连接超时时间
			HttpConnectionParams.setConnectionTimeout(params, CONNECT_TIMEOUT);
			// 设置读取超时时间
			HttpConnectionParams.setSoTimeout(params, READ_TIMEOUT);

			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
			HttpProtocolParams.setContentCharset(params, HTTP.DEFAULT_CONTENT_CHARSET);
			HttpProtocolParams.setUseExpectContinue(params, true);

			SchemeRegistry schReg = new SchemeRegistry();
			schReg.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
			schReg.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));

			ClientConnectionManager conManager = new ThreadSafeClientConnManager(params, schReg);

			defaultHttpClient = new DefaultHttpClient(conManager, params);
		}

		return defaultHttpClient;
	}

}