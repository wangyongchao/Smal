package com.bjjy.mainclient.phone.view.home.bean;

/**
 * Created by dell on 2016/8/4.
 */
public class LocalData {
    private String coursewareId;//课程ID
    private String lastupdateTime;//最后更新时间
    private int accomplished;//是否完成 0未完成 1已完成

    public String getCoursewareId() {
        return coursewareId;
    }

    public void setCoursewareId(String coursewareId) {
        this.coursewareId = coursewareId;
    }

    public String getLastupdateTime() {
        return lastupdateTime;
    }

    public void setLastupdateTime(String lastupdateTime) {
        this.lastupdateTime = lastupdateTime;
    }

    public int getAccomplished() {
        return accomplished;
    }

    public void setAccomplished(int accomplished) {
        this.accomplished = accomplished;
    }
}
