package com.bjjy.mainclient.phone.view.exam.activity.ExamList;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bjjy.mainclient.phone.app.BaseFragmentActivity;
import com.bjjy.mainclient.phone.view.exam.activity.ExamList.adapter.ExamPracticeAdapter;
import com.bjjy.mainclient.phone.widget.EmptyViewLayout;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.widget.pulltorefresh.PullToRefreshBase;
import com.bjjy.mainclient.phone.widget.pulltorefresh.PullToRefreshExpandableListView;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by wyc on 2016/5/10.
 */
public class ExamPracticeActivity extends BaseFragmentActivity implements PullToRefreshBase.OnRefreshListener<ExpandableListView>, ExpandableListView.OnChildClickListener, ExamPracticeView{
    @Bind(R.id.top_title_left)
    ImageView top_title_left;
    @Bind(R.id.examprictice_expanble_lv)
    PullToRefreshExpandableListView examprictice_expanble_lv;
    @Bind(R.id.top_title_right)
    ImageView top_title_right;
    @Bind(R.id.top_title_text)
    TextView top_title_text;
    private EmptyViewLayout mEmptyLayout;
    private ExamPracticePercenter examPracticePercenter;
    private ExamPracticeAdapter examPracticeAdapter;

    @OnClick(R.id.top_title_left) void onBack(){
        onBackPressed();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.exam_practice_activity);
        ButterKnife.bind(this);
        examPracticePercenter=new ExamPracticePercenter();
        examPracticePercenter.attachView(this);
        initView();
        initData();
    }

    @Override
    public void initView() {
        top_title_text.setText(getResources().getString(R.string.exam_prictice));
        examprictice_expanble_lv.setScrollingWhileRefreshingEnabled(true);
        examprictice_expanble_lv.getRefreshableView().setGroupIndicator(null);
        examprictice_expanble_lv.getRefreshableView().setOnChildClickListener(this);
        mEmptyLayout = new EmptyViewLayout(this, examprictice_expanble_lv);
//        ViewGroup viewGroup= (ViewGroup) LayoutInflater.from(this).inflate(R.layout.app_view_error_dongao,null);
//        ViewGroup dataError= (ViewGroup) LayoutInflater.from(this).inflate(R.layout.app_view_data_empty_dongao,null);
//        mEmptyLayout.setNetErrorView(viewGroup);
//        mEmptyLayout.setDataEmptyView(dataError);
        mEmptyLayout.setErrorButtonClickListener(mErrorClickListener);
        mEmptyLayout.setEmptyButtonClickListener(emptyButtonClickListener);
        mEmptyLayout.setEmptyMessage("");
        mEmptyLayout.setShowEmptyButton(false);
        initContrl();
    }
    

    @Override
    public void initData() {
        examPracticeAdapter = new ExamPracticeAdapter(this,  examPracticePercenter.courseList, true);
        examprictice_expanble_lv.getRefreshableView().setAdapter(examPracticeAdapter);
        examPracticePercenter.getData();
        setAllCollapsed();
    }

    @Override
    public void onClick(View v) {

    }

    /**
     * 默认打开全部
     */
    public void setAllCollapsed(){
        for(int i = 0; i < examPracticeAdapter.getGroupCount(); i++){
            examprictice_expanble_lv.getRefreshableView().expandGroup(i);
        }
        examprictice_expanble_lv.getRefreshableView().setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                return true;
            }
        });
    }

    @Override
    public EmptyViewLayout getEmptyLayout() {
        return mEmptyLayout;
    }

    @Override
    public PullToRefreshExpandableListView getPTREListView() {
        return examprictice_expanble_lv;
    }

    @Override
    public void initAdapter() {
        examPracticeAdapter = new ExamPracticeAdapter(this,  examPracticePercenter.courseList, true);
        examprictice_expanble_lv.getRefreshableView().setAdapter(examPracticeAdapter);
 
    }

    /**
     * 控制expandListview的刷新监听
     */
    public void initContrl() {
        // TODO Auto-generated method stub
        examprictice_expanble_lv.getRefreshableView().setOnChildClickListener(this);
        examprictice_expanble_lv.setOnRefreshListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        return false;
    }

    @Override
    public void onRefresh(PullToRefreshBase<ExpandableListView> refreshView) {
        examPracticePercenter.getData();
    }

    @Override
    public void showLoading() {
        
    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public Context context() {
        return ExamPracticeActivity.this;
    }

    /**
     * 错误监听
     */
    private View.OnClickListener mErrorClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        }
    };
    /**
     * 无数据监听
     */
    private View.OnClickListener emptyButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //showAppMsg("显示kong");
        }
    };


    @Override
    public void refreshAdapter() {
        examPracticeAdapter.notifyDataSetChanged();
    }
    
    
}
