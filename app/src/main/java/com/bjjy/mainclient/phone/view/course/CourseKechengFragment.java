package com.bjjy.mainclient.phone.view.course;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;

import com.alibaba.fastjson.JSON;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseFragment;
import com.bjjy.mainclient.phone.event.DeleteEvent;
import com.bjjy.mainclient.phone.view.course.bean.MCourseBean;
import com.bjjy.mainclient.phone.view.download.local.bean.YearInfo;
import com.bjjy.mainclient.phone.view.exam.activity.myexam.ExamMineActivity;
import com.bjjy.mainclient.phone.view.play.PlayActivity;
import com.dongao.mainclient.model.local.SharedPrefHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by dell on 2016/5/16.
 */
public class CourseKechengFragment extends BaseFragment implements ExpandableListView.OnChildClickListener, CourseWodeExpandAdapter.ExpableListListener, KechengAdapter.MCourseBeanItemClickListener {

    @Bind(R.id.rv_content)
    RecyclerView rv_content;
	@Bind(R.id.v_empty)
	View v_empty;
	@Bind(R.id.ll_content)
	LinearLayout ll_content;

    private View view;

    private KechengAdapter expandAdapter;
    private List<MCourseBean> mCourseBeanList=new ArrayList<>();

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        view = inflater.inflate(R.layout.course_kecheng_fragment, null);
        ButterKnife.bind(this, view);
        initView();

        initData();
        return view;
    }

    @Override
    public void initView() {
        rv_content.setLayoutManager(new LinearLayoutManager(rv_content.getContext(), LinearLayoutManager.VERTICAL, false));

        expandAdapter=new KechengAdapter(getActivity(),mCourseBeanList);
        expandAdapter.setMCourseBeanClickListener(this);
        rv_content.setAdapter(expandAdapter);
//		mEmptyLayout = new EmptyViewLayout(getActivity(), ll_content);
//		mEmptyLayout.setErrorButtonClickListener(mErrorClickListener);
//		mEmptyLayout.setEmptyButtonClickListener(emptyButtonClickListener);
	}

	/**
	 * 错误监听
	 */
	private View.OnClickListener mErrorClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
		}
	};
	/**
	 * 无数据监听
	 */
	private View.OnClickListener emptyButtonClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View view) {
			//showAppMsg("显示kong");
		}
	};

    @Override
    public void initData() {
    }
    
    public void setData(List<MCourseBean> list){
        mCourseBeanList.clear();
        mCourseBeanList.addAll(list);
        expandAdapter.notifyDataSetChanged();
		if (mCourseBeanList.size()==0){
			v_empty.setVisibility(View.VISIBLE);
			ll_content.setVisibility(View.GONE);
		}else{
			v_empty.setVisibility(View.GONE);
			ll_content.setVisibility(View.VISIBLE);
		}
    }

    private boolean first;
    @Override
    public void onResume() {
        super.onResume();
        if(first){
            initData();
        }
        first=true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.local_notask:
//                downlaod();
                break;
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }


    //onEventAsync
    @Subscribe
    public void onEventMainThread(DeleteEvent event) {
        initData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        try{    //无缘无故下标越界
//            Course course=subjects.get(groupPosition).getChilds().get(childPosition);
//            Intent intent = new Intent(getActivity(), CachedCourseActivity.class);
//            intent.putExtra("courseId", course.getCwCode());
//            intent.putExtra("title",course.getCwName());
//            startActivity(intent);
        }catch (Exception e){
            initData();
        }

        return false;
    }

    @Override
    public void onParentChick(int position) {
        
    }

    @Override
    public void mCourseBeanClick(int type, int position) {
        if (type==0){//考试
            Intent intent=new Intent(getActivity(), ExamMineActivity.class);
            intent.putExtra("ruleId",mCourseBeanList.get(position).getRuleId());
			intent.putExtra("accountId",mCourseBeanList.get(position).getAccountId());
			startActivity(intent);
        }else{
			String year= SharedPrefHelper.getInstance(getActivity()).getCurYear();
			YearInfo yearInfo= JSON.parseObject(year,YearInfo.class);
            Intent intent=new Intent(getActivity(), PlayActivity.class);
            String cwCode=mCourseBeanList.get(position).getCourseDetailInfo().get(0).getCwCode();
            if (yearInfo!=null){
				intent.putExtra("subjectId",yearInfo.getYearName());
			}
			intent.putExtra("classId",cwCode);
			intent.putExtra("courseBean", JSON.toJSONString(mCourseBeanList.get(position).getCourseDetailInfo().get(0)));
			startActivity(intent);
        }
    }
}
