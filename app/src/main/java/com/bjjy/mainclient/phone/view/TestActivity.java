package com.bjjy.mainclient.phone.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseActivity;

/**
 * 测试修改
 */
public class TestActivity extends BaseActivity {

    private String name = "";
    private boolean flag = false;
    private int code = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_test);

        initData();
    }

    @Override
    public void initView() {

    }

    @Override
    public void initData() {
        Intent intent = getIntent();
        if (intent != null) {
            name = intent.getStringExtra("name");
            flag = intent.getBooleanExtra("flag", false);
            code = intent.getIntExtra("code", 0);
        }

        String text = "name:" + name + "  flag:" + flag + "  code:" + code;
        TextView tv = (TextView) findViewById(R.id.main_test_tv);
        tv.setText(text);
    }

    @Override
    public void onClick(View v) {

    }
}
