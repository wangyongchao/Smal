package com.bjjy.mainclient.phone.view.course;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.AppContext;
import com.bjjy.mainclient.phone.app.BaseFragment;
import com.bjjy.mainclient.phone.event.DeleteEvent;
import com.bjjy.mainclient.phone.view.download.local.SelectYearPopupwindow.SelectYearPopwindow;
import com.bjjy.mainclient.phone.view.download.local.bean.YearInfo;
import com.bjjy.mainclient.phone.view.exam.activity.myexam.adapter.PopUpWindowAdapter;
import com.bjjy.mainclient.phone.view.studybar.Constant;
import com.bjjy.mainclient.phone.widget.EmptyViewLayout;
import com.bjjy.mainclient.phone.widget.statusbar.Utils;

import org.greenrobot.eventbus.EventBus;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author wyc
 */
public class CourseFragment extends BaseFragment implements ViewPager.OnPageChangeListener, CourseFragmentView {

    @Bind(R.id.top_title_left)
    ImageView topTitleLeft;
    @Bind(R.id.top_title_right)
    ImageView topTitleRight;
    @Bind(R.id.top_title_text)
    TextView topTitleText;
    @Bind(R.id.top_title_bar_layout)
    RelativeLayout topTitleBarLayout;

    @Bind(R.id.download_menu_tv)
    TextView downloadMenuTv;
    @Bind(R.id.course_line)
    View courseLine;
    @Bind(R.id.course_menu_tv)
    TextView courseMenuTv;
    @Bind(R.id.download_line)
    View downloadLine;
    @Bind(R.id.cache_vp)
    ViewPager cacheVp;
    @Bind(R.id.tv_pointnum)
    TextView tvPointnum;
    @Bind(R.id.ll_point)
    RelativeLayout llPoint;
    @Bind(R.id.status_bar_fix)
    View status_bar_fix;
    @Bind(R.id.ll_top_right)
    LinearLayout ll_top_right;
    @Bind(R.id.iv_right_pop)
    ImageView iv_right_pop;
    @Bind(R.id.tv_right)
    TextView tv_right;
    @Bind(R.id.ll_content)
    LinearLayout ll_content;
    

    private CourseListAdapter adapter;
    private View view_now_showLine;
    private int postion;
    private boolean mHasLoadedOnce;
    private View mRootView;
    private CourseFragmentPercenter courseFragmentPercenter;
    private SelectYearPopwindow selectYearPopwindow;
    private EmptyViewLayout mEmptyLayout;
//    private GestureImageView status_bar_fix;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if(mHasLoadedOnce){
                cacheVp.setCurrentItem(0);
                EventBus.getDefault().post(new DeleteEvent());
            }
            if (isVisibleToUser && !mHasLoadedOnce) {
                // async http request here
                mHasLoadedOnce = true;
                initData();
            }
        }
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.course_mine_activity, container, false);
        }
        ButterKnife.bind(this, mRootView);
        courseFragmentPercenter = new CourseFragmentPercenter();
        courseFragmentPercenter.attachView(this);
        ViewGroup parent = (ViewGroup) mRootView.getParent();
        if (parent != null) {
            parent.removeView(mRootView);
        }
        initView();
        initData();
        return mRootView;
    }

    @Override
    public void initView() {
        setTranslucentStatus();
        showYearOrNot(false);
        ll_top_right.setVisibility(View.VISIBLE);
        ll_top_right.setOnClickListener(this);
        topTitleLeft.setImageResource(R.drawable.nav_left);
        topTitleRight.setVisibility(View.INVISIBLE);
        topTitleLeft.setVisibility(View.INVISIBLE);
        topTitleLeft.setOnClickListener(this);
        topTitleRight.setOnClickListener(this);
        topTitleText.setText("我的课堂");

        downloadMenuTv.setOnClickListener(this);
        courseMenuTv.setOnClickListener(this);

        view_now_showLine = courseLine;
        cacheVp.addOnPageChangeListener(this);
        mEmptyLayout = new EmptyViewLayout(getActivity(), ll_content);
        mEmptyLayout.setErrorButtonClickListener(mErrorClickListener);
        mEmptyLayout.setEmptyButtonClickListener(emptyButtonClickListener);
    }
    
    

    @TargetApi(19)
    public void setTranslucentStatus() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            //当状态栏透明后，内容布局会上移，这里使用一个和状态栏高度相同的view来修正内容区域
            status_bar_fix.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Utils.getStatusHeight(getActivity())));
            status_bar_fix.setAlpha(1);
        }
    }

    /**
     * 错误监听
     */
    private View.OnClickListener mErrorClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showLoading();
            if (courseFragmentPercenter.yearList.size()==0){
                initData();
            }else{
                courseFragmentPercenter.getInterData();
            }
            
        }
    };
    /**
     * 无数据监听
     */
    private View.OnClickListener emptyButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
        }
    };

    @Override
    public void initData() {
        showLoading();
        courseFragmentPercenter.initData();
    }


    @Override
    public void showContentView(int type) {
        if (type == Constant.VIEW_TYPE_0) {
            mEmptyLayout.showContentView();
            showYearOrNot(true);
        } else if (type == Constant.VIEW_TYPE_1) {
            mEmptyLayout.showNetErrorView();
            showYearOrNot(false);
        } else if (type == Constant.VIEW_TYPE_2) {
            mEmptyLayout.showEmpty();
            showYearOrNot(false);
        } else if (type == Constant.VIEW_TYPE_3) {
            mEmptyLayout.showError();
            showYearOrNot(false);
        }
//        hideLoading();
    }

    @Override
    public void onResume() {
        super.onResume();
        initData();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.download_menu_tv:
                setAnimation(downloadLine);
                courseMenuTv.setTextColor(Color.parseColor("#000000"));
                downloadMenuTv.setTextColor(getResources().getColor(R.color.color_primary));
                cacheVp.setCurrentItem(1);
                break;
            case R.id.course_menu_tv:
                setAnimation(courseLine);
                downloadMenuTv.setTextColor(Color.parseColor("#000000"));
                courseMenuTv.setTextColor(getResources().getColor(R.color.color_primary));
                cacheVp.setCurrentItem(0);
                break;
            case R.id.top_title_left:
                break;
            case R.id.ll_top_right:
                selectYearPopwindow.showPop(courseFragmentPercenter.yearList, courseFragmentPercenter.currYear);
                break;
        }
    }

    /**
     * 年份的点击事件
     */
    private PopUpWindowAdapter.MainTypeItemClick onItemClickListener = new PopUpWindowAdapter.MainTypeItemClick() {
        @Override
        public void itemClick(int mainPosition, int itemType, int type, int position) {
            showLoading();
            courseFragmentPercenter.setOnItemClick(position);
        }
    };

    private void setAnimation(View lineView) {
        if (view_now_showLine != lineView) {
            Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.play_menu_line_out);
            view_now_showLine.startAnimation(animation);
            view_now_showLine.setVisibility(View.INVISIBLE);
            view_now_showLine = lineView;
            view_now_showLine.setVisibility(View.VISIBLE);
            Animation animation1 = AnimationUtils.loadAnimation(getActivity(), R.anim.play_menu_line_in);
            view_now_showLine.startAnimation(animation1);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        adapter.notifyDataSetChanged();
        this.postion=position;
        if (position == 0) {
            setAnimation(courseLine);
            downloadMenuTv.setTextColor(Color.parseColor("#000000"));
            courseMenuTv.setTextColor(getResources().getColor(R.color.color_primary));
//            if(courses!=null && courses.size()>0){
//                topTitleRight.setVisibility(View.VISIBLE);
//            }else{
//                topTitleRight.setVisibility(View.INVISIBLE);
//            }
        } else if (position == 1) {
            setAnimation(downloadLine);
            courseMenuTv.setTextColor(Color.parseColor("#000000"));
            downloadMenuTv.setTextColor(getResources().getColor(R.color.color_primary));
//            if (list != null && list.size() > 0) {
//                topTitleRight.setVisibility(View.VISIBLE);
//            } else {
//                topTitleRight.setVisibility(View.INVISIBLE);
//            }
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        AppContext.getInstance().setHandler(null);
    }

    @Override
    public void showLoading() {
        mEmptyLayout.showLoading();
    }

    @Override
    public void hideLoading() {
        mEmptyLayout.showContentView();
    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public Context context() {
        return null;
    }


    @Override
    public void initAdapter() {
        if(adapter==null){
            adapter = new CourseListAdapter(getChildFragmentManager());
            cacheVp.setAdapter(adapter);
            adapter.setBanxingData(courseFragmentPercenter.banxingList);
            adapter.setKechengData(courseFragmentPercenter.ketangList);
        }else{
            adapter.setBanxingData(courseFragmentPercenter.banxingList);
            adapter.setKechengData(courseFragmentPercenter.ketangList);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void initYearAdapter() {
        selectYearPopwindow = new SelectYearPopwindow(getActivity(), ll_top_right, iv_right_pop, onItemClickListener);
    }

    @Override
    public void showCurrentYear(YearInfo currYear) {
        if (currYear.getShowYear() == null || currYear.getShowYear().isEmpty()) {
            tv_right.setText(currYear.getYearName() );
        } else {
            tv_right.setText(currYear.getShowYear());
        }
    }

    @Override
    public void hideYearPop(boolean isHide) {
        if (isHide && selectYearPopwindow != null) {
            selectYearPopwindow.dissmissPop();
        }
    }

    @Override
    public void showYearOrNot(boolean show) {
        if (show)
            ll_top_right.setVisibility(View.VISIBLE);
        else
            ll_top_right.setVisibility(View.INVISIBLE);
    }
}
