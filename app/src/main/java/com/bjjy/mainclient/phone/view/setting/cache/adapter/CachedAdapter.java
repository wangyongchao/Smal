package com.bjjy.mainclient.phone.view.setting.cache.adapter;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bjjy.mainclient.phone.event.DeleteEvent;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.download.db.DownloadDB;
import com.bjjy.mainclient.phone.view.exam.activity.exampaperlist.bean.Course;
import com.bjjy.mainclient.phone.view.main.MainActivity;
import com.bjjy.mainclient.phone.widget.DialogManager;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * Created by dell on 2016/5/16.
 */
public class CachedAdapter extends BaseAdapter {

    private MainActivity mContext;
    private List<Course> mList;
    private DownloadDB db;
    private ImageLoader imageLoader;
    private String userId;

    public CachedAdapter(MainActivity context,ImageLoader imageLoader) {
        super();
        this.mContext = context;
        this.imageLoader=imageLoader;
        db = new DownloadDB(context);
        userId= SharedPrefHelper.getInstance(context).getUserId();
    }

    public void setList(List<Course> list) {
        this.mList = list;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = View.inflate(mContext, R.layout.cached_item, null);
            holder.title = (TextView) convertView.findViewById(R.id.cached_title);
            holder.teacher = (TextView) convertView.findViewById(R.id.cached_teacher);
            holder.count = (TextView) convertView.findViewById(R.id.cached_count);
            holder.img = (ImageView) convertView.findViewById(R.id.cached_img);
            holder.delete = (ImageView) convertView.findViewById(R.id.cached_delete_img);
            holder.line = convertView.findViewById(R.id.line);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if(position==0){
            holder.line.setVisibility(View.GONE);
        }else{
            holder.line.setVisibility(View.VISIBLE);
        }
        holder.title.setText(mList.get(position).getCourseTeacher());
        holder.teacher.setText(mList.get(position).getCwName());
        holder.count.setText("已缓存"+mList.get(position).getCourseCount()+"讲");
        if(!TextUtils.isEmpty(mList.get(position).getCourseImg()) && mList.get(position).getCourseImg().startsWith("http")){
            imageLoader.displayImage(mList.get(position).getCourseImg(),holder.img);
        }
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogManager.showNormalDialog(mContext, "确定要删除吗", "提示", "取消", "确定",
                        new DialogManager.CustomDialogCloseListener() {
                            @Override
                            public void yesClick() {
                                db.deleteCourse(userId, mList.get(position));
                                mList.remove(position);
                                if (mList != null && mList.size() < 1) {
                                    EventBus.getDefault().post(new DeleteEvent());
                                } else {
                                    notifyDataSetChanged();
                                }
                            }

                            @Override
                            public void noClick() {
                            }
                        });

            }
        });
        return convertView;
    }

    class ViewHolder {
        TextView title;
        TextView teacher;
        TextView count;
        ImageView img, delete;
        View line;
    }
}
