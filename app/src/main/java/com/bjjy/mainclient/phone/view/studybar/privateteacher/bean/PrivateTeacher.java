package com.bjjy.mainclient.phone.view.studybar.privateteacher.bean;

/**
 * Created by wyc on 2016/6/6.
 */
public class PrivateTeacher {
    private String noticeId;//公告ID
    private String teacherName; //老师名字
    private String createTime; //公告时间
    private String title;//公告标题
    private String content;//公告内容
    private String type;//类型
    private String mUrl;//私教班内容地址

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getNoticeId() {
        return noticeId;
    }

    public void setNoticeId(String noticeId) {
        this.noticeId = noticeId;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getmUrl() {
        return mUrl;
    }

    public void setmUrl(String mUrl) {
        this.mUrl = mUrl;
    }
}
