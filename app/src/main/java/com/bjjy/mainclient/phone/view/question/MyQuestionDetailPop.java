package com.bjjy.mainclient.phone.view.question;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.bjjy.mainclient.phone.view.exam.ExamActivity;
import com.bjjy.mainclient.phone.widget.DialogManager;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.bjjy.mainclient.phone.R;

/**
 * Created by fengzongwei on 2016/11/29 0029.
 */
public class MyQuestionDetailPop implements View.OnClickListener{

    private PopupWindow popupWindow;

    private QuestionDetailActivity context;

    private View contentView;

    private ImageView imageView_original,imageView_zhui,imageView_modify;
    private TextView textView_original,textView_zhui,textView_modify;
    private LinearLayout linearLayout_original,linearLayout_zhui,linearLayout_modify,linearLayout_delete;

    public MyQuestionDetailPop(QuestionDetailActivity context){
        this.context = context;
        initPop();
    }

    private void initPop(){
        if(popupWindow == null){
            contentView = LayoutInflater.from(context).inflate(R.layout.question_detail_right_pop,null);
            popupWindow = new PopupWindow(contentView, ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            // 需要设置一下此参数，点击外边可消失
            popupWindow.setBackgroundDrawable(new BitmapDrawable());
            // 设置点击窗口外边窗口消失
            popupWindow.setOutsideTouchable(true);
            // 设置此参数获得焦点，否则无法点击
            popupWindow.setFocusable(true);

            linearLayout_original = (LinearLayout)contentView.findViewById(R.id.question_detail_pop_original_ll);
            linearLayout_zhui = (LinearLayout)contentView.findViewById(R.id.question_detail_pop_zhui_ll);
            linearLayout_modify = (LinearLayout)contentView.findViewById(R.id.question_detail_pop_modify_ll);
            linearLayout_delete = (LinearLayout)contentView.findViewById(R.id.question_detail_pop_delete_ll);

            imageView_original = (ImageView)contentView.findViewById(R.id.question_detail_pop_original_img);
            imageView_zhui = (ImageView)contentView.findViewById(R.id.question_detail_pop_zhui_img);
            imageView_modify = (ImageView)contentView.findViewById(R.id.question_detail_pop_modify_img);

            textView_original = (TextView)contentView.findViewById(R.id.question_detail_pop_original_tv);
            textView_zhui = (TextView)contentView.findViewById(R.id.question_detail_pop_zhui_tv);
            textView_modify = (TextView)contentView.findViewById(R.id.question_detail_pop_modify_tv);

            linearLayout_original.setOnClickListener(this);
            linearLayout_zhui.setOnClickListener(this);
            linearLayout_modify.setOnClickListener(this);
            linearLayout_delete.setOnClickListener(this);
        }
    }

    public void showPop(View archer){
        if(context.questionDetailPersenter.questionDetail==null){
            return;
        }
        if(context.questionDetailPersenter.questionDetail.getQuestionSourceVo()!=null &&
                !TextUtils.isEmpty(context.questionDetailPersenter.questionDetail.getQuestionSourceVo().getQuestionId())){
            linearLayout_original.setVisibility(View.VISIBLE);
        }else{
            linearLayout_original.setVisibility(View.GONE);
        }
        popupWindow.showAsDropDown(archer,0,0);
    }

    public void dissmissPop(){
        popupWindow.dismiss();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.question_detail_pop_original_ll:
                Intent intent_yuanti = new Intent(context, ExamActivity.class);
                intent_yuanti.putExtra("subjectId",context.questionDetailPersenter.questionDetail.getsSubjectId());
                intent_yuanti.putExtra("questionId",context.questionDetailPersenter.questionDetail.getQuestionSourceVo().getQuestionId());
                SharedPrefHelper.getInstance(context).setExamTag(Constants.EXAM_ORIGINAL_QUESTION);
                context.startActivity(intent_yuanti);
                popupWindow.dismiss();
                break;
            case R.id.question_detail_pop_zhui_ll:
                if(context.questionDetailPersenter.questionDetail!=null &&
                        context.questionDetailPersenter.questionDetail.getAskedFlg() == 1) {
                    Intent intent_zhui = new Intent(context, QuestionInputActivity.class);
//                intent_zhui.putExtra("",);
                    intent_zhui.putExtra("questionId",context.questionAnswerId);
                    context.startActivity(intent_zhui);
                    popupWindow.dismiss();
                }else{
                    showMsg("当前不可追问");
                }
                break;
            case R.id.question_detail_pop_modify_ll:
                if(context.questionDetailPersenter.questionDetail!=null &&
                        context.questionDetailPersenter.questionDetail.getUpdateFlg() == 1){
                    Intent intent_modify = new Intent(context,QuestionInputActivity.class);
                    intent_modify.putExtra("title",context.questionDetailPersenter.questionDetail.getTitle());
                    intent_modify.putExtra("content",
                            context.questionDetailPersenter.questionDetail.getQaInfoVoList().
                                    get(context.questionDetailPersenter.questionDetail.getQaInfoVoList().size()-1).getContent());
                    intent_modify.putExtra("isModifyQues",true);
                    intent_modify.putExtra("isCanModifyTitle",context.questionDetailPersenter.questionDetail.getQaInfoVoList().size()<=1);
                    intent_modify.putExtra("questionId",context.questionAnswerId);
                    intent_modify.putExtra("qaInfoId",
                            context.questionDetailPersenter.questionDetail.getQaInfoVoList().
                                    get(context.questionDetailPersenter.questionDetail.getQaInfoVoList().size()-1).getId());
                    context.startActivity(intent_modify);
                    popupWindow.dismiss();
                }else{
                    showMsg("当前不可修改");
                }
                break;
            case R.id.question_detail_pop_delete_ll:
                if(context.questionDetailPersenter.questionDetail == null ||
                        context.questionDetailPersenter.questionDetail.getQaInfoVoList().size()==0){
                    return;
                }
                if(context.questionDetailPersenter.questionDetail.getDeleteFlg() != 1){
                    showMsg("当前不可删除");
                    return;
                }
                DialogManager.showNormalDialog(context, "确认删除吗？", "提示", "取消", "确定", new DialogManager.CustomDialogCloseListener() {
                    @Override
                    public void yesClick() {
                        popupWindow.dismiss();
                        if (context.questionDetailPersenter.questionDetail.getQaInfoVoList().size() == 1) {
                            context.deleteQuestion(context.questionDetailPersenter.questionDetail.getId(), false);
                        } else {
                            context.deleteQuestion(context.questionDetailPersenter.questionDetail.getId(), true);
                        }
                    }

                    @Override
                    public void noClick() {

                    }
                });
                break;
        }
    }

    private Toast toast;
    private void showMsg(String msg){
        if(toast == null){
            toast = Toast.makeText(context,msg,Toast.LENGTH_SHORT);
        }else
            toast.setText(msg);
        toast.show();
    }

}
