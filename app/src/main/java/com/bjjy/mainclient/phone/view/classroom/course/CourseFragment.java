/*
 * Copyright (C) 2013 Andreas Stuetz <andreas.stuetz@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bjjy.mainclient.phone.view.classroom.course;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bjjy.mainclient.phone.view.exam.ExamActivity;
import com.dongao.mainclient.model.bean.course.AnswerItem;
import com.dongao.mainclient.model.bean.course.CourseItem;
import com.dongao.mainclient.model.bean.course.CourseJson;
import com.dongao.mainclient.model.bean.course.ExamItem;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseFragment;
import com.bjjy.mainclient.phone.event.UpdateEvent;
import com.bjjy.mainclient.phone.view.classroom.subject.SubjectActivity;
import com.bjjy.mainclient.phone.view.exam.activity.ExamList.ExamListActivity;
import com.bjjy.mainclient.phone.view.exam.activity.course.CourseMoreActivity;
import com.bjjy.mainclient.phone.view.play.PlayActivity;
import com.bjjy.mainclient.phone.view.question.MyQuestionListActivity;
import com.bjjy.mainclient.phone.view.question.RecommQueActivity;
import com.bjjy.mainclient.phone.widget.EmptyViewLayout;
import com.bjjy.mainclient.phone.widget.swipestack.SwipeFlingAdapterView;
import java.util.ArrayList;
import java.util.List;
import butterknife.Bind;
import butterknife.ButterKnife;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class CourseFragment extends BaseFragment implements CourseFragmentView,SwipeFlingAdapterView.onFlingListener,
        SwipeFlingAdapterView.OnItemClickListener {

    @Bind(R.id.top_title_text)
    TextView top_title_text;

    @Bind(R.id.ll_title)
    LinearLayout ll_title;

    @Bind(R.id.course_fragment_course_rl)
    RelativeLayout course_fragment_course_rl;

    @Bind(R.id.course_fragment_exam_rl)
    RelativeLayout course_fragment_exam_rl;

    @Bind(R.id.course_fragment_course_layout)
    LinearLayout course_fragment_course_layout;

    @Bind(R.id.course_fragment_exam_layout)
    LinearLayout course_fragment_exam_layout;

    @Bind(R.id.course_fragment_answer_layout)
    LinearLayout course_fragment_answer_layout;

    @Bind(R.id.swipeStack)
    SwipeFlingAdapterView mSwipeStack;

    @Bind(R.id.swipe_container)
    SwipeRefreshLayout mSwipeRefreshLayout;

    private CourseListAdapter mAdapter;
    private EmptyViewLayout mEmptyLayout;
    LayoutInflater inflater;
    List<CourseItem> courseItems;

    private int cardWidth;
    private int cardHeight;

    private CourseFragmentPersenter courseFragmentPersenter;

    private View mRootView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mRootView == null){
            mRootView = inflater.inflate(R.layout.course_fragment,container,false);
        }
        ButterKnife.bind(this,mRootView);
        ViewGroup parent = (ViewGroup) mRootView.getParent();
        if (parent != null){
            parent.removeView(mRootView);
        }
        courseFragmentPersenter = new CourseFragmentPersenter();
        courseFragmentPersenter.attachView(this);
        initView();
        initData();
        return mRootView;
    }

    @Override
    public void initView() {
        // TODO Auto-generated method stub
        mEmptyLayout = new EmptyViewLayout(getActivity(),mSwipeRefreshLayout);
        setTitle();
        ll_title.setOnClickListener(this);
        course_fragment_course_rl.setOnClickListener(this);
        course_fragment_exam_rl.setOnClickListener(this);

        inflater = LayoutInflater.from(getActivity());

        mSwipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.color_primary));
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                showLoading();
                courseFragmentPersenter.getData();
            }
        });

        mEmptyLayout.setShowErrorButton(true);

        mEmptyLayout.setErrorButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initData();

            }
        });

        setCourse();
    }

    private void setCourse(){
        DisplayMetrics dm = getResources().getDisplayMetrics();
        float density = dm.density;
        cardWidth = (int) (dm.widthPixels - (2 * 18 * density));
        cardHeight = (int) (dm.heightPixels - (338 * density));

        mSwipeStack.setFlingListener(this);
        mSwipeStack.setOnItemClickListener(this);
    }

    private void setTitle(){
        String subjectName = SharedPrefHelper.getInstance(getActivity()).getSubjectName();
        if(subjectName == null || "".equals(subjectName)){
            top_title_text.setText("我的课堂");
        }else{
            top_title_text.setText(subjectName);
        }
    }

    @Override
    public void initData() {
        mEmptyLayout.showLoading();
        courseFragmentPersenter.getData();
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.ll_title:
                Intent intent = new Intent(getActivity(),SubjectActivity.class);
                startActivity(intent);
                break;
            case R.id.course_fragment_course_rl:
                Intent intent_course = new Intent(getActivity(),CourseMoreActivity.class);
                startActivity(intent_course);
                break;
            case R.id.course_fragment_exam_rl:
                Intent intent_exam = new Intent(getActivity(),ExamListActivity.class);
                startActivity(intent_exam);
                break;
            default:
                break;
        }
    }


    @Override
    public void showLoading() {
        if(mSwipeRefreshLayout != null){
            mSwipeRefreshLayout.setRefreshing(true);
        }
    }

    @Override
    public void hideLoading() {
        if(mSwipeRefreshLayout != null){
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void showRetry() {
    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {
        hideLoading();
        mEmptyLayout.showError();
    }

    @Override
    public Context context() {
        return getActivity();
    }

    @Override
    public void setView(CourseJson courseJson) {
        /**
         * 课程
         */
        hideLoading();
        mEmptyLayout.showContentView();
        //course_fragment_course_layout.removeAllViews();
        course_fragment_exam_layout.removeAllViews();
        course_fragment_answer_layout.removeAllViews();

        courseItems = courseJson.getCourseList();
        setCourseView();
        /**
         * 考试
         */
        for (ExamItem examItem : courseJson.getExaminationList()) {
            LinearLayout  exam_layout = (LinearLayout) inflater.inflate(R.layout.course_fragment_exam_item,null);
            TextView title = (TextView) exam_layout.findViewById(R.id.title);
            TextView subTitle = (TextView) exam_layout.findViewById(R.id.subTitle);
            title.setText(examItem.getTitle());
            subTitle.setText(examItem.getSubTitle());
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.topMargin = 50;
            //params.leftMargin = 30;
            //params.rightMargin = 30;

            exam_layout.setLayoutParams(params);
            course_fragment_exam_layout.addView(exam_layout);

            exam_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //设置测试参数的类型为能力评估
                    SharedPrefHelper.getInstance(getActivity()).setExamTag(Constants.EXAM_TAG_ABILITY);
                    Intent intent_answer = new Intent(getActivity(),ExamActivity.class);
                    startActivity(intent_answer);
                }
            });
        }

        /**
         * 答疑
         */
        for (int i = 0; i < courseJson.getQasList().size(); i++) {
            AnswerItem answerItem = courseJson.getQasList().get(i);
            LinearLayout  answer_layout = (LinearLayout) inflater.inflate(R.layout.course_fragment_answer_item,null);
            ImageView iv = (ImageView) answer_layout.findViewById(R.id.icon);
            TextView title = (TextView) answer_layout.findViewById(R.id.title);
            TextView subTitle = (TextView) answer_layout.findViewById(R.id.subTitle);

           /* Picasso.with(getActivity())
                    .load(answerItem.getImageUrl())
                    .placeholder(R.drawable.ic_launcher)
                    .error(R.drawable.ic_error)
                    .into(iv);*/

            title.setText(answerItem.getTitle());
            subTitle.setText(answerItem.getSubTitle());

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            answer_layout.setLayoutParams(params);
            course_fragment_answer_layout.addView(answer_layout);
            View  view = inflater.inflate(R.layout.app_line,null);
            course_fragment_answer_layout.addView(view);
            if(i < courseJson.getQasList().size()-1){
                course_fragment_answer_layout.addView(inflater.inflate(R.layout.app_line,null));
            }

            final int tag = i;
            answer_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(tag == 0){
                        Intent intent_answer = new Intent(getActivity(),MyQuestionListActivity.class);
                        startActivity(intent_answer);
                    }else{
                        Intent intent_answer = new Intent(getActivity(),RecommQueActivity.class);
                        startActivity(intent_answer);
                    }
                }
            });
        }
    }

    private void setCourseView(){
        mAdapter = new CourseListAdapter(getActivity());
        mAdapter.setList((ArrayList<CourseItem>)courseItems);
        mSwipeStack.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }


    @Subscribe
    public void onEventAsync(UpdateEvent event)
    {
        setTitle();
        initData();
    }

    @Override
    public void onItemClicked(MotionEvent event, View v, Object dataObject) {
        if (v.getTag() instanceof CourseListAdapter.ViewHolder) {
            int x = (int) event.getRawX();
            int y = (int) event.getRawY();
            CourseListAdapter.ViewHolder vh = (CourseListAdapter.ViewHolder) v.getTag();
            View child = vh.iv;
            Rect outRect = new Rect();
            child.getGlobalVisibleRect(outRect);
            if (outRect.contains(x, y)) {
                Intent intent = new Intent(getActivity(), PlayActivity.class);
                startActivity(intent);
            } else {
                outRect.setEmpty();
                child = vh.subTitle;
                child.getGlobalVisibleRect(outRect);
                if (outRect.contains(x, y)) {
                    Intent intent = new Intent(getActivity(), PlayActivity.class);
                    startActivity(intent);
                }
            }
        }
    }

    @Override
    public void removeFirstObjectInAdapter() {
        mAdapter.remove(0);
    }

    @Override
    public void onLeftCardExit(Object dataObject) {

    }

    @Override
    public void onRightCardExit(Object dataObject) {

    }

    @Override
    public void onAdapterAboutToEmpty(int itemsInAdapter) {
        if(itemsInAdapter == 4){

        }
        setCourseView();
    }

    @Override
    public void onScroll(float progress, float scrollXProgress) {

    }
}