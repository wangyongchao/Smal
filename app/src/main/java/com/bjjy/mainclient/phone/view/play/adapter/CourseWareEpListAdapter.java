package com.bjjy.mainclient.phone.view.play.adapter;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.AppConfig;
import com.bjjy.mainclient.phone.download.DownloadExcutor;
import com.bjjy.mainclient.phone.download.db.DownloadDB;
import com.bjjy.mainclient.phone.utils.NetWorkUtils;
import com.bjjy.mainclient.phone.view.play.PlayActivity;
import com.bjjy.mainclient.phone.view.play.widget.RoundProgressBar;
import com.bjjy.mainclient.phone.widget.DialogManager;
import com.dongao.mainclient.model.bean.play.CourseDetail;
import com.dongao.mainclient.model.bean.play.CourseWare;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;


/**
 * Created by fengzongwei on 2016/1/4.
 */
public class CourseWareEpListAdapter extends BaseExpandableListAdapter {

    private CourseDetail courseDetail;
    private PlayActivity context;
    private DownloadDB db;
    private Handler handler;

    //    private final int[] childeType = {0,1};
    public CourseWareEpListAdapter(PlayActivity context, CourseDetail courseDetail) {
        this.context = context;
        this.courseDetail = courseDetail;
        db = new DownloadDB(context);
    }

    public CourseWareEpListAdapter(PlayActivity context, CourseDetail courseDetail, Handler handler) {
        this.context = context;
        this.courseDetail = courseDetail;
        db = new DownloadDB(context);
        this.handler = handler;
    }

    @Override
    public int getGroupCount() {
//        return courseDetail.getMobileSectionList().size();
        return 0;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
//        return courseDetail.getMobileSectionList().get(groupPosition).getMobileCourseWareList().size();
        return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return null;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

//    @Override
//    public int getGroupType(int groupPosition) {
//        if(groupList.get(groupPosition).getSectionId()!=null && groupList.get(groupPosition).getSectionId().equals("kehouzuoye")){
//            return groupType[1];
//        }else{
//            return groupType[0];
//        }
//    }
//
//    @Override
//    public int getGroupTypeCount() {
//        return groupType.length;
//    }
//
//    @Override
//    public int getChildTypeCount() {
//        return childeType.length;
//    }

//    @Override
//    public int getChildType(int groupPosition, int childPosition) {
//        if(courseDetail.getMobileSectionList().get(groupPosition).getSectionId().equals("kehouzuoyezu")){
//            return childeType[1];
//        }else{
//            return childeType[0];
//        }
//    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        ViewHolderGroup viewHolderGroup = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.play_courselist_group, parent, false);
            viewHolderGroup = new ViewHolderGroup();
            viewHolderGroup.textView_groupTitle = (TextView) convertView.findViewById(R.id.play_courselist_group_tv);
            viewHolderGroup.imageView_arrow = (ImageView) convertView.findViewById(R.id.play_courselist_group_img);
            viewHolderGroup.linearLayout_course_body = (LinearLayout) convertView.findViewById(R.id.play_courselist_group_course_body);
            viewHolderGroup.linearLayout_practice_body = (LinearLayout) convertView.findViewById(R.id.play_courselist_fragment_practice_body);
            viewHolderGroup.line = convertView.findViewById(R.id.play_courselist_group_line);
            convertView.setTag(viewHolderGroup);
        } else {
            viewHolderGroup = (ViewHolderGroup) convertView.getTag();
        }

//        if (courseDetail.getMobileSectionList().get(groupPosition).getSectionId() != null &&
//                courseDetail.getMobileSectionList().get(groupPosition).getSectionId().equals("kehouzuoye")) {
//            viewHolderGroup.linearLayout_practice_body.setVisibility(View.VISIBLE);
//            viewHolderGroup.linearLayout_course_body.setVisibility(View.GONE);
//            viewHolderGroup.line.setVisibility(View.GONE);
//        } else {//非课后作业组的操作
//            if (isExpanded) {
//                viewHolderGroup.line.setVisibility(View.VISIBLE);
//            } else {
//                if (courseDetail.getMobileSectionList().size() > 1) {
//                    if (groupPosition != (courseDetail.getMobileSectionList().size() - 1))
//                        viewHolderGroup.line.setVisibility(View.GONE);
//                    else
//                        viewHolderGroup.line.setVisibility(View.VISIBLE);
//                } else {
//                    viewHolderGroup.line.setVisibility(View.VISIBLE);
//                }
//            }
//            viewHolderGroup.linearLayout_practice_body.setVisibility(View.GONE);
//            viewHolderGroup.linearLayout_course_body.setVisibility(View.VISIBLE);
//            if (isExpanded) {
//                viewHolderGroup.imageView_arrow.setImageResource(R.drawable.play_courselist_group_up);
//            } else {
//                viewHolderGroup.imageView_arrow.setImageResource(R.drawable.play_courselist_group_dowm);
//            }
//            viewHolderGroup.textView_groupTitle.setText(courseDetail.getMobileSectionList().get(groupPosition).getSectionName());
//        }

        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ViewHolderChildCourse viewHolderChildCourse = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.play_courselist_child, parent, false);
            viewHolderChildCourse = new ViewHolderChildCourse();
            viewHolderChildCourse.textView_title = (TextView) convertView.findViewById(R.id.play_courselist_child_tv);
            viewHolderChildCourse.linearLayout_body = (LinearLayout) convertView.findViewById(R.id.play_courselist_child_body);
            viewHolderChildCourse.imageView_test = (ImageView) convertView.findViewById(R.id.play_courselist_child_test_img);
            viewHolderChildCourse.linearLayout_download = (LinearLayout) convertView.findViewById(R.id.play_courselist_child_download_body);
            viewHolderChildCourse.imageView_dowanload = (ImageView) convertView.findViewById(R.id.play_courselist_child_download_img);
            viewHolderChildCourse.line_top = convertView.findViewById(R.id.play_courselist_child_line_top);
            viewHolderChildCourse.line_down = convertView.findViewById(R.id.play_courselist_child_line_down);
            viewHolderChildCourse.imageView_play_flag = (ImageView) convertView.findViewById(R.id.play_courselist_child_play_flag_img);
            viewHolderChildCourse.circularProgressBar = (RoundProgressBar) convertView.findViewById(R.id.play_courselist_child_download_circle_pb);
            viewHolderChildCourse.line_ex = convertView.findViewById(R.id.play_courselist_child_exline);
            convertView.setTag(viewHolderChildCourse);
        } else {
            viewHolderChildCourse = (ViewHolderChildCourse) convertView.getTag();
        }

        viewHolderChildCourse.imageView_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

//        viewHolderChildCourse.linearLayout_body.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                Message message = handler.obtainMessage();
//                message.what = 1;
//                message.obj = courseDetail.getMobileSectionList().get(groupPosition).getMobileCourseWareList().
//                        get(childPosition);
//                handler.sendMessage(message);
//                return false;
//            }
//        });
//        viewHolderChildCourse.linearLayout_body.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Message message = handler.obtainMessage();
//                message.what = 2;
//                message.arg1 = groupPosition;
//                message.arg2 = childPosition;
//                message.obj = courseDetail.getMobileSectionList().get(groupPosition).getMobileCourseWareList().
//                        get(childPosition);
//                handler.sendMessage(message);
//            }
//        });
//
//        if (courseDetail.getMobileSectionList().get(groupPosition).getMobileCourseWareList().get(childPosition).isPlayFinished())
//            viewHolderChildCourse.textView_title.setTextColor(Color.parseColor("#DBDBDB"));
//        else
//            viewHolderChildCourse.textView_title.setTextColor(Color.BLACK);
//
//        //state == -99 &&
////        int state = courseDetail.getMobileSectionList().get(groupPosition).getMobileCourseWareList().get(childPosition).getState();
//        int state = -1;
//        if (!courseDetail.getMobileSectionList().get(groupPosition).getMobileCourseWareList().get(childPosition).getCwId().equals("kehouzuoye")) {
//            state = db.getState(SharedPrefHelper.getInstance(context).getUserId(),
//                    courseDetail.getMobileSectionList().get(groupPosition).getMobileCourseWareList().get(childPosition).getClassId(),
//                    courseDetail.getMobileSectionList().get(groupPosition).getMobileCourseWareList().get(childPosition).getCwId());
//        }
////        int percent = 0;
////        if (state == Constants.STATE_DownLoading || state == Constants.STATE_Pause) {
////            percent = db.getPercent(SharedPrefHelper.getInstance(context).getUserId(),
////                    courseDetail.getMobileSectionList().get(groupPosition).getMobileCourseWareList().get(childPosition).getClassId(),
////                    courseDetail.getMobileSectionList().get(groupPosition).getMobileCourseWareList().get(childPosition).getCwId());
////        }
//
////        if (courseDetail.getMobileSectionList().get(groupPosition).getMobileCourseWareList().get(childPosition).getPracticeFlag() == 1) {
////            viewHolderChildCourse.imageView_test.setVisibility(View.VISIBLE);
////            if (courseDetail.getMobileSectionList().get(groupPosition).getMobileCourseWareList().get(childPosition).getCwId().equals("kehouzuoye")) {
////                viewHolderChildCourse.imageView_dowanload.setVisibility(View.GONE);
////                viewHolderChildCourse.circularProgressBar.setVisibility(View.GONE);
////                viewHolderChildCourse.imageView_test.setImageResource(R.drawable.ico_list_right);
////            } else {
////                viewHolderChildCourse.imageView_dowanload.setVisibility(View.GONE);
////                viewHolderChildCourse.circularProgressBar.setVisibility(View.GONE);
////                viewHolderChildCourse.imageView_test.setImageResource(R.drawable.play_courselist_practice);
////            }
////        } else {
//            viewHolderChildCourse.imageView_test.setVisibility(View.GONE);
////        }
////        viewHolderChildCourse.imageView_test.setVisibility(View.GONE);
//
//        if (state == Constants.STATE_DownLoaded) {
////            viewHolderChildCourse.circularProgressBar.setVisibility(View.GONE);
//            viewHolderChildCourse.imageView_dowanload.setVisibility(View.VISIBLE);
////            viewHolderChildCourse.imageView_dowanload.setImageResource(R.drawable.play_courselist_downloaded);
//        } else {
//            viewHolderChildCourse.imageView_dowanload.setVisibility(View.GONE);
//        }
//// else if (state == Constants.STATE_DownLoading) {
////            viewHolderChildCourse.circularProgressBar.setVisibility(View.VISIBLE);
////            viewHolderChildCourse.imageView_dowanload.setVisibility(View.GONE);
////            viewHolderChildCourse.circularProgressBar.setBackgroundResource(R.drawable.play_courselist_downloading);
////            viewHolderChildCourse.circularProgressBar.setProgress(percent);
////        } else if (state == Constants.STATE_Pause) {
////            viewHolderChildCourse.circularProgressBar.setVisibility(View.VISIBLE);
////            viewHolderChildCourse.imageView_dowanload.setVisibility(View.GONE);
////            viewHolderChildCourse.circularProgressBar.setBackgroundResource(R.drawable.play_courselist_download_pause);
////            viewHolderChildCourse.circularProgressBar.setProgress(percent);
////        } else if (state == Constants.STATE_Waiting) {
////            viewHolderChildCourse.circularProgressBar.setVisibility(View.GONE);
////            viewHolderChildCourse.imageView_dowanload.setVisibility(View.VISIBLE);
////            viewHolderChildCourse.imageView_dowanload.setImageResource(R.drawable.play_courselist_download_waiting);
////        } else if (state == Constants.STATE_Error) {
////            viewHolderChildCourse.circularProgressBar.setVisibility(View.GONE);
////            viewHolderChildCourse.imageView_dowanload.setVisibility(View.VISIBLE);
////            viewHolderChildCourse.imageView_dowanload.setImageResource(R.drawable.play_courselist_download_fail);
////        } else {
////            viewHolderChildCourse.circularProgressBar.setVisibility(View.GONE);
////            viewHolderChildCourse.imageView_dowanload.setVisibility(View.VISIBLE);
////            viewHolderChildCourse.imageView_dowanload.setImageResource(R.drawable.play_courselist_undownload);
////        }
//
//        if (courseDetail.getMobileSectionList().get(groupPosition).getMobileCourseWareList().get(childPosition).getCwId().equals("kehouzuoye")) {
//            viewHolderChildCourse.imageView_dowanload.setVisibility(View.GONE);
//            viewHolderChildCourse.circularProgressBar.setVisibility(View.GONE);
//        }
//
//        if (courseDetail.getMobileSectionList().get(groupPosition).getMobileCourseWareList().size() == 1) {
//            viewHolderChildCourse.line_down.setVisibility(View.INVISIBLE);
//            viewHolderChildCourse.line_top.setVisibility(View.INVISIBLE);
//            viewHolderChildCourse.line_ex.setVisibility(View.GONE);
//        } else {
//            if (childPosition == (courseDetail.getMobileSectionList().get(groupPosition).getMobileCourseWareList().size() - 1)) {
//                viewHolderChildCourse.line_down.setVisibility(View.INVISIBLE);
//                viewHolderChildCourse.line_top.setVisibility(View.VISIBLE);
//                viewHolderChildCourse.line_ex.setVisibility(View.GONE);
//            } else if (childPosition == 0) {
//                viewHolderChildCourse.line_ex.setVisibility(View.VISIBLE);
//                viewHolderChildCourse.line_down.setVisibility(View.VISIBLE);
//                viewHolderChildCourse.line_top.setVisibility(View.INVISIBLE);
//            } else {
//                viewHolderChildCourse.line_ex.setVisibility(View.VISIBLE);
//                viewHolderChildCourse.line_down.setVisibility(View.VISIBLE);
//                viewHolderChildCourse.line_top.setVisibility(View.VISIBLE);
//            }
//        }
//
//        //正常的课子view操作
//        if (courseDetail.getMobileSectionList().get(groupPosition).getMobileCourseWareList()
//                .get(childPosition).getChapterNo() != null)
//            viewHolderChildCourse.textView_title.setText(courseDetail.getMobileSectionList().get(groupPosition).getMobileCourseWareList()
//                    .get(childPosition).getChapterNo() + " " +
//                    courseDetail.getMobileSectionList().get(groupPosition).getMobileCourseWareList()
//                            .get(childPosition).getCwName());
//        else
//            viewHolderChildCourse.textView_title.setText(courseDetail.getMobileSectionList().get(groupPosition).getMobileCourseWareList()
//                    .get(childPosition).getCwName());
//
//        if (courseDetail.getMobileSectionList().get(groupPosition).getMobileCourseWareList().
//                get(childPosition).getCwId().equals(context.getPlayingCWId()) &&
//                !courseDetail.getMobileSectionList().get(groupPosition).getSectionId().equals("kehouzuoyezu")) {
//            viewHolderChildCourse.textView_title.setTextColor(Color.parseColor("#ff7666"));
//            viewHolderChildCourse.imageView_play_flag.setImageResource(R.drawable.play_courselist_playing);
//        } else {
////            viewHolderChildCourse.textView_title.setTextColor(Color.BLACK);
//            viewHolderChildCourse.imageView_play_flag.setImageResource(R.drawable.play_courselist_unplay);
//        }
//        viewHolderChildCourse.circularProgressBar.setVisibility(View.GONE);
////        viewHolderChildCourse.imageView_dowanload.setVisibility(View.GONE);
//
////        viewHolderChildCourse.imageView_dowanload.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                MobclickAgent.onEvent(context, PushConstants.PLAY_DOWNLOAD);
////                context.checkMachine(new DownloadView() {
////                    @Override
////                    public void isCheckMachineOk(String msg, boolean flag) {
////                        if (flag) {
////                            CourseWare cw = courseDetail.getMobileSectionList().get(groupPosition).getMobileCourseWareList().get(childPosition);
////                            checkDownState(cw);
////                            if(cw.getPracticeFlag()==1){
////                                context.downloadExam();
////                            }
////                        } else {
////                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
////                        }
////                    }
////                });
////
////            }
////        });
//        viewHolderChildCourse.circularProgressBar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                CourseWare cw = courseDetail.getMobileSectionList().get(groupPosition).getMobileCourseWareList().get(childPosition);
//                checkDownState(cw);
//            }
//        });
        return convertView;
    }

    private void checkDownState(CourseWare cw) {
        int state = db.getState(SharedPrefHelper.getInstance(context).getUserId(), cw.getClassId(), cw.getCwId());
        if (state == Constants.STATE_DownLoading) {
            cw.setState(Constants.STATE_Pause);
            DownloadExcutor.getInstance(context).setFlag(false);
            db.updateState(SharedPrefHelper.getInstance(context).getUserId(), cw.getClassId(), cw.getCwId(), Constants.STATE_Pause);
            notifyDataSetChanged();
        } else if (state == Constants.STATE_Waiting) {
            db.updateState(SharedPrefHelper.getInstance(context).getUserId(), cw.getClassId(), cw.getCwId(), Constants.STATE_Pause);
            notifyDataSetChanged();
            if (DownloadExcutor.getInstance(context).task != null) {
                if (DownloadExcutor.getInstance(context).task.getCourseWareId().equals(cw.getCwId()) && DownloadExcutor.getInstance(context).task.getCourseId().equals(cw.getClassId())) {
                    DownloadExcutor.getInstance(context).setFlag(false);
                }
            }
        } else if (state == Constants.STATE_Pause || state == Constants.STATE_Error || state == 0) {
            cw.setState(Constants.STATE_DownLoading);
            CheckSettingDownload(cw);
        }
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    static class ViewHolderGroup {
        TextView textView_groupTitle;
        ImageView imageView_arrow;
        LinearLayout linearLayout_course_body;
        LinearLayout linearLayout_practice_body;
        View line;
    }

    static class ViewHolderChildCourse {
        TextView textView_title;
        ImageView imageView_test;
        LinearLayout linearLayout_download, linearLayout_body;
        ImageView imageView_dowanload, imageView_play_flag;
        View line_top, line_down, line_ex;
        RoundProgressBar circularProgressBar;
    }

    /**
     * 下载前检查网络
     */
    private void CheckSettingDownload(final CourseWare cw) {
        boolean isDownload = SharedPrefHelper.getInstance(context).getIsNoWifiPlayDownload();
        NetWorkUtils type = new NetWorkUtils(context);
        if (type.getNetType() == 0) {//无网络
            DialogManager.showMsgDialog(context, context.getResources().getString(R.string.dialog_message_vedio), context.getResources().getString(R.string.dialog_title_download), "确定");
        } else if (type.getNetType() == 2) { //流量
            if (!isDownload) {
                DialogManager.showNormalDialog(context, context.getResources().getString(R.string.dialog_warnning_vedio), "提示", "取消", "确定", new DialogManager.CustomDialogCloseListener() {
                    @Override
                    public void yesClick() {
                        db.updateState(SharedPrefHelper.getInstance(context).getUserId(), cw.getClassId(), cw.getCwId(), Constants.STATE_Waiting);
                        notifyDataSetChanged();
                        AppConfig.getAppConfig(context).download(cw);
                    }

                    @Override
                    public void noClick() {
                    }
                });
            } else {
                db.updateState(SharedPrefHelper.getInstance(context).getUserId(), cw.getClassId(), cw.getCwId(), Constants.STATE_Waiting);
                notifyDataSetChanged();
                AppConfig.getAppConfig(context).download(cw);
            }
        } else if (type.getNetType() == 1) {//wifi
            db.updateState(SharedPrefHelper.getInstance(context).getUserId(), cw.getClassId(), cw.getCwId(), Constants.STATE_Waiting);
            notifyDataSetChanged();
            AppConfig.getAppConfig(context).download(cw);
        }
    }
}
