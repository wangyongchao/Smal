package com.bjjy.mainclient.phone.view.setting.cache.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;

import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseFragment;
import com.bjjy.mainclient.phone.download.db.DownloadDB;
import com.bjjy.mainclient.phone.event.DeleteEvent;
import com.bjjy.mainclient.phone.view.exam.activity.exampaperlist.bean.Course;
import com.bjjy.mainclient.phone.view.play.adapter.WrapperExpandableListAdapter;
import com.bjjy.mainclient.phone.view.setting.cache.CacheActivity;
import com.bjjy.mainclient.phone.view.setting.cache.CachedCourseActivity;
import com.bjjy.mainclient.phone.view.setting.cache.adapter.CachedExpandAdapter;
import com.bjjy.mainclient.phone.view.setting.cache.domain.GourpDomain;
import com.dongao.mainclient.model.local.SharedPrefHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by dell on 2016/5/16.
 */
public class CacheCourseFragment extends BaseFragment implements ExpandableListView.OnChildClickListener {

    @Bind(R.id.cached_ls)
    ExpandableListView cachedLs;
    @Bind(R.id.local_notask)

    LinearLayout localNotask;
    private View view;
//    private CachedAdapter adapter;

    private DownloadDB db;
    private List<Course> list;
    private List<GourpDomain> subjects;
    private CacheActivity activity;
    private CachedExpandAdapter expandAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        view = inflater.inflate(R.layout.cachecourse_fragment, null);
        ButterKnife.bind(this, view);
//        activity= (MainActivity) getActivity();
        activity= (CacheActivity) getActivity();
        initView();

        initData();
        return view;
    }

    @Override
    public void initView() {
        db = new DownloadDB(getActivity());

//        adapter = new CachedAdapter(activity,imageLoader);
        expandAdapter=new CachedExpandAdapter(activity,imageLoader);
        wrapperAdapter = new WrapperExpandableListAdapter(expandAdapter);
//        cachedLs.setOnItemClickListener(this);
        cachedLs.setOnChildClickListener(this);

        localNotask.setOnClickListener(this);
    }

    private GourpDomain other;
    private List<Course> otherChild;
    private List<GourpDomain> subjectsNew;
    private WrapperExpandableListAdapter wrapperAdapter;
    @Override
    public void initData() {
        other=new GourpDomain();
        otherChild =new ArrayList<>();
        subjectsNew=new ArrayList<>();
            subjects=db.getSubjects(SharedPrefHelper.getInstance(getActivity()).getUserId());
            if(subjects != null && subjects.size() > 0){
                for(int i=0;i<subjects.size();i++){
                    if(subjects.get(i).getName().equals("其他")){
                        otherChild.addAll(subjects.get(i).getChilds());
                        other.setName("其他");
                        other.setChilds(otherChild);
                    }else{
                        subjectsNew.add(subjects.get(i));
                    }
                }
                if(other!=null && !TextUtils.isEmpty(other.getName())){
                    subjectsNew.add(other);
                }
                localNotask.setVisibility(View.GONE);
                cachedLs.setVisibility(View.VISIBLE);

                expandAdapter.setList(subjectsNew);
                cachedLs.setAdapter(wrapperAdapter);
                int count = cachedLs.getCount();
                for (int i = 0; i < count; i++) {
                    cachedLs.expandGroup(i);
                }
            }else{
                if(localNotask!=null){
                    localNotask.setVisibility(View.VISIBLE);
                }
                if(cachedLs!=null){
                    cachedLs.setVisibility(View.GONE);
                }
                if(activity!=null){
                    activity.initData();
                }

            }
      
       
//        list = db.findCourses(SharedPrefHelper.getInstance(getActivity()).getUserId());
//        if (list != null && list.size() > 0) {
//            localNotask.setVisibility(View.GONE);
//            cachedLs.setVisibility(View.VISIBLE);
//
//            adapter.setList(list);
//            cachedLs.setAdapter(adapter);
//        } else {
//            localNotask.setVisibility(View.VISIBLE);
//            cachedLs.setVisibility(View.GONE);
//            activity.initData();
//        }
    }

    private boolean first;
    @Override
    public void onResume() {
        super.onResume();
        if(first){
            initData();
        }
        first=true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.local_notask:
//                downlaod();
                break;
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    //    @Override
//    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        builder.setTitle("确定删除吗");
//        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                Toast.makeText(getActivity(), "sure", Toast.LENGTH_SHORT).show();
//            }
//        });
//        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                Toast.makeText(getActivity(), "cancel", Toast.LENGTH_SHORT).show();
//            }
//        });
//        builder.show();
//        return false;
//    }

//    @Override
//    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//        Intent intent = new Intent(getActivity(), CachedCourseActivity.class);
//        intent.putExtra("courseId", list.get(position).getCwCode());
//        intent.putExtra("title",list.get(position).getCwName());
//        startActivity(intent);
//    }

    //onEventAsync
    @Subscribe
    public void onEventMainThread(DeleteEvent event) {
        initData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        try{    //无缘无故下标越界
            Course course=subjects.get(groupPosition).getChilds().get(childPosition);
            Intent intent = new Intent(getActivity(), CachedCourseActivity.class);
            intent.putExtra("courseId", course.getCwCode());
            intent.putExtra("title",course.getCwName());
            startActivity(intent);
        }catch (Exception e){
            initData();
        }

        return false;
    }
}
