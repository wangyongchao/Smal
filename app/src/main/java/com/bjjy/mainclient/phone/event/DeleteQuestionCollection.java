package com.bjjy.mainclient.phone.event;

/**
 * Created by fengzongwei on 2016/7/27 0027.
 * 在“我的”中删除收藏的答疑数据时发送的事件
 */
public class DeleteQuestionCollection {

    private String askId;//删除的答疑id

    public DeleteQuestionCollection(String askId){
        this.askId = askId;
    }

    public String getAskId(){
        return askId;
    }

}
