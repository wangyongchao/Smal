package com.bjjy.mainclient.phone.view.question;

import android.content.Context;
import android.content.Intent;
import android.graphics.Picture;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bjjy.mainclient.persenter.RecommQuePersenter;
import com.bjjy.mainclient.phone.app.BaseActivity;
import com.bjjy.mainclient.phone.utils.NetworkUtil;
import com.bjjy.mainclient.phone.widget.RefreshScrollView;
import com.dongao.mainclient.model.bean.question.Question;
import com.bjjy.mainclient.phone.R;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by fengzongwei on 2016/5/3 0003.
 * （废弃）
 */
public class RecommQueActivity extends BaseActivity implements RecommQueView,RefreshScrollView.OnRefreshScrollViewListener {
//    @Bind(R.id.top_title_left)
//    ImageView imageView_back;
//    @Bind(R.id.top_title_right)
//    TextView textView_que;
    @Bind(R.id.recommand_ques_list_lodingBody)
    RelativeLayout relativeLayout_hint_body;
    @Bind(R.id.recommand_ques_list_pb)
    ProgressBar progressBar;
    @Bind(R.id.recommand_ques_list_picBody)
    LinearLayout linearLayout_pic_body;
    @Bind(R.id.recommand_ques_list_pic_tvHint)
    TextView textView_hint;
    @Bind(R.id.top_title_text)
    TextView textView_title;
    @Bind(R.id.recommande_scrol)
    RefreshScrollView refreshScrollView;

    private int maxHeight = 220;
    private RelativeLayout.LayoutParams layoutParams_content = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, maxHeight);
    private RelativeLayout.LayoutParams layoutParams_contentAll = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

    private static final String encoding = "utf-8";
    private static final String mimeType = "text/html";

    private int index;
    private Intent intent;
    private String questionId,examId,subjectId;

    private String subjectPass;// 是否通过了验证，有资格可以进行提问
    private boolean isAskSuccess = false;//用户是否成功进行了提问
    private String isCanAskHint;//用户是否可以进行提问的提示文字

    private ArrayList<View> viewList = new ArrayList<>();
    private List<TextView> list_allBt = new ArrayList<>();
    private List<View> list_web = new ArrayList<>();

    private List<Question> lisMyQuestionObjs;//存放所以数据的集合

    private RecommQuePersenter recommQuePersenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question_recommand_main);
        ButterKnife.bind(this);
        recommQuePersenter = new RecommQuePersenter(examId,questionId,subjectId);
        recommQuePersenter.attachView(this);
        initData();
        initView();
    }

    @Override
    public void initView() {
        refreshScrollView.setOnRefreshScrollViewListener(this);
    }

    @Override
    public void initData() {
        intent = getIntent();
//        questionId = intent.getStringExtra("questionId");
//        examId= SharedPrefHelper.getInstance(this).getExamId();
//        subjectId=SharedPrefHelper.getInstance(this).getSubjectId();
        getData();
    }


    private void getData(){
//		SimpleDateFormat sm=new SimpleDateFormat("MM-dd HH:mm:ss");
//		SharedPrefHelper.getInstance().setRecommandQuestionTime(sm.format(new Date()));
//		refreshScrollView.setRefreshTime(SharedPrefHelper.getInstance().getRecommandQuestionTime());
        if(NetworkUtil.isNetworkAvailable(RecommQueActivity.this)) {
            if(lisMyQuestionObjs == null || lisMyQuestionObjs.size()==0) {
                showLoading();
            }
            recommQuePersenter.getData();
        }else{
            showError("网络异常,点击重试");
        }
    }

    @OnClick(R.id.recommand_ques_list_picBody) void retry(){
        getData();
    }

    @OnClick(R.id.top_title_left) void back(){
        finish();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void showLoading() {
        relativeLayout_hint_body.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        linearLayout_pic_body.setVisibility(View.GONE);
    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {
        refreshScrollView.stopRefresh();
        relativeLayout_hint_body.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        linearLayout_pic_body.setVisibility(View.VISIBLE);
        textView_hint.setText(message);
        refreshScrollView.stopRefresh();
    }


    @Override
    public Context context() {
        return this;
    }

    @Override
    public void showData(List<Question> lisMyQuestionObjs) {
        //TODO  判断当前页面的开启是在题目解析还是在课堂  由课堂开启的页面才展示原题按钮
        this.lisMyQuestionObjs = lisMyQuestionObjs;
        refreshScrollView.stopRefresh();
        freshContentView();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                relativeLayout_hint_body.setVisibility(View.GONE);
            }
        }, 500);
    }

    /**
     * 刷新scrollview的内容视图
     */
    private void freshContentView(){
        viewList.clear();
        for(int i=0;i<lisMyQuestionObjs.size();i++){
            index = i;
            View view = LayoutInflater.from(RecommQueActivity.this).inflate(R.layout.question_recommand_list_item,null);
            final TextView textView_time,textView_allContent1,textView_queTime;
            final WebView webView_que,webView_anw;
            ImageView imageView;
            final HtmlTextView htmlTextView_ques,htmlTextView_answ;
            LinearLayout linearLayout_contentBody;
            //TODO 进行新增的两个按钮的操作控制
            webView_que = (WebView) view.findViewById(R.id.list_item_quetContentWeb);
            webView_anw = (WebView) view.findViewById(R.id.list_item_anwContentWeb);
            htmlTextView_ques = (HtmlTextView)view.findViewById(R.id.list_item_quetContentHtml);
            htmlTextView_answ = (HtmlTextView)view.findViewById(R.id.list_item_anwContentHtml);
            htmlTextView_answ.setTag(i);
            webView_que.getSettings().setAppCacheEnabled(false);
            webView_que.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
            webView_que.getSettings().setDefaultTextEncodingName(encoding);
            webView_anw.getSettings().setAppCacheEnabled(false);
            webView_anw.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
            webView_anw.getSettings().setDefaultTextEncodingName(encoding);
            webView_anw.getSettings().setJavaScriptEnabled(true);
            textView_time = (TextView) view.findViewById(R.id.list_item_time);
            textView_queTime = (TextView)view.findViewById(R.id.list_item_quesTime);
            textView_allContent1 = (TextView) view.findViewById(R.id.list_item_allContent);
            textView_allContent1.setTag(i);
            list_allBt.add(textView_allContent1);
            imageView = (ImageView) view.findViewById(R.id.list_item_jinghuaImg);
            linearLayout_contentBody = (LinearLayout)view.findViewById(R.id.list_item_contentBody);
            if(lisMyQuestionObjs.get(i).getEssence()!=null && lisMyQuestionObjs.get(i).getEssence().equals("1")){
                imageView.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                layoutParams.setMargins(0,50,0,0);
                linearLayout_contentBody.setLayoutParams(layoutParams);
            }else{
                imageView.setVisibility(View.GONE);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                layoutParams.setMargins(0,0,0,0);
                linearLayout_contentBody.setLayoutParams(layoutParams);
            }

            webView_anw.setPictureListener(new WebView.PictureListener() {
                @Override
                public void onNewPicture(WebView w, Picture p) {
                    if (webView_anw.getVisibility() == View.VISIBLE) {
                        int content_height, totle_height;
                        content_height = w.getContentHeight();
                        totle_height = w.getHeight();
                        if (content_height < totle_height) {
                            lisMyQuestionObjs.get(index).setShowAllAns(0);
                        } else {
                            lisMyQuestionObjs.get(index).setShowAllAns(1);
                        }
                    }
                }
            });

            ViewTreeObserver viewTreeObserver = htmlTextView_answ.getViewTreeObserver();
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    if (htmlTextView_answ.getVisibility() == View.VISIBLE && lisMyQuestionObjs.get((int) htmlTextView_answ.getTag()).isShowAllAns() == -1) {
                        if (htmlTextView_answ.getHeight() >= maxHeight) {
                            lisMyQuestionObjs.get((int) htmlTextView_answ.getTag()).setShowAllAns(1);
                            htmlTextView_answ.setLayoutParams(layoutParams_content);
                            textView_allContent1.setText("全文");
                        } else {
                            lisMyQuestionObjs.get((int) htmlTextView_answ.getTag()).setShowAllAns(0);
                            textView_allContent1.setVisibility(View.GONE);
                        }
                        htmlTextView_answ.getViewTreeObserver().removeGlobalOnLayoutListener(this);//没有此句话，此监听listerner会一直监听，从而影响之后的setLayoutParams
                    }
                }
            });

            textView_allContent1.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (lisMyQuestionObjs.get(index).isShowAllAns() == 1) {
                        lisMyQuestionObjs.get(index).setShowAllAns(2);
                        textView_allContent1.setText("收起");
                        if(lisMyQuestionObjs.get((int)textView_allContent1.getTag()).getAnswer().contains("</td>")) {
                            webView_anw.setLayoutParams(layoutParams_contentAll);
                        }else {
                            htmlTextView_answ.setLayoutParams(layoutParams_contentAll);
                        }
                    } else {
                        lisMyQuestionObjs.get(index).setShowAllAns(1);
                        if(lisMyQuestionObjs.get((int)textView_allContent1.getTag()).getAnswer().contains("</td>")) {
                            webView_anw.setLayoutParams(layoutParams_content);
                        }else {
                            htmlTextView_answ.setLayoutParams(layoutParams_content);
                        }
                        textView_allContent1.setText("全文");
                    }
                }
            });
            if(lisMyQuestionObjs.get(i).getContent().contains("</td>")){
                webView_que.setVisibility(View.VISIBLE);
                webView_que.loadDataWithBaseURL("", "<font color='#808080' style='font-size:15px;'>" + (lisMyQuestionObjs.get(i).getContent()) + "</font>", mimeType, encoding, "");
            }else{
                htmlTextView_ques.setVisibility(View.VISIBLE);
                htmlTextView_ques.setHtmlFromString("<font color='#808080' style='font-size:15px;'>" + (lisMyQuestionObjs.get(i).getContent()) + "</font>", new HtmlTextView.RemoteImageGetter());
            }
            if(lisMyQuestionObjs.get(i).getAnswer().contains("</td>")){
                webView_anw.setVisibility(View.VISIBLE);
                list_web.add(webView_anw);
                webView_anw.loadDataWithBaseURL("", "<font color='#808080' style='font-size:15px;'>" + (lisMyQuestionObjs.get(i).getAnswer()) + "</font>", mimeType, encoding, "");
            }else{
                list_web.add(htmlTextView_answ);
                htmlTextView_answ.setVisibility(View.VISIBLE);
                htmlTextView_answ.setHtmlFromString("<font color='#808080' style='font-size:15px;'>" + (lisMyQuestionObjs.get(i).getAnswer()) + "</font>", new HtmlTextView.RemoteImageGetter());
            }
            textView_time.setText(lisMyQuestionObjs.get(i).getAnswerTime());
            textView_queTime.setText(lisMyQuestionObjs.get(i).getInterval());
            viewList.add(view);
        }
        refreshScrollView.refreshContainer(viewList);
    }


    @Override
    public String getQuestionId() {
        return questionId;
    }

    @Override
    public void onRefresh() {
        getData();
    }

    @Override
    public void onLoadMore() {

    }
}
