package com.bjjy.mainclient.phone.view.play.downloadmanager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dongao.mainclient.model.bean.play.CourseChapter;
import com.dongao.mainclient.model.bean.play.CourseWare;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseFragmentActivity;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by yunfei on 2016/11/29.
 */

public class DownLoadManagerActivity extends BaseFragmentActivity {

    @Bind(R.id.top_title_text)
    TextView top_title_text;
    @Bind(R.id.top_title_left)
    ImageView top_title_left;
    public static ArrayList<CourseChapter> courseChapterList;
    public static ArrayList<CourseWare> courseWareList;

    public static void startActivityByCourseList(Context context, ArrayList<CourseWare> list) {
        Intent intent = new Intent(context, DownLoadManagerActivity.class);
        courseWareList = list;
        context.startActivity(intent);
    }

    public static void startActivityByCourseChapterList(Context context, ArrayList<CourseChapter> list) {
        Intent intent = new Intent(context, DownLoadManagerActivity.class);
        courseChapterList = list;
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pay_download_manager_activity);
        ButterKnife.bind(this);
        initView();
        initData();
    }

    @Override
    public void initView() {
        top_title_left.setImageResource(R.drawable.nav_left);
        top_title_left.setVisibility(View.VISIBLE);
        top_title_left.setOnClickListener(this);
        top_title_text.setText("下载管理");
        if (courseWareList != null)
            getSupportFragmentManager().beginTransaction().add(R.id.fl_content, CoursWareListFragment.getNewInstance(courseWareList)).commit();
        if (courseChapterList != null)
            getSupportFragmentManager().beginTransaction().add(R.id.fl_content, CoursWareEpListFragment.getNewInstance(courseChapterList)).commit();
    }

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.top_title_left:
                finish();
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        courseChapterList = null;
        courseWareList = null;
        ButterKnife.unbind(this);
    }
}
