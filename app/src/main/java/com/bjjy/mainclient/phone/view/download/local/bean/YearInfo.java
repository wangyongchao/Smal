package com.bjjy.mainclient.phone.view.download.local.bean;

import java.io.Serializable;

/**
 * 登录后返回 年份列表 对象
 * @author fengxiong
 *
 */
public class YearInfo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -123966448887713733L;
    /** 年份 */
    private String yearName;
    /** 年度别名*/
    private String shortYear;
    

    public String getYearName() {
        return yearName;
    }

    public void setYearName(String yearName) {
        this.yearName = yearName;
    }

    public String getShowYear() {
        return shortYear;
    }

    public void setShowYear(String showYear) {
        this.shortYear = showYear;
    }
}
