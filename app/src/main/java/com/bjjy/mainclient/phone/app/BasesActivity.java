package com.bjjy.mainclient.phone.app;

import android.app.Activity;
import android.os.Bundle;

import com.dongao.mainclient.core.app.AppManager;
import com.umeng.analytics.MobclickAgent;
import com.umeng.message.PushAgent;

public abstract class BasesActivity extends Activity {

	protected AppContext appContext;
	public PushAgent mpAgent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		init();

	}

	// 初始化
	private void init() {
		// 添加Activity到堆栈
		AppManager.getAppManager().addActivity(this);
		appContext = (AppContext) AppContext.getApp().getApplicationContext();
        //注册消息组件
      //  EventBus.getDefault().register(this);
		//初始化推送
		mpAgent= PushAgent.getInstance(this);
		mpAgent.onAppStart();
	}

	//
	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onStop() {
		super.onStop();
	}

	@Override
	public void onDestroy() {
		// 结束Activity&从堆栈中移除
		AppManager.getAppManager().finishActivity(this);
        //EventBus.getDefault().unregister(this);
		super.onDestroy();
	}
	


}
