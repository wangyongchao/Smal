package com.bjjy.mainclient.phone.view.play.downloadmanager;

import com.dongao.mainclient.model.bean.play.CourseChapter;
import com.dongao.mainclient.model.bean.play.CourseWare;
import com.dongao.mainclient.model.mvp.MvpView;

import java.util.List;

/**
 * Created by yunfei on 2016/11/29.
 */

public interface DownLoadManagerView extends MvpView {

    void setCourseWareList(List<CourseWare> list);

    void setCourseChapterList(List<CourseChapter> courseChapters);

    //显示下载个数
    void setDownloadNum(int num);

    //显示全选按钮
    void showSelectAllText();

    //显示清除按钮
    void showClearAllText();

    //数据源改变 刷新列表
    void updateList();

    //显示没选择提示
    void showNoSelectHit();

    //重复下载
    void showRepeatDownLoadHit();

    void downloadingHit();

    void showNetWorkErrorHit();
}
