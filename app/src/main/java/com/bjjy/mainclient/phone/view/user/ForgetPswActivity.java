package com.bjjy.mainclient.phone.view.user;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bjjy.mainclient.persenter.ForgetPswPersenter;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseActivity;
import com.bjjy.mainclient.phone.view.setting.WebViewActivity;
import com.dongao.mainclient.model.common.Constants;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by fengzongwei on 2016/10/19 0019.
 */
public class ForgetPswActivity extends BaseActivity implements ForgetPswView{
    @Bind(R.id.login_new_back)
    ImageView imageView_back;
    @Bind(R.id.registe_ruel_tv)
    TextView registe_ruel_tv;

    private EditText editText_phone;
    private  EditText editText_checknumber;
    private  EditText editText_psw;
    private TextView registe_get_checcknumber_bt;
    private Button registe_bt;
    private ImageView imageView_seePsw,registe_imageView_checkNum_clear,registe_imageView_psw_clear;
    private LinearLayout registe_error_body;
    private TextView registe_error_hint,login_error_hint;
    private ImageView registe_imageView_clear_name;

    private String resend = "倒计时";

    private ForgetPswPersenter forgetPswPersenter;

    private boolean isShowPswNow = false;//当前密码是否可见

    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if(msg.what == 0){
				registe_get_checcknumber_bt.setEnabled(true);
                registe_get_checcknumber_bt.setText("重新获取");
                registe_get_checcknumber_bt.setBackgroundDrawable(getResources().getDrawable(R.drawable.registe_check_bt));
                registe_get_checcknumber_bt.setTextColor(Color.WHITE);
                registe_get_checcknumber_bt.setEnabled(true);
                return;
            }
            registe_get_checcknumber_bt.setText(resend+"("+msg.what+"s"+")");
        }
    };
    
    @OnClick(R.id.registe_ruel_tv) void clickToRule(){
        Intent intent = new Intent(this, WebViewActivity.class);
        intent.putExtra(Constants.APP_WEBVIEW_TITLE, "《用户注册协议》");
        intent.putExtra(Constants.APP_WEBVIEW_URL, "http://member.bjsteach.com/study/static/html/agreement.html");
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reset_psw);
        ButterKnife.bind(this);
        forgetPswPersenter = new ForgetPswPersenter();
        forgetPswPersenter.attachView(this);
        initView();
    }

    @Override
    public void initView() {
        login_error_hint = (TextView)findViewById(R.id.registe_error_hint);
        editText_phone = (EditText)findViewById(R.id.registe_phone_et);
        editText_phone.addTextChangedListener(registeName);
        editText_checknumber = (EditText)findViewById(R.id.registe_phone_checknumber_et);
        editText_checknumber.addTextChangedListener(valiCode);
        editText_psw = (EditText)findViewById(R.id.registe_psw_et);
        editText_psw.addTextChangedListener(registeWatcher);
        registe_get_checcknumber_bt = (TextView)findViewById(R.id.registe_get_checcknumber_bt);
        registe_bt = (Button)findViewById(R.id.registe_bt);
        imageView_seePsw = (ImageView)findViewById(R.id.registe_psw_seePsw_img);
        registe_imageView_checkNum_clear = (ImageView)findViewById(R.id.registe_clear);
        registe_imageView_psw_clear = (ImageView)findViewById(R.id.registe_psw_clear_img);
        registe_error_body = (LinearLayout)findViewById(R.id.registe_error_body);
        registe_error_hint = (TextView)findViewById(R.id.registe_error_hint);
        registe_imageView_clear_name = (ImageView)findViewById(R.id.registe_new_clear_name);

        registe_get_checcknumber_bt.setOnClickListener(this);
        registe_bt.setOnClickListener(this);
        imageView_back.setOnClickListener(this);
        imageView_seePsw.setOnClickListener(this);
        registe_imageView_checkNum_clear.setOnClickListener(this);
        registe_imageView_clear_name.setOnClickListener(this);
        registe_imageView_psw_clear.setOnClickListener(this);

    }

    private TextWatcher valiCode = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (editText_checknumber.getText().toString() != null && !editText_checknumber.getText().toString().equals("")) {
                registe_imageView_checkNum_clear.setVisibility(View.VISIBLE);
            } else {
                registe_imageView_checkNum_clear.setVisibility(View.GONE);
            }
        }
    };

    private TextWatcher registeName = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (editText_phone.getText().toString() != null && !editText_phone.getText().toString().equals("")) {
                registe_imageView_clear_name.setVisibility(View.VISIBLE);
            } else {
                registe_imageView_clear_name.setVisibility(View.GONE);
            }
        }
    };

    private TextWatcher registeWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (editText_psw.getText().toString() != null && !editText_psw.getText().toString().equals("")) {
                imageView_seePsw.setVisibility(View.VISIBLE);
                registe_imageView_psw_clear.setVisibility(View.VISIBLE);
            } else {
                imageView_seePsw.setVisibility(View.GONE);
                registe_imageView_psw_clear.setVisibility(View.GONE);
            }
        }
    };

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.registe_clear:
                editText_checknumber.setText("");
                break;
            case R.id.registe_get_checcknumber_bt:
                registe_error_body.setVisibility(View.INVISIBLE);
                forgetPswPersenter.getMobileValid();
                break;
            case R.id.registe_bt:
                registe_error_body.setVisibility(View.INVISIBLE);
                forgetPswPersenter.getData();
                break;
            case R.id.registe_new_clear_name:
                editText_phone.setText("");
                registe_imageView_clear_name.setVisibility(View.GONE);
                break;
            case R.id.login_new_back:
                finish();
                break;
            case R.id.registe_psw_clear_img:
                registe_imageView_psw_clear.setVisibility(View.GONE);
                imageView_seePsw.setVisibility(View.GONE);
                editText_psw.setText("");
                break;
            case R.id.registe_psw_seePsw_img:
                if (!isShowPswNow) {
                    isShowPswNow = true;
                    imageView_seePsw.setImageResource(R.drawable.psw_see);
                    editText_psw.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    editText_psw.setSelection(editText_psw.getText().toString().length());
                } else {
                    isShowPswNow = false;
                    imageView_seePsw.setImageResource(R.drawable.login_clear_psw);
                    editText_psw.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    editText_psw.setSelection(editText_psw.getText().toString().length());
                }
                break;
        }
    }

    @Override
    public void switchBtStatus() {
        registe_get_checcknumber_bt.setText( 60 + "s");
        registe_get_checcknumber_bt.setTextColor(Color.parseColor("#999999"));
//        registe_get_checcknumber_bt.setBackgroundDrawable(getResources().getDrawable(R.drawable.registe_check_enable_bt));
        registe_get_checcknumber_bt.setEnabled(false);
        new Thread() {
            @Override
            public void run() {
                try {
                    int i = 59;
                    while (i >= 0) {
                        handler.sendEmptyMessage(i);
                        i--;
                        Thread.sleep(1000);
                    }
                } catch (InterruptedException e) {
                }
            }
        }.start();
    }

    @Override
    public void resetSuccess() {
        finish();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {
        forgetPswPersenter.isGetValidNow = false;
        forgetPswPersenter.isLoadingData = false;
        registe_error_body.setVisibility(View.VISIBLE);
        login_error_hint.setText(message);
    }

    @Override
    public Context context() {
        return this;
    }

    @Override
    public String phoneNumber() {
        return editText_phone.getText().toString();
    }

    @Override
    public String valideCode() {
        return editText_checknumber.getText().toString();
    }

    @Override
    public String newPsw() {
        return editText_psw.getText().toString();
    }
}
