package com.bjjy.mainclient.phone.view.payment.ordergoodswithjs.bean;

/**
 * Created by wyc on 2016/6/27.
 */
public class UserInfoJS {
    private String userId;
    private String type;//类型 “app”

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
