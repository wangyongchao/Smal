package com.bjjy.mainclient.phone.view.question.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dongao.mainclient.model.bean.question.QustionBook;
import com.bjjy.mainclient.phone.R;

import java.util.List;

/**
 * Created by dell on 2016/11/29 0029.
 */
public class QuestionSelectBookAdapter extends BaseAdapter{

    private Context context;
    private List<QustionBook> qustionBooks;

    public QuestionSelectBookAdapter(Context context,List<QustionBook> qustionBooks){
        this.context = context;
        this.qustionBooks = qustionBooks;
    }

    @Override
    public int getCount() {
        return qustionBooks.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.question_select_exm_lv_item,parent,false);
            viewHolder = new ViewHolder();
            viewHolder.textView = (TextView)convertView.findViewById(R.id.question_select_exm_lv_item_tv);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder)convertView.getTag();
        }
        viewHolder.textView.setText(qustionBooks.get(position).getName());
        return convertView;
    }
    class ViewHolder{
        TextView textView;
    }
}
