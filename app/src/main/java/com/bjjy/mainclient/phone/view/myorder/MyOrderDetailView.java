package com.bjjy.mainclient.phone.view.myorder;

import com.dongao.mainclient.model.mvp.MvpView;
import com.dongao.mainclient.model.bean.myorder.MyOrderDetail;

/**
 * Created by fengzongwei on 2016/4/7.
 */
public interface MyOrderDetailView extends MvpView {

    String orderId();//当前显示的订单id
    void showData(MyOrderDetail myOrderDetail);//显示详情数据
    void deviceTokenSuccess();//token验证成功
    void deviceTokenFailed();//token验证失败
}
