package com.bjjy.mainclient.phone.view.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.dongao.mainclient.core.webview.WVJBWebViewClient;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.BaseFragment;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author wyc
 */
public class StudyFragment extends BaseFragment {

    private View mRootView;

    @Bind(R.id.webview)
    WebView webView;
    private WVJBWebViewClient webViewClient;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        if (mRootView == null){
            mRootView = inflater.inflate(R.layout.study_fragment,container,false);
        }
        ButterKnife.bind(this,mRootView);

        ViewGroup parent = (ViewGroup) mRootView.getParent();
        if (parent != null){
            parent.removeView(mRootView);
        }

        webView.getSettings().setJavaScriptEnabled(true);
        // 修改ua使得web端正确判断
        webView.getSettings().setUserAgentString("dongao/payH5");
        webView.setWebChromeClient(new WebChromeClient());
        //webView.loadUrl("http://172.16.198.143/jstc/jstc/views");
       //webView.loadUrl("http://172.16.200.22/zsfa/to_swx.html");
       // webView.loadUrl("http://172.16.200.22/videoAct/to_video.html");

       // http://172.16.200.22/zsfa/to_swx.html
        //http://p.m.test.com/zsfa/to_swx.htm
         webView.loadUrl("http://172.16.208.156/appInfo/to_order.html");

        webViewClient = new MyWebViewClient(webView);
        webViewClient.enableLogging();
        webView.setWebViewClient(webViewClient);

        mRootView.findViewById(R.id.button1).setOnClickListener(this);

        mRootView.findViewById(R.id.button2).setOnClickListener(this);
        return mRootView;
    }

    class MyWebViewClient extends WVJBWebViewClient {
        public MyWebViewClient(WebView webView) {
            // support js send
            super(webView, new WVJBWebViewClient.WVJBHandler() {
                @Override
                public void request(Object data, WVJBResponseCallback callback) {
                    //Toast.makeText(getActivity(), "Android Received message from JS:" + data, Toast.LENGTH_LONG).show();
                    callback.callback("Responsessss for message from Android!");
                }
            });

          /*  // not support js send
            registerHandler("testAndroidCallback", new WVJBWebViewClient.WVJBHandler() {
                @Override
                public void request(Object data, WVJBResponseCallback callback) {
                    Toast.makeText(getActivity(), "testAndroidCallback called:" + data, Toast.LENGTH_LONG).show();
                    callback.callback("Response from testAndroidCallback!");
                }
            });*/

            // not support js send
            registerHandler("returnCommonParams", new WVJBWebViewClient.WVJBHandler() {
                @Override
                public void request(Object data, WVJBResponseCallback callback) {
                    UserBean userBean = new UserBean();
                    userBean.setType("app");
                    userBean.setUserId("21878075");
                    String json = JSON.toJSONString(userBean);
                    Toast.makeText(getActivity(), "Android got response! :" + json, Toast.LENGTH_LONG).show();
                    callback.callback(json);
                }
            });

            // not support js send
            registerHandler("payTableViewController", new WVJBWebViewClient.WVJBHandler() {
                @Override
                public void request(Object data, WVJBResponseCallback callback) {
                    Toast.makeText(getActivity(), "Android got response! :" + data, Toast.LENGTH_LONG).show();
                    callback.callback("response payTableViewController");
                }
            });

            try {
                callHandler("getJSRegisterInfoHandler",
                        new JSONObject("{\"userId\":\"21878075\",\"type\":\"app\" }"),
                        new WVJBResponseCallback() {
                            @Override
                            public void callback(Object data) {
                                Toast.makeText(getActivity(), "Android call testJavascriptHandler got response! :" + data, Toast.LENGTH_LONG).show();
                            }
                        });
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return super.shouldOverrideUrlLoading(view, url);
        }
    }
    @Override
    public void initView() {

    }

    @Override
    public void initData() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button1:
                try {
                    webViewClient.callHandler("getJSRegisterInfoHandler", new JSONObject("{\"greetingFromAndroid\": \"Hi there, JS!\" }"), new WVJBWebViewClient.WVJBResponseCallback() {

                        @Override
                        public void callback(Object data) {
                            Toast.makeText(getActivity(), "getJSRegisterInfoHandler responded: " + data, Toast.LENGTH_LONG).show();
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.button2:
                try {
                    webViewClient.callHandler("getAppUserInfoTransToM", new JSONObject("{\"greetingFromAndroid\": \"Hi there, JS!\" }"), new WVJBWebViewClient.WVJBResponseCallback() {

                        @Override
                        public void callback(Object data) {
                            Toast.makeText(getActivity(), "getNativeInfoHandler responded: " + data, Toast.LENGTH_LONG).show();
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}
