package com.bjjy.mainclient.phone.view.daytest;

import com.dongao.mainclient.model.mvp.MvpView;
import com.dongao.mainclient.model.bean.daytest.DayExSelectedSubject;

import java.util.List;

/**
 * Created by dell on 2016/5/24 0024.
 */
public interface DayTestView extends MvpView{

    void showData(List<DayExSelectedSubject> selectedSubjects);

}
