package com.bjjy.mainclient.phone.view.play.domain;

import android.content.Context;

import com.dongao.mainclient.model.bean.course.CoursePlay;
import com.yunqing.core.db.DBExecutor;
import com.yunqing.core.db.sql.Sql;
import com.yunqing.core.db.sql.SqlFactory;

/**
 * Created by wyc on 2015/1/12.
 */
public class CoursePlayDB {
    Context mContext;
    DBExecutor dbExecutor = null;
    Sql sql = null;

    public CoursePlayDB(Context mContext){
        this.mContext = mContext;
        dbExecutor = DBExecutor.getInstance(mContext);
    }

    public void insert(CoursePlay coursePlay){
        boolean flag=find(coursePlay);
        if(!flag){
            dbExecutor.insert(coursePlay);
        }
    }

    public void update(CoursePlay coursePlay){
        dbExecutor.updateById(coursePlay);
    }

    public boolean delete(CoursePlay coursePlay){
        return dbExecutor.deleteById(CoursePlay.class, coursePlay.getDbId());
    }

    /**
     * 查询方法
     * @return
     */
    public boolean find(CoursePlay coursePlay){
        sql = SqlFactory.find(CoursePlay.class).where("id=?", new Object[]{coursePlay.getId()});
        CoursePlay course=dbExecutor.executeQueryGetFirstEntry(sql);
        if(course!=null){
            return true;
        }
        return false;
    }

    /**
     * 查询方法
     * @return
     */
    public CoursePlay findCourse(String courseId){
        sql = SqlFactory.find(CoursePlay.class).where("id=?", new Object[]{courseId});
        return dbExecutor.executeQueryGetFirstEntry(sql);
    }

}
