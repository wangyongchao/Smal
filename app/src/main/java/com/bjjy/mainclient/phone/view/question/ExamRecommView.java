package com.bjjy.mainclient.phone.view.question;

import com.dongao.mainclient.model.bean.question.QuestionRecomm;
import com.dongao.mainclient.model.mvp.MvpView;

import java.util.List;

/**
 * Created by fengzongwei on 2016/12/13 0013.
 */
public interface ExamRecommView extends MvpView{
    String examQuestionId();
    void showRecommQuesList(List<QuestionRecomm> questionRecomms);
    void showNoData();
    void showNetError();
}
