package com.bjjy.mainclient.phone.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.bjjy.mainclient.phone.download.DownloadTask;
import com.bjjy.mainclient.phone.download.DownloadTaskManager;
import com.bjjy.mainclient.phone.download.DownloadTaskManagerThread;
import com.bjjy.mainclient.phone.download.db.DownloadDB;
import com.dongao.mainclient.core.util.FileUtil;
import com.dongao.mainclient.core.util.StringUtil;
import com.dongao.mainclient.core.util.SystemUtils;
import com.dongao.mainclient.model.bean.play.CourseWare;
import com.dongao.mainclient.model.common.Constants;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.bjjy.mainclient.phone.download.DownloadExcutor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

/**
 * 应用程序配置类：用于保存用户相关信息及设置
 */
public class AppConfig {
	
	private final static String APP_CONFIG = "config";//存储文件
	
	public final static String CONF_APP_UNIQUEID = "APP_UNIQUEID";
	public final static String CONF_EXPIRESIN = "expiresIn";
	public final static String CONF_LOAD_IMAGE = "perf_loadimage";
	public final static String CONF_COOKIE = "cookie";
	
	private Context mContext;
	private static AppConfig appConfig;

	DownloadTaskManager manager = DownloadTaskManager.getInstance();
	private static DownloadDB db;

	public static AppConfig getAppConfig(Context context)
	{
		if(appConfig == null){
			appConfig = new AppConfig();
			appConfig.mContext = context;
			db=new DownloadDB(context);
		}
		return appConfig;
	}
	
	/**
	 * 获取Preference设置
	 */
	public static SharedPreferences getSharedPreferences(Context context)
	{
		return PreferenceManager.getDefaultSharedPreferences(context);
	}
	
	/**
	 * 是否加载显示文章图片
	 */
	public static boolean isLoadImage(Context context)
	{
		return getSharedPreferences(context)
				.getBoolean(CONF_LOAD_IMAGE, true);
	}	
	
	/**
	 * 设置缓存存储的时间
	 * @param expiresIn
	 */
	public void setExpiresIn(long expiresIn){
		set(CONF_EXPIRESIN, String.valueOf(expiresIn));
	}
	
	public long getExpiresIn(){
		return StringUtil.toLong(get(CONF_EXPIRESIN));
	}
	
	/**
	 * 获取某一项的配置
	 * @param key
	 * @return
	 */
	public String get(String key)
	{
		Properties props = get();
		return (props!=null)?props.getProperty(key):null;
	}
	
	/**
	 * 获取存储的properties 主要设定存储文件的路径
	 * @return
	 */
	public Properties get() {
		FileInputStream fis = null;
		Properties props = new Properties();
		try{
			File dirConf = mContext.getDir(APP_CONFIG, Context.MODE_PRIVATE);
			fis = new FileInputStream(dirConf.getPath() + File.separator + APP_CONFIG);
			
			props.load(fis);
		}catch(Exception e){
		}finally{
			try {
				fis.close();
			} catch (Exception e) {}
		}
		return props;
	}
	
	/**
	 * 把配置写入存储文件
	 * @param p
	 */
	private void setProps(Properties p) {
		FileOutputStream fos = null;
		try{
			File dirConf = mContext.getDir(APP_CONFIG, Context.MODE_PRIVATE);
			File conf = new File(dirConf, APP_CONFIG);
			fos = new FileOutputStream(conf);
			
			p.store(fos, null);
			fos.flush();
		}catch(Exception e){	
			e.printStackTrace();
		}finally{
			try {
				fos.close();
			} catch (Exception e) {}
		}
	}

	/**
	 * 把properties全部存储到文件中
	 * @param ps
	 */
	public void set(Properties ps)
	{
		Properties props = get();
		props.putAll(ps);
		setProps(props);
	}
	
	/**
	 * 设置 key和value
	 * @param key
	 * @param value
	 */
	public void set(String key,String value)
	{
		Properties props = get();
		props.setProperty(key, value);
		setProps(props);
	}
	
	/**
	 * 取消某一项或者多项配置
	 * @param key
	 */
	public void remove(String...key)
	{
		Properties props = get();
		for(String k : key)
			props.remove(k);
		setProps(props);
	}

	/**
	 * 下载
	 * @param cw
	 */
	public DownloadTaskManagerThread downloadTaskManagerThread;
	public void download(CourseWare cw) {
		boolean flag=db.CheckIsDownloaded(SharedPrefHelper.getInstance(mContext).getUserId()+"",cw.getClassId(),cw.getCwId());
		if(flag){
			Toast.makeText(mContext,"该视频已下载",Toast.LENGTH_SHORT).show();
			return;
		}
//		if (AppContext.getInstance().isStart != 1) {
//			manager.clearList();
//			DownloadTaskManagerThread downloadTaskManagerThread = new DownloadTaskManagerThread(mContext);
//			Thread thread = new Thread(downloadTaskManagerThread);
//			thread.start();
//		}
		if (downloadTaskManagerThread == null) {
			manager.clearList();
			downloadTaskManagerThread = new DownloadTaskManagerThread(mContext);
			new Thread(downloadTaskManagerThread).start();
		}else if(downloadTaskManagerThread !=null && AppContext.getInstance().isStart != 1){
			manager.clearList();
			AppContext.getInstance().isStart=1;
		}
		String examId=cw.getExamId();
		String subjectId=cw.getSubjectId();
		String classId=cw.getClassId();
		String cwId=cw.getCwId();
		String version = SystemUtils.getVersion(mContext);

		db.add(SharedPrefHelper.getInstance(mContext).getUserId(),examId,subjectId,classId,cwId, Constants.STATE_Waiting, 0, FileUtil.getDownloadPath(mContext),cw.getCwBean(),cw.getCourseBean(),1,version);
		//从courseWare中读取
		DownloadTask task = new DownloadTask();
		task.setCwDesc(cw.getCwDesc());
		task.setCw(cw);
		task.setVideo_url(cw.getMobileDownloadUrl());//http://videodl.bjsteach.com/2014zs/ck/zcl/ydb/14zsck_zcldd_001_qy_yd/upload/media/m3u8_10/video.m3u8
		//http://172.16.208.9:8080/2767/2767/365/2a5/46b618238581049c159fbfa66205bbfe/video.m3u8
		task.setLectureUrl(cw.getMobileLectureUrl());//http://videodl.bjsteach.com/2014zs/ck/zcl/ydb/14zsck_zcldd_001_qy_yd/lecture.htm
		task.setCourseWareId(cwId);
		task.setUserId(SharedPrefHelper.getInstance(mContext).getUserId());
		task.setCourseId(classId);
		task.setDesPath(FileUtil.getDownloadPath(mContext));
		task.setSectionId(cw.getSectionId());
		task.setSubjectId(subjectId);
		task.setExamId(examId);

		manager.addDownloadTask(task);    //add地址出错了
//		Toast.makeText(mContext,"已加入下载列表",Toast.LENGTH_SHORT).show();
	}

	public void stopDownload(){
		if(downloadTaskManagerThread!=null){
			downloadTaskManagerThread.setStop(true);
			AppContext.getInstance().isStart = 0;
			DownloadExcutor.getInstance(mContext).setFlag(false);
			manager.clearList();
			downloadTaskManagerThread=null;
		}
	}
}
