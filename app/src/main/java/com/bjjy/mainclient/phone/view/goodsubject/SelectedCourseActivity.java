package com.bjjy.mainclient.phone.view.goodsubject;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bjjy.mainclient.phone.view.goodsubject.views.SelectedView;
import com.bjjy.mainclient.phone.widget.swipe.SwipeMenu;
import com.dongao.mainclient.model.bean.courselect.DiscountCode;
import com.dongao.mainclient.model.bean.courselect.Goods;
import com.dongao.mainclient.model.bean.courselect.GoodsInfos;
import com.dongao.mainclient.model.bean.courselect.GoodsResult;
import com.bjjy.mainclient.persenter.SelectedCoursePersenter;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.app.AppContext;
import com.bjjy.mainclient.phone.app.BaseActivity;
import com.bjjy.mainclient.phone.view.goodsubject.adapter.DiscountNumAdapter;
import com.bjjy.mainclient.phone.widget.swipe.SwipeMenuCreator;
import com.bjjy.mainclient.phone.widget.swipe.SwipeMenuItem;
import com.bjjy.mainclient.phone.widget.swipe.SwipeMenuListView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by dell on 2016/4/6.
 */
public class SelectedCourseActivity extends BaseActivity implements SelectedView, SwipeMenuListView.OnMenuItemClickListener {

    @Bind(R.id.top_title_left)
    ImageView top_title_left;

    @Bind(R.id.top_title_text)
    TextView top_title_text;

    @Bind(R.id.selected_course_ls)
    SwipeMenuListView selectedCourseLs;

    private List<Goods> list;
    private List<String> cards;
    private List<String> usedCards;
    private SwipeMenuCreator creator;//swipe滑动
    private DiscountNumAdapter disAdapter;
    private View footView;
    private SelectedCoursePersenter persenter;
    private List<GoodsInfos> goodList;
    private TextView totalPrice, integral, discount;
    private Button sure;
    private EditText cardnum;
    private LinearLayout lldiscount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.selectedgoods_activity);
        ButterKnife.bind(this);
        persenter = new SelectedCoursePersenter();
        persenter.attachView(this);

        initView();
        initData();
    }

    @Override
    public void initView() {
        createSwipe();
        cards = new ArrayList<>();
        usedCards = new ArrayList<>();

        selectedCourseLs.setMenuCreator(creator);
        selectedCourseLs.setOnMenuItemClickListener(this);
        top_title_left.setVisibility(View.VISIBLE);
        top_title_left.setImageResource(R.drawable.back);
        top_title_left.setOnClickListener(this);
        top_title_text.setText("已选课程");

        addBootomView();
        list = (List<Goods>) getIntent().getSerializableExtra("goodsList");

        disAdapter = new DiscountNumAdapter(this);

        selectedCourseLs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(SelectedCourseActivity.this, "1111111111", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void addBootomView() {
        footView = View.inflate(this, R.layout.goods_foot, null);
        cardnum = (EditText) footView.findViewById(R.id.et_cardnum);
        sure = (Button) footView.findViewById(R.id.bt_discard);
        totalPrice = (TextView) footView.findViewById(R.id.tv_totalPrice);
        integral = (TextView) footView.findViewById(R.id.tv_integral);
        discount = (TextView) footView.findViewById(R.id.tv_discount);
        sure.setOnClickListener(this);
        selectedCourseLs.addFooterView(footView);
    }

    private void addcard(final String num,List<DiscountCode> usedCardsList) {
        lldiscount = (LinearLayout) footView.findViewById(R.id.ll_discount);
        final View view = View.inflate(this, R.layout.course_discount_item, null);
        TextView nummm = (TextView) view.findViewById(R.id.tv_cardnum);
        nummm.setText(num);
        TextView disPrice = (TextView) view.findViewById(R.id.tv_discount);
        for(int i=0;i<usedCardsList.size();i++){
            if(num.equals(usedCardsList.get(i).getDiscountCode())){
                disPrice.setText("-¥ "+usedCardsList.get(i).getDiscountAmt());
            }
        }
        ImageView delete = (ImageView) view.findViewById(R.id.img_delete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cards.contains(num)) {
                    cards.remove(num);
                }
                if (usedCards.contains(num)) {
                    usedCards.remove(num);
                }
                initData();
                lldiscount.removeView(view);
            }
        });
        lldiscount.addView(view);
    }

    private void createSwipe() {
        creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {

                // create "open" item
                SwipeMenuItem openItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                openItem.setBackground(new ColorDrawable(Color.rgb(0xff, 0x00,
                        0x00)));
                // set item width
                openItem.setWidth(dp2px(75));
                // set item title
                openItem.setTitle("删除");
                // set item title fontsize
                openItem.setTitleSize(18);
                // set item title font color
                openItem.setTitleColor(Color.WHITE);

                menu.addMenuItem(openItem);
            }
        };

    }

    @Override
    public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
        Toast.makeText(SelectedCourseActivity.this, position + "", Toast.LENGTH_SHORT).show();
        list.remove(position);
        AppContext.getInstance().setGoods(list);
        initData();
        return false;
    }

    @Override
    public void setAdapter(List<GoodsInfos> goodsList) {
        disAdapter.setList(goodsList);
        selectedCourseLs.setAdapter(disAdapter);
    }

    @Override
    public void setResult(GoodsResult resultData) {
        totalPrice.setText("¥" + resultData.getPayAmt());
        integral.setText("（获得积分" + resultData.getIntegral() + "）");
        discount.setText("¥" + resultData.getMemberDiscountAmt());
    }

    @Override
    public void setUsedCards(List<DiscountCode> usedCardsList) {
        if (usedCardsList != null && usedCardsList.size() > 0) {
            for (int i = 0; i < usedCardsList.size(); i++) {
                String cardsNum = usedCardsList.get(i).getDiscountCode();
                if (!usedCardsList.contains(cardsNum)) {
                    usedCards.add(cardsNum);
                }
            }
            addcard(cardnum.getText().toString().trim(),usedCardsList);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_discard:
                String num = cardnum.getText().toString().trim();
                if (TextUtils.isEmpty(num)) {
                    Toast.makeText(SelectedCourseActivity.this, "优惠码不能为空", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (!cards.contains(num)) {
                    cards.add(num);
                    initData();
                }else{
                    Toast.makeText(SelectedCourseActivity.this, "优惠码不能重复使用", Toast.LENGTH_SHORT).show();
                }

                break;
        }

    }

    /**
     * dp2px
     *
     * @param dp
     * @return
     */
    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }

    @Override
    public void initData() {
        persenter.getData();
    }

    @Override
    public List<Goods> listData() {
        return list;
    }

    @Override
    public List<String> listCards() {
        return cards;
    }

    @Override
    public List<String> listUsedCards() {
        return usedCards;
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public Context context() {
        return null;
    }

}
