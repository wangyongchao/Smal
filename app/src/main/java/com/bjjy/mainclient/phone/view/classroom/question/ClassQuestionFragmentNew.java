package com.bjjy.mainclient.phone.view.classroom.question;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bjjy.mainclient.persenter.MyQuestionListNewPersenter;
import com.bjjy.mainclient.phone.event.AddQuestionSuccess;
import com.bjjy.mainclient.phone.event.UpdateEvent;
import com.bjjy.mainclient.phone.utils.NetworkUtil;
import com.bjjy.mainclient.phone.view.classroom.course.HeaderViewPagerFragment;
import com.bjjy.mainclient.phone.view.question.QuestionDetailActivity;
import com.bjjy.mainclient.phone.view.question.adapter.QuestionListAdapter;
import com.bjjy.mainclient.phone.widget.EmptyViewLayout;
import com.bjjy.mainclient.phone.widget.ListViewForScrollView;
import com.bjjy.mainclient.phone.widget.xlistview.XListView;
import com.dongao.mainclient.model.bean.question.QuestionRecomm;
import com.dongao.mainclient.model.common.Constants;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.event.CourseTabTypeEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by fengzongwei on 2016/6/2 0002.
 */
public class ClassQuestionFragmentNew extends HeaderViewPagerFragment implements MyQuestionListNewView,XListView.IXListViewListener {
    @Bind(R.id.myquestion_scroll)
	ListViewForScrollView xListView;
    @Bind(R.id.my_questiont_hintImg)
    ImageView imageView_hint;
    @Bind(R.id.my_ques_list_lodingBody)
    RelativeLayout relativeLayout_hint_body;
    @Bind(R.id.my_ques_list_pb)
    ProgressBar progressBar;
    @Bind(R.id.my_ques_list_picBody)
    LinearLayout linearLayout_pic_body;
    @Bind(R.id.my_ques_list_pic_tvHint)
    TextView textView_hint;
    @Bind(R.id.ketang_lv_body)
    LinearLayout linearLayout_contentBody;
    @Bind(R.id.ketang_scrollView)
    ScrollView scrollView_body;
    @Bind(R.id.ketang_question_ask)
    ImageView imageView_ask;

    private View view;

    private QuestionListAdapter questionListAdapter;

    private int page = 0,pageSize = 1000;

    private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private List<QuestionRecomm> questionRecomms;
    private EmptyViewLayout emptyViewLayout;

    private MyQuestionListNewPersenter myQuestionListPersenter;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
       // setUserVisibleHint(true);
        super.onActivityCreated(savedInstanceState);
    }

    private boolean mHasLoadedOnce = false;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if (isVisibleToUser) {
                scrollView_body.smoothScrollTo(0,0);
//                getData();
            }
        }
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.ketang_question_new,container,false);
        ButterKnife.bind(this, view);
        if(!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
        initView();
        initData();
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void initView() {
        emptyViewLayout = new EmptyViewLayout(getActivity(),linearLayout_contentBody);
//        ViewGroup viewGroup= (ViewGroup) LayoutInflater.from(getActivity()).inflate(R.layout.app_view_error_dongao,null);
//        emptyViewLayout.setNetErrorView(viewGroup);
        emptyViewLayout.setEmptyImageId(R.drawable.ketang_question_none);
        emptyViewLayout.setErrorButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emptyViewLayout.showLoading();
                getData();
            }
        });
        emptyViewLayout.setEmptyButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emptyViewLayout.showLoading();
                getData();
            }
        });
//        xListView.setFocusable(false);
        xListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                questionRecomms.get(position).setReadStatus("0");
                questionListAdapter.notifyDataSetChanged();
                Intent intent = new Intent(getActivity(), QuestionDetailActivity.class);
                intent.putExtra("isMyQuestion", true);
                intent.putExtra("questionAnswerId", questionRecomms.get(position).getQuestionId() + "");
                startActivity(intent);
            }
        });
        imageView_ask.setOnClickListener(this);
    }

    /**
     * 加载数据总方法
     */
    private void getData(){
        if(myQuestionListPersenter == null) {
            myQuestionListPersenter = new MyQuestionListNewPersenter();
            myQuestionListPersenter.attachView(this);
        }
        if(!NetworkUtil.isNetworkAvailable(getActivity())){
            emptyViewLayout.showNetErrorView();
            EventBus.getDefault().post(new CourseTabTypeEvent(Constants.COURSE_TAB_TYPE_QUESTION));
            return;
        }
        myQuestionListPersenter.getData();
        emptyViewLayout.showLoading();
    }

    @OnClick(R.id.ketang_question_add) void addQuestion(){

    }

    @Subscribe
    public void onEventAsync(UpdateEvent event) {
        initData();
    }

    @Subscribe
    public void onEventAsync(AddQuestionSuccess event) {
        initData();
    }

    /**
     * 显示没有数据的提示
     */
    @Override
    public void showNoData(){
//        relativeLayout_hint_body.setVisibility(View.VISIBLE);
//        progressBar.setVisibility(View.GONE);
//        linearLayout_pic_body.setVisibility(View.VISIBLE);
//        imageView_hint.setImageResource(R.drawable.my_question_nodata);
//        textView_hint.setVisibility(View.GONE);
        emptyViewLayout.showEmpty();
        EventBus.getDefault().post(new CourseTabTypeEvent(Constants.COURSE_TAB_TYPE_QUESTION));
    }

    @Override
    public void initData() {
        getData();
    }

    public void freshData(){
        page = 1;
        getData();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ketang_question_ask:
//                Intent intent = new Intent(getActivity(), SelectBookAndKpActivity.class);
//                startActivity(intent);
                break;
        }
    }

    @Override
    public void showNetError() {
        emptyViewLayout.showNetErrorView();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showRetry() {

    }

    @Override
    public void hideRetry() {

    }


    @Override
    public void showError(String message) {
        emptyViewLayout.showError();
        EventBus.getDefault().post(new CourseTabTypeEvent(Constants.COURSE_TAB_TYPE_QUESTION));
//        if(emptyViewLayout!=null)
//            emptyViewLayout.showError();
//        refreshScrollView.stopRefresh();
//        relativeLayout_hint_body.setVisibility(View.VISIBLE);
//        progressBar.setVisibility(View.GONE);
//        linearLayout_pic_body.setVisibility(View.VISIBLE);
//        imageView_hint.setImageResource(R.drawable.error_pic);
//        textView_hint.setText(message);
    }

    @Override
    public Context context() {
        return getActivity();
    }

    @Override
    public int getPage() {
        return page;
    }

    @Override
    public View getScrollableView() {
        return view;
    }

    @Override
    public void showMyQuesList(List<QuestionRecomm> questionRecomms) {
//        if(questionRecomms.size()<pageSize){
//            xListView.setPullLoadEnable(false);
//        }else {
//            xListView.setPullLoadEnable(true);
//        }
        emptyViewLayout.showContentView();
        this.questionRecomms = questionRecomms;
        questionListAdapter = new QuestionListAdapter(getActivity(),questionRecomms);
        xListView.setAdapter(questionListAdapter);
        scrollView_body.smoothScrollTo(0, 0);
        EventBus.getDefault().post(new CourseTabTypeEvent(Constants.COURSE_TAB_TYPE_QUESTION));
    }

    @Override
    public void showMoreList(List<QuestionRecomm> questionRecomms) {
        if(questionRecomms.size()<pageSize){
            if(questionRecomms.size() == 0)
                showError("无更多数据");
        }
        if(this.questionRecomms == null)
            this.questionRecomms = new ArrayList<>();
        this.questionRecomms.addAll(questionRecomms);
        questionListAdapter.notifyDataSetChanged();
    }

    @Override
    public int pageSize() {
        return pageSize;
    }

    @Override
    public void loadMoreFail() {
        page--;
        showError("加载失败");
    }

    @Override
    public void onRefresh() {
        page = 0;
        myQuestionListPersenter.getData();
    }

    @Override
    public void onLoadMore() {
        page++;
        myQuestionListPersenter.getData();
    }
}
