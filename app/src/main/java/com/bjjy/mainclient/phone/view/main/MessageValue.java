package com.bjjy.mainclient.phone.view.main;


import com.yunqing.core.db.annotations.Id;
import com.yunqing.core.db.annotations.Table;

import java.io.Serializable;

@Table(name="t_home")
public class MessageValue implements Serializable{

    @Id
    private int dbId;
    private String id;
    private String createTime;
    private String createTimeStr;

    private String msgValue;

    private MsgValue msgValues;

    private String userId;


    public MsgValue getMsgValues() {
        return msgValues;
    }

    public void setMsgValues(MsgValue msgValues) {
        this.msgValues = msgValues;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateTimeStr() {
        return createTimeStr;
    }

    public void setCreateTimeStr(String createTimeStr) {
        this.createTimeStr = createTimeStr;
    }

    public String getMsgValue() {
        return msgValue;
    }

    public void setMsgValue(String msgValue) {
        this.msgValue = msgValue;
    }

    public int getDbId() {
        return dbId;
    }

    public void setDbId(int dbId) {
        this.dbId = dbId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
