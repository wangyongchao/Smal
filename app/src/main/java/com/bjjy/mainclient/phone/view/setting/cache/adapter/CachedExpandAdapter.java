package com.bjjy.mainclient.phone.view.setting.cache.adapter;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.download.db.DownloadDB;
import com.bjjy.mainclient.phone.event.DeleteEvent;
import com.bjjy.mainclient.phone.view.exam.activity.exampaperlist.bean.Course;
import com.bjjy.mainclient.phone.view.setting.cache.CacheActivity;
import com.bjjy.mainclient.phone.view.setting.cache.domain.GourpDomain;
import com.bjjy.mainclient.phone.widget.DialogManager;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * Created by dell on 2016/8/23.
 */
public class CachedExpandAdapter extends BaseExpandableListAdapter {

    private List<GourpDomain> mList;
    private CacheActivity mContext;

    private DownloadDB db;
    private ImageLoader imageLoader;
    private String userId;

//    public CachedExpandAdapter(MainActivity context, ImageLoader imageLoader){
    public CachedExpandAdapter(CacheActivity context, ImageLoader imageLoader){
        this.mContext = context;
        this.imageLoader=imageLoader;
        db = new DownloadDB(context);
        userId= SharedPrefHelper.getInstance(context).getUserId();
    }

    public void setList(List list) {
        this.mList = list;
    }

    @Override
    public int getGroupCount() {
        return mList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mList.get(groupPosition).getChilds().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return null;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        GroupViewHolder viewHolder;
        if(convertView==null){
            viewHolder=new GroupViewHolder();
            convertView=View.inflate(mContext, R.layout.home_expand_group,null);
            viewHolder.subjectName=(TextView)convertView.findViewById(R.id.subject_name);
            viewHolder.img=(ImageView)convertView.findViewById(R.id.img_expand);
            convertView.setTag(viewHolder);
        }else{
            viewHolder=(GroupViewHolder)convertView.getTag();
        }
        viewHolder.subjectName.setText(mList.get(groupPosition).getName());
        if(isExpanded){
            viewHolder.img.setImageResource(R.drawable.expandlist_up);
        }else{
            viewHolder.img.setImageResource(R.drawable.expandlist_down);
        }
        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition,final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ChildViewHolder holder;
        if (convertView == null) {
            holder = new ChildViewHolder();
            convertView = View.inflate(mContext, R.layout.cached_item, null);
            holder.title = (TextView) convertView.findViewById(R.id.cached_title);
            holder.teacher = (TextView) convertView.findViewById(R.id.cached_teacher);
            holder.count = (TextView) convertView.findViewById(R.id.cached_count);
            holder.img = (ImageView) convertView.findViewById(R.id.cached_img);
            holder.delete = (ImageView) convertView.findViewById(R.id.cached_delete_img);
            holder.line = convertView.findViewById(R.id.line);
            convertView.setTag(holder);
        } else {
            holder = (ChildViewHolder) convertView.getTag();
        }
//        if(position==0){
//            holder.line.setVisibility(View.GONE);
//        }else{
//            holder.line.setVisibility(View.VISIBLE);
//        }
        final Course course=mList.get(groupPosition).getChilds().get(childPosition);
        holder.title.setText(course.getCourseTeacher());
        holder.teacher.setText(course.getCwName());
        holder.count.setText("已缓存"+course.getCourseCount()+"讲");
        if(!TextUtils.isEmpty(course.getCourseImg()) && course.getCourseImg().startsWith("http")){
            imageLoader.displayImage(course.getCourseImg(),holder.img);
        }
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogManager.showNormalDialog(mContext, "确定要删除吗", "提示", "取消", "确定",
                        new DialogManager.CustomDialogCloseListener() {
                            @Override
                            public void yesClick() {
                                db.deleteCourse(userId, course);
                                mList.get(groupPosition).getChilds().remove(childPosition);
                                if(mList.get(groupPosition).getChilds().size()==0){
                                    mList.remove(groupPosition);
                                }
                                EventBus.getDefault().post(new DeleteEvent());
//                                if (mList != null && mList.size() < 1) {
//                                    EventBus.getDefault().post(new DeleteEvent());
//                                } else {
//                                    notifyDataSetChanged();
//                                }
                            }

                            @Override
                            public void noClick() {
                            }
                        });

            }
        });
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    class GroupViewHolder {
        TextView subjectName;
        ImageView img;
    }

    class ChildViewHolder {
        TextView title;
        TextView teacher;
        TextView count;
        ImageView img, delete;
        View line;
    }
}
