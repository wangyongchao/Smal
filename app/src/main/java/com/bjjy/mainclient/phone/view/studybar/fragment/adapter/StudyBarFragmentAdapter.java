package com.bjjy.mainclient.phone.view.studybar.fragment.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dongao.mainclient.core.util.DateUtil;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.view.studybar.fragment.bean.StudyBar;
import com.bjjy.mainclient.phone.view.studybar.Constant;

import java.util.List;

/**
 * @author wyc
 */
public class StudyBarFragmentAdapter extends BaseAdapter {
	private Context mContext;
	public List<StudyBar> options ;

	public StudyBarFragmentAdapter(Context context, List<StudyBar> options) {
		this.mContext = context;
		this.options = options;
	}

	public int getCount() {
		return options.size();
	}

	@Override
	public boolean areAllItemsEnabled() {
		return false;
	}

	@Override
	public boolean isEnabled(int position) {
		 
		return true;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.study_bar_fragment_item, null);
			viewHolder.studybar_item_title = (TextView) convertView.findViewById(R.id.studybar_item_title);
			viewHolder.studybar_item_time = (TextView) convertView.findViewById(R.id.studybar_item_time);
			viewHolder.studybar_item_subtitle = (TextView) convertView.findViewById(R.id.studybar_item_subtitle);
			viewHolder.studybar_left_iv = (ImageView) convertView.findViewById(R.id.studybar_left_iv);
			viewHolder.studybar_fragment_item_tubiao_iv = (ImageView) convertView.findViewById(R.id.studybar_fragment_item_tubiao_iv);
			viewHolder.ll_studybat_item= (LinearLayout) convertView.findViewById(R.id.ll_studybat_item);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		if (options.get(position).getTitle().equals("测试")){
			viewHolder.ll_studybat_item.setVisibility(View.INVISIBLE);
		}else{
			viewHolder.ll_studybat_item.setVisibility(View.VISIBLE);
		}
		viewHolder.studybar_item_title.setText(options.get(position).getTitle());
		viewHolder.studybar_item_subtitle.setText(options.get(position).getMessageTitle());
		if(options.get(position).getUpdateTime()!=null&&!options.get(position).getUpdateTime().isEmpty()){
			viewHolder.studybar_item_time.setText(DateUtil.twoDateDistanceForStudyBar(options.get(position).getUpdateTime()));
		}else{
			viewHolder.studybar_item_time.setText("");
		}
		if (options.get(position).getType()== Constant.STUDY_BAR_TYPE_1){
			viewHolder.studybar_left_iv.setBackgroundResource(R.drawable.studybar_1);
		}else if (options.get(position).getType()== Constant.STUDY_BAR_TYPE_2){
			viewHolder.studybar_left_iv.setBackgroundResource(R.drawable.studybar_2);
		}else if (options.get(position).getType()== Constant.STUDY_BAR_TYPE_3){
			viewHolder.studybar_left_iv.setBackgroundResource(R.drawable.studybar_3);
		}else if (options.get(position).getType()== Constant.STUDY_BAR_TYPE_4){
			viewHolder.studybar_left_iv.setBackgroundResource(R.drawable.studybar_4);
		}
		
		if (options.get(position).isRead()){
			viewHolder.studybar_fragment_item_tubiao_iv.setVisibility(View.INVISIBLE);
		}else{
			viewHolder.studybar_fragment_item_tubiao_iv.setVisibility(View.VISIBLE);
		}
		return convertView;
	 
	}

	class ViewHolder {
		TextView studybar_item_title,studybar_item_time,studybar_item_subtitle;// 已学完/播放中
		ImageView studybar_left_iv,studybar_fragment_item_tubiao_iv;
		LinearLayout ll_studybat_item;
	}

	

}
