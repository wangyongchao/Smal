package com.bjjy.mainclient.phone.view.exam.activity.ExamList;

import android.content.Intent;

import com.alibaba.fastjson.JSON;
import com.bjjy.mainclient.phone.view.exam.activity.exampaperlist.bean.Knowledge;
import com.bjjy.mainclient.phone.view.exam.activity.exampaperlist.bean.KnowledgeListData;
import com.bjjy.mainclient.phone.view.exam.activity.exampaperlist.db.KnowledgeDataDB;
import com.bjjy.mainclient.phone.view.exam.activity.myfault.MyFaultActivity;
import com.dongao.mainclient.model.mvp.BasePersenter;
import com.dongao.mainclient.core.util.NetUtils;
import com.dongao.mainclient.model.local.SharedPrefHelper;
import com.dongao.mainclient.model.remote.ApiClient;
import com.dongao.mainclient.model.remote.ParamsUtils;
import com.dongao.mainclient.model.remote.bean.BaseBean;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.utils.NetworkUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by dell on 2016/5/11.
 */
public class ExamListPercenter extends BasePersenter<ExamListView> {
    private String subjectId;
    private String userId;
    private String typeId;
    private String year;
    public List<Knowledge> knowledgeList;
    private KnowledgeListData knowledgeListData;
    public int page=1;
    public boolean isfreshing=false;   //判断是否正在刷新
    private int totalPage=2;
    private KnowledgeDataDB knowledgeDB;

    @Override
    public void attachView(ExamListView mvpView) {
        super.attachView(mvpView);
        initData();
    }

    private void initData() {
        subjectId= SharedPrefHelper.getInstance(getMvpView().context()).getSubjectId();
        userId=SharedPrefHelper.getInstance(getMvpView().context()).getUserId();
        typeId=SharedPrefHelper.getInstance(getMvpView().context()).getMainTypeId();
        year="";
        knowledgeList= new ArrayList<>();
        knowledgeListData=new KnowledgeListData();
        knowledgeDB=new KnowledgeDataDB(getMvpView().context());
    }

    @Override
    public void getData() {
        getMvpView().showLoading();
        if (NetUtils.checkNet().isAvailable()){
            testData();
            apiModel.getData(ApiClient.getClient().getExamPaperList(ParamsUtils.getInstance(getMvpView().context()).getExamPaperList(page)));

        }else{
            KnowledgeListData data= knowledgeDB.findByUserType(userId + "", typeId,  subjectId);
            if (data==null){
                getMvpView().getEmptyLayout().showError();
                return;
            }
            knowledgeListData=JSON.parseObject(data.getContent(),KnowledgeListData.class);
            knowledgeList=  knowledgeListData.getExaminationList();
            getMvpView().initAdapter();
        }
    }

    private void testData() {
        getMvpView().getEmptyLayout().showContentView();
        page++;
        getMvpView().hideLoading();
        KnowledgeListData testList=new KnowledgeListData();
        testList.setSubjectId(subjectId);
        testList.setType(typeId);
        testList.setUserId(userId + "");
        testList.setYear(year);
        List<Knowledge> kliset=new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Knowledge knowledge=new Knowledge();
            knowledge.setExaminationId(subjectId);
            knowledge.setFinishedQuestions( i);
            knowledge.setExaminationName("hahah" + i);
            knowledge.setTotalQuestions( (5 + i * 10));
            knowledge.setCorrectRate("30%");
            knowledge.setIsSubmit(false);
            kliset.add(knowledge);
        }
        testList.setExaminationList(kliset);
        knowledgeList=kliset;
        getMvpView().initAdapter();
        getMvpView().loadOver();
    }

    public void initRefresh(){
        if (!NetworkUtil.isNetworkAvailable(getMvpView().context())){
            getMvpView().showError(getMvpView().context().getResources().getString(R.string.net_error));
            return;
        }
        page=1;
        if(isfreshing){
            return;
        }
        isfreshing=true;
        SimpleDateFormat sm=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SharedPrefHelper.getInstance(getMvpView().context()).setRealRefreshTime(sm.format(new Date()));
        page = 1;
        getMvpView().getListView().setFootWords("查看更多");
        getData();
    }
    
    public void initLoadMore(){
        if (!NetworkUtil.isNetworkAvailable(getMvpView().context())){
            getMvpView().showError(getMvpView().context().getResources().getString(R.string.net_error));
            return;
        }
        int pageSize = totalPage;
        if (page != pageSize) {
            page++;
            getData();
        } else {
            getMvpView().loadOver();
            getMvpView().setFootWords("没有更多数据了");
        }
    }

    @Override
    public void setData(String obj) {
        try {
            page++;
            getMvpView().hideLoading();
            BaseBean baseBean = JSON.parseObject(obj, BaseBean.class);
            if (baseBean == null) {
                return;
            }
            if (baseBean.getResult().getCode() != 1000) {
                if (baseBean.getResult().getCode() == 9) {
//                        showLoginOtherPlace();
                } else {
                }
                return;
            }
            getMvpView().getEmptyLayout().showContentView();
            knowledgeListData = JSON.parseObject(baseBean.getBody(), KnowledgeListData.class);
            totalPage=knowledgeListData.getTotalPages();
            if (page==1){
                knowledgeList= (ArrayList<Knowledge>) knowledgeListData.getExaminationList();
                getMvpView().initAdapter();
                KnowledgeListData oldData=knowledgeDB.findByUserType(userId + "", typeId,  subjectId);
                if (oldData!=null){
                    knowledgeDB.delete(oldData);
                }
                knowledgeDB.insert(knowledgeListData);
            }
            knowledgeList.addAll(knowledgeListData.getExaminationList());
            getMvpView().refreshAdapter();
            getMvpView().loadOver();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadMore(HashMap<String, String> params) {
        Call<String> call = ApiClient.getClient().getExamPaper(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    String str = response.body();
                    try {
                        getMvpView().hideLoading();
                        BaseBean baseBean = JSON.parseObject(str, BaseBean.class);
                        if (baseBean == null) {
                            return;
                        } else {
                            if (baseBean.getResult().getCode() != 1000) {
                                if (baseBean.getResult().getCode() == 9) {
//                        showLoginOtherPlace();
                                } else {
                                    
                                }
                                return;
                            }
                        }
                    } catch (Exception e) {
                    }

                } else {
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }
    
    public void setOnitemListener(int position){
        if(isfreshing){
            return;
        }
        if (position==1){
            Intent intent=new Intent(getMvpView().context(), MyFaultActivity.class);
            getMvpView().context().startActivity(intent);
        }
        /*nextKnowledge=list.get(i - 1);
        isRun=true;
        if (mainItem.getTypeId() == MainItemEnum.MAIN_TYPE_ZHISHIDIANLIANXI.getId()) {
            SharedPrefHelper.getInstance().setExamTag(Constants.EXAM_TAG_KNOWLEDGE);
        } else if(mainItem.getTypeId() == MainItemEnum.MAIN_TYPE_GAOPINKAODIAN.getId()){
            SharedPrefHelper.getInstance().setExamTag(Constants.EXAM_TAG_HIGHFREQUENCY);
        }else{
            SharedPrefHelper.getInstance().setExamTag(Constants.EXAM_TAG_EVERY_YEAR);
        }

        SharedPrefHelper.getInstance().setKnowledgeId(nextKnowledge.getKnowledgeId());
        SharedPrefHelper.getInstance().setExaminationId(nextKnowledge.getKnowledgeId());
        AnswerLog anser=answerDB.findByExamination(userId, examId,subjectId,typeId,nextKnowledge.getKnowledgeId(),nextKnowledge.getKnowledgeId());
        if(anser==null){//判断是否有做题记录
            Intent intent = new Intent(KnowledgeActivity.this, ExamActivity.class);
            startActivity(intent);
        }else{
            if(anser.isFinished()){
                Intent intent = new Intent(KnowledgeActivity.this, ExamActivity.class);
                //     intent.putExtra("EXAM_KNOWLEDGE",Constants.EXAM_KNOWLEDGE_RESTART);
                startActivity(intent);
            }else{
                dialog_knowledge.show();
            }
        }*/
    }
}
