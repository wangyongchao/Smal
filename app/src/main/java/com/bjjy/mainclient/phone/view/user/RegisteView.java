package com.bjjy.mainclient.phone.view.user;

import com.dongao.mainclient.model.mvp.MvpView;

/**
 * Created by dell on 2016/4/5.
 */
public interface RegisteView extends MvpView {
    String phoneNumber();//注册手机号
    String checkNumber();//手机验证码
    String psw();//用户密码
    boolean isReadRuleAndAccess();//是否已经阅读并接受条款
    void intent();//成功后跳转页面
    String validBtText();//获取验证码按钮显示的文字
    void switchBtStatus();//切换获取验证码按钮的状态
}
