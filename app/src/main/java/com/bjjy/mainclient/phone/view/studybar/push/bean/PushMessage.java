package com.bjjy.mainclient.phone.view.studybar.push.bean;

import com.yunqing.core.db.annotations.Id;
import com.yunqing.core.db.annotations.Table;

import java.io.Serializable;

/**
 * Created by wyc on 16/6/3.
 */
@Table(name="t_pushmessage")
public class PushMessage implements Serializable {

    @Id
    private int dbId;
    
    private String scheme;//项目名称
    private String action;//类型 是哪种推送
    private String target;//目标作用
    private String transformType;//打开页面的方式横推或者上下覆盖
    private Push params;//消息对象

    public int getDbId() {
        return dbId;
    }

    public void setDbId(int dbId) {
        this.dbId = dbId;
    }

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Push getParams() {
        return params;
    }

    public void setParams(Push params) {
        this.params = params;
    }

    public String getTransformType() {
        return transformType;
    }

    public void setTransformType(String transformType) {
        this.transformType = transformType;
    }
}
