package com.bjjy.mainclient.phone.view.book;

import com.yunqing.core.db.annotations.Table;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wyc on 11/8/15.
 */
@Table(name = "t_book")
public class Book implements Serializable {


	/**
	 * couponNo : 1481528381877
	 * couponBookRuleDto : {"categoryName":"会计从业资格","enddateStr":"2016-12-12 12:00:00","goodsName":"2016年会计从业 - 高效私教班","couponBookExplainDtos":[{"explainName":"111111111111"},{"explainName":"2222222222"}]}
	 */

	private String couponNo;
	private CouponBookRuleDtoEntity couponBookRuleDto;

	public String getCouponNo() {
		return couponNo;
	}

	public void setCouponNo(String couponNo) {
		this.couponNo = couponNo;
	}

	public CouponBookRuleDtoEntity getCouponBookRuleDto() {
		return couponBookRuleDto;
	}

	public void setCouponBookRuleDto(CouponBookRuleDtoEntity couponBookRuleDto) {
		this.couponBookRuleDto = couponBookRuleDto;
	}

	public static class CouponBookRuleDtoEntity {
		/**
		 * categoryName : 会计从业资格
		 * enddateStr : 2016-12-12 12:00:00
		 * goodsName : 2016年会计从业 - 高效私教班
		 * couponBookExplainDtos : [{"explainName":"111111111111"},{"explainName":"2222222222"}]
		 */

		private String categoryName;
		private String enddateStr;
		private String goodsName;
		private List<CouponBookExplainDtosEntity> couponBookExplainDtos;

		public String getCategoryName() {
			return categoryName;
		}

		public void setCategoryName(String categoryName) {
			this.categoryName = categoryName;
		}

		public String getEnddateStr() {
			return enddateStr;
		}

		public void setEnddateStr(String enddateStr) {
			this.enddateStr = enddateStr;
		}

		public String getGoodsName() {
			return goodsName;
		}

		public void setGoodsName(String goodsName) {
			this.goodsName = goodsName;
		}

		public List<CouponBookExplainDtosEntity> getCouponBookExplainDtos() {
			return couponBookExplainDtos;
		}

		public void setCouponBookExplainDtos(List<CouponBookExplainDtosEntity> couponBookExplainDtos) {
			this.couponBookExplainDtos = couponBookExplainDtos;
		}

		public static class CouponBookExplainDtosEntity {
			/**
			 * explainName : 111111111111
			 */

			private String explainName;

			public String getExplainName() {
				return explainName;
			}

			public void setExplainName(String explainName) {
				this.explainName = explainName;
			}
		}
	}
}
