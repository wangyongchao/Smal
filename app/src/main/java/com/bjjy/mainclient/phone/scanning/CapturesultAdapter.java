package com.bjjy.mainclient.phone.scanning;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dongao.mainclient.core.util.SystemUtils;
import com.dongao.mainclient.model.bean.play.CourseWare;
import com.bjjy.mainclient.phone.R;
import com.bjjy.mainclient.phone.view.play.audition.AuditionPlayActivity;

import java.util.List;

/**
 * Created by dell on 2016/6/14.
 */
public class CapturesultAdapter extends BaseAdapter {

    private Context context;
    private List<CourseWare> list;

    public CapturesultAdapter(Context context) {
        this.context = context;
    }

    public void setList(List<CourseWare> list) {
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.capturesult_item, null);
            viewHolder.title = (TextView) convertView.findViewById(R.id.capture_title);
            viewHolder.time = (TextView) convertView.findViewById(R.id.capture_time);
            viewHolder.play = (ImageView) convertView.findViewById(R.id.capture_play);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        int startTime=Integer.parseInt(list.get(position).getBeginSec());
        int endTime=Integer.parseInt(list.get(position).getEndSec());

        viewHolder.title.setText(list.get(position).getCwName());
        viewHolder.time.setText(SystemUtils.secToTime(startTime)+"--"+SystemUtils.secToTime(endTime));
        viewHolder.play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent itent=new Intent(context, AuditionPlayActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("cw", list.get(0));
                itent.putExtras(bundle);
                context.startActivity(itent);
            }
        });

        return convertView;
    }

    public class ViewHolder {
        TextView title;
        TextView time;
        ImageView play;
    }
}
