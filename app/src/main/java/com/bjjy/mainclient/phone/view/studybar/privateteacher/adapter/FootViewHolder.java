package com.bjjy.mainclient.phone.view.studybar.privateteacher.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.bjjy.mainclient.phone.R;

public class FootViewHolder extends RecyclerView.ViewHolder  {

    public final TextView no_data;
    private PrivateTeacherAdapter.ReclerViewItemClick reclerViewItemClick;

    public FootViewHolder(View itemView) {
        super(itemView);
        no_data = (TextView) itemView.findViewById(R.id.no_data);
    }

}
