package com.bjjy.mainclient.jni;


import java.io.ByteArrayOutputStream;

public class FileJNILib {
	
	static {
        System.loadLibrary("filejni");
        
    }
    public static native byte[] videoCrypt(byte[] content);
    public static native byte[] videoDecrypt(byte[] content);

    public static native String videoDecrypt(String dest_file, ByteArrayOutputStream outputstream);
    
}
